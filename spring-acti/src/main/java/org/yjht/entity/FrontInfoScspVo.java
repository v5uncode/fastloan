package org.yjht.entity;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class FrontInfoScspVo implements Serializable{

	private static final long serialVersionUID = 1L;
	/**
     * 审查审批ID
     */
    private String scspId;

    /**
     * 客户编号
     */
    private String custId;

    /**
     * 证件号码
     */
    private String idNo;

    /**
     * 客户姓名
     */
    private String custName;

    /**
     * 客户经理编号
     */
    private String custGrpJl;
    
    /**
     * 所属机构 机构号
     */
    private String orgCd;

    /**
     * 所属机构 机构名
     */
    private String orgName;
    
    /**
     * 法人号
     */
    private String corpCd;

    /**
     * 客户经理姓名
     */
    private String custGrpName;
    
    /**
     * 主营项目
     */
    private String mainProject;
    
    /**
     * 业务品种
     */
    private String serviceBreed;
    
    private String serviceBreedC;//业务品种名称

    /**
     * 业务品种2
     */
    private String serviceBreed2;
    
    private String serviceBreed2C;//业务品种2名称

    /**
     * 担保方式
     */
    private String warrant;
    
    private String[]  warrants={};
    
    private String warrantC;//担保方式名称
    
    /**
     * 评级级别
     */
    private String pjJb;
    
    /**
     * 测评失效日期
     */
    private String loseDate;

    /**
     * 申请金额
     */
    private String applyMoney;

    /**
     * 申请期限
     */
    private String applyLimit;

    /**
     * 数据创建日期
     */
    private String crtDate;

    /**
     * 数据修改日期
     */
    private String mtnDate;

    /**
     * 审批结果
     */
    private String spResult;

    /**
     * 流程状态
     */
    private String processState;

    /**
     * 流程id
     */
    private String processId;
    /**
     * 调查报告是否完成
     */
    private String surveyState;
    /**
     * 授信方案是否完成
     */
    private String creditState;

    private String ext1;

    private String ext2;

    private String ext3;
    
    //***实时节点信息
    private String taskName;

}
