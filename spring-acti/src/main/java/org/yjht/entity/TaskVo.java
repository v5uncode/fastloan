package org.yjht.entity;

import java.util.Date;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 *
 * 流程任务
 */

@Getter
@Setter
@ToString
public class TaskVo extends FrontInfoScspVo{
  
  private String id;   //任务id
  private String name;
  private Date createTime;  //创建时间
  private String assignee;
  private String processInstanceId;//流程实例id
  private String processDefinitionId;//流程定义id
  private String description;
  private String category;

  private Boolean selfFlag;  //是否是自己个人的任务
  
  public TaskVo() {
  }
  public TaskVo(org.activiti.engine.task.Task t) {
    this.id=t.getId();
    this.name=t.getName();
    this.createTime=t.getCreateTime();
    this.assignee=t.getAssignee();
    this.processInstanceId=t.getProcessInstanceId();
    this.processDefinitionId=t.getProcessDefinitionId();
    this.description=t.getDescription();
    this.category=t.getCategory();
  }
}
