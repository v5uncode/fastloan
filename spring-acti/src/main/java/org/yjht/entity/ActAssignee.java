package org.yjht.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * 任务节点和代理人、候选人、候选组的绑定实体
 */
@Table(name = "ACT_ASSIGNEE")
public class ActAssignee implements Serializable {
	@Column(name = "ID")
    @Id
    @GeneratedValue(generator="UUID")
    private String id;
	
	@Column(name = "SID")
    private String sid;
	
	@Column(name = "ASSIGNEE")
    private String assignee;
	
	@Column(name = "ROLE_ID")
    private String roleId;
	
	@Column(name = "ASSIGNEE_TYPE")
    private Integer assigneeType;
	
	@Column(name = "ACTIVTI_NAME")
    private String activtiName;

    private static final long serialVersionUID = 1L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid == null ? null : sid.trim();
    }

    public String getAssignee() {
        return assignee;
    }

    public void setAssignee(String assignee) {
        this.assignee = assignee == null ? null : assignee.trim();
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId == null ? null : roleId.trim();
    }

    public Integer getAssigneeType() {
        return assigneeType;
    }

    public void setAssigneeType(Integer assigneeType) {
        this.assigneeType = assigneeType;
    }

    public String getActivtiName() {
        return activtiName;
    }

    public void setActivtiName(String activtiName) {
        this.activtiName = activtiName == null ? null : activtiName.trim();
    }

    public ActAssignee() {
    }

    public ActAssignee(String sid) {
        this.sid = sid;
    }
}