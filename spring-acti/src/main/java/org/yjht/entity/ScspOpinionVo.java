package org.yjht.entity;

import java.io.Serializable;
import java.util.Date;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 审批意见
 * @author zhaolulu
 *
 */
@Setter
@Getter
@ToString
public class ScspOpinionVo implements Serializable{

	private static final long serialVersionUID = 1L;

	/**
	 * 审查审批id
	 */
	private String scspId; 
	
	/**
	 * 审批结果
	 */
	private String flag;
	
	/**
	 * 审批意见
	 */	
	private String opinion;
	
	
	/**
	 * 流程id
	 */
	private String processInstancesId;
	
	/**
	 * 任务id
	 */
	private String taskId;
	
	/**
	 * 任务名称
	 */
	private String taskName;
	
	
	/**
	 * 审批人id
	 */
	private String opId;
	
	/**
	 * 审批人姓名
	 */
	private String opName;
	
	/**
	 * 创建时间
	 */
	private Date creTime;

	
}
