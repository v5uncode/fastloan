package org.yjht.actlistener;

import java.util.List;
import java.util.Map;

import org.yjht.bean.LrdUser;
import org.yjht.util.SpringUtil;
import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.yjht.entity.FrontInfoScspVo;
import org.yjht.service.LrdUserService;

/**
 * 支行行长任务监听
 * @author zhaolulu
 *
 */
public class SubBranchManagerListener implements TaskListener {

	private static final long serialVersionUID = 1L;

	@Override
	public void notify(DelegateTask delegateTask) {
		Map<String,Object> map = delegateTask.getVariables();
		FrontInfoScspVo scsp=(FrontInfoScspVo) map.get("scsp");
		LrdUserService lrdUserService= SpringUtil.getBean("lrdUserServiceImpl");
		List<LrdUser> list = lrdUserService.findBranchManagerByOrgCd(scsp.getCorpCd(), scsp.getOrgCd(), "0002");
		for(LrdUser user : list){
			delegateTask.addCandidateUser(user.getUserId());
		}
	}

}
