package org.yjht.actlistener;

import java.util.Map;

import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.yjht.entity.FrontInfoScspVo;
/**
 * 客户经理任务监听
 * @author zhaolulu
 *
 */
public class ManageListener implements TaskListener{

	private static final long serialVersionUID = 1L;

	@Override
	public void notify(DelegateTask delegateTask) {
		System.out.println(delegateTask.getName()+"===="+delegateTask.getCandidates());
		Map<String,Object> map = delegateTask.getVariables();
		FrontInfoScspVo scsp=(FrontInfoScspVo) map.get("scsp");
		delegateTask.addCandidateUser(scsp.getCustGrpJl());
	}

}
