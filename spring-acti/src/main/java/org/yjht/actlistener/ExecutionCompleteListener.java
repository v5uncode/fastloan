package org.yjht.actlistener;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;
import org.yjht.bean.FrontInfoScsp;
import org.yjht.bean.cust.CustBase;
import org.yjht.bean.scsp.FrontInfoSurvey;
import org.yjht.core.Constants;
import org.yjht.entity.FrontInfoScspVo;
import org.yjht.mapper.cust.CustBaseMapper;
import org.yjht.mapper.scsp.FrontInfoSurveyMapper;
import org.yjht.service.ScspProcessService;
import org.yjht.util.DateTools;
import org.yjht.util.SendHttpService;
import org.yjht.util.SpResultType;
import org.yjht.util.SpringUtil;
import org.yjht.wechat.bean.ScspResult;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;
/**
 * 流程结束节点监听
 * @author zhaolulu
 *
 */
public class ExecutionCompleteListener implements ExecutionListener{

	private static final long serialVersionUID = 1L;

	@Override
	public void notify(DelegateExecution execution) throws Exception {
		//更改流程状态
		Map<String,Object> map = execution.getVariables();
		FrontInfoScspVo scspVo=(FrontInfoScspVo) map.get("scsp");
		String flag = (String) map.get("flag");
		ScspProcessService scspProcessService=SpringUtil.getBean("scspProcessServiceImpl");
		FrontInfoScsp scsp = scspProcessService.selectByPrimaryKey(scspVo.getScspId());
		scsp.setProcessState(Constants.STATYJS);
		if(flag.equals(SpResultType.AGREE.getCode())){
			scsp.setSpResult(SpResultType.AGREE.getCode());
		}else if(flag.equals(SpResultType.NO_AGREE.getCode())){
			scsp.setSpResult(SpResultType.NO_AGREE.getCode());
		}else if(flag.equals(SpResultType.REVOCATION.getCode())){
			scsp.setSpResult(SpResultType.REVOCATION.getCode());
		}	
		scspProcessService.updateByPrimaryKeySelective(scsp);
		sendSpjgToWx(scsp);	
	}

	/**
	 * 向微信端发送审批结果
	 */
	public void sendSpjgToWx(FrontInfoScsp scsp){
		ScspResult scspResult = new ScspResult();
		Example example = new Example(CustBase.class);
		example.createCriteria().andEqualTo("custId",scsp.getCustId()).andEqualTo("isdel","0");
		example.setOrderByClause("CRT_DATE DESC");
		CustBaseMapper custBaseMapper=SpringUtil.getBean("custBaseMapper");
		List<CustBase> bases = custBaseMapper.selectByExample(example);
		if(bases != null && bases.size() > 0){
			CustBase base = bases.get(0);
			scspResult.setIdNo(base.getIdNo());
			scspResult.setName(base.getCustName());
			scspResult.setManagerId(base.getCustGrpJl());
			scspResult.setPhone(base.getTelNo());
		}
		Example exampleSurvey = new Example(FrontInfoSurvey.class);
		exampleSurvey.createCriteria().andEqualTo("scspId",scsp.getScspId());
		exampleSurvey.setOrderByClause("REPORT_DATE DESC");
		FrontInfoSurveyMapper frontInfoSurveyMapper=SpringUtil.getBean("frontInfoSurveyMapper");
		List<FrontInfoSurvey> surveyList = frontInfoSurveyMapper.selectByExample(exampleSurvey);
		if(surveyList != null && surveyList.size() > 0){
			FrontInfoSurvey survey = surveyList.get(0);
			scspResult.setSxed(survey.getSurveyHighLimit());
			scspResult.setSxqx(survey.getSurveyDateLimit());
		}
		scspResult.setModelType("spjg");
		scspResult.setApprovalResult(scsp.getSpResult());
		scspResult.setDealTime(DateTools.getCurrentSysData("yyyy-MM-dd HH:mm:ss"));
		scspResult.setSqje(scsp.getApplyMoney());
		SendHttpService.sendHttp(scspResult);
	}

}
