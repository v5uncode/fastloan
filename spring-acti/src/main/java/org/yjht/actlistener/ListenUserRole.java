package org.yjht.actlistener;

import org.yjht.bean.LrdRole;
import org.yjht.core.Result;
import org.yjht.core.ResultCode;
import org.yjht.dao.LrdUserroleMapper;
import org.yjht.bean.LrdUser;
import org.yjht.service.LrdUserService;
import org.activiti.engine.IdentityService;
import org.activiti.engine.identity.Group;
import org.activiti.engine.identity.User;
import org.activiti.engine.impl.persistence.entity.GroupEntity;
import org.activiti.engine.impl.persistence.entity.UserEntity;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * 切入实现系统用户 角色 用户-角色 同步到 activiti 用户 组 用户组 同步到工作流,模块化 无侵入
 * @author zhaolulu
 */
@Aspect
@Component
public class ListenUserRole {

  @Autowired
  IdentityService identityService;

  @Autowired
  LrdUserService userService;
  
  @Autowired
  LrdUserroleMapper lrdUserroleMapper;

  /**********************用户处理begin***************************/
  /**
   * 明确切入方法的参数
   * @param joinPoint
   */
  @Around("execution(org.yjht.core.Result org.yjht.web.LrdUserController.updateUser(..))")
  public Object listenerUserUpdate(ProceedingJoinPoint joinPoint){
    Object o=new Object();
    try{
      //更新前拿到用户-角色数据
      Object[] args = joinPoint.getArgs();
      o = joinPoint.proceed(joinPoint.getArgs());
      Result result= (Result) o;
      if(result.getCode()==ResultCode.SUCCESS.getCode()){
        changeUser(args);
      }
    }catch (Throwable  e){
      e.printStackTrace();
    }
    return o;
  }

  /**
   * 新增用户监听 同步工作流用户表 环绕注解能得到 插入用户id 啊哈哈
   * @param joinPoint
   */
  @Around("execution(org.yjht.core.Result org.yjht.web.LrdUserController.insertUser(..))")
  public Object  listenerUserInsert(ProceedingJoinPoint joinPoint){
    Object o=new Object();
    try{
      o = joinPoint.proceed(joinPoint.getArgs());
      Object[] args = joinPoint.getArgs();
	  Result result= (Result) o;
      if(result.getCode()==ResultCode.SUCCESS.getCode()){
         changeUser(args);
      }

    }catch (Throwable  e){
      e.printStackTrace();
    }
    return o;
  }

  @Around("execution(org.yjht.core.Result org.yjht.web.LrdUserController.deleteUser(..))")
  public Object listenDelUser(ProceedingJoinPoint point){
    Object o=new Object();
    try{
      o = point.proceed(point.getArgs());
      Result result= (Result) o;
      if(result.getCode()==ResultCode.SUCCESS.getCode()){
        Object[] args = point.getArgs();
        identityService.deleteUser((String) args[0]);
      }

    }catch (Throwable  e){
      e.printStackTrace();
    }
    return o;
  }


  /**
   * 保存进 activiti 用户 角色 用户角色中间表
   * @param obj
   */
  private void changeUser(Object[] obj){
    LrdUser user= (LrdUser) obj[0];
    identityService.deleteUser(user.getUserId());
    User au =new UserEntity();
    au.setId(user.getUserId());
    au.setFirstName(user.getUserName());
    au.setEmail(user.getEmail());
    identityService.saveUser(au);

    //删除用户-组关联
    for(String roleId:user.getRoleCds()){
      identityService.deleteMembership(user.getUserId(),roleId);
    }
    //再次关联
    if(user.getRoleCds()!=null){
      for(String roleId:user.getRoleCds()){
        identityService.createMembership(user.getUserId(),roleId);
      }
    }
  }

  /**********************用户处理end***************************/



  /**********************角色处理begin***************************/
  @Around("execution(org.yjht.core.Result org.yjht.web.LrdRoleController.insertLrdRole(..))")
  public Object listenRoleInsert(ProceedingJoinPoint joinPoint){
    Object o=null;
    try{
      o=joinPoint.proceed(joinPoint.getArgs());
      Result j=(Result)o;
      if(j.getCode()==ResultCode.SUCCESS.getCode()){
        Object[] args = joinPoint.getArgs();
        changeRole(args);
      }
    }catch (Throwable throwable){

    }
    return o;
  }

  @Around("execution(org.yjht.core.Result org.yjht.web.LrdRoleController.updateLrdRole(..))")
  public Object listenRoleUpdate(ProceedingJoinPoint joinPoint){
    Object o=null;
    try{
      o=joinPoint.proceed(joinPoint.getArgs());
      Object[] args = joinPoint.getArgs();
      if(((Result)o).getCode()==ResultCode.SUCCESS.getCode()){
       changeRole(args);
      }
    }catch (Throwable throwable){
      
    }
    return o;
  }

  @Around("execution(org.yjht.core.Result org.yjht.web.LrdRoleController.delete(..))")
  public Object listenDelRole(ProceedingJoinPoint point){
    Object o=new Object();
    try{
      o = point.proceed(point.getArgs());
      Result result= (Result) o;
      if(result.getCode()==ResultCode.SUCCESS.getCode()){
        Object[] args = point.getArgs();
        identityService.deleteGroup((String) args[0]);
      }
    }catch (Throwable  e){
      e.printStackTrace();
    }
    return o;
  }

  /**
   * 更新进组
   * @param obj
   */
  public void changeRole(Object[] obj){
    LrdRole role = (LrdRole) obj[0];
    identityService.deleteGroup(role.getRoleCd());
    Group group=new GroupEntity();
    group.setId(role.getRoleCd());
    group.setName(role.getRoleName());
    identityService.saveGroup(group);
  }

  /**********************角色处理end***************************/
}
