package org.yjht.actlistener;

import org.yjht.entity.ActAssignee;
import org.yjht.service.ActAssigneeService;
import org.yjht.service.impl.ActAssigneeServiceImpl;
import org.yjht.util.AssigneeType;
import org.yjht.util.SpringUtil;
import java.util.List;
import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;

/**
 * 
 * 流程监听器 动态注入节点办理人
 */
public class ActNodeListener implements TaskListener{

	private static final long serialVersionUID = 1L;
	
	@Override
	public void notify(DelegateTask delegateTask) {
	   String nodeId=delegateTask.getTaskDefinitionKey();//key
	   ActAssigneeService actAssigneeService= (ActAssigneeService) SpringUtil.getBean(ActAssigneeServiceImpl.class);
	   List<ActAssignee> assigneeList=actAssigneeService.selectList(new ActAssignee(nodeId));
	   for(ActAssignee assignee:assigneeList){
		  switch (assignee.getAssigneeType()){
		     case AssigneeType.GROUP_TYPE:
		      delegateTask.addCandidateGroup(assignee.getRoleId());
		      break;
		     case AssigneeType.USER_TYPE:
		      delegateTask.addCandidateUser(assignee.getAssignee());
		      break;
	      }
	   }
    }
}
