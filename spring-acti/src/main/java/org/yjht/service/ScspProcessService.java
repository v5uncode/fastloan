package org.yjht.service;

import java.util.List;

import org.yjht.base.BaseService;
import org.yjht.bean.FrontInfoScsp;
import org.yjht.core.ReType;
import org.yjht.core.Result;
import org.yjht.entity.FrontInfoScspVo;
import org.yjht.entity.ScspOpinionVo;
import org.yjht.entity.TaskVo;

public interface ScspProcessService extends BaseService<FrontInfoScsp,String>{
	/**
	 * 申请流程信息列表
	 * @param scsp
	 * @param page
	 * @param limit
	 * @return
	 */
	ReType findList(FrontInfoScsp scsp, String page, String limit);
	/**
	 * 提交申请
	 * @param voScsp
	 * @return
	 */
	Result addScsp(FrontInfoScspVo voScsp);
	/**
	 * 查询审核详情列表
	 * @param processId
	 * @return
	 */
	List<ScspOpinionVo> findOptionList(String processId);
	/**
	 * 查询我的任务列表
	 * @param task
	 * @param page
	 * @param limit
	 * @return
	 */
	ReType showTaskList(TaskVo task, String page, String limit);
	
	void complete(ScspOpinionVo op);

}
