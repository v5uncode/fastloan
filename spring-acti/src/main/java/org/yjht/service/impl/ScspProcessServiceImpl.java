package org.yjht.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.activiti.engine.HistoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.history.HistoricDetail;
import org.activiti.engine.history.HistoricVariableUpdate;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.yjht.base.impl.BaseServiceImpl;
import org.yjht.bean.FrontInfoScsp;
import org.yjht.bean.scsp.FrontInfoGuarantor;
import org.yjht.bean.scsp.FrontInfoSurveyMortgage;
import org.yjht.bean.scsp.FrontInfoSurveyPledge;
import org.yjht.bean.vo.User;
import org.yjht.core.Constants;
import org.yjht.core.ReType;
import org.yjht.core.Result;
import org.yjht.enmus.WarrantType;
import org.yjht.entity.FrontInfoScspVo;
import org.yjht.entity.ScspOpinionVo;
import org.yjht.entity.TaskVo;
import org.yjht.exception.MyException;
import org.yjht.service.ScspProcessService;
import org.yjht.service.scsp.GuarantorService;
import org.yjht.service.scsp.SurveyMortgageService;
import org.yjht.service.scsp.SurveyPledgeService;
import org.yjht.util.CommonUtil;
import org.yjht.util.DicReplace;
import org.yjht.util.ResultGenerator;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
@Service
public class ScspProcessServiceImpl extends BaseServiceImpl<FrontInfoScsp,String> implements ScspProcessService {
	@Autowired
	private ScspProcessService scspProcessService;	
	@Autowired
	private RuntimeService runtimeService;
    @Autowired
    private TaskService taskService;    
    @Autowired
    private HistoryService historyService;
    
    @Autowired
    private GuarantorService guarantorService;
    @Autowired
    private SurveyMortgageService surveyMortgageService;
    @Autowired
    private SurveyPledgeService surveyPledgeService;
    

    private  String scspOpinionList="scspOpinionList";
    
	@Override
	public ReType findList(FrontInfoScsp scsp, String page, String limit) {
	  List<FrontInfoScsp> tList=null;
      List<FrontInfoScspVo> tVoList=new ArrayList<FrontInfoScspVo>();
      Page<FrontInfoScsp> tPage= PageHelper.startPage(Integer.valueOf(page),Integer.valueOf(limit),true);     
      tList=scspProcessService.selectList(scsp);  
     
      Map<String, String> dicMap = new HashMap<String, String>();
      dicMap.put("warrant", "WARRANT");
      dicMap.put("serviceBreed", "SERVICE_BREED");
      for(FrontInfoScsp scspInfo:tList){
    	FrontInfoScspVo scspVo=new FrontInfoScspVo();
    	BeanUtils.copyProperties(scspInfo, scspVo);
    	if(StringUtils.isNotBlank(scspInfo.getWarrant())){
    		String[] warrants=scspInfo.getWarrant().split(",");
        	scspVo.setWarrants(warrants);
    	} 	
    	if(StringUtils.isNotBlank(scspInfo.getProcessId())){
    		ProcessInstance instance= runtimeService.createProcessInstanceQuery()
    	            .processInstanceId(scspInfo.getProcessId()).singleResult();
	        //保证运行ing
	        if(instance!=null){
	          Task task = this.taskService.createTaskQuery().processInstanceId(scspInfo.getProcessId()).singleResult();
	          scspVo.setTaskName(task.getName());
	        }
    	}     
        //字典名称转化
        DicReplace.replaceDics(scspVo, dicMap);
        DicReplace.replaceChildDic(scspVo, "serviceBreed", "serviceBreed2");
        tVoList.add(scspVo);
        
      }     
      return new ReType(tPage.getTotal(),tVoList);
	}

	@Override
	@Transactional
	public Result addScsp(FrontInfoScspVo voScsp) {
		if(voScsp==null||StringUtils.isBlank(voScsp.getScspId())){
    		return ResultGenerator.genFailResult("获取数据失败");
    	}
    	FrontInfoScsp scspInfo=scspProcessService.selectByPrimaryKey(voScsp.getScspId());
    	if(addScspCheck(scspInfo)){
	    	Map<String,Object> map=new HashMap<String,Object>();
	        map.put("scsp",voScsp);
	        ProcessInstance processInstance = runtimeService.startProcessInstanceByKey("scsp_process",map);
	        if (processInstance == null) {
	        	return ResultGenerator.genFailResult("未识别key");
	        }
	        voScsp.setProcessId(processInstance.getId());
	        voScsp.setProcessState(Constants.STATLCZ);
	        BeanUtils.copyProperties(voScsp, scspInfo);
	        scspProcessService.updateByPrimaryKeySelective(scspInfo);
    	}
    	return ResultGenerator.genSuccessResult("申批流程提交成功");
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ScspOpinionVo> findOptionList(String processId) {
		//获取流程实例
		ProcessInstance instance= runtimeService.createProcessInstanceQuery()
		          .processInstanceId(processId).singleResult();
        //保证运行ing
        List<ScspOpinionVo> optionList=new ArrayList<>();
        if(instance!=null){
          Task task = this.taskService.createTaskQuery().processInstanceId(processId).singleResult();
          Map<String, Object> variables = taskService.getVariables(task.getId());
          Object o = variables.get(scspOpinionList);
          if(o!=null){
            /*获取历史审核信息*/
            optionList= ( List<ScspOpinionVo>) o;
          }
        }else{
            List<HistoricDetail> list = historyService.createHistoricDetailQuery().
                  processInstanceId(processId).list();
            HistoricVariableUpdate variable=null;
            for(HistoricDetail historicDetail:list){
              variable= (HistoricVariableUpdate) historicDetail;
              String variableName = variable.getVariableName();
              if(scspOpinionList.equals(variableName)){
            	  optionList.clear();
            	  optionList.addAll((List<ScspOpinionVo>)variable.getValue());
              }
            }
        }
		return optionList;
	}

	@Override
	public ReType showTaskList(TaskVo task, String page, String limit) {
		User user = CommonUtil.getUser();
	    List<Task> taskList = taskService.createTaskQuery()
	    		  .taskCandidateUser(user.getUser_id())
	    		  .orderByTaskCreateTime().desc()
	    		  .listPage(Integer.valueOf(limit) * (Integer.valueOf(page) - 1), Integer.valueOf(limit));
	    List<TaskVo> tasks=new ArrayList<TaskVo>();
	    Map<String,Object> map=new HashMap<>();
	    TaskVo taskEntity=null;	 
	    for(Task task1:taskList){
	      map=taskService.getVariables(task1.getId());
	      FrontInfoScspVo scsp= (FrontInfoScspVo) map.get("scsp");
	      if(StringUtils.isNotBlank(scsp.getWarrant())){
	    	String[] warrants=scsp.getWarrant().split(",");
	    	scsp.setWarrants(warrants);
	      } 
	      taskEntity=new TaskVo(task1);
	      BeanUtils.copyProperties(scsp, taskEntity);
	      /**如果是自己*/
	      if(user.getUser_id().equals(scsp.getCustGrpJl())){
        	taskEntity.setSelfFlag(true);
	      }else{
        	taskEntity.setSelfFlag(false);
	      }
	      tasks.add(taskEntity);
	    }
		return new ReType(taskList.size(),tasks);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void complete(ScspOpinionVo op) {
		Map<String, Object> variables = taskService.getVariables(op.getTaskId());
		FrontInfoScspVo scspVo= (FrontInfoScspVo) variables.get("scsp");
		User user = CommonUtil.getUser();
		op.setScspId(scspVo.getScspId());
		op.setCreTime(new Date());
		op.setOpId(user.getUser_id());
		op.setOpName(user.getUserName());
		op.setProcessInstancesId(scspVo.getProcessId());
		Map<String,Object> map=new HashMap<>();
		map.put("flag",op.getFlag());
		//审批信息叠加
		List<ScspOpinionVo> scspOptionList=new ArrayList<>();
		Object o = variables.get("scspOpinionList");
		if(o!=null){
    	  scspOptionList= (List<ScspOpinionVo>) o;
		}
		scspOptionList.add(op);
		map.put("scspOpinionList",scspOptionList);
		String processId = scspProcessService.selectByPrimaryKey(scspVo.getScspId()).getProcessId();
		taskService.addComment(op.getTaskId(), processId, op.getOpinion()==null?"":op.getOpinion());
		taskService.complete(op.getTaskId(),map);	
	}
	
	private boolean addScspCheck(FrontInfoScsp scspInfo){
		boolean flag=true;
		String scspId=scspInfo.getScspId();
		String warrant=scspInfo.getWarrant();
		if(StringUtils.isBlank(warrant)){
			throw new MyException("担保方式不能为空");
		}
		if(warrant.contains(WarrantType.BZ.getCode())){
			List<FrontInfoGuarantor>  guList=guarantorService.findList(scspId);
			if(guList.isEmpty()){
				throw new MyException("担保方式为保证,没有添加保证人信息");
			}else{
				/*for(FrontInfoGuarantor guarantor:guList){
					if(guarantor.getGuarantorAmount()==null){
						throw new MyException("请完善保证人【"+guarantor.getName()+"】的信息");
					}
				}*/
			}			
		}
		if(warrant.contains(WarrantType.DY.getCode())){
			List<FrontInfoSurveyMortgage> moList=surveyMortgageService.findByScspId(scspId);
			if(moList.isEmpty()){
				throw new MyException("担保方式为抵押,没有添加抵押信息");
			}			
		}
		if(warrant.contains(WarrantType.ZY.getCode())){
			List<FrontInfoSurveyPledge>  plList=surveyPledgeService.findByScspId(scspId);
			if(plList.isEmpty()){
				throw new MyException("担保方式为质押,没有添加质押信息");
			}		
		}		
		if(StringUtils.isEmpty(scspInfo.getCreditState())){
    		throw new MyException("请先完成授信方案");
    	}
		if(StringUtils.isEmpty(scspInfo.getSurveyState())){
    		throw new MyException("请先完成调查报告");
    	}  	
    	return flag;
	}
}
