package org.yjht.service.impl;

import org.yjht.mapper.ActAssigneeMapper;
import org.yjht.service.ActAssigneeService;
import org.yjht.service.base.AbstractService;

import org.yjht.base.impl.BaseServiceImpl;
import org.yjht.entity.ActAssignee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ActAssigneeServiceImpl extends BaseServiceImpl<ActAssignee,String> implements
    ActAssigneeService{

  @Autowired
  ActAssigneeMapper actAssigneeMapper;

  @Override
  public int deleteByNodeId(String nodeId) {
    return actAssigneeMapper.deleteByNodeId(nodeId);
  }
}
