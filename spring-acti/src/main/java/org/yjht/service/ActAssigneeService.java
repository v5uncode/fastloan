package org.yjht.service;

import org.yjht.base.BaseService;
import org.yjht.entity.ActAssignee;

public interface ActAssigneeService extends BaseService<ActAssignee,String>{
  int deleteByNodeId(String nodeId);

}
