package org.yjht.mapper;

import org.yjht.common.MyMapper;
import org.yjht.entity.ActAssignee;

public interface ActAssigneeMapper extends MyMapper<ActAssignee>{

    int deleteByNodeId(String nodeId);
}