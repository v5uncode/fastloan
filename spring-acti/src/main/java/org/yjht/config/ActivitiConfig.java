package org.yjht.config;

import javax.sql.DataSource;

import org.activiti.spring.SpringProcessEngineConfiguration;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceBuilder;
@Configuration
public class ActivitiConfig {
	@Value("${mybatis.mapper-locations}")
	String mapsrc;
	@Bean
	@ConfigurationProperties(prefix = "spring.datasource")
	public DataSource druidDataSource() {
		return DruidDataSourceBuilder.create().build();
	}
	 /* @Autowired
	  DruidDataSource druidDataSource;
	  */
	  @Bean
	public PlatformTransactionManager transactionManager() {
		return new DataSourceTransactionManager(druidDataSource());
	}
	
	  @Bean
	  public SpringProcessEngineConfiguration getProcessEngineConfiguration(){
	      SpringProcessEngineConfiguration config =  
	                             new SpringProcessEngineConfiguration();
	      //指定数据库
	      config.setDataSource(druidDataSource());
	      config.setTransactionManager(transactionManager());
	      config.setDatabaseType("mysql");
	      //表不存在创建表
	      config.setDatabaseSchemaUpdate("true");
	      //历史变量
	      config.setHistory("full");//最完整的历史记录，除了包含audit级别的信息之外还能保存详细，例如：流程变量。
	      //指定字体
	      config.setActivityFontName("宋体");
	      config.setAnnotationFontName("宋体");
	      config.setLabelFontName("宋体");
	      return config;
	  }
}
