package org.yjht.util;
/**
 * 审批结果枚举
 * @author zhaolulu
 */
public enum SpResultType {
	AGREE("T","同意"),
	NO_AGREE("F","拒绝"),
	BACK("B","驳回"),
	REVOCATION("R","撤销");
	
	private String code;
    private String desc;
    
    SpResultType(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}
    
}
