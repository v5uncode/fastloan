package org.yjht.util;

/**
 *
 * 工作流节点绑定类型
 */
public class AssigneeType {
  public static final int USER_TYPE=1;
  public static final int USER_S_TYPE=2;
  public static final int GROUP_TYPE=3;
}
