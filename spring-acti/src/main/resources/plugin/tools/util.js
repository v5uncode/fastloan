var tip = {
    alert: function (info, iconIndex) {
        layer.msg(info, {
            icon: iconIndex
        });
    }
} ;
$(function(){
    // 设置jQuery Ajax全局的参数
    $.ajaxSetup({
      //  type: "POST",
        error: function(jqXHR, textStatus, errorThrown){
            switch (jqXHR.status){
                case(500):
                    tip.alert("服务器系统内部错误");
                    break;
                case(401):
                    tip.alert("未登录");
                	parent.location.href='/login';
                    break;
                case(403):
                    tip.alert("无权限执行此操作");
                    break;
                case(408):
                    tip.alert("请求超时");
                    break;
                default:
                    tip.alert("未知错误");
            }
        },
        beforeSend:function(){
            layer.load(2, {shade: [0.6, '#fff']})
        },
        complete:function(){
          layer.closeAll('loading');
        }
    });
});
//获取路径参数
getUrlParam = function(name)
{
    var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)");
    var r = window.location.search.substr(1).match(reg);
    if (r!=null) return decodeURI(r[2]); return null;
};

//select枚举值选项
function selectOnload(settings){
	var defaultSetting = {
			json:{},//
			func:"",
			length:0,//json长度
			isDefault:true,//是否开启默认值
	};
	$.extend(defaultSetting,settings);
	var json = defaultSetting.json,
		func = defaultSetting.func,
		length = defaultSetting.length,
		isDefault = defaultSetting.isDefault;
	var n = 0;	
	for(var x in json){// id:val
		selectEva(x,json[x]);
	}
	function selectEva(id,val){	
		$.get("/admin/dic/"+val,null,function(data){
			if(data.code=='200'){
				n++;
				var ids=$("#"+id);
				if(data.data!=null && data.data.length){
					//ids.append('<option value="" checked>-请选择-</option>');
					var result=data.data;
			        for(var i=0;i<result.length;i++){			        	
			        	if(result[i].children&&result[i].children.length){			        		
			        		ids.append('<optgroup label="'+result[i].label+'">')
			        		var children=result[i].children;
			        		var value=result[i].value;
			        		for(var j=0;j<children.length;j++){
			        			ids.append('<option value="'+value+','+children[j].value+'">'+children[j].label+'</option>')
			        		}
			        		ids.append('</optgroup>')
			        	}else{
			        		ids.append('<option value="'+result[i].value+'">'+result[i].label+'</option>')
			        	}		            
			        }
				}
			}else{
				//ids.append("<option value='' checked>-请选择-</option>");
			}
			while(n>=length){	
				if(length!="0"&&func){
					func();
					break;
				}else{
					break;
				}	
			}
	    });
		
	}
}
//layui多级联动下拉框
function selectOnload2(settings){
    var $form;
    var form;
    var $;
    layui.use(['jquery', 'form'], function() {
        $ = layui.jquery;
        form = layui.form;
        $form = $('form');
    });
	var defaultSetting = {
			json:{},//
			func:"",
			length:0//json长度
	};
	$.extend(defaultSetting,settings);
	var json = defaultSetting.json,
		func = defaultSetting.func,
		length = defaultSetting.length;
	
	var n = 0;
	for(var i=0;i<json.length;i++){// id:val
		selectEva(json[i].name,json[i].code,json[i].value);
	}
	function selectEva(name,code,value){
		var init = function() {
			$.get("/admin/dic/"+code, function(data) {		
				if(data.code=="200"){
					n++;
					var result=data.data;
					loadSelect(result,name);
					if(value){
						var filter=name;
						var resultdata=result;
						var selectValue=value.split(",");
						var i=0;
						while(i<selectValue.length){
							if(resultdata!=null){
								$('select[name='+filter+']').val(selectValue[i]);
								filter=filter.substring(0,filter.length-1)+(parseInt(filter.substring(filter.length-1,filter.length))+1); 			
								resultdata=getData(resultdata,selectValue[i]);
								if(resultdata&&resultdata.length>0){
									loadSelect(resultdata,filter);
								}
							}
							i++;
						}
						form.render('select');
					}
				}
				while(n>=length){	
					if(length!="0"&&func){
						func();
						break;
					}else{
						break;
					}	
				}
			})
		};
		init();
		var getData=function(data,value){
			for (var i = 0; i < data.length; i++) {
				if(data[i].value==value){
					return data[i].children;
				}
			}
			return null;
		}
		//加载数据
		var loadSelect = function(resultdata,filter) {
		       var proHtml = '';
		       proHtml='<option value="">请选择</option>';
		       for (var i = 0; i < resultdata.length; i++) {
		           proHtml += '<option index="'+i+'" count="'+(resultdata[i].children==""?0:resultdata[i].children.length)+'" value="' + resultdata[i].value + '">' + resultdata[i].label + '</option>';
		       }
		       //初始化数据
		       $form.find('select[name='+filter+']').html('');
		       $form.find('select[name='+filter+']').append(proHtml);
		       form.render('select');
		       onSelect(resultdata,filter);
		      
		};
		var onSelect=function(resultdata,filter){
		   	 form.on('select('+filter+')', function(data) {
		   		 var select=data.elem.getAttribute("lay-filter");
		   		 var option=$(data.elem).find("option:selected");
		            var count = option.attr("count");
		            var index = option.attr("index");
		            select=select.substring(0,select.length-1)+(parseInt(select.substring(select.length-1,select.length))+1); 
		            if (count!='undefined'&&count > 0) {              	               	
		            	loadSelect(resultdata[index].children,select);
		            	while(true){
			            	select=select.substring(0,select.length-1)+(parseInt(select.substring(select.length-1,select.length))+1); 
			            	var nextselect = $form.find('select[name='+select+']');
			            	if(nextselect.length&&nextselect.length>0){
			           			nextselect.html('<option value="">请选择</option>');
			           			nextselect=nextselect.attr("lay-filter");
			           		} else{			           			
			           			break;
			           		}
		            	}
		            	
		            } else {
			           	 while(true){
			           		var nextselect = $form.find('select[name='+select+']');
			           		if(nextselect.length&&nextselect.length>0){
			           			nextselect.html('<option value="">请选择</option>');
			           			nextselect=nextselect.attr("lay-filter");
			           			select=nextselect.substring(0,nextselect.length-1)+(parseInt(nextselect.substring(nextselect.length-1,nextselect.length))+1); 
			           		} else{
			           			break;
			           		}
			           	 }
		           	 
		            }
		            form.render('select');
		       });
		};
		
	}	
    
}
