<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>我的任务</title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport"
        content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi"/>
  <link rel="stylesheet" href="${re.contextPath}/plugin/layui/css/layui.css">
  <link rel="stylesheet" href="${re.contextPath}/plugin/lenos/main.css">
  <script type="text/javascript" src="${re.contextPath}/plugin/jquery/jquery-3.2.1.min.js"></script>
  <script type="text/javascript" src="${re.contextPath}/plugin/layui/layui.all.js" charset="utf-8"></script>
  <script type="text/javascript" src="${re.contextPath}/plugin/tools/tool.js"></script>
  <script type="text/javascript" src="${re.contextPath}/plugin/tools/util.js" charset="utf-8"></script>
</head>

<body>
<div class="lenos-search">
  <div class="select">
    <button class="layui-btn layui-btn-sm icon-position-button" id="refresh" style="float: right;"
            data-type="reload">
      <i class="layui-icon">ဂ</i>
    </button>
  </div>
</div>

<table id="taskList" class="layui-hide" lay-filter="task"></table>

<script type="text/html" id="serviceBreedC">
  {{#if(d.selfFlag){}} 
    <div lay-event="serviceBreedSelect">{{d.serviceBreedC}}&nbsp;{{d.serviceBreed2C}} 
	     <i class="layui-icon" style="float: right;">&#xe615;</i>
    </div>
  {{# }else{}} 	    
     <div>{{d.serviceBreedC}}&nbsp;{{d.serviceBreed2C}}</div>
  {{# }}}
</script>
<script type="text/html" id="warrantC">
  {{#if(d.selfFlag){}} 
    <div lay-event="warrantSelect">{{d.warrantC}} 
	     <i class="layui-icon" style="float: right;">&#xe615;</i>
    </div>
  {{# }else{}} 	    
     <div>{{d.warrantC}}</div>
  {{# }}}
</script>
<script type="text/html" id="warrantSelect">
	<div style="margin: 15px;" align="center">
	  	<form class="layui-form" action="" id="warrantForm">	  	
		  		<div class="layui-inline">
		  			<select id="warrant" name="warrant" lay-verify="required">
		             	<option value="">请选择</option>
		        	</select>
		     	</div>
	  	</form>
	</div>
</script>
<script type="text/html" id="serviceBreedSelect">
	<div style="margin: 15px;" align="center">
	  	<form class="layui-form" action="" id="serviceBreedForm">	  	
		  		<div class="layui-inline">
		  			<select id="serviceBreed1" name="serviceBreed1" lay-verify="required" lay-filter="serviceBreed1">
		             	<option value="">请选择</option>
		        	</select>
		     	</div>
		     	<div class="layui-inline">
		  			<select id="serviceBreed2" name="serviceBreed2" lay-verify="required" lay-filter="serviceBreed2">
		             	<option value="">请选择</option>
		        	</select>
		     	</div>
	  	</form>
	</div>
</script>
<script type="text/html" id="toolBar">
  {{# if(d.selfFlag){ }}
  <a class="layui-btn layui-btn-normal layui-btn-xs" lay-event="update"><i class="layui-icon">&#xe642;</i>编辑</a>
  <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="scspOption"><i class="layui-icon">&#xe615;</i>审核详情</a>
  {{# }else{ }}
  <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="handle"><i class="layui-icon">&#x1005;</i>办理</a>
  <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="scspOption"><i class="layui-icon">&#xe615;</i>审核详情</a>
  {{# } }}
</script>
<script>
  layui.use('table', function () {
    var table = layui.table;
    //方法级渲染
    table.render({
        id: 'taskList'
      , elem: '#taskList'
      , url: 'showTaskList'
      , response: {
      		statusCode: 200
      	}
      , cols: [[
          {checkbox: true, fixed: true, width: '3%'}
        , {field: 'custName', title: '客户名称',width: '10%', sort: true}
        , {field: 'idNo', title: '证件号码',width: '10%', sort: true}
        , {field: 'serviceBreedC', title: '业务品种',width: '20%', sort: true, templet:'#serviceBreedC'}
        , {field: 'warrantC', title: '主担保方式',width: '8%', sort: true,templet:'#warrantC'}
        , {field: 'custGrpName', title: '申请人', width: '8%', sort: true}
        , {field: 'orgName', title: '申请机构',width: '8%', sort: true}
        , {field: 'name', title: '任务名称',width: '8%', sort: true}
        , {field: 'createTime', title: '创建时间', sort: true,templet: '<div>{{ layui.laytpl.toDateString(d.createTime,"yyyy-MM-dd HH:mm") }}</div>'}
        , {fixed: 'right',width: '15%', title: '操作', toolbar:'#toolBar'}

      ]]
      , page: true
      , height: 'full-80'
    });

    var $ = layui.$, active = {   
      del:function(){
          var checkStatus = table.checkStatus('taskList')
                  , data = checkStatus.data;
          if (data.length ==0) {
              layer.msg('请选择要删除的数据', {icon: 5});
              return false;
          }
          var ids=[];
          for(item in data){
              ids.push(data[item].id);
          }
          del(ids);
        }
      ,reload:function(){
        table.reload('taskList');
      },
    };
    //监听工具条
    table.on('tool(task)', function (obj) {
      var data = obj.data;
      if (obj.event === 'handle') {
      	popup('办理','editScsp?zyxm='+data.mainProject+'&custId='+data.custId+'&scspId='+data.scspId+'&stat=3&taskId='+data.id+'&taskName='+data.name,'100%','100%','task-agent');
      }else if(obj.event === 'update'){
      	if(!(data.serviceBreed&&data.serviceBreed2)){
   	 		layer.msg('请填写业务品种', {icon: 5});
   	 		return;
   	 	}
   	 	if(!data.warrant){
   	 		layer.msg('请填写担保方式', {icon: 5});
   	 		return;
   	 	}
        popup('编辑','editScsp?zyxm='+data.mainProject+'&scspId='+data.scspId+'&stat=2&taskId='+data.id+'&taskName='+data.taskName,'100%','100%','task-update');
      }else if (obj.event === 'warrantSelect') {
  		//担保方式单元格触发事件（下拉选择）
  	    var form=$("#warrantSelect").html();
        layer.open({
          id: 'scsp-warrant'
         ,type: 1
         ,title: false //不显示标题栏
         ,closeBtn: false
         ,area: ['400px', '300px']
         ,shade: 0.4
         ,btn: ['确定', '关闭']
         ,btnAlign: 'c'
         ,moveType: 1 //拖拽模式，0或者1
         ,content: form
         ,yes: function (index) {//确定按钮促发事件
         	if(!$("#warrant").val()){
         		layer.msg('担保方式不能为空', {icon: 5});
         		return;
         	}
         	layer.close(index);//关闭弹出框
         	//数据更新
         	obj.update({
         		  warrant: $("#warrant").val(),
		          warrantC: "<div lay-event='warrantSelect'>"+$("#warrant").find("option:selected").text()+"<i class='layui-icon' style='float: right;'>&#xe615;</i></div>"
		    });          
          },
          success: function (layero, index) {
            var form = layui.form;
            //字典加载回掉函数
            var formRender=function(){
            	layero.find("select[name='warrant']").val(data.warrant);
            	form.render();
            	
            }
            //担保方式字典加载
          	var json = {//(字典枚举)筛选
		        "warrant":"WARRANT" //担保方式
		    };
		    selectOnload({
		        "json" : json,
		        "isDefault" : false,
		        "isElse" : false,
		        "func" :formRender,
		        "length" : 1
			});      	
          }
        });
      }else if(obj.event === 'serviceBreedSelect'){
		//业务品种单元格事件
		layer.open({
	      id: 'scsp-serviceBreed'
	     ,type: 1
	     ,title: false //不显示标题栏
	     ,closeBtn: false
	     ,area: ['400px', '300px']
	     ,shade: 0.4
	     ,btn: ['确定', '关闭']
	     ,btnAlign: 'c'
	     ,moveType: 1 //拖拽模式，0或者1
	     ,content: $("#serviceBreedSelect").html()
	     ,yes: function (index) {
	     	if(!($("#serviceBreed1").val()&&$("#serviceBreed2").val())){
	     		layer.msg('业务品种不能为空', {icon: 5});
	     		return;
	     	}
	     	layer.close(index);
	     	obj.update({
	     		  serviceBreed: $("#serviceBreed1").val(),
	     		  serviceBreed2: $("#serviceBreed2").val(),
	     		  serviceBreedC: "<div>"+$("#serviceBreed1").find("option:selected").text()+"&nbsp;"+$("#serviceBreed2").find("option:selected").text()+"<i class='layui-icon' style='float: right;'>&#xe615;</i></div>"
		    });          
      	  },
	      success: function (layero, index) {
	        var form = layui.form;
	        var formRender=function(){
	        	form.render();
	        }
	      	var json = [{//(字典枚举)筛选
	          "code":"SERVICE_BREED",//行业分类
	       	  "name":"serviceBreed1",//select name
	          "value":data.serviceBreed+','+data.serviceBreed2
			}];
		    selectOnload2({
		        "json" : json,
		        "func" :formRender,
		        "length" : 1
			});      	
	      }
    	});     	
   	   }else if(obj.event === 'scspOption'){
	   	 	layer.open({
	              id: 'leave-detail',
	              type: 2,
	              area: [ '980px', '400px'],
	              fix: false,
	              maxmin: true,
	              shadeClose: false,
	              shade: 0.4,
	              title: '审核详情',
	              content: "scspOption?processId="+data.processInstanceId
	       });
   	   }
    });

    $('.select .layui-btn').on('click', function () {
      var type = $(this).data('type');
      active[type] ? active[type].call(this) : '';
    });

  });
</script>
</body>

</html>
