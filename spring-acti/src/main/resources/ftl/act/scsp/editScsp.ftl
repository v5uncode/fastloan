<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>编辑授信流程</title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <link rel="stylesheet" href="${re.contextPath}/plugin/layui/css/layui.css">
  <link rel="stylesheet" href="${re.contextPath}/plugin/lenos/main.css"/>
  <script type="text/javascript" src="${re.contextPath}/plugin/jquery/jquery-3.2.1.min.js"></script>
  <script type="text/javascript" src="${re.contextPath}/plugin/layui/layui.all.js" charset="utf-8"></script>
  <script type="text/javascript" src="${re.contextPath}/plugin/tools/tool.js" charset="utf-8"></script>
  <script type="text/javascript" src="${re.contextPath}/plugin/tools/util.js" charset="utf-8"></script>
  <script>
  	//iframe自适应高度
	function iFrameHeight(frame) {   
	  	var ifm= document.getElementById(frame);	
	  	if(ifm==null) return;
	  	var subWeb = document.frames ? document.frames[frame].document : ifm.contentDocument;    
	  	if(ifm != null && subWeb != null) {
	  	   ifm.height = subWeb.getElementsByTagName("html")[0].offsetHeight;
	  	}
	} 
  </script>
</head>
<body>
<div class="x-body">
	<div class="layui-tab admin-nav-card layui-tab-brief" lay-filter="scsp">
		<ul class="layui-tab-title">
	    	<li lay-id="report" class="layui-this">调查报告</li>
	    	<li lay-id="creditProject">授信方案</li>
	    	<li lay-id="scspInfo">审查审批表</li>
	    	<li lay-id="scspOption" id="scspOption" style="display:none;">审批意见</li>
	    	<li lay-id="custInfo" id="custInfo" style="display:none;">客户信息</li>
	  	</ul>
	  	<div class="layui-tab-content" style="width:95%">
	  		<iframe  id="myTabsFrame" name="myTabsFrame" src="" onload="iFrameHeight('myTabsFrame')" style="width:100%;border:0">
	    		正在加载
	    	</iframe>
	  	</div>
	</div>
	<div id="toor" style="width: 100%;height: 50px;background-color: white;border-top:1px solid #e6e6e6;
	  position: fixed;bottom: 0px;margin-left:-20px;">
	    <div class="layui-form-item" style=" float: right;margin-right: 10px;margin-top: 8px">
	      <input name="flag" type="hidden">
	      <span id="save">
		      <button class="layui-btn layui-btn-normal" data-type="save">
		        	保存
		      </button>
		  </span>
	      <button id="callprint" class="layui-btn layui-btn-normal" data-type="callprint">
	    	          打印
	      </button> 
          <span id="process" style="margin-left: 10px;margin-right: 10px;">
		      <button class="layui-btn layui-btn-normal" id="ok" data-type="ok" lay-submit>
		        	重新提交
		      </button>
		      <button class="layui-btn layui-btn-normal" id="no" data-type="no" lay-submit>
		        	撤销
		      </button>		      
	      </span>
	      <button class="layui-btn layui-btn-primary" id="close" data-type="close">
		        	关闭
		  </button>
	    </div>
	 </div>
</div>
<script>
layui.use('element', function(){
  var element = layui.element;
  var scspId=getUrlParam("scspId")||"",
  	  custId=getUrlParam("custId")||"",
  	  zyxm=getUrlParam("zyxm")||"",
      stat=getUrlParam("stat")||"1",  //0:流程查看 1：流程编辑  //2：任务编辑
      taskId=getUrlParam("taskId")||"", //任务编辑时流程任务id
      taskName=getUrlParam("taskName")||""; //任务编辑时流程任务名称
  if(stat=="0"){//审批流程查看详情
  	$("#save").hide();
  	$("#process").hide();
  }else if(stat=="1"){//审批流程编辑信息
  	$("#process").hide();
  }else if(stat=='3'){//任务办理
  	$("#save").hide();
  	$("#ok").html("同意");
  	$("#no").html("不同意");
  	$("#callprint").hide();
  	$("#process").hide();
  	$("#scspOption").show();
  	$("#custInfo").show();
  }else if(stat=='2'){//任务编辑
 
  }
  //默认初始化加载
  $("#myTabsFrame").attr("src","/survey?zyxm="+zyxm+"&scspId="+scspId+"&stat="+stat);
  //监听Tab切换，以改变地址hash值
  element.on('tab(scsp)', function(data){
     //location.hash=this.getAttribute('lay-id'); 
     if(data.index==0){//调查报告
     	if(stat=='3'){
     		$("#process").hide();
     	}
     	$("#myTabsFrame").attr("src","/survey?zyxm="+zyxm+"&scspId="+scspId+"&stat="+stat);
     }else if(data.index==1){//授信方案
     	if(stat=='3'){   		
     		$("#process").hide();
     	}
     	$("#myTabsFrame").attr("src","/credit?scspId="+scspId+"&stat="+stat);
     }else if(data.index==2){//审查审批表
     	if(stat=='3'){
     		$("#process").hide();
     	}
     	$("#myTabsFrame").attr("src","/approval?scspId="+scspId);
     }else if(data.index==3){//审批意见
     	$("#process").show();
     	$("#myTabsFrame").attr("src","agent/"+taskId+"/"+taskName);
     }else{
     	if(stat=='3'){
     		$("#process").hide();
     	}
     	$("#myTabsFrame").attr("src","http://"+window.location.host+"/#/khgl/"+custId+"/main?layout=false");
     } 
     

  });
  var active = {
  	  save: function(){  //保存
  	  	myTabsFrame.window.$("#btnCommit").click(); //调用子窗口方法
  	  },
  	  callprint: function(){ //打印
  	  	myTabsFrame.window.myprint(); //调用子窗口方法
  	  },
      close: function (){ //关闭
        var index = parent.layer.getFrameIndex(window.name);
        parent.layer.close(index);
        
      },
      no: function(){
      	if(stat!='3'){
	    	layerAjax('agent/complete',{"taskId":taskId,"taskName":taskName,"flag":false},'taskList');
	    }else{
	    	myTabsFrame.window.$("#no").click();
	    }
	    return false;
      },
      ok: function(){
      	if(stat!='3'){
	    	layerAjax('agent/complete',{"taskId":taskId,"taskName":taskName,"flag":true},'taskList');
	    }else{
	    	myTabsFrame.window.$("#ok").click();
	    }
	    return false;
      }
      
  };
  eleClick(active,'.layui-form-item .layui-btn');
});
</script>
</body>
</html>