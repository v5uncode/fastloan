<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>审批详情</title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport"
        content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi"/>
  <link rel="stylesheet" href="${re.contextPath}/plugin/layui/css/layui.css">
  <script type="text/javascript" src="${re.contextPath}/plugin/jquery/jquery-3.2.1.min.js"></script>
  <script type="text/javascript" src="${re.contextPath}/plugin/layui/layui.all.js"
          charset="utf-8"></script>
  <script type="text/javascript" src="${re.contextPath}/plugin/tools/tool.js" charset="utf-8"></script>
  <script type="text/javascript" src="${re.contextPath}/plugin/tools/util.js" charset="utf-8"></script>
  <style>
    .layui-input {
      height: 30px;
      width: 120px;
    }

    .x-nav {
      padding: 0 20px;
      position: relative;
      z-index: 99;
      border-bottom: 1px solid #e5e5e5;
      height: 32px;
      overflow: hidden;
    }
  </style>
</head>

<body>

<table id="scspOption" class="layui-hide" lay-filter="leave"></table>
<script type="text/html" id="flag">
  {{# if(d.flag){ }}
  <span class="layui-badge layui-bg-green">通过</span>

  {{# }else{ }}
  <span class="layui-badge">未通过</span>
  {{#  } }}
</script>
<script>
  layui.use('table', function () {
    var table = layui.table;
    //方法级渲染
    table.render({
      id: 'scspOption',
      elem: '#scspOption'
      , response: {
      		statusCode: 200
      	}
      , data: ${scspOption}
      , cols: [[
      	 {field: 'taskName', title: '任务名称'}
        ,{field: 'opName', title: '审批人'}
        ,{field: 'approveLimit', title: '审批金额'}
        ,{field: 'rateFloat', title: '利率上浮比例%',width: '15%'}
        , {field: 'opinion', title: '审批信息', width: '30%'}
        , {
          field: 'createTime',
          title: '审批时间',
          templet: '<div>{{ layui.laytpl.toDateString(d.beginTime,"yyyy-MM-dd HH:mm") }}</div>'
        }
        , {field: 'flag', title: '是否通过', width: '10%',templet:'#flag'}
      ]]
      , page: false
      , height: 'full-160'
    });
  });
</script>
</body>

</html>
