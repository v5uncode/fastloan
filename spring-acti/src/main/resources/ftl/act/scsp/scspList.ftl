<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>审查审批列表</title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <link rel="stylesheet" href="${re.contextPath}/plugin/layui/css/layui.css">
  <link rel="stylesheet" href="${re.contextPath}/plugin/lenos/main.css"/>
  <script type="text/javascript" src="${re.contextPath}/plugin/jquery/jquery-3.2.1.min.js"></script>
  <script type="text/javascript" src="${re.contextPath}/plugin/layui/layui.all.js" charset="utf-8"></script>
  <script type="text/javascript" src="${re.contextPath}/plugin/tools/tool.js" charset="utf-8"></script>
  <script type="text/javascript" src="${re.contextPath}/plugin/tools/util.js" charset="utf-8"></script>
</head>
<body>
<div class="layui-tab admin-nav-card layui-tab-brief" lay-filter="scsp">
  <ul class="layui-tab-title">
    <li layid="scspWtj" class="layui-this">未 提 交</li>
    <li layid="scspLcz">流 程 中</li>
    <li layid="scspYjs">已 结 束</li>
  </ul>
  <div class="layui-tab-content">
  	<!--公共搜索框-->
  	<div class="lenos-search">
	  <div class="select">
	  	  客户姓名：
	    <div class="layui-inline">
	      <input class="layui-input"  placeholder="姓名" height="20px" id="custName" autocomplete="off">
	    </div>
	           证件号码：
	    <div class="layui-inline">
	      <input class="layui-input"  placeholder="证件号码" height="20px" id="idNo" autocomplete="off">
	    </div>
	    <button class="select-on layui-btn layui-btn-sm" data-type="select"><i class="layui-icon"></i>
	    </button>
	    <button class="layui-btn layui-btn-sm icon-position-button" id="refresh" style="float: right;"
	            data-type="reload">
	      <i class="layui-icon">ဂ</i>
	    </button>
	  </div>
	</div>
  	<!--选项卡1-->
  	<div class="layui-tab-item layui-show">
		<div class="layui-col-md12">
		    <div class="layui-btn-group">
		        <button class="layui-btn layui-btn-normal" data-type="deleteScsp">
		            <i class="layui-icon">&#xe640;</i>撤销
		        </button>
		    </div>
		</div>
		<table id="scspListWtj" class="layui-hide" lay-filter="scsp"></table>
  	</div>
  	<!--选项卡2-->
    <div class="layui-tab-item">
    	<div class="layui-col-md12">
		    <div class="layui-btn-group">
		        <button class="layui-btn layui-btn-normal" data-type="showScsp">
		            <i class="layui-icon">&#xe615;</i>查看详情
		        </button>
		    </div>
		</div>
		<table id="scspListLcz" class="layui-hide" lay-filter="scsp"></table>
    </div>
    <!--选项卡3-->
    <div class="layui-tab-item">
    	<div class="layui-col-md12">
		    <div class="layui-btn-group">
		        <button class="layui-btn layui-btn-normal" data-type="showScsp">
		            <i class="layui-icon">&#xe615;</i>查看详情
		        </button>
		    </div>
		</div>
		<table id="scspListYjs" class="layui-hide" lay-filter="scsp"></table>
    </div>
  </div>
</div>
<script type="text/html" id="toolBar">
	{{#if(d.processState=='0'){}}
	  <a class="layui-btn layui-btn-normal layui-btn-xs" lay-event="editScsp"><i class="layui-icon">&#xe642;</i>编辑信息</a>
	  <a class="layui-btn layui-btn layui-btn-xs" lay-event="addScspProcess"><i class="layui-icon">&#x1005;</i>提交申请</a>	 
	{{# }else if(d.processState=='1'){}} 
	  <a class="layui-btn layui-btn-warm layui-btn-xs" lay-event="getProcImage"><i class="layui-icon">&#xe615;</i>查看流程</a>
	  <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="scspOption"><i class="layui-icon">&#xe615;</i>审核详情</a>
	{{# }else{}} 
	  <a class="layui-btn layui-btn-warm layui-btn-xs" lay-event="getProcImage"><i class="layui-icon">&#xe615;</i>查看流程</a>
	  <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="scspOption"><i class="layui-icon">&#xe615;</i>审核详情</a>
	{{# }}} 
</script>

<script type="text/html" id="warrantC">
  {{#if(typeof(d.warrant)!='undefined'&&d.warrant!=null){}}
    <div>{{d.warrantC}}
    {{#if(d.processState=='0'){}}     
     <i class="layui-icon" style="float: right;">&#xe615;</i>
    {{# }}}
    </div>
  {{# }else{}}     
    {{#if(d.processState=='0'){}}     
     <i class="layui-icon" style="float: right;">&#xe615;</i>
    {{# }}}
  {{# }}}
</script>
<script type="text/html" id="serviceBreedC">
  {{#if(typeof(d.serviceBreed)!='undefined'&&d.serviceBreed!=null&&d.serviceBreed!=''){}}
    <div>{{d.serviceBreedC}}&nbsp;{{d.serviceBreed2C}}
	    {{#if(d.processState=='0'){}}     
	     <i class="layui-icon" style="float: right;">&#xe615;</i>
	    {{# }}}
    </div>
  {{# }else{}}
  	{{#if(d.processState=='0'){}}     
     <i class="layui-icon" style="float: right;">&#xe615;</i>
    {{# }}}
  {{# }}}
</script>
<script type="text/html" id="warrantSelect">
	<div style="margin: 15px;" align="center">
	  	<form class="layui-form" action="" id="warrantForm">	  	
		  		<div class="layui-inline">
		  			<select id="warrant" name="warrant" lay-verify="required">
		             	<option value="">请选择</option>
		        	</select>
		     	</div>
	  	</form>
	</div>
</script>
<script type="text/html" id="serviceBreedSelect">
	<div style="margin: 15px;" align="center">
	  	<form class="layui-form" action="" id="serviceBreedForm">	  	
		  		<div class="layui-inline">
		  			<select id="serviceBreed1" name="serviceBreed1" lay-verify="required" lay-filter="serviceBreed1">
		             	<option value="">请选择</option>
		        	</select>
		     	</div>
		     	<div class="layui-inline">
		  			<select id="serviceBreed2" name="serviceBreed2" lay-verify="required" lay-filter="serviceBreed2">
		             	<option value="">请选择</option>
		        	</select>
		     	</div>
	  	</form>
	</div>
</script>
<script type="text/html" id="spResultC">
    {{#if(typeof(d.spResult)!='undefined'&&d.spResult!=null&&d.spResult!=''){}}
    	{{#if(d.spResult=='T'){}}
    		<span class="layui-badge layui-bg-blue">通过</span>
    	{{# }else{}}
    		<span class="layui-badge">拒绝</span>
    	{{# }}}
    {{# }else{}}

    {{# }}}
</script>
<script>

var scsp={
	wtj:0,
	lcz:1,
	yjs:2,
	tableId:'scspListWtj'
}
layui.use(['table','form','element'], function(){
  var table = layui.table,
      laydate = layui.laydate,
      element = layui.element,
      form = layui.form;
  //监听Tab切换，以改变地址hash值
  element.on('tab(scsp)', function(data){
     location.hash=this.getAttribute('lay-id');
     if(data.index==0){
     	scsp.tableId='scspListWtj';
     	scspWtj;
     }else if(data.index==1){
     	scsp.tableId='scspListLcz';
     	scspLcz();
     }else{
     	scsp.tableId='scspListYjs';
     	scspYjs();
     }
  });
  //未提交加载     初始加载
  var scspWtj=function(){
  	//方法级渲染
    table.render({
      id: 'scspListWtj'
      , elem: '#scspListWtj'
      , url: 'showScspList/'+scsp.wtj
      , response: {
      		statusCode: 200
      	}
      , done: function(res, curr, count){  }
      , cols: [[
          {checkbox: true, fixed: true, align: 'center', width: '5%'}
        , {field: 'custName', title: '客户名称', align: 'center',width: '10%', sort: true}
        , {field: 'idNo', title: '证件号码', align: 'center', sort: true}
        , {field: 'serviceBreedC', title: '业务品种', event: 'serviceBreedSelect', templet:'#serviceBreedC'}
        , {field: 'warrantC', title: '主担保方式', align: 'center', width: '12%', event: 'warrantSelect', templet:'#warrantC'}
        , {field: 'loseDate', title: '评级失效日期', align: 'center', width: '10%', templet: '<div>{{ layui.laytpl.toDateString(d.endTime,"yyyy-MM-dd") }}</div>'}
        , {field: 'text', fixed: 'right', title: '操作', toolbar:'#toolBar'}
      ]]
      , page: true
      , height: 'full-175'
    });
  }();
  //流程中加载
  var scspLcz=function(){
  	//方法级渲染
    table.render({
      id: 'scspListLcz'
      , elem: '#scspListLcz'
      , url: 'showScspList/'+scsp.lcz
      , response: {
      		statusCode: 200
      	}
      , done: function(res, curr, count){  }
      , cols: [[
          {checkbox: true, fixed: true,align: 'center', width: '5%'}
        , {field: 'custName', title: '客户名称',align: 'center', width: '10%', sort: true}
        , {field: 'idNo', title: '证件号码',align: 'center', sort: true}
        , {field: 'serviceBreedC', title: '业务品种', templet:'#serviceBreedC'}
        , {field: 'warrantC', title: '主担保方式',align: 'center', width: '12%', templet:'#warrantC'}
        , {field: 'taskName', title: '状态',align: 'center'}
        , {field: 'text', fixed: 'right', title: '操作', toolbar:'#toolBar'}
      ]]
      , page: true
      , height: 'full-175'
    });
  }
  //已结束加载
  var scspYjs=function(){
  	//方法级渲染
    table.render({
      id: 'scspListYjs'
      , elem: '#scspListYjs'
      , url: 'showScspList/'+scsp.yjs
      , response: {
      		statusCode: 200
      	}
      , done: function(res, curr, count){  }
      , cols: [[
          {checkbox: true, fixed: true,align: 'center', width: '5%'}
        , {field: 'custName', title: '客户名称',align: 'center', width: '10%', sort: true}
        , {field: 'idNo', title: '证件号码',align: 'center', sort: true}
        , {field: 'serviceBreedC', title: '业务品种', templet:'#serviceBreedC'}
        , {field: 'warrantC', title: '主担保方式',align: 'center', width: '12%', templet:'#warrantC'}
        , {field: 'loseDate', title: '评级失效日期',align: 'center', width: '10%', templet: '<div>{{ layui.laytpl.toDateString(d.endTime,"yyyy-MM-dd") }}</div>'}
        , {field: 'spResult', title: '审批结果',align: 'center', width: '8%', templet: '#spResultC'}
        , {field: 'text', fixed: 'right', title: '操作', align: 'center', toolbar:'#toolBar'}
      ]]
      , page: true
      , height: 'full-175'
    });
  }
  var $ = layui.$, active = {
      select: function () {
        var custName = $('#custName').val().trim();
        var idNo = $('#idNo').val().trim();
        table.reload(scsp.tableId, {
          where: {
            custName: custName,
            idNo:idNo
          }
        });
      }
      ,showScsp: function(){
  	   		var checkStatus = table.checkStatus(scsp.tableId);
  			if(checkStatus.data.length!=1){
  				layer.msg('请选中一行数据', {icon: 5});
  				return;
  			}
  			popup('查看详情','editScsp?zyxm='+checkStatus.data[0].mainProject+'&scspId='+checkStatus.data[0].scspId+'&stat=0','100%','100%','scsp-show');
       }
      ,deleteScsp:function(){
      		var checkStatus = table.checkStatus(scsp.tableId);
      		if(checkStatus.data.length!=1){
      			layer.msg('请选中一行数据', {icon: 5});
      			return;
      		}
      		var data=checkStatus.data;
	   		layer.confirm('确定删除[' + data[0].custName + ']的授信流程？', {
	          	btn: ['确定', '取消'] //按钮
	        }, function () {
	          	del(data[0].scspId);
	        }, function () {
	        });
       }
      ,reload:function(){
	        $('#custName').val('');
	        $('#idNo').val('');
	        table.reload(scsp.tableId, {
	          where: {
	            custName: null,
	            idNo: null
	          }
	        });
       }
    };
    //监听工具条
    table.on('tool(scsp)', function (obj) {  	
      var data = obj.data;
      if (obj.event === 'warrantSelect') {
  		//担保方式单元格触发事件（下拉选择）
  	    var form=$("#warrantSelect").html();
        layer.open({
          id: 'scsp-warrant'
         ,type: 1
         ,title: false //不显示标题栏
         ,closeBtn: false
         ,area: ['400px', '300px']
         ,shade: 0.4
         ,btn: ['确定', '关闭']
         ,btnAlign: 'c'
         ,moveType: 1 //拖拽模式，0或者1
         ,content: form
         ,yes: function (index) {//确定按钮促发事件
         	if(!$("#warrant").val()){
         		layer.msg('担保方式不能为空', {icon: 5});
         		return;
         	}
         	layer.close(index);//关闭弹出框
         	//数据更新
         	obj.update({
         		  warrant: $("#warrant").val(),
		          warrantC: "<div>"+$("#warrant").find("option:selected").text()+"<i class='layui-icon' style='float: right;'>&#xe615;</i></div>"
		    });          
          },
          success: function (layero, index) {
            var form = layui.form;
            //字典加载回掉函数
            var formRender=function(){
            	layero.find("select[name='warrant']").val(data.warrant);
            	form.render();
            	
            }
            //担保方式字典加载
          	var json = {//(字典枚举)筛选
		        "warrant":"WARRANT" //担保方式
		    };
		    selectOnload({
		        "json" : json,
		        "isDefault" : false,
		        "isElse" : false,
		        "func" :formRender,
		        "length" : 1
			});      	
          }
        });
    	}else if(obj.event === 'serviceBreedSelect'){
			//业务品种单元格除法事件
			layer.open({
		      id: 'scsp-serviceBreed'
		     ,type: 1
		     ,title: false //不显示标题栏
		     ,closeBtn: false
		     ,area: ['400px', '300px']
		     ,shade: 0.4
		     ,btn: ['确定', '关闭']
		     ,btnAlign: 'c'
		     ,moveType: 1 //拖拽模式，0或者1
		     ,content: $("#serviceBreedSelect").html()
		     ,yes: function (index) {
		     	if(!($("#serviceBreed1").val()&&$("#serviceBreed2").val())){
		     		layer.msg('业务品种不能为空', {icon: 5});
		     		return;
		     	}
		     	layer.close(index);
		     	obj.update({
		     		  serviceBreed: $("#serviceBreed1").val(),
		     		  serviceBreed2: $("#serviceBreed2").val(),
		     		  serviceBreedC: "<div>"+$("#serviceBreed1").find("option:selected").text()+"&nbsp;"+$("#serviceBreed2").find("option:selected").text()+"<i class='layui-icon' style='float: right;'>&#xe615;</i></div>"
			    });          
	      	},
		      success: function (layero, index) {
		        var form = layui.form;
		        var formRender=function(){
		        	form.render();
		        }
		      	var json = [{//(字典枚举)筛选
		          "code":"SERVICE_BREED",//行业分类
		       	  "name":"serviceBreed1",//select name
		          "value":data.serviceBreed+','+data.serviceBreed2
				}];
			    selectOnload2({
			        "json" : json,
			        "func" :formRender,
			        "length" : 1
				});      	
		      }
	    	});     	
   	 }else if(obj.event === 'addScspProcess'){
   	 	addScspProcess(data);//提交审批流程 	
   	 }else if(obj.event === 'editScsp'){
   	 	if(!(data.serviceBreed&&data.serviceBreed2)){
   	 		layer.msg('请填写业务品种', {icon: 5});
   	 		return;
   	 	}
   	 	if(!data.warrant){
   	 		layer.msg('请填写担保方式', {icon: 5});
   	 		return;
   	 	}
   	 	var data={"scspId":data.scspId,"serviceBreed":data.serviceBreed,"serviceBreed2":data.serviceBreed2,"warrant":data.warrant};
   	 	$.ajax({
              url: "/scsp/saveScspTable/"+data.scspId,
              type: "put",
              data: JSON.stringify(data),
              dataType: "json", 
              contentType: "application/json",
              success: function (result) {
                  if(result.code==200){
                  	//location.href="editScsp?zyxm='+data.mainProject+'&scspId='+data.scspId+'&stat=1";
                      popup('编辑信息','editScsp?zyxm='+data.mainProject+'&scspId='+data.scspId+'&stat=1','100%','100%','scsp-edit');
				  }else{
                      layer.msg(result.message, {icon: 6});
				  }
              }
        });

   	 }else if(obj.event === 'scspOption'){
   	 	layer.open({
              id: 'leave-detail',
              type: 2,
              area: [ '880px', '400px'],
              fix: false,
              maxmin: true,
              shadeClose: false,
              shade: 0.4,
              title: '审核详情',
              content: "scspOption?processId="+data.processId
       });
   	 }else if(obj.event === 'getProcImage'){
        var url='getProcImage?processInstanceId='+data.processId+'';
        layer.open({
          id: 'scsp-image',
          type: 1,
          area: [ '880px', '400px'],
          fix: false,
          maxmin: true,
          shadeClose: false,
          shade: 0.4,
          title: '流程图',
          content: "<img src='"+url+"'/>"
        });
   	 }
   });
   eleClick(active,'.layui-col-md12 .layui-btn');
   eleClick(active,'.select .layui-btn');
});
function addScspProcess(scspInfo) {
	$.ajax({
      url: "addScspProcess",
      type: "post",
      data: JSON.stringify(scspInfo),
      dataType: "json", traditional: true,
      contentType: "application/json",
      success: function (data) {
      	if(data.code==200){
	        layer.msg(data.message, {icon: 6});
	        layui.table.reload(scsp.tableId);    
        }else{
        	layer.msg(data.message, {icon: 5});
        }
      }
    });
}
function del(scspId){
	$.ajax({
	  url: "deleteScsp/"+scspId,
	  type: "delete",
	  dataType: "json", traditional: true,
	  success: function (data) {
	    layer.msg(data.message, {icon: 6});
	    layui.table.reload(scsp.tableId);
	  }
	});
}
</script>
</body>
</html>
