<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>模型列表</title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport"
        content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi"/>
  <link rel="stylesheet" href="${re.contextPath}/plugin/layui/css/layui.css">
  <link rel="stylesheet" href="${re.contextPath}/plugin/lenos/main.css"/>
  <script type="text/javascript" src="${re.contextPath}/plugin/jquery/jquery-3.2.1.min.js"></script>
  <script type="text/javascript" src="${re.contextPath}/plugin/layui/layui.all.js"
          charset="utf-8"></script>
  <script type="text/javascript" src="${re.contextPath}/plugin/tools/tool.js" charset="utf-8"></script>
  <script type="text/javascript" src="${re.contextPath}/plugin/tools/util.js" charset="utf-8"></script>
</head>

<body>
<div class="lenos-search">
  <div class="select">
           模型名称：
    <div class="layui-inline">
      <input class="layui-input" height="20px" id="name" autocomplete="off">
    </div>
    key：
    <div class="layui-inline">
      <input class="layui-input" height="20px" id="key" autocomplete="off">
    </div>
    <button class="select-on layui-btn layui-btn-sm" data-type="select"><i class="layui-icon"></i>
    </button>
    <button class="layui-btn layui-btn-sm icon-position-button" id="refresh" style="float: right;"
            data-type="reload">
      <i class="layui-icon">ဂ</i>
    </button>
  </div>
</div>
<div class="layui-col-md12">
    <div class="layui-btn-group">
       <button class="layui-btn layui-btn-normal" data-type="syncdata">
            <i class="layui-icon">&#xe618;</i>同步数据
       </button>
      <button class="layui-btn layui-btn-normal" id="processGroup" data-type="add">
        <i class="layui-icon">&#xe642;</i>新建流程
      </button>
    </div>
  </button>
</div>

<table id="actModelList" class="layui-hide" lay-filter="act"></table>
<script type="text/html" id="toolBar">
  <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="update"><i class="layui-icon">&#xe640;</i>编辑</a>
  <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="open"><i class="layui-icon">&#xe640;</i>发布</a>
  <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del"><i class="layui-icon">&#xe640;</i>删除</a>
</script>
<script>
  $('#processGroup').on('mouseover',function(){
    layer.tips('设置流程节点的代办人/候选人/候选组，目前只开发到组', this,{time:2000});
  });
  layui.use('table', function () {
    var table = layui.table;
    //方法级渲染
    table.render({
      id: 'actModelList'
      , elem: '#actModelList'
      , url: 'showAm'
      , response: {
      		statusCode: 200
      	}
      , cols: [[
          {checkbox: true, fixed: true, width: '5%'}
        , {field: 'id', title: '编号', sort: true}
        , {field: 'name', title: '流程名称', sort: true}
        , {field: 'key', title: 'key', sort: true}
        , {field: 'version', title: '版本', sort: true}
        , {field: 'createTime', title: '创建时间', templet: '<div>{{ layui.laytpl.toDateString(d.createTime,"yyyy-MM-dd HH:mm:ss") }}</div>'}
        , {field: 'text', fixed: 'right', title: '操作', width: '20%', toolbar:'#toolBar'}

      ]]
      , page: true
      , height: 'full-100'
    });

    var $ = layui.$, active = {
      select: function () {
        var name = $('#name').val();
        var key = $('#key').val();
        table.reload('actModelList', {
          where: {
            name: name,
            key: key
          }
        });
      },
     syncdata:function () {
        syncdata();
      }
      ,reload:function(){
       $('#name').val('');
       $('#key').val('');
        table.reload('actModelList', {
          where: {
            name: null,
            key: null
          }
        });
      },add:function(){
        window.location.href='goActiviti'
      }
    };
    //监听工具条
    table.on('tool(act)', function (obj) {
      var data = obj.data;
      if (obj.event === 'open') {
        open(data.id);
      }else if(obj.event === 'update'){
        window.location.href='actUpdate/'+data.id
      }else if(obj.event === 'del') {
        layer.confirm('确定删除[' + data.name + ']？', {
          btn: ['确定', '取消'] //按钮
        }, function () {
          del(data.id);
        }, function () {
        });
      }
    });

    $('.layui-col-md12 .layui-btn').on('click', function () {
      var type = $(this).data('type');
      active[type] ? active[type].call(this) : '';
    });
    $('.select .layui-btn').on('click', function () {
      var type = $(this).data('type');
      active[type] ? active[type].call(this) : '';
    });

  });
  function del(id) {
    $.ajax({
      url: "delModel/"+id,
      type: "delete",
      dataType: "json", 
      traditional: true,
      success: function (d) {    
        if(d.flag){
          layer.msg(d.msg, {icon: 6});
          layui.table.reload('actModelList');
        }else{
          layer.msg(d.msg, {icon: 5});
        }
      }
    });
  }
  function syncdata() {
    $.ajax({
      url: "syncdata",
      type: "post",
      dataType: "json", traditional: true,
      success: function (data) {
        layer.msg(data.message, {icon: 6});
      }
    });
  }
  function open(id) {
    $.ajax({
      url: "open",
      type: "post",
      data: {id: id},
      dataType: "json", traditional: true,
      success: function (data) {
        layer.msg(data.msg, {icon: 6});
        layui.table.reload('actModelList');
      }
    });
  }
</script>
</body>

</html>
