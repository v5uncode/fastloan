var sysUser = {
    baseUrl: "/admin/user/updatePwd",
};
layui.use(['form', 'layedit', 'laydate'], function () {
    var layerTips = layui.layer, //获取当前窗口的layer对象
        form = layui.form,
        layedit = layui.layedit,
        laydate = layui.laydate;
    form.on('submit(formDemo)', function(data){
        var oldPassWord =data.field.oldPassWord;
    	var newPassWord = data.field.newPassWord;
    	var repeatPassWord =data.field.repeatPassWord;
    	if(oldPassWord==newPassWord){
    		layerTips.msg("原密码与新密码不能相同");
    		return false;
    	}else if(newPassWord !=repeatPassWord ){
    		layerTips.msg("新密码与确认密码不一致");
    		return false;
    	}
    	if(newPassWord.length<6){
    		layerTips.msg("密码长度至少六位");
    		return false;
    	}
    	var data0 ={"oldPassWord":$.base64.btoa(oldPassWord),
    				"newPassWord":$.base64.btoa(newPassWord)};
    	$.ajax({
			type:'post',
			dataType:"json",
			contentType:"application/json",
			url:sysUser.baseUrl,
			data:JSON.stringify(data0),
			success:function(data){
				if(data.code=='200'){
					layerTips.msg("修改成功")    						
				}else{
					layerTips.msg(data.message);    						
				}
			},
    	})
    	return false;
    });
    
   /* $("#btn_return").on('click',function(){
    	window.history.back(-1); 
    })*/
})