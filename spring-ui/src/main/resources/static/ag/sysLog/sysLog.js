var sysLog = {
    baseUrl: "/sysLog/log",
    entity: "sysLog",
    tableId: "sysLogTable",
    toolbarId: "toolbar",
    currentItem: {}
};

sysLog.queryParams = function (params,orgCd,userId,usecrtDate,mtnDate) {
	var temp = { //这里的键的名字和控制器的变量名必须一直，这边改动，控制器也需要改成一样的
    	page: params.offset/params.limit+1, //页码
        limit: params.limit,//页面大小
        orgCd:$("#orgCd").attr("orgCd"),
		userId:$("#userId").attr('userId'),
		crtDate:$("#crtDate").val(),
		mtnDate:$("#mtnDate").val()
     };
    return temp;
};

sysLog.responseHandler = function(response){
	if(response.code =='200'){
		return response.data;
	}
	return false;
};


sysLog.init = function (params) {
    sysLog.table = $('#' + sysLog.tableId).bootstrapTable({
    	url: sysLog.baseUrl, //请求后台的URL（*）
    	method: 'get', //请求方式（*）
    	queryParams: sysLog.queryParams,//传递参数（*）
        toolbar: '#' + sysLog.toolbarId, //工具按钮用哪个容器
        cache: false, //是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
        pagination: true, //是否显示分页（*）
        sortOrder: sysLog.order, //排序方式
        pageNumber: 1, //初始化加载第一页，默认第一页
        pageSize: 10, //每页的记录行数（*）
        pageList: [10, 25, 50, 100], //可供选择的每页的行数（*）
        showColumns: false, //是否显示所有的列
        showRefresh: true, //是否显示刷新按钮
        minimumCountColumns: 2, //最少允许的列数
        clickToSelect: true, 
        sidePagination: "server", //分页方式：client客户端分页，server服务端分页（*）
        showToggle: true, //是否显示详细视图和列表视图的切换按钮
        singleSelect:true,
        responseHandler:sysLog.responseHandler,
        columns:[{  
            title: '序号',
            formatter: function (value, row, index) {  
                return index+1;  
            }  
        },{
            field: 'corpCd',
            title: '法人号'
        },{
            field: 'orgCd',
            title: '机构号'
        },{
            field: 'userId',
            title: '用户号'
        },{
            field: 'operDate',
            title: '操作时间'
        },{
            field: 'operName',
            title: '操作名称'
        },{
            field: 'operDX',
            title: '操作对象'
        },{
            field: 'operPara',
            title: '请求参数'
        },{
            field: 'operIg',
            title: '操作结果'
        }]
    });
};

layui.use(['form', 'layedit', 'laydate'], function () {
    sysLog.init();
    var editIndex;
    //var layerTips = parent.layer === undefined ? layui.layer : parent.layer, //获取父窗口的layer对象
    var layerTips = layui.layer;
        layer = layui.layer, //获取当前窗口的layer对象
        form = layui.form,
        layedit = layui.layedit,
        laydate = layui.laydate;
    var addBoxIndex = -1;
    //初始化页面上面的按钮事件
    $('#btn_query').on('click', function () {
    	sysLog.table.bootstrapTable('refresh');
    });
    
    $('#btn_clear').on('click',function(){
    	$("#orgCd").removeAttr("orgCd");
    	$("#orgCd").val('');
    	$("#userId").removeAttr('userId');
    	$("#userId").val('');
		$("#crtDate").val('');
		$("#mtnDate").val('');
    });
    $('#btn_del').on('click', function () {
        layer.confirm('确定删除数据吗？', null, function (index) {
            $.ajax({
                url: sysLog.baseUrl,
                type: "DELETE",
                success: function (data) {
                    if (data.rel == true) {
                        layerTips.msg("移除成功！");
                        location.reload();
                    } else {
                        layerTips.msg("移除失败！")
                        location.reload();
                    }
                }
            });
            layer.close(index);
        });
    });
    //机构查询
    $("#orgCdQuery").on("click",function(){
    	$("#orgCd").val("");
    	$("#myModal1").modal({
    		backdrop:false
    	});
    	upviewdata();
    });
    $("#crtDate,#mtnDate").datepicker();
    //用户查询
    $("#userCdQuery").on("click",function(){
    	$("#userId").val("");
    	$("#myModa2").modal({
    		backdrop:false
    	});
    	upviewdata();
    });
});

function upviewdata(){
	var setting = {
			data: {
				simpleData: {
					enable: true,
				},
			},
			callback: {
				beforeClick: function(treeId, treeNode) {
					var zTree = $.fn.zTree.getZTreeObj("tree");
					$("#orgCd").val(treeNode.name).attr("orgCd",treeNode.id);
					$("#myModal1").modal("hide");
					return true;
				}
			}
		};
	data2={
			"orgReleType":"00"
		};
	$.ajax({
		type:"get",
		url:sysLog.baseUrl+'/log',
		dataType:"json",
		contentType:"application/json",
		data:data2,
		success:function(response){
			if(response.code=="200"){
				var org=response.data.org;
				var role=response.data.user;
				$.fn.zTree.init($("#tree1"), setting, org);
				var html="";
				for(var i=0;i<role.length;i++){
					var result = role[i];
					html+='<tr userId="'+result.userId+'"userName="'+result.userName+'">';
					html+='<td><input type="checkbox" name="checkBox" userName="'+result.userName+'"userId="'+result.userId+'"/></td>';
					html+='<td class="text-center">' + (i+1)+ '</td>';						
					html+='<td>' + result.userId + '</td>';						
					html+='<td>' + result.userName + '</td>';
					html+='</tr>';
				}
				$("#tree2").html(html);
				//console.log(data)
			}else{
				layerTips.msg("移除成功！");
			}
		},error:function(data){
			//console.log("错误");
		}
	});
}
//选择带回
$("#take_back").on("click",function(){
	var checkboxs=$("[name=checkBox]:checked");
	var roleStr="",roleNameStr="";
	if(checkboxs.size()>0){
		if(checkboxs.size()==1){
			roleStr=checkboxs.attr("userId");
			roleNameStr=checkboxs.attr("userName");
			}else{
				for(var i=0;i<checkboxs.size();i++){
					if(i==checkboxs.size()-1){
						roleStr+=checkboxs.eq(i).attr("userId");
						roleNameStr+=checkboxs.eq(i).attr("userName");
					}else{
						roleStr+=checkboxs.eq(i).attr("userId")+",";
						roleNameStr+=checkboxs.eq(i).attr("userName")+",";
					}
				}
			}
	$("#userId").val(roleNameStr).attr("userId",roleStr);
	$("#myModa2").modal("hide");
	}
});