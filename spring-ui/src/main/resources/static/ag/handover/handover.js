var handoverOne = {
    baseUrl: "/turnover",
    entity: "handover",
    tableId: "tableOne",
    toolbarId: "toolbar",
    unique: "id",
    currentItem: []
};
handoverOne.columns = function () {
    return [{
        field: 'sqName',
        title: '发起人'
    }, {
        field: 'custName',
        title: '客户名称'
    }, {
        field: 'idNo',
        title: '身份证号'
    }, {
        field: 'jlName',
        title: '接收人'
    }, {
        field: 'crtDate',
        title: '发起日期'
    }, {
        title: '操作',
        formatter:function(index,row){
        	if(row.PROCESS){
        		return "<button class='layui-btn layui-btn-small' type='button' name='receive' onclick='receive("+JSON.stringify(row)+")'> <i class='layui-icon'>&#xe654;</i>处理</button>";
        	}else{
        		return "正在处理中";
        	}
        }
    }];
};
 function receive(row) {
    handoverOne.currentItem = row;
    return handoverOne.currentItem;
};

handoverOne.queryParams = function (params) {
    var temp = { //这里的键的名字和控制器的变量名必须一直，这边改动，控制器也需要改成一样的
        limit: params.limit, //页面大小
        page: params.offset/params.limit+1, //页码
    };
    return temp;
};

handoverOne.responseHandler = function(response){
	if(response.code =='200'){
		return response.data;
	}
	return false;
};

handoverOne.init = function () {
	handoverOne.table = $('#' + handoverOne.tableId).bootstrapTable({
        url: handoverOne.baseUrl+"/dcl", //请求后台的URL（*）
        method: 'get', //请求方式（*）
        toolbar: '#' + handoverOne.toolbarId, //工具按钮用哪个容器
        striped: true, //是否显示行间隔色
        cache: false, //是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
        pagination: true, //是否显示分页（*）
        sortable: false, //是否启用排序
        sortOrder: handoverOne.order, //排序方式
        queryParams: handoverOne.queryParams,//传递参数（*）
        sidePagination: "server", //分页方式：client客户端分页，server服务端分页（*）
        pageNumber: 1, //初始化加载第一页，默认第一页
        pageSize: 10, //每页的记录行数（*）
        pageList: [10, 25, 50, 100], //可供选择的每页的行数（*）
        strictSearch: false,
        showColumns: false, //是否显示所有的列
        showRefresh: true, //是否显示刷新按钮
        minimumCountColumns: 2, //最少允许的列数
        clickToSelect: true, //是否启用点击选中行
        uniqueId: handoverOne.unique, //每一行的唯一标识，一般为主键列
        showToggle: true, //是否显示详细视图和列表视图的切换按钮
        cardView: false, //是否显示详细视图
        detailView: false, //是否显示父子表
        columns: handoverOne.columns(),
        responseHandler:handoverOne.responseHandler
    });
};


var handoverTwo = {
	baseUrl: "/turnover",
    entity: "handover",
    tableId: "tableTwo",
    toolbarId: "toolbar",
    unique: "id",
    currentItem: []
};

handoverTwo.columns = function () {
    return [{
        field: 'sqName',
        title: '发起人'
    }, {
        field: 'custName',
        title: '客户名称'
    }, {
        field: 'idNo',
        title: '身份证号'
    }, {
        field: 'jlName',
        title: '接收人'
    }, {
        field: 'crtDate',
        title: '发起日期'
    },{
        field: 'updDate',
        title: '处理日期'
    },{
        title: '状态',
        formatter:function(index,row){
        	if(row.stat=='2'){
				return "<button class='layui-btn layui-btn-small' type='button' name='refuse' onclick='refuse("+JSON.stringify(row)+")'> <i class='layui-icon'>&#xe654;</i>已退回</button>";
	    	}else if(row.stat=='3'){
				return "批量移交";
			}else{
				return "已移交";
			}
        }
    }];
};
function refuse(row) {
	handoverTwo.currentItem = row;
    return handoverTwo.currentItem;
};

handoverTwo.queryParams = function (params) {
    var temp = { //这里的键的名字和控制器的变量名必须一直，这边改动，控制器也需要改成一样的
        limit: params.limit, //页面大小
        page: params.offset/params.limit+1, //页码
    };
    return temp;
};

handoverTwo.responseHandler = function(response){
	if(response.code =='200'){
		return response.data;
	}
	return false;
};

handoverTwo.init = function () {
	handoverTwo.table = $('#' + handoverTwo.tableId).bootstrapTable({
        url: handoverTwo.baseUrl+"/ycl", //请求后台的URL（*）
        method: 'get', //请求方式（*）
        toolbar: '#' + handoverOne.toolbarId, //工具按钮用哪个容器
        striped: true, //是否显示行间隔色
        cache: false, //是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
        pagination: true, //是否显示分页（*）
        sortable: false, //是否启用排序
        sortOrder: handoverTwo.order, //排序方式
        queryParams: handoverTwo.queryParams,//传递参数（*）
        sidePagination: "server", //分页方式：client客户端分页，server服务端分页（*）
        pageNumber: 1, //初始化加载第一页，默认第一页
        pageSize: 10, //每页的记录行数（*）
        pageList: [10, 25, 50, 100], //可供选择的每页的行数（*）
        strictSearch: false,
        showColumns: false, //是否显示所有的列
        showRefresh: true, //是否显示刷新按钮
        minimumCountColumns: 2, //最少允许的列数
        clickToSelect: true, //是否启用点击选中行
        uniqueId: handoverTwo.unique, //每一行的唯一标识，一般为主键列
        showToggle: true, //是否显示详细视图和列表视图的切换按钮
        cardView: false, //是否显示详细视图
        detailView: false, //是否显示父子表
        columns: handoverTwo.columns(),
        responseHandler:handoverTwo.responseHandler
    });
};

layui.use(['form', 'layedit', 'laydate', 'element'], function () {
	
	handoverOne.init();
	//handoverTwo.init();
    var layerTips = layui.layer,
	    layer = layui.layer, //获取当前窗口的layer对象
	    layedit = layui.layedit,
	    laydate = layui.laydate;
    Io.on("[name='receive']","click",function(){
    	var row = handoverOne.currentItem;
    	$.get(handoverOne.entity+'/receive', null, function (form) {
            layer.open({
                type: 1,
                title: '处理',
                content: form,
                btn: ['保存', '取消'],
                id:'receive',
                shade: false,
                area: ['100%', '100%'],
                maxmin: true,
                yes: function (index) {
                    //触发表单的提交事件
                    $('form.layui-form').find('button[lay-filter=receive]').click();
                },
                full: function (elem) {
                    var win = window.top === window.self ? window : parent.window;
                    $(win).on('resize', function () {
                        var $this = $(this);
                        elem.width($this.width()).height($this.height()).css({
                            top: 0,
                            left: 0
                        });
                        elem.children('div.layui-layer-content').height($this.height() - 95);
                    });
                },
                success: function (layero, index) {
                    var form = layui.form;
                    setFromValues(layero, row);
                    form.render();
                    form.on('submit(receive)', function (data) {
                    	var number =  $('input:radio:checked').attr('number');
                    	if(number== 1){
                    		data.field.stat = 1;
                    	}else{
                    		data.field.stat = 2;
                    	}
                    	if(data.field.content>=200){
                    		layerTips.msg('备注字数过长');
                    		return false;
                    	}
                    	$.ajax({
                    		url:handoverOne.baseUrl,
                            type: 'put',
                            contentType:"application/json",
                            data:JSON.stringify(data.field),
                            dataType: "json",
                            success: function (data) {
                            	if(data.code=='200'){
                                    layerTips.msg('保存成功');
                                    layer.close(index);
                                    handoverOne.table.bootstrapTable('refresh');
                                    handoverTwo.table.bootstrapTable('refresh');
                            	}else{
                            		layerTips.msg(data.message);
                            		layer.close(index);
                            	}
                            }

                        });
                        //这里可以写ajax方法提交表单
                        return false; //阻止表单跳转。如果需要表单跳转，去掉这段即可。
                    });
                }
            });
        });
    });
    
    Io.on("[name='refuse']","click",function(){
    	var row = handoverTwo.currentItem;
    	$.get(handoverTwo.entity+'/refuse', null, function (form) {
            layer.open({
                type: 1,
                title: '处理',
                content: form,
                //btn: ['保存', '取消'],
                id:'receive',
                shade: false,
                area: ['50%', '100%'],
                maxmin: false,
                yes: function (index) {
                    //触发表单的提交事件
                    $('form.layui-form').find('button[lay-filter=refuse]').click();
                },
                full: function (elem) {
                    var win = window.top === window.self ? window : parent.window;
                    $(win).on('resize', function () {
                        var $this = $(this);
                        elem.width($this.width()).height($this.height()).css({
                            top: 0,
                            left: 0
                        });
                        elem.children('div.layui-layer-content').height($this.height() - 95);
                    });
                },
                success: function (layero, index) {
                    var form = layui.form;
                    setFromValues(layero, row);
                    form.render();
                }
            });
        });
    });
    $('#btn_one').on('click',function(){
    	handoverOne.table.bootstrapTable('refresh');
    });
    $('#btn_two').on('click',function(){
    	handoverTwo.init();
    	//handoverTwo.table.bootstrapTable('refresh');
    })

});
    



