var baseurl = "";
var newCustName = getUrlParam("custName");
var custName = newCustName.split(".")[0];
var custId = newCustName.split(".")[1];
var idNo = newCustName.split(".")[2];
var custType = newCustName.split(".")[3];
var approvalVo={};
var loseDate;
var createRate;
var layer;
//正则表达式：只能输入整数或者小数
var moneyRegular = /^([0-9]*)(\.\d*)?$/;
//查询基准利率
var chparakey ="baseRate";
var chparavalue =4.35;
layui.use(['form', 'layedit', 'laydate'], function () {
    var layerTips = layui.layer,
    layer = layui.layer, //获取当前窗口的layer对象
    form = layui.form,
    layedit = layui.layedit,
    laydate = layui.laydate;
  //基准利率
	$.ajax({
		type:"get",
		url:"/param/getParam/"+chparakey,
		dataType:"json",
		contentType:"application/json",
		success:function(data){
			if(data.data!=null){
				chparavalue = data.data.chparavalue;
			}else{
				layer.msg(data.message);
			}
		},
		complete:function(){
			setTimeout(function(){
				  layer.closeAll('loading');
				}, 300);
	  	}
	});
	$("#passID").on('click',function(){
	//检测信息输入完整性
	var applyAmount = $("#sqsx").val();
	var applyTerm = $("#sqqx").val();
	if(applyAmount == "" && applyAmount == ""){
		layer.msg('请输入完整信息', {icon: 6});
		return;
	}else if(applyTerm == "" || applyTerm == ""){
		layer.msg('请输入完整信息', {icon: 6});
		return;
	};
	//检验申请金额的格式
	if(Math.ceil(applyAmount) > 500 ){
		layer.msg("贷款金额不能超过500万", {icon: 6});
    	  return;
  	}
	if(Math.ceil(applyAmount) == 0 ){
		layer.msg("贷款金额不能为0", {icon: 6});
    	  return;
  	}
	 if (!moneyRegular.test(applyAmount)){
		 layer.msg("金额输入格式有误!", {icon: 6});
		 return;
	 }
	 if (Math.ceil(applyTerm) == 0){
		 layer.msg("申请期限不能为0", {icon: 6});
		 return;
	 }
	 if (applyTerm.substring(0,applyTerm.length-(applyTerm.length-1)) == 0){
		 layer.msg("申请期限格式有误!", {icon: 6});
		 return;
	 }
	var data0 = {
		"custId":custId,"idNo":idNo,"applyAmount":applyAmount,"applyTerm":applyTerm
	};
	layer.closeAll('loading'); //关闭加载层
	$.ajax({
		url : "/edjs/callrule_unit",
        type : "post",
        dataType : "json",
        contentType : "application/json",
        data:JSON.stringify(data0),
        success:function(data){
         	if(data.code=='200'){
         		approvalVo=data.data;
          		if(data.data.isPass){
         			passOrFail = true;
                    $("#ifPass").show();
                    $("#ifFail").hide();
                    loseDate = data.data.loseDate;
                    $("#listbody1").html(pageLoadPass(data.data));
                    if(data.data.zrDesc != '' && data.data.zrDesc != null && data.data.zrDesc != "null"){$("#statusTips").text("温馨提醒:("+data.data.zrDesc+")")}
         		}else{
         			$("#ifPass").hide();
         			$("#ifFail").show();
         			$("#listbody2").html(pageLoadFail(data.data));
         		}
        	}else{
        		layer.msg(data.message);
        	}
        },
  	  complete:function(){
  	  	layer.closeAll('loading'); //关闭加载层
  	  }
	})
	})
})

    var creditRateFloat = "";//信用利率浮动比例
    var creditOneYearLoanOfDayRate = "";
    var passOrFail = false;//状态
    
    function pageLoadPass(result) {
        $("#custName").html(custName);//客户姓名
        $("#applyAmountShow").html(((result.applyAmount == null) ? "" : (parseFloat(result.applyAmount))));
        $("#applyTermShow").html(((result.applyTerm == null) ? "" : (result.applyTerm)));
        $("#pjJb").html(parseInt(result.pjJb));
        $("#creditAmount").html((result.creditAmount == null ? "" : parseFloat(result.creditAmount).toFixed(2)));
        /* 一年期月利率 */
        $("#creditMonthRate").text((parseFloat(result.creditRate)*30/10).toFixed(2));
        $("#creditRate").text("万分之" + doubleConvertToChineseUppers(parseFloat(result.creditRate)));    
        $("#crtDate").html(result.crtDate);
        $("#loseDate").html(result.loseDate);
        
        $("#pledgeAmount").text(result.pledgeAmount);
        $("#pledgeMonthRate").text((parseFloat(result.pledgeRate)*30/10).toFixed(2));
        $("#pledgeRate").text("万分之" + doubleConvertToChineseUppers(parseFloat(result.pledgeRate)));
    }
    function pageLoadFail(result) {
        var html = "";
        html += '<tr>';
        html += '<td>' + custName + '</td>';
        html += '<td>' + ((result.applyAmount == null) ? "" : (parseInt(result.applyAmount))) + '</td>';
        html += '<td>' + ((result.applyTerm == null) ? "" : (result.applyTerm)) + '</td>';
        html += '<td>' + ((result.pjJb == null) ? "暂无" : (result.pjJb)) + '</td>';
        html += '<td>' + (result.notPassReason == null ?"" :result.notPassReason) + '</td>';
        html += '<td>' + (result.crtDate) + '</td>';
        html += '</tr>';
        return html;
    }
    function scspCommit() {
       /* var dataScsp = {
                "custId": custId,
                "custName":custName,
                "applyMoney": $("#applyAmountShow").text(),//申请金额
                "applyLimit": APPLY_LIMIT.text(),//申请期限
                "pjResult": GUARANTOR_GRADE.text(),//评级结果
//                "highLimit": HIGH_LIMIT.text(),//最高授信额度
                "creditLimit": CREDIT_LIMIT.text(),//信用额度
                "creditUprate": CREDIT_RATE_FLOAT,//信用利率浮动比例
                "creditMonInterest":$("#creditRate").text(),//信用月利率
//                "monInterest":createRate,//月利息
                "dayInterest":creditOneYearLoanOfDayRate,//日利息
                "loseDate":loseDate,//失效日期
                "custType":custType
         	}
        console.info(dataScsp);*/
        if (passOrFail == true) {
            $.ajax({
                url: "/edjs/commitScsp",
                type: "post",
                dataType: "json",
                contentType: "application/json",
                data: JSON.stringify(approvalVo),
                beforeSend:function(){
        		  	layer.load(2, {shade: [0.6, '#fff'], time: 1000})
        		},
                success: function (data) {
                	if(data.code=='200'){
                		layer.msg("发起授信成功!");
                		$("#addButton").attr("onclick",'');
                	}else{
                		layer.msg("发起授信失败!");
                	}
                }
            });
        } else {
        	layer.msg("测算状态为同意，才能发起授信流程!");
        }
    }
    function back(){
    	window.history.back(-1);
        setTimeout(function(){
        	window.location.reload();
        },200)
    }
