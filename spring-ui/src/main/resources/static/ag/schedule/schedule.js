var schedule = {
    baseUrl: "/lrdPlatform",
};

var WORK_DATE = $("#WORK_DATE"), //日期
	WORK_CONT = $("#WORK_CONT"), //工作事项
	USER_ID="1";                 //ID


function gDate(){
	WORK_DATE.val(typeof(wDate)=='string'?wDate:y+'/'+geTime(m)+'/'+geTime(d))
}
//日程
$("#addSchedule").on('click',function(){
	$("#myModal").modal();
	$("#btnsave").removeAttr("zt");	
})
//日志
$("#addLogCancel").on('click',function(){
	$("#addLog").popover("hide");
})

$("#addLogAdd").on('click',function(){
	$("#addLog").popover("hide");
})
//日历
$(function(){
/*显示今天日期*/
	$("#logoDate").html(
		y+'年'+m+'月'+d+'日'
	);
	$("#htmlDate").html(
		y+'年'+m+'月'+d+'日  星期'+"日一二三四五六".charAt(DD)
	);
	$(".htmlDate").html(
		y+'年'+m+'月'+d+'日'
	);
	$("#WORK_DATE").datepicker("update",y+'/'+geTime(m)+'/'+geTime(d));
	$("[name=addLogCancel]").on("click",function(){
		$("#addLog").popover("hide");
	});

	$("#calendar").datepicker({
		todayBtn:1,
		templates: {
	        leftArrow: "<span><i class='glyphicon glyphicon-chevron-left'></i></span>",
	        rightArrow: "<span><i class='glyphicon glyphicon-chevron-right'></i></span>"
	    }
	}).datepicker("update",newDate).on("changeDate",function (e){
		var date=e.dates[0];
		var y=date.getFullYear();
		var m=date.getMonth()+1;
		var d=date.getDate();
		var DD=date.getDay();
		var newDate=y+"年"+m+"月"+d+"日";
		wDate = y+'/'+geTime(m)+'/'+geTime(d);		
		$("#htmlDate").html(
			y+'年'+m+'月'+d+'日  星期'+"日一二三四五六".charAt(DD)
		);
		$(".htmlDate").html(
			y+'年'+m+'月'+d+'日'
		);
		$("#WORK_DATE").val(wDate);
		xList();
		numList(y+"/"+geTime(m));
	});

	$(".table-condensed").find(".today").on("click",function(){
		$("#calendar").datepicker("update",newDate);
	})
	$(".prev").on("click",function(){
		var arr=$(".datepicker-switch").html().split(" ");
		var mm="";
		switch (arr[0]){
	 	 case "一月":mm="12";
	   	 break;
	 	case "二月":mm="01";
	   	 break;
	 	case "三月":mm="02";
	   	 break;
	 	case "四月":mm="03";
	   	 break;
	 	case "五月":mm="04";
	   	 break;
	 	case "六月":mm="05";
	   	 break;
	 	case "七月":mm="06";
	   	 break;
	 	case "八月":mm="07";
	   	 break;
	 	case "九月":mm="08";
	   	 break;
	 	case "十月":mm="09";
	   	 break;
	 	case "十一月":mm="10";
	   	 break;
	 	case "十二月":mm="11";
	   	 break;
		}
		numList(arr[1]+"/"+mm);
	})
	$(".next").on("click",function(){
		var arr=$(".datepicker-switch").html().split(" ");
		var mm="";
		switch (arr[0]){
	 	 case "一月":mm="02";
	   	 break;
	 	case "二月":mm="03";
	   	 break;
	 	case "三月":mm="04";
	   	 break;
	 	case "四月":mm="05";
	   	 break;
	 	case "五月":mm="06";
	   	 break;
	 	case "六月":mm="07";
	   	 break;
	 	case "七月":mm="08";
	   	 break;
	 	case "八月":mm="09";
	   	 break;
	 	case "九月":mm="10";
	   	 break;
	 	case "十月":mm="11";
	   	 break;
	 	case "十一月":mm="12";
	   	 break;
	 	case "十二月":mm="01";
	   	 break;
		}
		numList(arr[1]+"/"+mm);
	})
})


layui.use(['form', 'layedit', 'laydate'], function () {
	xList();
    txList();
    $(function(){numList(y+"/"+geTime(m));})
    var editIndex;
    var layer = layui.layer, //获取当前窗口的layer对象
        form = layui.form,
        layedit = layui.layedit,
        laydate = layui.laydate;
    
    var savelog=$("#savelog");//保存
    savelog.on("click",function(){//保存工作日志
    	gDate();
    	var DAILY=$("#logCont textarea");	
    	if(DAILY.val().length>1000){
    		layer.msg('工作日志字数过多');
    		return  false;
    	}
    	var data1={
    			"workDate":WORK_DATE.val(),
    			"daily":DAILY.val()
    	        };
    	$.ajax({
    		type:"post",
    		url:schedule.baseUrl+"/dailylog",
    		dataType:"json",
    		async: false,
    		contentType:"application/json",
    		data:JSON.stringify(data1),
    		success:function(data){
    			if(data.code=="200"){
    				layer.msg("工作日志保存成功");
    			}else{
    				layer.msg(data.message);
    			}
    		}
    	}) 
    })
    Io.on("#resetlog","click",function(){//清除
    	gDate();
    	var data1={
    			"workDate":WORK_DATE.val()
    	    };
    	$.ajax({
    		type:"delete",
    		url:schedule.baseUrl+"/dailylog",
    		dataType:"json",
    		async: false,
    		contentType:"application/json",
    		data:JSON.stringify(data1),
    		success:function(data){
    			if(data.code=="200"){
    				$("#DAILY").val("");
    			}else{
    				layer.msg(data.message);
    			}
    		}
    	}) 
    });
    //添加工作日程
    $("#btnsave").on('click',function(){
    	var wprkCont = $('#WORK_CONT').val();
    	if(wprkCont==''){
    		layer.msg('工作事项不能为空');
    		return false;
    	}
    	if(wprkCont.length>200){
    		layer.msg('工作事项字数过多');
    		return false;
    	}
    	if(WORK_DATE.val()=="")	gDate();
    	var data1={
    			"workCont":WORK_CONT.val(),
    			"finishFlag":$("[name=state]:checked").val(),
    			"workDate":WORK_DATE.val()
    	};
    	if($(this).attr("zt")){
    		var zt = $(this).attr("zt");
    		data1.scheduleId= zt;
    	}
    	var type="post";
    	if(data1.scheduleId){
    		type="put";
    	}
    	$.ajax({
    		type:type,
    		url:schedule.baseUrl+'/schedule',
    		dataType:"json",
    		contentType:"application/json",
    		data:JSON.stringify(data1),
    		success:function(data){
    			if(data.code=="200"){				
    				xList();
    			}else{
    				layer.msg(data.message);
    			}
    		}
    	});
    	$("#myModal").modal("hide").on("hidden.bs.modal",function(){
    		$(this).find("input[type=text]").val("");
    	});
    })
    Io.on("[name=btnEdit]","click",function(){
    	$("#myModal").modal();
    	var zt=$(this).attr("zt");
    	var flag=$(this).attr("flag");
    	var workCont=$(this).attr("workCont");
    	var workDate=$(this).attr("workDate");
    	WORK_DATE.val(workDate);
    	WORK_CONT.val(workCont);
    	$("[name=state]").each(function(){
    		var $this=$(this);
    		if($this.val()==flag){
    			$this.prop("checked",true);
    		}
    	});	 
    	$("#btnsave").attr("zt",zt); 
    })

    Io.on("[name=btnDel]","click",function(){//删除
    	var scheduleId=$(this).attr("zt");
    	Io.bs.alert("确定删除吗？", function(){
    		$.ajax({
    			type:"delete",
    			url:schedule.baseUrl+"/schedule/"+scheduleId,
    			dataType:"json",
    			contentType:"application/json",
    			success:function(data){
    				if(data.code=="200"){
    						$(this).removeData("bs.modal");
    					xList();
    				}else{
    					layer.msg(data.message);
    				}
    			}
    		}) 
    	})
    })
})
function xList(){
	gDate();
	var workDate = WORK_DATE.val();
	var data1={"workDate":workDate};
	$.ajax({
	type:"get",
	url:schedule.baseUrl+"/schedule",
	dataType:"json",
	contentType:"application/json",
	data:data1,
	success:function(data){
		if(data.code=="200"){
			if(data.data!=null && data.data.length){
				var resultArr=data.data;
				var html='';
				for(var i=0;i<resultArr.length;i++){
					var result = resultArr[i];
					var _FLAG = '';
					switch(result.finishFlag){
						case '0':_FLAG = '未完成';
						break;
						case '1':_FLAG = '已完成';
						break;
						default:_FLAG = '未知';													
					}
					html+='<tr>';
					html+='<td>'+ result.workCont +'</td>';
					html+='<td>'+ _FLAG +'</td>';
					html+='<td style="text-align:right;">';
					html+='<button class="btn btn-xs btn-primary" type="button" name="btnEdit" zt='+ result.scheduleId +' flag='+result.finishFlag+' workCont='+ result.workCont +' workDate='+ result.workDate +'><i class="glyphicon glyphicon-pencil"></i></button> ';
					html+='<button class="btn btn-xs btn-danger" type="button" name="btnDel" zt='+ result.scheduleId +'><i class="glyphicon glyphicon-trash"></i></button>';
					html+='</td>';
					html+='</tr>';
				}
				$("#tBody").html(html);
			}else{
				$("#tBody").html('');
			}
		}else{
			alert("查询用户日程计划失败");
		}
	}
	});
	$.ajax({
		type:"get",
		url:schedule.baseUrl+"/dailylog",
		dataType:"json",
		contentType:"application/json",
		data:data1,
		success:function(data){
			if(data.code=="200"){
				if(data.data!=null && data.data.length){
					var result=data.data[0];
					$("#DAILY").val(result.daily);
				}else{
					$("#DAILY").val("");
				}
			}else{
				layer.msg(data.message);
			}
		}
	})
}

//客户提醒
function txList(){
	$.ajax({
		type:"get",
		url:schedule.baseUrl,
		dataType:"json",
		contentType:"application/json",
		success:function(data){
			if(data.code=="200"){
				if(data.data!=null){
					var resultArr=data.data;
					var html='';
					for(var i=0;i<resultArr.length;i++){
						var result = resultArr[i];
						if(typeof(result.warnDesc)=='undefined' || result.warnDesc==null){
							result.warnDesc='';
						}
						html+='<tr>';
						html+='<td>'+ result.custName+'</td>';
						html+='<td>'+ result.telNo +'</td>';
			        	if(result.dateYm.length ==8){
			                var year = result.dateYm.slice(0,4);
			                var month = result.dateYm.slice(4,6);
			                var date = result.dateYm.slice(6,8);
			                result.dateYm = year+'-'+month+'-'+date;
			            }
						html+='<td>'+ result.dateYm+'</td>';
						html+='<td>'+ result.warnDesc+'</td>';
						html+='</tr>';
					}
					$("#tixibody").html(html);
				}
			}else{
				layer.msg(data.message);	
			}
		}
	});
}

function numList(d){
	var data1 = {"workDate":d};
	$.ajax({
		type:"get",
		url:schedule.baseUrl+"/schedule/schedule",
		dataType:"json",
		contentType:"application/json",
		data:data1,
		success:function(data){
			if(data.code=="200"){
				if(data.data!=null && data.data.length>0){
					var resultArr=data.data;
					for(var i=0;i<resultArr.length;i++){
						$("td.day").each(function(){
							if($(this).attr("class")=="day"||$(this).attr("class")=="day active"){
								var htmlTime =parseInt($(this).html());
								var dbTime = parseInt(resultArr[i].workDate.substring(8,10));
								if(htmlTime==dbTime){
									$(this).html( $(this).html() + "<span><i>.</i>"+resultArr[i].num+"</span>" );
								}
							}
						});
					}
				}
			}
		}
	});
}
