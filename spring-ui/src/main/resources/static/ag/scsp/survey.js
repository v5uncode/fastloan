var survey = {
    baseUrl: "/survey",
    scspUrl: "/scsp",
    entity: "survey",
    tableId: "ccUsersTable",
    toolbarId: "toolbar",
    unique: "id",
    order: "asc",
    currentItem: [],
    scspId: "",
    custId: "",
    warrant: "",
    stat: ''
};

survey.setOtherInfo = function () {
	$("#purpose1").val($("#purpose").text());//贷款用途
    if ($("#marriageFlag").text() == "已婚") {//是否已婚
        $("#FOR_SPOUSE").text("配偶工作单位");
    }
    var creditRate = $("#creditRate").html();
    var creditMonRate = ((creditRate*30)/10).toFixed(2);
    $("#creditMonRate").html(creditMonRate);
    var pledgeRate = $("#pledgeRate").html();
    var pledgeMonRate = ((pledgeRate*30)/10).toFixed(2);
    $("#pledgeMonRate").html(pledgeMonRate);

    var ensureRate = $("#ensureRate").html();
    var ensureMonRate = ((ensureRate*30)/10).toFixed(2);
    $("#ensureMonRate").html(ensureMonRate);
    //家庭成员列表
    var data = $.parseJSON($('#families').text());
    var htmlElement = '';
    for (var x = 0; x < data.length; x++) {
        var indexResult = data[x];
        //配偶的工作单位
        if (indexResult.relaType == '配偶') {
            $("#SPOUSE_VACATION").text(indexResult.workAddress);
        }
        htmlElement += "<tr>";
        htmlElement += "    <td style='height:10px; border-top: 1px solid black; text-align: center; vertical-align: middle;'>" + (indexResult.vsName) + "</td>";
        htmlElement += "    <td style='height:10px; border-left: 1px solid black; border-top: 1px solid black; text-align: center; vertical-align: middle;'>" + (indexResult.relaType) + "</td>";
        htmlElement += "    <td style='height:10px; border-left: 1px solid black; border-top: 1px solid black; text-align: center; vertical-align: middle;'>" + (indexResult.idType) + "</td>";
        htmlElement += "    <td style='height:10px; border-left: 1px solid black; border-top: 1px solid black; text-align: center; vertical-align: middle;'>" + (indexResult.idNo) + "</td>";
        htmlElement += "</tr>";
    }
    $('#families').html(htmlElement);
    //经营行业列表

    //负面信息
    var badInfo = $("#badInfo").text();
    var badOtherInfo = $("#badOtherInfo").text();
    if (badInfo == null || badInfo == "") {
        if (badOtherInfo == null || badOtherInfo == "") {
            $("#noBadBehavior").css("display", "block"); //该用户没有负面信息.
        } else {
            $("#badBehaviorElement15").css("display", "inline");
            $("#textareaCustOtherInfo").css("display", "inline");
        }

    } else {
        var badBehaviorArray = badInfo.split(",");
        for (var i = 0; i < badBehaviorArray.length; i++) {
            $("#badBehaviorElement" + badBehaviorArray[i]).text((i + 1) + ". " + $("#badBehaviorElement" + badBehaviorArray[i]).text());
            $("#badBehaviorElement" + badBehaviorArray[i]).css("display", "block");
        }
        if (badOtherInfo != null && badOtherInfo != "") {
            $("#badBehaviorElement15").css("display", "inline");
            $("#badOtherInfo").css("display", "inline");
        }
    }
    //经营信息
    addManagement();
    //担保还是抵制押
    judge();

}

//添加经营信息
function addManagement() {
    var zyxm = $('#zyxmShow').text();//主营项目
    var warrant = $('#warrantType').text();
    var management = "";

    var preHtml = " <td style='height:10px; border-left: 1px solid black; border-top: 1px solid black; text-align: center; vertical-align: middle;'>";
    var endHtml = " </td>";
    htmlElement = '';
    if (zyxm == '05') {//职工

    } else if (zyxm == '01' || zyxm == '02') {//种植情况plants
        var plant = new Array();
        if ($('#plants').text().trim() != '' && $('#plants').text() != null) {
            plant = $.parseJSON($('#plants').text());
        }
        for (var x = 0; x < plant.length; x++) {
            var indexResult = plant[x];
            htmlElement += "<tr>";
            htmlElement += preHtml + (indexResult.zwType) + endHtml;
            htmlElement += preHtml + (indexResult.zzModel) + endHtml;
            htmlElement += preHtml + indexResult.zzMs + endHtml;
            htmlElement += preHtml + (indexResult.bizYears) + endHtml;
            htmlElement += preHtml + (indexResult.incomeY == null ? "" : indexResult.incomeY ) + endHtml;
            htmlElement += "</tr>";
        }
        $('#plants').html(htmlElement);
    } else if (zyxm == '03' || zyxm == '04') {//养殖情况
        var breed = new Array();
        if ($('#breeds').text().trim() != '' && $('#breeds').text() != null) {
            breed = $.parseJSON($('#breeds').text());
        }
        for (var x = 0; x < breed.length; x++) {
            var indexResult = breed[x];
            htmlElement += "<tr>";
            htmlElement += preHtml + (indexResult.yzKind) + endHtml;
            htmlElement += preHtml + (indexResult.yzCnt) + endHtml;
            htmlElement += preHtml + (new Date().getFullYear() - indexResult.bizDate) + endHtml;
            htmlElement += preHtml + (indexResult.incomeY == null ? "" : indexResult.incomeY) + endHtml;
            htmlElement += preHtml + (indexResult.bizFixedasset == null ? "" : indexResult.bizFixedasset ) + endHtml;
            htmlElement += "</tr>";
        }
        $('#breeds').html(htmlElement);
    } else {//经营

        var busi = new Array();
        if ($('#businesses').text().trim() != '' && $('#businesses').text() != null) {
            busi = $.parseJSON($('#businesses').text());
        }

        for (var x = 0; x < busi.length; x++) {
            var indexResult = busi[x];
            htmlElement += "<tr>";
            htmlElement += preHtml + (indexResult.bizHy) + endHtml;
            htmlElement += preHtml + (new Date().getFullYear() - indexResult.bizDate) + endHtml;
            htmlElement += preHtml + (indexResult.ownArea) + endHtml;
            htmlElement += preHtml + (indexResult.rentArea) + endHtml;
            htmlElement += preHtml + (indexResult.incomeY == null ? "" : indexResult.incomeY) + endHtml;
            htmlElement += "</tr>";
        }

        $('#businesses').html(htmlElement);
    }
}

//判断是抵制押还是担保人，然后添加担保人信息或者抵制押信息
function judge() {
    //主担保方式
    var warrant = $('#warrantType').text();
//    console.log(warrant);
    $("#dbrTr").hide();
    $("#dzyTr").hide();
    if(warrant.indexOf('信用') != -1){
    	
    }
    if(warrant.indexOf('保证') != -1){
    	$("#dbrTr").show();
    	getGuarantorsInfo($("#scspId").text());
    }
    if(warrant.indexOf('抵押') != -1 || warrant.indexOf('质押') != -1){
    	$("#dzyTr").show();
    	getMortgageInfo($("#scspId").text());
    }
}

layui.use(['layedit','element'], function () {
    survey.scspId = getUrlParam("scspId")||"";
    survey.stat = getUrlParam("stat")||"";
    if(survey.stat=='0'||survey.stat=='3'){  	
    	 $("a").hide();
       	 $("[name='isDeleteButton']").hide();
       	 $("[contenteditable='true']").attr("contenteditable","false");
       	$('.print_save').hide();
    }   
    yunSetVal.init(survey.baseUrl + '/' + survey.scspId + '/report', survey.setOtherInfo);
});

//添加担保人
/*function addOneGuarantor() {
    $('#baoZhengRenPanel').modal();
    $("#baoZhengRenText").css("borderBottom", "solid 3px #286090").val("");
    $("#btnTiJiaoShuJu").attr("disabled", "disabled").css("border", "solid 3px #3c3c3c");
}*/

//保证人身份证号输入框
/*$(document).off("click", "#baoZhengRenText").on("keyup", function () {
    $("#btnTiJiaoShuJu").attr("disabled", "disabled").css("border", "solid 3px #3c3c3c");

    if (IdentityCodeValid($("#baoZhengRenText").val())) {//身份证校验通过.
        $("#baoZhengRenText").css("borderBottom", "solid 3px green");
        $("#btnTiJiaoShuJu").attr("disabled", false)
        getGuarantorBaseInfo($("#baoZhengRenText").val());
    } else {
        $("#baoZhengRenText").css("borderBottom", "solid 3px red");
    }
});*/

//抵质押物信息
function getMortgageInfo() {
    var sendData = {}; //被保证人SCSP表中的ID.
    sendData.scspId = $('#scspId').text();
    $.ajax({
        beforeSend: function () {
            $("#loadgif").fadeIn(100);
            //$("#baoZhengRenPanel").css("zIndex", "99")
        },
        type: "get",
        url: survey.baseUrl + '/getMortgage?scspId=' + sendData.scspId, //请求后台的URL（*）
        dataType: "json",
        async: false,
        contentType: "application/json",
        success: function (data) {
            $("#loadgif").fadeOut(100);
            if (data.code == "200") {
                if (data.data == '') {
                    var s = '<tr style="border-top: 1px solid black;"><td colspan="5">无抵质押物信息</td></tr>';
                    $("#dzyBody").html(s);
                    return
                }
                ;
                var preHtml = " <td style='height:10px; border-left: 1px solid black; border-top: 1px solid black; text-align: center; vertical-align: middle;'>";
                var endHtml = " </td>";
                var mortgages = data.data;
                var htmllist = "";
                for (var i = 0; i < mortgages.length; i++) {
                    var m = mortgages[i];
                    htmllist += '<tr>';
                    htmllist += preHtml + m.dylx + endHtml;
                    htmllist += preHtml + m.dywqsrxx + endHtml;
                    htmllist += preHtml + m.pgjz + endHtml;
                    htmllist += preHtml + m.dbzzq + endHtml;
                    htmllist += preHtml + m.dyzt + endHtml;
                    htmllist += '</tr>';
                }
                $("#dzyBody").html(htmllist);
            } else {
                Io.bs.alert("查询出错请重新尝试。.");
            }
        },
        error: function () {
            $("#loadgif").fadeOut(100);
            Io.bs.alert("保证人基本信息获取失败!");
            return;
        }
    });

}

//通过idCard查找客户基本信息.
//idCard: 保证人身份证号
function getGuarantorBaseInfo(idCard) {

    var sendData = {}; //被保证人SCSP表中的ID.
    sendData.idCard = idCard;

    $.ajax({
        beforeSend: function () {
            $("#loadgif").fadeIn(100);
            //$("#baoZhengRenPanel").css("zIndex", "99")
        },
        type: "get",
        url: survey.baseUrl + '/getFamilies?idCard=' + idCard, //请求后台的URL（*）
        dataType: "json",
        async: false,
        contentType: "application/json",
        success: function (data) {
            $("#loadgif").fadeOut(100);
            if (data.code == "200") {
                if (data.data.flag == false) {
                    Io.bs.alert("请先录入该担保人信息！");
                    return
                }
                ;
                var FAMLIST = data.data;
                var htmllist = "";
                for (var i = 0; i < FAMLIST.length; i++) {
                    var FAM = FAMLIST[i];
                    htmllist += '<tr>';
                    htmllist += '    <td name="familyMemberName">' + FAM.vsName + '</td>';
                    htmllist += '    <td name="familyMemberRelaType">' + FAM.sex + '</td>';
                    htmllist += '    <td name="familyMemberRelaType">' + FAM.relaType + '</td>';
                    htmllist += '    <td name="familyMemberIdType">' + FAM.idType + '</td>';
                    htmllist += '    <td name="familyMemberIdNo">' + FAM.idNo + '</td>';
                    htmllist += '</tr>';
                }
                $("#listbody3").html(htmllist);
            } else {
                Io.bs.alert("查询出错请重新尝试。.");
            }
        },
        error: function () {
            $("#loadgif").fadeOut(100);
            Io.bs.alert("保证人基本信息获取失败!");
            return;
        }
    });
}

//提交数据按钮
Io.on("#btnTiJiaoShuJu", "click", function () {
    //被保证人身份证号
    var insuredIdCard = $("#idNo").text();
    //保证人身份证号
    var idNo = $("#baoZhengRenText").val();
    //若输入为空，则不向后执行。
    if (idNo == "" || idNo == null) {
        layer. msg('您填写身份证号有误!', {icon: 7});
        return;
    }

    //检查是否添加自己的身份证号
    if (insuredIdCard == idNo) {
        layer.alert('你不能把自己作为担保人!', {icon: 7});
        return;
    }
    //检查重复保证人身份证号
    $("[name='dbrIdCard']").each(function () {   	
        if (idNo == $(this).text()) {
            layer.alert('该客户已存在此担保人信息!', {icon: 7});
            return false;
        }
    });
    saveGuarantor(idNo, insuredIdCard);
});

/*//添加保证人到数据库
function saveGuarantor(idNo, insuredIdCard) {
    //发送保存数据到controller
    var sendData = {
        "idCard": idNo, //保证人证件号码.
        "insuredIdCard": insuredIdCard, //被保证人的身份证号.
        "scspId": $("#scspId").text()
        //被保证人SCSP表中的ID.
    };

    $.ajax({
        beforeSend: function () {
            $("#loadgif").fadeIn(200);
        },
        type: "post",
        url: survey.baseUrl + "/addGuarantor",
        dataType: "json",
        contentType: "application/json",
        data: JSON.stringify(sendData),
        success: function (data) {
            $("#loadgif").fadeOut(100);
            if (data.code == "200") {
                getGuarantorsInfo($("#scspId").text());
                layer.msg('保证人保存成功', {icon: 6});
                closeModal();
            } else {
                layer.alert("保证人保存失败，" + data.message, {icon: 5});
            }
        },
        error: function (data) {
            $("#loadgif").fadeOut(100);
            alert("错误");

        }
    });
}*/

//拼接担保人信息
function getGuarantorsInfo(scspId) {
    var pageReadOnly = false;
    var dbrBody = $("#dbrBody");
    dbrBody.html("");
    $.ajax({
        type: "get",
        url: survey.scspUrl + "/guarantor/"+scspId,
        dataType: "json",
        contentType: "application/json",
        success: function (data1) {
            if (data1.code == "200") {
                var data = data1.data;
                for (var c = 0; c < data.length; c++) {
                    var htmlElement = '';
                    htmlElement += '<table border="1" class="dbrBody" style="text-align:center; width:99.5%; margin:3px;" cellspacing="0" cellpadding="0">';
                    htmlElement += '    <tr>';
                    htmlElement += '        <td height="10" style="font-size:8px;font-weight: bold;">担保人姓名</td>';
                    htmlElement += '        <td height="10" style="font-size:8px;">' + data[c].name + '</td>';
                    htmlElement += '        <td height="10" style="font-size:8px;font-weight: bold;">证件类型</td>';
                    htmlElement += '        <td height="10" style="font-size:8px;">身份证</td>';
                    htmlElement += '        <td height="10" style="font-size:8px;font-weight: bold;">证件号码</td>';
                    htmlElement += '        <td height="10" style="font-size:8px;" name="dbrIdCard">' + data[c].idCard + '</td>';
                    if (pageReadOnly == "true" || pageReadOnly == true) {
                        htmlElement += '    <td rowspan="4" style="width: 1px;"></td>';
                    } else {
                    	htmlElement += '    <td rowspan="4" style="width: 1px;"></td>';
//                        htmlElement += '    <td width="25"  height="10" rowspan="4" style="cursor: pointer; background-color: #E0E0E0;" name="isDeleteButton" onclick="removeGuarantor(\'' + data[c].id + '\')">删除</td>';
                    }
                    htmlElement += '    </tr>';

                    htmlElement += '    <tr>';
                    htmlElement += '        <td height="10" style="font-size:8px;font-weight: bold;">主营项目</td>';

                    switch (data[c].jyxm) {
                        case "01":
                            htmlElement += '        <td height="10" style="font-size:8px;">种植蔬菜</td>';
                            htmlElement += '    <td height="10" style="font-size:8px;font-weight: bold;">作物种类</td>';
                            htmlElement += '    <td height="10" style="font-size:8px;">' + data[c].workName + '</td>';
                            htmlElement += '    <td height="10" style="font-size:8px;font-weight: bold;">经营年限</td>';
                            break;
                        case "02":
                            htmlElement += '        <td height="10" style="font-size:8px;">种植其他</td>';
                            htmlElement += '    <td height="10" style="font-size:8px;font-weight: bold;">作物种类</td>';
                            htmlElement += '    <td height="10" style="font-size:8px;">' + data[c].workName + '</td>';
                            htmlElement += '    <td height="10" style="font-size:8px;font-weight: bold;">经营年限</td>';
                            break;
                        case "03":
                            htmlElement += '        <td height="10" style="font-size:8px;">养殖猪</td>';
                            htmlElement += '    <td height="10" style="font-size:8px;font-weight: bold;">养殖种类</td>';
                            htmlElement += '    <td height="10" style="font-size:8px;">' + data[c].workName + '</td>';
                            htmlElement += '    <td height="10" style="font-size:8px;font-weight: bold;">经营年限</td>';
                            break;
                        case "04":
                            htmlElement += '        <td height="10" style="font-size:8px;">养殖其他</td>';
                            htmlElement += '    <td height="10" style="font-size:8px;font-weight: bold;">养殖种类</td>';
                            htmlElement += '    <td height="10" style="font-size:8px;">' + data[c].workName + '</td>';
                            htmlElement += '    <td height="10" style="font-size:8px;font-weight: bold;">经营年限</td>';
                            break;
                        case "05":
                            htmlElement += '        <td height="10" style="font-size:8px;">职工</td>';
                            htmlElement += '    <td height="10" style="font-size:8px;font-weight: bold;">工作单位名称</td>';
                            htmlElement += '    <td height="10" style="font-size:8px;">' + data[c].workName + '</td>';
                            htmlElement += '    <td height="10" style="font-size:8px;font-weight: bold;">工作开始年份</td>';
                            break;
                        case "06":
                            htmlElement += '        <td height="10" style="font-size:8px;">制造业</td>';
                            htmlElement += '    <td height="10" style="font-size:8px;font-weight: bold;">经营项目</td>';
                            htmlElement += '    <td height="10" style="font-size:8px;">' + data[c].workName + '</td>';
                            htmlElement += '    <td height="10" style="font-size:8px;font-weight: bold;">经营年限</td>';
                            break;
                        case "07":
                            htmlElement += '        <td height="10" style="font-size:8px;">运输业</td>';
                            htmlElement += '    <td height="10" style="font-size:8px;font-weight: bold;">经营项目</td>';
                            htmlElement += '    <td height="10" style="font-size:8px;">' + data[c].workName + '</td>';
                            htmlElement += '    <td height="10" style="font-size:8px;font-weight: bold;">经营年限</td>';
                            break;
                        case "08":
                            htmlElement += '        <td height="10" style="font-size:8px;">批发零售业</td>';
                            htmlElement += '    <td height="10" style="font-size:8px;font-weight: bold;">经营项目</td>';
                            htmlElement += '    <td height="10" style="font-size:8px;">' + data[c].workName + '</td>';
                            htmlElement += '    <td height="10" style="font-size:8px;font-weight: bold;">经营年限</td>';
                            break;
                        case "09":
                            htmlElement += '        <td height="10" style="font-size:8px;">餐饮及住宿业</td>';
                            htmlElement += '    <td height="10" style="font-size:8px;font-weight: bold;">经营项目</td>';
                            htmlElement += '    <td height="10" style="font-size:8px;">' + data[c].workName + '</td>';
                            htmlElement += '    <td height="10" style="font-size:8px;font-weight: bold;">经营年限</td>';
                            break;
                        case "10":
                            htmlElement += '        <td height="10" style="font-size:8px;">其他行业</td>';
                            htmlElement += '    <td height="10" style="font-size:8px;font-weight: bold;">经营项目</td>';
                            htmlElement += '    <td height="10" style="font-size:8px;">' + data[c].workName + '</td>';
                            htmlElement += '    <td height="10" style="font-size:8px;font-weight: bold;">经营年限</td>';
                            break;
                    }

                    htmlElement += '        <td height="10" style="font-size:8px;">' + data[c].workYears + '</td>';
                    htmlElement += '    </tr>';
                    htmlElement += '    <tr>';
                    htmlElement += '        <td height="10" style="font-size:8px;font-weight: bold;">家庭总资产</td>';
                    htmlElement += '        <td height="10" style="font-size:8px;">' + parseInt(data[c].zzc).toFixed(2) + '</td>';
                    htmlElement += '        <td height="10" style="font-size:8px;font-weight: bold;">家庭总负债</td>';
                    htmlElement += '        <td height="10" style="font-size:8px;">' + parseInt(data[c].zfz).toFixed(2) + '</td>';
                    htmlElement += '        <td height="10" style="font-size:8px;font-weight: bold;">担保人评级结果</td>';
                    htmlElement += '        <td height="10" style="font-size:8px;">' + data[c].pjJb + '</td>';
                    htmlElement += '    </tr>';
                    htmlElement += '    <tr>';
                    htmlElement += '        <td height="10" style="font-size:8px;font-weight: bold;">近三年家庭年均收入</td>';
                    htmlElement += '        <td height="10" style="font-size:8px;">' + parseInt(data[c].annualIncome3y / 3).toFixed(2) + '</td>';
                    htmlElement += '        <td height="10" style="font-size:8px;font-weight: bold;">近三年家庭年均支出</td>';
                    htmlElement += '        <td height="10" style="font-size:8px;">' + parseInt(data[c].annualExpend3y / 3).toFixed(2) + '</td>';
                    htmlElement += '        <td height="10" style="font-size:8px;" colspan="2"></td>';
                    htmlElement += '    </tr>';
                    htmlElement += '</table>';

                    dbrBody.append(htmlElement);
                }//for end

            } else {
                layer.alert("保证人查询错误，" + data1.message, {icon: 5});
            }
        },//success end
        error: function (data) {
            alert("错误");
        }
    });//ajax end
}//getGuarantorsInfo method end

/**删除保证人*/
/*function removeGuarantor(id) {

    //确认是否真的删除保证人. 确定:删除,    取消:不删除
    if (!confirm("你确定要删除此保证人吗?")) {
        return;
    }

    $.ajax({
        type: "delete",
        url: survey.baseUrl + "/deleteGuarantor?id=" + id,
        dataType: "json",
        contentType: "application/json",
        success: function (data) {
            if (data.code == "200") {
                layer.alert("保证人删除成功，" + data.message, {icon: 6});
                getGuarantorsInfo(scspId);//通过审查审批ID查询数据库中的保证人.
            } else {
                layer.alert("保证人删除失败，" + data.message, {icon: 7});
            }
        }
    });
}*/

Io.on("#btnCommit", "click", function () {
    if ($("#purpose1").val() == "" || $("#purpose1").text() == null) {
        document.getElementById("purpose1").scrollIntoView(false); 
        layer.tips("贷款用途不能为空哦", $("#purpose1"), {tips: [1, '#78BA32']});
        return false;
    }

/*    if ($('#warrantType').text() == '02') {
        var haveGuarantor = $.trim($("#dbrBody").html());
        if (haveGuarantor == "") {
            layer.msg("保证人信息为空, 请检查.");
            return;
        }
    }*/

    if ($("#surveyHighLimit").text() == "" || $("#surveyHighLimit").text() == null) {
        document.getElementById("surveyHighLimit").scrollIntoView(true);
        layer.tips("授信额度不能为空哦", $("#surveyHighLimit"), {tips: [1, '#78BA32']});
        return false;
    }
    if ($("#rateFloat").text() == "" || $("#rateFloat").text() == null) {
        document.getElementById("rateFloat").scrollIntoView(true);
        layer.tips("利率浮动比例不能为空哦", $("#rateFloat"), {tips: [1, '#78BA32']});
        return false;
    }
    if ($("#surveyDateLimit").text() == "" || $("#surveyDateLimit").text() == null) {
        document.getElementById("surveyDateLimit").scrollIntoView(true);
        layer.tips("授信期限不能为空哦", $("#surveyDateLimit"), {tips: [1, '#78BA32']});
        return false;
    }
    var regex3 = /^\d*$/;
    if (!regex3.test($("#surveyDateLimit").text())) {
        $("#surveyDateLimit").text("")
        layer.msg("期限填写有误！");
        return;
    }

    var regex1 = /^([0-9]{1,8})(\.\d{1,2}){0,1}$/;
    if (!regex1.test($("#surveyHighLimit").text())) {
        $("#surveyHighLimit").text("");
        layer.msg("授信额度数值不正确！");
        return;
    } else if (parseFloat($("#surveyHighLimit").text()).toFixed(2) - parseFloat($("#highCredit").text()).toFixed(2) > 0) {
        $("#surveyHighLimit").text("");
        layer.msg("授信额度数值应小于等于最高授信额度！");
        return;
    } else {
        $("#surveyHighLimit").text(parseFloat($("#surveyHighLimit").text()).toFixed(2));
    }

    var sendSaveDataJson = {
        "scspId": $("#scspId").text(),
        "surveyHighLimit": $("#surveyHighLimit").text(),
        "rateFloat": $("#rateFloat").text(),
        "monRate": $("#monRate").text(),
        "dayRate": $("#dayRate").text(),
        "moneyForDay": $("#moneyForDay").text(),
        "surveyDateLimit": $("#surveyDateLimit").text(),
        "purpose": $("#purpose1").val(),
        "warrantType": $("#warrantType").text(),
    };
    $.ajax({
        url: survey.baseUrl + "/saveSurvey",
        type: "post",
        dataType: "json",
        contentType: "application/json",
        data: JSON.stringify(sendSaveDataJson),
        success: function (data) {
            if (data.code == "200") {
                layer.msg("保存成功！", {icon: 6,anim:2,offset: '300px',shade: 0});
            } else {
                layer.msg("保存失败，" + data.messege, {icon: 6});
            }
        }
    });
});
//利率浮动比例
Io.on("#rateFloat", "blur", function () {
    if ($.trim($("#rateFloat").text()) == "") {
        $("#dayRate").html("");
        $("#monRate").html("");
        $("#moneyForDay").html("");
        $("#rateFloat").text("");
        Io.bs.alert("利率浮动比例不能为空！");
        return;
    }

    var regex = /^(-)?([0-9]{1,3})(\.\d{1,2}){0,1}$/;
    if (regex.test($("#rateFloat").text())) {
        var baseRate = 4.35 / 100;
        $("#dayRate").html(doubleConvertToChineseUppers(((parseFloat($("#rateFloat").html()) / 100 + 1) * baseRate / 360 * 10000).toFixed(4)));
        $("#monRate").html(((parseFloat($("#rateFloat").html()) / 100 + 1) * baseRate / 12 * 1000).toFixed(4));
        $("#moneyForDay").html(((parseFloat($("#rateFloat").html()) / 100 + 1) * baseRate / 360 * 10000).toFixed(4));
        $("#rateFloat").text(parseFloat($("#rateFloat").text()));
    } else {
        $("#dayRate").html("");
        $("#monRate").html("");
        $("#moneyForDay").html("");
        $("#rateFloat").text("");
        Io.bs.alert("利率浮动比例填写有误, 请检查!");
        return;
    }
});


//关闭模态框
function closeModal() {
    $("#baoZhengRenPanel").modal('hide');
}

//打印
function myprint() {
    $("a").hide();
    $("[name='isDeleteButton']").hide();
    setTimeout(function () {
        $("a").show();
        $("[name='isDeleteButton']").show();
    }, 500)

    var winname = window.open('', 'newwin', '');
    var strHTML = document.getElementById('bigTable').innerHTML;
    winname.document.open('text/html', 'replace');
    winname.document.writeln(strHTML);
    winname.document.execCommand('print'); //打印
    winname.close();
}