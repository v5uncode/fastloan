var custStatistics = {
	    baseUrl: "/custStatistics/custStatistics",
	    entity: "custStatistics",
	};

layui.use(['form', 'layedit', 'laydate'], function () {
	var layer = layui.layer, //获取当前窗口的layer对象
    form = layui.form,
    layedit = layui.layedit,
    laydate = layui.laydate;
	
	require.config({
	    paths: {
	        echarts: 'plugins'
	    }
	});
	require(
	        [
	            'echarts',
	            "echarts/chart/bar",
	            "echarts/chart/line"
	        ],
	        function (ec) {
	            var myChart = ec.init(document.getElementById('chart'));
	            var strArr=new Array();
	            var dateArr=new Array();
	            $.ajax({
	            	url:custStatistics.baseUrl,
	            	type:"get",
	            	dataType:"json",
	            	contentType:"application/json",
	            	success:function(data){
	            		if(data.code=="200"){
	            			var result=data.data;
	            			for(var i=0;i<result.length;i++){
	            				strArr.push(result[i].NUM);
	            				dateArr.push(result[i].MON+"月");
	            			}
	            			var option = {
	                        		title : {
	                        	        text: '客户量统计表'
	                        	    },
	                        	    tooltip : {
	                        	        trigger: 'axis'
	                        	    },
	                        	    legend: {
	                        	        data:['客户量']
	                        	    },
	                        	    toolbox: {
	                        	        show : true,
	                        	        feature : {
	                        	            //mark : {show: true},
	                        	            //dataView : {show: true, readOnly: false},
	                        	            magicType : {show: true, type: ['line', 'bar']},
	                        	            restore : {show: true},
	                        	            saveAsImage : {show: true}
	                        	        }
	                        	    },
	                        	    calculable : true,
	                        	    color:["#87CEFA"],
	                        	    xAxis : [
	                        	        {
	                        	            type : 'category',
	                        	            data : dateArr
	                        	        }
	                        	    ],
	                        	    yAxis : [
	                        	        {
	                        	            type : 'value'
	                        	        }
	                        	    ],
	                        	    series : [
	                        	        {
	                        	            name:'客户量',
	                        	            type:'bar',
	                        	            data:strArr,
	                        	            markPoint : {
	                        	                data : [
	                        	                    {type : 'max', name: '最大值'},
	                        	                    {type : 'min', name: '最小值'}
	                        	                ]
	                        	            },
	                        	            markLine : {
	                        	                data : [
	                        	                    {type : 'average', name: '平均值'}
	                        	                ]
	                        	            }
	                        	        }
	                        	    ]
	                        }
	                        myChart.setOption(option);
	            		}else{
	            			layerTips.msg(data.message);
	            		}
	            	}
	            	
	            })
	            
	        }
	    );
})
	
