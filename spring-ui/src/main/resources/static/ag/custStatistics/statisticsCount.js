//经理table
var statisticsCountOne = {
    baseUrl: "/custStatistics/cust/statistic",
    tableId: "jlTable",
    toolbarId: "jlToolbar",
};

statisticsCountOne.queryParams = function (params) {
    if (!params)
        return {
            beginDate: $('#jlBeginDate').val(),
            endDate: $('#jlEndDate').val()
        };
    var temp = { //这里的键的名字和控制器的变量名必须一直，这边改动，控制器也需要改成一样的
        limit: params.limit, //页面大小
        page: params.offset/params.limit+1, //页码
        beginDate: $('#jlBeginDate').val(),
        endDate: $('#jlEndDate').val()
    };
    return temp;
};

statisticsCountOne.responseHandler = function(response){
    if(response.code =='200'){
        return response.data;
    }
    return false;
};
statisticsCountOne.columns = function () {
    return [
        [{
            field: 'orgName',
            title: "机构名称",
            valign: "middle",
            align: "center",
            colspan: 1,
            rowspan: 2
        },{
            field: 'userName',
            title: "客户经理",
            valign: "middle",
            align: "center",
            colspan: 1,
            rowspan: 2
        },
            {
                field: 'custCount',
                title: "总户数",
                valign: "middle",
                align: "center",
                colspan: 1,
                rowspan: 2
            },
            {
                title: "快贷系统新录入客户",
                valign: "middle",
                align: "center",
                colspan: 2,
                rowspan: 1
            },
            {
                title: "客户类型",
                valign: "middle",
                align: "center",
                colspan: 7,
                rowspan: 1
            }
        ],
        [{
            field: 'create',
            title: '信息完整',
            valign: "middle",
            align: "center"
        },
            {
                field: 'notCreate',
                title: '信息不完整',
                valign: "middle",
                align: "center"
            },
            {
                field: 'ng',
                title: '农工',
                valign: "middle",
                align: "center"
            },
            {
                field: 'ybnh',
                title: '一般农户',
                valign: "middle",
                align: "center"
            }
        ]
    ]
};

statisticsCountOne.init = function () {
    statisticsCountOne.table = $('#' + statisticsCountOne.tableId).bootstrapTable({
        url: statisticsCountOne.baseUrl, //请求后台的URL（*）
        method: 'get', //请求方式（*）
        toolbar: '#' + statisticsCountOne.toolbarId, //工具按钮用哪个容器
        cache: false, //是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
        pagination: true, //是否显示分页（*）
        pageNumber: 1, //初始化加载第一页，默认第一页
        pageSize: 10, //每页的记录行数（*）
        pageList: [10, 25, 50, 100], //可供选择的每页的行数（*）
        showColumns: false, //是否显示所有的列
        showRefresh: true, //是否显示刷新按钮
        showToggle: true, //是否显示详细视图和列表视图的切换按钮
        singleSelect:true,
        striped:true,
        queryParams: statisticsCountOne.queryParams,//传递参数（*）
        responseHandler:statisticsCountOne.responseHandler,
        columns: statisticsCountOne.columns()
    });
};



//机构tbale
var statisticsCountTwo = {
	    baseUrl: "/CLIENT/custStatistics/cust",
	    tableId: "orgTable",
	    toolbarId: "orgToolbar",
};

statisticsCountTwo.queryParams = function (params) {
    if (!params)
        return {
    	beginDate: $('#orgBeginDate').val(),
    	endDate: $('#orgEndDate').val()
        };
    var temp = { //这里的键的名字和控制器的变量名必须一直，这边改动，控制器也需要改成一样的
        limit: params.limit, //页面大小
        page: params.offset/params.limit+1, //页码
    	beginDate: $('#orgBeginDate').val(),
    	endDate: $('#orgEndDate').val()
    };
    return temp;
};

statisticsCountTwo.responseHandler = function(response){
	if(response.code =='200'){
		return response.data;
	}
	return false;
};
statisticsCountTwo.columns = function () {
    return [
		[{
				field: 'orgName',
				title: "机构名称",
				valign: "middle",
				align: "center",
				colspan: 1,
				rowspan: 2
			},
			{
				field: 'custCount',
				title: "总户数",
				valign: "middle",
				align: "center",
				colspan: 1,
				rowspan: 2
			},
			{
				title: "快贷系统新录入客户",
				valign: "middle",
				align: "center",
				colspan: 2,
				rowspan: 1
			},
			{
				title: "客户类型",
				valign: "middle",
				align: "center",
				colspan: 7,
				rowspan: 1
			}
		],
		[{
				field: 'create',
				title: '信息完整',
				valign: "middle",
				align: "center"
			},
			{
				field: 'notCreate',
				title: '信息不完整',
				valign: "middle",
				align: "center"
			},
			{
				field: 'ng',
				title: '农工',
				valign: "middle",
				align: "center"
			},
			{
				field: 'ybnh',
				title: '一般农户',
				valign: "middle",
				align: "center"
			 }
		]
	]
};

statisticsCountTwo.init = function () {
	statisticsCountTwo.table = $('#' + statisticsCountTwo.tableId).bootstrapTable({
        url: statisticsCountTwo.baseUrl, //请求后台的URL（*）
    	method: 'get', //请求方式（*）
    	toolbar: '#' + statisticsCountTwo.toolbarId, //工具按钮用哪个容器
    	cache: false, //是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
    	pagination: true, //是否显示分页（*）
        pageNumber: 1, //初始化加载第一页，默认第一页
        pageSize: 10, //每页的记录行数（*）
        pageList: [10, 25, 50, 100], //可供选择的每页的行数（*）
        showColumns: false, //是否显示所有的列
        showRefresh: true, //是否显示刷新按钮
        showToggle: true, //是否显示详细视图和列表视图的切换按钮
        singleSelect:true,
        striped:true,
        queryParams: statisticsCountTwo.queryParams,//传递参数（*）
        responseHandler:statisticsCountTwo.responseHandler,
        columns: statisticsCountTwo.columns()
    });
};

layui.use(['form', 'layedit', 'laydate', 'element'], function () {
    var editIndex;
    var layer = layui.layer, //获取当前窗口的layer对象
        form = layui.form,
        layedit = layui.layedit,
        laydate = layui.laydate;
    laydate.render({
        elem: '#jlBeginDate' //指定元素
      });
    laydate.render({
        elem: '#jlEndDate' //指定元素
      });
    laydate.render({
        elem: '#orgBeginDate' //指定元素
      });
    laydate.render({
        elem: '#orgEndDate' //指定元素
      });
    //默认显示客户经理统计表
    statisticsCountOne.init();
    statisticsCountTwo.init();
    
    $('#btn_jlQueryDate').on('click', function () {
    	statisticsCountOne.table.bootstrapTable('refresh');
    });
    
    $('#btn_orgQueryDate').on('click', function () {
    	statisticsCountTwo.table.bootstrapTable('refresh');
    });
    //客户经理导出
    $("#btn_jl_export").on('click',function(){
    	var beginDate = $('#jlBeginDate').val();
    	var endDate = $('#jlEndDate').val();
    	window.open("/CLIENT/custStatistics/cust/exportJlToExcel?beginDate="+beginDate+"&endDate="+endDate);
    });
    //机构导出
    $("#btn_org_export").on('click',function(){
    	var beginDate = $('#orgBeginDate').val();
    	var endDate = $('#orgEndDate').val();
    	window.open("/CLIENT/custStatistics/cust/exportOrgToExcel?beginDate="+beginDate+"&endDate="+endDate);
    });
    
    
})


