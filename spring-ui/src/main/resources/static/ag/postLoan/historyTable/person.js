var person = {
    baseUrl: '/CLIENT/checkHis/',
    entity: "person",
    tableId: "personHisTable",
    toolbarId: "personToolbar",
    unique: "id",
    order: "asc",
    code:"id",
    currentItem: {}
};

person.columns = function () {
	return [{
        checkbox: true
    },{
		field: 'custName',
		title: "客户姓名",
	},{
		field: 'idNo',
		title: "证件号",
	},{
		field: 'userName',
		title: "检查人员",
	},{
		field: 'contractNum',
		title: "合同编号",
	},{
		field: 'checkTime',
		title: "检查时间",
	},{
		title:"操作",
        formatter:function(value, row, index){
        	var tabMainSuretyMode = row.tabMainSuretyMode;
        	if(tabMainSuretyMode){
        		if(tabMainSuretyMode.indexOf('1,2')!=-1){
        			return "<a class='layui-btn layui-btn-sm' style='text-decoration:none' href='loanTable/grHisTable?checkId="+row.checkId+"&type="+row.creditProductCd+"&fileName="+row.docName+"&filePath="+row.docPath+"&stat=2'><i class='layui-icon'>&#xe615;</i></a> " +
     			   		   "<a class='layui-btn layui-btn-sm' style='text-decoration:none' onclick='clickEnsure("+JSON.stringify(row)+")'>保证人</a>";
        		}else if(tabMainSuretyMode.indexOf('1,1')!=-1){
        			return "<a class='layui-btn layui-btn-sm' style='text-decoration:none' href='loanTable/grHisTable?checkId="+row.checkId+"&type="+row.creditProductCd+"&fileName="+row.docName+"&filePath="+row.docPath+"&stat=2'><i class='layui-icon'>&#xe615;</i></a>"+
			   		   "<a class='layui-btn layui-btn-sm' style='text-decoration:none' onclick='clickCoinsurance("+JSON.stringify(row)+")'>联保成员</a>";
        		}else{
        			return "<a class='layui-btn layui-btn-sm' style='text-decoration:none' href='loanTable/grHisTable?checkId="+row.checkId+"&type="+row.creditProductCd+"&fileName="+row.docName+"&filePath="+row.docPath+"&stat=2'><i class='layui-icon'>&#xe615;</i></a>";
        		}
        	}else{
        		return "<a class='layui-btn layui-btn-sm' style='text-decoration:none' href='loanTable/grHisTable?checkId="+row.checkId+"&type="+row.creditProductCd+"&fileName="+row.docName+"&filePath="+row.docPath+"&stat=2'><i class='layui-icon'>&#xe615;</i></a>";
        	}
        	
        }
	}]
};

function clickEnsure(row){
	var checkId = row.checkId;
    $.get('postLoan/esnsure', null, function (form) {
        layer.open({
            type: 1,
            title: '保证人',
            content: form,
            shade: false,
            id:"ensure",
            area: ['50%', '90%'],
            maxmin: false,
            yes: function (index) {
                //触发表单的提交事件
                layedit.sync(editIndex);
                $('form.layui-form').find('button[lay-filter=edit]').click();
            },
            full: function (elem) {
                var win = window.top === window.self ? window : parent.window;
                $(win).on('resize', function () {
                    var $this = $(this);
                    elem.width($this.width()).height($this.height()).css({
                        top: 0,
                        left: 0
                    });
                    elem.children('div.layui-layer-content').height($this.height() - 95);
                });
            },
            success: function (layero, index) {
                var ensure = {
                	    baseUrl: "/CLIENT/guarantorCheckDoc",
                	    entity: "ensure",
                	    tableId: "ensureTable",
                	    toolbarId: "toolbar",
                	    unique: "id",
                	    order: "asc",
                	    code:"id",
                	    currentItem: {}
                	};

                	ensure.columns = function () {
                		return [{  
                			 field: 'guaranteeName',
                		     title: '姓名'
                	    },{  
                			 field: 'guaranteeContractNum',
                		     title: '被保证合同编号'
                	    },{
                			title:"操作",
                	        formatter:function(value, row, index){
                	        		return "<a class='layui-btn layui-btn-sm' style='text-decoration:none' href='loanTable/grHisTable?checkId="+row.checkId+"&type="+row.creditProductCd+"&fileName="+row.docName+"&filePath="+row.docPath+"&stat=2'><i class='layui-icon'>&#xe615;</i></a> ";
                	        	
                	        }
                		}]
                	};
                	ensure.queryParams = function (params) {
                	    if (!params)
                	        return false;
                	    var temp = { //这里的键的名字和控制器的变量名必须一直，这边改动，控制器也需要改成一样的
                	        limit: params.limit, //页面大小
                	        page: params.offset/params.limit+1, //页码
                	    };
                	    return temp;
                	};
                	ensure.responseHandler = function(response){
                		if(response.code =='200'){
                			return response.data;
                		}
                		return false;
                	};
                	ensure.init = function () {
                		ensure.table = $('#' + ensure.tableId).bootstrapTable({
                	        url:ensure.baseUrl+"/"+checkId, //请求后台的URL（*）
                	        method: 'get', //请求方式（*）
                	        striped: true, //是否显示行间隔色
                	        cache: false, //是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
                	        pagination: true, //是否显示分页（*）
                	        sortable: false, //是否启用排序
                	        sortOrder: ensure.order, //排序方式
                	        queryParams: ensure.queryParams,//传递参数（*）
//                	        sidePagination: "server", //分页方式：client客户端分页，server服务端分页（*）
                	        pageNumber: 1, //初始化加载第一页，默认第一页
                	        pageSize: 20, //每页的记录行数（*）
                	        pageList: [20, 50, 100], //可供选择的每页的行数（*）
                	        strictSearch: false,
                	        showColumns: false, //是否显示所有的列
                	        minimumCountColumns: 2, //最少允许的列数
                	        clickToSelect: true, //是否启用点击选中行
                	        uniqueId: ensure.unique, //每一行的唯一标识，一般为主键列
                	        cardView: false, //是否显示详细视图
                	        detailView: false, //是否显示父子表
                	        columns: ensure.columns(),
                	        responseHandler:ensure.responseHandler,
                	    });
                	};
                	ensure.init();
            }
        });
    });
}

function clickCoinsurance(row){
	var groupId = row.groupId;
    $.get('postLoan/esnsure', null, function (form) {
        layer.open({
            type: 1,
            title: '联保小组成员',
            content: form,
            shade: false,
            id:"ensure",
            area: ['80%', '90%'],
            maxmin: false,
            yes: function (index) {
                //触发表单的提交事件
                layedit.sync(editIndex);
                $('form.layui-form').find('button[lay-filter=edit]').click();
            },
            full: function (elem) {
                var win = window.top === window.self ? window : parent.window;
                $(win).on('resize', function () {
                    var $this = $(this);
                    elem.width($this.width()).height($this.height()).css({
                        top: 0,
                        left: 0
                    });
                    elem.children('div.layui-layer-content').height($this.height() - 95);
                });
            },
            success: function (layero, index) {
                var ensure = {
                	    baseUrl:person.baseUrl+"coinsurance/"+groupId,
                	    entity: "ensure",
                	    tableId: "ensureTable",
                	    toolbarId: "toolbar",
                	    unique: "id",
                	    order: "asc",
                	    code:"id",
                	    currentItem: {}
                	};

                	ensure.columns = function () {
                		return [{  
                			 field: 'custName',
                		     title: '姓名'
                	    },{  
                			 field: 'idNo',
                		     title: '证件号'
                	    },	{
                			field: 'contractNum',
                			title: "合同编号",
                		},
                		{
                			field: 'userName',
                			title: "检查人员",
                		},{
                			field: 'checkTime',
                			title: "检查时间",
                		},{
                			title:"操作",
                	        formatter:function(value, row, index){
                	        		return "<a class='layui-btn layui-btn-sm' style='text-decoration:none' href='loanTable/grHisTable?checkId="+row.checkId+"&type="+row.creditProductCd+"&fileName="+row.docName+"&filePath="+row.docPath+"&stat=2'><i class='layui-icon'>&#xe615;</i></a>";
                	        }
                		}]
                	};
                	ensure.queryParams = function (params) {
                	    if (!params)
                	        return false;
                	    var temp = { //这里的键的名字和控制器的变量名必须一直，这边改动，控制器也需要改成一样的
                	        limit: params.limit, //页面大小
                	        page: params.offset/params.limit+1, //页码
                	    };
                	    return temp;
                	};
                	ensure.responseHandler = function(response){
                		if(response.code =='200'){
                			return response.data;
                		}
                		return false;
                	};
                	ensure.init = function () {
                		ensure.table = $('#' + ensure.tableId).bootstrapTable({
                	        url:ensure.baseUrl, //请求后台的URL（*）
                	        method: 'get', //请求方式（*）
                	        striped: true, //是否显示行间隔色
                	        cache: false, //是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
                	        pagination: true, //是否显示分页（*）
                	        sortable: false, //是否启用排序
                	        sortOrder: ensure.order, //排序方式
                	        queryParams: ensure.queryParams,//传递参数（*）
//                	        sidePagination: "server", //分页方式：client客户端分页，server服务端分页（*）
                	        pageNumber: 1, //初始化加载第一页，默认第一页
                	        pageSize: 20, //每页的记录行数（*）
                	        pageList: [20, 50, 100], //可供选择的每页的行数（*）
                	        strictSearch: false,
                	        showColumns: false, //是否显示所有的列
                	        minimumCountColumns: 2, //最少允许的列数
                	        clickToSelect: true, //是否启用点击选中行
                	        uniqueId: ensure.unique, //每一行的唯一标识，一般为主键列
                	        cardView: false, //是否显示详细视图
                	        detailView: false, //是否显示父子表
                	        columns: ensure.columns(),
                	        responseHandler:ensure.responseHandler,
                	    });
                	};
                	ensure.init();
            }
        });
    });
}

person.queryParams = function (params) {
    if (!params)
        return {
    	checkType:"2",
    	contractNum:$("#personContractNum").val(),
    	checkTime:$("#personCheckTime").val()
        };
    var temp = { //这里的键的名字和控制器的变量名必须一直，这边改动，控制器也需要改成一样的
        limit: params.limit, //页面大小
        page: params.offset/params.limit+1, //页码
        checkType:"2",
    	contractNum:$("#personContractNum").val(),
    	checkTime:$("#personCheckTime").val()
    };
    return temp;
};

person.responseHandler = function(response){
	if(response.code =='200'){
		return response.data;
	}
	return false;
};

person.init = function () {
    person.table = $('#' + person.tableId).bootstrapTable({
        url: person.baseUrl, //请求后台的URL（*）
        method: 'get', //请求方式（*）
        toolbar: '#' + person.toolbarId, //工具按钮用哪个容器
        striped: true, //是否显示行间隔色
        cache: false, //是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
        pagination: true, //是否显示分页（*）
        sortable: false, //是否启用排序
        queryParams: person.queryParams,//传递参数（*）
        sidePagination: "server", //分页方式：client客户端分页，server服务端分页（*）
        pageNumber: 1, //初始化加载第一页，默认第一页
        pageSize: 10, //每页的记录行数（*）
        pageList: [10, 25, 50, 100], //可供选择的每页的行数（*）
        strictSearch: false,
        showColumns: false, //是否显示所有的列
        showRefresh: true, //是否显示刷新按钮
        singleSelect:true,//单选
        minimumCountColumns: 2, //最少允许的列数
        clickToSelect: true, //是否启用点击选中行
        uniqueId: person.unique, //每一行的唯一标识，一般为主键列
        showToggle: true, //是否显示详细视图和列表视图的切换按钮
        cardView: false, //是否显示详细视图
        detailView: false, //是否显示父子表
        columns: person.columns(),
        responseHandler:person.responseHandler
    });
};

person.select = function (layerTips) {
    var rows = person.table.bootstrapTable('getSelections');
    if (rows.length == 1) {
        person.currentItem = rows[0];
        return true;
    } else {
        layerTips.msg("请选中一行");
        return false;
    }
};
layui.use(['form', 'layedit', 'laydate','element'], function () {
    person.init();
    var layerTips = layui.layer, //获取父窗口的layer对象
        layer = layui.layer, //获取当前窗口的layer对象
        form = layui.form,
        layedit = layui.layedit,
        laydate = layui.laydate;
    laydate.render({
	  elem: '#personCheckTime' //指定元素
    });
    //初始化页面上面的按钮事件
	$('#btn_personQuery').on('click', function () {
		person.table.bootstrapTable('refresh');
	});
    $('#btn_personDel').on('click', function () {
        if (person.select(layerTips)) {       	
            layer.confirm('确定删除数据吗？', null, function (index) {
                $.ajax({
                    url: person.baseUrl+person.currentItem.checkId,
                    type: "delete",
                    contentType:"application/json",
                    success: function (data) {
                        if (data.code == '200') {
                            layerTips.msg("删除成功！");
                            person.table.bootstrapTable('refresh',person.queryParams());
                        } else {
                            layerTips.msg(data.message);
                        }
                    }
                });
            });
        }
    });
})