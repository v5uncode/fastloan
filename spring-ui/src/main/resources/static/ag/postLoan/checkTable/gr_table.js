var checkId = getUrlParam("checkId");
var dbType = getUrlParam("dbType");
var stat = getUrlParam("stat");
var peason ={
		"checkId":checkId,
		"majorEventList":[],//复选框数组
}
var aj_data;
var aj_data_arr;
peason.setOtherInfo=function (){
	//农户联保
	var lb = $("#contractNum").text();
	if(!lb){
		$("#creditProductCd").text('');
		$("#tabMainSuretyMode").text('');
	}
	//遍历按揭复选框数据
 	aj_data = $("#majorEventList").text();
	if(!aj_data){
//		return false;
	}else{
		layui.use(['form', 'layedit', 'laydate', 'element'], function () {
			form = layui.form;
			var checkboxs = $("tr").find("td").find("form").find("input");
			for(var i = 0;i<checkboxs.length; i ++ ){
				for(var j = 0;j<aj_data.length;j++){
					if(checkboxs[i].value == aj_data[j]){
						$(checkboxs[i]).attr("checked","checked");
							form.render();
					}
				}
			}
		})
	}
	//获取农户数据
	layui.use(['form', 'layedit', 'laydate', 'element'], function () {
		form = layui.form;
		var radioVal1 = $("#implStatusDesc").text();
		var radioVal2 = $("#riskStatusDesc").text();
		if(radioVal1 == "" && radioVal2){
			$("#implStatus1").attr("checked","checked");
			$("#riskStatus1").attr("checked","checked");
		}
		if(radioVal1 != "" && radioVal2 != ""){
			$("#implStatus2").attr("checked","checked");
			$("#riskStatus2").attr("checked","checked");
			$("#implStatusDesc").prop("contenteditable",true);
			$("#riskStatusDesc").prop("contenteditable",true)
		}else if(radioVal1 == "" && radioVal2 != ""){
			$("#implStatus1").attr("checked","checked");
			$("#riskStatus2").attr("checked","checked");
		}else if(radioVal1 != "" && radioVal2 == ""){
			$("#implStatus2").attr("checked","checked");
			$("#riskStatus1").attr("checked","checked");
		}else{
			$("#implStatus1").attr("checked","checked");
			$("#riskStatus1").attr("checked","checked");
		}
		form.render();
	})
	//获取个人经营水电费
	var sdf;
	if($("#itemList").text() != ""){
		sdf = JSON.parse($("#itemList").text())
	}
	if(sdf){
		if(sdf.length > 0){
			var str;
			for(var i = 0;i<sdf.length; i ++){
				str+='<TR>'
					+'<TD STYLE="font-size:10pt;">'+sdf[i].dtlType+'</TD>'
					+'<TD STYLE="font-size:10pt;">'+(sdf[i].threeMonth == null ? '':sdf[i].threeMonth )+'</TD>'
					+'<TD STYLE="font-size:10pt;">'+(sdf[i].twoMonth == null ? '': sdf[i].twoMonth)+'</TD>'
					+'<TD STYLE="font-size:10pt;">'+(sdf[i].oneMonth == null ?'':sdf[i].oneMonth)+'</TD>'
					+'<TD STYLE="font-size:10pt;">'+(sdf[i].rateChange == null ? '':sdf[i].rateChange )+'</TD>'
					+'<TD STYLE="font-size:10pt;">'+(sdf[i].dataExceptionReason == null ? '' : sdf[i].dataExceptionReason)+'</TD>'
					+'</TR>'
			}
			$("#addSDF_table").append(str);
			$("#isSDF").attr("ROWSPAN","4");//追加行
		}else{
		}
	}else{
		$("#isSDF").attr("ROWSPAN","3");
		$("#addSDF_tr").attr("hidden","hidden");
	}
    setTimeout(function(){
 	   $('select').selectlist({
 		   zIndex: 10,
 		   width: 150,
 		   height: 30,
 	   },100);
 	   //获取检查方式和检查原因、按揭房源和按揭类型
 	   var checkWay = $("#checkWay").text();
 	   var checkReason = $("#checkReason").text();
 	   var housing = $("#housing").text();
 	   var mortgageClass = $("#mortgageClass").text();
 	   var showFlag = $("#showFlag").text();
 	   if(checkWay != ""){
 	 		   //查询时赋值
 	 		   $("#checkWay2").find("input[name='checkWay2']").val(checkWay);
 	 		   $("#checkReason2").find("input[name='checkReason2']").val(checkReason);
 	 		   $("#housing2").find("input[name='housing2']").val(housing);
 	 		   $("#mortgageClass2").find("input[name='mortgageClass2']").val(mortgageClass);
 	 		  //赋值文本
 	 		   $("#checkWay2").find("input[class='select-button']").val($("#checkWay2 li")[checkWay].innerHTML);
 	 		   $("#checkReason2").find("input[class='select-button']").val($("#checkReason2 li")[checkReason].innerHTML);
 	 		   $("#housing2").find("input[class='select-button']").val($("#housing2 li")[housing] == undefined?"":$("#housing2 li")[housing].innerHTML);
 	 		   $("#mortgageClass2").find("input[class='select-button']").val($("#mortgageClass2 li")[mortgageClass] == undefined ?"":$("#mortgageClass2 li")[mortgageClass].innerHTML);
// 	 	   }
 	   }else{
 		   
 	   }
    })
}
layui.use(['form', 'layedit', 'laydate', 'element'], function () {
    var layerTips = layui.layer,
    layer = layui.layer, //获取当前窗口的layer对象
    form = layui.form,
    layedit = layui.layedit,
    laydate = layui.laydate;
    form.render();
  //加载字典
	  var json = {//(字典枚举)筛选
			  		"housing2":"HOUSING",//按揭房源
		  			"mortgageClass2":"MORTGAGE_CLASS",//按揭类型
		  			"checkReason2":"CHECK_REASON",//检查原因
		  			"checkWay2":"CHECK_WAY",//检查方式
                   };
       selectOnload({
                   "json" : json,
                   "isDefault" : false,
                   "func":form.render,
                   "length" : 4
       })
   
    $(function(){
  	  yunSetVal.init('/CLIENT/person/personTable/'+checkId+'',peason.setOtherInfo);
  });
    //监听农户表
	form.on('radio(radioYes_filter)', function(data){
		  if( $(data.elem).attr("data-value") == "0"){
			  $("#"+$(data.elem).attr("data-name")).prop("contenteditable",true);
			  $("#"+$(data.elem).attr("data-name")).text('');
			  $("#"+$(data.elem).attr("data-name")).focus();
		  }else{
			  $("#"+$(data.elem).attr("data-name")).prop("contenteditable",false);
			  $("#"+$(data.elem).attr("data-name")).text('');
		  }
		});
	form.on('radio(radio2Yes_filter)', function(data){
		  if( $(data.elem).attr("data-value") == "0"){
			  $("#"+$(data.elem).attr("data-name")).prop("contenteditable",true);
			  $("#"+$(data.elem).attr("data-name")).text('');
			  $("#"+$(data.elem).attr("data-name")).focus();
		  }else{
			  $("#"+$(data.elem).attr("data-name")).prop("contenteditable",false);
			  $("#"+$(data.elem).attr("data-name")).text('');
		  }
		});
})

//按揭
function ajSave(status){
	var arr = [];//复选框数组
	var arr2 = [];//单选框数组
	var checkboxs2 = $("tr").find("td").find("form").find("input");//按揭复选框
	//遍历复选框数据
	if(checkboxs2){
		for(var i = 0;i<checkboxs2.length;i ++){
			if(checkboxs2[i].checked){
				arr.push(checkboxs2[i].value);
			}
		}
		peason.majorEventList = arr;
		console.log(JSON.stringify(arr));
	}
	//个人检查表 检查方式和检查原因保存（按揭表还包括按揭房源和按揭类型）
	if(checkFun() && checkFyFun()){
		peason.checkWay = $("#checkWay2").find("input[name='checkWay2']").val();
		peason.checkReason = $("#checkReason2").find("input[name='checkReason2']").val();
		peason.housing = $("#housing2").find("input[name='housing2']").val();
		peason.mortgageClass = $("#mortgageClass2").find("input[name='mortgageClass2']").val();
	}else{
		return false;
	}
	//保存
	ajaxSave(status);
}
//个人经营
function grjySave(status){
	//保存
	//个人检查表 检查方式和检查原因保存
	if(checkFun()){
		peason.checkWay = $("#checkWay2").find("input[name='checkWay2']").val();
		peason.checkReason = $("#checkReason2").find("input[name='checkReason2']").val();
	}else{
		return false;
	}
	ajaxSave(status);
}
//农户
function nhSave(status){
	//个人检查表 检查方式和检查原因保存
	if(checkFun()){
		peason.checkWay = $("#checkWay2").find("input[name='checkWay2']").val();
		peason.checkReason = $("#checkReason2").find("input[name='checkReason2']").val();
	}else{
		return false;
	}
	var radios2 = $("tr").find("td").find("form").find("input");//农户单选框
	if(radios2){
		for(var i = 0;i<radios2.length;i ++){
			if(radios2[1]){
				if(radios2[1].checked == true){
					if($("#implStatusDesc").text() == ""){
						layer.tips("不按审批要求落实限制性条款，请填写说明情况", $("#implStatusDesc"),{
							tips: [1, '#1E9FFF']});
						return false;
					}
				}
			}
			if(radios2[3]){
				if(radios2[3].checked == true){
					if($("#riskStatusDesc").text() == ""){
						layer.tips("不向其他非主业扩张，请填写说明情况", $("#riskStatusDesc"),{
							tips: [1, '#1E9FFF']});
						return false;
					}
				}
			}
		}
		//农户数据
		var radioVal1 = $("#implStatusDesc").text();
		var radioVal2 = $("#riskStatusDesc").text();
		if(radioVal1 != "" && radioVal2 != ""){
			peason.implStatus = 0
			peason.riskStatus = 0
		}else if(radioVal1 == "" && radioVal2 != ""){
			peason.implStatus = 1
			peason.riskStatus = 0
		}else if(radioVal1 != "" && radioVal2 == ""){
			peason.implStatus = 0
			peason.riskStatus = 1
		}else{
			peason.implStatus = 1
			peason.riskStatus = 1
		}
		peason.implStatusDesc = radioVal1;
		peason.riskStatusDesc = radioVal2;
	}
	ajaxSave(status);
}
//公职
function gzSave(status){
	//个人检查表 检查方式和检查原因保存
	if(checkFun()){
		peason.checkWay = $("#checkWay2").find("input[name='checkWay2']").val();
		peason.checkReason = $("#checkReason2").find("input[name='checkReason2']").val();
	}else{
		return false;
	}
	ajaxSave(status);
}
//按揭提交
function ajHistorySave(status){
	 status = "1";
	 layer.confirm('提交后无法更改，确定执行该操作吗？', {icon: 4, title:'提示',shift:4}, function(index){
			//do something
		 	ajSave(status);
			layer.close(index);
		});
	
}
//公职提交
function gzHistorySave(status){
	 status = "1";
	 layer.confirm('提交后无法更改，确定执行该操作吗？', {icon: 4, title:'提示',shift:4}, function(index){
			//do something
		 	gzSave(status);
		 	layer.close(index);
		});
	
}
//个人经营提交
function grjyHistorySave(status){
	 status = "1";
	 if(dbType == "1,2"){
		 layer.confirm('请先提交其保证人检查表，确定后无法更改，要执行此操作吗？', {icon: 4, title:'提示',shift:4}, function(index){
			 //do something
			 grjySave(status);
			 layer.close(index);
		 });
	 }else{
		 layer.confirm('确定后无法更改，要执行此操作吗？', {icon: 4, title:'提示',shift:4}, function(index){
			 //do something
			 grjySave(status);
			 layer.close(index);
		 });
	 }
	
	
}
//农户提交
function nhHistorySave(status){
	 status = "1";
	 if(dbType == "1,2"){
		 layer.confirm('请先提交其保证人检查表，确定后无法更改，要执行此操作吗？', {icon: 4, title:'提示',shift:4}, function(index){
			 //do something
			 nhSave(status);
			 layer.close(index);
		 });
	 }else{
		 layer.confirm('确定后无法更改，要执行此操作吗？', {icon: 4, title:'提示',shift:4}, function(index){
			 //do something
			 nhSave(status);
			 layer.close(index);
		 });
	 }
	
	
}

//检查原因和检查方式校验公共方法
function checkFun(){
	var checkWay = $("#checkWay2").find("input[name='checkWay2']").val();
	var checkReason = $("#checkReason2").find("input[name='checkReason2']").val();
	if(checkWay == "" || checkWay == "请选择"){
		layer.tips("请选择检查方式", $("#checkWay1"),{
			tips: [1, '#1E9FFF']});
		document.getElementById("checkWay1").scrollIntoView(600);
		return false;
	}
	if(checkReason == "" || checkReason == "请选择"){
		layer.tips("请选择检查原因", $("#checkReason1"),{
			tips: [1, '#1E9FFF']});
		document.getElementById("checkReason1").scrollIntoView(600);
		return false;
	}
	return true;
}
//按揭房源方法
function checkFyFun(){
	var housing = $("#housing2").find("input[name='housing2']").val();
	var mortgageClass = $("#mortgageClass2").find("input[name='mortgageClass2']").val();
	if(housing == "" || housing == "请选择"){
		layer.tips("请选择按揭房源", $("#housing1"),{
			tips: [1, '#1E9FFF']});
		document.getElementById("housing1").scrollIntoView(600);
		return false;
	}
	if(mortgageClass == "" || mortgageClass == "请选择"){
		layer.tips("请选择按揭类型", $("#mortgageClass1"),{
			tips: [1, '#1E9FFF']});
		document.getElementById("mortgageClass1").scrollIntoView(600);
		return false;
	}
	return true;
}
//ajax公共保存方法
function ajaxSave(status){
	if(status == "1"){
		peason.showFlag = "1";
	}
	//保存
	$.ajax({
	  url:'/CLIENT/person/personTable',
	  type: 'put',
	  contentType:"application/json",
	  data:JSON.stringify(peason),
	  dataType: "json",
	  success: function (data) {
	  	if(data.code=='200'){
	  		if(status == "1"){
	  			layer.msg('提交成功');
	  			getCanvasImage();//上传图片
	  			setTimeout(function(){
		  			history.back(-1);
		  		},1000);
	  		}else{
	  			layer.msg('保存成功');
	  		}
	  		//查询按揭数据
	  	}else{
	  		layer.msg(data.message);
	  	}
	  },
	});
}
//打印表格
function myprint(tableid){
	var checkboxs3 = $("tr").find("td").find("form").find("input");
	if(checkboxs3){
		for(var i = 0;i<checkboxs3.length;i ++){
			if(!checkboxs3[i].checked){
				$(checkboxs3[i]).parent().hide();
			}
		}
	}
	$("#print").hide();
	setTimeout(function(){
		$("#print").show();
		if(checkboxs3){
			for(var i = 0;i<checkboxs3.length;i ++){
				if(!checkboxs3[i].checked){
					$(checkboxs3[i]).parent().show();
				}
			}
		}
	},200)
 	window.print();
    winname.document.execCommand('print'); //打印
    winname.close();
}
function getCanvasImage(){
	//要将 canvas 的宽高设置成容器宽高的 2 倍
	 	var img={};
		var w = $("#myCanvas").width();
		var h = $("#myCanvas").height();
		var canvas = document.createElement("canvas");
		canvas.width = w * 2;
		canvas.height = h * 2;
		canvas.style.width = w + "px";
		canvas.style.height = h + "px";
		var context = canvas.getContext("2d");
		//然后将画布缩放，将图像放大两倍画到画布上
		context.scale(2,2);
		 html2canvas($("#myCanvas"), {
			canvas: canvas,
	         onrendered: function(canvas) { 
	        	 //下载到本地
	             $('#down_button').attr('href', canvas.toDataURL());  
	             $('#down_button').attr('download', 'table.png');
	             //保存到服务器
	             canvas.id = "myCanvas";
                 //生成base64图片数据
                 var dataUrl = canvas.toDataURL();
                 img.file=dataUrl;
                 img.checkId = checkId;
                 img.checkType='2';
             	$.ajax({
             		  url:'/CLIENT/checkHis',
             		  type: 'post',
             		  contentType:"application/json",
             		  data:JSON.stringify(img),
             		  dataType: "json",
             		  success: function (data) {
             		  	if(data.code=='200'){
//             		  			layer.msg('保存成功');
             		  	}else{
             		  		layer.msg(data.message);
             		  	}
             		  },
             		});
	         }   
	     });
}