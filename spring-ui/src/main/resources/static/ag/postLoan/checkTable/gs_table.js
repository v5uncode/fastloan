var checkId = getUrlParam("checkId");
var company ={
		
}
company.setOtherInfo=function (){
	
	//设置负责人
	$("#userName1").text($("#userName").text());
	//授信业务中是否存在应关注的问题 不存在隐藏贷款基本信息
	var creditFocus = $("#creditFocus").text();
	var hzData = $("#companyFinancial").text();
	if(creditFocus == "是" && hzData != ''){
		$("#isHz").attr("ROWSPAN","4");
	}else if(creditFocus == "是" && hzData == ''){
		$("#isHz").attr("ROWSPAN","3");
		$("#addHZ_tr").hide();
	}else if(creditFocus == "否" && hzData != ''){
		$("#isHz").attr("ROWSPAN","3");
		$("#loanMsg").hide();
	}else{
		$("#isHz").attr("ROWSPAN","3");
		$("#loanMsg").hide();
		$("#addHZ_tr").hide();
		$("#testTR").show();
	}
	//获取公司经营变化情况
	$("ol").find("li").each(function(i){
		var data = $("#"+$(this).attr("name")).text();
		if(data){
			if(data.indexOf($(this).attr("value")) != -1){
				$(this).show();
			}else{
				
			}
		}else{
			if($(this).parent().parent().attr("id") == "inspectionReasonTD"){
			$(this).parent().parent().text('相关证件均已通过年检');
			}
			$(this).parent().parent().text('无（正常）');
		}
		
	})
	//获取公司经营水电费
	var sdf;
	if($("#itemList").text() != ""){
		sdf = JSON.parse($("#itemList").text())
	}
	if(sdf){
		if(sdf.length > 0){
			var str;
			for(var i = 0;i<sdf.length; i ++){
				str+='<TR>'
					+'<TD STYLE="font-size:10pt;">'+sdf[i].dtlType+'</TD>'
					+'<TD STYLE="font-size:10pt;">'+(sdf[i].threeMonth == null ? '' : sdf[i].threeMonth)+'</TD>'
					+'<TD STYLE="font-size:10pt;">'+(sdf[i].twoMonth == null ? '': sdf[i].twoMonth)+'</TD>'
					+'<TD STYLE="font-size:10pt;">'+(sdf[i].oneMonth == null ? '': sdf[i].oneMonth)+'</TD>'
					+'<TD STYLE="font-size:10pt;">'+(sdf[i].dataExceptionReason == null ? '相关数据显示正常':sdf[i].dataExceptionReason )+'</TD>'
					+'</TR>'
			}
			$("#addSDF_table").append(str);
			$("#isSDF").attr("ROWSPAN","11");//追加行
		}else{
		}
	}else{
		$("#isSDF").attr("ROWSPAN","10");
		$("#addSDF_tr").attr("hidden","hidden");
	}
	//获取公司融资机构信息
	var rz;
	if($("#financList").text() != ""){
		rz = JSON.parse($("#financList").text())
	}
	if(rz){
		if(rz.length > 0){
			var str1;
			for(var i = 0;i<rz.length; i ++){
				str1+='<TR>'
					+'<TD STYLE="font-size:10pt;">'+rz[i].financingTime+'</TD>'
					+'<TD STYLE="font-size:10pt;">'+(rz[i].financingOrgName == null ? '' :rz[i].financingOrgName)+'</TD>'
					+'<TD STYLE="font-size:10pt;">'+rz[i].financingAmount+'</TD>'
					+'<TD STYLE="font-size:10pt;">'+rz[i].financingMonth+'</TD>'
					+'<TD STYLE="font-size:10pt;">'+(rz[i].financingDesc == null ? '':rz[i].financingDesc)+'</TD>'
					+'</TR>'
			}
			$("#addRZ_table").append(str1);
			$("#isRZ").attr("ROWSPAN","16");//追加行
		}else{
		}
	}else{
		$("#isRZ").attr("ROWSPAN","15");
		$("#addRZ_tr").attr("hidden","hidden");
	}
	//获取公司合作机构
	var hz;
	if($("#companyFinancial").text() != ""){
		hz = JSON.parse($("#companyFinancial").text())
	}
	if(hz){
		if(hz.length > 0){
			var str2;
			for(var i = 0;i<hz.length; i ++){
				str2+='<TR>'
					+'<TD STYLE="font-size:10pt;">'+hz[i].agencyName+'</TD>'
					+'<TD STYLE="font-size:10pt;">'+hz[i].creditAmount+'</TD>'
					+'<TD STYLE="font-size:10pt;">'+hz[i].letterAmount+'</TD>'
					+'<TD STYLE="font-size:10pt;">'+(hz[i].security == null ?'':hz[i].security)+'</TD>'
					+'<TD STYLE="font-size:10pt;">'+(hz[i].formClass == null ?'':hz[i].formClass)+'</TD>'
					+'<TD STYLE="font-size:10pt;">'+(hz[i].overdueInterest == null ?'':hz[i].overdueInterest)+'</TD>'
					+'</TR>'
			}
			$("#addHZ_table").append(str2);
			$("#isHZ").attr("ROWSPAN","16");//追加行
		}else{
		}
	}else{
		$("#isHZ").attr("ROWSPAN","15");
		$("#addHZ_tr").attr("hidden","hidden");
	}
}
layui.use(['form', 'layedit', 'laydate', 'element'], function () {
    var layerTips = layui.layer,
    layer = layui.layer, //获取当前窗口的layer对象
    form = layui.form,
    layedit = layui.layedit,
    laydate = layui.laydate
    $(function(){
  	  yunSetVal.init('/CLIENT/companyCheck/'+checkId+'/companyCheck',company.setOtherInfo);
  });
})
//保存公司数据
function gsSave(status){
	var str= [$("#currentConclusion")[0],
	           $("#useMoneyDesc")[0],
	           $("#premiseMeetDesc")[0],
	           $("#continuaMeetDesc")[0],
	           $("#otherRiskDesc")[0],
	           $("#conclusionDesc")[0]
	           ];
	for(var i = 0;i<str.length;i++){
		if(str[i].contentEditable == "true"){//编辑表格为true时执行
			if(str[i].innerHTML==''||str[i].innerHTML==null ){
				document.getElementById(str[i].id).scrollIntoView(600);
					layer.tips(str[i].innerHTML+"不能为空哦", str[i],{
						tips: [1, '#78BA32']}); //在元素的事件回调体中，this或者元素本事
					return false;
			}
		}
	}
	company.checkId = checkId;
	company.currentConclusion = $("#currentConclusion").text();
	company.useMoneyDesc = $("#useMoneyDesc").text();
	company.premiseMeetDesc = $("#premiseMeetDesc").text();
	company.continuaMeetDesc = $("#continuaMeetDesc").text();
	company.otherRiskDesc = $("#otherRiskDesc").text();
	company.conclusionDesc = $("#conclusionDesc").text();
		//保存
		$.ajax({
		  url:'/CLIENT/companyCheck/companyCheck',
		  type: 'put',
		  contentType:"application/json",
		  data:JSON.stringify(company),
		  dataType: "json",
		  success: function (data) {
		  	if(data.code=='200'){
		  		if(status == "1"){
		  			layer.msg('提交成功');
		  			getCanvasImage();//上传图片
		  			setTimeout(function(){
			  			history.back(-1);
			  		},1000);
		  		}else{
		  			layer.msg('保存成功');
		  		}
		  	}else{
		  		layer.msg(data.message);
		  	}
		  },
		});
}
//提交公司贷后表
function gsHistorySave(status){
	 status = "1";
	 layer.confirm('提交后无法更改，确定执行该操作吗？', {icon: 4, title:'提示',shift:4}, function(index){
			//do something
		 	gsSave(status);
			layer.close(index);
		});
}
//打印公司表格
function myprint(tableid){
	$("#print").hide();
	setTimeout(function(){
		$("#print").show();
	},200)
 	window.print();
    winname.document.execCommand('print'); //打印
    winname.close();
}
function getCanvasImage(){
	//要将 canvas 的宽高设置成容器宽高的 2 倍
	 	var img={};
		var w = $("#myCanvas").width();
		var h = $("#myCanvas").height();
		var canvas = document.createElement("canvas");
		canvas.width = w * 2;
		canvas.height = h * 2;
		canvas.style.width = w + "px";
		canvas.style.height = h + "px";
		var context = canvas.getContext("2d");
		//然后将画布缩放，将图像放大两倍画到画布上
		context.scale(2,2);
		 html2canvas($("#myCanvas"), {
			canvas: canvas,
	         onrendered: function(canvas) { 
	        	 //下载到本地
	             $('#down_button').attr('href', canvas.toDataURL());  
	             $('#down_button').attr('download', 'table.png');
	             //保存到服务器
	             canvas.id = "myCanvas";
                 //生成base64图片数据
                 var dataUrl = canvas.toDataURL();
                 img.file=dataUrl;
                 img.checkId = checkId;
                 img.checkType='1';//1公司 2个人
             	$.ajax({
             		  url:'/CLIENT/checkHis',
             		  type: 'post',
             		  contentType:"application/json",
             		  data:JSON.stringify(img),
             		  dataType: "json",
             		  success: function (data) {
             		  	if(data.code=='200'){
//             		  			layer.msg('保存成功');
             		  	}else{
             		  		layer.msg(data.message);
             		  	}
             		  },
             		});
	         }   
	     });
}