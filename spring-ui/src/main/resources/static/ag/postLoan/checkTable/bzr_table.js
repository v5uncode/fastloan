var checkTime = getUrlParam("checkTime");
var checkWay = getUrlParam("checkWay");
var checkReason = getUrlParam("checkReason");
var custId = getUrlParam("custId");//保证人id
var custName = getUrlParam("oldCustName");//被保证人姓名
var oldCustId = getUrlParam("oldCustId");//被保证人ID
var oldCustName = getUrlParam("custName");//被保证人姓名
var contractNum = getUrlParam("contractNum");//被保证人合同编号
var checkId = getUrlParam("checkId");//检查表ID
var stat = getUrlParam("stat");
var personTable ={
}
personTable.setOtherInfo=function(){
		//合同数据
		var ht = $("#contractList").text();
		//水电费
		var sdf = $("#itemList").text();
		//遍历合同数据
		var htData;
		var str1;
		if(ht != ""){
			htData = JSON.parse(ht);
		}
		if(htData){
		if(htData.length > 0){
		for(var i = 0 ;i < htData.length;i ++ ){
			str1="<table>"
				+"<tr>"
				+"<TD STYLE='font-size:10pt;'>合同编号</TD>"
				+"<TD STYLE='font-size:10pt;' id='contractNum' name='contractNum'>"+htData[i].contractNum+"</TD>"
				+"<TD STYLE='font-size:10pt;'>贷款金额(万元)</TD>"
				+"<TD STYLE='font-size:10pt;' id='contractTotalAmt' name='contractTotalAmt'>"+htData[i].contractTotalAmt+"</TD>"
				+"<TD STYLE='font-size:10pt;'>贷款期限(月)</TD>"
				+"<TD STYLE='font-size:10pt;' id='contractTerm' name='contractTerm'>"+htData[i].contractTerm+"</TD>"
				+"</tr>"
				+"<tr>"
				+"<TD STYLE='font-size:10pt;'>贷款余额(万元)</TD>"
				+"<TD STYLE='font-size:10pt;' id='bondRemain' name='bondRemain'>"+htData[i].balanceAmount+"</TD>"
				+"<TD STYLE='font-size:10pt;'>贷款利率（‰）</TD>"
				+"<TD STYLE='font-size:10pt;' id='monthRate' name='monthRate'>"+htData[i].monthRate+"</TD>"
				+"<TD STYLE='font-size:10pt;'>贷款种类</TD>"
				+"<TD STYLE='font-size:10pt;' id='creditProductCd' name='creditProductCd'>"+htData[i].creditProductCd+"</TD>"
				+"</tr>"
				+"<tr>"
				+"<TD STYLE='font-size:10pt;'>贷款用途</TD>"
				+"<TD STYLE='font-size:10pt;' id='contractProvidePurpose' name='contractProvidePurpose'>"+htData[i].contractProvidePurpose+"</TD>"
				+"<TD STYLE='font-size:10pt;'>担保方式</TD>"
				+"<TD STYLE='font-size:10pt;' id='vouchWay' name='vouchWay'>"+htData[i].mainSuretyMode+"</TD>"
				+"<TD STYLE='font-size:10pt;'>还款方式</TD>"
				+"<TD STYLE='font-size:10pt;' id='payWay' name='payWay'>"+htData[i].payWay+"</TD>"
				+"</tr>"
				+"</table>"
				$("#htAddTD").append(str1);
			}
		}
	}else{
//		$("#isSDF").attr("ROWSPAN","3");
		$("#htAddTD").attr("hidden","hidden");
	}
    	var sdfData;
			if(sdf){
				sdfData = JSON.parse(sdf)
			}
			if(sdf){
				if(sdfData.length > 0){
					var str;
					for(var i = 0;i<sdfData.length; i ++){
						str+='<TR>'
							+'<TD STYLE="font-size:10pt;">'+sdfData[i].dtlType+'</TD>'
							+'<TD STYLE="font-size:10pt;">'+(sdfData[i].threeMonth == null ?'' :sdfData[i].threeMonth)+'</TD>'
							+'<TD STYLE="font-size:10pt;">'+(sdfData[i].twoMonth == null ?'' : sdfData[i].twoMonth)+'</TD>'
							+'<TD STYLE="font-size:10pt;">'+(sdfData[i].oneMonth == null ?'' : sdfData[i].oneMonth)+'</TD>'
							+'<TD STYLE="font-size:10pt;">'+(sdfData[i].rateChange == null ?"":sdfData[i].rateChange)+'</TD>'
							+'<TD STYLE="font-size:10pt;">'+(sdfData[i].dataExceptionReason == null ? "相关数据显示正常":sdfData[i].dataExceptionReason)+'</TD>'
							+'</TR>'
					}
					$("#addSDF_table").append(str);
					$("#isSDF").attr("ROWSPAN","4");//追加行
				}else{
				}
			}else{
				$("#isSDF").attr("ROWSPAN","3");
				$("#addSDF_tr").attr("hidden","hidden");
			}
}
layui.use(['form', 'layedit', 'laydate', 'element'], function () {
    var layerTips = layui.layer,
    layer = layui.layer, //获取当前窗口的layer对象
    form = layui.form,
    layedit = layui.layedit,
    laydate = layui.laydate;
   
    $(function(){
    	//查询保证人信息
    	 yunSetVal.init('/CLIENT/person/personTable/ensure/'+checkTime+'/'+checkWay+'/'+checkReason+'/'+custId+'',personTable.setOtherInfo);
    });
})
//保证人提交
var dataUrl;
function bzrHistorySave(status){
	//要将 canvas 的宽高设置成容器宽高的 2 倍
 	var img={};
	var w = $("#myCanvas").width();
	var h = $("#myCanvas").height();
	var canvas = document.createElement("canvas");
	canvas.width = w * 2;
	canvas.height = h * 2;
	canvas.style.width = w + "px";
	canvas.style.height = h + "px";
	var context = canvas.getContext("2d");
	//然后将画布缩放，将图像放大两倍画到画布上
	context.scale(2,2);
	 html2canvas($("#myCanvas"), {
		canvas: canvas,
         onrendered: function(canvas) {
             //保存到服务器
             canvas.id = "myCanvas";
             //生成base64图片数据
             dataUrl = canvas.toDataURL();
         }   
     });
	 status = "1";
	 layer.confirm('提交后无法更改，确定执行该操作吗？', {icon: 4, title:'提示',shift:4}, function(index){
			//do something
		 ajaxSave(status);
 		layer.close(index);
		});
}
//ajax公共保存方法
function ajaxSave(status){
	if($("#checkWay").text() == ''){
		layer.msg("请先保存借款人的检查方式和检查原因");
		return false;
	}
	var peason={
		checkId : checkId,
		guaranteeCustomerNum :custId,
		guaranteeContractNum : contractNum,
		guaranteePersonName : oldCustName,
		guaranteeName : custName,
		guaranteePersonNum : oldCustId,
		file: dataUrl//上传图片
	}
	//保存
	$.ajax({
	  url:'/CLIENT/guarantorCheckDoc/',
	  type: 'post',
	  contentType:"application/json",
	  data:JSON.stringify(peason),
	  dataType: "json",
	  async: false,
	  success: function (data) {
	  	if(data.code=='200'){
	  			layer.msg('提交成功');
	  			setTimeout(function(){
		  			history.back(-1);
		  		},1000);
	  		//查询按揭数据
	  	}else{
	  		layer.msg(data.message);
	  	}
	  },
	});
}
//打印表格
function myprint(tableid){
	var checkboxs3 = $("tr").find("td").find("form").find("input");
	if(checkboxs3){
		for(var i = 0;i<checkboxs3.length;i ++){
			if(!checkboxs3[i].checked){
				$(checkboxs3[i]).parent().hide();
			}
		}
	}
	$("#print").hide();
	setTimeout(function(){
		$("#print").show();
		if(checkboxs3){
			for(var i = 0;i<checkboxs3.length;i ++){
				if(!checkboxs3[i].checked){
					$(checkboxs3[i]).parent().show();
				}
			}
		}
	},200)
 	window.print();
    winname.document.execCommand('print'); //打印
    winname.close();
}
//function getCanvasImage(){
//
//}