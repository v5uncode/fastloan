var company = {
    baseUrl: "/CLIENT/companyCheck/companyCheck",
    entity: "company",
    tableId: "companyTable",
    toolbarId: "companyToolbar",
    unique: "id",
    order: "asc",
    code:"id",
    currentItem: {}
};

company.columns = function () {
	return [{
        checkbox: true
    },{
		field: 'custName',
		title: "客户姓名",
	},{
		field: 'idNo',
		title: "证件号",
	},
	{
		field: 'contractNum',
		title: "合同编号",
	},
	{
		field: 'userName',
		title: "检查人员",
	},{
		field: 'checkTime',
		title: "检查时间",
	},{
		title:"操作",
        formatter:function(value, row, index){
        	return "<a class='layui-btn layui-btn-sm' style='text-decoration:none' href='loanTable/gsTable?checkId="+row.checkId+"&title=公司检查表&type=gsTab&stat=1'><i class='layui-icon'>&#xe615;</i></a>";
        }
	}]
};


company.queryParams = function (params) {
    if (!params)
        return {
    	contractNum: $('#companyContractNum').val(),
    	checkTime: $('#companyCheckTime').val()
        };
    var temp = { //这里的键的名字和控制器的变量名必须一直，这边改动，控制器也需要改成一样的
        limit: params.limit, //页面大小
        page: params.offset/params.limit+1, //页码
    	contractNum: $('#companyContractNum').val(),
    	checkTime: $('#companyCheckTime').val()
    };
    return temp;
};

company.responseHandler = function(response){
	if(response.code =='200'){
		return response.data;
	}
	return false;
};

company.init = function () {
    company.table = $('#' + company.tableId).bootstrapTable({
        url: company.baseUrl, //请求后台的URL（*）
        method: 'get', //请求方式（*）
        toolbar: '#' + company.toolbarId, //工具按钮用哪个容器
        striped: true, //是否显示行间隔色
        cache: false, //是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
        pagination: true, //是否显示分页（*）
        sortable: false, //是否启用排序
        queryParams: company.queryParams,//传递参数（*）
        sidePagination: "server", //分页方式：client客户端分页，server服务端分页（*）
        pageNumber: 1, //初始化加载第一页，默认第一页
        pageSize: 10, //每页的记录行数（*）
        pageList: [10, 25, 50, 100], //可供选择的每页的行数（*）
        strictSearch: false,
        showColumns: false, //是否显示所有的列
        showRefresh: true, //是否显示刷新按钮
        singleSelect:true,//单选
        minimumCountColumns: 2, //最少允许的列数
        clickToSelect: true, //是否启用点击选中行
        uniqueId: company.unique, //每一行的唯一标识，一般为主键列
        showToggle: true, //是否显示详细视图和列表视图的切换按钮
        cardView: false, //是否显示详细视图
        detailView: false, //是否显示父子表
        columns: company.columns(),
        responseHandler:company.responseHandler
    });
};

company.select = function (layerTips) {
    var rows = company.table.bootstrapTable('getSelections');
    if (rows.length == 1) {
        company.currentItem = rows[0];
        return true;
    } else {
        layerTips.msg("请选中一行");
        return false;
    }
};
layui.use(['form', 'layedit', 'laydate','element'], function () {
    company.init();
    var layerTips = layui.layer, //获取父窗口的layer对象
        layer = layui.layer, //获取当前窗口的layer对象
        form = layui.form,
        layedit = layui.layedit,
        laydate = layui.laydate;
    laydate.render({
	  elem: '#companyCheckTime' //指定元素
    });
    //初始化页面上面的按钮事件
	$('#btn_companyQuery').on('click', function () {
		company.table.bootstrapTable('refresh');
	});
    $('#btn_companyDel').on('click', function () {
        if (company.select(layerTips)) {       	
            layer.confirm('确定删除数据吗？', null, function (index) {
                $.ajax({
                    url: company.baseUrl+"/"+company.currentItem.checkId,
                    type: "delete",
                    contentType:"application/json",
                    success: function (data) {
                        if (data.code == '200') {
                            layerTips.msg("删除成功！");
                            company.table.bootstrapTable('refresh',company.queryParams());
                        } else {
                            layerTips.msg(data.message);
                        }
                    }
                });
            });
        }
    });
})