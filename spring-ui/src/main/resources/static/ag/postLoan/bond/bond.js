var bond = {
    baseUrl: "/CLIENT/bond/bond",
    entity: "bond",
    tableId: "bondTable",
    toolbarId: "toolbar",
    unique: "id",
    order: "asc",
    code:"id",
    currentItem: {}
};

bond.columns = function () {
	return [{  
		 field: 'custName',
	     title: '姓名'
    },{  
		 field: 'idNo',
	     title: '身份证号'
    },{
        field: 'bondId',
        title: '借据号'
    },{
        field: 'bondLimit',
        title: '借据金额'
    },{
        field: 'pactId',
        title: '合同号'
    },{
        field: 'startDate',
        title: '始贷日期'
    },{
        field: 'dueDate',
        title: '到期日期'
    },{
        field: 'postloanDate',
        title: '贷后调查日期'
    },{
        field: 'loanFiveClassC',
        title: '贷款五级分类形态'
    }]
};


bond.queryParams = function (params) {
    if (!params)
        return {
    	bondId: $("#bondId").val(),
    	pactId: $("#pactId").val(),
    	custName:$("#custName").val(),
    	idNo:$("#idNo").val(),
        };
    var temp = { //这里的键的名字和控制器的变量名必须一直，这边改动，控制器也需要改成一样的
        limit: params.limit, //页面大小
        page: params.offset/params.limit+1, //页码
    	bondId: $("#bondId").val(),
    	pactId: $("#pactId").val(),
    	custName:$("#custName").val(),
    	idNo:$("#idNo").val(),
    };
    return temp;
};

bond.responseHandler = function(response){
	if(response.code =='200'){
		return response.data;
	}
	return false;
};

bond.init = function () {
    bond.table = $('#' + bond.tableId).bootstrapTable({
        url: bond.baseUrl, //请求后台的URL（*）
        method: 'get', //请求方式（*）
        toolbar: '#' + bond.toolbarId, //工具按钮用哪个容器
        striped: true, //是否显示行间隔色
        cache: false, //是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
        pagination: true, //是否显示分页（*）
        sortable: false, //是否启用排序
        sortOrder: bond.order, //排序方式
        queryParams: bond.queryParams,//传递参数（*）
        sidePagination: "server", //分页方式：client客户端分页，server服务端分页（*）
        pageNumber: 1, //初始化加载第一页，默认第一页
        pageSize: 10, //每页的记录行数（*）
        pageList: [10, 25, 50, 100], //可供选择的每页的行数（*）
        strictSearch: false,
        showColumns: false, //是否显示所有的列
        showRefresh: true, //是否显示刷新按钮
        minimumCountColumns: 2, //最少允许的列数
        clickToSelect: true, //是否启用点击选中行
        uniqueId: bond.unique, //每一行的唯一标识，一般为主键列
        showToggle: true, //是否显示详细视图和列表视图的切换按钮
        cardView: false, //是否显示详细视图
        detailView: false, //是否显示父子表
        columns: bond.columns(),
        responseHandler:bond.responseHandler,
    });
};

bond.select = function (layerTips) {
    var rows = bond.table.bootstrapTable('getSelections');
    if (rows.length == 1) {
        bond.currentItem = rows[0];
        return true;
    } else {
        layerTips.msg("请选中一行");
        return false;
    }
};

layui.use(['form', 'layedit', 'laydate'], function () {
    bond.init();
    var editIndex;
    var layerTips = layui.layer, //获取父窗口的layer对象
        layer = layui.layer, //获取当前窗口的layer对象
        form = layui.form,
        layedit = layui.layedit,
        laydate = layui.laydate;
    var addBoxIndex = -1;
    //初始化页面上面的按钮事件
    $('#btn_query').on('click', function () {
        bond.table.bootstrapTable('refresh',bond.queryParams());
    });
})