var contract = {
	    baseUrl: "/CLIENT/contract/listPage",
	    tableId: "contractTable",
	    order: "asc",
	    currentItem: {}
	};
contract.responseHandler = function(response){
	if(response.code =='200'){
		return response.data;
	}
	return false;
};
contract.queryParams = function (params) {
    if (!params)
        return {
    			contractNum: $("#contractNum").val(),
    			idNo: $("#idNo").val()
        };
    var temp = { //这里的键的名字和控制器的变量名必须一直，这边改动，控制器也需要改成一样的
        limit: params.limit, //页面大小
        page: params.offset/params.limit+1, //页码
        contractNum: $("#contractNum").val(),
		idNo: $("#idNo").val()
    };
    return temp;
};
contract.columns = function () {
	return [{
			field: 'custName',
			title: "姓名",
			align: "center"
		},{
			field: 'idNo',
			title: "证件号",
			align: "center"
		},{
			field: 'contractNum',
			title: "合同编号",
			align: "center"
		},{
			field: 'contractTotalAmt',
			title: "合同金额",
			align: "center"
		},
		{
			field: 'creditNatureC',
			title: "合同性质",
			align: "center"
		},
		{
			field: 'creditProductCdC',
			title: "授信品质代码",
			align: "center"
		},{
			field: 'jjCount',
			title: "借据笔数",
			align: "center",
	        formatter:function(value, row, index){
	        	if(row.jjCount>0){
	        		return "<a href='#'style='color :blue' onclick='clickBondCell("+JSON.stringify(row)+")'>"+value+"</a>";
	        	}
	        }
		},{
			field: 'dkAmount',
			title: "借据总金额",
			align: "center"
		}];
};

function clickBondCell(row){
    $.get('contract/bondBase', null, function (form) {
        layer.open({
            type: 1,
            title: '借据',
            content: form,
            shade: false,
            area: ['100%', '100%'],
            maxmin: false,
            yes: function (index) {
                //触发表单的提交事件
                layedit.sync(editIndex);
                $('form.layui-form').find('button[lay-filter=edit]').click();
            },
            full: function (elem) {
                var win = window.top === window.self ? window : parent.window;
                $(win).on('resize', function () {
                    var $this = $(this);
                    elem.width($this.width()).height($this.height()).css({
                        top: 0,
                        left: 0
                    });
                    elem.children('div.layui-layer-content').height($this.height() - 95);
                });
            },
            success: function (layero, index) {
                var form = layui.form;
                form.render();
                var bond = {
                	    baseUrl: "/CLIENT/bond/bond",
                	    entity: "bond",
                	    tableId: "bondTable",
                	    toolbarId: "toolbar",
                	    unique: "id",
                	    order: "asc",
                	    code:"id",
                	    currentItem: {}
                	};

                	bond.columns = function () {
                		return [{  
                			 field: 'custName',
                		     title: '姓名'
                	    },{  
                			 field: 'idNo',
                		     title: '身份证号'
                	    },{
                	        field: 'bondId',
                	        title: '借据号'
                	    },{
                	        field: 'bondLimit',
                	        title: '借据金额'
                	    },{
                	        field: 'pactId',
                	        title: '合同号'
                	    },{
                	        field: 'startDate',
                	        title: '始贷日期'
                	    },{
                	        field: 'dueDate',
                	        title: '到期日期'
                	    },{
                	        field: 'postloanDate',
                	        title: '贷后调查日期'
                	    },{
                	        field: 'loanFiveClassC',
                	        title: '贷款五级分类形态'
                	    }]
                	};


                	bond.queryParams = function (params) {
                	    if (!params)
                	        return false;
                	    var temp = { //这里的键的名字和控制器的变量名必须一直，这边改动，控制器也需要改成一样的
                	        limit: params.limit, //页面大小
                	        page: params.offset/params.limit+1, //页码
                	    };
                	    return temp;
                	};

                	bond.responseHandler = function(response){
                		if(response.code =='200'){
                			return response.data;
                		}
                		return false;
                	};

                	bond.init = function () {
                	    bond.table = $('#' + bond.tableId).bootstrapTable({
                	        url: bond.baseUrl+"?pactId="+row.contractNum, //请求后台的URL（*）
                	        method: 'get', //请求方式（*）
//                	        toolbar: '#' + bond.toolbarId, //工具按钮用哪个容器
                	        striped: true, //是否显示行间隔色
                	        cache: false, //是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
                	        pagination: true, //是否显示分页（*）
                	        sortable: false, //是否启用排序
                	        sortOrder: bond.order, //排序方式
                	        queryParams: bond.queryParams,//传递参数（*）
                	        sidePagination: "server", //分页方式：client客户端分页，server服务端分页（*）
                	        pageNumber: 1, //初始化加载第一页，默认第一页
                	        pageSize: 20, //每页的记录行数（*）
                	        pageList: [20, 50, 100], //可供选择的每页的行数（*）
                	        strictSearch: false,
                	        showColumns: false, //是否显示所有的列
                	        minimumCountColumns: 2, //最少允许的列数
                	        clickToSelect: true, //是否启用点击选中行
                	        uniqueId: bond.unique, //每一行的唯一标识，一般为主键列
                	        cardView: false, //是否显示详细视图
                	        detailView: false, //是否显示父子表
                	        columns: bond.columns(),
                	        responseHandler:bond.responseHandler,
                	    });
                	};
                	bond.init();
            }
        });
    });
}


contract.init = function () {
	contract.table = $('#' + contract.tableId).bootstrapTable({
        url: contract.baseUrl, //请求后台的URL（*）
        method: 'get', //请求方式（*）
        striped: true, //是否显示行间隔色
        cache: false, //是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
        pagination: true, //是否显示分页（*）
        sortable: false, //是否启用排序
        sortOrder: contract.order, //排序方式
        queryParams: contract.queryParams,//传递参数（*）
        sidePagination: "server", //分页方式：client客户端分页，server服务端分页（*）
        pageNumber: 1, //初始化加载第一页，默认第一页
        pageSize: 10, //每页的记录行数（*）
        pageList: [10, 25, 50, 100], //可供选择的每页的行数（*）
        strictSearch: false,
        showColumns: false, //是否显示所有的列
        showRefresh: true, //是否显示刷新按钮
        minimumCountColumns: 2, //最少允许的列数
        clickToSelect: true, //是否启用点击选中行
        uniqueId: contract.unique, //每一行的唯一标识，一般为主键列
        showToggle: true, //是否显示详细视图和列表视图的切换按钮
        cardView: false, //是否显示详细视图
        detailView: false, //是否显示父子表
        columns: contract.columns(),
        responseHandler:contract.responseHandler
    });
};

layui.use(['form', 'layedit', 'laydate'], function () {
	contract.init();
    var editIndex;
    var layerTips = layui.layer, //获取父窗口的layer对象
        layer = layui.layer, //获取当前窗口的layer对象
        form = layui.form,
        layedit = layui.layedit,
        laydate = layui.laydate;
    var addBoxIndex = -1;
    //初始化页面上面的按钮事件
    $('#btn_query').on('click', function () {
    	contract.table.bootstrapTable('refresh',contract.queryParams());
    });
})