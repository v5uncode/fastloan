var custRemind = {
    baseUrl: "/custRemind",
    entity: "custRemind",
    tableId: "custRemindTable",
    toolbarId: "toolbar",
    unique: "id",
    order: "asc",
    currentItem: []
};

custRemind.columns = function () {
    return [{
        checkbox: true
    },{  
        title: '序号',
        formatter: function (value, row, index) {  
            return index+1;  
        }  
    }, {
        field: 'custName',
        title: '用户姓名'
    }, {
        field: 'telNo',
        title: '电话号码'
    }, {
        field: 'custTypeC',
        title: '客户类型'
    }, {
        field: 'mtnDate',
        title: '更新日期',
        formatter:function(value, row, index){
        	if(row.mtnDate.length ==8){
                var year = row.mtnDate.slice(0,4);
                var month = row.mtnDate.slice(4,6);
                var date = row.mtnDate.slice(6,8);
                row.mtnDate = year+'-'+month+'-'+date;
            }
        	return row.mtnDate;
        }
    }, {
        field: 'custGrpName',
        title: '管户经理'
    }];
};

custRemind.queryParams = function (params) {
    if (!params)
        return {
    	custName: $("#custName").val(),
    	telNo: $("#telNo").val()
        };
    var temp = { //这里的键的名字和控制器的变量名必须一直，这边改动，控制器也需要改成一样的
        limit: params.limit, //页面大小
        page: params.offset/params.limit+1, //页码
        custName: $("#custName").val(),
        telNo: $("#telNo").val()
    };
    return temp;
};

custRemind.responseHandler = function(response){
	if(response.code =='200'){
		return response.data;
	}
	return false;
};

custRemind.init = function () {
    custRemind.table = $('#' + custRemind.tableId).bootstrapTable({
        url: custRemind.baseUrl, //请求后台的URL（*）
        method: 'get', //请求方式（*）
        toolbar: '#' + custRemind.toolbarId, //工具按钮用哪个容器
        striped: true, //是否显示行间隔色
        cache: false, //是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
        pagination: true, //是否显示分页（*）
        sortable: false, //是否启用排序
        sortOrder: custRemind.order, //排序方式
        queryParams: custRemind.queryParams,//传递参数（*）
        sidePagination: "server", //分页方式：client客户端分页，server服务端分页（*）
        pageNumber: 1, //初始化加载第一页，默认第一页
        pageSize: 10, //每页的记录行数（*）
        pageList: [10, 25, 50, 100], //可供选择的每页的行数（*）
        strictSearch: false,
        showColumns: false, //是否显示所有的列
        showRefresh: true, //是否显示刷新按钮
        minimumCountColumns: 2, //最少允许的列数
        clickToSelect: true, //是否启用点击选中行
        uniqueId: custRemind.unique, //每一行的唯一标识，一般为主键列
        showToggle: true, //是否显示详细视图和列表视图的切换按钮
        cardView: false, //是否显示详细视图
        detailView: false, //是否显示父子表
        columns: custRemind.columns(),
        responseHandler:custRemind.responseHandler
    });
};

custRemind.select = function (layerTips) {
    var rows = custRemind.table.bootstrapTable('getSelections');
    if (rows.length >0) {
   		custRemind.currentItem=rows;
        return true;
    } else {
        layerTips.msg("请选中一行");
        return false;
    }
};

custRemind.refresh = function () {
	custRemind.table.bootstrapTable('refresh', custRemind.queryParams());
};

layui.use('form', function(){
	   var layer = layui.layer;
	   var json = {//(字典枚举)筛选
	        "custType" : "Cust_Type",//客户类型
	    };
	    selectOnload({
	        "json" : json,
	        "isDefault" : false,
	        "length" : 1
	    });
});

layui.use(['form', 'layedit', 'laydate', 'element'], function () {	
	
	custRemind.init();
    var editIndex;
   //var layerTips = parent.layer === undefined ? layui.layer : parent.layer, //获取父窗口的layer对象
    var layerTips = layui.layer,
        layer = layui.layer, //获取当前窗口的layer对象
        form = layui.form,
        layedit = layui.layedit,
        laydate = layui.laydate;
    form.render();
    var addBoxIndex = -1;  
    //初始化页面上面的按钮事件
    $('#btn_query').on('click', function () {
    	custRemind.table.bootstrapTable('refresh', custRemind.queryParams());
    });
    $('#btn_screen').on('click', function () {
        if (addBoxIndex !== -1)
            return;
        var rows = custRemind.table.bootstrapTreeTable('getSelections');
        var id = "-1";
        if (rows.length == 1) {
            id = rows[0].id;
        }
    });
    
    $('#btn_remind').on('click', function () {
        if (custRemind.select(layerTips)) {
        	var result = custRemind.currentItem;
            $.get(custRemind.entity+'/remind', null, function (form) {
                var custNames ="";
                for(var i=0;i<result.length;i++){
                	custNames+= typeof(result[i].custName)=="undefined"?"无名,":result[i].custName+',';
                }
                custNames = custNames.substr(0,custNames.length-1);
                layer.open({
                    type: 1,
                    title: '批量提醒',
                    content: form,
                    btn: ['保存', '取消'],
                    id:'remind',
                    shade: false,
                    area: ['100%', '100%'],
                    maxmin: true,
                    yes: function (index) {
                        //触发表单的提交事件
                        layedit.sync(editIndex);
                        $('form.layui-form').find('button[lay-filter=edit]').click();
                    },
                    full: function (elem) {
                        var win = window.top === window.self ? window : parent.window;
                        $(win).on('resize', function () {
                            var $this = $(this);
                            elem.width($this.width()).height($this.height()).css({
                                top: 0,
                                left: 0
                            });
                            elem.children('div.layui-layer-content').height($this.height() - 95);
                        });
                    },
                    success: function (layero, index) {
                    	laydate.render({
                            elem: '#dateYm' //指定元素
                          });
                    	custRemind.currentItem = [];
                        var form = layui.form;
                        layero.find('#custNames').val(custNames);
                        editIndex = layedit.build('warnDesc',{
                       	 tool: [  'strong' //加粗
                    	          ,'italic' //斜体
                    	          ,'underline' //下划线
                    	          ,'del' //删除线
                    	          ,'|' //分割线
                    	          ,'left' //左对齐
                    	          ,'center' //居中对齐
                    	          ,'right' //右对齐
                    	       ]
                        });
                        form.render();
                        var newCustIds ="";
                        for(var i=0;i<result.length;i++){
                        	newCustIds += result[i].custId+',';
                        }
                        form.on('submit(edit)', function (data) {
                        	var custIds = newCustIds;
                        	var dateYm = $('#dateYm').val();
                        	var warnDesc = $('#warnDesc').val();
                        	if(warnDesc.length>=100){
                        		 layerTips.msg('提醒内容字数过多');
                        		 return false;
                        	}
                        	if(warnDesc.length == 0){
                        		layerTips.msg('提醒内容不能为空');
                        		return false;
                        	}
//                        	var warnFlag = $('#warnNum').val();
                        	var warnType = $('#warnType').val();
                        	var data0 = {'custIds':custIds,'dateYm':dateYm,'warnDesc':warnDesc,'warnType':warnType}
                        	$.ajax({
                                url: custRemind.baseUrl,
                                type: 'post',
                                contentType:"application/json",
                                data: JSON.stringify(data0),
                                dataType: "json",
                                success: function (data) {
                                	if(data.code=='200'){
                                        layerTips.msg('保存成功');
                                        layer.close(index);
                                        custRemind.table.bootstrapTable('refresh',custRemind.queryParams());
                                	}else{
                                		layerTips.msg(data.message);
                                		layer.close(index);
                                	}
                                }

                            });
                            //这里可以写ajax方法提交表单
                            return false; //阻止表单跳转。如果需要表单跳转，去掉这段即可。
                        });
                    }
                });
            });
        }
    }); 

    
});
