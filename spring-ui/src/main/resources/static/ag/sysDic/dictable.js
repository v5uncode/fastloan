var dicTable = {
    baseUrl: "/dic/sysDic",
    entity: "sysDic",
    tableId: "dicTable",
    //toolbarId: "treeBtn",
    parentid:"0",
    currentItem: {}
};
dicTable.columns = function () {
    return [{
        checkbox: true
    }, {
        field: 'dicId',
        title: 'Code ID'
    }, {
        field: 'dicName',
        title: '代码名'
    }, {
        field: 'dicSort',
        title: 'Sort'
    },{
        field: 'dicDesc',
        title: '中文备注'
    }
    ];
};
dicTable.queryParams = function (params) {
    return {
        parentId:sysDic.currentItem.dicId
    };

};
dicTable.init = function () {
	dicTable.table = $('#' + dicTable.tableId).bootstrapTable({
        url: dicTable.baseUrl +'/page', //请求后台的URL（*）
        method: 'get', //请求方式（*）
        //toolbar: '#' + dicTable.toolbarId, //工具按钮用哪个容器
        sortOrder: dicTable.order, //排序方式
        queryParams: dicTable.queryParams,//传递参数（*）
        striped: true, //是否显示行间隔色
        cache: false, //是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
        sortable: false, //是否启用排序
        search: false, //是否显示表格搜索，此搜索是客户端搜索，不会进服务端，所以，个人感觉意义不大
        strictSearch: false,
        showColumns: false, //是否显示所有的列
        minimumCountColumns: 2, //最少允许的列数
        clickToSelect: true, //是否启用点击选中行
        singleSelect:true,
        checkboxHeader: false,
        cardView: false, //是否显示详细视图
        detailView: false, //是否显示父子       
        columns: dicTable.columns()
    });
};
dicTable.select = function (layerTips) {
    var rows = dicTable.table.bootstrapTable('getSelections');
    if (rows.length == 1) {
    	dicTable.currentItem = rows[0];
        return true;
    } else {
        //layerTips.msg("请选中一行");
        return false;
    }
};

dicTable.refresh = function(){
    dicTable.table.bootstrapTable("refresh");
}
layui.use(['form', 'layedit', 'laydate'], function () {
	dicTable.init();
    var editIndex;
    var layerTips = parent.layer === undefined ? layui.layer : parent.layer, //获取父窗口的layer对象
        layer = layui.layer, //获取当前窗口的layer对象
        form = layui.form,
        layedit = layui.layedit,
        laydate = layui.laydate;
    var addBoxIndex = -1;
});