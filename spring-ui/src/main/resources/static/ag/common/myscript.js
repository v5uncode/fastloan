//select枚举值选项
function selectOnloadByParentId(settings){
	var defaultSetting = {
			json:{},//
			func:"",//回调函数名 string
			length:0,//json长度
			isDefault:true,//是否开启默认值
			isElse:true//联动默认值开启
	};
	$.extend(defaultSetting,settings);
	var json = defaultSetting.json,
		func = defaultSetting.func,
		length = defaultSetting.length,
		isDefault = defaultSetting.isDefault,
		isElse = defaultSetting.isElse;
	
	var n = 0;	
	for(var x in json){// id:val
		selectEva(x,json[x]);
	}
	function selectEva(id,val){	
		$.ajax({
			type:"get",
			url:"admin/"+val+"/dic",
			dataType:"json",
			contentType:"application/json",
			success:function(data){
				if(data.code=="200"){
					n++;
					var ids=$("#"+id);
					if(data.data!=null && data.data.length){
						var result=data.data;
						var html="";
						html+='<option value="">-请选择-</option>';
						for(var i=0;i<result.length;i++){
								html+='<option value="'+ result[i].dicId + '" >'+ result[i].dicName+'</option>';
						}
						ids.html(html);						
						$("#"+id).on("change",function(){
							var $this=$(this);
							var childVal=$this.val();
							var next=$this.next("select");
							var ida ={};
							if(next.size()>0){
								if(!next.attr("id")){
									next.attr("id",id+"2");
								}
								var id = next.attr("id");
								if(childVal==""){									
									return;
								}
								ida[id] = childVal;
								if(isElse){
									selectOnloadByParentId({"json":ida}); 
								}else{
									selectOnloadByParentId({"json":ida,"isDefault":false}); 
								}
																
							}
						});
					}else{
						ids.html("<option value=''>-请选择-</option>");
					}
					while(n>=length){
						if(length!="0"&&func){
							func();
							break;
						}else{
							break;
						}	
					}
				}
			}
		});
		
	}
}