var lrdLog = {
    baseUrl: "/log",
    entity: "lrdLog",
    tableId: "lrdLogTable",
    toolbarId: "toolbar",
    unique: "id",
    order: "asc",
    currentItem: {}
};
lrdLog.columns = function () {
    return [{  
        title: '序号',
        formatter: function (value, row, index) {  
            return index+1;  
        }  
    },{
    	field: 'corpCd',
        title: '法人号'
    },{
    	field: 'orgCd',
        title: '机构号'
    }, {
        field: 'menu',
        title: '菜单'
    }, {
        field: 'opt',
        title: '动作'
    }, {
        field: 'uri',
        title: 'uri'
    }, {
        field: 'crtName',
        title: '操作人'
    }, {
        field: 'crtTime',
        title: '操作时间'
    }, {
        field: 'crtHost',
        title: '操作IP'
    }];
};
lrdLog.queryParams = function (params) {
	console.info(params);
	if(!params){
		return {
	        orgCd:$("#orgCd").attr("orgCd"),
			crtUser:$("#crtUser").attr('userId'),
			crtTime:$("#crtTime").val(),
			endTime:$("#endTime").val()
		}
	};
    var temp = { //这里的键的名字和控制器的变量名必须一直，这边改动，控制器也需要改成一样的
        limit: params.limit, //页面大小
        page: params.offset/params.limit+1, //页码
        orgCd:$("#orgCd").attr("orgCd"),
		crtUser:$("#crtUser").val(),
		crtTime:$("#crtTime").val(),
		endTime:$("#endTime").val()
    };
    return temp;
};
lrdLog.responseHandler = function(response){
	if(response.code =='200'){
		return response.data;
	}
	return false;
};

lrdLog.init = function () {
	lrdLog.table = $('#' + lrdLog.tableId).bootstrapTable({
        url: lrdLog.baseUrl + '/page', //请求后台的URL（*）
        method: 'get', //请求方式（*）
        toolbar: '#' + lrdLog.toolbarId, //工具按钮用哪个容器
        striped: true, //是否显示行间隔色
        cache: false, //是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
        pagination: true, //是否显示分页（*）
        sortable: false, //是否启用排序
        sortOrder: lrdLog.order, //排序方式
        queryParams: lrdLog.queryParams,//传递参数（*）
        sidePagination: "server", //分页方式：client客户端分页，server服务端分页（*）
        pageNumber: 1, //初始化加载第一页，默认第一页
        pageSize: 10, //每页的记录行数（*）
        pageList: [10, 25, 50, 100], //可供选择的每页的行数（*）
        search: false, //是否显示表格搜索，此搜索是客户端搜索，不会进服务端，所以，个人感觉意义不大
        strictSearch: false,
        showColumns: false, //是否显示所有的列
        showRefresh: true, //是否显示刷新按钮
        minimumCountColumns: 2, //最少允许的列数
        clickToSelect: true, //是否启用点击选中行
        uniqueId: lrdLog.unique, //每一行的唯一标识，一般为主键列
        showToggle: true, //是否显示详细视图和列表视图的切换按钮
        cardView: false, //是否显示详细视图
        detailView: false, //是否显示父子表
        responseHandler:lrdLog.responseHandler,
        columns: lrdLog.columns()
    });
};
layui.use(['form', 'layedit', 'laydate'], function () {
	lrdLog.init();
	//var layerTips = parent.layer === undefined ? layui.layer : parent.layer, //获取父窗口的layer对象
	var layerTips = layui.layer;
	        layer = layui.layer, //获取当前窗口的layer对象
	        form = layui.form,
	        layedit = layui.layedit,
	        laydate = layui.laydate;
	$('#btn_query').on('click', function () {
		lrdLog.table.bootstrapTable('refresh', lrdLog.queryParams());
    });
	 $('#btn_clear').on('click',function(){
	    	$("#orgCd").removeAttr("orgCd");
	    	$("#orgCd").val('');
	    	$("#crtUser").removeAttr('userId');
	    	$("#crtUser").val('');
			$("#crtTime").val('');
			$("#endTime").val('');
	});
	 $('#btn_del').on('click', function () {
	        layer.confirm('确定删除数据吗？', null, function (index) {
	            $.ajax({
	                url: lrdLog.baseUrl,
	                type: "DELETE",
	                success: function (data) {
	                    if (data.code == '200') {
	                        layerTips.msg("移除成功！");
	                        lrdLog.table.bootstrapTable('refresh', lrdLog.queryParams());
	                    } else {
	                        layerTips.msg("移除失败！")	                        
	                    }
	                }
	            });
	            layer.close(index);
	        });
	    });
	 //时间控件
	$("#crtTime,#endTime").datetimepicker({language:'zh'});
	//机构查询
	var upviewOrgdata=function(){
		var setting = {
				data: {
					simpleData: {
						enable: true,
					},
				},
				callback: {
					beforeClick: function(treeId, treeNode) {
						var zTree = $.fn.zTree.getZTreeObj("tree");
						$("#orgCd").val(treeNode.name).attr("orgCd",treeNode.id);
						$("#myModal1").modal("hide");
						return true;
					}
				}
			};
		data2={"orgRelaType":"00"};
		$.ajax({
			type:"get",
			url:lrdLog.baseUrl+'/orgTree',
			dataType:"json",
			contentType:"application/json",
			data:data2,
			success:function(response){
				if(response.code=="200"){
					var org=response.data;
					$.fn.zTree.init($("#tree1"), setting, org);
				}else{
					layerTips.msg("加载失败");
				}
			}
		});
	}
	//操作员列表
	/*var upviewUserdata=function(){
		$.ajax({
			type:"get",
			url:lrdLog.baseUrl+'/user',
			dataType:"json",
			contentType:"application/json",
			success:function(response){
				if(response.code=="200"){
					var users=response.data;
					var html="";
					if(users!=null){
						for(var i=0;i<role.length;i++){
							var result = users[i];
							html+='<tr userId="'+result.userId+'"userName="'+result.userName+'">';
							html+='<td><input type="checkbox" name="checkBox" userName="'+result.userName+'"userId="'+result.userId+'"/></td>';
							html+='<td class="text-center">' + (i+1)+ '</td>';						
							html+='<td>' + result.userId + '</td>';						
							html+='<td>' + result.userName + '</td>';
							html+='</tr>';
						}
					}
					$("#tree2").html(html);
				}else{
					layerTips.msg("加载失败");
				}
			}
		});
	}*/
	$("#orgCdQuery").on("click",function(){
    	$("#orgCd").val("");
    	$("#myModal1").modal({
    		backdrop:false
    	});
    	upviewOrgdata();
    });
	//用户查询
    /*$("#userCdQuery").on("click",function(){
    	$("#userId").val("");
    	$("#myModa2").modal({
    		backdrop:false
    	});
    	upviewUserdata();
    });*/
  //选择带回
    $("#take_back").on("click",function(){
    	var checkboxs=$("[name=checkBox]:checked");
    	var roleStr="",roleNameStr="";
    	if(checkboxs.size()>0){
    		if(checkboxs.size()==1){
    			roleStr=checkboxs.attr("userId");
    			roleNameStr=checkboxs.attr("userName");
    			}else{
    				for(var i=0;i<checkboxs.size();i++){
    					if(i==checkboxs.size()-1){
    						roleStr+=checkboxs.eq(i).attr("userId");
    						roleNameStr+=checkboxs.eq(i).attr("userName");
    					}else{
    						roleStr+=checkboxs.eq(i).attr("userId")+",";
    						roleNameStr+=checkboxs.eq(i).attr("userName")+",";
    					}
    				}
    			}
    	$("#crtUser").val(roleNameStr).attr("userId",roleStr);
    	$("#myModa2").modal("hide");
    	}
    });
});

