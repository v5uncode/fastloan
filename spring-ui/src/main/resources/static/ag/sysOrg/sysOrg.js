var sysOrg = {
    baseUrl: "/lrdOrg/lrdOrg",
    sysId:"00",
    fucId:"020000",
};

layui.use(['form', 'layedit', 'laydate'], function () {
   // var layerTips = parent.layer === undefined ? layui.layer : parent.layer, //获取父窗口的layer对象
	var layerTips = layui.layer;
        layer = layui.layer, //获取当前窗口的layer对象
        form = layui.form,
        layedit = layui.layedit,
        laydate = layui.laydate;
    var addBoxIndex = -1;

    function renderForm(){
  	  layui.use('form', function(){
  	   var form = layui.form;
  	   form.render();
  	  });
    }
    function init(){
	    if(sysOrg.sysId!=''&&sysOrg.sysId!=null){
	    	$("#sysList a[zt='"+sysOrg.sysId+"']").parent("li").addClass("active");
	    	if($("#btnAdd").is(":visible")){
	    		$("#btnAdd").attr("sid",sysOrg.sysId);
	    		$("#btnAdd").attr("pid",sysOrg.fucId);
	    	}
	    	if($("#btnEdit").is(":visible")){
	    		$("#btnEdit").attr("sid",sysOrg.sysId);
	    		$("#btnEdit").attr("zt",sysOrg.fucId);
	    	}
	    	if($("#btnDel").is(":visible")){
	    		$("#btnDel").attr("sid",sysOrg.sysId);
	    		$("#btnDel").attr("zt",sysOrg.fucId);
	    	}
	    	treeWork(sysOrg.sysId,sysOrg.fucId);
	    }
    }
    $(".sysBtn").on("click",function(){
    	$(this).parent().addClass("active").siblings().removeClass("active");
    	var sid = $(this).attr("zt");
    	treeWork(sid);
    	if($("#btnAdd").is(":visible")){
    		$("#btnAdd").attr("sid",sid);
    		$("#btnAdd").attr("pid",'');
    	}
    	if($("#btnEdit").is(":visible")){
    		$("#btnEdit").attr("sid",sid);
    		$("#btnEdit").attr("zt",'');
    	}
    	if($("#btnDel").is(":visible")){
    		$("#btnDel").attr("sid",sid);
    		$("#btnDel").attr("zt",'');
    	}
    })
    $(".sysBtn").on("click",function(){
		$(this).parent().addClass("active").siblings().removeClass("active");
		var sid = $(this).attr("zt");
		treeWork(sid);
		if($("#btnAdd").is(":visible")){
			$("#btnAdd").attr("sid",sid);
			$("#btnAdd").attr("pid",'');
		}
		if($("#btnEdit").is(":visible")){
			$("#btnEdit").attr("sid",sid);
			$("#btnEdit").attr("zt",'');
		}
		if($("#btnDel").is(":visible")){
			$("#btnDel").attr("sid",sid);
			$("#btnDel").attr("zt",'');
		}
    })
    
  //添加功能
    $('#btnAdd').on('click', function () {
    	var sid = $(this).attr("sid");
    	if(typeof(sid)=="undefined"){
    		sid="00";
    	}
    	var id =$(this).attr("pid");
    	if(typeof(id)=="undefined"){
    		id="";
    	}
        //本表单通过ajax加载 --以模板的形式，当然你也可以直接写在页面上读取
        $.get('sysOrg/add', null, function (form) {
            var index  =layer.open({
                type: 1,
                title: '添加机构',
                content: form,
                btn: ['保存', '取消'],
                shade: false,
                id:'sysOrg',
                area: ['100%', '100%'],
                maxmin: true,
                yes: function (index) {
                    //触发表单的提交事件
                    $('form.layui-form').find('button[lay-filter=add]').click();
                },
                full: function (elem) {
                    var win = window.top === window.self ? window : parent.window;
                    $(win).on('resize', function () {
                        var $this = $(this);
                        elem.width($this.width()).height($this.height()).css({
                            top: 0,
                            left: 0
                        });
                        elem.children('div.layui-layer-content').height($this.height() - 95);
                    });
                },
                success: function (layero, index) {
                	//日期
                    $("#crtDate1,#mtnDate1").datepicker();
                	var corpCd1 = $("#corpCd").val();
                	var orgPcd1 =$("#orgCd").val();
                	var orgName1 = $("#orgName").val();
                	$("#corpCd1").val(corpCd1);
                	$("#orgPcd1").val(orgName1).attr("orgPcd1",orgPcd1);
                    var form = layui.form;
                    form.render();
                    //查询selec 状态
                    $.get('/admin/dic/SYS_ORG_FLAG', null, function (data) {
                    	var flag = data.data;
                    	for(var i=0;i<flag.length;i++ ){
                    		$("#orgFlag1").append("<option value="+flag[i].value+">"+flag[i].label+"</option>"); 
                    	}
                    	//重新渲染form
                    	renderForm();
                    });
                    
                    //查询selec 是否虚构
                    $.get('/admin/dic/SYS_ORG_VFLAG', null, function (data) {
                    	var flag = data.data;
                    	for(var i=0;i<flag.length;i++ ){
                    		$("#orgVflag1").append("<option value="+flag[i].value+">"+flag[i].label+"</option>"); 
                    	}
                    	//重新渲染form
                    	renderForm();
                    });
                    
                    form.on('submit(add)', function (data) {
                    	var corpCd1 =$('#corpCd1').val();
                    	var orgCd1 = $('#orgCd1').val();
                    	var orgName1 =  $('#orgName1').val();
                    	var orgPcd1 = $('#orgPcd1').attr("orgPcd1");
                    	var crtDate1 = $('#crtDate1').val();
                    	var mtnDate1 = $('#mtnDate1').val();
                    	var orgFlag1 = $('#orgFlag1').val();
                    	var orgVflag1 = $('#orgVflag1').val();
                    	var orgAbb1 =$('#orgAbb1').val();
                    	var orgDesc1 =$('#orgDesc1').val();
                    	var wdId1 =$('#wdId1').val();
                    	var data0 = {"corpCd":corpCd1,"orgCd":orgCd1,"orgPcd":orgPcd1,
                    			"crtDate":crtDate1,"mtnDate":mtnDate1,"orgFlag":orgFlag1,"orgAbb":orgAbb1,
                    			"orgDesc":orgDesc1,"wdId":wdId1,"orgVflag":orgVflag1,"orgName":orgName1,"orgRelaType":sid
                    	};
                 		$.ajax({
                 			url:sysOrg.baseUrl,
                 			type:"post",
                 			dataType:"json",
                 			contentType:"application/json",
                 			data:JSON.stringify(data0),
                 			success:function(data){
                 				if(data.code =='200'){
                     				layerTips.msg('添加成功');
	                                layerTips.close(index);
                                	sysOrg.sysId=sid;
                                	sysOrg.fucId=orgCd1;
                                	layer.close(index);
                 					init(); 
                 				}else{
                 					layerTips.msg(data.message);
	                                layerTips.close(index);
                 				}
                 			},
                 		})
                        return false; //阻止表单跳转。如果需要表单跳转，去掉这段即可。
                    });
                }
            });
        });
    });
  //编辑功能
    $('#btnEdit').on('click', function () {
        //本表单通过ajax加载 --以模板的形式，当然你也可以直接写在页面上读取
        var id = $(this).attr("zt");
        var sid = $(this).attr("sid");
    	if(sid!='' && typeof(sid)!="undefined" && id!='' && typeof(id)!="undefined"){
        	$.get('sysOrg/add', null, function (form) {
    	        var index  =layer.open({
    	            type: 1,
    	            title: '编辑用户',
    	            content: form,
    	            btn: ['保存', '取消'],
    	            shade: false,
    	            maxmin: true,
    	            id:'sysOrg',
                    area: ['100%', '100%'],
    	            yes: function (index) {
    	                //触发表单的提交事件
    	                $('form.layui-form').find('button[lay-filter=add]').click();
    	            },
    	            success: function (layero, index) {
    	            	$("#crtDate1,#mtnDate1").datepicker();
                    	var corpCd1 = $("#corpCd").val();
                    	var orgCd1 = $("#orgCd").val();
                    	var orgName1 = $("#orgName").val();
                    	var orgPcd1 = $("#orgPcd").val();
                    	var crtDate1 = $("#crtDate").val();
                    	var mtnDate1 = $("#mtnDate").val();
                    	var orgFlag1 =$("#orgFlag").attr('number');
                    	var orgVflag1 = $("#orgVflag").attr('number');
                    	var orgAbb1 = $("#orgAbb").val();
                    	var orgDesc1 = $("#orgDesc").val();
                    	var wdId1= $("#wdId").val();
                    	$("#corpCd1").val(corpCd1);
                      	$('#corpCd1').attr("disabled","disabled");
                    	$("#orgCd1").val(orgCd1);
                    	$('#orgCd1').attr("disabled","disabled");
                    	$("#orgName1").val(orgName1);
                    	$("#orgPcd1").val(orgPcd1);
                    	$('#orgPcd1').attr("disabled","disabled");
                    	$("#crtDate1").val(crtDate1);
                    	$("#mtnDate1").val(mtnDate1);
                    	$("#orgAbb1").val(orgAbb1);
                    	$("#orgDesc1").val(orgDesc1);
                    	$("#wdId1").val(wdId1);
    	                var form = layui.form;
    	                form.render();
                        //查询selec 状态
                        $.get('/admin/dic/SYS_ORG_FLAG', null, function (data) {
                        	var flag = data.data;
                        	for(var i=0;i<flag.length;i++ ){
                        		$("#orgFlag1").append("<option value="+flag[i].value+">"+flag[i].label+"</option>"); 
                        	}
                        	$("#orgFlag1").val(orgFlag1);
                        	//重新渲染form
                        	renderForm();
                        });
                        //查询selec 是否虚构
                        $.get('/admin/dic/SYS_ORG_VFLAG', null, function (data) {
                        	var flag = data.data;
                        	for(var i=0;i<flag.length;i++ ){
                        		$("#orgVflag1").append("<option value="+flag[i].value+">"+flag[i].label+"</option>"); 
                        	}
                        	 $("#orgVflag1").val(orgVflag1);
                        	//重新渲染form
                        	renderForm();
                        });
                        form.render('select');
    	                form.on('submit(add)', function (data) {
    	                	var corpCd1 =$("#corpCd1").val();
    	                	var orgCd1 = $("#orgCd1").val();
    	                	var orgName1 = $("#orgName1").val();
    	                	var orgPcd1 =$("#orgPcd1").val();
    	                	var crtDate1 =$("#crtDate1").val();
    	                	var mtnDate1 =$("#mtnDate1").val();
    	                	var orgFlag1 = $("#orgFlag1").val();
    	                	var orgVflag1 = $("#orgVflag1").val();
    	                	var orgAbb1= $("#orgAbb1").val();
    	                	var orgDesc1= $("#orgDesc1").val();
    	                	var wdId1= $("#wdId1").val();
                        	var data0 = {"corpCd":corpCd1,"orgCd":orgCd1,"orgPcd":orgPcd1,
                        			"crtDate":crtDate1,"mtnDate":mtnDate1,"orgFlag":orgFlag1,"orgAbb":orgAbb1,
                        			"orgDesc":orgDesc1,"wdId":wdId1,"orgVflag":orgVflag1,"orgName":orgName1,"orgRelaType":sid
                        	};
                     		$.ajax({
                     			url:sysOrg.baseUrl,
                     			type:"put",
                     			dataType:"json",
                     			contentType:"application/json",
                     			data:JSON.stringify(data0),
                     			success:function(data){
                     				if(data.code=='200'){
	                     				layerTips.msg('修改成功');
	                 					layer.close(index);
	                 					sysOrg.sysId=sid;
                                    	sysOrg.fucId=orgCd1;
	            	                	$('#corpCd1').attr("disabled","");
	            	                	$('#orgCd1').attr("disabled","");
	            	                	//location.reload();
	            	                	init();
                     				}else{
                     					layerTips.msg(data.message);
                     					layer.close(index);
                     					$('#corpCd1').attr("disabled","");
	            	                	$('#orgCd1').attr("disabled","");
	            	                	//location.reload();
                     				}
                     				
                     			},
                     		})

    	                    return false; //阻止表单跳转。如果需要表单跳转，去掉这段即可。
    	                });
    	            }
    	        });
    	    });
    	}else{
    		layerTips.msg("请选择一项");
    	}
    });
    $('#btnDel').on('click', function () {
        //本表单通过ajax加载 --以模板的形式，当然你也可以直接写在页面上读取
      	var id = $(this).attr("zt");
        var sid = $(this).attr("sid");
        var corpCd = $(this).attr("corpCd");
        if(sid!='' && typeof(sid)!="undefined" && id!='' && typeof(id)!="undefined"){
        	layer.confirm('确定删除数据吗？', null, function (index) {       	
            	var data0 = {"orgCd":id,"orgRelaType":sid,"corpCd":corpCd};
         		$.ajax({
         			url:sysOrg.baseUrl,
         			type:"delete",
         			dataType:"json",
         			contentType:"application/json",
         			data:JSON.stringify(data0),
         			success:function(data){
         				if(data.code=='200'){
         					layerTips.msg('删除成功');
         					treeWork(sid);
         					$("#btnAdd").attr("pid",'');
    						$("#btnEdit").attr("zt",'');
    						$("#btnDel").attr("zt",'');
    						$(".form-horizontal input").val('');
    						$("#treeTit").html('');	
         					//location.reload();
         				}else{
         					layerTips.msg(data.message);
         				}
         			},
         			
         		})
         		layer.close(index);
        	})
        }else{
    		layerTips.msg("请选择一项");
    	}
    });
    
});

function treeWork(id,x){
	var data0 = {"orgRelaType":id};
	$.ajax({
		type:"get",
		url:sysOrg.baseUrl,
		dataType:"json",
		contentType:"application/json",
		data:data0,
		success:function(data){
			if(data.code=="200"){
				if(data.data!=null && data.data.length !=0){
					var resultArr=data.data;
					$.fn.zTree.init($("#tree"), setting, resultArr);
					if(x!=''&&x!=null){
						var treeObj = $.fn.zTree.getZTreeObj("tree");
						var node = treeObj.getNodeByParam("id", x);
						treeObj.selectNode(node);
						treeCont(node);
					}
				}
			}
		}
	})
}

var setting = {
		data: {
			simpleData: {
				enable: true
			}
		},
		callback: {
			beforeClick: function(treeId, treeNode) {
				treeCont(treeNode);
				if($("#btnAdd").is(":visible")){
					$("#btnAdd").attr("pid",treeNode.id);
				}
				if($("#btnEdit").is(":visible")){
					$("#btnEdit").attr("zt",treeNode.id);
				}
				if($("#btnDel").is(":visible")){
					$("#btnDel").attr("zt",treeNode.id);
					$("#btnDel").attr("corpCd",treeNode.corpCd);
				}
			}
		}
	};
function treeCont(treeObj){
	$("#corpCd").val(treeObj.corpCd);
	$("#orgCd").val(treeObj.id);
	$("#orgName").val(treeObj.name);
	$("#orgPcd").val(treeObj.pId);
	$("#crtDate").val(treeObj.crtDate);
	$("#mtnDate").val(treeObj.mtnDate);
	$("#orgFlag").val(treeObj.orgFlagC);
	$("#orgFlag").attr('number',treeObj.orgFlag);
	$("#orgVflag").val(treeObj.orgVflagC);
	$("#orgVflag").attr('number',treeObj.orgVflag);
	$("#orgAbb").val(treeObj.orgAbb);
	$("#orgDesc").val(treeObj.orgDesc);
	$("#wdId").val(treeObj.wdId);
};


