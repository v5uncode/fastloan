layui.laytpl.toDateString = function(d, format){
    var date = new Date(d || new Date())
        ,ymd = [
      this.digit(date.getFullYear(), 4)
      ,this.digit(date.getMonth() + 1)
      ,this.digit(date.getDate())
    ]
        ,hms = [
      this.digit(date.getHours())
      ,this.digit(date.getMinutes())
      ,this.digit(date.getSeconds())
    ];

    format = format || 'yyyy-MM-dd HH:mm:ss';

    return format.replace(/yyyy/g, ymd[0])
    .replace(/MM/g, ymd[1])
    .replace(/dd/g, ymd[2])
    .replace(/HH/g, hms[0])
    .replace(/mm/g, hms[1])
    .replace(/ss/g, hms[2]);
};

//数字前置补零
layui.laytpl.digit = function(num, length, end){
    var str = '';
    num = String(num);
    length = length || 2;
    for(var i = num.length; i < length; i++){
      str += '0';
    }
    return num < Math.pow(10, length) ? str + (num|0) : num;
};

document.onkeydown = function (e) { // 回车提交表单
    var theEvent = window.event || e;
    var code = theEvent.keyCode || theEvent.which;
    if (code == 13) {
      $(".select .select-on").click();
    }
}

//通用
function popup(title, url, w, h,id) {
  if (title == null || title == '') {
    title = false;
  };
  
  if (url == null || url == '') {
    url = "error/404";
  };
  
  if (w == null || w == '') {
    w = ($(window).width() * 0.9);
  };
  
  if (h == null || h == '') {
    h = ($(window).height() - 50);
  };
  if(w.indexOf('%') == -1){
	  w = w + 'px';
  };
  if(h.indexOf('%') == -1){
	  h = h + 'px';
  }
  layer.open({
    id: id,
    type: 2,
    area: [w, h],
    fix: false,
    maxmin: true,  //最大最小化
    scrollbar: false, //是否允许浏览器出现滚动条
    shadeClose: true, //是否点击遮罩关闭
    shade: 0.4,
    title: title,
    content: url
  });
}

/**
 * 父窗口弹出
 * @param url
 * @param data
 * @param tableId
 */
function postAjaxre(url,data,tableId){
  $.ajax({
    url: url,
    type: "post",
    data:data,
    dataType: "json", traditional: true,
    success: function (data) {
      if(data.flag){
        var index = parent.layer.getFrameIndex(window.name);
        parent.layer.close(index);
        window.parent.layui.table.reload(tableId);
        window.top.layer.msg(data.msg,{icon:6,offset: 'rb',area:['120px','80px'],anim:2});
      }else{
        layer.msg(data.msg,{icon:5,offset: 'rb',area:['120px','80px'],anim:2});
      }
    }
  });
}

function layerAjax(url,data,tableId){
  $.ajax({
    url:url,
    type:'post',
    data:data,
    traditional: true,
    success:function(d){
      var index = parent.layer.getFrameIndex(window.name);
      if(d.code==200){
        parent.layer.close(index);
        window.parent.layui.table.reload(tableId);
        window.parent.layer.msg(d.message,{icon:6,offset: 'rb',area:['200px','80px'],anim:2});
      }else{
        layer.msg(d.message,{icon:5});
      }
    },error:function(){
      layer.alert("请求失败", {icon: 6},function () {
        var index = parent.layer.getFrameIndex(window.name);
        parent.layer.close(index);
      });
    }
  });
}
//子iframe内调用
function childLayerAjax(url,data,tableId){
  $.ajax({
    url:url,
    type:'post',
    data:data,
    traditional: true,
    success:function(d){
      if(d.code==200){
    	$("#close" , parent.document).click();//调用父页面关闭
        window.parent.parent.layui.table.reload(tableId);
        window.parent.parent.layer.msg(d.message,{icon:6,offset: 'rb',area:['200px','80px'],anim:2});
      }else{
        layer.msg(d.message,{icon:5});
      }
    },error:function(){
      layer.alert("请求失败", {icon: 6},function () {
    	  $("#close" , parent.document).click();//调用父页面关闭
      });
    }
  });
}

function eleClick(active,ele){
    $(ele).on('click', function () {
    var type = $(this).data('type');
    active[type] ? active[type].call(this) : '';
  });
  
}