//获取路径参数
getUrlParam = function(name)
{
    var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)");
    var r = window.location.search.substr(1).match(reg);
    if (r!=null) return decodeURI(r[2]); return null;
};
var yunStyle = (function() {
	var config = [];
	//初始化函数
	var init = function(dataUrl) {
		setStyle();
		if(ifnull(dataUrl)) return false;
		$.get(dataUrl, function(data) {
			
			if(data.code=="200"){
				var result=data.data;
				setVal(result);
			}			
		})
	}
	//判空函数
	var ifnull = function(code) {
		if(code instanceof Array) {
			if(code.length == 0) {
				return true;
			}
		} else if(code instanceof Object) {
			if(!code) {
				return true;
			}
		} else {
			if(code == 'undefined' || code == undefined || code == '' || code == 'null' || code == null) {
				return true;
			}
		}
		return false;

	};
	//赋值值
	var setVal = function(data) {
		for(var i = 0; i < data.length; i++) {
			var obj = document.getElementById(data[i].id);
			if(ifnull(obj)) continue;
			//值赋值
			if(!ifnull(data[i].value)) obj.value = data[i].value;
		}
	}
	//赋值函数
	var setStyle = function(id) {
		var objlist = document.getElementsByTagName('input');
		for(var i=0;i<objlist.length;i++){
			var obj=objlist[i];
			if(ifnull(obj)) return false;
			var data = obj.dataset;
			var styles = {
				width: data.width,
			}
			var params = {
				disabled: data.disabled,
			}
			//样式赋值
			for(var o in styles) {
				if(!ifnull(styles[o])) obj.style[o] = styles[o];
			}
			//是否禁止输入
			if(params.disabled == 'disabled') obj.disabled = 'disabled';
		}
		

	}

	return {
		init: init,
	}
})();
function myprint(){	
	window.print(); 
}