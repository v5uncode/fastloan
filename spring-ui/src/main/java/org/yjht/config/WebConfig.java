package org.yjht.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
public class WebConfig extends WebMvcConfigurerAdapter {
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
    	registry.addResourceHandler("/plugin/**")  //对外暴漏的地址
        .addResourceLocations("classpath:/plugin/","classpath:/static/*"); //文件放置的目录
    	//registry.addResourceHandler("/ftl/**").addResourceLocations("classpath:/ftl/");
    	super.addResourceHandlers(registry);
    }
}
