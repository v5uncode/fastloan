package org.yjht.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.yjht.core.Constants;

@Controller
@RequestMapping("")
public class HomeController {   
	
    @RequestMapping(value = "pjcs",method = RequestMethod.GET)
    public String process(){
        return "rule/pjcs";
    }
    
    @RequestMapping(value = "scsp",method = RequestMethod.GET)
    public String scsp(){
        return "scsp/list";
    }
    //审查审批结果弹出
    @RequestMapping(value = "scsp/result",method = RequestMethod.GET)
    public String result(){
        return "scsp/result";
    } 
    //调查报告
    @RequestMapping(value = "scsp/bg",method = RequestMethod.GET)
    public String bg(){
        return "scsp/pact/pact";
    } 
    //审查审批表
    @RequestMapping(value = "scsp/scspTable",method = RequestMethod.GET)
    public String scspTable(){
        return "scsp/pact/pact";
    }
    //具体报告
    @RequestMapping(value = "scsp/bg/detail",method = RequestMethod.GET)
    public String bgDetail(String zyxm){
    	String result="";
    	if(zyxm.equals("01")||zyxm.equals("02")||zyxm.equals("03")) {
    		result="scsp/nhzz";
    	}else if(zyxm.equals("04")) {
    		result="scsp/nhyz";
    	}else if(zyxm.equals("05")) {
    		result="scsp/dkdcbg_jm";
    	}else {
    		result="scsp/dkdcbg_jy";
    	}
        return result;
    } 
   
    //抵质押添加修改弹出框
    @RequestMapping(value = "scsp/bg/nhzzadd",method = RequestMethod.GET)
    public String nhzzadd(String custType,String value){
    	return "scsp/nhzz_add";
    }
    //工作平台
    @RequestMapping(value = "schedule",method = RequestMethod.GET)
    public String schedule(){
        return "schedule/list";
    }
   //批量提醒
    @RequestMapping(value = "custRemind",method = RequestMethod.GET)
    public String custRemind(){
        return "custRemind/list";
    } 
    @RequestMapping(value = "custRemind/screen",method = RequestMethod.GET)
    public String custRemindScreen(){
    	return "custRemind/screen";
    } 
    @RequestMapping(value = "custRemind/remind",method = RequestMethod.GET)
    public String custRemindRemind(){
    	return "custRemind/remind";
    } 
    //规章制度
    @RequestMapping(value = "ruleDef",method = RequestMethod.GET)
    public String ruleDef(){
        return "ruleDef/list";
    }
    @RequestMapping(value = "ruleDef/edit",method = RequestMethod.GET)
    public String ruleDefEdit(){
    	return "ruleDef/edit";
    }
    @RequestMapping(value = "ruleDef/detail",method = RequestMethod.GET)
    public String ruleDefDeail(){
    	return "ruleDef/detail";
    }
    //统计报表
    @RequestMapping(value = "custStatistics",method = RequestMethod.GET)
    public String custInputSum(){
    	return "custStatistics/list";
    }
    //客户统计
    @RequestMapping(value = "custStatistics/listCount",method = RequestMethod.GET)
    public String listCount(){
    	return "custStatistics/listCount";
    }
    
    //客户移交
    @RequestMapping(value = "handover",method = RequestMethod.GET)
    public String handoverList(){
    	return "handover/list";
    }
    //移交详情
    @RequestMapping(value = "handover/receive",method = RequestMethod.GET)
    public String handoverReceive(){
    	return "handover/receive";
    }
    //拒绝详情
    @RequestMapping(value = "handover/refuse",method = RequestMethod.GET)
    public String handoverRefuse(){
    	return "handover/refuse";
    }
    //贷后借据列表
    @RequestMapping(value = "bond",method = RequestMethod.GET)
    public String bond(){
    	return "postLoan/bond/list";
    }
    //客户管理
    @RequestMapping(value = "custBase",method = RequestMethod.GET)
    public String custBase(){
    	return "postLoan/custBase/list";
    }
    
    @RequestMapping(value = "custBase/add",method = RequestMethod.GET)
    public String custBaseAdd(){
    	return "postLoan/custBase/add";
    }
    @RequestMapping(value = "custBase/addList",method = RequestMethod.GET)
    public String addListAddList(){
    	return "postLoan/custBase/addList";
    }
    //合同管理
    @RequestMapping(value = "contract",method = RequestMethod.GET)
    public String contractList(){
    		return "postLoan/contract/list";
    }
    //借据弹窗
    @RequestMapping(value = "contract/bondBase",method = RequestMethod.GET)
    public String bondBase(){
    	return "postLoan/contract/bondBase";
    }
    //公司贷后检查表管理
    @RequestMapping(value = "checkTable",method = RequestMethod.GET)
    public String checkTableList(){
    	return "postLoan/checkTable/list";
    }
    //公司贷后历史表管理
    @RequestMapping(value = "historyTable",method = RequestMethod.GET)
    public String historyTableList(){
    	return "postLoan/historyTable/list";
    }
    //公司贷后检查表
    @RequestMapping(value = "compangCheck/gs_table",method = RequestMethod.GET)
    public String checkGsTable(){
    	return "postLoan/loanTable/gs_table";
    }   
    
    //公司table
    @RequestMapping(value = "loanTable/gsTable",method = RequestMethod.GET)
    public String gsTable(){
        return "postLoan/loanTable/pact";
    }
    //具体公司Table
    @RequestMapping(value = "loanTable/gsTable/detail",method = RequestMethod.GET)
    public String gsTableDetail(String type){
    	String result="";
    	if(type.equals("01")) {
    		result="postLoan/loanTable/gs_table";
    	}
        return result;
    } 
    //个人table
    @RequestMapping(value = "loanTable/grTable",method = RequestMethod.GET)
    public String grTable(){
        return "postLoan/loanTable/pact";
    }
    //具体个人table
    @RequestMapping(value = "loanTable/grTable/detail",method = RequestMethod.GET)
    public String grTableDetail(String type){
    	String result = "";
     	if(type.contains("MORTGAGE")) {
     		result="postLoan/loanTable/aj_table";
    	}else if(type.contains("TOWNSFOLK")) {
    		result="postLoan/loanTable/gzorjm_table";
    	}else if(type.contains("FARMER")){
    		result="postLoan/loanTable/peasan_table";
    	}else if(type.contains("BUSINESS")) {
    		result="postLoan/loanTable/grjy_table";
    	}else{
    		result="postLoan/loanTable/grjy_bzr_table";
    	}
        return result;
    }
    //保证人
    @RequestMapping(value = "postLoan/esnsure",method = RequestMethod.GET)
    public String ensureList(){
    	return "postLoan/checkTable/userinfo";
    }
    //个人历史table
    @RequestMapping(value = "loanTable/grHisTable",method = RequestMethod.GET)
    public String grHisTable(){
        return "postLoan/loanHistoryTable/pact";
    }
    //具体个人table
    @RequestMapping(value = "loanTable/grHisTable/detail",method = RequestMethod.GET)
    public String grHisTableDetail(String type){
    	String result = "";
     	if(type.contains("MORTGAGE")) {
     		result="postLoan/loanHistoryTable/aj_his_table";
    	}
        return result;
    }

    //调查报告

    /**
     * 调查报告
     * 根据主营项目跳转页面
     * @param zyxm
     * @return
     */
    @RequestMapping(value = "/survey",method = RequestMethod.GET)
    public String researchReport(String zyxm){
        String result = "";
        if (zyxm.equals(Constants.MAIN05)) {
            //职工
            result="scsp/staffReport";
        } else if (zyxm.equals(Constants.MAIN01) || zyxm.equals(Constants.MAIN02)) {
            //种植情况
            result="scsp/plantReport";
        } else if (zyxm.equals(Constants.MAIN03) || zyxm.equals(Constants.MAIN04)) {
            //养殖情况
            result="scsp/breedReport";
        } else {
            //经营
            result="scsp/manageReport";
        }
        return result;
    }

    //授信方案
    @RequestMapping(value = "credit",method = RequestMethod.GET)
    public String creditScheme(){
        return "scsp/creditProgram";
    }
    //审查审批表
    @RequestMapping(value = "approval",method = RequestMethod.GET)
    public String approval(){
        return "scsp/reviewApproval";
    }
}
