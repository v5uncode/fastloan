package org.yjht;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.yjht.util.DBLog;
import lombok.extern.slf4j.Slf4j;

@SpringBootApplication
@ServletComponentScan
@MapperScan({"org.yjht.dao","org.yjht.mapper","org.yjht.wechat.mapper"})
@Slf4j
public class UIBootstrap {
    public static void main(String[] args) {
    	DBLog.getInstance().start(); 
    	SpringApplication.run(UIBootstrap.class, args);
        log.info("spring boot start");  
    }
}