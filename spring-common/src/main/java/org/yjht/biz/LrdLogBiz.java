package org.yjht.biz;

import org.springframework.stereotype.Service;
import org.yjht.bean.LrdLog;
import org.yjht.mapper.LrdLogMapper;

@Service
public class LrdLogBiz extends BaseBiz<LrdLogMapper,LrdLog> {

    @Override
    public void insert(LrdLog entity) {
        mapper.insert(entity);
    }

    @Override
    public void insertSelective(LrdLog entity) {
        mapper.insertSelective(entity);
    }
    
    public void deleteAll() {
        mapper.deleteAll();
    }
    
}
