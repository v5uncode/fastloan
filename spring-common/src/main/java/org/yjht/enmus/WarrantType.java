package org.yjht.enmus;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum WarrantType {
	XY("01","信用"),
	BZ("02","保证"),
	DY("03","抵押"),
	ZY("04","质押");
	@Getter
	private String code;
	@Getter
	private String desc;
	

}
