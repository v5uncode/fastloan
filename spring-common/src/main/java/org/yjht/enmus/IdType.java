package org.yjht.enmus;

import org.yjht.exception.MyException;

public enum IdType {
	IDCARD("身份证","1"),
	POSSPORT("护照","2"),
	JRZ("军人证","3"),
	WJZ("武警证","4"),
	GAPOSSPORT("港澳居民来往内地通行证","5"),
	RESIDENCE("户口簿","6"),
	OTHER("其他","7"),
	JGZ("警官证","8"),
	ZXGWZ("执行公务证","9"),
	SBZ("士兵证","A"),
	TWPOSSPORT("台湾同胞来往内地通行证","B"),
	TEMIDCARD("临时身份证","C"),
	WGRJZZ("外国人居留证","D");
	/** 
	   * 活动状态的值。 
	   */
	private String value; 
	/** 
	 * 活动状态的中文描述。 
	 */
	private String code; 
	/** 
	 * @param status 活动状态的值 
	 * @param desc 活动状态的中文描述 
	 */
	private IdType(String value, String code) { 
		this.value = value; 
	    this.code = code; 
	} 
	/** 
	 * @return 当前枚举对象的值。 
	 */
	public String getValue() { 
	    return value; 
	} 
	/** 
	 * @return 当前状态的中文描述。 
	 */
	public String getCode() { 
	    return code; 
	} 
	/** 
	 * 根据活动状态的值获取枚举对象。 
	 * 
	 * @param status 活动状态的值 
	 * @return 枚举对象 
	 */
	public static String getCode(String status) { 
		IdType[] allStatus = IdType.values(); 
	    for (IdType ws : allStatus) { 
	      if (ws.getValue().equalsIgnoreCase(status)) { 
	        return ws.getCode(); 
	      } 
	    } 
	    throw new MyException("status值非法，没有符合关系类型的枚举对象"); 
	} 
	

}
