package org.yjht.thread;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.yjht.core.Result;
import org.yjht.service.SysDicService;

@Service
public class AsyncTaskServcie {
	
	@Autowired
	SysDicService SysDicService;
	@Async
	public Result executeAsyncTaskPlus(String parentId){		
		return SysDicService.finSysDicByParentId(parentId);
	}
	
}
