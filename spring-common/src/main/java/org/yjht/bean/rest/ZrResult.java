package org.yjht.bean.rest;

public class ZrResult {
	private String code;//返回编号  000000为正常，其余为异常
	
	private String desc;//描述
	
	private String token;//暂时无用
	
	private String haveCreditInfo;//是否有征信信息（暂无)
	
	private String admitResult;//准入结果  0:通过    1:拒绝
	
	private String rateResult;//评级结果
	
	private String limitAmountResult;//额度结果
	
	private String dayRate;//日利率
	
	private String tmpResult;//暂时无用
	
	private String limit; //期限（暂未使用）
	
	private String productCode;//产口编号

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getHaveCreditInfo() {
		return haveCreditInfo;
	}

	public void setHaveCreditInfo(String haveCreditInfo) {
		this.haveCreditInfo = haveCreditInfo;
	}

	public String getAdmitResult() {
		return admitResult;
	}

	public void setAdmitResult(String admitResult) {
		this.admitResult = admitResult;
	}

	public String getRateResult() {
		return rateResult;
	}

	public void setRateResult(String rateResult) {
		this.rateResult = rateResult;
	}

	public String getLimitAmountResult() {
		return limitAmountResult;
	}

	public void setLimitAmountResult(String limitAmountResult) {
		this.limitAmountResult = limitAmountResult;
	}

	public String getDayRate() {
		return dayRate;
	}

	public void setDayRate(String dayRate) {
		this.dayRate = dayRate;
	}

	public String getTmpResult() {
		return tmpResult;
	}

	public void setTmpResult(String tmpResult) {
		this.tmpResult = tmpResult;
	}

	public String getLimit() {
		return limit;
	}

	public void setLimit(String limit) {
		this.limit = limit;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	
	

}
