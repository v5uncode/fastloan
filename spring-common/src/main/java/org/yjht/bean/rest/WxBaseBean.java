package org.yjht.bean.rest;
/**
 * 微信请求基础对象
 * @author lenovo
 *
 */
public class WxBaseBean {
	
	/*
	 * 对象类型
	 */
	private String modelType;
	
	/*
	 *客户身份证号码
	 */
	private String idNo;
	
	/*
	 *客户姓名
	 */
	private String name;
	
	/*
	 * 客户经理
	 */
	private String managerId;
	
	/*
	 * 处理时间
	 */
	private String dealTime;
	

	public String getModelType() {
		return modelType;
	}

	public void setModelType(String modelType) {
		this.modelType = modelType;
	}

	public String getIdNo() {
		return idNo;
	}

	public void setIdNo(String idNo) {
		this.idNo = idNo;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getManagerId() {
		return managerId;
	}

	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}

	public String getDealTime() {
		return dealTime;
	}

	public void setDealTime(String dealTime) {
		this.dealTime = dealTime;
	}

	

}
