package org.yjht.bean.rest;

import org.yjht.enmus.IdType;

public class ZrParam {
	
	private String custname;//客户姓名
	 
	private String idtype=IdType.IDCARD.getCode();//证件类型   默认身份证
	 
	private String idno;//证件号码

	public String getCustname() {
		return custname;
	}

	public void setCustname(String custname) {
		this.custname = custname;
	}

	public String getIdtype() {
		return idtype;
	}

	public void setIdtype(String idtype) {
		this.idtype = idtype;
	}

	public String getIdno() {
		return idno;
	}

	public void setIdno(String idno) {
		this.idno = idno;
	}
}
