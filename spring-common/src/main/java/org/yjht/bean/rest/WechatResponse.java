package org.yjht.bean.rest;

public class WechatResponse {
	/**
	 * 运算是否异常
	 */
	private Boolean isException;
	/**
	 * 异常信息
	 */
	private String exceptionInfo;
	/**
	 * 是否通过
	 */
	private Boolean isPass;
	/**
	 * 不通过原因
	 */
	private String notPassReason;
	
	public Boolean getIsException() {
		return isException;
	}
	public void setIsException(Boolean isException) {
		this.isException = isException;
	}
	public String getExceptionInfo() {
		return exceptionInfo;
	}
	public void setExceptionInfo(String exceptionInfo) {
		this.exceptionInfo = exceptionInfo;
	}
	public Boolean getIsPass() {
		return isPass;
	}
	public void setIsPass(Boolean isPass) {
		this.isPass = isPass;
	}
	public String getNotPassReason() {
		return notPassReason;
	}
	public void setNotPassReason(String notPassReason) {
		this.notPassReason = notPassReason;
	}
	
}
