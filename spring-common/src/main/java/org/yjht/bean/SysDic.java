package org.yjht.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.*;

@Table(name = "SYS_DIC")
public class SysDic {
	@Column(name = "DIC_ID")
    @Id
    private String dicId;

    @Column(name = "DIC_NAME")
    private String dicName;

    @Column(name = "DIC_PARENTID")
    @Id
    private String dicParentid;

    @Column(name = "DIC_DESC")
    private String dicDesc;

    @Column(name = "DIC_ISENABLE")
    private String dicIsenable;

    @Column(name = "DIC_SORT")
    private BigDecimal dicSort;

    @Column(name = "CRT_DATE")
    private String crtDate;

    @Column(name = "MTN_DATE")
    private String mtnDate;

    @Column(name = "CORP_CD")
    private String corpCd;

    @Column(name = "ISLEAF")
    private String isleaf;
    
    @Column(name = "CLASSID")
    private String classId;

    /**
     * @return DIC_ID
     */
    public String getDicId() {
        return dicId;
    }

    /**
     * @param dicId
     */
    public void setDicId(String dicId) {
        this.dicId = dicId == null ? null : dicId.trim();
    }

    /**
     * @return DIC_NAME
     */
    public String getDicName() {
        return dicName;
    }

    /**
     * @param dicName
     */
    public void setDicName(String dicName) {
        this.dicName = dicName == null ? null : dicName.trim();
    }

    /**
     * @return DIC_PARENTID
     */
    public String getDicParentid() {
        return dicParentid;
    }

    /**
     * @param dicParentid
     */
    public void setDicParentid(String dicParentid) {
        this.dicParentid = dicParentid == null ? null : dicParentid.trim();
    }

    /**
     * @return DIC_DESC
     */
    public String getDicDesc() {
        return dicDesc;
    }

    /**
     * @param dicDesc
     */
    public void setDicDesc(String dicDesc) {
        this.dicDesc = dicDesc == null ? null : dicDesc.trim();
    }

    /**
     * @return DIC_ISENABLE
     */
    public String getDicIsenable() {
        return dicIsenable;
    }

    /**
     * @param dicIsenable
     */
    public void setDicIsenable(String dicIsenable) {
        this.dicIsenable = dicIsenable == null ? null : dicIsenable.trim();
    }

    /**
     * @return DIC_SORT
     */
    public BigDecimal getDicSort() {
        return dicSort;
    }

    /**
     * @param dicSort
     */
    public void setDicSort(BigDecimal dicSort) {
        this.dicSort = dicSort;
    }

    /**
     * @return CRT_DATE
     */
    public String getCrtDate() {
        return crtDate;
    }

    /**
     * @param crtDate
     */
    public void setCrtDate(String crtDate) {
        this.crtDate = crtDate == null ? null : crtDate.trim();
    }

    /**
     * @return MTN_DATE
     */
    public String getMtnDate() {
        return mtnDate;
    }

    /**
     * @param mtnDate
     */
    public void setMtnDate(String mtnDate) {
        this.mtnDate = mtnDate == null ? null : mtnDate.trim();
    }

    /**
     * @return CORP_CD
     */
    public String getCorpCd() {
        return corpCd;
    }

    /**
     * @param corpCd
     */
    public void setCorpCd(String corpCd) {
        this.corpCd = corpCd == null ? null : corpCd.trim();
    }

    /**
     * @return ISLEAF
     */
    public String getIsleaf() {
        return isleaf;
    }

    /**
     * @param isleaf
     */
    public void setIsleaf(String isleaf) {
        this.isleaf = isleaf == null ? null : isleaf.trim();
    }
    /**
     * @return classId
     */

	public String getClassId() {
		return classId;
	}
    /**
     * @param classId
     */
	public void setClassId(String classId) {
		 this.classId = classId == null ? null : classId.trim();
	}

	@Override
	public String toString() {
		return "SysDic [dicId=" + dicId + ", dicName=" + dicName + ", dicParentid=" + dicParentid + ", dicDesc="
				+ dicDesc + ", dicIsenable=" + dicIsenable + ", dicSort=" + dicSort + ", crtDate=" + crtDate
				+ ", mtnDate=" + mtnDate + ", corpCd=" + corpCd + ", isleaf=" + isleaf + ", classId=" + classId + "]";
	}
	
    
}