package org.yjht.bean;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import javax.persistence.*;

@Table(name = "lrd_log")
public class LrdLog {
    /**
     * 序号
     */
    @Id
    @Column(name = "ID")
    private Integer id;

    /**
     * 菜单
     */
    @Column(name = "MENU")
    private String menu;

    /**
     * 操作
     */
    @Column(name = "OPT")
    private String opt;

    /**
     * 地址
     */
    @Column(name = "URI")
    private String uri;

    /**
     * 创建时间
     */
    @Column(name = "CRT_TIME")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm",timezone = "GMT+8")
    private Date crtTime;

    /**
     * 操作人编号
     */
    @Column(name = "CRT_USER")
    private String crtUser;

    /**
     * 操作人姓名
     */
    @Column(name = "CRT_NAME")
    private String crtName;

    /**
     * 操作人IP
     */
    @Column(name = "CRT_HOST")
    private String crtHost;

    /**
     * 操作人法人号
     */
    @Column(name = "CORP_CD")
    private String corpCd;

    /**
     * 操作人机构号
     */
    @Column(name = "ORG_CD")
    private String orgCd;

    @Transient    //非数据库表字段
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
    private Date endTime; //查询条件结束时间

    /**
     * 获取序号
     *
     * @return ID - 序号
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置序号
     *
     * @param id 序号
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取菜单
     *
     * @return MENU - 菜单
     */
    public String getMenu() {
        return menu;
    }

    /**
     * 设置菜单
     *
     * @param menu 菜单
     */
    public void setMenu(String menu) {
        this.menu = menu == null ? null : menu.trim();
    }

    /**
     * 获取操作
     *
     * @return OPT - 操作
     */
    public String getOpt() {
        return opt;
    }

    /**
     * 设置操作
     *
     * @param opt 操作
     */
    public void setOpt(String opt) {
        this.opt = opt == null ? null : opt.trim();
    }

    /**
     * 获取地址
     *
     * @return URI - 地址
     */
    public String getUri() {
        return uri;
    }

    /**
     * 设置地址
     *
     * @param uri 地址
     */
    public void setUri(String uri) {
        this.uri = uri == null ? null : uri.trim();
    }

    /**
     * 获取创建时间
     *
     * @return CRT_TIME - 创建时间
     */

    public Date getCrtTime() {
        return crtTime;
    }

    /**
     * 设置创建时间
     *
     * @param crtTime 创建时间
     */

    public void setCrtTime(Date crtTime) {
        this.crtTime = crtTime;
    }

    /**
     * 获取操作人编号
     *
     * @return CRT_USER - 操作人编号
     */
    public String getCrtUser() {
        return crtUser;
    }

    /**
     * 设置操作人编号
     *
     * @param crtUser 操作人编号
     */
    public void setCrtUser(String crtUser) {
        this.crtUser = crtUser == null ? null : crtUser.trim();
    }

    /**
     * 获取操作人姓名
     *
     * @return CRT_NAME - 操作人姓名
     */
    public String getCrtName() {
        return crtName;
    }

    /**
     * 设置操作人姓名
     *
     * @param crtName 操作人姓名
     */
    public void setCrtName(String crtName) {
        this.crtName = crtName == null ? null : crtName.trim();
    }

    /**
     * 获取操作人IP
     *
     * @return CRT_HOST - 操作人IP
     */
    public String getCrtHost() {
        return crtHost;
    }

    /**
     * 设置操作人IP
     *
     * @param crtHost 操作人IP
     */
    public void setCrtHost(String crtHost) {
        this.crtHost = crtHost == null ? null : crtHost.trim();
    }

    /**
     * 获取操作人法人号
     *
     * @return CORP_CD - 操作人法人号
     */
    public String getCorpCd() {
        return corpCd;
    }

    /**
     * 设置操作人法人号
     *
     * @param corpCd 操作人法人号
     */
    public void setCorpCd(String corpCd) {
        this.corpCd = corpCd == null ? null : corpCd.trim();
    }

    /**
     * 获取操作人机构号
     *
     * @return ORG_CD - 操作人机构号
     */
    public String getOrgCd() {
        return orgCd;
    }

    /**
     * 设置操作人机构号
     *
     * @param orgCd 操作人机构号
     */
    public void setOrgCd(String orgCd) {
        this.orgCd = orgCd == null ? null : orgCd.trim();
    }

    public Date getEndTime() {
        return endTime;
    }


    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }
}