package org.yjht.bean;

import javax.persistence.*;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
@Setter
@Getter
@ToString
@Table(name = "front_info_scsp")
public class FrontInfoScsp{

	/**
     * 审查审批ID
     */
    @Id
    @Column(name = "SCSP_ID")
    private String scspId;

    /**
     * 客户编号
     */
    @Column(name = "CUST_ID")
    private String custId;
    
    /**
     * 证件类型
     */
    @Column(name = "ID_TYPE")
    private String idType;
    
    /**
     * 证件号码
     */
    @Column(name = "ID_NO")
    private String idNo;

    /**
     * 客户姓名
     */
    @Column(name = "CUST_NAME")
    private String custName;

    /**
     * 客户经理编号
     */
    @Column(name = "CUST_GRP_JL")
    private String custGrpJl;
    
    /**
     * 所属机构 机构号
     */
    @Column(name = "ORG_CD")
    private String orgCd;

    /**
     * 所属机构 机构名
     */
    @Column(name = "ORG_NAME")
    private String orgName;

    /**
     * 法人号
     */
    @Column(name = "CORP_CD")
    private String corpCd;

    /**
     * 客户经理姓名
     */
    @Column(name = "CUST_GRP_NAME")
    private String custGrpName;
    
    /**
     * 主营项目
     */
    @Column(name = "MAIN_PROJECT")
    private String mainProject;
    
    /**
     * 业务品种
     */
    @Column(name = "SERVICE_BREED")
    private String serviceBreed;

    /**
     * 业务品种2
     */
    @Column(name = "SERVICE_BREED2")
    private String serviceBreed2;

    /**
     * 担保方式
     */
    @Column(name = "WARRANT")
    private String warrant;
    
    /**
     * 评级级别
     */
    @Column(name = "PJ_JB")
    private String pjJb;
    
    /**
     * 测评失效日期
     */
    @Column(name = "LOSE_DATE")
    private String loseDate;

    /**
     * 申请金额
     */
    @Column(name = "APPLY_MONEY")
    private String applyMoney;

    /**
     * 申请期限
     */
    @Column(name = "APPLY_LIMIT")
    private String applyLimit;

    /**
     * 数据创建日期

     */
    @Column(name = "CRT_DATE")
    private String crtDate;

    /**
     * 数据修改日期

     */
    @Column(name = "MTN_DATE")
    private String mtnDate;
    
    /**
     * 审批结果  T：通过 F：拒绝
     */
    @Column(name = "SP_RESULT")
    private String spResult;
    /**
     * 流程状态0：未提交 1：流程中 2：已结束
     */
    @Column(name = "PROCESS_STATE")
    private String processState;
    /**
     * 流程实例ID
     */
    @Column(name = "PROCESS_ID")
    private String processId;
    
    /**
     * 调查报告是否完成 T：是 F:否
     */
    @Column(name = "SURVEY_STATE")
    private String surveyState;
    
    /**
     * 授信方案是否完成 T：是 F:否
     */
    @Column(name = "CREDIT_STATE")
    private String creditState;
    
    @Column(name = "EXT1")
    private String ext1;

    @Column(name = "EXT2")
    private String ext2;

    @Column(name = "EXT3")
    private String ext3;
    
}