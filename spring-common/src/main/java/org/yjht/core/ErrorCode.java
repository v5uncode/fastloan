package org.yjht.core;

public enum ErrorCode {
	PARAM_ISNULL("WC0001", "传入参数为空"),
	ID_NO_ISNULL("WC0002", "身份证号码为空"),
	ORG_CODE_ISNULL("WC0003", "机构编码为空"),
	SJYC("WC0004", "存在多条客户信息"),
	NAME_ISNULL("WC005","客户姓名为空"),
	CUSTCODE_ISNULL("WC006","客户编号为空");
	
	private ErrorCode(String code, String desc) {
		this.code = code;
		this.desc = desc;
	}
	private String code;
	private String desc;
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	
}
