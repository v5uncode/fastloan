package org.yjht.core;

public class AjaxResult {
	/**
	 * 是否成功
	 */
	private boolean isSuccess;
	/**
	 * 编码
	 */
	private String code="00";
	/**
	 * 描述
	 */
	private String desc;
	public AjaxResult(boolean isSuccess, String desc){
		this.isSuccess = isSuccess;
		this.desc = desc;
	}
	public boolean isSuccess() {
		return isSuccess;
	}
	public void setSuccess(boolean isSuccess) {
		this.isSuccess = isSuccess;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	
}
