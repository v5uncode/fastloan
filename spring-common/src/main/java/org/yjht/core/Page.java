package org.yjht.core;

import java.util.List;

public class Page {
	//当前页
		private Integer page=1;
		//每页记录数
		private Integer limit;

		//总记录数
		private Long total;

		private List<?> rows;

		public Integer getPage() {
			return page;
		}

		public void setPage(Integer page) {
			this.page = page;
		}

		public Integer getLimit() {
			return limit;
		}

		public void setLimit(Integer limit) {
			this.limit = limit;
		}

		public Long getTotal() {
			return total;
		}

		public void setTotal(Long total) {
			this.total = total;
		}

		public List<?> getRows() {
			return rows;
		}

		public void setRows(List<?> rows) {
			this.rows = rows;
		}

		@Override
		public String toString() {
			return "Page [page=" + page + ", limit=" + limit + ", total=" + total + ", rows=" + rows + "]";
		}
		

		
}
