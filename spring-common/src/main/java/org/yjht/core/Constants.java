package org.yjht.core;


public class Constants {
	
    
    /**外部数据类型*/ 
   
	public static final String SLS_CORP = "1";// 市联社总权限
	
	public static final String ADMIN_ROLE = "0000";// 超级管理员权限
	//是否
	public static final String YES="T"; //是
	public static final String NO="F";  //否
	//性别
	public static final String MAN="1"; //男
	public static final String WOMAN="2";  //女
	// 统计报表 所用的状态
	public static final String CREATE_TYPE_NO = "1";// 信息不完整
	public static final String CREATE_TYPE_YES = "2";// 信息完整

	public static final String IS_BALANCE_NO = "0";// 没有授信
	public static final String IS_BALANCE_YES = "1";// 有授信

	public static final String DATA_FROM_CL = "1";// 存量
	public static final String DATA_FROM_XZ = "0";// 采集

	public static final String ISDEL_NO = "0";// 正常
	public static final String ISDEL_YES = "1";// 已删除

	public static final String ISMOD = "1";// 被修改过

	public static final String ISTMP_YES = "T";// 正式客户
	public static final String ISTMP_NO = "F";// 预客户

	// 客户移交状态
	public static final String STATDCL = "0";// 待处理
	public static final String STATYCL = "1";// 已同意
	public static final String STATJJ = "2";// 已拒绝
	public static final String STATPL = "3";// 批量
	// 字典是否有子叶
	public static final String ISLEAF_NO = "0";// 没有分支
	public static final String ISLEAF_YES = "1";// 有分支
	//字典启用禁用
	public static final String ISENABLE_NO = "0";// 禁用
	public static final String ISENABLE_YES = "1";// 启用

	// 客户类型
	public static final String CUST_TYPE_NH = "01";// 农户
	public static final String CUST_TYPE_GTGS = "02";// 个体工商户
	public static final String CUST_TYPE_CZJM = "03";// 城镇居民
	public static final String CUST_TYPE_XS = "04";// 学生
	public static final String CUST_TYPE_JG = "05";// 军官
	public static final String CUST_TYPE_GAT = "06";// 港澳台同胞
	public static final String CUST_TYPE_WJ = "07";// 外籍人士

	// 主营项目
	public static final String MAIN01="01"; //种植蔬菜
	public static final String MAIN02="02"; //种植其他
	public static final String MAIN03="03"; //养殖猪
	public static final String MAIN04="04"; //养殖其他
	public static final String MAIN05="05"; //职工
	public static final String MAIN06="06"; //制造业
	public static final String MAIN07="07"; //运输业
	public static final String MAIN08="08"; //批发零售业
	public static final String MAIN09="09"; //餐饮及住宿业
	public static final String MAIN10="10"; //其他行业
	public static final String MAIN11="11"; //海产品养殖
	//作物类型
	public static final String ZWTYPE01="ZW_Type_Vegetable";//蔬菜
	public static final String ZWTYPE02="ZW_Type_Fruit";//水果
	public static final String ZWTYPE03="ZW_Type_Crop"; //粮食作物
	public static final String ZWTYPE04="ZW_Type_Others"; //其他
	
	public static final String ZZMODEL01="1";//露天
	public static final String ZZMODEL02="2";//大棚
	//经营行业
	public static final String BIZHY01="01";//制造业
	public static final String BIZHY02="02";//运输业
	public static final String BIZHY03="03";//餐饮及住宿业
	public static final String BIZHY04="04";//批发零售业
	public static final String BIZHY05="05";//其他行业
	
	public static final String MARINE_BREED="marine_breed";//海产品养殖
	public static final String OTHER_BREED="other_breed"; //其他养殖
	
	public static final String YZZ="01";//养殖猪
	public static final String YZHC="15";//养殖海参
	// 审批状态
	public static final String STATWTJ = "0";// 未提交
	public static final String STATLCZ = "1";// 流程中
	public static final String STATYJS = "2";// 已结束

	//审批结果
	public static final String SPJGPASS = "T";// 通过
	public static final String SPJGREJECT = "F";// 拒绝

	//放款结果
	public static final String FKCG = "1"; //放款成功
	public static final String FKSB = "9";// 放款失败
	
	
	//角色范围
	public static final String ROLE_FW_0 = "00";//本级
	public static final String ROLE_FW_1 = "01";//所有
	public static final String ROLE_FW_2 = "02";//本级及所有下级
	public static final String ROLE_FW_3 = "03";//个人
	
	//异步线程设置参数
	public static final int CORE_POOL_SIZE =10;//线程池维护线程的最少数量 
	public static final int MAX_POOL_SIZE = 100;//线程池维护线程的最大数量  
	public static final int QUEUE_CAPACITY = 10;//线程池所使用的缓冲队列  
	
	//默认密码
	public static final String PASSWORD = "11111111";

   //基础利率
	public static final Double BASE_RATE = 4.35/100;
}
