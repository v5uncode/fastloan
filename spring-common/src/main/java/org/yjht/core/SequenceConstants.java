package org.yjht.core;

public class SequenceConstants {
	/**
     * 流水号生成规则
     */
    public final static String SEQUENCE_PATTERN = "yyyyMMdd-%05d";
    

    /**
     * 合同号生成规则
     */
    public final static String HTH_SEQUENCE_PATTERN = "yyMMdd-%06d";
    
    /**
     * 保证合同号
     */
    public final static String BZ = "01";
    
    /**
     * 抵押合同号
     */
    public final static String DY = "02";
    /**
     * 质押合同号
     */
    public final static String ZY = "03";
}
