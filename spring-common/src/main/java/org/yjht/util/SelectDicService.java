package org.yjht.util;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.yjht.service.SysDicService;

@Service
public class SelectDicService {
	
	private static SysDicService lrdDicService;	
	@Resource
	public  void setLrdDicService(SysDicService lrdDicService) {
		SelectDicService.lrdDicService = lrdDicService;
	}
	
	public static SysDicService getLrdDicService() {
		return lrdDicService;
	}
}
