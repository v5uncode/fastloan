package org.yjht.util;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.yjht.bean.SysDic;
public class DicReplace {
	/**
	 * 
	 * @param list 结果集
	 * @param map key 字段名,value 字典id
	 */
	public static void replaceDic(List<Map<String, Object>> list,Map<String, String> map) {
		if(null!=list && list.size()>0){
			for(Map<String,Object> rmap :list){
				for(String key :map.keySet()){
					String parent_id = map.get(key);
					Object dic_id = rmap.get(key);
					rmap.put(key+"C", "");
					if(null!=dic_id && !"".equals(dic_id.toString())){
						if(dic_id.toString().contains(",")) {
							StringBuffer name=new StringBuffer();
							String[] id=dic_id.toString().split(",");
							for(int i=0;i<id.length;i++) {
								if(i>0) {
									parent_id=id[i-1];
								}
								SysDic dic=new SysDic();
								dic.setDicId(id[i]);
								dic.setDicParentid(parent_id);
								dic=SelectDicService.getLrdDicService().selectDic(dic);
								if(dic!=null) {
									name.append(dic.getDicName()+",");
								}								
							}
							if(StringUtils.isNotBlank(name.toString())) {
								String resultName=name.substring(0, name.lastIndexOf(","));
								rmap.put(key+"C", resultName);
							}							
						}else {
							//查询
							SysDic dic=new SysDic();
							dic.setDicId(dic_id.toString());
							dic.setDicParentid(parent_id);
							dic=SelectDicService.getLrdDicService().selectDic(dic);
							if(dic!=null) {
								rmap.put(key+"C", dic.getDicName());
							}
						}						
					}
				}
			}
		}
		
	}
	/**
	 * 替换结果对象中的字典值
	 * @param o
	 * @param map
	 */
	public static void replaceDic(Object o,Map<String, String> map) {
		if(o!=null) {
			for(String key :map.keySet()){
				String parent_id = map.get(key);
				Object dic_id = ParamsReflectUtils.getFieldValue(o, key);
				if(null!=dic_id && !"".equals(dic_id.toString())){
					if(dic_id.toString().contains(",")) {
						StringBuffer name=new StringBuffer();
						String[] id=dic_id.toString().split(",");
						for(int i=0;i<id.length;i++) {
							if(i>0) {
								parent_id=id[i-1];
							}
							SysDic dic=new SysDic();
							dic.setDicId(id[i]);
							dic.setDicParentid(parent_id);
							dic=SelectDicService.getLrdDicService().selectDic(dic);
							if(dic!=null) {
								name.append(dic.getDicName()+",");
							}							
						}
						if(StringUtils.isNotBlank(name.toString())) {
							String resultName=name.substring(0, name.lastIndexOf(","));
							if(ParamsReflectUtils.isExistFile(o, key+"C"))
								key+="C";
							ParamsReflectUtils.setFieldValue(o, key, resultName);
						}
						
					}else {
						SysDic dic=new SysDic();
						dic.setDicId(dic_id.toString());
						dic.setDicParentid(parent_id);
						dic=SelectDicService.getLrdDicService().selectDic(dic);
						if(dic!=null) {
							if(ParamsReflectUtils.isExistFile(o, key+"C"))
								key+="C";
							ParamsReflectUtils.setFieldValue(o, key, dic.getDicName());
						}
						
					}					
				}				
			}			
		}
		
	}
	/**
	 * 替换结果集对象中的字典值
	 * @param o
	 * @param map
	 */
	public static void replaceDicList(List<?> list,Map<String, String> map) {
		if(null!=list && list.size()>0){
			for(Object o:list) {
				if(o!=null) {
					for(String key :map.keySet()){
						String parent_id = map.get(key);
						Object dic_id = ParamsReflectUtils.getFieldValue(o, key);
						if(null!=dic_id && !"".equals(dic_id.toString())){
							if(dic_id.toString().contains(",")) {
								StringBuffer name=new StringBuffer();
								String[] id=dic_id.toString().split(",");
								for(int i=0;i<id.length;i++) {
									if(i>0) {
										parent_id=id[i-1];
									}
									SysDic dic=new SysDic();
									dic.setDicId(id[i]);
									dic.setDicParentid(parent_id);
									dic=SelectDicService.getLrdDicService().selectDic(dic);
									if(dic!=null) {
										name.append(dic.getDicName()+",");
									}
								}
								if(StringUtils.isNotBlank(name.toString())) {
									String resultName=name.substring(0, name.lastIndexOf(","));
									if(ParamsReflectUtils.isExistFile(o, key+"C"))
										key+="C";
									ParamsReflectUtils.setFieldValue(o, key, resultName);
								}
							}else {
								SysDic dic=new SysDic();
								dic.setDicId(dic_id.toString());
								dic.setDicParentid(parent_id);
								dic=SelectDicService.getLrdDicService().selectDic(dic);
								if(dic!=null) {
									if(ParamsReflectUtils.isExistFile(o, key+"C"))
										key+="C";
									ParamsReflectUtils.setFieldValue(o, key, dic.getDicName());
								}
							}		
						}				
					}			
				}
			}
		}
	}
	
	/**
	 * 替换结果对象中的字典值,(一个字段包含多个同级字典值)
	 * @param o
	 * @param map
	 */
	public static void replaceDics(Object o,Map<String, String> map) {
		if(o!=null) {
			for(String key :map.keySet()){
				String parent_id = map.get(key);
				Object dic_id = ParamsReflectUtils.getFieldValue(o, key);
				if(null!=dic_id && !"".equals(dic_id.toString())){
					if(dic_id.toString().contains(",")) {
						StringBuffer name=new StringBuffer();
						String[] id=dic_id.toString().split(",");
						for(int i=0;i<id.length;i++) {
							SysDic dic=new SysDic();
							dic.setDicId(id[i]);
							dic.setDicParentid(parent_id);
							dic=SelectDicService.getLrdDicService().selectDic(dic);
							if(dic!=null) {
								name.append(dic.getDicName()+",");
							}							
						}
						if(StringUtils.isNotBlank(name.toString())) {
							String resultName=name.substring(0, name.lastIndexOf(","));
							if(ParamsReflectUtils.isExistFile(o, key+"C"))
								key+="C";
							ParamsReflectUtils.setFieldValue(o, key, resultName);
						}
						
					}else {
						SysDic dic=new SysDic();
						dic.setDicId(dic_id.toString());
						dic.setDicParentid(parent_id);
						dic=SelectDicService.getLrdDicService().selectDic(dic);
						if(dic!=null) {
							if(ParamsReflectUtils.isExistFile(o, key+"C"))
								key+="C";
							ParamsReflectUtils.setFieldValue(o, key, dic.getDicName());
						}
						
					}					
				}				
			}			
		}
		
	}
	
	
	/**
	 * 替换结果集中具有子节点的字典值
	 * @param list
	 * @param pIdColumn
	 * @param childColumn
	 */
	public static void replaceChildDic(List<Map<String, Object>> list,String pIdColumn,String childColumn){
		if(null!=list && list.size()>0){
			for(Map<String,Object> rmap :list){				
					rmap.put(childColumn+"C", "");
					Object parent_id = rmap.get(pIdColumn);
					Object dic_id = rmap.get(childColumn);
					if(null!=dic_id &&null!=parent_id&& !"".equals(dic_id.toString())&&!"".equals(parent_id.toString())){
						//查询
						SysDic dic=new SysDic();
						dic.setDicId(dic_id.toString());
						dic.setDicParentid(parent_id.toString());
						dic=SelectDicService.getLrdDicService().selectDic(dic);
						if(dic!=null) {
							rmap.put(childColumn+"C", dic.getDicName());
						}						
					}
				}
			}		
	}
	/**
	 * 替换结果对象中具有子节点的字典值   判断 对象中有无childColumn+C属性  有给childColumn+C赋字典名称 无给childColumn赋字典名称 
	 * @param list
	 * @param pIdColumn  父列
	 * @param childColumn 子列
	 */
	public static void replaceChildDic(Object obj,String pIdColumn,String childColumn){
		if(obj!=null){
			String parent_id = ParamsReflectUtils.getFieldValue(obj, pIdColumn);
			String dic_id = ParamsReflectUtils.getFieldValue(obj, childColumn);
			if(StringUtils.isNotBlank(parent_id)&&StringUtils.isNotBlank(dic_id)){
				//查询
				SysDic dic=new SysDic();
				dic.setDicId(dic_id);
				dic.setDicParentid(parent_id);
				dic=SelectDicService.getLrdDicService().selectDic(dic);
				if(dic!=null) {
					if(ParamsReflectUtils.isExistFile(obj, childColumn+"C"))
						childColumn+="C";
					ParamsReflectUtils.setFieldValue(obj, childColumn,dic.getDicName());
				}						
			}
		}		
	}
}
