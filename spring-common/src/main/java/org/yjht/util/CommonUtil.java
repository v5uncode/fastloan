package org.yjht.util;


import org.yjht.bean.vo.User;

/**
 * @author zhaolulu
 * 管理工具类
 */
public class CommonUtil {

  /**
   * 获取当前用户
   */
  public static User getUser() {	
    return (User) ContextUtil.getSession().getAttribute("user");
  }
}

 