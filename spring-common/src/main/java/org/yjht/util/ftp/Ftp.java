package org.yjht.util.ftp;

import java.io.IOException;
import java.io.InputStream;
import java.net.SocketException;
import java.util.List;

public interface Ftp {
	/**
	 * 连接远程ftp服务器
	 * @param server 服务器ip 或者机器名
	 * @param port  端口
	 * @param user  用户名
	 * @param password  密码
	 * @throws SocketException
	 * @throws IOException
	 */
	public void connect(String server, int port, String user,
                        String password) throws SocketException, IOException ;

	/**
	 * 连接远程ftp服务器
	 * @param server ip 或者机器名称
	 * @param port   端口
	 * @param user   用户名
	 * @param password  用户密码
	 * @param path     远程路径
	 * @throws SocketException
	 * @throws IOException
	 */
	public void connect(String server, int port, String user,
                        String password, String path) throws SocketException, IOException ;

	/**
	 * 文件传输模式二进制
	 * @throws IOException
	 */
	public void bin()throws IOException;
	
	/**
	 * 文件传输模式文本模式
	 * @throws IOException
	 */
	public void asc()throws IOException;
	
	/**
	 * 关闭ftp连接
	 * @throws IOException
	 */
	public void close() throws IOException ;

	/**
	 * 切换路径
	 * @param path 路径
	 * @return
	 * @throws IOException
	 */
	public boolean cd(String path) throws IOException;
	
	/**
	 * 建立路径
	 * @param pathName  路径名称
	 * @return
	 * @throws IOException
	 */
	public boolean mkdir(String pathName) throws IOException;

	/**
	 * 删除目录
	 * @param path
	 * @return
	 * @throws IOException
	 */
	public boolean rm(String path) throws IOException ;

    /**
     * 删除目录极其子目录
     * @param path   目录
     * @param isAll  是否删除子目录
     * @return
     * @throws IOException
     */
	public boolean rm(String path, boolean isAll)
			throws IOException ;
	
    /**
     * 检查目录是否存在
     * @param path  路径
     * @return
     * @throws IOException
     */
	public boolean existDirectory(String path) throws IOException;
	
	/**
	 * 获取指定目录文件名列表
	 * @param path
	 * @return
	 * @throws IOException
	 */
	public List<String> getFileList(String path) throws IOException;
	
	/**
	 * 删除ftp服务器上的文件
	 * @param pathName  
	 * @return
	 * @throws IOException
	 */
	public boolean deleteFile(String pathName) throws IOException;
	
    /**
     * 上传文件到指定服务器
     * @param fileName 待上传文件名称
     * @param newName  上传的文件名称
     * @return
     * @throws IOException
     */
	public boolean uploadFile(String fileName, String newName)
			throws IOException;
	
	/**
	 * 上传文件
	 * @param fileName  文件名称
	 * @return
	 * @throws IOException
	 */
	public boolean uploadFile(String fileName) throws IOException;
	
    /**
     * 下载文件
     * @param remoteFileName  远程文件名称
     * @param localFileName   本地文件名称
     * @return
     * @throws IOException
     */
	public boolean download(String remoteFileName, String localFileName)
			throws IOException;
	
	/**
	 * 下载文件获取流
	 * @param remoteFileName  远程文件名称
	 * @return
	 * @throws IOException
	 */
	public InputStream downloadStream(String remoteFileName);
}
