package org.yjht.util;

import org.yjht.core.Result;
import org.yjht.core.ResultCode;

/**
 * 响应结果生成工具
 */
public class ResultGenerator {
	private static final String DEFAULT_SUCCESS_MESSAGE = "SUCCESS";

	public static Result genSuccessResult() {
		return new Result().setCode(ResultCode.SUCCESS).setMessage(DEFAULT_SUCCESS_MESSAGE);
	}

	public static Result genSuccessResult(Object data) {
		return new Result().setCode(ResultCode.SUCCESS).setMessage(DEFAULT_SUCCESS_MESSAGE).setData(data);
	}
	public static Result genSuccessResult(String message) {
		return new Result().setCode(ResultCode.SUCCESS).setMessage(message);
	}
	public static Result genIsExist(Object data) {
		return new Result().setCode(ResultCode.IS_EXIST).setMessage("数据已存在").setData(data);
	}
	public static Result genFailResult(String message) {
		return new Result().setCode(ResultCode.FAIL).setMessage(message);
	}

	public static Result genUNAUTHORResult() {
		return new Result().setCode(ResultCode.UNAUTHORIZED).setMessage("认证失败");
	}
	public static Result genNOTFOUNDResult() {
		return new Result().setCode(ResultCode.NOT_FOUND).setMessage("没有发现该接口");
	}
}
