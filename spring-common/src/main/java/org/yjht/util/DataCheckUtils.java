package org.yjht.util;

import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.yjht.exception.MyException;

public class DataCheckUtils {
	public static String checkTelNo(String telNo) throws MyException {
		 if (StringUtils.isNotBlank(telNo)) {
            // 固定电话或者手机号校验
        	/*telNo = Base64.decode(telNo);*/
            String telReg = "^1[34578]\\d{9}$";// 手机
            String mobile = "^0\\d{2,3}-?\\d{7,8}$";// 电话
            telNo = telNo.replace("-", "");
            Boolean boo = Pattern.compile(mobile).matcher(telNo).matches();
            String err = null;
            if (boo) {
                String headNum = (telNo).substring(0, 3);
                String headNum2 = (telNo).substring(0, 4);
                // 前三位为010、020、021、022、023、024、025、027、028、029时，要求格式为（3位-8位，如果没有“-”，则补“-”，要求总长度必须为12位。
                if ("010".equals(headNum) || "020".equals(headNum) || "021".equals(headNum) || "022".equals(headNum) || "023".equals(headNum) || "024".equals(headNum) || "025".equals(headNum)
                        || "027".equals(headNum) || "028".equals(headNum) || "029".equals(headNum)) {
                    if ((telNo).length() == 11) {
                    	telNo = (telNo).substring(0, 3) + "-" + (telNo).substring(3, 11);
                    } else {
                    	err = "联系电话输入错误，请重新输入！";
                    }
                } else if ("0531".equals(headNum2) || "0532".equals(headNum2)) {
                    // 前四位为0531、0532，要求格式为（4位-8位），如果没有“-”，则补“-”，要求总长度必须为13位。
                    if ((telNo).length() == 12) {
                    	telNo = (telNo).substring(0, 4) + "-" + (telNo).substring(4, 12);
                    } else {
                        err = "联系电话输入错误，请重新输入！";
                    }
                } else {
                    // 前三位不等于010、020、021、022、023、024、025、027、028、029，且前四位不等于0531、0532，要求格式为（4位-7位），如果没有“-”，则补“-”，要求总长度必须为12位。
                    if ((telNo).length() == 11) {
                    	telNo = (telNo).substring(0, 4) + "-" + (telNo).substring(4, 11);
                    } else {
                        err = "联系电话输入错误，请重新输入！";
                    }
                }
            }else {
	           	 if(!Pattern.compile(telReg).matcher(telNo).matches()) {
	        		 err = "联系电话输入错误，请重新输入！";
	        	 }
            }
            if (err != null) {
                throw new MyException(err);
            } 

        }else {
        	throw new MyException("联系电话不能为空");
        }
		return telNo;
	}
}
