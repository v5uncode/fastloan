package org.yjht.util;

import java.net.InetAddress;
import java.net.UnknownHostException;

import javax.servlet.http.HttpServletRequest;

public class CommonGenerator {

	public static String getIpAddress(HttpServletRequest request) {
		// TODO Auto-generated method stub
		String ip = request.getHeader("x-forwarded-for"); 
	    if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) { 
	      ip = request.getHeader("Proxy-Client-IP"); 
	    } 
	    if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) { 
	      ip = request.getHeader("WL-Proxy-Client-IP"); 
	    } 
	    if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) { 
	      ip = request.getHeader("HTTP_CLIENT_IP"); 
	    } 
	    if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) { 
	      ip = request.getHeader("HTTP_X_FORWARDED_FOR"); 
	    } 
	    if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) { 
	      ip = request.getRemoteAddr();
	      if(ip.equals("127.0.0.1")){
	    	 InetAddress inet=null;
	    	 
	    	 try {
				inet=InetAddress.getLocalHost();
			} catch (UnknownHostException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    	ip=inet.getHostAddress();  
	      }
	    } 
	    if(ip!=null&&ip.length()>15){
	    	if(ip.indexOf(",")>0){
	    		ip=ip.substring(0, ip.indexOf(","));
	    	}
	    }
	    return ip;
	}
}
