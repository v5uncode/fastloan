package org.yjht.util.xml;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author lengleng
 * @date 2018/2/2
 * 适配器请求对象
 */
@XmlRootElement(name = "reqmessage")
class Request {
    /**
     * 请求报文头
     */
    private Head head;

    /**
     * 报文体
     */
    private String body;

    @XmlElement(name = "head")
    public Head getHead() {
        return head;
    }

    public void setHead(Head head) {
        this.head = head;
    }

    @XmlElement(name = "body")
    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}
