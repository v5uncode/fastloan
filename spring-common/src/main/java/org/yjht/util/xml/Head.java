package org.yjht.util.xml;

import javax.xml.bind.annotation.XmlElement;

/**
 * @author lengleng
 * @date 2018/2/2
 * 报文头对象
 */
public class Head {
    /**
     * 交易代码
     */
    private String trxtype;

    /**
     * 目标机构代码
     */
    private String destinst;

    /**
     * 交易来源
     */
    private String source;

    /**
     * 网点代号
     */
    private String brn_no;

    /**
     * 操作员编号
     */
    private String ope_no;

    /**
     * 唯一流水号yyyymmddAA******
     */
    private String seqno;

    /**
     * 用途说明
     */
    private String app_info;

    @XmlElement(name = "trxtype")
    public String getTrxtype() {
        return trxtype;
    }

    public void setTrxtype(String trxtype) {
        this.trxtype = trxtype;
    }

    @XmlElement(name = "destinst")
    public String getDestinst() {
        return destinst;
    }

    public void setDestinst(String destinst) {
        this.destinst = destinst;
    }

    @XmlElement(name = "source")
    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    @XmlElement(name = "brn_no")
    public String getBrn_no() {
        return brn_no;
    }

    public void setBrn_no(String brn_no) {
        this.brn_no = brn_no;
    }

    @XmlElement(name = "ope_no")
    public String getOpe_no() {
        return ope_no;
    }

    public void setOpe_no(String ope_no) {
        this.ope_no = ope_no;
    }

    @XmlElement(name = "seqno")
    public String getSeqno() {
        return seqno;
    }

    public void setSeqno(String seqno) {
        this.seqno = seqno;
    }

    @XmlElement(name = "app_info")
    public String getApp_info() {
        return app_info;
    }

    public void setApp_info(String app_info) {
        this.app_info = app_info;
    }

	@Override
	public String toString() {
		return "Head [trxtype=" + trxtype + ", destinst=" + destinst + ", source=" + source + ", brn_no=" + brn_no
				+ ", ope_no=" + ope_no + ", seqno=" + seqno + ", app_info=" + app_info + "]";
	}
    
}
