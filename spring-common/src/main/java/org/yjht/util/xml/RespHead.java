package org.yjht.util.xml;

import javax.xml.bind.annotation.XmlElement;

public class RespHead extends Head{
    private String ret_code;
    private String ret_desc;

    @XmlElement(name = "ret_code")
    public String getRet_code() {
        return ret_code;
    }

    public void setRet_code(String ret_code) {
        this.ret_code = ret_code;
    }

    @XmlElement(name = "ret_desc")
    public String getRet_desc() {
        return ret_desc;
    }

    public void setRet_desc(String ret_desc) {
        this.ret_desc = ret_desc;
    }

	@Override
	public String toString() {
		return "RespHead [ret_code=" + ret_code + ", ret_desc=" + ret_desc + "]";
	}
    
}
