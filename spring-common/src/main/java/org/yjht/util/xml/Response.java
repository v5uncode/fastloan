package org.yjht.util.xml;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Map;

/**
 * @author lengleng
 * @date 2018/05/22
 * 兴业返回报文对象
 */
@XmlRootElement(name = "respMessage")
public class Response<T> {
    /**
     * 报文头
     */
    private RespHead head;
    private T body;

    @XmlElement(name = "head")
    public RespHead getHead() {
        return head;
    }

    public void setHead(RespHead head) {
        this.head = head;
    }

    public T getBody() {
        return body;
    }

    public void setBody(T body) {
        this.body = body;
    }
}
