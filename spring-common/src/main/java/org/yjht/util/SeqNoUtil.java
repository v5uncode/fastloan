package org.yjht.util;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.thread.ThreadUtil;
import cn.hutool.core.util.RandomUtil;

import java.util.Date;

/**
 * @author lengleng
 * @date 2018/6/1
 * 流水号生成工具类
 */
public class SeqNoUtil {
    /**
     * 生成兴业业务流水号
     *
     * @param count 随机数位数
     * @return yyyyMMddHHmmss（14） + 系统流水
     */
    public static synchronized String xySerial(int count) {
        ThreadUtil.sleep(5);
        String datetime = DateUtil.format(new Date(), "yyyyMMddHHSSS");
        return datetime + RandomUtil.randomNumbers(count - 13);
    }

}
