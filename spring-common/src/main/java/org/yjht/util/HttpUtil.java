package org.yjht.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

import org.apache.http.HttpHost;
import org.apache.http.NameValuePair;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;

import com.alibaba.fastjson.JSONObject;

public class HttpUtil {
	private static Logger logger = Logger.getLogger(HttpUtil.class);
	
	public static String getHttpGetReq(String accessUrl) {
		StringBuffer sb = new StringBuffer();
		BufferedReader in = null;
		HttpURLConnection httpConn = null;
		try {
			//Proxy proxy = new Proxy(Proxy.Type.DIRECT.HTTP, new InetSocketAddress("172.17.8.51",8090));
			URL url = new URL(accessUrl);
			httpConn = (HttpURLConnection) url.openConnection();
			httpConn.setConnectTimeout(30000);
			httpConn.setReadTimeout(30000);
			httpConn.setDoInput(true);
			httpConn.setDoOutput(true);
			httpConn.connect();
			
			InputStream is = httpConn.getInputStream();
			// 定义 BufferedReader输入流来读取URL的响应
			in = new BufferedReader(new InputStreamReader(is, "UTF-8"));
			String line;
			while ((line = in.readLine()) != null) {
				sb.append(line);
			}
		} catch (Exception e) {
			logger.error("HTTP GET err:" + e.getMessage());
		} finally {
			try {
				if (in != null) {
					in.close();
				}
				httpConn.disconnect();
			} catch (Exception e) {
				logger.error("HTTP GET [close resouse] err:" + e.getMessage());
			}
		}
		return sb.toString();
	}

	public static String getHttpPostReq(String strURL, String params) {
		try {
			//Proxy proxy = new Proxy(Proxy.Type.DIRECT.HTTP, new InetSocketAddress("172.17.8.51",8090));
			URL url = new URL(strURL);// 创建连接
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setDoOutput(true);
			connection.setDoInput(true);
			connection.setUseCaches(false);
			connection.setInstanceFollowRedirects(true);
			connection.setRequestMethod("POST"); // 设置请求方式
			connection.setRequestProperty("Accept", "application/json"); // 设置接收数据的格式
			connection.setRequestProperty("Content-Type", "application/json"); // 设置发送数据的格式
			connection.connect();
			OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream(), "UTF-8"); // utf-8编码
			out.append(params);
			out.flush();
			out.close();
			// 读取响应
			int length = (int) connection.getContentLength();// 获取长度
			InputStream is = connection.getInputStream();
			if (length != -1) {
				byte[] data = new byte[length];
				byte[] temp = new byte[512];
				int readLen = 0;
				int destPos = 0;
				while ((readLen = is.read(temp)) > 0) {
					System.arraycopy(temp, 0, data, destPos, readLen);
					destPos += readLen;
				}
				String result = new String(data, "UTF-8"); // utf-8编码
				JSONObject jsonObject = JSONObject.parseObject(result);
				System.out.println(result);
				return result;
			}
		} catch (IOException e) {
			logger.error(e.getMessage());
		}
		
		return "error"; // 自定义错误信息
	}
	
	
	public static String doGet(String url, Map<String, String> param) {
		 // 创建Httpclient对象
		 CloseableHttpClient httpclient = HttpClients.createDefault();
		 String resultString = "";
		 CloseableHttpResponse response = null;
		 try {
			 // 创建uri
			 URIBuilder builder = new URIBuilder(url);
			 if (param != null) {
				 for (String key : param.keySet()) {
					 builder.addParameter(key, param.get(key));
				 }
			 }
		 URI uri = builder.build();

		 // 创建http GET请求
		 HttpGet httpGet = new HttpGet(uri);
		 // 执行请求
		 response = httpclient.execute(httpGet);
		 // 判断返回状态是否为200
		 if (response.getStatusLine().getStatusCode() == 200) {
			 resultString = EntityUtils.toString(response.getEntity(), "UTF-8");
		 }
		 } catch (Exception e) {
			 e.printStackTrace();
		 } finally {
			 try {
				 if (response != null) {
					 response.close();
				 }
				 httpclient.close();
			 } catch (IOException e) {
			     e.printStackTrace();
			 }
		 }
		 return resultString;
	}

	public static String doGet(String url) {
		return doGet(url, null);
	}
		  
	public static String doPost(String url, Map<String, Object> param) {
		 // 创建Httpclient对象
		 CloseableHttpClient httpClient = HttpClients.createDefault();
		 CloseableHttpResponse response = null;
		 String resultString = "";
		 try {			 		 
			// 创建Http Post请求
			 HttpPost httpPost = new HttpPost(url);
			 // 创建参数列表
			 if (param != null) {
				 List<NameValuePair> paramList = new ArrayList<>();
				 for (String key : param.keySet()) {
					 paramList.add(new BasicNameValuePair(key, param.get(key).toString()));
				 }
				 // 模拟表单
				 UrlEncodedFormEntity entity = new UrlEncodedFormEntity(paramList, "utf-8");
				 httpPost.setEntity(entity);
			 }
			 // 执行http请求
			response = httpClient.execute(httpPost);
			resultString = EntityUtils.toString(response.getEntity(), "utf-8");
		} catch (Exception e) {
			e.printStackTrace();
		 } finally {
			 try {
				 if (response != null) {
					 response.close();
				 }
				 httpClient.close();
			 } catch (IOException e) {
				 e.printStackTrace();
			 }
		 }
		 return resultString;
	}

	public static String doPost(String url) {
		return doPost(url, null);
	}
	/**
	* 请求的参数类型为json
	* @param url
	* @param json
	* @return
	* {username:"",pass:""}
	*/
	public static String doPostJson(String url, String json) {
		// 创建Httpclient对象
		CloseableHttpClient httpClient = HttpClients.createDefault();
		CloseableHttpResponse response = null;
		String resultString = "";
		try {
			// 创建Http Post请求
			HttpPost httpPost = new HttpPost(url);
			// 创建请求内容
			StringEntity entity = new StringEntity(json, ContentType.APPLICATION_JSON);
			httpPost.setEntity(entity);
			// 执行http请求
			response = httpClient.execute(httpPost);
			resultString = EntityUtils.toString(response.getEntity(), "utf-8");
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (response != null) {
					 response.close();
				 }
				httpClient.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return resultString;
	}
	
}
