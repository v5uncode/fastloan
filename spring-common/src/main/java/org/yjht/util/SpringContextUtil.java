package org.yjht.util;

import org.springframework.context.ApplicationContext;
/**
 * 手动获取springboot中spring中的bean
 * @author lixiaopeng
 *
 */
public class SpringContextUtil {
	/**
	 * spring-boot上下文
	 */
	private static ApplicationContext context;

	public static ApplicationContext getContext() {
		return context;
	}

	public static void setContext(ApplicationContext context) {
		SpringContextUtil.context = context;
	}
	
}
