package org.yjht.util;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

public class MapUtil {
	public static Map<String, Object> objectToMap(Object o){
		Map<String, Object> map = new HashMap<String, Object>();
		Field[] fs = o.getClass().getDeclaredFields();
		for(Field f : fs){
			String fieldName = f.getName();
			char[] cs = fieldName.toCharArray();
			String tailMethod = fieldName;
			if(cs[0] >= 'a' && cs[0] <= 'z'){
				cs[0] -= 32;
				tailMethod = String.valueOf(cs);
			}
			try {
				Method m = o.getClass().getDeclaredMethod("get" + tailMethod, new Class[]{});
				Object tmp = m.invoke(o, new Object[]{});
				map.put(fieldName, tmp);
			} catch (Exception e) {
				e.printStackTrace();
			} 
		}
		return map;
	}
}
