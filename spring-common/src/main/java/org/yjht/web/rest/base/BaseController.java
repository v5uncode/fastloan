package org.yjht.web.rest.base;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Base64Utils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.yjht.biz.BaseBiz;
import org.yjht.core.Result;
import org.yjht.util.ResultGenerator;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
public class BaseController<Biz extends BaseBiz,Entity> {
    @Autowired
    protected HttpServletRequest request;
    @Autowired
    protected Biz baseBiz;

    @SuppressWarnings("unchecked")
	@RequestMapping(value = "",method = RequestMethod.POST)
    @ResponseBody
    public Result add(Entity entity){
        baseBiz.insertSelective(entity);
        return ResultGenerator.genSuccessResult();
    }

    @RequestMapping(value = "/{id}",method = RequestMethod.GET)
    @ResponseBody
    public Result get(@PathVariable int id){
    	return ResultGenerator.genSuccessResult(baseBiz.selectById(id));
    }

    @RequestMapping(value = "/{id}",method = RequestMethod.PUT)
    @ResponseBody
    public Result update(Entity entity){
    	baseBiz.updateSelectiveById(entity);
    	return ResultGenerator.genSuccessResult();
    }
    @RequestMapping(value = "/{id}",method = RequestMethod.DELETE)
    @ResponseBody
    public Result remove(@PathVariable int id){
        baseBiz.deleteById(id);
        return ResultGenerator.genSuccessResult();
    }

    @RequestMapping(value = "/all",method = RequestMethod.GET)
    @ResponseBody
    public List<Entity> list(){
        return baseBiz.selectListAll();
    }

    public String getCurrentUserName(){
        String authorization = request.getHeader("Authorization");
        return new String(Base64Utils.decodeFromString(authorization));
    }
}
