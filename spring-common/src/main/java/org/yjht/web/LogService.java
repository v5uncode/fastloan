package org.yjht.web;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.yjht.bean.LogInfo;
import org.yjht.bean.LrdLog;
import org.yjht.biz.LrdLogBiz;
import org.yjht.util.DateTools;

import java.text.ParseException;
import java.util.Date;

@RequestMapping("api")
@Controller
public class LogService {
	@Autowired
    private LrdLogBiz gateLogBiz;
    @RequestMapping(value="/log/save",method = RequestMethod.POST)
    public void saveLog(@RequestBody LogInfo info) throws ParseException {
    	LrdLog log = new LrdLog();
        BeanUtils.copyProperties(info,log);
        gateLogBiz.insertSelective(log);
    }
}
