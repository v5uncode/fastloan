package org.yjht.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.yjht.core.Result;
import org.yjht.service.SysDicService;
import org.yjht.util.ResultGenerator;

@RestController
@RequestMapping("/admin")
public class AdminController {
	@Autowired
	private SysDicService sysDicService;
	
	@RequestMapping(value = "/dic/{parentId}",method = RequestMethod.GET)
    public Result selectList(@PathVariable String parentId){ 		
        return sysDicService.finSysDicByParentId(parentId);
    }
	@RequestMapping(value = "/{parentId}/dic",method = RequestMethod.GET)
    public Result selectDic(@PathVariable String parentId){ 		
        return ResultGenerator.genSuccessResult(sysDicService.selectDic(parentId));
    }
	

}
