package org.yjht.base.impl;

import tk.mybatis.mapper.entity.Condition;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.yjht.base.BaseService;
import org.yjht.common.MyMapper;
import org.yjht.core.ReType;
import org.yjht.util.DateTools;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
public abstract class BaseServiceImpl <T,E extends Serializable> implements BaseService<T,E> {

  @Autowired
  public MyMapper<T> myMapper;

  @Override
  public int deleteByPrimaryKey(E id) {
    return myMapper.deleteByPrimaryKey(id);
  }

  @Override
  public int insert(T record) {
    record=addValue(record,true);
    return myMapper.insert(record);
  }

  /**
   * 通用注入创建 更新信息 可通过super调用
   * @param record
   * @param flag
   * @return
   */
  public T  addValue(T record,boolean flag){
    //User currentUser= CommonUtil.getUser();
    //统一处理公共字段
    Class<?> clazz=record.getClass();
    try {
      if(flag){
        Field fieldDate=clazz.getDeclaredField("crtDate");        
        if(fieldDate!=null){
	        fieldDate.setAccessible(true);
	        fieldDate.set(record,DateTools.getCurrentSysData(DateTools.FULLTIME_FORMAT));
        }
        Field fieldDate2=clazz.getDeclaredField("mtnDate");
        if(fieldDate2!=null){
	        fieldDate2.setAccessible(true);
	        fieldDate2.set(record,DateTools.getCurrentSysData(DateTools.FULLTIME_FORMAT));
        }
      }else{
        Field fieldDate=clazz.getDeclaredField("mtnDate");
        if(fieldDate!=null){
	        fieldDate.setAccessible(true);
	        fieldDate.set(record,DateTools.getCurrentSysData(DateTools.FULLTIME_FORMAT));
        }
      }
    } catch (NoSuchFieldException e) {
      //无此字段
    } catch (IllegalAccessException e) {
      e.printStackTrace();
    }
    return record;
  }

  @Override
  public int insertSelective(T record) {
    record=addValue(record,true);
    return myMapper.insertSelective(record);
  }

  @Override
  public T selectByPrimaryKey(E id) {
    return myMapper.selectByPrimaryKey(id);
  }
  @Override
  public List<T> selectByCondition(Condition condition) { //条件查找
      return myMapper.selectByExample(condition);
  }

  @Override
  public int updateByPrimaryKeySelective(T record) {
    record=addValue(record,false);
    return myMapper.updateByPrimaryKeySelective(record);
  }

  @Override
  public int updateByPrimaryKey(T record) {
    record=addValue(record,false);
    return myMapper.updateByPrimaryKey(record);
  }
  
  @Override
  public int updateByExampleSelective(T record, Object example) {
	return myMapper.updateByExampleSelective(record, example);
 }

  @Override
  public List<T> selectList(T record){
      return myMapper.select(record);
  }

  /**
   * 公共展示类
   * @param t 实体
   * @param page 页
   * @param limit 行
   * @return
   */
  @Override
  public ReType show(T t,int page,int limit){
    PageHelper.startPage(page,limit, true);
    List<T> tList=myMapper.select(t);
    ReType reType=new ReType(new PageInfo<T>(tList).getTotal(),tList);
    return reType;
  }

}
