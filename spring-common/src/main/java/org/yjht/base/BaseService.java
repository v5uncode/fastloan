package org.yjht.base;

import java.io.Serializable;
import java.util.List;

import org.yjht.core.ReType;

import tk.mybatis.mapper.entity.Condition;

public interface BaseService <T,E extends Serializable>{
  /**
   * 根据id删除
   * @param id
   * @return
   */
  int deleteByPrimaryKey(E id);

  /**
   * 插入
   * @param record
   * @return
   */
  int insert(T record);

  /**
   *插入非空字段
   * @param record
   * @return
   */
  int insertSelective(T record);

  /**
   * 根据id查询
   * @param id
   * @return
   */
  T selectByPrimaryKey(E id);
  /**
   * 根据条件查找
   * @param condition
   * @return
   */
  List<T> selectByCondition(Condition condition);

  /**
   * 更新非空数据
   * @param record
   * @return
   */
  int updateByPrimaryKeySelective(T record);
  
  /*
   * 根据条件更新非空字段
   */
  int updateByExampleSelective(T record,Object example);

  /**
   * 更新
   * @param record
   * @return
   */
  int updateByPrimaryKey(T record);


  List<T> selectList(T record);

  public ReType show(T t, int page, int limit);

}
