package org.yjht.events;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.context.ApplicationEvent;

/**
 * @author lengleng
 * @date 2018/6/4
 * 工作任务事件
 */
@ToString
public class ScheduleEvent extends ApplicationEvent {
    @Getter
    @Setter
    private String userName;
    @Getter
    @Setter
    private String workDate;
    @Getter
    @Setter
    private String bgnTime;
    @Getter
    @Setter
    private String endTime;
    @Getter
    @Setter
    private String workTitle;
    @Getter
    @Setter
    private String workCont;
    @Getter
    @Setter
    private String warnRule;
    @Getter
    @Setter
    private String warnCnt;
    @Getter
    @Setter
    private String finishFlag = "0";
    @Getter
    @Setter
    private String workDesc;

    public ScheduleEvent(Object source) {
        super(source);
    }
}
