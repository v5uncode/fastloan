package org.yjht.mapper;

import org.yjht.bean.LrdLog;
import org.yjht.common.MyMapper;

public interface LrdLogMapper extends MyMapper<LrdLog> {
    public void deleteAll();
}