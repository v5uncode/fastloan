package org.yjht.mapper;

import org.yjht.bean.FrontInfoScsp;
import org.yjht.common.MyMapper;

public interface FrontInfoScspMapper extends MyMapper<FrontInfoScsp> {
}