package org.yjht.config;

import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.yjht.core.Result;
import org.yjht.exception.MyException;
import org.yjht.util.ResultGenerator;

/**
 * 统一异常处理
 * @author lenovo
 *
 */
@RestControllerAdvice 
public class GlobalExceptionHandler {
	private Logger log=Logger.getLogger(GlobalExceptionHandler.class); 
	@ExceptionHandler(value = { MyException.class })
	public Result MyErrorHandler(MyException e) {
		log.error(e.getMessage());
		return ResultGenerator.genFailResult(e.getMessage());
	}
	@ExceptionHandler(value = { RuntimeException.class })
	public Result defaultErrorHandler(Exception e) {
		e.printStackTrace();
		log.error(e.getMessage(),e);
		return ResultGenerator.genFailResult("操作失败");
	}

}
