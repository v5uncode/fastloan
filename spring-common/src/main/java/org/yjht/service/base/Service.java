package org.yjht.service.base;

import java.util.List;

import org.apache.ibatis.exceptions.TooManyResultsException;
import org.yjht.core.Query;
import org.yjht.core.TableResultResponse;

import tk.mybatis.mapper.entity.Condition;

public interface Service<T> {
	int save(T model);//持久化
	int save(List<T> models);//批量持久化  支持mysql数据库
	int deleteById(Object id);//通过主鍵刪除
	int deleteByCondition(Condition condition);//根据条件删除
	int update(T model);//更新
	List<T> find(T model);
	T findOne(T model);
    T findById(Object id);//通过ID查找
    T findBy(String property, Object value) throws TooManyResultsException; //通过某个成员属性查找,value需符合unique约束
    List<T> findByCondition(Condition condition);//根据条件查找
    TableResultResponse<T> findByQuery(Query query);//根据条件查找
    List<T> findAll();//获取所有
}
