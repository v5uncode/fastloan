package org.yjht.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.yjht.bean.SysDic;
import org.yjht.bean.Tree;
import org.yjht.bean.vo.User;
import org.yjht.core.Constants;
import org.yjht.core.Result;
import org.yjht.mapper.SysDicMapper;
import org.yjht.service.SysDicService;
import org.yjht.service.base.AbstractService;
import org.yjht.thread.AsyncTaskServcie;
import org.yjht.util.ResultGenerator;

import tk.mybatis.mapper.entity.Condition;
import tk.mybatis.mapper.entity.Example.Criteria;

@Service
public class SysDicServiceImpl extends AbstractService<SysDic> implements SysDicService {
	@Autowired
	SysDicMapper sysDicMapper;
	@Autowired
	AsyncTaskServcie asyncTaskServcie;
	
	@Override
	@Cacheable(value = "dic",key="#parentId")
	public Result finSysDicByParentId(String parentId) {
		List<Tree>  list=finSysDicByParentId1(parentId);
		return ResultGenerator.genSuccessResult(list);
	}
	public List<Tree> finSysDicByParentId1(String parentId) {
		Condition condition = new Condition(SysDic.class);
		condition.createCriteria().andEqualTo("dicParentid",parentId);
		condition.createCriteria().andEqualTo("dicIsenable",Constants.ISENABLE_YES);
		List<SysDic> list=findByCondition(condition);
		List<Tree> list1=new ArrayList<Tree>();
		for(SysDic dic:list) {
			Tree tree=new Tree();
			tree.setLabel(dic.getDicName());
			tree.setValue(dic.getDicId());
			if(dic.getIsleaf().equals("1")) {
				tree.setChildren(finSysDicByParentId1(dic.getDicId()));
			}else {
				tree.setChildren("");
			}
			list1.add(tree);
		}
		return list1;
	}
	
	@Override
	public Result querySysDicTree() {
		// TODO Auto-generated method stub
		Condition condition = new Condition(SysDic.class);
		condition.createCriteria().andEqualTo("isleaf",Constants.ISLEAF_YES);
		condition.createCriteria().andEqualTo("dicIsenable",Constants.ISENABLE_YES);
		condition.orderBy("dicSort").desc();
		List<SysDic> list=findByCondition(condition);
		return ResultGenerator.genSuccessResult(list);
	}
	
	@Override
	@CacheEvict( value= "dic",key="#dic.classId",beforeInvocation=true)
	public Result updateSysDic(SysDic dic) {
		sysDicMapper.updateByPrimaryKeySelective(dic);
		if(!dic.getDicParentid().equals("xxx")){//排除根节点
			asyncTaskServcie.executeAsyncTaskPlus(dic.getDicParentid());
		}
		return ResultGenerator.genSuccessResult(dic);
	}
	
	@Transactional
	@Override
	@CacheEvict( value= "dic",key="#sysdic.classId",beforeInvocation=true)
	public Result deleteSysDic(SysDic sysdic) {	
		
		if(sysdic.getIsleaf().equals(Constants.ISLEAF_YES)) {
			Condition condition = new Condition(SysDic.class);
			Criteria criteria =condition.createCriteria();			
			criteria.andEqualTo("dicParentid",sysdic.getDicId());		
			criteria.andEqualTo("dicIsenable",Constants.ISENABLE_YES);
			List<SysDic> dics=sysDicMapper.selectByExample(condition);
			if(!dics.isEmpty()) {
				return ResultGenerator.genFailResult("请先删除子节点数据");
			}
		}															
		sysDicMapper.deleteByPrimaryKey(sysdic);
		if(!sysdic.getDicParentid().equals("xxx")){  //排除根节点
			asyncTaskServcie.executeAsyncTaskPlus(sysdic.getDicParentid());
		}
		return ResultGenerator.genSuccessResult("删除成功");
	}
	
	@Override
	@Transactional
	@CacheEvict( value= "dic",key="#list.get(0).classId",beforeInvocation=true)
	public Result saveSysDic(HttpServletRequest request,List<SysDic> list) {
		// TODO Auto-generated method stub
		User user = (User)request.getSession().getAttribute("user");
		for(SysDic sysDic:list ){
			Condition condition = new Condition(SysDic.class);
			Criteria criteria =condition.createCriteria();
			if(StringUtils.isNotBlank(sysDic.getDicId())&&StringUtils.isNotBlank(sysDic.getDicParentid())){
				criteria.andEqualTo("dicParentid",sysDic.getDicParentid());
				criteria.andEqualTo("dicId",sysDic.getDicId());
				List<SysDic> listSysDic=findByCondition(condition);
				if(!listSysDic.isEmpty()){
					return ResultGenerator.genFailResult("字典代码重复");
				}else{
					if(StringUtils.isBlank(sysDic.getIsleaf())){
						sysDic.setIsleaf(Constants.ISLEAF_NO);
					}
					sysDic.setCorpCd(user.getCorpCD());
					sysDicMapper.insertSelective(sysDic);
				}
			}
		}
		if(!list.get(0).getDicParentid().equals("xxx")){//排除根节点
			asyncTaskServcie.executeAsyncTaskPlus(list.get(0).getDicParentid());
		}
		return ResultGenerator.genSuccessResult("添加成功");
	}
	@Override
	public List<SysDic> querySysDicById(String parentId) {
		// TODO Auto-generated method stub		
		Condition condition = new Condition(SysDic.class);
		condition.createCriteria().andEqualTo("dicParentid",parentId).andEqualTo("dicIsenable",Constants.ISENABLE_YES);		
		return findByCondition(condition);		
	}
	@Override
	public List<SysDic> selectDic(String parentId) {
		Condition condition = new Condition(SysDic.class);
		condition.createCriteria().andEqualTo("dicParentid", parentId);
		condition.createCriteria().andEqualTo("dicIsenable", "1");
		List<SysDic> list = findByCondition(condition);
		return list;
	}
	@Override
	public SysDic selectDic(SysDic dic) {
		return findById(dic);
	}

}
