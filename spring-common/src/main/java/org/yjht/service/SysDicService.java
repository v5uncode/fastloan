package org.yjht.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.yjht.bean.SysDic;
import org.yjht.core.Result;

public interface SysDicService {
	/**
	 * 根据父id查找所有子集
	 * @param parentId
	 * @return
	 */
	Result finSysDicByParentId(String parentId);
	Result querySysDicTree();
	Result updateSysDic(SysDic dic);
	Result deleteSysDic(SysDic sysdic);
	Result saveSysDic(HttpServletRequest request,List<SysDic> list);
	List<SysDic> querySysDicById(String parentId);
	/**
	 * 根据主键查找
	 * @param dic
	 * @return
	 */
	SysDic selectDic(SysDic dic);
	/**
	 * 查找下层子集
	 * @param parentId
	 * @return
	 */
	List<SysDic>  selectDic(String parentId);


}
