package org.yjht.util;

import org.junit.Test;
import org.yjht.util.xml.Head;
import org.yjht.util.xml.XmlUtils;

/**
 * @author lengleng
 * @date 2018/5/17
 */
public class XmlUtilsTest {

    @Test
    public void object2XML() {
        Head head = new Head();
        head.setTrxtype("2000");
        head.setApp_info("123");
        head.setBrn_no("!23");
        head.setDestinst("!23");
        head.setOpe_no("123");
        head.setSeqno("123");
        head.setSource("123");

        Demo demo = new Demo();
        demo.setUsername("!23");
        demo.setPassword("123");

        System.out.println(XmlUtils.req2XML(head, demo));


    }

    @Test
    public void test2() {

    }
}