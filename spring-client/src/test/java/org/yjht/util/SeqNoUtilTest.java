package org.yjht.util;

import cn.hutool.core.thread.ThreadUtil;
import org.junit.Test;

import java.util.concurrent.ExecutorService;

import static org.junit.Assert.*;

/**
 * @author lengleng
 * @date 2018/6/1
 */
public class SeqNoUtilTest {

    public static void main(String[] args) {
        ExecutorService executorService = ThreadUtil.newExecutor(10);
        for (int i = 0; i < 10; i++) {
            executorService.execute(() -> System.out.println(SeqNoUtil.xySerial(16)));
        }
    }
}