package org.yjht.util;

import lombok.Data;

/**
 * @author lengleng
 * @date 2018/5/17
 */
@Data
public class Demo {
    private String username;
    private String password;
}
