package org.yjht.xindai.bean;

import cn.hutool.core.util.RandomUtil;
import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpResponse;
import org.junit.Test;
import org.yjht.util.xml.Head;
import org.yjht.util.xml.XmlUtils;
import org.yjht.xindai.common.req.XingYe2001;

/**
 * @author lengleng
 * @date 2018/5/22
 * 2000信贷客户信息实时同步单元测试
 */
public class XingYe2001Test {
    @Test
    public void testXml() {
        XingYe2001 xingYe2001 = new XingYe2001();
        String hth=RandomUtil.randomString(14);
        System.out.println(hth);
        xingYe2001.setHth("hb180620000008");
        xingYe2001.setZjh("370406199307106059");
        xingYe2001.setJgh("0001");
        xingYe2001.setPz("108201");
        xingYe2001.setFkje("100000");
        xingYe2001.setFkqx("010100");
        xingYe2001.setHkfs("1");
        xingYe2001.setHkjg("0");
        xingYe2001.setHkr("15");
        xingYe2001.setJzll("1");
        xingYe2001.setFdbl("0");
        xingYe2001.setZxll("1");
        xingYe2001.setLltzfs("0");
        xingYe2001.setFxbl("1000");
        xingYe2001.setFxll("1000");
        xingYe2001.setDflbz("0");
        xingYe2001.setHytx("U");
        xingYe2001.setJkrsx("5");
        xingYe2001.setKxqbz("0");
        xingYe2001.setKxts("0");
        xingYe2001.setKhjlh("zhaolulu");
        xingYe2001.setDkyt("1");
        xingYe2001.setDkms("测试");
        xingYe2001.setDbfs("01110");
        xingYe2001.setFkzh("298010100100006926");//对私298010126200000620   对公298010100100006926
        xingYe2001.setHkzh("298010126200000620");


        Head head = new Head();
        head.setTrxtype("2001");
        head.setDestinst("29100");
        head.setSource("wd");
        head.setSeqno(RandomUtil.randomString(16));
        head.setBrn_no("29802");
        head.setOpe_no("zhaolulu");
        String reqXml = XmlUtils.req2XML(head,xingYe2001);
        System.out.println(reqXml);

        //链式构建请求
        String url = "http://163.1.17.90:8082/loan/oLoanSync";
        HttpResponse response = HttpRequest.post(url)
                .contentType("text/plain; charset=utf-8")
                .body(reqXml)
                .execute();
        System.out.println(response.getStatus());
        System.out.println(response.body());
    }
}