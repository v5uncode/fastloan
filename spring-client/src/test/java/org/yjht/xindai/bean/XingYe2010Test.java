package org.yjht.xindai.bean;

import org.junit.Test;
import org.yjht.util.xml.Head;
import org.yjht.util.xml.XmlUtils;
import org.yjht.xindai.common.req.XingYe2010;

import cn.hutool.core.util.RandomUtil;
import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpResponse;

public class XingYe2010Test {
	@Test
    public void testXml() {
		XingYe2010 xy2010=new XingYe2010("hb180620000007","1","370406199307106059");
		Head head = new Head();
        head.setTrxtype("2010");
        head.setDestinst("29100");
        head.setSource("wd");
        head.setSeqno(RandomUtil.randomString(16));
        head.setBrn_no("29802");
        head.setOpe_no("test01");
        String reqXml = XmlUtils.req2XML(head,xy2010);
        System.out.println(reqXml);

        //链式构建请求
        String url = "http://163.1.17.90:8082/loan/oAppLoan";
        HttpResponse response = HttpRequest.post(url)
                .contentType("text/plain; charset=utf-8")
                .body(reqXml)
                .execute();
        System.out.println(response.getStatus());
        System.out.println(response.body());
		
		
	}

}
