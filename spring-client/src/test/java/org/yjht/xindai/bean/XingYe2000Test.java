package org.yjht.xindai.bean;

import cn.hutool.core.util.RandomUtil;
import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpResponse;
import org.junit.Test;
import org.yjht.util.xml.Head;
import org.yjht.util.xml.Response;
import org.yjht.util.xml.XmlUtils;
import org.yjht.xindai.common.req.XingYe2000;

import java.io.IOException;

/**
 * @author lengleng
 * @date 2018/5/22
 * 2000信贷客户信息实时同步单元测试
 */
public class XingYe2000Test {
    @Test
    public void testXml() {
        XingYe2000 xingYe2000 = new XingYe2000();
        xingYe2000.setZjlx("1");
        xingYe2000.setZjhm("370782199302035223");
        xingYe2000.setKhmc("李欣");
        xingYe2000.setXb("2");
        xingYe2000.setMz("00");
        xingYe2000.setHyzk("10");
        xingYe2000.setHjxz("1");
        xingYe2000.setHjdz("山东潍坊");
        xingYe2000.setZgxl("10");
        xingYe2000.setZgxw("3");
        xingYe2000.setJkzk("2");
        xingYe2000.setZw("1");
        xingYe2000.setZc("1");
        xingYe2000.setGwxz("1");
        xingYe2000.setGznx("1002");
        xingYe2000.setJznx("1002");
        xingYe2000.setJtrk("5");
        xingYe2000.setJtzz("山东潍坊");
        xingYe2000.setZzxz("1");
        xingYe2000.setJtyb("277518");
        xingYe2000.setJtdh("18369656568");
        xingYe2000.setSj("18369656568");
        xingYe2000.setDwxz("1");
        xingYe2000.setGzdw("云集汇通");
        xingYe2000.setGsgm("2");
        xingYe2000.setHy("A");
        xingYe2000.setDwdz("山东潍坊");
        xingYe2000.setKhfl("AAA");
        xingYe2000.setSfbhyg("0");
        xingYe2000.setFhh("29802");
        xingYe2000.setZhh("29802");
        xingYe2000.setZy("0");

        xingYe2000.setYsr("8000");
        xingYe2000.setJtsr("15000");
        xingYe2000.setJtwdsr("15000");
        xingYe2000.setJtzj("0");
        xingYe2000.setYzc("2000");


        Head head = new Head();
        head.setTrxtype("2000");
        head.setDestinst("29100");
        head.setSource("wd");
        head.setSeqno(RandomUtil.randomString(16));
        head.setBrn_no("29802");
        head.setOpe_no("test01");
        String reqXml = XmlUtils.req2XML(head,xingYe2000);
        System.out.println(reqXml);
        //链式构建请求
        String url = "http://163.1.17.90:8082/loan/oCustSync";
        HttpResponse response = HttpRequest.post(url)
                .contentType("text/plain; charset=utf-8")
                .body(reqXml)
                .execute();
        System.out.println(response.getStatus());
        System.out.println(response.body());
    }

    @Test
    public void parseXml() throws IOException {
        String respXml = "<respMessage><head><app_info></app_info><seqno>6twaud0xa12zpvsw</seqno><ret_code>000000</ret_code><ret_desc>成功!</ret_desc><source>wd</source><trxtype>2000</trxtype><ope_no>test01</ope_no><destinst>29100</destinst><brn_no>29802</brn_no></head></respMessage>";

        Response<Demo> response = XmlUtils.xml2Resp(respXml,null);
        System.out.println(response.getHead());
        System.out.println(response.getBody());
    }
}