package org.yjht.xindai.bean;

import cn.hutool.core.util.RandomUtil;
import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpResponse;
import org.junit.Test;
import org.yjht.util.xml.Head;
import org.yjht.util.xml.XmlUtils;
import org.yjht.xindai.common.req.XingYe2017;

/**
 * @author lengleng
 * @date 2018/5/22
 * 2017信贷客户关系人关联
 */
public class XingYe2017Test {
    @Test
    public void testXml() {
        XingYe2017 xingYe2017 = new XingYe2017();
        xingYe2017.setGx("6");
        xingYe2017.setKjrzjlx("1");
        xingYe2017.setKjrzjh("370406199307106059");
        xingYe2017.setGxrzjh("370782199302035223");
        xingYe2017.setGxrzjlx("1");

        Head head = new Head();
        head.setTrxtype("2017");
        head.setDestinst("29100");
        head.setSource("wd");
        head.setSeqno(RandomUtil.randomString(16));
        head.setBrn_no("29802");
        head.setOpe_no("test01");
        String reqXml = XmlUtils.req2XML(head,xingYe2017);

        //链式构建请求
        String url = "http://163.1.17.90:8082/loan/oCustRelate";
        HttpResponse response = HttpRequest.post(url)
                .contentType("text/plain; charset=utf-8")
                .body(reqXml)
                .execute();
        System.out.println(response.getStatus());
        System.out.println(response.body());
    }
}