package org.yjht.xindai.bean;

import cn.hutool.core.util.RandomUtil;
import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpResponse;
import org.junit.Test;
import org.yjht.util.xml.Head;
import org.yjht.util.xml.XmlUtils;
import org.yjht.xindai.common.req.XingYe2003;

/**
 * @author lengleng
 * @date 2018/5/23
 * 2003抵押信息同步
 */
public class XingYe2003Test {
    @Test
    public void testXml() {
        XingYe2003 xingYe2003 = new XingYe2003();


        Head head = new Head();
        head.setTrxtype("2003");
        head.setDestinst("29100");
        head.setSource("wd");
        head.setSeqno(RandomUtil.randomString(16));
        head.setBrn_no("29802");
        head.setOpe_no("test01");
        String reqXml = XmlUtils.req2XML(head,xingYe2003);

        //链式构建请求
        String url = "http://163.1.17.90:8082/loan/oMortgageSync";
        HttpResponse response = HttpRequest.post(url)
                .contentType("text/plain; charset=utf-8")
                .body(reqXml)
                .execute();
        System.out.println(response.getStatus());
        System.out.println(response.body());
    }
}