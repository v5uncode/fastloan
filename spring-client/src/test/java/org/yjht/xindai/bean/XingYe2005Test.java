package org.yjht.xindai.bean;

import cn.hutool.core.util.RandomUtil;
import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpResponse;
import org.junit.Test;
import org.yjht.util.xml.Head;
import org.yjht.util.xml.XmlUtils;
import org.yjht.xindai.common.req.XingYe2005;

public class XingYe2005Test {
    @Test
    public void test2005(){
        XingYe2005 test2005=new XingYe2005();
        test2005.setHth("cx5ngc7gz9cc6y");
        test2005.setZjhm("370406199307106059");
        test2005.setZjlx("1");
        Head head = new Head();
        head.setTrxtype("2005");
        head.setDestinst("29100");
        head.setSource("wd");
        head.setSeqno(RandomUtil.randomString(16));
        head.setBrn_no("29802");
        head.setOpe_no("test01");
        String reqXml = XmlUtils.req2XML(head,test2005);
        System.err.println(reqXml);
        //链式构建请求
        String url = "http://163.1.17.90:8082/loan/oQryLoanStat";
        HttpResponse response = HttpRequest.post(url)
                .contentType("text/plain; charset=utf-8")
                .body(reqXml)
                .execute();
        System.out.println(response.getStatus());
        System.err.println(response.body());
    }
}
