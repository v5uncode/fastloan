package org.yjht.mapper;

import org.yjht.bean.RuleDef;
import org.yjht.common.MyMapper;

public interface RuleDefMapper extends MyMapper<RuleDef> {
}