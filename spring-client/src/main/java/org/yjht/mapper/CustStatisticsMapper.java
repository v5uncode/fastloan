package org.yjht.mapper;

import java.util.List;
import java.util.Map;

import org.yjht.bean.CustStatistics;
import org.yjht.common.MyMapper;

public interface CustStatisticsMapper extends MyMapper<CustStatistics> {

	List<Map<String,Object>> queryCustSum(String userId);

	List<Map<String, Object>> queryOrgCustSum(Map<String, Object> map);

	List<Map<String, Object>> queryJlCustSum(Map<String, Object> map);

}