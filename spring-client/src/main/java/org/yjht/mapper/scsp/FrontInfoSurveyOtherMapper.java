package org.yjht.mapper.scsp;

import org.yjht.bean.scsp.FrontInfoSurveyOther;
import org.yjht.common.MyMapper;

public interface FrontInfoSurveyOtherMapper extends MyMapper<FrontInfoSurveyOther> {
}