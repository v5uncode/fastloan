package org.yjht.mapper.scsp;

import org.yjht.bean.scsp.FrontInfoSurvey;
import org.yjht.common.MyMapper;

public interface FrontInfoSurveyMapper extends MyMapper<FrontInfoSurvey> {
}