package org.yjht.mapper.scsp;

import org.yjht.bean.scsp.FrontInfoSurveyBusiness;
import org.yjht.common.MyMapper;

public interface FrontInfoSurveyBusinessMapper extends MyMapper<FrontInfoSurveyBusiness> {
}