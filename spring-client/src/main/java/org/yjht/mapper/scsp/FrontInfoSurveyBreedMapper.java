package org.yjht.mapper.scsp;

import org.yjht.common.MyMapper;
import org.yjht.bean.scsp.FrontInfoSurveyBreed;

public interface FrontInfoSurveyBreedMapper extends MyMapper<FrontInfoSurveyBreed> {
}