package org.yjht.mapper.scsp;

import org.yjht.bean.scsp.FrontInfoCustDzyr;
import org.yjht.common.MyMapper;

public interface FrontInfoCustDzyrMapper extends MyMapper<FrontInfoCustDzyr> {
}