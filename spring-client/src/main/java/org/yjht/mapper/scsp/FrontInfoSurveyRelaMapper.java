package org.yjht.mapper.scsp;

import org.yjht.bean.scsp.FrontInfoSurveyRela;
import org.yjht.common.MyMapper;

public interface FrontInfoSurveyRelaMapper extends MyMapper<FrontInfoSurveyRela> {
}