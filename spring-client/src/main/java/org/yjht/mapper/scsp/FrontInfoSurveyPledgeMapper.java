package org.yjht.mapper.scsp;

import org.yjht.bean.scsp.FrontInfoSurveyPledge;
import org.yjht.common.MyMapper;

public interface FrontInfoSurveyPledgeMapper extends MyMapper<FrontInfoSurveyPledge> {
}