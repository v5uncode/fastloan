package org.yjht.mapper.scsp;

import org.yjht.bean.scsp.FrontInfoSurveyMortgage;
import org.yjht.common.MyMapper;

public interface FrontInfoSurveyMortgageMapper extends MyMapper<FrontInfoSurveyMortgage> {
}