package org.yjht.mapper.scsp;

import org.yjht.common.MyMapper;
import org.yjht.bean.scsp.FrontInfoSurveyPlant;

public interface FrontInfoSurveyPlantMapper extends MyMapper<FrontInfoSurveyPlant> {
}