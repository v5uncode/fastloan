package org.yjht.mapper.scsp;

import org.yjht.bean.scsp.FrontInfoCredit;
import org.yjht.common.MyMapper;

public interface FrontInfoCreditMapper extends MyMapper<FrontInfoCredit> {
}