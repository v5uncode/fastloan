package org.yjht.mapper.scsp;

import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.yjht.bean.scsp.FrontInfoGuarantor;
import org.yjht.common.MyMapper;

/**
 * @author shixinfeng
 */
public interface FrontInfoGuarantorMapper extends MyMapper<FrontInfoGuarantor> {
	Map<String,Object> getCustInfo(@Param(value = "idNo") String idNo);
	
	Map<String,Object> getWorkYears(@Param(value = "custId") String custId,@Param(value = "bizHy") String bizHy);

	Map<String,Object> getPlantWorkYears(@Param(value = "custId")String custId,@Param(value = "flag") boolean flag);

	Map<String,Object> getBreedWorkYears(@Param(value = "custId")String custId,@Param(value = "yzType")String yzType,@Param(value = "yzKind")String yzKind,@Param(value = "flag") boolean flag);

    Map<String,Object> getFinancial(String custId);
}