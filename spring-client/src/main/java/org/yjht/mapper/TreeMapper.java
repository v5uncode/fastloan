package org.yjht.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

public interface TreeMapper {
	
	List<Map<String,Object>> findTreeJL(String corpCd);
	
	List<Map<String,Object>> findTreeOrg(String corpCd);
	
	List<Map<String,Object>> findUserAndOrg(@Param(value="corpCd") String corpCd,@Param(value="userName") String userName);
	
}
