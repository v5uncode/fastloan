package org.yjht.mapper;

import org.yjht.bean.scsp.FrontInfoCustImpdate;
import org.yjht.common.MyMapper;

public interface FrontInfoCustImpdateMapper extends MyMapper<FrontInfoCustImpdate> {
}