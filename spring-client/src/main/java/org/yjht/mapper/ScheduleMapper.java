package org.yjht.mapper;

import java.util.List;
import java.util.Map;

import org.yjht.bean.Schedule;
import org.yjht.common.MyMapper;

public interface ScheduleMapper extends MyMapper<Schedule> {

	List<Map<String, Object>> queryNum(Schedule schedule);
}