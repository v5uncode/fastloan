package org.yjht.mapper;

import org.yjht.bean.FrontInfoLoan;
import org.yjht.common.MyMapper;

public interface FrontInfoLoanMapper extends MyMapper<FrontInfoLoan> {
}