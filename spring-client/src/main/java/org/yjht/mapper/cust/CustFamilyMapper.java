package org.yjht.mapper.cust;

import org.yjht.bean.cust.CustFamily;
import org.yjht.common.MyMapper;

public interface CustFamilyMapper extends MyMapper<CustFamily> {
}