package org.yjht.mapper.cust;

import java.math.BigDecimal;

import org.apache.ibatis.annotations.Param;
import org.yjht.bean.cust.CustPledge;
import org.yjht.common.MyMapper;

public interface CustPledgeMapper extends MyMapper<CustPledge> {
	/**
	 * 质押担保主债权
	 * @param custId
	 * @return
	 */
	BigDecimal selectPledgeValue(@Param(value="custId") String custId);
}