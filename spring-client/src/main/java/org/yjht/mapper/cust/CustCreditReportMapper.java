package org.yjht.mapper.cust;

import org.apache.ibatis.annotations.Param;
import org.yjht.bean.cust.CustCreditReport;
import org.yjht.common.MyMapper;

public interface CustCreditReportMapper extends MyMapper<CustCreditReport> {

    /**
     * 信贷接口:根据客户证件号、证件类型查询征信信息
     * @param idNo
     * @param idType
     * @return
     */
    CustCreditReport getCreditInfoByIdNo(@Param(value = "idNo") String idNo,@Param(value = "idType") String idType);
}