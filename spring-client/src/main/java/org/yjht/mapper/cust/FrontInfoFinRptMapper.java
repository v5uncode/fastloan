package org.yjht.mapper.cust;

import org.yjht.bean.cust.FrontInfoFinRpt;
import org.yjht.common.MyMapper;

public interface FrontInfoFinRptMapper extends MyMapper<FrontInfoFinRpt> {
}