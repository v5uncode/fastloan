package org.yjht.mapper.cust;

import org.yjht.bean.cust.FrontInfoCustDoc;
import org.yjht.common.MyMapper;

public interface FrontInfoCustDocMapper extends MyMapper<FrontInfoCustDoc> {
}