package org.yjht.mapper.cust;

import org.yjht.bean.cust.CustPlant;
import org.yjht.common.MyMapper;

public interface CustPlantMapper extends MyMapper<CustPlant> {
}