package org.yjht.mapper.cust;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.yjht.bean.cust.FrontInfoCustYj;
import org.yjht.common.MyMapper;

public interface FrontInfoCustYjMapper extends MyMapper<FrontInfoCustYj> {
	List<Map<String,Object>> findYjInfo(@Param(value="userId") String userId,@Param(value="stat") String stat);
	
	Map<String,Object> findGjInfo(@Param(value="userId") String userId);
}