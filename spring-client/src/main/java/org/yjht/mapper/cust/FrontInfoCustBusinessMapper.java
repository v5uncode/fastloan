package org.yjht.mapper.cust;

import org.apache.ibatis.annotations.Param;
import org.yjht.bean.cust.FrontInfoCustBusiness;
import org.yjht.common.MyMapper;

public interface FrontInfoCustBusinessMapper extends MyMapper<FrontInfoCustBusiness> {

    FrontInfoCustBusiness getBusinessByHy(@Param(value = "custId") String custId, @Param(value = "bizHy") String bizHy);
}