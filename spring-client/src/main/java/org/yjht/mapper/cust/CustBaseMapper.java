package org.yjht.mapper.cust;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.yjht.bean.cust.CustBase;
import org.yjht.common.MyMapper;


public interface CustBaseMapper extends MyMapper<CustBase> {
	List<Map<String,Object>> selectList(CustBase cust);
	List<Map<String,Object>> selectBykey(Map<String,Object> map);
	List<Map<String,Object>> findCustByJL(String userIdSq);
	int updateCustJL(Map<String,Object> map);	
	int updCustBaseMtnData(@Param(value="mtnDate") String mtnDate,@Param(value="custId") String custId);

}