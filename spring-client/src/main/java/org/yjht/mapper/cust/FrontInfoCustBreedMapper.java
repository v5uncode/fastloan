package org.yjht.mapper.cust;

import org.apache.ibatis.annotations.Param;
import org.yjht.bean.cust.FrontInfoCustBreed;
import org.yjht.common.MyMapper;

public interface FrontInfoCustBreedMapper extends MyMapper<FrontInfoCustBreed> {

    String getBreedOtherYears(@Param(value = "custId") String custId, @Param(value = "yzType") String yzType,@Param(value = "yzKind") String yzKind);
}