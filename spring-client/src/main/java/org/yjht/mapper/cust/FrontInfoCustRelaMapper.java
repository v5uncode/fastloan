package org.yjht.mapper.cust;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.yjht.bean.cust.FrontInfoCustRela;
import org.yjht.common.MyMapper;

public interface FrontInfoCustRelaMapper extends MyMapper<FrontInfoCustRela> {
	int deleteRela(@Param(value = "custId") String custId,@Param(value = "seqNo") String seqNo);
	List<Map<String,Object>> getRelaLocation(@Param(value = "idNo") String idNo,@Param(value = "relaType") String relaType);
}