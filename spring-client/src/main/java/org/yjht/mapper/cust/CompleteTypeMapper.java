package org.yjht.mapper.cust;

import java.util.List;
import java.util.Map;

import org.yjht.bean.cust.CompleteType;
import org.yjht.common.MyMapper;

public interface CompleteTypeMapper extends MyMapper<CompleteType> {
	Integer getTotal(String type);
	List<Map<String,Object>>  getFormTotal(); 
}