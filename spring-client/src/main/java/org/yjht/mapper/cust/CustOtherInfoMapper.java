package org.yjht.mapper.cust;

import org.yjht.bean.cust.CustOtherInfo;
import org.yjht.common.MyMapper;

public interface CustOtherInfoMapper extends MyMapper<CustOtherInfo> {
}