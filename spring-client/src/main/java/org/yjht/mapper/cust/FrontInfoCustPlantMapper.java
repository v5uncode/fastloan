package org.yjht.mapper.cust;

import org.apache.ibatis.annotations.Param;
import org.yjht.bean.cust.FrontInfoCustPlant;
import org.yjht.common.MyMapper;

import java.util.List;
import java.util.Map;

public interface FrontInfoCustPlantMapper extends MyMapper<FrontInfoCustPlant> {

    List<Map<String,Object>> getZzms(@Param(value = "custId") String custId, @Param(value = "cropType") String cropType,@Param(value = "flag") String flag);

    String getPlantYears(@Param(value = "custId") String custId, @Param(value = "cropType") String cropType);

    String getOtherPlantYears(@Param(value = "custId") String custId, @Param(value = "cropType") String cropType);
}