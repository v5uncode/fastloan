package org.yjht.mapper.cust;

import org.yjht.bean.cust.FrontInfoCustStaff;
import org.yjht.common.MyMapper;

public interface FrontInfoCustStaffMapper extends MyMapper<FrontInfoCustStaff> {
}