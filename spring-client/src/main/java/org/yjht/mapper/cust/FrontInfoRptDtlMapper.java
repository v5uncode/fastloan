package org.yjht.mapper.cust;

import org.springframework.data.repository.query.Param;
import org.yjht.bean.cust.FrontInfoRptDtl;
import org.yjht.common.MyMapper;

import java.util.List;
import java.util.Map;

public interface FrontInfoRptDtlMapper extends MyMapper<FrontInfoRptDtl> {
	/**
	 * 根据客户id查询家庭收入详情
	 * @param custId
	 * @return
	 */
    List<Map<String,String>> findSrRptDtlById(@Param("custId") String custId);

    Integer getDtlInfoByCustId(@Param(value = "custId") String custId);
}