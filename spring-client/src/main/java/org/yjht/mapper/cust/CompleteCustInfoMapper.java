package org.yjht.mapper.cust;

import java.util.Map;

import org.yjht.bean.cust.CompleteCustInfo;
import org.yjht.common.MyMapper;

public interface CompleteCustInfoMapper extends MyMapper<CompleteCustInfo> {
	Map<String,Object> findMainProject(String custId);
	//是否结婚
	String findJhFlag(String custId);
}