package org.yjht.mapper.cust;

import org.yjht.bean.cust.CompleteDetails;
import org.yjht.common.MyMapper;

public interface CompleteDetailsMapper extends MyMapper<CompleteDetails> {
}