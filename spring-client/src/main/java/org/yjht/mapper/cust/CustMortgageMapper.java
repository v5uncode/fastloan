package org.yjht.mapper.cust;

import java.math.BigDecimal;

import org.apache.ibatis.annotations.Param;
import org.yjht.bean.cust.CustMortgage;
import org.yjht.common.MyMapper;

public interface CustMortgageMapper extends MyMapper<CustMortgage> {
	/**
	 * 查询抵押额度担保主债权
	 * @param custId
	 * @return
	 */
	BigDecimal selectMortGageValue(@Param(value="custId") String custId);
}