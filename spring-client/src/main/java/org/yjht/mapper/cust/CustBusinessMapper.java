package org.yjht.mapper.cust;

import org.yjht.bean.cust.CustBusiness;
import org.yjht.common.MyMapper;

public interface CustBusinessMapper extends MyMapper<CustBusiness> {
}