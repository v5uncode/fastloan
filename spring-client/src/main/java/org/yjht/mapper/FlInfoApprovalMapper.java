package org.yjht.mapper;

import org.apache.ibatis.annotations.Param;
import org.yjht.bean.FlInfoApproval;
import org.yjht.common.MyMapper;

public interface FlInfoApprovalMapper extends MyMapper<FlInfoApproval> {
    /**
     * 微信进度查询：获取评级结果
     * @param idNo
     * @param applytime
     * @return
     */
    FlInfoApproval queryByIdNo(@Param(value = "idNo") String idNo, @Param(value = "applytime") String applytime);
	
}