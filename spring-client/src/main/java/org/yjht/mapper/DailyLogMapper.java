package org.yjht.mapper;

import org.yjht.bean.DailyLog;
import org.yjht.common.MyMapper;

public interface DailyLogMapper extends MyMapper<DailyLog> {
}