package org.yjht.mapper;

import java.util.List;
import java.util.Map;

public interface LrdPlatformMapper {

	List<Map<String, Object>> queryTx(String custGrpJl);
	
}
