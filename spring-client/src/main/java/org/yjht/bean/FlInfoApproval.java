package org.yjht.bean;

import java.math.BigDecimal;
import javax.persistence.*;

@Table(name = "fl_info_approval")
public class FlInfoApproval{

	/**
     * 业务编号-主键
     */
    @Id
    @Column(name = "BUSINESS_CODE")
    private String businessCode;

    /**
     * 身份证号
     */
    @Column(name = "ID_NO")
    private String idNo;

    /**
     * 机构名称
     */
    @Column(name = "ORG_NAME")
    private String orgName;

    /**
     * 主营项目
     */
    @Column(name = "ZYXM")
    private String zyxm;

    /**
     * 接口编码
     */
    @Column(name = "INTERCODE")
    private String intercode;

    /**
     * 客户编号
     */
    @Column(name = "CUST_ID")
    private String custId;

    /**
     * 客户经理编号
     */
    @Column(name = "CUST_GRP_GL")
    private String custGrpGl;

    /**
     * 客户经理名称
     */
    @Column(name = "CUST_GRP_GL_NAME")
    private String custGrpGlName;

    /**
     * 准入传入信息
     */
    @Column(name = "ZR_SENDMSG")
    private String zrSendmsg;

    /**
     * 准入结果
     */
    @Column(name = "ZR_RESULT")
    private String zrResult;

    /**
     * 准入结果编码
     */
    @Column(name = "ZR_CODE")
    private String zrCode;

    /**
     * 准入描述
     */
    @Column(name = "ZR_DESC")
    private String zrDesc;

    /**
     * 评级传入信息
     */
    @Column(name = "PJ_SENDMSG")
    private String pjSendmsg;

    /**
     * 评级等级
     */
    @Column(name = "PJ_JB")
    private String pjJb;

    /**
     * 评分卡类型
     */
    @Column(name = "PJ_MODEL_TYPE")
    private String pjModelType;

    /**
     * 评级分数
     */
    @Column(name = "PJ_LMT")
    private String pjLmt;

    /**
     * 评级结果编码
     */
    @Column(name = "PJ_CODE")
    private String pjCode;

    /**
     * 评级描述
     */
    @Column(name = "PJ_DESC")
    private String pjDesc;

    /**
     * 额度传入信息
     */
    @Column(name = "ED_SENDMSG")
    private String edSendmsg;

    /**
     * 最高额度
     */
    @Column(name = "ED_LMT")
    private BigDecimal edLmt;

    /**
     * 额度结果编码
     */
    @Column(name = "ED_CODE")
    private String edCode;

    /**
     * 额度结果描述
     */
    @Column(name = "ED_DESC")
    private String edDesc;

    /**
     * 初审传入信息
     */
    @Column(name = "CS_SENDMSG")
    private String csSendmsg;

    /**
     * 初审结果
     */
    @Column(name = "CS_RESULT")
    private String csResult;

    /**
     * 初审结果编码
     */
    @Column(name = "CS_CODE")
    private String csCode;

    /**
     * 初审描述
     */
    @Column(name = "CS_DESC")
    private String csDesc;

    /**
     * 信用额度
     */
    @Column(name = "CREDIT_AMOUNT")
    private BigDecimal creditAmount;

    /**
     * 信用利率
     */
    @Column(name = "CREDIT_RATE")
    private BigDecimal creditRate;

    /**
     * 亲情额度
     */
    @Column(name = "QQ_AMOUNT")
    private BigDecimal qqAmount;

    /**
     * 亲情利率
     */
    @Column(name = "QQ_RATE")
    private BigDecimal qqRate;

    /**
     * 抵（质）押额度
     */
    @Column(name = "PLEDGE_AMOUNT")
    private BigDecimal pledgeAmount;

    /**
     * 抵（质）押利率
     */
    @Column(name = "PLEDGE_RATE")
    private BigDecimal pledgeRate;

    /**
     * 计算日期
     */
    @Column(name = "CRT_DATE")
    private String crtDate;

    /**
     * 失效日期
     */
    @Column(name = "LOSE_DATE")
    private String loseDate;

    /**
     * 操作员编号
     */
    @Column(name = "OPERID")
    private String operid;

    /**
     * 申请额度
     */
    @Column(name = "APPLY_AMOUNT")
    private BigDecimal applyAmount;

    /**
     * 申请期限
     */
    @Column(name = "APPLY_TERM")
    private Integer applyTerm;

    /**
     * 是否有效 T：是  F：否
     */
    @Column(name = "STATE")
    private String state;

    /**
     * 信用初审代码
     */
    @Column(name = "CREDIT_CODE")
    private String creditCode;

    /**
     * 信用初审描述
     */
    @Column(name = "CREDIT_DESC")
    private String creditDesc;

    /**
     * 保证利率
     */
    @Column(name = "GUARANTEE_RATE")
    private BigDecimal guaranteeRate;

    /**
     * 保证额度
     */
    @Column(name = "GUARANTEE_AMOUNT")
    private BigDecimal guaranteeAmount;

    /**
     * 获取业务编号-主键
     *
     * @return BUSINESS_CODE - 业务编号-主键
     */
    public String getBusinessCode() {
        return businessCode;
    }

    /**
     * 设置业务编号-主键
     *
     * @param businessCode 业务编号-主键
     */
    public void setBusinessCode(String businessCode) {
        this.businessCode = businessCode == null ? null : businessCode.trim();
    }

    /**
     * 获取身份证号
     *
     * @return ID_NO - 身份证号
     */
    public String getIdNo() {
        return idNo;
    }

    /**
     * 设置身份证号
     *
     * @param idNo 身份证号
     */
    public void setIdNo(String idNo) {
        this.idNo = idNo == null ? null : idNo.trim();
    }

    /**
     * 获取机构名称
     *
     * @return ORG_NAME - 机构名称
     */
    public String getOrgName() {
        return orgName;
    }

    /**
     * 设置机构名称
     *
     * @param orgName 机构名称
     */
    public void setOrgName(String orgName) {
        this.orgName = orgName == null ? null : orgName.trim();
    }

    /**
     * 获取主营项目
     *
     * @return ZYXM - 主营项目
     */
    public String getZyxm() {
        return zyxm;
    }

    /**
     * 设置主营项目
     *
     * @param zyxm 主营项目
     */
    public void setZyxm(String zyxm) {
        this.zyxm = zyxm == null ? null : zyxm.trim();
    }

    /**
     * 获取接口编码
     *
     * @return INTERCODE - 接口编码
     */
    public String getIntercode() {
        return intercode;
    }

    /**
     * 设置接口编码
     *
     * @param intercode 接口编码
     */
    public void setIntercode(String intercode) {
        this.intercode = intercode == null ? null : intercode.trim();
    }

    /**
     * 获取客户编号
     *
     * @return CUST_ID - 客户编号
     */
    public String getCustId() {
        return custId;
    }

    /**
     * 设置客户编号
     *
     * @param custId 客户编号
     */
    public void setCustId(String custId) {
        this.custId = custId == null ? null : custId.trim();
    }

    /**
     * 获取客户经理编号
     *
     * @return CUST_GRP_GL - 客户经理编号
     */
    public String getCustGrpGl() {
        return custGrpGl;
    }

    /**
     * 设置客户经理编号
     *
     * @param custGrpGl 客户经理编号
     */
    public void setCustGrpGl(String custGrpGl) {
        this.custGrpGl = custGrpGl == null ? null : custGrpGl.trim();
    }

    /**
     * 获取客户经理名称
     *
     * @return CUST_GRP_GL_NAME - 客户经理名称
     */
    public String getCustGrpGlName() {
        return custGrpGlName;
    }

    /**
     * 设置客户经理名称
     *
     * @param custGrpGlName 客户经理名称
     */
    public void setCustGrpGlName(String custGrpGlName) {
        this.custGrpGlName = custGrpGlName == null ? null : custGrpGlName.trim();
    }

    /**
     * 获取准入传入信息
     *
     * @return ZR_SENDMSG - 准入传入信息
     */
    public String getZrSendmsg() {
        return zrSendmsg;
    }

    /**
     * 设置准入传入信息
     *
     * @param zrSendmsg 准入传入信息
     */
    public void setZrSendmsg(String zrSendmsg) {
        this.zrSendmsg = zrSendmsg == null ? null : zrSendmsg.trim();
    }

    /**
     * 获取准入结果
     *
     * @return ZR_RESULT - 准入结果
     */
    public String getZrResult() {
        return zrResult;
    }

    /**
     * 设置准入结果
     *
     * @param zrResult 准入结果
     */
    public void setZrResult(String zrResult) {
        this.zrResult = zrResult == null ? null : zrResult.trim();
    }

    /**
     * 获取准入结果编码
     *
     * @return ZR_CODE - 准入结果编码
     */
    public String getZrCode() {
        return zrCode;
    }

    /**
     * 设置准入结果编码
     *
     * @param zrCode 准入结果编码
     */
    public void setZrCode(String zrCode) {
        this.zrCode = zrCode == null ? null : zrCode.trim();
    }

    /**
     * 获取准入描述
     *
     * @return ZR_DESC - 准入描述
     */
    public String getZrDesc() {
        return zrDesc;
    }

    /**
     * 设置准入描述
     *
     * @param zrDesc 准入描述
     */
    public void setZrDesc(String zrDesc) {
        this.zrDesc = zrDesc == null ? null : zrDesc.trim();
    }

    /**
     * 获取评级传入信息
     *
     * @return PJ_SENDMSG - 评级传入信息
     */
    public String getPjSendmsg() {
        return pjSendmsg;
    }

    /**
     * 设置评级传入信息
     *
     * @param pjSendmsg 评级传入信息
     */
    public void setPjSendmsg(String pjSendmsg) {
        this.pjSendmsg = pjSendmsg == null ? null : pjSendmsg.trim();
    }

    /**
     * 获取评级等级
     *
     * @return PJ_JB - 评级等级
     */
    public String getPjJb() {
        return pjJb;
    }

    /**
     * 设置评级等级
     *
     * @param pjJb 评级等级
     */
    public void setPjJb(String pjJb) {
        this.pjJb = pjJb == null ? null : pjJb.trim();
    }

    /**
     * 获取评分卡类型
     *
     * @return PJ_MODEL_TYPE - 评分卡类型
     */
    public String getPjModelType() {
        return pjModelType;
    }

    /**
     * 设置评分卡类型
     *
     * @param pjModelType 评分卡类型
     */
    public void setPjModelType(String pjModelType) {
        this.pjModelType = pjModelType == null ? null : pjModelType.trim();
    }

    /**
     * 获取评级分数
     *
     * @return PJ_LMT - 评级分数
     */
    public String getPjLmt() {
        return pjLmt;
    }

    /**
     * 设置评级分数
     *
     * @param pjLmt 评级分数
     */
    public void setPjLmt(String pjLmt) {
        this.pjLmt = pjLmt == null ? null : pjLmt.trim();
    }

    /**
     * 获取评级结果编码
     *
     * @return PJ_CODE - 评级结果编码
     */
    public String getPjCode() {
        return pjCode;
    }

    /**
     * 设置评级结果编码
     *
     * @param pjCode 评级结果编码
     */
    public void setPjCode(String pjCode) {
        this.pjCode = pjCode == null ? null : pjCode.trim();
    }

    /**
     * 获取评级描述
     *
     * @return PJ_DESC - 评级描述
     */
    public String getPjDesc() {
        return pjDesc;
    }

    /**
     * 设置评级描述
     *
     * @param pjDesc 评级描述
     */
    public void setPjDesc(String pjDesc) {
        this.pjDesc = pjDesc == null ? null : pjDesc.trim();
    }

    /**
     * 获取额度传入信息
     *
     * @return ED_SENDMSG - 额度传入信息
     */
    public String getEdSendmsg() {
        return edSendmsg;
    }

    /**
     * 设置额度传入信息
     *
     * @param edSendmsg 额度传入信息
     */
    public void setEdSendmsg(String edSendmsg) {
        this.edSendmsg = edSendmsg == null ? null : edSendmsg.trim();
    }

    /**
     * 获取最高额度
     *
     * @return ED_LMT - 最高额度
     */
    public BigDecimal getEdLmt() {
        return edLmt;
    }

    /**
     * 设置最高额度
     *
     * @param edLmt 最高额度
     */
    public void setEdLmt(BigDecimal edLmt) {
        this.edLmt = edLmt;
    }

    /**
     * 获取额度结果编码
     *
     * @return ED_CODE - 额度结果编码
     */
    public String getEdCode() {
        return edCode;
    }

    /**
     * 设置额度结果编码
     *
     * @param edCode 额度结果编码
     */
    public void setEdCode(String edCode) {
        this.edCode = edCode == null ? null : edCode.trim();
    }

    /**
     * 获取额度结果描述
     *
     * @return ED_DESC - 额度结果描述
     */
    public String getEdDesc() {
        return edDesc;
    }

    /**
     * 设置额度结果描述
     *
     * @param edDesc 额度结果描述
     */
    public void setEdDesc(String edDesc) {
        this.edDesc = edDesc == null ? null : edDesc.trim();
    }

    /**
     * 获取初审传入信息
     *
     * @return CS_SENDMSG - 初审传入信息
     */
    public String getCsSendmsg() {
        return csSendmsg;
    }

    /**
     * 设置初审传入信息
     *
     * @param csSendmsg 初审传入信息
     */
    public void setCsSendmsg(String csSendmsg) {
        this.csSendmsg = csSendmsg == null ? null : csSendmsg.trim();
    }

    /**
     * 获取初审结果
     *
     * @return CS_RESULT - 初审结果
     */
    public String getCsResult() {
        return csResult;
    }

    /**
     * 设置初审结果
     *
     * @param csResult 初审结果
     */
    public void setCsResult(String csResult) {
        this.csResult = csResult == null ? null : csResult.trim();
    }

    /**
     * 获取初审结果编码
     *
     * @return CS_CODE - 初审结果编码
     */
    public String getCsCode() {
        return csCode;
    }

    /**
     * 设置初审结果编码
     *
     * @param csCode 初审结果编码
     */
    public void setCsCode(String csCode) {
        this.csCode = csCode == null ? null : csCode.trim();
    }

    /**
     * 获取初审描述
     *
     * @return CS_DESC - 初审描述
     */
    public String getCsDesc() {
        return csDesc;
    }

    /**
     * 设置初审描述
     *
     * @param csDesc 初审描述
     */
    public void setCsDesc(String csDesc) {
        this.csDesc = csDesc == null ? null : csDesc.trim();
    }

    /**
     * 获取信用额度
     *
     * @return CREDIT_AMOUNT - 信用额度
     */
    public BigDecimal getCreditAmount() {
        return creditAmount;
    }

    /**
     * 设置信用额度
     *
     * @param creditAmount 信用额度
     */
    public void setCreditAmount(BigDecimal creditAmount) {
        this.creditAmount = creditAmount;
    }

    /**
     * 获取信用利率
     *
     * @return CREDIT_RATE - 信用利率
     */
    public BigDecimal getCreditRate() {
        return creditRate;
    }

    /**
     * 设置信用利率
     *
     * @param creditRate 信用利率
     */
    public void setCreditRate(BigDecimal creditRate) {
        this.creditRate = creditRate;
    }

    /**
     * 获取亲情额度
     *
     * @return QQ_AMOUNT - 亲情额度
     */
    public BigDecimal getQqAmount() {
        return qqAmount;
    }

    /**
     * 设置亲情额度
     *
     * @param qqAmount 亲情额度
     */
    public void setQqAmount(BigDecimal qqAmount) {
        this.qqAmount = qqAmount;
    }

    /**
     * 获取亲情利率
     *
     * @return QQ_RATE - 亲情利率
     */
    public BigDecimal getQqRate() {
        return qqRate;
    }

    /**
     * 设置亲情利率
     *
     * @param qqRate 亲情利率
     */
    public void setQqRate(BigDecimal qqRate) {
        this.qqRate = qqRate;
    }

    /**
     * 获取抵（质）押额度
     *
     * @return PLEDGE_AMOUNT - 抵（质）押额度
     */
    public BigDecimal getPledgeAmount() {
        return pledgeAmount;
    }

    /**
     * 设置抵（质）押额度
     *
     * @param pledgeAmount 抵（质）押额度
     */
    public void setPledgeAmount(BigDecimal pledgeAmount) {
        this.pledgeAmount = pledgeAmount;
    }

    /**
     * 获取抵（质）押利率
     *
     * @return PLEDGE_RATE - 抵（质）押利率
     */
    public BigDecimal getPledgeRate() {
        return pledgeRate;
    }

    /**
     * 设置抵（质）押利率
     *
     * @param pledgeRate 抵（质）押利率
     */
    public void setPledgeRate(BigDecimal pledgeRate) {
        this.pledgeRate = pledgeRate;
    }

    /**
     * 获取计算日期
     *
     * @return CRE_DATE - 计算日期
     */
    public String getCrtDate() {
        return crtDate;
    }

    /**
     * 设置计算日期
     *
     * @param crtDate 计算日期
     */
    public void setCrtDate(String crtDate) {
        this.crtDate = crtDate == null ? null : crtDate.trim();
    }

    /**
     * 获取失效日期
     *
     * @return LOSE_DATE - 失效日期
     */
    public String getLoseDate() {
        return loseDate;
    }

    /**
     * 设置失效日期
     *
     * @param loseDate 失效日期
     */
    public void setLoseDate(String loseDate) {
        this.loseDate = loseDate == null ? null : loseDate.trim();
    }

    /**
     * 获取操作员编号
     *
     * @return OPERID - 操作员编号
     */
    public String getOperid() {
        return operid;
    }

    /**
     * 设置操作员编号
     *
     * @param operid 操作员编号
     */
    public void setOperid(String operid) {
        this.operid = operid == null ? null : operid.trim();
    }

    /**
     * 获取申请额度
     *
     * @return APPLY_AMOUNT - 申请额度
     */
    public BigDecimal getApplyAmount() {
        return applyAmount;
    }

    /**
     * 设置申请额度
     *
     * @param applyAmount 申请额度
     */
    public void setApplyAmount(BigDecimal applyAmount) {
        this.applyAmount = applyAmount;
    }

    /**
     * 获取申请期限
     *
     * @return APPLY_TERM - 申请期限
     */
    public Integer getApplyTerm() {
        return applyTerm;
    }

    /**
     * 设置申请期限
     *
     * @param applyTerm 申请期限
     */
    public void setApplyTerm(Integer applyTerm) {
        this.applyTerm = applyTerm;
    }

    /**
     * 获取是否有效 T：是  F：否
     *
     * @return STATE - 是否有效 T：是  F：否
     */
    public String getState() {
        return state;
    }

    /**
     * 设置是否有效 T：是  F：否
     *
     * @param state 是否有效 T：是  F：否
     */
    public void setState(String state) {
        this.state = state == null ? null : state.trim();
    }

    /**
     * 获取信用初审代码
     *
     * @return CREDIT_CODE - 信用初审代码
     */
    public String getCreditCode() {
        return creditCode;
    }

    /**
     * 设置信用初审代码
     *
     * @param creditCode 信用初审代码
     */
    public void setCreditCode(String creditCode) {
        this.creditCode = creditCode == null ? null : creditCode.trim();
    }

    /**
     * 获取信用初审描述
     *
     * @return CREDIT_DESC - 信用初审描述
     */
    public String getCreditDesc() {
        return creditDesc;
    }

    /**
     * 设置信用初审描述
     *
     * @param creditDesc 信用初审描述
     */
    public void setCreditDesc(String creditDesc) {
        this.creditDesc = creditDesc == null ? null : creditDesc.trim();
    }

    /**
     * 获取保证利率
     *
     * @return GUARANTEE_RATE - 保证利率
     */
    public BigDecimal getGuaranteeRate() {
        return guaranteeRate;
    }

    /**
     * 设置保证利率
     *
     * @param guaranteeRate 保证利率
     */
    public void setGuaranteeRate(BigDecimal guaranteeRate) {
        this.guaranteeRate = guaranteeRate;
    }

    /**
     * 获取保证额度
     *
     * @return GUARANTEE_AMOUNT - 保证额度
     */
    public BigDecimal getGuaranteeAmount() {
        return guaranteeAmount;
    }

    /**
     * 设置保证额度
     *
     * @param guaranteeAmount 保证额度
     */
    public void setGuaranteeAmount(BigDecimal guaranteeAmount) {
        this.guaranteeAmount = guaranteeAmount;
    }
}