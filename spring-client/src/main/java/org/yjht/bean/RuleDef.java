package org.yjht.bean;

import javax.persistence.*;

import lombok.Getter;
import lombok.Setter;

@Table(name = "RULE_DEF")
@Getter
@Setter
public class RuleDef {
    @Id
    @Column(name = "RULE_NO")
    private String ruleNo;
    
    @Column(name = "CORP_CD")
    private String corpCd;

    @Column(name = "RULE_NAME")
    private String ruleName;

    @Column(name = "PUB_DATE")
    private String pubDate;

    @Column(name = "PUB_DEPT")
    private String pubDept;

    @Column(name = "RULE_TYPE")
    private String ruleType;

    @Column(name = "FILE_NAME")
    private String fileName;

    @Column(name = "FILE_PATH")
    private String filePath;

    @Column(name = "CRT_DATE")
    private String crtDate;

    @Column(name = "MTN_DATE")
    private String mtnDate;

    @Column(name = "RULE_DESC")
    private String ruleDesc;

}