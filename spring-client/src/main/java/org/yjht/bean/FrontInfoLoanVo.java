package org.yjht.bean;

/**
 * @author lengleng
 * @date 2018/6/5
 */
public class FrontInfoLoanVo extends FrontInfoLoan {
    private String applyMoney;

    public String getApplyMoney() {
        return applyMoney;
    }

    public void setApplyMoney(String applyMoney) {
        this.applyMoney = applyMoney;
    }
}
