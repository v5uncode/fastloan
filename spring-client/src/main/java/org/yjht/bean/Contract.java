package org.yjht.bean;

import java.math.BigDecimal;

public class Contract {
	private String contractNum;

    private BigDecimal contractTotalAmt;

    private String custId;

    private String creditNature;

    private String creditProductCd;

    private String monthRate;

    private String loanTypeInstructionCd;

    private String contractProvidePurpose;

    private String isAgriculture;

    private BigDecimal creditLimit;

    private String loanOrganizeCd;

    private String payWay;

    private String bearingWay;

    private Integer contractTerm;

    private String handlingOrgCd;

    private String handlingUserNum;

    private BigDecimal basrRateFloan;

    private String startDate;

    private String expirationDate;

    private String contractId;

    private String bizNum;

    private String contractTermUnitCd;

    private String mainSuretyMode;

    private String mainSuertySubchild;

    private String classificationResult;

    private String towardTrade;

    public String getContractNum() {
        return contractNum;
    }

    public void setContractNum(String contractNum) {
        this.contractNum = contractNum == null ? null : contractNum.trim();
    }

    public BigDecimal getContractTotalAmt() {
        return contractTotalAmt;
    }

    public void setContractTotalAmt(BigDecimal contractTotalAmt) {
        this.contractTotalAmt = contractTotalAmt;
    }

    public String getCustId() {
        return custId;
    }

    public void setCustId(String custId) {
        this.custId = custId == null ? null : custId.trim();
    }

    public String getCreditNature() {
        return creditNature;
    }

    public void setCreditNature(String creditNature) {
        this.creditNature = creditNature == null ? null : creditNature.trim();
    }

    public String getCreditProductCd() {
        return creditProductCd;
    }

    public void setCreditProductCd(String creditProductCd) {
        this.creditProductCd = creditProductCd == null ? null : creditProductCd.trim();
    }

    public String getMonthRate() {
        return monthRate;
    }

    public void setMonthRate(String monthRate) {
        this.monthRate = monthRate == null ? null : monthRate.trim();
    }

    public String getLoanTypeInstructionCd() {
        return loanTypeInstructionCd;
    }

    public void setLoanTypeInstructionCd(String loanTypeInstructionCd) {
        this.loanTypeInstructionCd = loanTypeInstructionCd == null ? null : loanTypeInstructionCd.trim();
    }

    public String getContractProvidePurpose() {
        return contractProvidePurpose;
    }

    public void setContractProvidePurpose(String contractProvidePurpose) {
        this.contractProvidePurpose = contractProvidePurpose == null ? null : contractProvidePurpose.trim();
    }

    public String getIsAgriculture() {
        return isAgriculture;
    }

    public void setIsAgriculture(String isAgriculture) {
        this.isAgriculture = isAgriculture == null ? null : isAgriculture.trim();
    }

    public BigDecimal getCreditLimit() {
        return creditLimit;
    }

    public void setCreditLimit(BigDecimal creditLimit) {
        this.creditLimit = creditLimit;
    }

    public String getLoanOrganizeCd() {
        return loanOrganizeCd;
    }

    public void setLoanOrganizeCd(String loanOrganizeCd) {
        this.loanOrganizeCd = loanOrganizeCd == null ? null : loanOrganizeCd.trim();
    }

    public String getPayWay() {
        return payWay;
    }

    public void setPayWay(String payWay) {
        this.payWay = payWay == null ? null : payWay.trim();
    }

    public String getBearingWay() {
        return bearingWay;
    }

    public void setBearingWay(String bearingWay) {
        this.bearingWay = bearingWay == null ? null : bearingWay.trim();
    }

    public Integer getContractTerm() {
        return contractTerm;
    }

    public void setContractTerm(Integer contractTerm) {
        this.contractTerm = contractTerm;
    }

    public String getHandlingOrgCd() {
        return handlingOrgCd;
    }

    public void setHandlingOrgCd(String handlingOrgCd) {
        this.handlingOrgCd = handlingOrgCd == null ? null : handlingOrgCd.trim();
    }

    public String getHandlingUserNum() {
        return handlingUserNum;
    }

    public void setHandlingUserNum(String handlingUserNum) {
        this.handlingUserNum = handlingUserNum == null ? null : handlingUserNum.trim();
    }

    public BigDecimal getBasrRateFloan() {
        return basrRateFloan;
    }

    public void setBasrRateFloan(BigDecimal basrRateFloan) {
        this.basrRateFloan = basrRateFloan;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate == null ? null : startDate.trim();
    }

    public String getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate == null ? null : expirationDate.trim();
    }

    public String getContractId() {
        return contractId;
    }

    public void setContractId(String contractId) {
        this.contractId = contractId == null ? null : contractId.trim();
    }

    public String getBizNum() {
        return bizNum;
    }

    public void setBizNum(String bizNum) {
        this.bizNum = bizNum == null ? null : bizNum.trim();
    }

    public String getContractTermUnitCd() {
        return contractTermUnitCd;
    }

    public void setContractTermUnitCd(String contractTermUnitCd) {
        this.contractTermUnitCd = contractTermUnitCd == null ? null : contractTermUnitCd.trim();
    }

    public String getMainSuretyMode() {
        return mainSuretyMode;
    }

    public void setMainSuretyMode(String mainSuretyMode) {
        this.mainSuretyMode = mainSuretyMode == null ? null : mainSuretyMode.trim();
    }

    public String getMainSuertySubchild() {
        return mainSuertySubchild;
    }

    public void setMainSuertySubchild(String mainSuertySubchild) {
        this.mainSuertySubchild = mainSuertySubchild == null ? null : mainSuertySubchild.trim();
    }

    public String getClassificationResult() {
        return classificationResult;
    }

    public void setClassificationResult(String classificationResult) {
        this.classificationResult = classificationResult == null ? null : classificationResult.trim();
    }

    public String getTowardTrade() {
        return towardTrade;
    }

    public void setTowardTrade(String towardTrade) {
        this.towardTrade = towardTrade == null ? null : towardTrade.trim();
    }
}
