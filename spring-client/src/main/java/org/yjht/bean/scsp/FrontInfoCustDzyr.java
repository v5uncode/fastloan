package org.yjht.bean.scsp;

import javax.persistence.*;

@Table(name = "FRONT_INFO_CUST_DZYR")
public class FrontInfoCustDzyr {
    @Id
    @Column(name = "DZYR_ID")
    private String dzyrId;

    @Column(name = "SCSP_ID")
    private String scspId;

    @Column(name = "DZYR_NAME")
    private String dzyrName;

    @Column(name = "DZYR_ID_CARD")
    private String dzyrIdCard;

    @Column(name = "DZYR_TEL_NO")
    private String dzyrTelNo;

    @Column(name = "DZYW_NAME")
    private String dzywName;

    @Column(name = "DZYW_PGJZ")
    private String dzywPgjz;

    @Column(name = "DZYW_KDBYE")
    private String dzywKdbye;

    @Column(name = "DZYW_SJDBYE")
    private String dzywSjdbye;

    @Column(name = "DKYE")
    private String dkye;

    /**
     * @return DZYR_ID
     */
    public String getDzyrId() {
        return dzyrId;
    }

    /**
     * @param dzyrId
     */
    public void setDzyrId(String dzyrId) {
        this.dzyrId = dzyrId == null ? null : dzyrId.trim();
    }

    /**
     * @return SCSP_ID
     */
    public String getScspId() {
        return scspId;
    }

    /**
     * @param scspId
     */
    public void setScspId(String scspId) {
        this.scspId = scspId == null ? null : scspId.trim();
    }

    /**
     * @return DZYR_NAM
     */
    public String getDzyrName() {
        return dzyrName;
    }

    /**
     * @param dzyrNam
     */
    public void setDzyrNam(String dzyrName) {
        this.dzyrName = dzyrName == null ? null : dzyrName.trim();
    }

    /**
     * @return DZYR_ID_CARD
     */
    public String getDzyrIdCard() {
        return dzyrIdCard;
    }

    /**
     * @param dzyrIdCard
     */
    public void setDzyrIdCard(String dzyrIdCard) {
        this.dzyrIdCard = dzyrIdCard == null ? null : dzyrIdCard.trim();
    }

    /**
     * @return DZYR_TEL_NO
     */
    public String getDzyrTelNo() {
        return dzyrTelNo;
    }

    /**
     * @param dzyrTelNo
     */
    public void setDzyrTelNo(String dzyrTelNo) {
        this.dzyrTelNo = dzyrTelNo == null ? null : dzyrTelNo.trim();
    }

    /**
     * @return DZYW_NAME
     */
    public String getDzywName() {
        return dzywName;
    }

    /**
     * @param dzywName
     */
    public void setDzywName(String dzywName) {
        this.dzywName = dzywName == null ? null : dzywName.trim();
    }

    /**
     * @return DZYW_PGJZ
     */
    public String getDzywPgjz() {
        return dzywPgjz;
    }

    /**
     * @param dzywPgjz
     */
    public void setDzywPgjz(String dzywPgjz) {
        this.dzywPgjz = dzywPgjz == null ? null : dzywPgjz.trim();
    }

    /**
     * @return DZYW_KDBYE
     */
    public String getDzywKdbye() {
        return dzywKdbye;
    }

    /**
     * @param dzywKdbye
     */
    public void setDzywKdbye(String dzywKdbye) {
        this.dzywKdbye = dzywKdbye == null ? null : dzywKdbye.trim();
    }

    /**
     * @return DZYW_SJDBYE
     */
    public String getDzywSjdbye() {
        return dzywSjdbye;
    }

    /**
     * @param dzywSjdbye
     */
    public void setDzywSjdbye(String dzywSjdbye) {
        this.dzywSjdbye = dzywSjdbye == null ? null : dzywSjdbye.trim();
    }

    /**
     * @return DKYE
     */
    public String getDkye() {
        return dkye;
    }

    /**
     * @param dkye
     */
    public void setDkye(String dkye) {
        this.dkye = dkye == null ? null : dkye.trim();
    }
}