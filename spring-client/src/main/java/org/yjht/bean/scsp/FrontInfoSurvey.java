package org.yjht.bean.scsp;

import lombok.Getter;
import lombok.Setter;
import javax.persistence.*;
import java.math.BigDecimal;

@Table(name = "front_info_survey")
@Getter
@Setter
public class FrontInfoSurvey {
    /**
     * 报告ID-对应审查审批表
     */
    @Id
    @Column(name = "SCSP_ID")
    private String scspId;
    
    
    /**
     * 客户姓名
     */
    @Column(name = "CUST_NAME")
    private String custName;

    /**
     * 主营项目
     */
    @Column(name = "ZYXM")
    private String zyxm;

    /**
     * 客户ID
     */
    @Column(name = "CUST_ID")
    private String custId;

    /**
     * 客户性别
     */
    @Column(name = "CUST_SEX")
    private String sex;


    /**
     * 民族
     */
    @Column(name = "NATION")
    private String nation;

    /**
     * 婚姻状况
     */
    @Column(name = "MARRIAGE_FLAG")
    private String marriageFlag;

    /**
     * 结婚等级日期
     */
    @Column(name = "JH_DATE")
    private String marriageDate;
    /**
     * 证件类型
     */
    @Column(name = "ID_TYPE")
    private String idType;


    /**
     * 身份证号
     */
    @Column(name = "ID_NO")
    private String idNo;

    /**
     * 家庭人口
     */
    @Column(name = "FAMILY_COUNT")
    private Integer familyCount;

    /**
     * 家庭住址
     */
    @Column(name = "ADDRESS")
    private String address;

    /**
     * 申请金额
     */
    @Column(name = "APPLY_MONEY")
    private BigDecimal applyMoney;

    /**
     * 申请期限
     */
    @Column(name = "APPLY_LIMIT")
    private Integer applyLimit;
    /**
     * 联系电话
     */
    @Column(name = "TEL_NO")
    private String telNo;

    /**
     * 贷款用途
     */
    @Column(name = "PURPOSE")
    private String purpose;

    /**
     * 职业
     */
    @Column(name = "VOCATION")
    private String vocation;

    /**
     * 工作单位名称
     */
    @Column(name = "WORK_NAME")
    private String workName;

    /**
     * 职工从业年限
     */
    @Column(name = "WORK_YEARS")
    private Integer workYears;

    /**
     * 职工职务
     */
    @Column(name = "DUTY")
    private String duty;

    /**
     * 有无子女
     */
    @Column(name = "CHILD_FLAG")
    private String childFlag;

    /**
     * 发证机关
     */
    @Column(name = "GOV")
    private String gov;

    /**
     * 学历
     */
    @Column(name = "EDU_LEVEL")
    private String eduLevel;

    /**
     * 职工性质
     */
    @Column(name = "ZB_FLAG")
    private String zbFlag;

    /**
     * 工作单位地址
     */
    @Column(name = "WORK_ADDRESS")
    private String workAddress;

    /**
     * 营业执照名称
     */
    @Column(name = "LICENCE_NAME")
    private String licenceName;
    /**
     * 医疗保险缴纳金额
     */
    @Column(name = "YLBXAMT")
    private BigDecimal ylbxamt;

    /**
     * 营业执照号码
     */
    @Column(name = "LICENCE_NO")
    private String licenceNo;

    /**
     * 经营地址
     */
    @Column(name = "STORE_ADDRESS")
    private String storeAddress;

    /**
     * 借款人及家庭成员工作情况
     */
    @Column(name = "CUST_FAMILIES_STATUS")
    private String custFamiliesStatus;

    /**
     * 是否在城区购买商住商用房
     */
    @Column(name = "BUSINESS_HOME")
    private String businessHome;

    /**
     * 自有房产数量
     */
    @Column(name = "HOUSE_COUNT")
    private Integer houseCount ;


    /**
     * 上年家庭收入
     */
    @Column(name = "SR_1Y")
    private BigDecimal sr1y;

    /**
     * 上年家庭支出
     */
    @Column(name = "ZC_1Y")
    private BigDecimal zc1y;

    /**
     * 家庭总资产
     */
    @Column(name = "ZZC")
    private BigDecimal zzc;

    /**
     * 现金及银行存款
     */
    @Column(name = "XJJYHCK")
    private BigDecimal xjjyhck;

    /**
     * 存货
     */
    @Column(name = "CK")
    private BigDecimal ck;

    /**
     * 应收及预付账款
     */
    @Column(name = "YSYF")
    private Double ysyf;
    /**
     * 应付及预收账款
     */
    @Column(name = "YFYS")
    private Double yfys;

    /**
     * 固定资产
     */
    @Column(name = "GDZC")
    private BigDecimal gdzc;

    /**
     * 其它资产
     */
    @Column(name = "QTJYZC")
    private BigDecimal qtjyzc;

    /**
     * 总负债
     */
    @Column(name = "ZFZ")
    private BigDecimal zfz;

    /**
     * 银行贷款（长期贷款）
     */
    @Column(name = "CQDK")
    private BigDecimal cqdk;

    /**
     * 银行贷款（短期）
     */
    @Column(name = "DQDK")
    private BigDecimal dqdk;

    /**
     * 其他负债
     */
    @Column(name = "QTFZ")
    private BigDecimal qtfz;

    /**
     * 家庭或有负债
     */
    @Column(name = "JTHYFZ")
    private BigDecimal jthyfz;

    /**
     * 负面信息
     */
    @Column(name = "BAD_INFO")
    private String badInfo;

    /**
     * 其他负面信息
     */
    @Column(name = "BAD_OTHER_INFO")
    private String badOtherInfo;

    /**
     * 近三年家庭年均收入
     */
    @Column(name = "NJSR")
    private String njsr ;

    /**
     * 近三年家庭年均支出
     */
    @Column(name = "NJZC")
    private String njzc;


    /**
     * 成立日期
     */
    @Column(name = "CREATE_DATE")
    private String createDate;

    /**
     * 员工数量
     */
    @Column(name = "EMPLOYEE_COUNT")
    private Integer employeeCount;
    /**
     * 有保险员工数量
     */
    @Column(name = "BX_CNT")
    private Integer bxCnt;

    /**
     * 营运车数量
     */
    @Column(name = "YYC_NUM")
    private Integer yycNum;


    /**
     * 评级结果
     */
    @Column(name = "GRADE")
    private String grade;

    /**
     * 评级级别
     */
    @Column(name = "PJ_LMT")
    private String pjLmt;

    /**
     * 最高授信
     */
    @Column(name = "HIGH_CREDIT")
    private BigDecimal highCredit;
    /**
     * 信用额度
     */
    @Column(name = "CREDIT_LIMIT")
    private BigDecimal creditLimit;
    /**
     * 信用利率浮动比例(%)
     */
    @Column(name = "CREDIT_FLOAT_RATE")
    private BigDecimal creditFloatRate;
    /**
     * 信用一年期贷款日利率(‱)
     */
    @Column(name = "CREDIT_RATE")
    private Double creditRate;

    /**
     * 抵制押额度
     */
    @Column(name = "PLEDGE_LIMIT")
    private BigDecimal pledgeLimit;
    /**
     * 抵制押利率浮动比例(%)
     */
    @Column(name = "PLEDGE_FLOAT_RATE")
    private BigDecimal pledgeFloatRate;
    /**
     * 抵制押一年期贷款日利率(‱)
     */
    @Column(name = "PLEDGE_RATE")
    private Double pledgeRate;

    /**
     * 保证额度
     */
    @Column(name = "ENSURE_LIMIT")
    private BigDecimal ensureLimit;
    /**
     * 保证利率浮动比例(%)
     */
    @Column(name = "ENSURE_FLOAT_RATE")
    private BigDecimal ensureFloatRate;
    /**
     * 保证一年期贷款日利率(‱)
     */
    @Column(name = "ENSURE_RATE")
    private Double ensureRate;


    /**
     * 同意授信额度
     */
    @Column(name = "SURVEY_HIGH_LIMIT")
    private String surveyHighLimit;
    /**
     * 担保方式
     */
    @Column(name = "WARRANT_TYPE")
    private String warrantType;
    /**
     * 利率浮动比率
     */
    @Column(name = "RATE_FLOAT")
    private String rateFloat;
    /**
     * 一年期贷款月利率
     */
    @Column(name = "MON_RATE")
    private String monRate;
    /**
     * 日息为万分之 数字
     */
    @Column(name = "DAY_RATE")
    private String dayRate;
    /**
     * 日息为万分之  汉字
     */
    @Column(name = "MONEY_FOR_DAY")
    private String moneyForDay;
    /**
     * 授信期限
     */
    @Column(name = "SURVEY_DATE_LIMIT")
    private String surveyDateLimit;
    
    /**
     * 客户经理编号
     */
    @Column(name = "CUST_GRP_JL")
    private String custGrpJl;
    
    /**
     * 调查客户经理
     */
    @Column(name = "CUST_GRP_NAME")
    private String custGrpName;

    /**
     * 调查日期
     */
    @Column(name = "REPORT_DATE")
    private String reportDate;
    
    /**
     * 调查报告完成状态
     */
    @Column(name = "STATE")
    private String state;

}