package org.yjht.bean.scsp;

import lombok.Getter;
import lombok.Setter;
import javax.persistence.*;

@Table(name = "front_info_survey_rela")
@Getter
@Setter
public class FrontInfoSurveyRela {
    /**
     * 序号（编号）

     */
    @Id
    @Column(name = "SEQ_NO")
    private String seqNo;

    /**
     * 调查报告ID

     */
    @Column(name = "SCSP_ID")
    private String scspId;

    /**
     * 关系人姓名

     */
    @Column(name = "VS_NAME")
    private String vsName;

    /**
     * 家庭关系

     */
    @Column(name = "RELA_TYPE")
    private String relaType;

    /**
     * 证件类型

     */
    @Column(name = "ID_TYPE")
    private String idType;

    /**
     * 证件号码

     */
    @Column(name = "ID_NO")
    private String idNo;

    /**
     * 联系电话

     */
    @Column(name = "TEL_NO")
    private String telNo;



    /**
     * 备注

     */
    @Column(name = "OTH_DESC")
    private String othDesc;



    /**
     * 数据创建日期

     */
    @Column(name = "CRT_DATE")
    private String crtDate;

    /**
     * 数据修改日期

     */
    @Column(name = "MTN_DATE")
    private String mtnDate;

    /**
     * 性别
     */
    @Column(name = "SEX")
    private String sex;

    /**
     * 年龄
     */
    @Column(name = "AGE")
    private Integer age;

    /**
     * 是否删除
     */
    @Column(name = "ISDEL")
    private String isdel;

    /**
     * 工作单位
     */
    @Column(name = "WORK_ADDRESS")
    private String workAddress;


}