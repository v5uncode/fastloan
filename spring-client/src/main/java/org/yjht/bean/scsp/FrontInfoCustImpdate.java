package org.yjht.bean.scsp;

import javax.persistence.*;

@Table(name = "FRONT_INFO_CUST_IMPDATE")
public class FrontInfoCustImpdate {
    @Id
    @Column(name = "SEQ_NO")
    private String seqNo;

    @Column(name = "CUST_ID")
    private String custId;

    @Column(name = "DATE_YM")
    private String dateYm;

    @Column(name = "JNR_TYPE")
    private String jnrType;

    @Column(name = "WARN_DESC")
    private String warnDesc;

    @Column(name = "WARN_TYPE")
    private String warnType;

    @Column(name = "WARN_FLAG")
    private String warnFlag;

    @Column(name = "CRT_DATE")
    private String crtDate;

    @Column(name = "MTN_DATE")
    private String mtnDate;

    /**
     * @return SEQ_NO
     */
    public String getSeqNo() {
        return seqNo;
    }

    /**
     * @param seqNo
     */
    public void setSeqNo(String seqNo) {
        this.seqNo = seqNo == null ? null : seqNo.trim();
    }

    /**
     * @return CUST_ID
     */
    public String getCustId() {
        return custId;
    }

    /**
     * @param custId
     */
    public void setCustId(String custId) {
        this.custId = custId == null ? null : custId.trim();
    }

    /**
     * @return DATE_YM
     */
    public String getDateYm() {
        return dateYm;
    }

    /**
     * @param dateYm
     */
    public void setDateYm(String dateYm) {
        this.dateYm = dateYm == null ? null : dateYm.trim();
    }

    /**
     * @return JNR_TYPE
     */
    public String getJnrType() {
        return jnrType;
    }

    /**
     * @param jnrType
     */
    public void setJnrType(String jnrType) {
        this.jnrType = jnrType == null ? null : jnrType.trim();
    }

    /**
     * @return WARN_DESC
     */
    public String getWarnDesc() {
        return warnDesc;
    }

    /**
     * @param warnDesc
     */
    public void setWarnDesc(String warnDesc) {
        this.warnDesc = warnDesc == null ? null : warnDesc.trim();
    }

    /**
     * @return WARN_TYPE
     */
    public String getWarnType() {
        return warnType;
    }

    /**
     * @param warnType
     */
    public void setWarnType(String warnType) {
        this.warnType = warnType == null ? null : warnType.trim();
    }

    /**
     * @return WARN_FLAG
     */
    public String getWarnFlag() {
        return warnFlag;
    }

    /**
     * @param warnFlag
     */
    public void setWarnFlag(String warnFlag) {
        this.warnFlag = warnFlag == null ? null : warnFlag.trim();
    }

    /**
     * @return CRT_DATE
     */
    public String getCrtDate() {
        return crtDate;
    }

    /**
     * @param crtDate
     */
    public void setCrtDate(String crtDate) {
        this.crtDate = crtDate == null ? null : crtDate.trim();
    }

    /**
     * @return MTN_DATE
     */
    public String getMtnDate() {
        return mtnDate;
    }

    /**
     * @param mtnDate
     */
    public void setMtnDate(String mtnDate) {
        this.mtnDate = mtnDate == null ? null : mtnDate.trim();
    }
}