package org.yjht.bean.scsp;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import javax.persistence.*;

/**
 * @author shixinfeng
 */
@Table(name = "front_info_survey_business")
@Getter
@Setter
public class FrontInfoSurveyBusiness {
    /**
     * 调查报告ID
     */
    @Column(name = "SCSP_ID")
    private String scspId;

    /**
     *  经营ID
     */
    @Column(name = "SEQ_NO")
    private String id;

    /**
     * 经营行业
     */
    @Column(name = "BIZ_HY")
    private String bizHy;

    /**
     * 经营项目
     */
    @Column(name = "BIZ_XM")
    private String bizXm;

    /**
     * 经营开始日期
     */
    @Column(name = "BIZ_DATE")
    private String bizDate;

    /**
     * 经营年限
     */
    @Column(name = "BIZ_YEARS")
    private BigDecimal bizYears;

    @Column(name = "OWN_FLAG")
    private String ownFlag;

    /**
     * 经营场所建筑面积-自有
     */
    @Column(name = "OWN_AREA")
    private BigDecimal ownArea;

    @Column(name = "RENT_FLAG")
    private String rentFlag;

    /**
     * 经营场所建筑面积-租赁
     */
    @Column(name = "RENT_AREA")
    private BigDecimal rentArea;

    /**
     * 员工总数量
     */
    @Column(name = "WORKERS")
    private Integer workers;

    /**
     * 年收入
     */
    @Column(name = "INCOME_Y")
    private BigDecimal incomeY;

    /**
     * 年利润
     */
    @Column(name = "PROFIT_Y")
    private BigDecimal profitY;



    /**
     * 数据创建日期
     */
    @Column(name = "CRT_DATE")
    private String crtDate;

    /**
     * 数据修改日期
     */
    @Column(name = "MTN_DATE")
    private String mtnDate;


}