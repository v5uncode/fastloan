package org.yjht.bean.scsp;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Table(name = "front_info_guarantor")
@Getter
@Setter
public class FrontInfoGuarantor {
    /**
     * 编号
     */
    @Id
    @Column(name = "ID")
    @NotNull
    private String id;
    /**
     * 保证合同号
     */
    @Column(name = "GUARANTOR_NO")
    @NotNull
    private String guarantorNo;

    /**
     * 审查审批编号
     */
    @Column(name = "SCSP_ID")
    private String scspId;
    
    /**
     * 保证人证号类型
     */
    @Column(name = "ID_TYPE")
    private String idType;

    /**
     * 保证人证号号码
     */
    @Column(name = "ID_CARD")
    private String idCard;
    
    /**
     * 保证人姓名
     */
    @Column(name = "NAME")
    private String name;

    /**
     * 被保证人身份证号
     */
    @Column(name = "INSURED_ID_CARD")
    private String insuredIdCard;

    

    /**
     * 经营项目
     */
    @Column(name = "JYXM")
    private String jyxm;

    /**
     * 工作单位
     */
    @Column(name = "WORK_NAME")
    private String workName;

    /**
     * 家庭总资产
     */
    @Column(name = "ZZC")
    private String zzc;

    /**
     * 家庭总负债
     */
    @Column(name = "ZFZ")
    private String zfz;

    /**
     * 工作年限
     */
    @Column(name = "WORK_YEARS")
    private String workYears;

    /**
     * 近三年家庭年均收入
     */
    @Column(name = "ANNUAL_INCOME_3Y")
    private String annualIncome3y;

    /**
     * 近三年家庭年均支出
     */
    @Column(name = "ANNUAL_EXPEND_3Y")
    private String annualExpend3y;

    /**
     * 担保人评级结果
     */
    @Column(name = "PJ_JB")
    private String pjJb;
    
    /**
     * 担保金额
     */
    @Column(name = "GUARANTOR_AMOUNT")
    private String guarantorAmount;

}