package org.yjht.bean.scsp;

import java.util.Date;
import javax.persistence.*;

@Table(name = "front_info_survey_other")
public class FrontInfoSurveyOther {
    /**
     * 调查报告ID
     */
    @Id
    @Column(name = "SCSP_ID")
    private String scspId;

    /**
     * 其他情况说明
     */
    @Column(name = "INFO")
    private String info;

    @Column(name = "CREATE_TIME")
    private Date createTime;

    @Column(name = "LAST_CHANGE_TIME")
    private Date lastChangeTime;

    /**
     * 客户坏行为
     */
    @Column(name = "BAD_BEHAVIOR")
    private String badBehavior;

    /**
     * 获取调查报告ID
     *
     * @return SCSP_ID - 调查报告ID
     */
    public String getScspId() {
        return scspId;
    }

    /**
     * 设置调查报告ID
     *
     * @param scspId 调查报告ID
     */
    public void setScspId(String scspId) {
        this.scspId = scspId == null ? null : scspId.trim();
    }

    /**
     * 获取其他情况说明
     *
     * @return INFO - 其他情况说明
     */
    public String getInfo() {
        return info;
    }

    /**
     * 设置其他情况说明
     *
     * @param info 其他情况说明
     */
    public void setInfo(String info) {
        this.info = info == null ? null : info.trim();
    }

    /**
     * @return CREATE_TIME
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return LAST_CHANGE_TIME
     */
    public Date getLastChangeTime() {
        return lastChangeTime;
    }

    /**
     * @param lastChangeTime
     */
    public void setLastChangeTime(Date lastChangeTime) {
        this.lastChangeTime = lastChangeTime;
    }

    /**
     * 获取客户坏行为
     *
     * @return BAD_BEHAVIOR - 客户坏行为
     */
    public String getBadBehavior() {
        return badBehavior;
    }

    /**
     * 设置客户坏行为
     *
     * @param badBehavior 客户坏行为
     */
    public void setBadBehavior(String badBehavior) {
        this.badBehavior = badBehavior == null ? null : badBehavior.trim();
    }
}