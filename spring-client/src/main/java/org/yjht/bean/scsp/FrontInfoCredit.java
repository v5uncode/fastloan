package org.yjht.bean.scsp;

import javax.persistence.*;

import lombok.Getter;
import lombok.Setter;

@Table(name = "front_info_credit")
@Getter
@Setter
public class FrontInfoCredit {
    /**
     * 授信方案ID-对应审查审批ID
     */
    @Id
    @Column(name = "SCSP_ID")
    private String scspId;

    /**
     * 客户ID
     */
    @Column(name = "CUST_ID")
    private String custId;

    /**
     * 客户姓名
     */
    @Column(name = "CUST_NAME")
    private String custName;

    /**
     * 证件类型
     */
    @Column(name = "ID_TYPE")
    private String idType;

    /**
     * 证件号码
     */
    @Column(name = "ID_NO")
    private String idNo;

    /**
     * 机构号
     */
    @Column(name = "ORG_CD")
    private String orgCd;

    /**
     * 客户经理ID
     */
    @Column(name = "CUST_GRP_JL")
    private String custGrpJl;

    /**
     * 客户经理姓名
     */
    @Column(name = "CUST_GRP_NAME")
    private String custGrpName;

    /**
     * 申请日期
     */
    @Column(name = "APPLY_DATE")
    private String applyDate;

    /**
     * 利率调整方式
     */
    @Column(name = "RATE_CYCLE")
    private String rateCycle;

    /**
     * 利率调整日期
     */
    @Column(name = "RATE_ADJUST_DATE")
    private String rateAdjustDate;

    /**
     * 利率浮动率(%）
     */
    @Column(name = "RATE_FLOAT")
    private String rateFloat;

    /**
     * 利率浮动值
     */
    @Column(name = "RATE_FLOAT_VALUE")
    private String rateFloatValue;

    /**
     * 执行利率(%）
     */
    @Column(name = "EXECUTE_RATE")
    private String executeRate;

    /**
     * 贷款用途
     */
    @Column(name = "LOAN_PURPOSE")
    private String loanPurpose;

    /**
     * 贷款用途描述
     */
    @Column(name = "LOAN_PURPOSE_DETAIL")
    private String loanPurposeDetail;

    /**
     * 主担保方式
     */
    @Column(name = "GUARANTEE_WAY")
    private String guaranteeWay;

    /**
     * 从担保方式
     */
    @Column(name = "WARRANT_WAY")
    private String warrantWay;

    /**
     * 放款日期
     */
    @Column(name = "START_DATE")
    private String startDate;

    /**
     * 到期日
     */
    @Column(name = "END_DATE")
    private String endDate;

    /**
     * 宽限期（日）
     */
    @Column(name = "GRACE_PERIOD")
    private String gracePeriod;

    /**
     * 宽限期计息标志（1-是；0-否）
     */
    @Column(name = "GRACE_PERIOD_SIGN")
    private String gracePeriodSign;

    /**
     * 放款金额
     */
    @Column(name = "GRANT_MONEY")
    private String grantMoney;

    /**
     * 放款期限
     */
    @Column(name = "GRANT_TERM")
    private String grantTerm;

    /**
     * 放款账户
     */
    @Column(name = "GRANT_ACCOUNT")
    private String grantAccount;

    /**
     * 还款账户
     */
    @Column(name = "REPAYMENT_ACCOUNT")
    private String repaymentAccount;

    /**
     * 还款日确定方式
     */
    @Column(name = "REPAYMENT_DATE_WAY")
    private String repaymentDateWay;

    /**
     * 还款日
     */
    @Column(name = "REPAYMENT_DATE")
    private String repaymentDate;

    /**
     * 贷款品种
     */
    @Column(name = "LOAN_TYPE")
    private String loanType;
    
    /**
     * 贷款品种
     */
    @Transient
    private String[] loanTypes=new String[2];

    /**
     * 还款方式
     */
    @Column(name = "REPAYMENT_WAY")
    private String repaymentWay;

    /**
     * 还款间隔
     */
    @Column(name = "REPAYMENT_INTERVAL")
    private String repaymentInterval;

    /**
     * 基准利率
     */
    @Column(name = "BASE_RATE")
    private String baseRate;

    /**
     * 罚息浮动率（%）      ==罚息比率
     */
    @Column(name = "DEFAULT_INTEREST_FLOAT")
    private String defaultInterestFloat;

    /**
     * 罚息浮动方式
     */
    @Column(name = "DEFAULT_INTEREST_FLOAT_WAY")
    private String defaultInterestFloatWay;

    /**
     * 罚息比率
     */
    /*@Column(name = "DEFAULT_INTEREST_RATIO")
    private String defaultInterestRatio;*/

    /**
     * 罚息利率
     */
    @Column(name = "DEFAULT_INTEREST_RATE")
    private String defaultInterestRate;

    /**
     * 单复利标志
     */
    @Column(name = "SINGLE_RATE_SIGN")
    private String singleRateSign;

    /**
     * 行业投向
     */
    @Column(name = "HYTX")
    private String hytx;
    
    /**
     * 行业投向
     */
    @Transient
    private String[] hytxs=new String[5];

    /**
     * 借款人属性
     */
    @Column(name = "LOANER_ATTRIBUTE")
    private String loanerAttribute;

    /**
     * 宽限期方式
     */
    @Column(name = "GRACE_PERIOD_WAY")
    private String gracePeriodWay;

    /**
     * 创建时间
     */
    @Column(name = "CREATED_TIME")
    private String createdTime;

    /**
     * 修改时间
     */
    @Column(name = "UPDTAE_TIME")
    private String updtaeTime;

    /**
     * 备注
     */
    @Column(name = "REMARKS")
    private String remarks;

    /**
     * 扩展字段1
     */
    @Column(name = "EXT1")
    private String ext1;

    /**
     * 扩展字段2
     */
    @Column(name = "EXT2")
    private String ext2;

    /**
     * 扩展字段3
     */
    @Column(name = "EXT3")
    private String ext3;

}