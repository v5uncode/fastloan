package org.yjht.bean.scsp;

import java.math.BigDecimal;
import javax.persistence.*;

@Table(name = "front_info_survey_plant")
public class FrontInfoSurveyPlant {
    /**
     * ID
     */
    @Id
    @Column(name = "ID")
    private String id;

    /**
     * 审查审批ID
     */
    @Column(name = "SCSP_ID")
    private String scspId;

    /**
     * 作物种类
     */
    @Column(name = "ZW_TYPE")
    private String zwType;

    /**
     * 种植种类

     */
    @Column(name = "CROP_TYPE")
    private String cropType;

    /**
     * 种植总亩数

     */
    @Column(name = "ZZ_MS")
    private BigDecimal zzMs;

    /**
     * 种植模式

     */
    @Column(name = "ZZ_MODEL")
    private String zzModel;

    /**
     * 下架时间

     */
    @Column(name = "XJ_SJ")
    private String xjSj;

    /**
     * 经营开始日期

     */
    @Column(name = "BIZ_DATE")
    private String bizDate;

    /**
     * 经营年限

     */
    @Column(name = "BIZ_YEARS")
    private BigDecimal bizYears;

    /**
     * 年收入

     */
    @Column(name = "INCOME_Y")
    private BigDecimal incomeY;

    /**
     * 数据创建日期

     */
    @Column(name = "CRT_DATE")
    private String crtDate;

    /**
     * 数据修改日期

     */
    @Column(name = "MTN_DATE")
    private String mtnDate;

    /**
     * 获取ID
     *
     * @return ID - ID
     */
    public String getId() {
        return id;
    }

    /**
     * 设置ID
     *
     * @param id ID
     */
    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    /**
     * 获取审查审批ID
     *
     * @return SCSP_ID - 审查审批ID
     */
    public String getScspId() {
        return scspId;
    }

    /**
     * 设置审查审批ID
     *
     * @param scspId 审查审批ID
     */
    public void setScspId(String scspId) {
        this.scspId = scspId == null ? null : scspId.trim();
    }

    /**
     * 获取作物种类
     *
     * @return ZW_TYPE - 作物种类
     */
    public String getZwType() {
        return zwType;
    }

    /**
     * 设置作物种类
     *
     * @param zwType 作物种类
     */
    public void setZwType(String zwType) {
        this.zwType = zwType == null ? null : zwType.trim();
    }

    /**
     * 获取种植种类

     *
     * @return CROP_TYPE - 种植种类

     */
    public String getCropType() {
        return cropType;
    }

    /**
     * 设置种植种类

     *
     * @param cropType 种植种类

     */
    public void setCropType(String cropType) {
        this.cropType = cropType == null ? null : cropType.trim();
    }

    /**
     * 获取种植总亩数

     *
     * @return ZZ_MS - 种植总亩数

     */
    public BigDecimal getZzMs() {
        return zzMs;
    }

    /**
     * 设置种植总亩数

     *
     * @param zzMs 种植总亩数

     */
    public void setZzMs(BigDecimal zzMs) {
        this.zzMs = zzMs;
    }

    /**
     * 获取种植模式

     *
     * @return ZZ_MODEL - 种植模式

     */
    public String getZzModel() {
        return zzModel;
    }

    /**
     * 设置种植模式

     *
     * @param zzModel 种植模式

     */
    public void setZzModel(String zzModel) {
        this.zzModel = zzModel == null ? null : zzModel.trim();
    }

    /**
     * 获取下架时间

     *
     * @return XJ_SJ - 下架时间

     */
    public String getXjSj() {
        return xjSj;
    }

    /**
     * 设置下架时间

     *
     * @param xjSj 下架时间

     */
    public void setXjSj(String xjSj) {
        this.xjSj = xjSj == null ? null : xjSj.trim();
    }

    /**
     * 获取经营开始日期

     *
     * @return BIZ_DATE - 经营开始日期

     */
    public String getBizDate() {
        return bizDate;
    }

    /**
     * 设置经营开始日期

     *
     * @param bizDate 经营开始日期

     */
    public void setBizDate(String bizDate) {
        this.bizDate = bizDate == null ? null : bizDate.trim();
    }

    /**
     * 获取经营年限

     *
     * @return BIZ_YEARS - 经营年限

     */
    public BigDecimal getBizYears() {
        return bizYears;
    }

    /**
     * 设置经营年限

     *
     * @param bizYears 经营年限

     */
    public void setBizYears(BigDecimal bizYears) {
        this.bizYears = bizYears;
    }

    /**
     * 获取年收入

     *
     * @return INCOME_Y - 年收入

     */
    public BigDecimal getIncomeY() {
        return incomeY;
    }

    /**
     * 设置年收入

     *
     * @param incomeY 年收入

     */
    public void setIncomeY(BigDecimal incomeY) {
        this.incomeY = incomeY;
    }

    /**
     * 获取数据创建日期

     *
     * @return CRT_DATE - 数据创建日期

     */
    public String getCrtDate() {
        return crtDate;
    }

    /**
     * 设置数据创建日期

     *
     * @param crtDate 数据创建日期

     */
    public void setCrtDate(String crtDate) {
        this.crtDate = crtDate == null ? null : crtDate.trim();
    }

    /**
     * 获取数据修改日期

     *
     * @return MTN_DATE - 数据修改日期

     */
    public String getMtnDate() {
        return mtnDate;
    }

    /**
     * 设置数据修改日期

     *
     * @param mtnDate 数据修改日期

     */
    public void setMtnDate(String mtnDate) {
        this.mtnDate = mtnDate == null ? null : mtnDate.trim();
    }
}