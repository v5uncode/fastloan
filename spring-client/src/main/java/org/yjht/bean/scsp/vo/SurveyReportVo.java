package org.yjht.bean.scsp.vo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import org.yjht.bean.scsp.FrontInfoSurvey;
import org.yjht.bean.scsp.FrontInfoSurveyBreed;
import org.yjht.bean.scsp.FrontInfoSurveyBusiness;
import org.yjht.bean.scsp.FrontInfoSurveyPlant;
import org.yjht.bean.scsp.FrontInfoSurveyRela;
import lombok.Getter;
import lombok.Setter;
@Getter
@Setter
public class SurveyReportVo extends FrontInfoSurvey{
	/**
     * 主营项目--展示
     */
    private String zyxmShow;
	/**
     * 经营信息
     */
    private List<FrontInfoSurveyBusiness> businesses = new ArrayList<>();

    /**
     * 种植信息
     */
    private List<FrontInfoSurveyPlant> plants = new ArrayList<>();

    /**
     * 养殖信息
     */
    private List<FrontInfoSurveyBreed> breeds = new ArrayList<>();

    /**
     * 家庭成员
     */
    private List<FrontInfoSurveyRela> families = new ArrayList<>();
    
    /**
     * 报告ID-对应审查审批表
     */
    private String scspId;
       
    /**
     * 客户姓名
     */
    private String custName;

    /**
     * 主营项目
     */
    private String zyxm;

    /**
     * 客户ID
     */
    private String custId;

    /**
     * 客户性别
     */
    private String sex;


    /**
     * 民族
     */
    private String nation;

    /**
     * 婚姻状况
     */
    private String marriageFlag;

    /**
     * 结婚等级日期
     */
    private String marriageDate;
    /**
     * 证件类型
     */
    private String idType;


    /**
     * 身份证号
     */
    private String idNo;

    /**
     * 家庭人口
     */
    private Integer familyCount;

    /**
     * 家庭住址
     */
    private String address;

    /**
     * 申请金额
     */
    private BigDecimal applyMoney;

    /**
     * 申请期限
     */
    private Integer applyLimit;
    /**
     * 联系电话
     */
    private String telNo;

    /**
     * 贷款用途
     */
    private String purpose;

    /**
     * 职业
     */
    private String vocation;

    /**
     * 工作单位名称
     */
    private String workName;

    /**
     * 职工从业年限
     */
    private Integer workYears;

    /**
     * 职工职务
     */
    private String duty;

    /**
     * 有无子女
     */
    private String childFlag;

    /**
     * 发证机关
     */
    private String gov;

    /**
     * 学历
     */
    private String eduLevel;

    /**
     * 职工性质
     */
    private String zbFlag;

    /**
     * 工作单位地址
     */
    private String workAddress;

    /**
     * 营业执照名称
     */
    private String licenceName;
    /**
     * 医疗保险缴纳金额
     */
    private BigDecimal ylbxamt;

    /**
     * 营业执照号码
     */
    private String licenceNo;

    /**
     * 经营地址
     */
    private String storeAddress;

    /**
     * 借款人及家庭成员工作情况
     */
    private String custFamiliesStatus;

    /**
     * 是否在城区购买商住商用房
     */
    private String businessHome;

    /**
     * 自有房产数量
     */
    private Integer houseCount ;


    /**
     * 上年家庭收入
     */
    private BigDecimal sr1y;

    /**
     * 上年家庭支出
     */
    private BigDecimal zc1y;

    /**
     * 家庭总资产
     */
    private BigDecimal zzc;

    /**
     * 现金及银行存款
     */
    private BigDecimal xjjyhck;

    /**
     * 存货
     */
    private BigDecimal ck;

    /**
     * 应收及预付账款
     */
    private Double ysyf;
    /**
     * 应付及预收账款
     */
    private Double yfys;

    /**
     * 固定资产
     */
    private BigDecimal gdzc;

    /**
     * 其它资产
     */
    private BigDecimal qtjyzc;

    /**
     * 总负债
     */
    private BigDecimal zfz;

    /**
     * 银行贷款（长期贷款）
     */
    private BigDecimal cqdk;

    /**
     * 银行贷款（短期）
     */
    private BigDecimal dqdk;

    /**
     * 其他负债
     */
    private BigDecimal qtfz;

    /**
     * 家庭或有负债
     */
    private BigDecimal jthyfz;

    /**
     * 负面信息
     */
    private String badInfo;

    /**
     * 其他负面信息
     */
    private String badOtherInfo;

    /**
     * 近三年家庭年均收入
     */
    private String njsr ;

    /**
     * 近三年家庭年均支出
     */
    private String njzc;


    /**
     * 成立日期
     */
    private String createDate;

    /**
     * 员工数量
     */
    private Integer employeeCount;
    /**
     * 有保险员工数量
     */
    private Integer bxCnt;

    /**
     * 营运车数量
     */
    private Integer yycNum;


    /**
     * 评级结果
     */
    private String grade;

    /**
     * 评级级别
     */
    private String pjLmt;

    /**
     * 最高授信
     */
    private BigDecimal highCredit;
    /**
     * 信用额度
     */
    private BigDecimal creditLimit;
    /**
     * 信用利率浮动比例(%)
     */
    private BigDecimal creditFloatRate;
    /**
     * 信用一年期贷款日利率(‱)
     */
    private Double creditRate;

    /**
     * 抵制押额度
     */
    private BigDecimal pledgeLimit;
    /**
     * 抵制押利率浮动比例(%)
     */
    private BigDecimal pledgeFloatRate;
    /**
     * 抵制押一年期贷款日利率(‱)
     */
    private Double pledgeRate;

    /**
     * 保证额度
     */
    private BigDecimal ensureLimit;
    /**
     * 保证利率浮动比例(%)
     */
    private BigDecimal ensureFloatRate;
    /**
     * 保证一年期贷款日利率(‱)
     */
    private Double ensureRate;


    /**
     * 同意授信额度
     */
    private String surveyHighLimit;
    /**
     * 担保方式
     */
    private String warrantType;
    /**
     * 利率浮动比率
     */
    private String rateFloat;
    /**
     * 一年期贷款月利率
     */
    private String monRate;
    /**
     * 日息为万分之 数字
     */
    private String dayRate;
    /**
     * 日息为万分之  汉字
     */
    private String moneyForDay;
    /**
     * 授信期限
     */
    private String surveyDateLimit;
    
    /**
     * 客户经理编号
     */
    private String custGrpJl;
    
    /**
     * 调查客户经理
     */
    private String custGrpName;

    /**
     * 调查日期
     */
    private String reportDate;
    
    /**
     * 调查报告完成状态
     */
    private String state;

}
