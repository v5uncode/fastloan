package org.yjht.bean.scsp;

import java.math.BigDecimal;
import javax.persistence.*;

import lombok.Getter;
import lombok.Setter;

@Table(name = "front_info_survey_breed")
@Setter
@Getter
public class FrontInfoSurveyBreed {
    /**
     * ID
     */
    @Id
    @Column(name = "ID")
    private String id;

    /**
     * 审查审批id
     */
    @Column(name = "SCSP_ID")
    private String scspId;

    /**
     * 养殖类型
     */
    @Column(name = "YZ_TYPE")
    private String yzType;
    
    /**
     * 养殖种类
     */
    @Column(name = "YZ_KIND")
    private String yzKind;

    /**
     * 数量
     */
    @Column(name = "YZ_CNT")
    private BigDecimal yzCnt;

    /**
     * 出栏时间
     */
    @Column(name = "CL_SJ")
    private String clSj;

    /**
     * 经营开始日期

     */
    @Column(name = "BIZ_DATE")
    private String bizDate;

    /**
     * 经营年限

     */
    @Column(name = "BIZ_YEARS")
    private Integer bizYears;

    /**
     * 年收入

     */
    @Column(name = "INCOME_Y")
    private BigDecimal incomeY;

    /**
     * 年利润

     */
    @Column(name = "PROFIT_Y")
    private BigDecimal profitY;


    /**
     * 年利润

     */
    @Column(name = "BIZ_FIXEDASSET")
    private BigDecimal bizFixedasset;
    /**
     * 数据创建日期

     */
    @Column(name = "CRT_DATE")
    private String crtDate;

    /**
     * 数据修改日期

     */
    @Column(name = "MTN_DATE")
    private String mtnDate;
}