package org.yjht.bean;

import java.math.BigDecimal;
import javax.persistence.*;

@Table(name = "CUST_STATISTICS")
public class CustStatistics {
    @Column(name = "CORP_CD")
    private String corpCd;

    @Column(name = "CREATE_TYPE")
    private String createType;

    @Column(name = "ISCREDIT")
    private String iscredit;

    @Column(name = "DATAFROM")
    private String datafrom;

    @Column(name = "ISMOD")
    private String ismod;

    @Column(name = "COUNT")
    private BigDecimal count;

    /**
     * @return CORP_CD
     */
    public String getCorpCd() {
        return corpCd;
    }

    /**
     * @param corpCd
     */
    public void setCorpCd(String corpCd) {
        this.corpCd = corpCd == null ? null : corpCd.trim();
    }

    /**
     * @return CREATE_TYPE
     */
    public String getCreateType() {
        return createType;
    }

    /**
     * @param createType
     */
    public void setCreateType(String createType) {
        this.createType = createType == null ? null : createType.trim();
    }

    /**
     * @return ISCREDIT
     */
    public String getIscredit() {
        return iscredit;
    }

    /**
     * @param iscredit
     */
    public void setIscredit(String iscredit) {
        this.iscredit = iscredit == null ? null : iscredit.trim();
    }

    /**
     * @return DATAFROM
     */
    public String getDatafrom() {
        return datafrom;
    }

    /**
     * @param datafrom
     */
    public void setDatafrom(String datafrom) {
        this.datafrom = datafrom == null ? null : datafrom.trim();
    }

    /**
     * @return ISMOD
     */
    public String getIsmod() {
        return ismod;
    }

    /**
     * @param ismod
     */
    public void setIsmod(String ismod) {
        this.ismod = ismod == null ? null : ismod.trim();
    }

    /**
     * @return COUNT
     */
    public BigDecimal getCount() {
        return count;
    }

    /**
     * @param count
     */
    public void setCount(BigDecimal count) {
        this.count = count;
    }
}