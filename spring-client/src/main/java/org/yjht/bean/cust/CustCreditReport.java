package org.yjht.bean.cust;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Table(name = "cust_credit_report")
@Getter
@Setter
public class CustCreditReport {
    /**
     * 客户ID
     */
    @Id
    @Column(name = "CUST_ID")
    private String custId;



    /**
     * 是否有征信报告（准入、评级）
     */
    @Column(name = "IS_HAVE_CREDIT_INFO")
    private String isHaveCreditInfo;

    /**
     * 征信近6个月征信报告查询次数（评级）
     */
    @Column(name = "CR_QUERY_COUNT_IN6_M")
    private String crQueryCountIn6M;

    /**
     * 征信近12个月征信报告查询次数（准入、评级）
     */
    @Column(name = "CR_QUERY_COUNT_IN12_M")
    private String crQueryCountIn12M;

    /**
     * 征信近24个月征信报告查询次数（评级）
     */
    @Column(name = "CR_QUERY_COUNT_IN24_M")
    private String crQueryCountIn24M;

    /**
     * 征信近36个月征信报告查询次数（评级）
     */
    @Column(name = "CR_QUERY_COUNT_IN36_M")
    private String crQueryCountIn36M;

    /**
     * 征信近6个月本息逾期次数（银行贷款、信用卡）（评级）
     */
    @Column(name = "CR_OVERDUE_COUNT_IN6_M")
    private String crOverdueCountIn6M;

    /**
     * 征信近12个月本息逾期次数（信用卡）（评级）
     */
    @Column(name = "CREDIT_OVERDUE_CARD_TIMES12")
    private String creditOverdueCardTimes12;

    /**
     * 征信近12个月本息逾期次数（银行贷款）（评级）
     */
    @Column(name = "CREDIT_OVERDUE_LOAN_TIMES12")
    private String creditOverdueLoanTimes12;

    /**
     * 征信近24个月本息逾期次数（银行贷款、信用卡）（评级）
     */
    @Column(name = "CR_OVERDUE_COUNT_I24_M")
    private String crOverdueCountI24M;

    /**
     * 征信近36个月本息逾期次数（银行贷款、信用卡）（评级）
     */
    @Column(name = "CR_OVERDUE_COUNT_IN36_M")
    private String crOverdueCountIn36M;

    /**
     * 征信本息最近一次逾期距离现在的月份数（银行贷款、信用卡）（评级）
     */
    @Column(name = "CR_OVERDUE_MONTH_COUNT")
    private String crOverdueMonthCount;

    /**
     * 征信是否有本息逾期（评级）
     */
    @Column(name = "CRE_IS_OVERDUE_IN_AND_C")
    private String creIsOverdueInAndC;

    /**
     * 征信近12个月贷记卡开户个数（评级）
     */
    @Column(name = "CR_CREDITCARD_OPEN_COUNT")
    private String crCreditcardOpenCount;

    /**
     * 征信最早贷款账户距今月份数（银行贷款、信用卡）（评级）
     */
    @Column(name = "CR_EARLY")
    private String crEarly;

    /**
     * 征信近6个月本息最大逾期期数（银行贷款、信用卡）（评级）
     */
    @Column(name = "CR_OVERDUE_MAX_COUNT_IN6_M")
    private String crOverdueMaxCountIn6M;

    /**
     * 征信近12个月本息最大逾期期数（银行贷款、信用卡）（评级）
     */
    @Column(name = "CR_OVERDUE_MAX_COUNT_IN12_M")
    private String crOverdueMaxCountIn12M;

    /**
     * 征信近24个月本息最大逾期期数（银行贷款、信用卡）（准入、评级）
     */
    @Column(name = "CR_OVERDUE_MAX_COUNT_IN24_M")
    private String crOverdueMaxCountIn24M;

    /**
     * 征信贷记卡数量（不含销户）（评级）
     */
    @Column(name = "CREDIT_CARD_COUNT")
    private String creditCardCount;

    /**
     * 征信对外担保贷款本金余额（元）
     */
    @Column(name = "CR_GURANTEE_AMOUNT")
    private String crGuranteeAmount;

    /**
     * 征信当前逾期期数（准入）
     */
    @Column(name = "CR_OVERDUE_COUNT")
    private String crOverdueCount;

    /**
     * 征信报告中贷款法人机构数（准入）
     */
    @Column(name = "CR_LOAN_ORG_COUNT")
    private String crLoanOrgCount;

    /**
     * 征信报告中已激活未销户信用卡发卡法人机构数（准入）
     */
    @Column(name = "NORMAL_CREDIT_CARD_CORP_NUM")
    private String normalCreditCardCorpNum;
    
    /**
     * 征信报告中未销户信用卡发卡机构数（准入）
     */
    @Column(name = "CR_CREDIT_CARD_ORG_COUNT")
    private String crCreditCardOrgCount;
    
    /**
     * 未销户贷记卡与准贷记卡最近6个月平均使用率（准入）
     */
    @Column(name = "WITHOUT_CREDIT_AND_SEMI_LATE_USE_RATIO_IN6_M")
    private String withoutCreditAndSemiLateUseRatioIn6M;
    
    /**
     * 当前征信信用卡额度使用率(评级)
     */
    @Column(name = "CREDIT_CARD_USE_RATIO")
    private String creditCardUseRatio;

    /**
     * 征信他行贷款最差五级分类形态（准入）
     */
    @Column(name = "CR_OTHER_WORST_FIVE_CLASS")
    private String crOtherWorstFiveClass;

    /**
     * 征信对外担保贷款最差五级分类形态（准入）
     */
    @Column(name = "CR_GUARANTE_WORST_FIVE_CLASS")
    private String crGuaranteWorstFiveClass;

    /**
     * 征信报告中是否存在保证人代偿记录（准入）
     */
    @Column(name = "HAVE_GUARANTOR_HELP")
    private String haveGuarantorHelp;

    /**
     * 征信近12个月信用卡办理次数（首次信用卡审批查询后）（准入）
     */
    @Column(name = "AFTER12_CREDIT_FIRST_QUERY_CARD_TIMES")
    private String after12CreditFirstQueryCardTimes;

    /**
     * 征信近12个月贷款办理次数（首次贷款审批查询后）（准入）
     */
    @Column(name = "AFTER12_LOAN_FIRST_QUERY_LOAN_TIMES")
    private String after12LoanFirstQueryLoanTimes;

    /**
     * 征信报告中是否有助学贷款信息（准入）
     */
    @Column(name = "HAVE_STUDENT_LOAN")
    private String haveStudentLoan;

    /**
     * 征信近12个月查询记录中信用卡审批的查询次数（准入）
     */
    @Column(name = "QUERY_TIMES_WITH_CREDIT12")
    private String queryTimesWithCredit12;

    /**
     * 征信近12个月查询记录中贷款审批的查询次数（准入）
     */
    @Column(name = "QUERY_TIMES_WITH_LOAN12")
    private String queryTimesWithLoan12;

    /**
     * 呆账信息汇总笔数（准入）
     */
    @Column(name = "CR_COUNT_DEBTS")
    private String crCountDebts;

    /**
     * 资产处置信息汇总笔数（准入）
     */
    @Column(name = "CR_COUNT_REFORMS")
    private String crCountReforms;

    /**
     * 保证人代偿信息汇总笔数（准入）
     */
    @Column(name = "CR_COUNT_PENSAS")
    private String crCountPensas;

    /**
     * 贷款逾期笔数（准入）
     */
    @Column(name = "CR_DUE_COUNTS")
    private String crDueCounts;

    /**
     * 贷记卡逾期账户数（准入）
     */
    @Column(name = "CR_CARD_COUNTS")
    private String crCardCounts;
    
    /**
     * 准贷记卡60天以上透支账户数(准入)
     */
    @Column(name = "CR_COUNTS60_DAYS")
    private String crCounts60Days;
    
    /**
     * 征信报告中是否有职业信息
     */
    @Column(name = "HAVE_VOCATION")
    private String haveVocation;
    
    /**
     * 数据创建日期
     */
    @Column(name = "CRT_DATE")
    private String crtDate;
    
    /**
     * 数据修改日期
     */
    @Column(name = "MTN_DATE")
    private String mtnDate;

    /**
     * 征信近12个月本息逾期次数（银行贷款、信用卡）==creditOverdueLoanTimes12+creditOverdueCardTimes12
     */
    @Transient
    private String  crOverdueCountIn12M;

}