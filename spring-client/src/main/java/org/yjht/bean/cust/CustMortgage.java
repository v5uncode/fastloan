package org.yjht.bean.cust;

import java.math.BigDecimal;
import javax.persistence.*;

import lombok.Getter;
import lombok.Setter;

@Table(name = "cust_mortgage")
@Setter
@Getter
public class CustMortgage {
    /**
     * 抵押ID
     */
    @Id
    @Column(name = "MORTGAGE_ID")
    private String mortgageId;


    /**
     * 客户号
     */
    @Column(name = "CUST_ID")
    private String custId;

    /**
     * 抵押物类型1-住房 2-写字楼 3-商铺 4-动产 5-其他
     */
    @Column(name = "DYLX")
    private String dylx;

    /**
     * 抵押状态
     */
    @Column(name = "DYZT")
    private String dyzt;

    /**
     * 抵押物权属人信息
     */
    @Column(name = "DYWQSRXX")
    private String dywqsrxx;

    /**
     * 权属人与借款人关系
     */
    @Column(name = "QSRYJKRGX")
    private String qsryjkrgx;

    /**
     * 抵押物所有权属
     */
    @Column(name = "DYWSYQS")
    private String dywsyqs;

    /**
     * 抵押物动产型号
     */
    @Column(name = "DYWDCXH")
    private String dywdcxh;

    /**
     * 发动机号
     */
    @Column(name = "FDJH")
    private String fdjh;

    /**
     * 车牌号
     */
    @Column(name = "CPH")
    private String cph;

    /**
     * 汽车底盘号
     */
    @Column(name = "QCDPH")
    private String qcdph;

    /**
     * 楼盘抵押分类选择
     */
    @Column(name = "LPDYFLXZ")
    private String lpdyflxz;

    /**
     * 抵押物(不动产)位置
     */
    @Column(name = "DYWBDCWZ")
    private String dywbdcwz;

    /**
     * 房屋面积
     */
    @Column(name = "FWMJ")
    private BigDecimal fwmj;

    /**
     * 丘号
     */
    @Column(name = "QH")
    private String qh;

    /**
     * 房屋建成日期
     */
    @Column(name = "FWJCRQ")
    private String fwjcrq;

    /**
     * 权证编号
     */
    @Column(name = "QZBH")
    private String qzbh;

    /**
     * 他项权证号
     */
    @Column(name = "TXQZH")
    private String txqzh;

    /**
     * 权证编号2
     */
    @Column(name = "QZBH2")
    private String qzbh2;

    /**
     * 他项权证号2
     */
    @Column(name = "TXQZH2")
    private String txqzh2;

    /**
     * 交易价值
     */
    @Column(name = "JYJZ")
    private BigDecimal jyjz;

    /**
     * 使用情况
     */
    @Column(name = "SYQK")
    private String syqk;

    /**
     * 抵押起始日
     */
    @Column(name = "DYQSR")
    private String dyqsr;

    /**
     * 抵押终止日
     */
    @Column(name = "DYZZR")
    private String dyzzr;

    /**
     * 评估价值
     */
    @Column(name = "PGJZ")
    private BigDecimal pgjz;

    /**
     * 评估人姓名
     */
    @Column(name = "PGRXM")
    private String pgrxm;

    /**
     * 评估单位
     */
    @Column(name = "PGDW")
    private String pgdw;

    /**
     * 评估时间
     */
    @Column(name = "PGSJ")
    private String pgsj;

    /**
     * 评估编号
     */
    @Column(name = "PGBGBH")
    private String pgbgbh;
    /**
     * 抵押比例
     */
    @Column(name = "DYBL")
    private BigDecimal dybl;

    /**
     * 担保主债权
     */
    @Column(name = "DBZZQ")
    private BigDecimal dbzzq;

    /**
     * 抵押机关
     */
    @Column(name = "DYJG")
    private String dyjg;

    /**
     * 抵押费用
     */
    @Column(name = "DYFY")
    private BigDecimal dyfy;
    
    /**
     * 抵押价值
     */
    @Column(name = "DYJZ")
    private BigDecimal dyjz;

    /**
     * 投保情况
     */
    @Column(name = "TBQK")
    private String tbqk;

    /**
     * 保险公司
     */
    @Column(name = "BXGS")
    private String bxgs;

    /**
     * 保险受益人
     */
    @Column(name = "BXSYR")
    private String bxsyr;

    /**
     * 保单号码
     */
    @Column(name = "BDHM")
    private String bdhm;

    @Column(name = "TBJE")
    private BigDecimal tbje;

    /**
     * 保险费
     */
    @Column(name = "BXF")
    private BigDecimal bxf;

    /**
     * 缴费方式
     */
    @Column(name = "JFFS")
    private String jffs;

    /**
     * 保险起始日
     */
    @Column(name = "BXQSR")
    private String bxqsr;

    /**
     * 保险终止日
     */
    @Column(name = "BXZZR")
    private String bxzzr;

    /**
     * 出单日期
     */
    @Column(name = "CDRQ")
    private String cdrq;

    /**
     * 续保标志
     */
    @Column(name = "XBBZ")
    private String xbbz;

    /**
     * 备注
     */
    @Column(name = "BZ")
    private String bz;
    
    /**
     * 数据创建日期
     */
    @Column(name = "CRT_DATE")
    private String crtDate;
    
    /**
     * 数据修改日期
     */
    @Column(name = "MTN_DATE")
    private String mtnDate;
  
}