package org.yjht.bean.cust;

import javax.persistence.*;

@Table(name = "cust_family")
public class CustFamily {
    /**
     * 客户ID
     */
    @Id
    @Column(name = "CUST_ID")
    private String custId;

    /**
     * 借款人及家庭成员工作情况

     */
    @Column(name = "CUST_FAMILIES_STATUS")
    private String custFamiliesStatus;

    /**
     * 居住状况

     */
    @Column(name = "LIVE_STATUS")
    private String liveStatus;

    /**
     * 配偶工作单位

     */
    @Column(name = "VACATION")
    private String vacation;

    /**
     * 家庭成员数量

     */
    @Column(name = "FAMILY_COUNT")
    private Integer familyCount;

    /**
     * 自有房产数量

     */
    @Column(name = "HOUSE_COUNT")
    private Integer houseCount;

    /**
     * 是否在城区购买商住商用房

     */
    @Column(name = "BUSINESS_HOME")
    private String businessHome;

    /**
     * 婚姻状况


     */
    @Column(name = "MARRIAGE_FLAG")
    private String marriageFlag;

    /**
     * 结婚/登记日期

     */
    @Column(name = "MARRIAGE_DATE")
    private String marriageDate;
    
    /**
     * 数据创建日期

     */
    @Column(name = "CRT_DATE")
    private String crtDate;

    /**
     * 数据修改日期

     */
    @Column(name = "MTN_DATE")
    private String mtnDate;

    /**
     * 健康状况
     */
    @Column(name = "HEALTH_STATUS")
    private String healthStatus;

    /**
     * 户籍号
     */
    @Column(name = "REGISTER_NO")
    private String registerNo;

    /**
     * 有无子女
     */
    @Column(name = "CHILD_FLAG")
    private String childFlag;

    /**
     * 家庭邮编
     */
    @Column(name = "FAMILY_POSTCODE")
    private String familyPostcode;

    /**
     * 家庭电话
     */
    @Column(name = "FAMILY_PHONE")
    private String familyPhone;

    /**
     * 获取客户ID
     *
     * @return CUST_ID - 客户ID
     */
    public String getCustId() {
        return custId;
    }

    /**
     * 设置客户ID
     *
     * @param custId 客户ID
     */
    public void setCustId(String custId) {
        this.custId = custId == null ? null : custId.trim();
    }

    /**
     * 获取借款人及家庭成员工作情况

     *
     * @return CUST_FAMILIES_STATUS - 借款人及家庭成员工作情况

     */
    public String getCustFamiliesStatus() {
        return custFamiliesStatus;
    }

    /**
     * 设置借款人及家庭成员工作情况

     *
     * @param custFamiliesStatus 借款人及家庭成员工作情况

     */
    public void setCustFamiliesStatus(String custFamiliesStatus) {
        this.custFamiliesStatus = custFamiliesStatus == null ? null : custFamiliesStatus.trim();
    }

    /**
     * 获取居住状况

     *
     * @return LIVE_STATUS - 居住状况

     */
    public String getLiveStatus() {
        return liveStatus;
    }

    /**
     * 设置居住状况

     *
     * @param liveStatus 居住状况

     */
    public void setLiveStatus(String liveStatus) {
        this.liveStatus = liveStatus == null ? null : liveStatus.trim();
    }

    /**
     * 获取配偶工作单位

     *
     * @return VACATION - 配偶工作单位

     */
    public String getVacation() {
        return vacation;
    }

    /**
     * 设置配偶工作单位

     *
     * @param vacation 配偶工作单位

     */
    public void setVacation(String vacation) {
        this.vacation = vacation == null ? null : vacation.trim();
    }

    /**
     * 获取家庭成员数量

     *
     * @return FAMILY_COUNT - 家庭成员数量

     */
    public Integer getFamilyCount() {
        return familyCount;
    }

    /**
     * 设置家庭成员数量

     *
     * @param familyCount 家庭成员数量

     */
    public void setFamilyCount(Integer familyCount) {
        this.familyCount = familyCount;
    }

    /**
     * 获取自有房产数量

     *
     * @return HOUSE_COUNT - 自有房产数量

     */
    public Integer getHouseCount() {
        return houseCount;
    }

    /**
     * 设置自有房产数量

     *
     * @param houseCount 自有房产数量

     */
    public void setHouseCount(Integer houseCount) {
        this.houseCount = houseCount;
    }

    /**
     * 获取是否在城区购买商住商用房

     *
     * @return BUSINESS_HOME - 是否在城区购买商住商用房

     */
    public String getBusinessHome() {
        return businessHome;
    }

    /**
     * 设置是否在城区购买商住商用房

     *
     * @param businessHome 是否在城区购买商住商用房

     */
    public void setBusinessHome(String businessHome) {
        this.businessHome = businessHome == null ? null : businessHome.trim();
    }

    /**
     * 获取婚姻状况


     *
     * @return MARRIAGE_FLAG - 婚姻状况


     */
    public String getMarriageFlag() {
        return marriageFlag;
    }

    /**
     * 设置婚姻状况


     *
     * @param marriageFlag 婚姻状况


     */
    public void setMarriageFlag(String marriageFlag) {
        this.marriageFlag = marriageFlag == null ? null : marriageFlag.trim();
    }

    /**
     * 获取结婚/登记日期

     *
     * @return MARRIAGE_DATE - 结婚/登记日期

     */
    public String getMarriageDate() {
        return marriageDate;
    }

    /**
     * 设置结婚/登记日期

     *
     * @param marriageDate 结婚/登记日期

     */
    public void setMarriageDate(String marriageDate) {
        this.marriageDate = marriageDate == null ? null : marriageDate.trim();
    }
    
    /**
     * 获取数据创建日期

     *
     * @return MTN_DATE - 数据创建日期

     */
    public String getCrtDate() {
        return crtDate;
    }

    /**
     * 设置数据创建日期

     *
     * @param crtDate 数据创建日期

     */
    public void setCrtDate(String crtDate) {
        this.crtDate = crtDate == null ? null : crtDate.trim();
    }

    /**
     * 获取数据修改日期

     *
     * @return MTN_DATE - 数据修改日期

     */
    public String getMtnDate() {
        return mtnDate;
    }

    /**
     * 设置数据修改日期

     *
     * @param mtnData 数据修改日期

     */
    public void setMtnDate(String mtnDate) {
        this.mtnDate = mtnDate == null ? null : mtnDate.trim();
    }

    /**
     * 获取健康状况
     *
     * @return HEALTH_STATUS - 健康状况
     */
    public String getHealthStatus() {
        return healthStatus;
    }

    /**
     * 设置健康状况
     *
     * @param healthStatus 健康状况
     */
    public void setHealthStatus(String healthStatus) {
        this.healthStatus = healthStatus == null ? null : healthStatus.trim();
    }

    /**
     * 获取户籍号
     *
     * @return REGISTER_NO - 户籍号
     */
    public String getRegisterNo() {
        return registerNo;
    }

    /**
     * 设置户籍号
     *
     * @param registerNo 户籍号
     */
    public void setRegisterNo(String registerNo) {
        this.registerNo = registerNo == null ? null : registerNo.trim();
    }

    /**
     * 获取有无子女
     *
     * @return CHILD_FLAG - 有无子女
     */
    public String getChildFlag() {
        return childFlag;
    }

    /**
     * 设置有无子女
     *
     * @param childFlag 有无子女
     */
    public void setChildFlag(String childFlag) {
        this.childFlag = childFlag == null ? null : childFlag.trim();
    }

    /**
     * 获取家庭邮编
     *
     * @return FAMILY_POSTCODE - 家庭邮编
     */
    public String getFamilyPostcode() {
        return familyPostcode;
    }

    /**
     * 设置家庭邮编
     *
     * @param familyPostcode 家庭邮编
     */
    public void setFamilyPostcode(String familyPostcode) {
        this.familyPostcode = familyPostcode == null ? null : familyPostcode.trim();
    }

    /**
     * 获取家庭电话
     *
     * @return FAMILY_PHONE - 家庭电话
     */
    public String getFamilyPhone() {
        return familyPhone;
    }

    /**
     * 设置家庭电话
     *
     * @param familyPhone 家庭电话
     */
    public void setFamilyPhone(String familyPhone) {
        this.familyPhone = familyPhone == null ? null : familyPhone.trim();
    }

}