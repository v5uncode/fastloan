package org.yjht.bean.cust;

import java.math.BigDecimal;
import javax.persistence.*;

@Table(name = "FRONT_INFO_FIN_RPT")
public class FrontInfoFinRpt {
	
    @Column(name = "DATE_ID")
    private String dateId;

    @Id
    @Column(name = "CUST_ID")
    private String custId;

    @Column(name = "RPT_TYPE")
    private String rptType;

    @Column(name = "XJJYHCK")
    private BigDecimal xjjyhck;

    @Column(name = "YSZK")
    private BigDecimal yszk;

    @Column(name = "YFCX")
    private BigDecimal yfcx;

    @Column(name = "LDZC")
    private BigDecimal ldzc;

    @Column(name = "CK")
    private BigDecimal ck;

    @Column(name = "GDZC")
    private BigDecimal gdzc;

    @Column(name = "QTJYZC")
    private BigDecimal qtjyzc;

    @Column(name = "QTFJYZC")
    private BigDecimal qtfjyzc;

    @Column(name = "ZZC")
    private BigDecimal zzc;

    @Column(name = "ZFZK")
    private BigDecimal zfzk;

    @Column(name = "YSKX")
    private BigDecimal yskx;

    @Column(name = "DQDK")
    private BigDecimal dqdk;

    @Column(name = "DQFZ")
    private BigDecimal dqfz;

    @Column(name = "CQDK")
    private BigDecimal cqdk;

    @Column(name = "QTFZ")
    private BigDecimal qtfz;

    @Column(name = "ZFZ")
    private BigDecimal zfz;

    @Column(name = "QY")
    private BigDecimal qy;

    @Column(name = "JTHYFZ")
    private BigDecimal jthyfz;

    @Column(name = "ZYFCSL")
    private Integer zyfcsl;

    @Column(name = "ZCFZL")
    private BigDecimal zcfzl;
    
    @Column(name = "YSR")
    private BigDecimal ysr;
    
    @Column(name = "YZC")
    private BigDecimal yzc;

    @Column(name = "SDBL")
    private BigDecimal sdbl;

    @Column(name = "SR_1Y")
    private BigDecimal sr1y;

    @Column(name = "ZC_1Y")
    private BigDecimal zc1y;

    @Column(name = "SR_2Y")
    private BigDecimal sr2y;

    @Column(name = "ZC_2Y")
    private BigDecimal zc2y;

    @Column(name = "SR_3Y")
    private BigDecimal sr3y;

    @Column(name = "ZC_3Y")
    private BigDecimal zc3y;

    @Column(name = "CRT_DATE")
    private String crtDate;

    @Column(name = "MTN_DATE")
    private String mtnDate;

    @Column(name = "ZYXM_SR_1Y")
    private BigDecimal zyxmSr1y;

    @Column(name = "ZYXM_SR_2Y")
    private BigDecimal zyxmSr2y;

    @Column(name = "ZYXM_SR_3Y")
    private BigDecimal zyxmSr3y;

    @Column(name = "ZYXM_ZC_1Y")
    private BigDecimal zyxmZc1y;

    @Column(name = "ZYXM_ZC_2Y")
    private BigDecimal zyxmZc2y;

    @Column(name = "ZYXM_ZC_3Y")
    private BigDecimal zyxmZc3y;

    /**
     * @return DATE_ID
     */
    public String getDateId() {
        return dateId;
    }

    /**
     * @param dateId
     */
    public void setDateId(String dateId) {
        this.dateId = dateId == null ? null : dateId.trim();
    }

    /**
     * @return CUST_ID
     */
    public String getCustId() {
        return custId;
    }

    /**
     * @param custId
     */
    public void setCustId(String custId) {
        this.custId = custId == null ? null : custId.trim();
    }

    /**
     * @return RPT_TYPE
     */
    public String getRptType() {
        return rptType;
    }

    /**
     * @param rptType
     */
    public void setRptType(String rptType) {
        this.rptType = rptType == null ? null : rptType.trim();
    }

    /**
     * @return XJJYHCK
     */
    public BigDecimal getXjjyhck() {
        return xjjyhck;
    }

    /**
     * @param xjjyhck
     */
    public void setXjjyhck(BigDecimal xjjyhck) {
        this.xjjyhck = xjjyhck;
    }

    /**
     * @return YSZK
     */
    public BigDecimal getYszk() {
        return yszk;
    }

    /**
     * @param yszk
     */
    public void setYszk(BigDecimal yszk) {
        this.yszk = yszk;
    }

    /**
     * @return YFCX
     */
    public BigDecimal getYfcx() {
        return yfcx;
    }

    /**
     * @param yfcx
     */
    public void setYfcx(BigDecimal yfcx) {
        this.yfcx = yfcx;
    }

    /**
     * @return LDZC
     */
    public BigDecimal getLdzc() {
        return ldzc;
    }

    /**
     * @param ldzc
     */
    public void setLdzc(BigDecimal ldzc) {
        this.ldzc = ldzc;
    }

    /**
     * @return CK
     */
    public BigDecimal getCk() {
        return ck;
    }

    /**
     * @param ck
     */
    public void setCk(BigDecimal ck) {
        this.ck = ck;
    }

    /**
     * @return GDZC
     */
    public BigDecimal getGdzc() {
        return gdzc;
    }

    /**
     * @param gdzc
     */
    public void setGdzc(BigDecimal gdzc) {
        this.gdzc = gdzc;
    }

    /**
     * @return QTJYZC
     */
    public BigDecimal getQtjyzc() {
        return qtjyzc;
    }

    /**
     * @param qtjyzc
     */
    public void setQtjyzc(BigDecimal qtjyzc) {
        this.qtjyzc = qtjyzc;
    }

    /**
     * @return QTFJYZC
     */
    public BigDecimal getQtfjyzc() {
        return qtfjyzc;
    }

    /**
     * @param qtfjyzc
     */
    public void setQtfjyzc(BigDecimal qtfjyzc) {
        this.qtfjyzc = qtfjyzc;
    }

    /**
     * @return ZZC
     */
    public BigDecimal getZzc() {
        return zzc;
    }

    /**
     * @param zzc
     */
    public void setZzc(BigDecimal zzc) {
        this.zzc = zzc;
    }

    /**
     * @return ZFZK
     */
    public BigDecimal getZfzk() {
        return zfzk;
    }

    /**
     * @param zfzk
     */
    public void setZfzk(BigDecimal zfzk) {
        this.zfzk = zfzk;
    }

    /**
     * @return YSKX
     */
    public BigDecimal getYskx() {
        return yskx;
    }

    /**
     * @param yskx
     */
    public void setYskx(BigDecimal yskx) {
        this.yskx = yskx;
    }

    /**
     * @return DQDK
     */
    public BigDecimal getDqdk() {
        return dqdk;
    }

    /**
     * @param dqdk
     */
    public void setDqdk(BigDecimal dqdk) {
        this.dqdk = dqdk;
    }

    /**
     * @return DQFZ
     */
    public BigDecimal getDqfz() {
        return dqfz;
    }

    /**
     * @param dqfz
     */
    public void setDqfz(BigDecimal dqfz) {
        this.dqfz = dqfz;
    }

    /**
     * @return CQDK
     */
    public BigDecimal getCqdk() {
        return cqdk;
    }

    /**
     * @param cqdk
     */
    public void setCqdk(BigDecimal cqdk) {
        this.cqdk = cqdk;
    }

    /**
     * @return QTFZ
     */
    public BigDecimal getQtfz() {
        return qtfz;
    }

    /**
     * @param qtfz
     */
    public void setQtfz(BigDecimal qtfz) {
        this.qtfz = qtfz;
    }

    /**
     * @return ZFZ
     */
    public BigDecimal getZfz() {
        return zfz;
    }

    /**
     * @param zfz
     */
    public void setZfz(BigDecimal zfz) {
        this.zfz = zfz;
    }

    /**
     * @return QY
     */
    public BigDecimal getQy() {
        return qy;
    }

    /**
     * @param qy
     */
    public void setQy(BigDecimal qy) {
        this.qy = qy;
    }

    /**
     * @return JTHYFZ
     */
    public BigDecimal getJthyfz() {
        return jthyfz;
    }

    /**
     * @param jthyfz
     */
    public void setJthyfz(BigDecimal jthyfz) {
        this.jthyfz = jthyfz;
    }

    /**
     * @return ZYFCSL
     */
    public Integer getZyfcsl() {
        return zyfcsl;
    }

    /**
     * @param zyfcsl
     */
    public void setZyfcsl(Integer zyfcsl) {
        this.zyfcsl = zyfcsl;
    }

    /**
     * @return ZCFZL
     */
    public BigDecimal getZcfzl() {
        return zcfzl;
    }

    /**
     * @param zcfzl
     */
    public void setZcfzl(BigDecimal zcfzl) {
        this.zcfzl = zcfzl;
    }
    
    /**
     * @return YSR
     */
    public BigDecimal getYsr() {
		return ysr;
	}
    /**
     * @param ysr
     */
	public void setYsr(BigDecimal ysr) {
		this.ysr = ysr;
	}
	/**
	 * @return YZC
	 */
	public BigDecimal getYzc() {
		return yzc;
	}
	/**
	 * @param yzc
	 */
	public void setYzc(BigDecimal yzc) {
		this.yzc = yzc;
	}

	/**
     * @return SDBL
     */
    public BigDecimal getSdbl() {
        return sdbl;
    }

    /**
     * @param sdbl
     */
    public void setSdbl(BigDecimal sdbl) {
        this.sdbl = sdbl;
    }

    /**
     * @return SR_1Y
     */
    public BigDecimal getSr1y() {
        return sr1y;
    }

    /**
     * @param sr1y
     */
    public void setSr1y(BigDecimal sr1y) {
        this.sr1y = sr1y;
    }

    /**
     * @return ZC_1Y
     */
    public BigDecimal getZc1y() {
        return zc1y;
    }

    /**
     * @param zc1y
     */
    public void setZc1y(BigDecimal zc1y) {
        this.zc1y = zc1y;
    }

    /**
     * @return SR_2Y
     */
    public BigDecimal getSr2y() {
        return sr2y;
    }

    /**
     * @param sr2y
     */
    public void setSr2y(BigDecimal sr2y) {
        this.sr2y = sr2y;
    }

    /**
     * @return ZC_2Y
     */
    public BigDecimal getZc2y() {
        return zc2y;
    }

    /**
     * @param zc2y
     */
    public void setZc2y(BigDecimal zc2y) {
        this.zc2y = zc2y;
    }

    /**
     * @return SR_3Y
     */
    public BigDecimal getSr3y() {
        return sr3y;
    }

    /**
     * @param sr3y
     */
    public void setSr3y(BigDecimal sr3y) {
        this.sr3y = sr3y;
    }

    /**
     * @return ZC_3Y
     */
    public BigDecimal getZc3y() {
        return zc3y;
    }

    /**
     * @param zc3y
     */
    public void setZc3y(BigDecimal zc3y) {
        this.zc3y = zc3y;
    }

    /**
     * @return CRT_DATE
     */
    public String getCrtDate() {
        return crtDate;
    }

    /**
     * @param crtDate
     */
    public void setCrtDate(String crtDate) {
        this.crtDate = crtDate == null ? null : crtDate.trim();
    }

    /**
     * @return MTN_DATE
     */
    public String getMtnDate() {
        return mtnDate;
    }

    /**
     * @param mtnDate
     */
    public void setMtnDate(String mtnDate) {
        this.mtnDate = mtnDate == null ? null : mtnDate.trim();
    }

    /**
     * @return ZYXM_SR_1Y
     */
    public BigDecimal getZyxmSr1y() {
        return zyxmSr1y;
    }

    /**
     * @param zyxmSr1y
     */
    public void setZyxmSr1y(BigDecimal zyxmSr1y) {
        this.zyxmSr1y = zyxmSr1y;
    }

    /**
     * @return ZYXM_SR_2Y
     */
    public BigDecimal getZyxmSr2y() {
        return zyxmSr2y;
    }

    /**
     * @param zyxmSr2y
     */
    public void setZyxmSr2y(BigDecimal zyxmSr2y) {
        this.zyxmSr2y = zyxmSr2y;
    }

    /**
     * @return ZYXM_SR_3Y
     */
    public BigDecimal getZyxmSr3y() {
        return zyxmSr3y;
    }

    /**
     * @param zyxmSr3y
     */
    public void setZyxmSr3y(BigDecimal zyxmSr3y) {
        this.zyxmSr3y = zyxmSr3y;
    }

    /**
     * @return ZYXM_ZC_1Y
     */
    public BigDecimal getZyxmZc1y() {
        return zyxmZc1y;
    }

    /**
     * @param zyxmZc1y
     */
    public void setZyxmZc1y(BigDecimal zyxmZc1y) {
        this.zyxmZc1y = zyxmZc1y;
    }

    /**
     * @return ZYXM_ZC_2Y
     */
    public BigDecimal getZyxmZc2y() {
        return zyxmZc2y;
    }

    /**
     * @param zyxmZc2y
     */
    public void setZyxmZc2y(BigDecimal zyxmZc2y) {
        this.zyxmZc2y = zyxmZc2y;
    }

    /**
     * @return ZYXM_ZC_3Y
     */
    public BigDecimal getZyxmZc3y() {
        return zyxmZc3y;
    }

    /**
     * @param zyxmZc3y
     */
    public void setZyxmZc3y(BigDecimal zyxmZc3y) {
        this.zyxmZc3y = zyxmZc3y;
    }

	@Override
	public String toString() {
		return "FrontInfoFinRpt [dateId=" + dateId + ", custId=" + custId + ", rptType=" + rptType + ", xjjyhck="
				+ xjjyhck + ", yszk=" + yszk + ", yfcx=" + yfcx + ", ldzc=" + ldzc + ", ck=" + ck + ", gdzc=" + gdzc
				+ ", qtjyzc=" + qtjyzc + ", qtfjyzc=" + qtfjyzc + ", zzc=" + zzc + ", zfzk=" + zfzk + ", yskx=" + yskx
				+ ", dqdk=" + dqdk + ", dqfz=" + dqfz + ", cqdk=" + cqdk + ", qtfz=" + qtfz + ", zfz=" + zfz + ", qy="
				+ qy + ", jthyfz=" + jthyfz + ", zyfcsl=" + zyfcsl + ", zcfzl=" + zcfzl + ", sdbl=" + sdbl + ", sr1y="
				+ sr1y + ", zc1y=" + zc1y + ", sr2y=" + sr2y + ", zc2y=" + zc2y + ", sr3y=" + sr3y + ", zc3y=" + zc3y
				+ ", crtDate=" + crtDate + ", mtnDate=" + mtnDate + ", zyxmSr1y=" + zyxmSr1y + ", zyxmSr2y=" + zyxmSr2y
				+ ", zyxmSr3y=" + zyxmSr3y + ", zyxmZc1y=" + zyxmZc1y + ", zyxmZc2y=" + zyxmZc2y + ", zyxmZc3y="
				+ zyxmZc3y + "]";
	}
    
}