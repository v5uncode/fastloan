package org.yjht.bean.cust;

import javax.persistence.*;

@Table(name = "CUST_OTHER_INFO")
public class CustOtherInfo {
    @Id
    @Column(name = "CUST_ID")
    private String custId;

    @Column(name = "INFO")
    private String info;

    @Column(name = "CRT_DATE")
    private String crtDate;

    @Column(name = "MTN_DATE")
    private String mtnDate;

    @Column(name = "BAD_BEHAVIOR")
    private String badBehavior;
    
    @Column(name = "EVALUATE")
    private String evaluate;//信贷员评价
    /**
     * @return CUST_ID
     */
    public String getCustId() {
        return custId;
    }

    /**
     * @param custId
     */
    public void setCustId(String custId) {
        this.custId = custId == null ? null : custId.trim();
    }

    /**
     * @return INFO
     */
    public String getInfo() {
        return info;
    }

    /**
     * @param info
     */
    public void setInfo(String info) {
        this.info = info == null ? null : info.trim();
    }

    /**
     * @return CRT_DATE
     */
    public String getCrtDate() {
        return crtDate;
    }

    /**
     * @param crtDate
     */
    public void setCrtDate(String crtDate) {
        this.crtDate = crtDate ;
    }

    /**
     * @return MTN_DATE
     */
    public String getMtnDate() {
        return mtnDate;
    }

    /**
     * @param mtnDate
     */
    public void setMtnDate(String mtnDate) {
        this.mtnDate = mtnDate;
    }

    /**
     * @return BAD_BEHAVIOR
     */
    public String getBadBehavior() {
        return badBehavior;
    }

    /**
     * @param badBehavior
     */
    public void setBadBehavior(String badBehavior) {
        this.badBehavior = badBehavior == null ? null : badBehavior.trim();
    }
    /**
     * @return EVALUATE
     */
	public String getEvaluate() {
		return evaluate;
	}
	/**
	 * 
	 * @param evaluate
	 */
	public void setEvaluate(String evaluate) {
		this.evaluate = evaluate == null ? null : evaluate.trim();;
	}
    
}