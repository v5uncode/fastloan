package org.yjht.bean.cust;

import java.math.BigDecimal;
import javax.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Table(name = "cust_pledge")
@Setter
@Getter
public class CustPledge {
    /**
     * 质押ID
     */
    @Id
    @Column(name = "PLEDGE_ID")
    private String pledgeId;

    /**
     * 客户ID
     */
    @Column(name = "CUST_ID")
    private String custId;

    /**
     * 是否第三方质押
     */
    @Column(name = "SFDSFZY")
    private String sfdsfzy;

    /**
     * 出质人姓名
     */
    @Column(name = "CZRXM")
    private String czrxm;

    /**
     * 证件类型
     */
    @Column(name = "ZJLX")
    private String zjlx;

    /**
     * 证件号码
     */
    @Column(name = "ZJHM")
    private String zjhm;

    /**
     * 地址
     */
    @Column(name = "DZ")
    private String dz;

    /**
     * 邮编
     */
    @Column(name = "YB")
    private String yb;

    /**
     * 质物种类
     */
    @Column(name = "ZWZL")
    private String zwzl;

    /**
     * 质押起始日
     */
    @Column(name = "ZYQSR")
    private String zyqsr;

    /**
     * 质押终止日
     */
    @Column(name = "ZYZZR")
    private String zyzzr;

    /**
     * 单证号码
     */
    @Column(name = "DZHM")
    private String dzhm;

    /**
     * 质物面值
     */
    @Column(name = "ZWMZ")
    private BigDecimal zwmz;

    /**
     * 折人民币金额
     */
    @Column(name = "ZRMBJE")
    private BigDecimal zrmbje;

    /**
     * 质押比例
     */
    @Column(name = "ZYBL")
    private BigDecimal zybl;

    /**
     * 担保主债权
     */
    @Column(name = "DBZZQ")
    private BigDecimal dbzzq;

    /**
     * 质物情况说明
     */
    @Column(name = "ZYWQKSM")
    private String zywqksm;

    /**
     * 质物保管地点
     */
    @Column(name = "ZYBGDD")
    private String zybgdd;

    /**
     * 退保金金额
     */
    @Column(name = "TBJJE")
    private BigDecimal tbjje;

    /**
     * 保险公司
     */
    @Column(name = "BXGS")
    private String bxgs;

    /**
     * 质押状态
     */
    @Column(name = "ZYZT")
    private String zyzt;

    /**
     * 备注
     */
    @Column(name = "BZ")
    private String bz;

    /**
     * 数据创建日期
     */
    @Column(name = "CRT_DATE")
    private String crtDate;
    
    /**
     * 数据修改日期
     */
    @Column(name = "MTN_DATE")
    private String mtnDate;
    
}