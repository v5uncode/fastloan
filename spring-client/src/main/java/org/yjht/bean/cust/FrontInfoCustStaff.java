package org.yjht.bean.cust;

import java.math.BigDecimal;
import javax.persistence.*;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
@Table(name = "FRONT_INFO_CUST_STAFF")
public class FrontInfoCustStaff {
    @Id
    @Column(name = "CUST_ID")
    private String custId;

    @Column(name = "WORK_NAME")
    private String workName;

    @Column(name = "DEPT")
    private String dept;

    @Column(name = "DUTY")
    private String duty;

    @Column(name = "WORK_DATE")
    private String workDate;

    @Column(name = "WORK_YEARS")
    private Integer workYears;

    @Column(name = "ZB_FLAG")
    private String zbFlag;

    @Column(name = "CRT_DATE")
    private String crtDate;

    @Column(name = "MTN_DATE")
    private String mtnDate;

    @Column(name = "YLBXAMT")
    private BigDecimal ylbxamt; //医疗保险缴纳金额

    @Column(name = "MONTHAMT")
    private BigDecimal monthamt;  //工资月收入

    @Column(name = "SUBORD_INDUSTRY")
    private String subordIndustry;

    @Column(name = "PROFESS_TITLE")
    private String professTitle;

    @Column(name = "WORK_ADDRESS")
    private String workAddress;
    
    @Column(name = "BUS_INSURANCE")
    private String busInsurance;
    
    @Column(name = "DWXZ")
    private String dwxz;
    
    @Column(name = "GWXZ")
    private String gwxz;   
}