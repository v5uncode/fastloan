package org.yjht.bean.cust;

import java.math.BigDecimal;
import javax.persistence.*;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Table(name = "FRONT_INFO_CUST_BREED")
@Setter
@Getter
@ToString
public class FrontInfoCustBreed {
    @Id
    @Column(name = "ID")
    private String id;

    @Column(name = "CUST_ID")
    private String custId;

    @Column(name = "YZ_TYPE")
    private String yzType;
    
    /**
     * 养殖种类
     */
    @Column(name = "YZ_KIND")
    private String yzKind;
    
    @Column(name = "YZ_MODEL")
    private String yzModel;//养殖模式
    
    @Column(name = "YZ_CNT")
    private BigDecimal yzCnt;

    @Column(name = "CL_SJ")
    private String clSj;

    @Column(name = "BIZ_DATE")
    private String bizDate;

    @Column(name = "BIZ_YEARS")
    private Integer bizYears;

    @Column(name = "INCOME_Y")
    private BigDecimal incomeY;

    @Column(name = "PROFIT_Y")
    private BigDecimal profitY;

    @Column(name = "CRT_DATE")
    private String crtDate;

    @Column(name = "MTN_DATE")
    private String mtnDate;
    
    @Column(name = "YZ_SCALE")
    private String yzScale;//养殖规模
    
    @Column(name = "YZ_CYCLE")
    private Integer yzCycle;//养殖周期(月)
    
    @Column(name = "YZ_LOAN_USE")
    private String yzLoanUse;//海水养殖贷款用途
    
    @Column(name = "BIZ_FIXEDASSET")
    private BigDecimal bizFixedasset;//项目固定资产
    
    @Column(name = "FY_ABILITY")   
    private String fyAbility;   //防疫意识和防疫能力
    
    @Column(name = "STAFF_INSURANCE")
    private String staffInsurance;  //是否购买养殖保险

}