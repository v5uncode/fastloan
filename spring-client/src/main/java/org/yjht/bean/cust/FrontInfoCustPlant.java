package org.yjht.bean.cust;

import java.math.BigDecimal;
import javax.persistence.*;

@Table(name = "FRONT_INFO_CUST_PLANT")
public class FrontInfoCustPlant {
    @Id
    @Column(name = "ID")
    private String id;

    @Column(name = "CUST_ID")
    private String custId;

    @Column(name = "ZW_TYPE")
    private String zwType;

    @Column(name = "ZZ_MS")
    private BigDecimal zzMs;

    @Column(name = "ZZ_MODEL")
    private String zzModel;

    @Column(name = "MC")
    private BigDecimal mc;

    @Column(name = "XJ_SJ")
    private String xjSj;

    @Column(name = "BIZ_DATE")
    private String bizDate;

    @Column(name = "BIZ_YEARS")
    private BigDecimal bizYears;

    @Column(name = "INCOME_Y")
    private BigDecimal incomeY;

    @Column(name = "PROFIT_Y")
    private BigDecimal profitY;

    @Column(name = "CRT_DATE")
    private String crtDate;

    @Column(name = "MTN_DATE")
    private String mtnDate;

    @Column(name = "CROP_TYPE")
    private String cropType;

    @Column(name = "CROP_AGE")
    private BigDecimal cropAge;

    @Column(name = "TEN_MS")
    private BigDecimal tenMs;

    @Column(name = "FIVETOTEN_MS")
    private BigDecimal fivetotenMs;

    @Column(name = "LESSFIVE_MS")
    private BigDecimal lessfiveMs;

    /**
     * @return ID
     */
    public String getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    /**
     * @return CUST_ID
     */
    public String getCustId() {
        return custId;
    }

    /**
     * @param custId
     */
    public void setCustId(String custId) {
        this.custId = custId == null ? null : custId.trim();
    }

    /**
     * @return ZW_TYPE
     */
    public String getZwType() {
        return zwType;
    }

    /**
     * @param zwType
     */
    public void setZwType(String zwType) {
        this.zwType = zwType == null ? null : zwType.trim();
    }

    /**
     * @return ZZ_MS
     */
    public BigDecimal getZzMs() {
        return zzMs;
    }

    /**
     * @param zzMs
     */
    public void setZzMs(BigDecimal zzMs) {
        this.zzMs = zzMs;
    }

    /**
     * @return ZZ_MODEL
     */
    public String getZzModel() {
        return zzModel;
    }

    /**
     * @param zzModel
     */
    public void setZzModel(String zzModel) {
        this.zzModel = zzModel == null ? null : zzModel.trim();
    }

    /**
     * @return MC
     */
    public BigDecimal getMc() {
        return mc;
    }

    /**
     * @param mc
     */
    public void setMc(BigDecimal mc) {
        this.mc = mc;
    }

    /**
     * @return XJ_SJ
     */
    public String getXjSj() {
        return xjSj;
    }

    /**
     * @param xjSj
     */
    public void setXjSj(String xjSj) {
        this.xjSj = xjSj == null ? null : xjSj.trim();
    }

    /**
     * @return BIZ_DATE
     */
    public String getBizDate() {
        return bizDate;
    }

    /**
     * @param bizDate
     */
    public void setBizDate(String bizDate) {
        this.bizDate = bizDate == null ? null : bizDate.trim();
    }

    /**
     * @return BIZ_YEARS
     */
    public BigDecimal getBizYears() {
        return bizYears;
    }

    /**
     * @param bizYears
     */
    public void setBizYears(BigDecimal bizYears) {
        this.bizYears = bizYears;
    }

    /**
     * @return INCOME_Y
     */
    public BigDecimal getIncomeY() {
        return incomeY;
    }

    /**
     * @param incomeY
     */
    public void setIncomeY(BigDecimal incomeY) {
        this.incomeY = incomeY;
    }

    /**
     * @return PROFIT_Y
     */
    public BigDecimal getProfitY() {
        return profitY;
    }

    /**
     * @param profitY
     */
    public void setProfitY(BigDecimal profitY) {
        this.profitY = profitY;
    }

    /**
     * @return CRT_DATE
     */
    public String getCrtDate() {
        return crtDate;
    }

    /**
     * @param crtDate
     */
    public void setCrtDate(String crtDate) {
        this.crtDate = crtDate == null ? null : crtDate.trim();
    }

    /**
     * @return MTN_DATE
     */
    public String getMtnDate() {
        return mtnDate;
    }

    /**
     * @param mtnDate
     */
    public void setMtnDate(String mtnDate) {
        this.mtnDate = mtnDate == null ? null : mtnDate.trim();
    }

    /**
     * @return CROP_TYPE
     */
    public String getCropType() {
        return cropType;
    }

    /**
     * @param cropType
     */
    public void setCropType(String cropType) {
        this.cropType = cropType == null ? null : cropType.trim();
    }

    /**
     * @return CROP_AGE
     */
    public BigDecimal getCropAge() {
        return cropAge;
    }

    /**
     * @param cropAge
     */
    public void setCropAge(BigDecimal cropAge) {
        this.cropAge = cropAge;
    }

    /**
     * @return TEN_MS
     */
    public BigDecimal getTenMs() {
        return tenMs;
    }

    /**
     * @param tenMs
     */
    public void setTenMs(BigDecimal tenMs) {
        this.tenMs = tenMs;
    }

    /**
     * @return FIVETOTEN_MS
     */
    public BigDecimal getFivetotenMs() {
        return fivetotenMs;
    }

    /**
     * @param fivetotenMs
     */
    public void setFivetotenMs(BigDecimal fivetotenMs) {
        this.fivetotenMs = fivetotenMs;
    }

    /**
     * @return LESSFIVE_MS
     */
    public BigDecimal getLessfiveMs() {
        return lessfiveMs;
    }

    /**
     * @param lessfiveMs
     */
    public void setLessfiveMs(BigDecimal lessfiveMs) {
        this.lessfiveMs = lessfiveMs;
    }

	@Override
	public String toString() {
		return "FrontInfoCustPlant [id=" + id + ", custId=" + custId + ", zwType=" + zwType + ", zzMs=" + zzMs
				+ ", zzModel=" + zzModel + ", mc=" + mc + ", xjSj=" + xjSj + ", bizDate=" + bizDate + ", bizYears="
				+ bizYears + ", incomeY=" + incomeY + ", profitY=" + profitY + ", crtDate=" + crtDate + ", mtnDate="
				+ mtnDate + ", cropType=" + cropType + ", cropAge=" + cropAge + ", tenMs=" + tenMs + ", fivetotenMs="
				+ fivetotenMs + ", lessfiveMs=" + lessfiveMs + "]";
	}
    
}