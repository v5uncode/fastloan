package org.yjht.bean.cust;

import javax.persistence.*;

@Table(name = "CUST_BUSINESS")
public class CustBusiness {
    @Id
    @Column(name = "CUST_ID")
    private String custId;

    @Column(name = "LICENCE_NAME")
    private String licenceName;

    @Column(name = "LICENCE_NO")
    private String licenceNo;

    @Column(name = "STORE_ADDRESS")
    private String storeAddress;

    @Column(name = "OPERATOR_NAME")
    private String operatorName;

    @Column(name = "EMPLOYEE_COUNT")
    private Integer employeeCount;

    @Column(name = "BX_CNT")
    private Integer bxCnt;

    @Column(name = "CRT_DATE")
    private String crtDate;

    @Column(name = "MTN_DATE")
    private String mtnDate;
    
    @Column(name = "CREATE_DATE")
    private String createDate;
    
    @Column(name = "BUS_INSURANCE")
    private String busInsurance;
    
    /**
     * @return CREATE_DATE
     */
    public String getCreateDate() {
		return createDate;
	}
    /**
     * @param createDate
     */
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	/**
     * @return CUST_ID
     */
    public String getCustId() {
        return custId;
    }

    /**
     * @param custId
     */
    public void setCustId(String custId) {
        this.custId = custId == null ? null : custId.trim();
    }

    /**
     * @return LICENCE_NAME
     */
    public String getLicenceName() {
        return licenceName;
    }

    /**
     * @param licenceName
     */
    public void setLicenceName(String licenceName) {
        this.licenceName = licenceName == null ? null : licenceName.trim();
    }

    /**
     * @return LICENCE_NO
     */
    public String getLicenceNo() {
        return licenceNo;
    }

    /**
     * @param licenceNo
     */
    public void setLicenceNo(String licenceNo) {
        this.licenceNo = licenceNo == null ? null : licenceNo.trim();
    }

    /**
     * @return STORE_ADDRESS
     */
    public String getStoreAddress() {
        return storeAddress;
    }

    /**
     * @param storeAddress
     */
    public void setStoreAddress(String storeAddress) {
        this.storeAddress = storeAddress == null ? null : storeAddress.trim();
    }

    /**
     * @return OPERATOR_NAME
     */
    public String getOperatorName() {
        return operatorName;
    }

    /**
     * @param operatorName
     */
    public void setOperatorName(String operatorName) {
        this.operatorName = operatorName == null ? null : operatorName.trim();
    }

    /**
     * @return EMPLOYEE_COUNT
     */
    public Integer getEmployeeCount() {
        return employeeCount;
    }

    /**
     * @param employeeCount
     */
    public void setEmployeeCount(Integer employeeCount) {
        this.employeeCount = employeeCount;
    }

    /**
     * @return BX_CNT
     */
    public Integer getBxCnt() {
        return bxCnt;
    }

    /**
     * @param bxCnt
     */
    public void setBxCnt(Integer bxCnt) {
        this.bxCnt = bxCnt;
    }

    /**
     * @return CRT_DATE
     */
    public String getCrtDate() {
        return crtDate;
    }

    /**
     * @param crtDate
     */
    public void setCrtDate(String crtDate) {
        this.crtDate = crtDate == null ? null : crtDate.trim();
    }

    /**
     * @return MTN_DATE
     */
    public String getMtnDate() {
        return mtnDate;
    }

    /**
     * @param mtnDate
     */
    public void setMtnDate(String mtnDate) {
        this.mtnDate = mtnDate == null ? null : mtnDate.trim();
    }
    /**
     * @return BUS_INSURANCE
     */
	public String getBusInsurance() {
		return busInsurance;
	}
	/**
	 * 
	 * @param busInsurance
	 */
	public void setBusInsurance(String busInsurance) {
		this.busInsurance = busInsurance == null ? null : busInsurance.trim();
	}
	@Override
	public String toString() {
		return "CustBusiness [custId=" + custId + ", licenceName=" + licenceName + ", licenceNo=" + licenceNo
				+ ", storeAddress=" + storeAddress + ", operatorName=" + operatorName + ", employeeCount="
				+ employeeCount + ", bxCnt=" + bxCnt + ", crtDate=" + crtDate + ", mtnDate=" + mtnDate + ", createDate="
				+ createDate + ", busInsurance=" + busInsurance + "]";
	}
    
}