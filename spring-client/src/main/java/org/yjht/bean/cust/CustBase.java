package org.yjht.bean.cust;

import javax.persistence.*;

import lombok.Getter;
import lombok.Setter;

@Table(name = "cust_base")
@Setter
@Getter
public class CustBase {
    /**
     * 客户ID
     */
    @Id
    @Column(name = "CUST_ID")
    private String custId;

    /**
     * 客户姓名
     */
    @Column(name = "CUST_NAME")
    private String custName;

    /**
     * 证件类型
     */
    @Column(name = "ID_TYPE")
    private String idType;

    /**
     * 证件号码
     */
    @Column(name = "ID_NO")
    private String idNo;

    /**
     * 性别
     */
    @Column(name = "SEX")
    private String sex;

    /**
     * 客户类型
     */
    @Column(name = "CUST_TYPE")
    private String custType;

    /**
     * 客户性质

     */
    @Column(name = "NATURE")
    private String nature;

    /**
     * 学历

     */
    @Column(name = "EDU_LEVEL")
    private String eduLevel;

    /**
     * 职业
     */
    @Column(name = "VOCATION")
    private String vocation;
    
    /**
     * 职业细分
     */
    @Column(name = "OCC_SUBDIVISION")
    private String occSubdivision;

    /**
     * 行业分类
     */
    @Column(name = "INDUSTRY_CLASS")
    private String industryClass;
    
    /**
     * 法人号
     */
    @Column(name = "CORP_CD")
    private String corpCd;

    /**
     * 所属客户经理

     */
    @Column(name = "CUST_GRP_JL")
    private String custGrpJl;

    /**
     * 客户经理名称

     */
    @Column(name = "CUST_GRP_NAME")
    private String custGrpName;

    /**
     * 客户经理所属机构

     */
    @Column(name = "ORG_CD")
    private String orgCd;

    /**
     * 客户经理所属机构名称

     */
    @Column(name = "ORG_NAME")
    private String orgName;

    /**
     * 家庭主营项目

     */
    @Column(name = "MAIN_PROJECT")
    private String mainProject;

    /**
     * 民族

     */
    @Column(name = "NATION")
    private String nation;

    /**
     * 最高学位

     */
    @Column(name = "HIGEST_DEGREE")
    private String higestDegree;

    /**
     * 关系人

     */
    @Column(name = "PARTY")
    private String party;

    /**
     * 出生日期

     */
    @Column(name = "BIRTH_DATE")
    private String birthDate;

    /**
     * 年龄

     */
    @Column(name = "CUST_AGE")
    private Integer custAge;

    /**
     * 电话

     */
    @Column(name = "TEL_NO")
    private String telNo;

    /**
     * 备用电话

     */
    @Column(name = "TEL_NO_SPARE")
    private String telNoSpare;

    /**
     * 家庭地址

     */
    @Column(name = "ADDRESS")
    private String address;

    /**
     * 所在城市

     */
    @Column(name = "CITY")
    private String city;

    /**
     * 所属区域1(县级)

     */
    @Column(name = "AREA_CD")
    private String areaCd;

    /**
     * 所属区域2(乡镇)

     */
    @Column(name = "AREA_CD2")
    private String areaCd2;

    /**
     * 所属区域3(村/居委会)

     */
    @Column(name = "AREA_CD3")
    private String areaCd3;

    /**
     * 客户信息完整性1:不完整 2：完整

     */
    @Column(name = "CREATE_TYPE")
    private String createType;

    /**
     * 删除标记0:正常  1：已删除

     */
    @Column(name = "ISDEL")
    private String isdel;
    
    /**
     * 是否是正式客户
     */
    @Column(name = "IS_TMP")
    private String isTmp;

    /**
     * 客户数据来源

     */
    @Column(name = "DATAFROM")
    private String datafrom;

    /**
     * 是否授信

     */
    @Column(name = "ISCREDIT")
    private String iscredit;

    /**
     * 发证机关

     */
    @Column(name = "GOV")
    private String gov;

    /**
     * 证件有效期开始日期

     */
    @Column(name = "CARDDATESTART")
    private String carddatestart;

    /**
     * 证件有效期结束日期

     */
    @Column(name = "CARDDATEEND")
    private String carddateend;

    /**
     * 客户信息创建日期

     */
    @Column(name = "CRT_DATE")
    private String crtDate;

    /**
     * 客户信息最近一次修改日期

     */
    @Column(name = "MTN_DATE")
    private String mtnDate;

    /**
     * 商业保险购买情况

     */
    @Column(name = "BUS_INSURANCE")
    private String busInsurance;

    /**
     * 其他收入来源
     */
    @Column(name = "OTHER_INCOME")
    private String otherIncome;

    /**
     * 本行是否开立账户
     */
    @Column(name = "OPEN_ACCOUNT")
    private String openAccount;

    /**
     * 户籍性质
     */
    @Column(name = "HJXZ")
    private String hjxz;

    /**
     * 户籍地址
     */
    @Column(name = "HJDZ")
    private String hjdz;

    /**
     * 健康状况
     */
    @Column(name = "JKZK")
    private String jkzk;

    /**
     * 本地持续居住年限
     */
    @Column(name = "JZNX")
    private String jznx;

    /**
     * 公司规模
     */
    @Column(name = "GSGM")
    private String gsgm;

    /**
     * 客户分类
     */
    @Column(name = "KHFL")
    private String khfl;
    
    /**
     * 行业
     */
    @Column(name = "HY")
    private String hy;
    
    @Transient    //非数据库表字段
    String childsOrgCd;
    
    
    @Transient    //非数据库表字段  居住年
    String jzYear;
    
    @Transient    //非数据库表字段  居住年
    String jzMonth;
    
}