package org.yjht.bean.cust;

import javax.persistence.*;

@Table(name = "FRONT_INFO_CUST_DOC")
public class FrontInfoCustDoc {
    @Id
    @Column(name = "SEQ_NO")
    private String seqNo;

    @Column(name = "CUST_ID")
    private String custId;

    @Column(name = "DOC_NAME")
    private String docName;

    @Column(name = "FILE_NAME")
    private String fileName;

    @Column(name = "DOC_PATH")
    private String docPath;

    @Column(name = "CRT_DATE")
    private String crtDate;

    @Column(name = "MTN_DATE")
    private String mtnDate;

    @Column(name = "DOC_DESC")
    private String docDesc;

    /**
     * @return SEQ_NO
     */
    public String getSeqNo() {
        return seqNo;
    }

    /**
     * @param seqNo
     */
    public void setSeqNo(String seqNo) {
        this.seqNo = seqNo == null ? null : seqNo.trim();
    }

    /**
     * @return CUST_ID
     */
    public String getCustId() {
        return custId;
    }

    /**
     * @param custId
     */
    public void setCustId(String custId) {
        this.custId = custId == null ? null : custId.trim();
    }

    /**
     * @return DOC_NAME
     */
    public String getDocName() {
        return docName;
    }

    /**
     * @param docName
     */
    public void setDocName(String docName) {
        this.docName = docName == null ? null : docName.trim();
    }

    /**
     * @return FILE_NAME
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * @param fileName
     */
    public void setFileName(String fileName) {
        this.fileName = fileName == null ? null : fileName.trim();
    }

    /**
     * @return DOC_PATH
     */
    public String getDocPath() {
        return docPath;
    }

    /**
     * @param docPath
     */
    public void setDocPath(String docPath) {
        this.docPath = docPath == null ? null : docPath.trim();
    }

    /**
     * @return CRT_DATE
     */
    public String getCrtDate() {
        return crtDate;
    }

    /**
     * @param crtDate
     */
    public void setCrtDate(String crtDate) {
        this.crtDate = crtDate == null ? null : crtDate.trim();
    }

    /**
     * @return MTN_DATE
     */
    public String getMtnDate() {
        return mtnDate;
    }

    /**
     * @param mtnDate
     */
    public void setMtnDate(String mtnDate) {
        this.mtnDate = mtnDate == null ? null : mtnDate.trim();
    }

    /**
     * @return DOC_DESC
     */
    public String getDocDesc() {
        return docDesc;
    }

    /**
     * @param docDesc
     */
    public void setDocDesc(String docDesc) {
        this.docDesc = docDesc == null ? null : docDesc.trim();
    }

	@Override
	public String toString() {
		return "FrontInfoCustDoc [seqNo=" + seqNo + ", custId=" + custId + ", docName=" + docName + ", fileName="
				+ fileName + ", docPath=" + docPath + ", crtDate=" + crtDate + ", mtnDate=" + mtnDate + ", docDesc="
				+ docDesc + "]";
	}
    
}