package org.yjht.bean.cust;

import java.math.BigDecimal;
import javax.persistence.*;

@Table(name = "FRONT_INFO_CUST_RELA")
public class FrontInfoCustRela {
    @Id
    @Column(name = "SEQ_NO")
    private String seqNo;

    @Column(name = "CUST_ID")
    private String custId;

    @Column(name = "VS_NAME")
    private String vsName;
    
    @Transient
    private String vsMing;

    @Column(name = "RELA_TYPE")
    private String relaType;

    @Column(name = "ID_TYPE")
    private String idType;

    @Column(name = "ID_NO")
    private String idNo;

    @Column(name = "TEL_NO")
    private String telNo;

    @Column(name = "VOCATION")
    private String vocation;

    @Column(name = "WORK_INFO")
    private String workInfo;

    @Column(name = "OTH_DESC")
    private String othDesc;

    @Column(name = "JH_FLAG")
    private String jhFlag;

    @Column(name = "INCOME_Y")
    private BigDecimal incomeY;

    @Column(name = "CRT_DATE")
    private String crtDate;

    @Column(name = "MTN_DATE")
    private String mtnDate;

    @Column(name = "SEX")
    private String sex;

    @Column(name = "AGE")
    private Integer age;

    @Column(name = "ISDEL")
    private String isdel;
    
    @Column(name = "WORK_ADDRESS")
    private String workAddress;
    
    @Column(name = "INCOME_M")
    private BigDecimal incomeM;
    
    /**
     * @return SEQ_NO
     */
    public String getSeqNo() {
        return seqNo;
    }

    /**
     * @param seqNo
     */
    public void setSeqNo(String seqNo) {
        this.seqNo = seqNo == null ? null : seqNo.trim();
    }

    /**
     * @return CUST_ID
     */
    public String getCustId() {
        return custId;
    }

    /**
     * @param custId
     */
    public void setCustId(String custId) {
        this.custId = custId == null ? null : custId.trim();
    }

    /**
     * @return VS_NAME
     */
    public String getVsName() {
        return vsName;
    }

    /**
     * @param vsName
     */
    public void setVsName(String vsName) {
        this.vsName = vsName == null ? null : vsName.trim();
    }

    /**
     * @return RELA_TYPE
     */
    public String getRelaType() {
        return relaType;
    }

    /**
     * @param relaType
     */
    public void setRelaType(String relaType) {
        this.relaType = relaType == null ? null : relaType.trim();
    }

    /**
     * @return ID_TYPE
     */
    public String getIdType() {
        return idType;
    }

    /**
     * @param idType
     */
    public void setIdType(String idType) {
        this.idType = idType == null ? null : idType.trim();
    }

    /**
     * @return ID_NO
     */
    public String getIdNo() {
        return idNo;
    }

    /**
     * @param idNo
     */
    public void setIdNo(String idNo) {
        this.idNo = idNo == null ? null : idNo.trim();
    }

    /**
     * @return TEL_NO
     */
    public String getTelNo() {
        return telNo;
    }

    /**
     * @param telNo
     */
    public void setTelNo(String telNo) {
        this.telNo = telNo == null ? null : telNo.trim();
    }

    /**
     * @return VOCATION
     */
    public String getVocation() {
        return vocation;
    }

    /**
     * @param vocation
     */
    public void setVocation(String vocation) {
        this.vocation = vocation == null ? null : vocation.trim();
    }

    /**
     * @return WORK_INFO
     */
    public String getWorkInfo() {
        return workInfo;
    }

    /**
     * @param workInfo
     */
    public void setWorkInfo(String workInfo) {
        this.workInfo = workInfo == null ? null : workInfo.trim();
    }

    /**
     * @return OTH_DESC
     */
    public String getOthDesc() {
        return othDesc;
    }

    /**
     * @param othDesc
     */
    public void setOthDesc(String othDesc) {
        this.othDesc = othDesc == null ? null : othDesc.trim();
    }

    /**
     * @return JH_FLAG
     */
    public String getJhFlag() {
        return jhFlag;
    }

    /**
     * @param jhFlag
     */
    public void setJhFlag(String jhFlag) {
        this.jhFlag = jhFlag == null ? null : jhFlag.trim();
    }

    /**
     * @return INCOME_Y
     */
    public BigDecimal getIncomeY() {
        return incomeY;
    }

    /**
     * @param incomeY
     */
    public void setIncomeY(BigDecimal incomeY) {
        this.incomeY = incomeY;
    }

    /**
     * @return CRT_DATE
     */
    public String getCrtDate() {
        return crtDate;
    }

    /**
     * @param crtDate
     */
    public void setCrtDate(String crtDate) {
        this.crtDate = crtDate == null ? null : crtDate.trim();
    }

    /**
     * @return MTN_DATE
     */
    public String getMtnDate() {
        return mtnDate;
    }

    /**
     * @param mtnDate
     */
    public void setMtnDate(String mtnDate) {
        this.mtnDate = mtnDate == null ? null : mtnDate.trim();
    }

    /**
     * @return SEX
     */
    public String getSex() {
        return sex;
    }

    /**
     * @param sex
     */
    public void setSex(String sex) {
        this.sex = sex == null ? null : sex.trim();
    }

    /**
     * @return AGE
     */
    public Integer getAge() {
        return age;
    }

    /**
     * @param age
     */
    public void setAge(Integer age) {
        this.age = age;
    }

    /**
     * @return ISDEL
     */
    public String getIsdel() {
        return isdel;
    }

    /**
     * @param isdel
     */
    public void setIsdel(String isdel) {
        this.isdel = isdel == null ? null : isdel.trim();
    }

	public String getWorkAddress() {
		return workAddress;
	}

	public void setWorkAddress(String workAddress) {
		this.workAddress = workAddress== null ? null : workAddress.trim();
	}

	public BigDecimal getIncomeM() {
		return incomeM;
	}

	public void setIncomeM(BigDecimal incomeM) {
		this.incomeM = incomeM== null ? null : incomeM;
	}

	public String getVsMing() {
		return vsMing;
	}

	public void setVsMing(String vsMing) {
		this.vsMing = vsMing;
	}

	@Override
	public String toString() {
		return "FrontInfoCustRela [seqNo=" + seqNo + ", custId=" + custId + ", vsName=" + vsName + ", vsMing=" + vsMing
				+ ", relaType=" + relaType + ", idType=" + idType + ", idNo=" + idNo + ", telNo=" + telNo
				+ ", vocation=" + vocation + ", workInfo=" + workInfo + ", othDesc=" + othDesc + ", jhFlag=" + jhFlag
				+ ", incomeY=" + incomeY + ", crtDate=" + crtDate + ", mtnDate=" + mtnDate + ", sex=" + sex + ", age="
				+ age + ", isdel=" + isdel + ", workAddress=" + workAddress + ", incomeM=" + incomeM + "]";
	}
	
    
}