package org.yjht.bean.cust;

import javax.persistence.*;

@Table(name = "COMPLETE_CUST_INFO")
public class CompleteCustInfo {
    @Column(name = "CUST_ID")
    @Id
    private String custId;

    @Column(name = "TYPE")
    @Id
    private String type;

    @Column(name = "CURRENT_NUM")
    private Integer currentNum;

    @Column(name = "ISCOMPLETE")
    private String iscomplete;

    /**
     * @return CUST_ID
     */
    public String getCustId() {
        return custId;
    }

    /**
     * @param custId
     */
    public void setCustId(String custId) {
        this.custId = custId == null ? null : custId.trim();
    }

    /**
     * @return TYPE
     */
    public String getType() {
        return type;
    }

    /**
     * @param type
     */
    public void setType(String type) {
        this.type = type == null ? null : type.trim();
    }

    /**
     * @return CURRENT_NUM
     */
    public Integer getCurrentNum() {
        return currentNum;
    }

    /**
     * @param currentNum
     */
    public void setCurrentNum(Integer currentNum) {
        this.currentNum = currentNum;
    }

    /**
     * @return ISCOMPLETE
     */
    public String getIscomplete() {
        return iscomplete;
    }

    /**
     * @param iscomplete
     */
    public void setIscomplete(String iscomplete) {
        this.iscomplete = iscomplete == null ? null : iscomplete.trim();
    }
}