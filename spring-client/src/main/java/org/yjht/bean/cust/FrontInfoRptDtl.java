package org.yjht.bean.cust;

import javax.persistence.*;

@Table(name = "FRONT_INFO_RPT_DTL")
public class FrontInfoRptDtl {
    @Id
    @Column(name = "RPT_DTL_ID")
    private String rptDtlId;

    @Column(name = "DATE_ID")
    private String dateId;

    @Column(name = "CUST_ID")
    private String custId;

    @Column(name = "SUBJ_CD")
    private String subjCd;

    @Column(name = "DTL_SEQ")
    private Integer dtlSeq;

    @Column(name = "DTL_TYPE")
    private String dtlType;

    @Column(name = "SUBJ_TYPE")
    private String subjType;

    @Column(name = "DTL_NUM")
    private String dtlNum;

    @Column(name = "DTL_DESC")
    private String dtlDesc;

    @Column(name = "SUBJ_VAL")
    private String subjVal;

    @Column(name = "CRT_DATE")
    private String crtDate;

    @Column(name = "MTN_DATE")
    private String mtnDate;

    /**
     * @return RPT_DTL_ID
     */
    public String getRptDtlId() {
        return rptDtlId;
    }

    /**
     * @param rptDtlId
     */
    public void setRptDtlId(String rptDtlId) {
        this.rptDtlId = rptDtlId == null ? null : rptDtlId.trim();
    }

    /**
     * @return DATE_ID
     */
    public String getDateId() {
        return dateId;
    }

    /**
     * @param dateId
     */
    public void setDateId(String dateId) {
        this.dateId = dateId == null ? null : dateId.trim();
    }

    /**
     * @return CUST_ID
     */
    public String getCustId() {
        return custId;
    }

    /**
     * @param custId
     */
    public void setCustId(String custId) {
        this.custId = custId == null ? null : custId.trim();
    }

    /**
     * @return SUBJ_CD
     */
    public String getSubjCd() {
        return subjCd;
    }

    /**
     * @param subjCd
     */
    public void setSubjCd(String subjCd) {
        this.subjCd = subjCd == null ? null : subjCd.trim();
    }

    /**
     * @return DTL_SEQ
     */
    public Integer getDtlSeq() {
        return dtlSeq;
    }

    /**
     * @param dtlSeq
     */
    public void setDtlSeq(Integer dtlSeq) {
        this.dtlSeq = dtlSeq;
    }

    /**
     * @return DTL_TYPE
     */
    public String getDtlType() {
        return dtlType;
    }

    /**
     * @param dtlType
     */
    public void setDtlType(String dtlType) {
        this.dtlType = dtlType == null ? null : dtlType.trim();
    }

    /**
     * @return SUBJ_TYPE
     */
    public String getSubjType() {
        return subjType;
    }

    /**
     * @param subjType
     */
    public void setSubjType(String subjType) {
        this.subjType = subjType == null ? null : subjType.trim();
    }

    /**
     * @return DTL_NUM
     */
    public String getDtlNum() {
        return dtlNum;
    }

    /**
     * @param dtlNum
     */
    public void setDtlNum(String dtlNum) {
        this.dtlNum = dtlNum == null ? null : dtlNum.trim();
    }

    /**
     * @return DTL_DESC
     */
    public String getDtlDesc() {
        return dtlDesc;
    }

    /**
     * @param dtlDesc
     */
    public void setDtlDesc(String dtlDesc) {
        this.dtlDesc = dtlDesc == null ? null : dtlDesc.trim();
    }

    /**
     * @return SUBJ_VAL
     */
    public String getSubjVal() {
        return subjVal;
    }

    /**
     * @param subjVal
     */
    public void setSubjVal(String subjVal) {
        this.subjVal = subjVal == null ? null : subjVal.trim();
    }

    /**
     * @return CRT_DATE
     */
    public String getCrtDate() {
        return crtDate;
    }

    /**
     * @param crtDate
     */
    public void setCrtDate(String crtDate) {
        this.crtDate = crtDate == null ? null : crtDate.trim();
    }

    /**
     * @return MTN_DATE
     */
    public String getMtnDate() {
        return mtnDate;
    }

    /**
     * @param mtnDate
     */
    public void setMtnDate(String mtnDate) {
        this.mtnDate = mtnDate == null ? null : mtnDate.trim();
    }

	@Override
	public String toString() {
		return "FrontInfoRptDtl [rptDtlId=" + rptDtlId + ", dateId=" + dateId + ", custId=" + custId + ", subjCd="
				+ subjCd + ", dtlSeq=" + dtlSeq + ", dtlType=" + dtlType + ", subjType=" + subjType + ", dtlNum="
				+ dtlNum + ", dtlDesc=" + dtlDesc + ", subjVal=" + subjVal + ", crtDate=" + crtDate + ", mtnDate="
				+ mtnDate + "]";
	}
    
}