package org.yjht.bean.cust;

import javax.persistence.*;

@Table(name = "CUST_PLANT")
public class CustPlant {
    @Id
    @Column(name = "CUST_ID")
    private String custId;

    @Column(name = "HEADWATER_STATUS")
    private String headwaterStatus;//水源情况

    @Column(name = "AGRO_INSURANCE")
    private String agroInsurance;//购买农业保险

    @Column(name = "LAND_CONSTACT_NO")
    private String landConstactNo;//土地经营承包权证
    
    @Column(name = "IS_LAND")
    private String isLand;//本年度的经营地是否为轮耕地、休耕地
    
    /**
     * 数据创建日期

     */
    @Column(name = "CRT_DATE")
    private String crtDate;

    /**
     * 数据修改日期

     */
    @Column(name = "MTN_DATE")
    private String mtnDate;

    /**
     * @return CUST_ID
     */
    public String getCustId() {
        return custId;
    }

    /**
     * @param custId
     */
    public void setCustId(String custId) {
        this.custId = custId == null ? null : custId.trim();
    }

    /**
     * @return HEADWATER_STATUS
     */
    public String getHeadwaterStatus() {
        return headwaterStatus;
    }

    /**
     * @param headwaterStatus
     */
    public void setHeadwaterStatus(String headwaterStatus) {
        this.headwaterStatus = headwaterStatus == null ? null : headwaterStatus.trim();
    }

    /**
     * @return AGRO_INSURANCE
     */
    public String getAgroInsurance() {
        return agroInsurance;
    }

    /**
     * @param agroInsurance
     */
    public void setAgroInsurance(String agroInsurance) {
        this.agroInsurance = agroInsurance == null ? null : agroInsurance.trim();
    }

    /**
     * @return LAND_CONSTACT_NO
     */
    public String getLandConstactNo() {
        return landConstactNo;
    }

    /**
     * @param landConstactNo
     */
    public void setLandConstactNo(String landConstactNo) {
        this.landConstactNo = landConstactNo == null ? null : landConstactNo.trim();
    }

	public String getIsLand() {
		return isLand;
	}

	public void setIsLand(String isLand) {
		this.isLand = isLand == null ? null : isLand.trim();
	}
	
	/**
     * 获取数据创建日期

     *
     * @return MTN_DATE - 数据创建日期

     */
    public String getCrtDate() {
        return crtDate;
    }

    /**
     * 设置数据创建日期

     *
     * @param mtnDate 数据创建日期

     */
    public void setCrtData(String crtData) {
        this.crtDate = crtDate == null ? null : crtDate.trim();
    }

    /**
     * 获取数据修改日期

     *
     * @return MTN_DATE - 数据修改日期

     */
    public String getMtnDate() {
        return mtnDate;
    }

    /**
     * 设置数据修改日期

     *
     * @param mtnDate 数据修改日期

     */
    public void setMtnDate(String mtnDate) {
        this.mtnDate = mtnDate == null ? null : mtnDate.trim();
    }
}