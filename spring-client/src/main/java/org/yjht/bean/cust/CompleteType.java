package org.yjht.bean.cust;

import javax.persistence.*;

@Table(name = "COMPLETE_TYPE")
public class CompleteType {
    @Id
    @Column(name = "TYPE")
    private String type;

    @Column(name = "TOTAL")
    private Integer total;
    
    @Column(name = "TYPE_DESC")
    private String typeDesc;
    
    @Column(name = "DATA_TYPE")
    private String dataType;

    /**
     * @return TYPE
     */
    public String getType() {
        return type;
    }

    /**
     * @param type
     */
    public void setType(String type) {
        this.type = type == null ? null : type.trim();
    }

    /**
     * @return TOTAL
     */
    public Integer getTotal() {
        return total;
    }

    /**
     * @param total
     */
    public void setTotal(Integer total) {
        this.total = total;
    }
    /**
     * @return TYPE_DESC
     */
	public String getTypeDesc() {
		return typeDesc;
	}
	/**
	 * @param typeDesc
	 */
	public void setTypeDesc(String typeDesc) {
		this.typeDesc = typeDesc == null ? null : typeDesc.trim();;
	}
	/**
	 * @return DATA_TYPE
	 */
	public String getDataType() {
		return dataType;
	}
	/**
	 * @param dataType
	 */
	public void setDataType(String dataType) {
		this.dataType = dataType  == null ? null : dataType.trim();;
	}
    
    
}