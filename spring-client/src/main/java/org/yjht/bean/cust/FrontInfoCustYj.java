package org.yjht.bean.cust;

import javax.persistence.*;

@Table(name = "FRONT_INFO_CUST_YJ")
public class FrontInfoCustYj {
    @Id
    @Column(name = "YJ_ID")
    private String yjId;

    @Column(name = "USER_ID")
    private String userId;

    @Column(name = "USER_ID_SQ")
    private String userIdSq;

    @Column(name = "CUST_ID")
    private String custId;

    @Column(name = "STAT")
    private String stat;

    @Column(name = "CRT_DATE")
    private String crtDate;

    @Column(name = "UPD_DATE")
    private String updDate;

    @Column(name = "CONTENT")
    private String content;

    /**
     * @return YJ_ID
     */
    public String getYjId() {
        return yjId;
    }

    /**
     * @param yjId
     */
    public void setYjId(String yjId) {
        this.yjId = yjId == null ? null : yjId.trim();
    }

    /**
     * @return USER_ID
     */
    public String getUserId() {
        return userId;
    }

    /**
     * @param userId
     */
    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    /**
     * @return USER_ID_SQ
     */
    public String getUserIdSq() {
        return userIdSq;
    }

    /**
     * @param userIdSq
     */
    public void setUserIdSq(String userIdSq) {
        this.userIdSq = userIdSq == null ? null : userIdSq.trim();
    }

    /**
     * @return CUST_ID
     */
    public String getCustId() {
        return custId;
    }

    /**
     * @param custId
     */
    public void setCustId(String custId) {
        this.custId = custId == null ? null : custId.trim();
    }

    /**
     * @return STAT
     */
    public String getStat() {
        return stat;
    }

    /**
     * @param stat
     */
    public void setStat(String stat) {
        this.stat = stat == null ? null : stat.trim();
    }

    /**
     * @return CRT_DATE
     */
    public String getCrtDate() {
        return crtDate;
    }

    /**
     * @param crtDate
     */
    public void setCrtDate(String crtDate) {
        this.crtDate = crtDate == null ? null : crtDate.trim();
    }

    /**
     * @return UPD_DATE
     */
    public String getUpdDate() {
        return updDate;
    }

    /**
     * @param updDate
     */
    public void setUpdDate(String updDate) {
        this.updDate = updDate == null ? null : updDate.trim();
    }

    /**
     * @return CONTENT
     */
    public String getContent() {
        return content;
    }

    /**
     * @param content
     */
    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }
}