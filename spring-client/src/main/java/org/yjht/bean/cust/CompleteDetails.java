package org.yjht.bean.cust;

import javax.persistence.*;

@Table(name = "COMPLETE_DETAILS")
public class CompleteDetails {
    @Column(name = "ID")
    @Id
    private Integer id;

    @Column(name = "TYPE")
    private String type;

    @Column(name = "PROPERTY")
    private String property;
    
    @Column(name = "IS_VALID")
    private String isValid;

    @Column(name = "IS_FIXED")
    private String isFixed;
    /**
     * @return ID
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return TYPE
     */
    public String getType() {
        return type;
    }

    /**
     * @param type
     */
    public void setType(String type) {
        this.type = type == null ? null : type.trim();
    }

    /**
     * @return PROPERTY
     */
    public String getProperty() {
        return property;
    }

    /**
     * @param property
     */
    public void setProperty(String property) {
        this.property = property == null ? null : property.trim();
    }
    /**
     * @return IS_VALID
     */
	public String getIsValid() {
		return isValid;
	}
	
	/**
	 * @param isValid
	 */
	public void setIsValid(String isValid) {
		this.isValid = isValid == null ? null : isValid.trim();
	}
	/**
	 * @return IS_FIXED
	 */
	public String getIsFixed() {
		return isFixed;
	}
	/**
	 * @param isFixed
	 */
	public void setIsFixed(String isFixed) {
		this.isFixed = isFixed == null ? null : isFixed.trim();
	}
    
    
}