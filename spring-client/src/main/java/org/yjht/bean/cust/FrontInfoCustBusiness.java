package org.yjht.bean.cust;

import java.math.BigDecimal;
import javax.persistence.*;

@Table(name = "FRONT_INFO_CUST_BUSINESS")
public class FrontInfoCustBusiness {
    @Id
    @Column(name = "ID")
    private String id;

    @Column(name = "CUST_ID")
    private String custId;

    @Column(name = "BIZ_HY")
    private String bizHy;

    @Column(name = "BIZ_XM")
    private String bizXm;

    @Column(name = "BIZ_DATE")
    private String bizDate;

    @Column(name = "BIZ_YEARS")
    private BigDecimal bizYears;

    @Column(name = "OWN_FLAG")
    private String ownFlag;

    @Column(name = "OWN_AREA")
    private BigDecimal ownArea;

    @Column(name = "RENT_FLAG")
    private String rentFlag;

    @Column(name = "RENT_AREA")
    private BigDecimal rentArea;

    @Column(name = "WORKERS")
    private BigDecimal workers;

    @Column(name = "INCOME_Y")
    private BigDecimal incomeY;

    @Column(name = "PROFIT_Y")
    private BigDecimal profitY;

    @Column(name = "BX_CNT")
    private BigDecimal bxCnt;

    @Column(name = "CRT_DATE")
    private String crtDate;

    @Column(name = "MTN_DATE")
    private String mtnDate;


    @Column(name = "REG_BRAND")      
    private String regBrand;//是否注册商标

    @Column(name = "PATANT_STATUS") 
    private String patantStatus;//专利获得情况

    @Column(name = "CHANNEL_NUM")
    private String channelNum;//销售渠道是否稳定

    @Column(name = "ALLIBUSINESS_STATUS")
    private String allibusinessStatus;//是否是加盟商/总代理

    @Column(name = "CHAIN_STATUS")
    private String chainStatus;//是否连锁经营

    @Column(name = "POSITION_INFO")
    private String positionInfo;//经营地段

    @Column(name = "CONTRACT")
    private String contract;//是否有经营合同
    
    @Column(name = "DOWN_CONYEARS")
    private BigDecimal downConyears;//下游客户合作年限
    
    @Column(name = "UP_CONYEARS")
    private BigDecimal upConyears;//上游客户合作年限
    
    @Column(name = "PRE_PUT")
    private BigDecimal prePut;//前期成本投入

    /**
     * @return ID
     */
    public String getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    /**
     * @return CUST_ID
     */
    public String getCustId() {
        return custId;
    }

    /**
     * @param custId
     */
    public void setCustId(String custId) {
        this.custId = custId == null ? null : custId.trim();
    }

    /**
     * @return BIZ_HY
     */
    public String getBizHy() {
        return bizHy;
    }

    /**
     * @param bizHy
     */
    public void setBizHy(String bizHy) {
        this.bizHy = bizHy == null ? null : bizHy.trim();
    }

    /**
     * @return BIZ_XM
     */
    public String getBizXm() {
        return bizXm;
    }

    /**
     * @param bizXm
     */
    public void setBizXm(String bizXm) {
        this.bizXm = bizXm == null ? null : bizXm.trim();
    }

    /**
     * @return BIZ_DATE
     */
    public String getBizDate() {
        return bizDate;
    }

    /**
     * @param bizDate
     */
    public void setBizDate(String bizDate) {
        this.bizDate = bizDate == null ? null : bizDate.trim();
    }

    /**
     * @return BIZ_YEARS
     */
    public BigDecimal getBizYears() {
        return bizYears;
    }

    /**
     * @param bizYears
     */
    public void setBizYears(BigDecimal bizYears) {
        this.bizYears = bizYears;
    }

    /**
     * @return OWN_FLAG
     */
    public String getOwnFlag() {
        return ownFlag;
    }

    /**
     * @param ownFlag
     */
    public void setOwnFlag(String ownFlag) {
        this.ownFlag = ownFlag == null ? null : ownFlag.trim();
    }

    /**
     * @return OWN_AREA
     */
    public BigDecimal getOwnArea() {
        return ownArea;
    }

    /**
     * @param ownArea
     */
    public void setOwnArea(BigDecimal ownArea) {
        this.ownArea = ownArea;
    }

    /**
     * @return RENT_FLAG
     */
    public String getRentFlag() {
        return rentFlag;
    }

    /**
     * @param rentFlag
     */
    public void setRentFlag(String rentFlag) {
        this.rentFlag = rentFlag == null ? null : rentFlag.trim();
    }

    /**
     * @return RENT_AREA
     */
    public BigDecimal getRentArea() {
        return rentArea;
    }

    /**
     * @param rentArea
     */
    public void setRentArea(BigDecimal rentArea) {
        this.rentArea = rentArea;
    }

    /**
     * @return WORKERS
     */
    public BigDecimal getWorkers() {
        return workers;
    }

    /**
     * @param workers
     */
    public void setWorkers(BigDecimal workers) {
        this.workers = workers;
    }

    /**
     * @return INCOME_Y
     */
    public BigDecimal getIncomeY() {
        return incomeY;
    }

    /**
     * @param incomeY
     */
    public void setIncomeY(BigDecimal incomeY) {
        this.incomeY = incomeY;
    }

    /**
     * @return PROFIT_Y
     */
    public BigDecimal getProfitY() {
        return profitY;
    }

    /**
     * @param profitY
     */
    public void setProfitY(BigDecimal profitY) {
        this.profitY = profitY;
    }

    /**
     * @return BX_CNT
     */
    public BigDecimal getBxCnt() {
        return bxCnt;
    }

    /**
     * @param bxCnt
     */
    public void setBxCnt(BigDecimal bxCnt) {
        this.bxCnt = bxCnt;
    }

    /**
     * @return CRT_DATE
     */
    public String getCrtDate() {
        return crtDate;
    }

    /**
     * @param crtDate
     */
    public void setCrtDate(String crtDate) {
        this.crtDate = crtDate == null ? null : crtDate.trim();
    }

    /**
     * @return MTN_DATE
     */
    public String getMtnDate() {
        return mtnDate;
    }

    /**
     * @param mtnDate
     */
    public void setMtnDate(String mtnDate) {
        this.mtnDate = mtnDate == null ? null : mtnDate.trim();
    }

    /**
     * @return REG_BRAND
     */
    public String getRegBrand() {
        return regBrand;
    }

    /**
     * @param regBrand
     */
    public void setRegBrand(String regBrand) {
        this.regBrand = regBrand == null ? null : regBrand.trim();
    }

    /**
     * @return PATANT_STATUS
     */
    public String getPatantStatus() {
        return patantStatus;
    }

    /**
     * @param patantStatus
     */
    public void setPatantStatus(String patantStatus) {
        this.patantStatus = patantStatus == null ? null : patantStatus.trim();
    }

    /**
     * @return CHANNEL_NUM
     */
    public String getChannelNum() {
        return channelNum;
    }

    /**
     * @param channelNum
     */
    public void setChannelNum(String channelNum) {
        this.channelNum = channelNum == null ? null : channelNum.trim();
    }

    /**
     * @return ALLIBUSINESS_STATUS
     */
    public String getAllibusinessStatus() {
        return allibusinessStatus;
    }

    /**
     * @param allibusinessStatus
     */
    public void setAllibusinessStatus(String allibusinessStatus) {
        this.allibusinessStatus = allibusinessStatus == null ? null : allibusinessStatus.trim();
    }

    /**
     * @return CHAIN_STATUS
     */
    public String getChainStatus() {
        return chainStatus;
    }

    /**
     * @param chainStatus
     */
    public void setChainStatus(String chainStatus) {
        this.chainStatus = chainStatus == null ? null : chainStatus.trim();
    }

    /**
     * @return POSITION_INFO
     */
    public String getPositionInfo() {
        return positionInfo;
    }

    /**
     * @param positionInfo
     */
    public void setPositionInfo(String positionInfo) {
        this.positionInfo = positionInfo == null ? null : positionInfo.trim();
    }

    /**
     * @return CONTRACT
     */
    public String getContract() {
        return contract;
    }

    /**
     * @param contract
     */
    public void setContract(String contract) {
        this.contract = contract == null ? null : contract.trim();
    }

	public BigDecimal getDownConyears() {
		return downConyears;
	}

	public void setDownConyears(BigDecimal downConyears) {
		this.downConyears = downConyears;
	}

	public BigDecimal getUpConyears() {
		return upConyears;
	}

	public void setUpConyears(BigDecimal upConyears) {
		this.upConyears = upConyears;
	}

	public BigDecimal getPrePut() {
		return prePut;
	}

	public void setPrePut(BigDecimal prePut) {
		this.prePut = prePut;
	}

	@Override
	public String toString() {
		return "FrontInfoCustBusiness [id=" + id + ", custId=" + custId + ", bizHy=" + bizHy + ", bizXm=" + bizXm
				+ ", bizDate=" + bizDate + ", bizYears=" + bizYears + ", ownFlag=" + ownFlag + ", ownArea=" + ownArea
				+ ", rentFlag=" + rentFlag + ", rentArea=" + rentArea + ", workers=" + workers + ", incomeY=" + incomeY
				+ ", profitY=" + profitY + ", bxCnt=" + bxCnt + ", crtDate=" + crtDate + ", mtnDate=" + mtnDate
				+ ", regBrand=" + regBrand + ", patantStatus=" + patantStatus + ", channelNum=" + channelNum
				+ ", allibusinessStatus=" + allibusinessStatus + ", chainStatus=" + chainStatus + ", positionInfo="
				+ positionInfo + ", contract=" + contract + ", downConyears=" + downConyears + ", upConyears="
				+ upConyears + ", prePut=" + prePut + "]";
	}
    
}