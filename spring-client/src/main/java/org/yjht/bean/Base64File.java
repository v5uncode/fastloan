package org.yjht.bean;

public class Base64File {
	private String index;
	private String custId;
	private String docName;
	private String docDesc;
	private String data;
	
	
	public String getIndex() {
		return index;
	}

	public void setIndex(String index) {
		this.index = index;
	}

	public String getCustId() {
		return custId;
	}

	public void setCustId(String custId) {
		this.custId = custId;
	}

	public String getDocName() {
		return docName;
	}

	public void setDocName(String docName) {
		this.docName = docName;
	}

	public String getDocDesc() {
		return docDesc;
	}

	public void setDocDesc(String docDesc) {
		this.docDesc = docDesc;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "Base64File [custId=" + custId + ", docName=" + docName + ", docDesc=" + docDesc + ", data=" + data
				+ "]";
	}

	
	
	
}
