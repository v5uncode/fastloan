package org.yjht.bean;

import javax.persistence.*;

@Table(name = "front_info_loan")
public class FrontInfoLoan {
    /**
     * ID
     */
    @Id
    @Column(name = "LOAN_ID")
    private String loanId;
    /**
     * 审查审批ID
     */
    @Column(name = "SCSP_ID")
    private String scspId;

    /**
     * 客户编号
     */
    @Column(name = "CUST_ID")
    private String custId;

    @Column(name = "LOAN_DATE")
    private String loanDate;

    /**
     * 客户姓名
     */
    @Column(name = "CUST_NAME")
    private String custName;

    /**
     * 放款状态
     */
    @Column(name = "LOAN_STATUS")
    private String loanStatus;

    /**
     * 放款金额
     */
    @Column(name = "LOAN_MONEY")
    private String loanMoney;

    /**
     * 客户经理编号
     */
    @Column(name = "CUST_GRP_JL")
    private String custGrpJl;

    /**
     * 机构号
     */
    @Column(name = "ORG_CD")
    private String orgCd;

    /**
     * 所属机构
     */
    @Column(name = "ORG_NAME")
    private String orgName;

    /**
     * 法人号
     */
    @Column(name = "CORP_CD")
    private String corpCd;

    /**
     * 客户经理姓名
     */
    @Column(name = "CUST_GRP_NAME")
    private String custGrpName;

    /**
     * 申请期限
     */
    @Column(name = "APPLY_LIMIT")
    private String applyLimit;

    /**
     * 数据创建日期

     */
    @Column(name = "CRT_DATE")
    private String crtDate;

    /**
     * 数据修改日期

     */
    @Column(name = "MTN_DATE")
    private String mtnDate;

    @Column(name = "EXT1")
    private String ext1;

    @Column(name = "EXT2")
    private String ext2;

    @Column(name = "EXT3")
    private String ext3;

    /**
     * 获取审查审批ID
     *
     * @return SCSP_ID - 审查审批ID
     */
    public String getScspId() {
        return scspId;
    }

    /**
     * 设置审查审批ID
     *
     * @param scspId 审查审批ID
     */
    public void setScspId(String scspId) {
        this.scspId = scspId == null ? null : scspId.trim();
    }

    /**
     * 获取客户编号
     *
     * @return CUST_ID - 客户编号
     */
    public String getCustId() {
        return custId;
    }

    /**
     * 设置客户编号
     *
     * @param custId 客户编号
     */
    public void setCustId(String custId) {
        this.custId = custId == null ? null : custId.trim();
    }

    /**
     * @return LOAN_DATE
     */
    public String getLoanDate() {
        return loanDate;
    }

    /**
     * @param loanDate
     */
    public void setLoanDate(String loanDate) {
        this.loanDate = loanDate == null ? null : loanDate.trim();
    }

    /**
     * 获取客户姓名
     *
     * @return CUST_NAME - 客户姓名
     */
    public String getCustName() {
        return custName;
    }

    /**
     * 设置客户姓名
     *
     * @param custName 客户姓名
     */
    public void setCustName(String custName) {
        this.custName = custName == null ? null : custName.trim();
    }

    /**
     * 获取放款状态
     *
     * @return LOAN_STATUS - 放款状态
     */
    public String getLoanStatus() {
        return loanStatus;
    }

    /**
     * 设置放款状态
     *
     * @param loanStatus 放款状态
     */
    public void setLoanStatus(String loanStatus) {
        this.loanStatus = loanStatus == null ? null : loanStatus.trim();
    }

    /**
     * 获取放款金额
     *
     * @return LOAN_MONEY - 放款金额
     */
    public String getLoanMoney() {
        return loanMoney;
    }

    /**
     * 设置放款金额
     *
     * @param loanMoney 放款金额
     */
    public void setLoanMoney(String loanMoney) {
        this.loanMoney = loanMoney == null ? null : loanMoney.trim();
    }

    /**
     * 获取客户经理编号
     *
     * @return CUST_GRP_JL - 客户经理编号
     */
    public String getCustGrpJl() {
        return custGrpJl;
    }

    /**
     * 设置客户经理编号
     *
     * @param custGrpJl 客户经理编号
     */
    public void setCustGrpJl(String custGrpJl) {
        this.custGrpJl = custGrpJl == null ? null : custGrpJl.trim();
    }

    /**
     * 获取机构号
     *
     * @return ORG_CD - 机构号
     */
    public String getOrgCd() {
        return orgCd;
    }

    /**
     * 设置机构号
     *
     * @param orgCd 机构号
     */
    public void setOrgCd(String orgCd) {
        this.orgCd = orgCd == null ? null : orgCd.trim();
    }

    /**
     * 获取所属机构
     *
     * @return ORG_NAME - 所属机构
     */
    public String getOrgName() {
        return orgName;
    }

    /**
     * 设置所属机构
     *
     * @param orgName 所属机构
     */
    public void setOrgName(String orgName) {
        this.orgName = orgName == null ? null : orgName.trim();
    }

    /**
     * 获取法人号
     *
     * @return CORP_CD - 法人号
     */
    public String getCorpCd() {
        return corpCd;
    }

    /**
     * 设置法人号
     *
     * @param corpCd 法人号
     */
    public void setCorpCd(String corpCd) {
        this.corpCd = corpCd == null ? null : corpCd.trim();
    }

    /**
     * 获取客户经理姓名
     *
     * @return CUST_GRP_NAME - 客户经理姓名
     */
    public String getCustGrpName() {
        return custGrpName;
    }

    /**
     * 设置客户经理姓名
     *
     * @param custGrpName 客户经理姓名
     */
    public void setCustGrpName(String custGrpName) {
        this.custGrpName = custGrpName == null ? null : custGrpName.trim();
    }

    /**
     * 获取申请期限
     *
     * @return APPLY_LIMIT - 申请期限
     */
    public String getApplyLimit() {
        return applyLimit;
    }

    /**
     * 设置申请期限
     *
     * @param applyLimit 申请期限
     */
    public void setApplyLimit(String applyLimit) {
        this.applyLimit = applyLimit == null ? null : applyLimit.trim();
    }

    /**
     * 获取数据创建日期

     *
     * @return CRT_DATE - 数据创建日期

     */
    public String getCrtDate() {
        return crtDate;
    }

    /**
     * 设置数据创建日期

     *
     * @param crtDate 数据创建日期

     */
    public void setCrtDate(String crtDate) {
        this.crtDate = crtDate == null ? null : crtDate.trim();
    }

    /**
     * 获取数据修改日期

     *
     * @return MTN_DATE - 数据修改日期

     */
    public String getMtnDate() {
        return mtnDate;
    }

    /**
     * 设置数据修改日期

     *
     * @param mtnDate 数据修改日期

     */
    public void setMtnDate(String mtnDate) {
        this.mtnDate = mtnDate == null ? null : mtnDate.trim();
    }

    /**
     * @return EXT1
     */
    public String getExt1() {
        return ext1;
    }

    /**
     * @param ext1
     */
    public void setExt1(String ext1) {
        this.ext1 = ext1 == null ? null : ext1.trim();
    }

    /**
     * @return EXT2
     */
    public String getExt2() {
        return ext2;
    }

    /**
     * @param ext2
     */
    public void setExt2(String ext2) {
        this.ext2 = ext2 == null ? null : ext2.trim();
    }

    /**
     * @return EXT3
     */
    public String getExt3() {
        return ext3;
    }

    /**
     * @param ext3
     */
    public void setExt3(String ext3) {
        this.ext3 = ext3 == null ? null : ext3.trim();
    }

    public String getLoanId() {
        return loanId;
    }

    public void setLoanId(String loanId) {
        this.loanId = loanId;
    }
}