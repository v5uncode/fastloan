package org.yjht.bean;

/**
 * @author lengleng
 * @date 2018/6/5
 */
public class FrontInfoScspVo extends FrontInfoScsp {
    private String loanStatus;

    public String getLoanStatus() {
        return loanStatus;
    }

    public void setLoanStatus(String loanStatus) {
        this.loanStatus = loanStatus;
    }
}
