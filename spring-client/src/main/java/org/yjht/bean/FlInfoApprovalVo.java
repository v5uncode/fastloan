package org.yjht.bean;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/***
 * 
 * @author djl
 *
 */
@Setter
@Getter
@ToString
public class FlInfoApprovalVo extends FlInfoApproval{
	/**
	 * 运算是否异常
	 */
	private Boolean isException;
	/**
	 * 异常信息
	 */
	private String exceptionInfo;
	/**
	 * 是否通过
	 */
	private Boolean isPass;
	/**
	 * 不通过理由
	 */
	private String notPassReason;
	
	
	
}
