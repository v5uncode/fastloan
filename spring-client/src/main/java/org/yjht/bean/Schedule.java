package org.yjht.bean;

import javax.persistence.*;

public class Schedule {
    @Id
    @Column(name = "SCHEDULE_ID")
    private String scheduleId;

    @Column(name = "USER_ID")
    private String userId;

    @Column(name = "USER_NAME")
    private String userName;

    @Column(name = "WORK_DATE")
    private String workDate;

    @Column(name = "BGN_TIME")
    private String bgnTime;

    @Column(name = "END_TIME")
    private String endTime;

    @Column(name = "WORK_TITLE")
    private String workTitle;

    @Column(name = "WORK_CONT")
    private String workCont;

    @Column(name = "WARN_RULE")
    private String warnRule;

    @Column(name = "WARN_CNT")
    private String warnCnt;

    @Column(name = "FINISH_FLAG")
    private String finishFlag;

    @Column(name = "WORK_DESC")
    private String workDesc;

    @Column(name = "CRT_DATE")
    private String crtDate;

    @Column(name = "MTN_DATE")
    private String mtnDate;

    /**
     * @return SCHEDULE_ID
     */
    public String getScheduleId() {
        return scheduleId;
    }

    /**
     * @param scheduleId
     */
    public void setScheduleId(String scheduleId) {
        this.scheduleId = scheduleId == null ? null : scheduleId.trim();
    }

    /**
     * @return USER_ID
     */
    public String getUserId() {
        return userId;
    }

    /**
     * @param userId
     */
    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    /**
     * @return USER_NAME
     */
    public String getUserName() {
        return userName;
    }

    /**
     * @param userName
     */
    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }

    /**
     * @return WORK_DATE
     */
    public String getWorkDate() {
        return workDate;
    }

    /**
     * @param workDate
     */
    public void setWorkDate(String workDate) {
        this.workDate = workDate == null ? null : workDate.trim();
    }

    /**
     * @return BGN_TIME
     */
    public String getBgnTime() {
        return bgnTime;
    }

    /**
     * @param bgnTime
     */
    public void setBgnTime(String bgnTime) {
        this.bgnTime = bgnTime == null ? null : bgnTime.trim();
    }

    /**
     * @return END_TIME
     */
    public String getEndTime() {
        return endTime;
    }

    /**
     * @param endTime
     */
    public void setEndTime(String endTime) {
        this.endTime = endTime == null ? null : endTime.trim();
    }

    /**
     * @return WORK_TITLE
     */
    public String getWorkTitle() {
        return workTitle;
    }

    /**
     * @param workTitle
     */
    public void setWorkTitle(String workTitle) {
        this.workTitle = workTitle == null ? null : workTitle.trim();
    }

    /**
     * @return WORK_CONT
     */
    public String getWorkCont() {
        return workCont;
    }

    /**
     * @param workCont
     */
    public void setWorkCont(String workCont) {
        this.workCont = workCont == null ? null : workCont.trim();
    }

    /**
     * @return WARN_RULE
     */
    public String getWarnRule() {
        return warnRule;
    }

    /**
     * @param warnRule
     */
    public void setWarnRule(String warnRule) {
        this.warnRule = warnRule == null ? null : warnRule.trim();
    }

    /**
     * @return WARN_CNT
     */
    public String getWarnCnt() {
        return warnCnt;
    }

    /**
     * @param warnCnt
     */
    public void setWarnCnt(String warnCnt) {
        this.warnCnt = warnCnt == null ? null : warnCnt.trim();
    }

    /**
     * @return FINISH_FLAG
     */
    public String getFinishFlag() {
        return finishFlag;
    }

    /**
     * @param finishFlag
     */
    public void setFinishFlag(String finishFlag) {
        this.finishFlag = finishFlag == null ? null : finishFlag.trim();
    }

    /**
     * @return WORK_DESC
     */
    public String getWorkDesc() {
        return workDesc;
    }

    /**
     * @param workDesc
     */
    public void setWorkDesc(String workDesc) {
        this.workDesc = workDesc == null ? null : workDesc.trim();
    }

    /**
     * @return CRT_DATE
     */
    public String getCrtDate() {
        return crtDate;
    }

    /**
     * @param crtDate
     */
    public void setCrtDate(String crtDate) {
        this.crtDate = crtDate == null ? null : crtDate.trim();
    }

    /**
     * @return MTN_DATE
     */
    public String getMtnDate() {
        return mtnDate;
    }

    /**
     * @param mtnDate
     */
    public void setMtnDate(String mtnDate) {
        this.mtnDate = mtnDate == null ? null : mtnDate.trim();
    }
}