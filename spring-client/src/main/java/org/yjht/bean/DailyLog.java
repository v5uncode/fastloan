package org.yjht.bean;

import javax.persistence.*;

@Table(name = "DAILY_LOG")
public class DailyLog {
    @Id
    @Column(name = "USER_ID")
    private String userId;

    @Id
    @Column(name = "WORK_DATE")
    private String workDate;

    @Column(name = "USER_NAME")
    private String userName;

    @Column(name = "DAILY")
    private String daily;

    /**
     * @return USER_ID
     */
    public String getUserId() {
        return userId;
    }

    /**
     * @param userId
     */
    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    /**
     * @return WORK_DATE
     */
    public String getWorkDate() {
        return workDate;
    }

    /**
     * @param workDate
     */
    public void setWorkDate(String workDate) {
        this.workDate = workDate == null ? null : workDate.trim();
    }

    /**
     * @return USER_NAME
     */
    public String getUserName() {
        return userName;
    }

    /**
     * @param userName
     */
    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }

    /**
     * @return DAILY
     */
    public String getDaily() {
        return daily;
    }

    /**
     * @param daily
     */
    public void setDaily(String daily) {
        this.daily = daily == null ? null : daily.trim();
    }
}