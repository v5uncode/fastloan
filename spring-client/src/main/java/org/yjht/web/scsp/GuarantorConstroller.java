package org.yjht.web.scsp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.yjht.aop.SystemControllerLog;
import org.yjht.bean.scsp.FrontInfoGuarantor;
import org.yjht.core.Result;
import org.yjht.service.scsp.GuarantorService;
import org.yjht.util.ResultGenerator;

@RestController
@RequestMapping("/scsp")
public class GuarantorConstroller {
	@Autowired
	private GuarantorService guarantorService;
	
	@PostMapping(value = "/guarantor", produces = "application/json;charset=UTF-8")
    @SystemControllerLog(description = "添加保证人")
    public Result addGuarantor(@RequestBody FrontInfoGuarantor guarantor) {
        return guarantorService.addGuarantor(guarantor);
    }
	
	@GetMapping(value = "/guarantor/{scspId}", produces = "application/json;charset=UTF-8")
    @SystemControllerLog(description = "查询保证人")
    public Result getGuarantor(@PathVariable String scspId) {
        return ResultGenerator.genSuccessResult(guarantorService.findList(scspId));
    }
	@DeleteMapping(value = "/guarantor/{id}", produces = "application/json;charset=UTF-8")
    @SystemControllerLog(description = "删除保证人")
    public Result deleteGuarantor(@PathVariable String id) {
        return ResultGenerator.genSuccessResult(guarantorService.deleteGuarantor(id));
    }
	@PutMapping(value = "/guarantor/{id}", produces = "application/json;charset=UTF-8")
    @SystemControllerLog(description = "维护保证人")
    public Result updateGuarantor(@RequestBody FrontInfoGuarantor guarantor) {
        return guarantorService.updateGuarantor(guarantor);
    }
	
}
