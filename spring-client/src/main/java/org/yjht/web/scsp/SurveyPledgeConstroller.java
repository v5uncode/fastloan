package org.yjht.web.scsp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.yjht.aop.SystemControllerLog;
import org.yjht.bean.scsp.FrontInfoSurveyPledge;
import org.yjht.core.Result;
import org.yjht.service.scsp.SurveyPledgeService;
import org.yjht.util.ResultGenerator;

@RestController
@RequestMapping("/scsp")
public class SurveyPledgeConstroller {
	@Autowired
	private SurveyPledgeService surveyPledgeService;
	
	@GetMapping("/pledge/{scspId}")
	@SystemControllerLog(description = "查询贷款审批质押信息")
	public Result findListByScspId(@PathVariable String scspId){
		return ResultGenerator.genSuccessResult(surveyPledgeService.findByScspId(scspId));
	}
	@PutMapping("/pledge/{pledgeId}")
	@SystemControllerLog(description = "维护贷款审批质押信息")
	public Result updateById(@RequestBody FrontInfoSurveyPledge pledge){
		surveyPledgeService.update(pledge);
		return ResultGenerator.genSuccessResult();
	}
	

}
