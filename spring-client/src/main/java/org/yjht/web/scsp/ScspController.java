package org.yjht.web.scsp;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.yjht.aop.SystemControllerLog;
import org.yjht.bean.FrontInfoScsp;
import org.yjht.bean.scsp.FrontInfoCredit;
import org.yjht.core.Result;
import org.yjht.service.scsp.CreditService;
import org.yjht.service.scsp.ScspService;
import org.yjht.util.DicReplace;
import org.yjht.util.ResultGenerator;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/scsp")
public class ScspController {
    @Autowired
    private ScspService scspService;
    @Autowired
    private CreditService creditService;

    @GetMapping(value = "/credit/{scspId}", produces = "application/json;charset=UTF-8")
    @SystemControllerLog(description = "查询授信方案")
    public Result findCreditInfo(@PathVariable String scspId) {
        if(scspId != null && !"".equals(scspId)){
           FrontInfoCredit credit = creditService.findCreditInfoById(scspId);         
           if(credit != null){
        	   if(StringUtils.isNotBlank(credit.getLoanType())){
        		   credit.setLoanTypes(credit.getLoanType().split(","));
               }
        	   if(StringUtils.isNotBlank(credit.getHytx())){
            	   String[] hytxs=credit.getHytx().split(",");
            	   System.arraycopy(hytxs, 0, credit.getHytxs(), 0, hytxs.length); 
               }
        	   //字典名称转化
               Map<String, String> dicMap = new HashMap<String, String>();
               dicMap.put("idType", "ID_TYPE");
               DicReplace.replaceDic(credit, dicMap);
                return ResultGenerator.genSuccessResult(credit);
           }else{
               return ResultGenerator.genFailResult("查无数据");
           }
        }else{
            return ResultGenerator.genFailResult("参数异常");
        }
    }
    @GetMapping(value = "/credit1/{scspId}", produces = "application/json;charset=UTF-8")
    @SystemControllerLog(description = "查询授信方案")
    public Result findCreditInfo1(@PathVariable String scspId) {
        if(scspId != null && !"".equals(scspId)){
           FrontInfoCredit credit = creditService.findCreditInfoById(scspId);
           if(credit != null){
        	   //字典名称转化
               Map<String, String> dicMap = new HashMap<String, String>();
               dicMap.put("idType", "ID_TYPE");
               DicReplace.replaceDic(credit, dicMap);
               return ResultGenerator.genSuccessResult(credit);
           }else{
               return ResultGenerator.genFailResult("查无数据");
           }
        }else{
            return ResultGenerator.genFailResult("参数异常");
        }
    }

    @RequestMapping(value = "/saveCredit")
    @SystemControllerLog(description = "授信方案保存")
    public Result saveCredit(@RequestBody FrontInfoCredit credit) {
        String scspId = credit.getScspId();
        String hytx = credit.getHytx();
        if(hytx == null || "".equals(hytx)){
            credit.setHytx("U"); //行业投向默认为"U"
        }

        if(scspId != null && !"".equals(scspId)){
            int n = creditService.updateCreditById(credit);
            if(n > 0){
                return ResultGenerator.genSuccessResult("保存成功");
            }
            return ResultGenerator.genFailResult("保存失败");
        }

        return ResultGenerator.genFailResult("参数错误");
    }
    @PutMapping(value = "/{scspId}")
    @SystemControllerLog(description = "维护审查审批表及其关联表信息")
    public Result saveScsp(@RequestBody FrontInfoScsp scsp) {
    	scspService.saveScsp(scsp);        
        return ResultGenerator.genSuccessResult("保存成功");
    }
    
    @DeleteMapping(value = "/{scspId}")
    @SystemControllerLog(description = "删除审查审批流程信息")
    public Result deleteScsp(@PathVariable String scspId){
    	if(StringUtils.isBlank(scspId)){
    		return ResultGenerator.genFailResult("获取数据失败");
    	}
    	scspService.deleteScsp(scspId);
    	return ResultGenerator.genSuccessResult("删除成功");
    }



}
