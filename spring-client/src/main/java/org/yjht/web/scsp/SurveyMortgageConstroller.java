package org.yjht.web.scsp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.yjht.aop.SystemControllerLog;
import org.yjht.bean.scsp.FrontInfoSurveyMortgage;
import org.yjht.core.Result;
import org.yjht.service.scsp.SurveyMortgageService;
import org.yjht.util.ResultGenerator;

@RestController
@RequestMapping("/scsp")
public class SurveyMortgageConstroller {
	@Autowired
	private SurveyMortgageService surveyMortgageService;
	
	@GetMapping("/mortgage/{scspId}")
	@SystemControllerLog(description = "查询贷款审批抵押信息")
	public Result findListByScspId(@PathVariable String scspId){		
		return ResultGenerator.genSuccessResult(surveyMortgageService.findByScspId(scspId));
	}
	@PutMapping("/mortgage/{mortgageId}")
	@SystemControllerLog(description = "维护贷款审批抵押信息")
	public Result updateById(@RequestBody FrontInfoSurveyMortgage mortgage){
		surveyMortgageService.update(mortgage);
		return ResultGenerator.genSuccessResult();
	}

}
