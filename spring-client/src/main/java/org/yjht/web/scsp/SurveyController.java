package org.yjht.web.scsp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.yjht.aop.SystemControllerLog;
import org.yjht.bean.scsp.FrontInfoSurveyMortgage;
import org.yjht.bean.scsp.FrontInfoSurveyPledge;
import org.yjht.bean.scsp.vo.SurveyReportVo;
import org.yjht.bean.scsp.*;
import org.yjht.core.Result;
import org.yjht.service.cust.CustRelaService;
import org.yjht.service.scsp.SurveyMortgageService;
import org.yjht.service.scsp.SurveyPledgeService;
import org.yjht.service.scsp.SurveyService;
import org.yjht.util.DicReplace;
import org.yjht.util.ParamsReflectUtils;
import org.yjht.util.ResultGenerator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author sxf
 */
@RestController
@RequestMapping("/survey")
public class SurveyController {
    @Autowired
    private SurveyService surveyService;
    @Autowired
    private CustRelaService custRelaService;
    @Autowired
    private SurveyMortgageService mortgageService;
    @Autowired
    private SurveyPledgeService pledgeService;


    @GetMapping(value = "/{scspId}/report", produces = "application/json;charset=UTF-8")
    @SystemControllerLog(description = "查询调查报告记录")
    public Result findScspReport(@PathVariable String scspId) {
    	SurveyReportVo reportVo = surveyService.getReport(scspId);
        List<Map<String, Object>> list = ParamsReflectUtils.getFiledsInfo(reportVo);
        return ResultGenerator.genSuccessResult(list);
    }

    @PostMapping(value = "/saveSurvey", produces = "application/json;charset=UTF-8")
    @SystemControllerLog(description = "保存调查报告")
    public Result saveSurvey(@RequestBody FrontInfoSurvey survey) {
    	surveyService.updateSurvey(survey);
        return ResultGenerator.genSuccessResult("保存成功");
    }

    @GetMapping(value = "/getFamilies", produces = "application/json;charset=UTF-8")
    @SystemControllerLog(description = "根据借款人身份证查询家庭成员")
    public Result getFamilies(String idCard) {
        return custRelaService.getFamilies(idCard);
    }

    @GetMapping(value = "/getMortgage", produces = "application/json;charset=UTF-8")
    @SystemControllerLog(description = "查询抵质押物")
    public Result getMortgage(String scspId) {
        FrontInfoSurveyMortgage mortgage = new FrontInfoSurveyMortgage();
        mortgage.setScspId(scspId);
        List<FrontInfoSurveyMortgage> mortgages = mortgageService.find(mortgage);
        Map<String,String> dicMap = new HashMap<>();
        dicMap.put("dylx", "dylx_type");
        dicMap.put("dyzt", "dyzt_type");

        DicReplace.replaceDicList(mortgages, dicMap);

        FrontInfoSurveyPledge pledge = new FrontInfoSurveyPledge();
        pledge.setScspId(scspId);
        List<FrontInfoSurveyPledge> pledges = pledgeService.find(pledge);
        dicMap.clear();
        dicMap.put("zwzl", "zwzl_type");
        dicMap.put("zyzt", "zyzt_type");
        DicReplace.replaceDicList(pledges, dicMap);


        //为了前台页面列表方便展示，这里把质押同意换成抵押信息
        for (FrontInfoSurveyPledge p : pledges) {
            FrontInfoSurveyMortgage m = new FrontInfoSurveyMortgage();
            m.setDylx(p.getZwzl());
            m.setDywqsrxx(p.getZyrxm());
            m.setPgjz(p.getZwmz());
            m.setDbzzq(p.getDbzzq());
            m.setDyzt(p.getZyzt());
            mortgages.add(m);
        }


        return ResultGenerator.genSuccessResult(mortgages);
    }
    @GetMapping(value = "/approval/{scspId}", produces = "application/json;charset=UTF-8")
    @SystemControllerLog(description = "查询审查审批表")
    public Result getApproval(@PathVariable String scspId){
        if(scspId != null && !"".equals(scspId)){
            FrontInfoSurvey survey = surveyService.findById(scspId);
            if(survey != null){
            	Map<String,String> dicMap = new HashMap<String,String>();
                dicMap.put("idType", "ID_TYPE");
                DicReplace.replaceDic(survey, dicMap);
                dicMap.clear();
                dicMap.put("warrantType", "WARRANT");
                DicReplace.replaceDics(survey,dicMap);
                return ResultGenerator.genSuccessResult(survey);
            }
            return ResultGenerator.genFailResult("未查询到数据");

        }else{
            return ResultGenerator.genFailResult("参数异常");
        }
    }
}
