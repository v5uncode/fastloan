package org.yjht.web.callrule;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.yjht.aop.SystemControllerLog;
import org.yjht.bean.FlInfoApprovalVo;
import org.yjht.bean.FrontInfoScsp;
import org.yjht.calculate.service.FlInfoApprovalService;
import org.yjht.core.Result;
import org.yjht.exception.MyException;
import org.yjht.service.scsp.ScspService;
import org.yjht.service.scsp.SurveyService;
import org.yjht.util.ResultGenerator;


@RestController
@RequestMapping("/edjs")
public class CallRuleController {
	@Autowired
	private ScspService scspService;	
	@Autowired
	private SurveyService surveyService;
	@Autowired
	private FlInfoApprovalService flInfoApprovalService;
	
    @PostMapping(value="/callrule_unit",produces = "application/json;charset=UTF-8")
    @SystemControllerLog(description = "评级测算-单笔")
    public Result callrule_unit(@RequestBody Map<String,Object> map) throws Exception { 
    	List<FrontInfoScsp> list = scspService.findScspByCustId(map.get("custId").toString());
    	if(!list.isEmpty()) {
    		throw new MyException("该客户的审查审批流程未结束，请检查。");
    	}
    	if(map.get("custId")==null||map.get("custId").toString().equals("")){
    		throw new MyException("参数错误");
    	}
    	BigDecimal applyAmount = BigDecimal.valueOf(Double.parseDouble(map.get("applyAmount").toString()));//申请金额
    	Integer applyTerm=Integer.parseInt(map.get("applyTerm").toString());	
    	FlInfoApprovalVo vo=null;
    	try{
    		vo = flInfoApprovalService.calculate(map.get("custId").toString(), applyAmount, applyTerm);
    	}catch(MyException e){
    		throw new MyException(e.getMessage()); 
    	}catch(Exception e){
    		e.printStackTrace();
    		throw new MyException("评级测算接口调用失败"); 
    	}	
    	return ResultGenerator.genSuccessResult(vo);  
    } 
    
    @PostMapping(value="/commitScsp",produces = "application/json;charset=UTF-8")
	@SystemControllerLog(description = "首次提交审查审批和报告信息(发起授信)")
	public Result saveScspReport(@RequestBody FlInfoApprovalVo approvalVo) {
    	surveyService.insertSurvey(approvalVo);
		return ResultGenerator.genSuccessResult();
	}
}
