package org.yjht.web.cust;

import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.yjht.aop.SystemControllerLog;
import org.yjht.bean.cust.FrontInfoCustYj;
import org.yjht.core.Page;
import org.yjht.core.Result;
import org.yjht.service.cust.CustYjService;
import org.yjht.service.cust.TreeService;
@RestController
@EnableAutoConfiguration 
public class CustYjController {
	@Autowired
	CustYjService custYjService;
	@Autowired
	TreeService treeService;
	
	@RequestMapping(value = "/turnover/ycl",produces = "application/json;charset=UTF-8",method = RequestMethod.GET)
	@SystemControllerLog(description = "客户管理-查询已处理移交信息")
	public Result findYjYcl(Page page,HttpSession session) {		
		return custYjService.findYjYcl(page, session);
	}
	@RequestMapping(value = "/turnover/dcl",produces = "application/json;charset=UTF-8",method = RequestMethod.GET)
	@SystemControllerLog(description = "客户管理-查询待处理移交信息")
	public Result findYjDcl(Page page,HttpSession session) {	
		return custYjService.findYjDcl(page, session);
	}
	
	@RequestMapping(value = "/turnover/single",produces = "application/json;charset=UTF-8",method = RequestMethod.POST)
	@SystemControllerLog(description = "客户管理-申请移交")
	public Result saveYjInfo(@RequestBody FrontInfoCustYj custYj,HttpSession session) {	
		return custYjService.saveYjInfo(custYj, session);
	}
	@RequestMapping(value = "/turnover",produces = "application/json;charset=UTF-8",method = RequestMethod.PUT)
	@SystemControllerLog(description = "客户管理-修改移交状态")
	public Result updateYjInfo(@RequestBody FrontInfoCustYj custYj) {	
		return custYjService.updateYjInfo(custYj);
	}
	@RequestMapping(value = "/{userId}/turnover",produces = "application/json;charset=UTF-8",method = RequestMethod.POST)
	@SystemControllerLog(description = "客户管理-批量移交")
	public Result custYj(@RequestBody Map<String,Object> map, HttpSession session) {
		return custYjService.custYj(map, session);
	}
	@RequestMapping(value = "/cust/turnover/finduser/{userName}",produces = "application/json;charset=UTF-8",method = RequestMethod.GET)
	@SystemControllerLog(description = "客户管理-查询批量移交接收的客户经理")
	public Result saveYjInfo(@PathVariable String userName, HttpSession session) {
		return treeService.findUserAndOrg(userName,session);
	}
}
