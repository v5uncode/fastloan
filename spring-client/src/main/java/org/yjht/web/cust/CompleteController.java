package org.yjht.web.cust;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.yjht.aop.SystemControllerLog;
import org.yjht.core.Result;
import org.yjht.service.cust.CompleteCustInfoService;

@RestController
@RequestMapping("/cust")  
@EnableAutoConfiguration 
public class CompleteController {
	@Autowired
	CompleteCustInfoService completeCustInfoService;
	@RequestMapping(value = "/complete/{custId}",produces = "application/json;charset=UTF-8",method = RequestMethod.GET)
	@SystemControllerLog(description = "查询客户信息完整性")
	public Result getCustInfoComplete(@PathVariable String custId){ 		
        return completeCustInfoService.getCustInfoComplete(custId);
    }
}
