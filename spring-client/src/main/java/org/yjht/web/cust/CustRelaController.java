package org.yjht.web.cust;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.yjht.aop.SystemControllerLog;
import org.yjht.bean.cust.FrontInfoCustRela;
import org.yjht.core.Result;
import org.yjht.service.cust.CustRelaService;
import org.yjht.util.ResultGenerator;

@RestController
@RequestMapping("/cust")  
@EnableAutoConfiguration 
public class CustRelaController {
	@Autowired
	CustRelaService custRelaService;
	@RequestMapping(value = "/rela",produces = "application/json;charset=UTF-8",method = RequestMethod.GET)
	@SystemControllerLog(description = "查询客户家庭成员信息")
    public Result findRela(String custId){  		
        return ResultGenerator.genSuccessResult(custRelaService.findRela(custId));
    }
	@RequestMapping(value = "/rela/{id}",produces = "application/json;charset=UTF-8",method = RequestMethod.GET)
	@SystemControllerLog(description = "根据ID查询客户家庭成员信息")
    public Result findRelaById(@PathVariable String id){  		
        return custRelaService.findRelaById(id);
    }
	@RequestMapping(value = "/rela",produces = "application/json;charset=UTF-8",method = RequestMethod.PUT)
	@SystemControllerLog(description = "修改客户家庭成员信息")
    public Result updateRela(@RequestBody FrontInfoCustRela rela){ 
        return custRelaService.updateRela(rela);
    }
	@RequestMapping(value = "/rela",produces = "application/json;charset=UTF-8",method = RequestMethod.POST)
	@SystemControllerLog(description = "保存客户家庭成员信息")
    public Result saveRela(@RequestBody FrontInfoCustRela rela){ 
        return custRelaService.saveRela(rela);
    }
	@RequestMapping(value = "/rela/{custId}/{id}",produces = "application/json;charset=UTF-8",method = RequestMethod.DELETE)
	@SystemControllerLog(description = "删除客户家庭成员信息")
    public Result deleteRela(@PathVariable String custId,@PathVariable String id){ 
        return custRelaService.deleteRela(custId,id);
    }
		
	
}