package org.yjht.web.cust;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.yjht.aop.SystemControllerLog;
import org.yjht.bean.cust.FrontInfoCustDoc;
import org.yjht.core.Result;
import org.yjht.service.cust.CustDocService;
@RestController
@RequestMapping("/cust")  
@EnableAutoConfiguration 
public class CustDocController {
	@Autowired
	CustDocService custDocService;
	@RequestMapping(value = "/doc/{custId}/{docName}",produces = "application/json;charset=UTF-8",method = RequestMethod.GET)
	@SystemControllerLog(description = "查询客户档案信息")
	public Result findDoc(@PathVariable String custId,@PathVariable String docName) {
		return custDocService.findDoc(custId,docName);
	}
	@RequestMapping(value = "/doc",produces = "application/json;charset=UTF-8",method = RequestMethod.POST)
	@SystemControllerLog(description = "保存客户档案信息")
	public Result saveDoc(@RequestBody List<FrontInfoCustDoc> docs) {
		return custDocService.saveDoc(docs);
	}
	@RequestMapping(value = "/doc",produces = "application/json;charset=UTF-8",method = RequestMethod.PUT)
	@SystemControllerLog(description = "修改客户档案信息")
	public Result updateDoc(@RequestBody FrontInfoCustDoc doc) {
		return custDocService.updateDoc(doc);
	}
	@RequestMapping(value = "/doc/{seqNo}",produces = "application/json;charset=UTF-8",method = RequestMethod.DELETE)
	@SystemControllerLog(description = "删除客户档案信息")
	public Result deleteDoc(@PathVariable String seqNo) {
		return custDocService.deleteDoc(seqNo);
	}

}
