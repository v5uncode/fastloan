package org.yjht.web.cust;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.yjht.aop.SystemControllerLog;
import org.yjht.bean.cust.CustCreditReport;
import org.yjht.core.Result;
import org.yjht.service.cust.CustCreditReportService;

@RestController
@RequestMapping("/cust")  
@EnableAutoConfiguration 
public class CustCreditReportController {
	@Autowired
	CustCreditReportService custCreditReportService;
	
	@RequestMapping(value = "/report/{custId}",produces = "application/json;charset=UTF-8",method = RequestMethod.GET)
	@SystemControllerLog(description = "查询客户征信信息")
	public Result findCustCreditReport(@PathVariable String custId) {
		return custCreditReportService.findCustCreditReport(custId);
	}
	@RequestMapping(value = "/report",produces = "application/json;charset=UTF-8",method = RequestMethod.POST)
	@SystemControllerLog(description = "维护客户征信信息")
	public Result saveCustCreditReport(@RequestBody CustCreditReport custCreditReport) {
		return custCreditReportService.saveCustCreditReport(custCreditReport);
	}

}
