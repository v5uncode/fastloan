package org.yjht.web.cust;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.yjht.aop.SystemControllerLog;
import org.yjht.bean.cust.FrontInfoCustPlant;
import org.yjht.core.Result;
import org.yjht.service.cust.FrontInfoCustPlantService;

@RestController
@RequestMapping("/cust")  
@EnableAutoConfiguration 
public class FrontInfoCustPlantController {
	@Autowired
	FrontInfoCustPlantService custPlantService;
	@RequestMapping(value = "/plants",produces = "application/json;charset=UTF-8",method = RequestMethod.GET)
	@SystemControllerLog(description = "查询客户种植信息")
    public Result findPlant(String custId){  		
        return custPlantService.findPlant(custId);
    }
	@RequestMapping(value = "/plants/{id}",produces = "application/json;charset=UTF-8",method = RequestMethod.GET)
	@SystemControllerLog(description = "根据ID查询客户种植信息")
    public Result findPlantById(@PathVariable String id){  		
        return custPlantService.findPlantById(id);
    }
	@RequestMapping(value = "/plants",produces = "application/json;charset=UTF-8",method = RequestMethod.PUT)
	@SystemControllerLog(description = "修改客户种植信息")
    public Result updatePlant(@RequestBody FrontInfoCustPlant plant){ 
        return custPlantService.updatePlant(plant);
    }
	@RequestMapping(value = "/plants",produces = "application/json;charset=UTF-8",method = RequestMethod.POST)
	@SystemControllerLog(description = "保存客户种植信息")
    public Result savePlant(@RequestBody FrontInfoCustPlant plant){ 
        return custPlantService.savePlant(plant);
    }
	@RequestMapping(value = "/plants/{custId}/{id}",produces = "application/json;charset=UTF-8",method = RequestMethod.DELETE)
	@SystemControllerLog(description = "删除客户种植信息")
    public Result deletePlant(@PathVariable String custId,@PathVariable String id){ 
        return custPlantService.deletePlant(custId,id);
    }

}
