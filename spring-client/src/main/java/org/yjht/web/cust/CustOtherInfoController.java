package org.yjht.web.cust;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.yjht.aop.SystemControllerLog;
import org.yjht.bean.cust.CustOtherInfo;
import org.yjht.core.Result;
import org.yjht.service.cust.CustOtherInfoService;

@RestController
@RequestMapping("/cust")  
@EnableAutoConfiguration 
public class CustOtherInfoController {
	@Autowired
	CustOtherInfoService custOtherInfoService;
	@RequestMapping(value = "/otherinfo/{custId}",produces = "application/json;charset=UTF-8",method = RequestMethod.GET)
	@SystemControllerLog(description = "查询客户负面信息")
	public Result findOtherInf(@PathVariable String custId) {
		return custOtherInfoService.findOtherInf(custId);
	}
	@RequestMapping(value = "/otherinfo",produces = "application/json;charset=UTF-8",method = RequestMethod.POST)
	@SystemControllerLog(description = "维护客户负面信息")
	public Result saveOtherInfo(@RequestBody CustOtherInfo custOtherInfo) {
		return custOtherInfoService.saveOtherInfo(custOtherInfo);
	}
	

}
