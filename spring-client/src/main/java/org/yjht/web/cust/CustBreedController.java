package org.yjht.web.cust;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.yjht.aop.SystemControllerLog;
import org.yjht.bean.cust.FrontInfoCustBreed;
import org.yjht.core.Result;
import org.yjht.service.cust.CustBreedService;

@RestController
@RequestMapping("/cust")  
@EnableAutoConfiguration 
public class CustBreedController {
	@Autowired
	CustBreedService custBreedService;
	@RequestMapping(value = "/breed",produces = "application/json;charset=UTF-8",method = RequestMethod.GET)
	@SystemControllerLog(description = "查询客户养殖信息")
    public Result findBreed(String custId){  
        return custBreedService.findBreed(custId);
    }
	@RequestMapping(value = "/breed/{id}",produces = "application/json;charset=UTF-8",method = RequestMethod.GET)
	@SystemControllerLog(description = "根据ID查询客户养殖信息")
    public Result findBreedById(@PathVariable String id){ 
        return custBreedService.findBreedById(id);
    }
	@RequestMapping(value = "/breed",produces = "application/json;charset=UTF-8",method = RequestMethod.PUT)
	@SystemControllerLog(description = "维护客户养殖信息")
    public Result updateBreed(@RequestBody FrontInfoCustBreed breed){ 
        return custBreedService.updateBreed(breed);
    }
	@RequestMapping(value = "/breed",produces = "application/json;charset=UTF-8",method = RequestMethod.POST)
	@SystemControllerLog(description = "保存客户养殖信息")
    public Result saveBreed(@RequestBody FrontInfoCustBreed breed){ 
        return custBreedService.saveBreed(breed);
    }
	@RequestMapping(value = "/breed/{custId}/{id}",produces = "application/json;charset=UTF-8",method = RequestMethod.DELETE)
	@SystemControllerLog(description = "删除客户养殖信息")
    public Result deleteBreed(@PathVariable String custId,@PathVariable String id){ 
        return custBreedService.deleteBreed(custId,id);
    }

}
