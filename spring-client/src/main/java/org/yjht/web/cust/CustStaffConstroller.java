package org.yjht.web.cust;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.yjht.aop.SystemControllerLog;
import org.yjht.bean.cust.FrontInfoCustStaff;
import org.yjht.core.Result;
import org.yjht.service.cust.CustStaffService;
import org.yjht.util.ResultGenerator;

@RestController
@RequestMapping("/cust")  
@EnableAutoConfiguration 
public class CustStaffConstroller {
	@Autowired
	CustStaffService custStaffService;
	@RequestMapping(value = "/staff/{custId}",produces = "application/json;charset=UTF-8",method = RequestMethod.GET)
	@SystemControllerLog(description = "客户管理-查询职工信息")
    public Result findStaff(@PathVariable String custId){  		
        return ResultGenerator.genSuccessResult(custStaffService.findStaff(custId));
    }
	@RequestMapping(value = "/staff",produces = "application/json;charset=UTF-8",method = RequestMethod.PUT)
    public Result updateCust(@RequestBody FrontInfoCustStaff satff){ 
        return custStaffService.updateStaff(satff);
    }
	@RequestMapping(value = "/staff",method = RequestMethod.POST)
	@SystemControllerLog(description = "客户管理-维护职工信息")
    public Result saveStaff(@RequestBody FrontInfoCustStaff staff){ 
        return custStaffService.saveStaff(staff);
    }

	

}
