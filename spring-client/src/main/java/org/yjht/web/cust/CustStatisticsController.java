package org.yjht.web.cust;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.yjht.aop.SystemControllerLog;
import org.yjht.core.Result;
import org.yjht.service.cust.CustStatisticsService;
import org.yjht.util.ResultGenerator;

@RestController
public class CustStatisticsController {
	
	@Autowired
	CustStatisticsService custStatisticsService;
	
	@RequestMapping(value = "/custStatistics/custStatistics", produces = "application/json;charset=UTF-8", method = { RequestMethod.GET })
	@SystemControllerLog(description = "客户量统计")
	public Result queryCustSum(HttpServletRequest request) {
		
		return custStatisticsService.queryCustSum(request);
	}
	
	@RequestMapping(value = "/CLIENT/custStatistics/cust", produces = "application/json;charset=UTF-8", method = { RequestMethod.GET })
	@SystemControllerLog(description = "支行客户量统计")
	public Result queryOrgCustSum(HttpServletRequest request,@RequestParam Map<String,Object> map) {
		
		return custStatisticsService.queryOrgCustSum(request,map);
	}
	
	
	@RequestMapping(value = "/custStatistics/cust/statistic", produces = "application/json;charset=UTF-8", method = { RequestMethod.GET })
	@SystemControllerLog(description = "客户经理客户量统计")
	public Result queryJlCustSum(HttpServletRequest request,@RequestParam Map<String,Object> map) {
		
		return custStatisticsService.queryJlCustSum(request,map);
	}
	
	@RequestMapping(value = "/CLIENT/custStatistics/cust/exportJlToExcel",produces = "application/json;charset=UTF-8", method = {RequestMethod.GET })
	@SystemControllerLog(description = "导出客户经理客户量统计")
	public Result exportJlToExcel(HttpServletRequest request,@RequestParam Map<String,Object> map,HttpServletResponse response){
		/////////////////////////////// 查数据库中的数据///////////////////////////////////////
		Result result = custStatisticsService.queryJlCustSum(request,map);
		List<Map<String,Object>> list =(List<Map<String,Object>>)result.getData();
		// 声明一个工作薄
		HSSFWorkbook wb = new HSSFWorkbook();
		//声明一个单子并命名
		HSSFSheet sheet = wb.createSheet("客户采集信息明细表");
		//给单子名称一个长度
		sheet.setDefaultColumnWidth(10);
		// 生成一个样式  
		HSSFCellStyle style = wb.createCellStyle();
		//创建第一行（也可以称为表头）
		HSSFRow row = sheet.createRow(0);
		
		//给表头第一行一次创建单元格
		HSSFCell cell = row.createCell(0);
		cell.setCellValue("机构名称");  
		cell.setCellStyle(style);  
		cell = row.createCell(1); 
		cell.setCellValue("客户经理");  
		cell.setCellStyle(style); 
		cell = row.createCell(2); 
		cell.setCellValue("总户数");  
		cell.setCellStyle(style); 
		cell = row.createCell(3);  
		cell.setCellValue("信息完整");  
		cell.setCellStyle(style);  
		cell = row.createCell(4);  
		cell.setCellValue("信息不完整");  
		cell.setCellStyle(style);  
		cell = row.createCell(5);  
		cell.setCellValue("农工");  
		cell.setCellStyle(style); 
		cell = row.createCell(6);
		cell.setCellValue("一般农户");  
		cell.setCellStyle(style);  
//		cell = row.createCell(7);
//		cell.setCellValue("农村个体工商户");
//		cell.setCellStyle(style);
//		cell = row.createCell(8);
//		cell.setCellValue("城镇自然人");
//		cell.setCellStyle(style);
//		cell = row.createCell(9);
//		cell.setCellValue("个体工商户");
//		cell.setCellStyle(style);
//		cell = row.createCell(10);
//		cell.setCellValue("兵团自然人");
//		cell.setCellStyle(style);
//		cell = row.createCell(11);
//		cell.setCellValue("兵团个体工商户");
//		cell.setCellStyle(style);
		//向单元格里填充数据
		for (short i = 0; i < list.size(); i++) {
			Map<String, Object> indexMap = list.get(i);
			row = sheet.createRow(i + 1);
			row.createCell(0).setCellValue(indexMap.get("orgName").toString());
			row.createCell(1).setCellValue(indexMap.get("userName").toString());
			row.createCell(2).setCellValue(Integer.parseInt(indexMap.get("custCount").toString()));
			row.createCell(3).setCellValue(Integer.parseInt(indexMap.get("create").toString()));
			row.createCell(4).setCellValue(Integer.parseInt(indexMap.get("notCreate").toString()));
			row.createCell(5).setCellValue(Integer.parseInt(indexMap.get("ng").toString()));
			row.createCell(6).setCellValue(Integer.parseInt(indexMap.get("ybnh").toString()));
//			row.createCell(7).setCellValue(Integer.parseInt(indexMap.get("ncgtgsh").toString()));
//			row.createCell(8).setCellValue(Integer.parseInt(indexMap.get("czzrr").toString()));
//			row.createCell(9).setCellValue(Integer.parseInt(indexMap.get("gtgsh").toString()));
//			row.createCell(10).setCellValue(Integer.parseInt(indexMap.get("btzrr").toString()));
//			row.createCell(11).setCellValue(Integer.parseInt(indexMap.get("btgtgsh").toString()));
		}
		
		try {
			String fileName = "JL"+System.currentTimeMillis()+"";//设置导出的文件名称
			String contentType = "application/x-msdownload";//定义导出文件的格式的字符串
			String recommendedName = new String(fileName.getBytes(),"utf-8");//设置文件名称的编码格式
			response.setContentType(contentType);//设置导出文件格式
			response.setHeader("Content-Disposition", "attachment; filename=" + recommendedName + ".xls");
			response.resetBuffer();
			//利用输出输入流导出文件
			OutputStream os = response.getOutputStream();
			wb.write(os);
			os.flush();
			os.close();
		} catch (Exception e) {
			throw new RuntimeException();
		}
		return ResultGenerator.genSuccessResult();
	}
	
	@RequestMapping(value = "/CLIENT/custStatistics/cust/exportOrgToExcel",produces = "application/json;charset=UTF-8", method = {RequestMethod.GET })
	@SystemControllerLog(description = "导出机构客户量统计")
	public Result exportOrgToExcel(HttpServletRequest request,@RequestParam Map<String,Object> map,HttpServletResponse response){
		/////////////////////////////// 查数据库中的数据///////////////////////////////////////
		Result result = custStatisticsService.queryJlCustSum(request,map);
		List<Map<String,Object>> list =(List<Map<String,Object>>)result.getData();
		// 声明一个工作薄
		HSSFWorkbook wb = new HSSFWorkbook();
		//声明一个单子并命名
		HSSFSheet sheet = wb.createSheet("客户采集信息明细表");
		//给单子名称一个长度
		sheet.setDefaultColumnWidth(10);
		// 生成一个样式  
		HSSFCellStyle style = wb.createCellStyle();
		//创建第一行（也可以称为表头）
		HSSFRow row = sheet.createRow(0);
		
		//给表头第一行一次创建单元格
		HSSFCell cell = row.createCell(0);
		cell.setCellValue("机构名称");  
		cell.setCellStyle(style);  
		cell = row.createCell(1); 
		cell.setCellValue("总户数");  
		cell.setCellStyle(style); 
		cell = row.createCell(2);  
		cell.setCellValue("信息完整");  
		cell.setCellStyle(style);  
		cell = row.createCell(3);  
		cell.setCellValue("信息不完整");  
		cell.setCellStyle(style);  
		cell = row.createCell(4);  
		cell.setCellValue("农工");  
		cell.setCellStyle(style); 
		cell = row.createCell(5);  
		cell.setCellValue("一般农户");  
		cell.setCellStyle(style);  
//		cell = row.createCell(6);
//		cell.setCellValue("农村个体工商户");
//		cell.setCellStyle(style);
//		cell = row.createCell(7);
//		cell.setCellValue("城镇自然人");
//		cell.setCellStyle(style);
//		cell = row.createCell(8);
//		cell.setCellValue("个体工商户");
//		cell.setCellStyle(style);
//		cell = row.createCell(9);
//		cell.setCellValue("兵团自然人");
//		cell.setCellStyle(style);
//		cell = row.createCell(10);
//		cell.setCellValue("兵团个体工商户");
//		cell.setCellStyle(style);
		//向单元格里填充数据
		for (short i = 0; i < list.size(); i++) {
			Map<String, Object> indexMap = list.get(i);
			row = sheet.createRow(i + 1);
			row.createCell(0).setCellValue(indexMap.get("orgName").toString());
			row.createCell(1).setCellValue(Integer.parseInt(indexMap.get("custCount").toString()));
			row.createCell(2).setCellValue(Integer.parseInt(indexMap.get("create").toString()));
			row.createCell(3).setCellValue(Integer.parseInt(indexMap.get("notCreate").toString()));
			row.createCell(4).setCellValue(Integer.parseInt(indexMap.get("ng").toString()));
			row.createCell(5).setCellValue(Integer.parseInt(indexMap.get("ybnh").toString()));
//			row.createCell(6).setCellValue(Integer.parseInt(indexMap.get("ncgtgsh").toString()));
//			row.createCell(7).setCellValue(Integer.parseInt(indexMap.get("czzrr").toString()));
//			row.createCell(8).setCellValue(Integer.parseInt(indexMap.get("gtgsh").toString()));
//			row.createCell(9).setCellValue(Integer.parseInt(indexMap.get("btzrr").toString()));
//			row.createCell(10).setCellValue(Integer.parseInt(indexMap.get("btgtgsh").toString()));
		}
		
		try {
			String fileName = "JG"+System.currentTimeMillis()+"";//设置导出的文件名称
			String contentType = "application/x-msdownload";//定义导出文件的格式的字符串
			String recommendedName = new String(fileName.getBytes(),"utf-8");//设置文件名称的编码格式
			response.setContentType(contentType);//设置导出文件格式
			response.setHeader("Content-Disposition", "attachment; filename=" + recommendedName + ".xls");
			response.resetBuffer();
			//利用输出输入流导出文件
			OutputStream os = response.getOutputStream();
			wb.write(os);
			os.flush();
			os.close();
		} catch (Exception e) {
			throw new RuntimeException();
		}
		return ResultGenerator.genSuccessResult();
	}
	
	
}
