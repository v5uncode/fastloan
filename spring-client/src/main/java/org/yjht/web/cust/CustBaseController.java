package org.yjht.web.cust;



import java.util.List;

import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.yjht.aop.SystemControllerLog;
import org.yjht.bean.cust.CustBase;
import org.yjht.core.Page;
import org.yjht.core.Result;
import org.yjht.service.cust.CustBaseService;
import org.yjht.util.ResultGenerator;


/**
 * 给个差评，既然rest通信，为啥不遵守rest风格
 * @author sxf
 *
 */
@RestController
@RequestMapping("/cust")  
@EnableAutoConfiguration 
public class CustBaseController {
	@Autowired
	private CustBaseService custBaseService;

	@RequestMapping(value = "/cust",produces = "application/json;charset=UTF-8",method = RequestMethod.GET)
	@SystemControllerLog(description = "查询客户信息")
    public Result selectList(CustBase cust,Page page){ 		
        return custBaseService.selectList(cust,page);
    }
	@RequestMapping(value = "/cust",method = RequestMethod.PUT )
	@SystemControllerLog(description = "维护客户信息")
    public Result updateCust(@RequestBody CustBase cust,HttpSession session){ 		
			return custBaseService.updateCust(cust,session);		
    }
	@RequestMapping(value = "/cust",produces = "application/json;charset=UTF-8",method = RequestMethod.POST)
	@SystemControllerLog(description = "添加客户信息")
    public Result saveCust(@RequestBody CustBase cust,HttpSession session){ 
		return custBaseService.addCust(cust,session);
		
    }
	@RequestMapping(value = "/cust",produces = "application/json;charset=UTF-8",method = RequestMethod.DELETE)
	@SystemControllerLog(description = "删除客户信息")
	public Result deleteCust(@RequestBody List<String> custIds){ 		
		return custBaseService.deleteCust(custIds);		
    }
	@RequestMapping(value = "/cust/{key}",produces = "application/json;charset=UTF-8",method = RequestMethod.GET)
	@SystemControllerLog(description = "模糊查询客户信息")
	public Result selectLikeKey(Page page,@PathVariable String key,HttpSession session) {
		return custBaseService.selectLikeKey(page, key, session);
		
	}
	@RequestMapping(value = "/cust/by/{idNo}",produces = "application/json;charset=UTF-8",method = RequestMethod.GET)
	@SystemControllerLog(description = "根据身份证号查询客户信息")
	public Result selectByIdNo(@PathVariable String idNo,HttpSession session) {
		return ResultGenerator.genSuccessResult(custBaseService.selectByIdNo(idNo, session));
		
	}
	

}
