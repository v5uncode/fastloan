package org.yjht.web.cust;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.yjht.aop.SystemControllerLog;
import org.yjht.bean.cust.FrontInfoFinRpt;
import org.yjht.core.Result;
import org.yjht.service.cust.FinRptService;
import org.yjht.util.ResultGenerator;

@RestController
@RequestMapping("/cust")  
@EnableAutoConfiguration 
public class FinRptController {
	@Autowired
	FinRptService finRptService;
	@RequestMapping(value = "/finrpt/{custId}",produces = "application/json;charset=UTF-8",method = RequestMethod.GET)
	@SystemControllerLog(description = "查询客户财务信息")
	public Result findFinRpt(@PathVariable String custId) {
		if(StringUtils.isBlank(custId)){
			return ResultGenerator.genFailResult("参数错误");
		}
		return ResultGenerator.genSuccessResult(finRptService.findFinRpt(custId));
	}
	@RequestMapping(value = "/finrpt",produces = "application/json;charset=UTF-8",method = RequestMethod.POST)
	@SystemControllerLog(description = "维护客户财务信息")
	public Result saveFinRpt(@RequestBody FrontInfoFinRpt finRpt) {
		return finRptService.saveFinRpt(finRpt);
	}
	@RequestMapping(value = "/finrpt",produces = "application/json;charset=UTF-8",method = RequestMethod.DELETE)
	public Result deleteFinRpt(FrontInfoFinRpt finRpt) {
		return finRptService.deleteFinRpt(finRpt);
	}

}
