package org.yjht.web.cust;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Base64Utils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.yjht.bean.Base64File;
import org.yjht.bean.cust.FrontInfoCustDoc;
import org.yjht.core.Result;
import org.yjht.exception.MyException;
import org.yjht.service.cust.CustDocService;
import org.yjht.util.DateTools;
import org.yjht.util.ResourcesUtil;
import org.yjht.util.ResultGenerator;


@CrossOrigin(origins = "*", maxAge = 36000)
@RestController
@RequestMapping("/file")
public class UploadController {
	private final static Logger log = LoggerFactory.getLogger(UploadController.class);
	@Autowired
	private CustDocService custDocService;
	@RequestMapping(value="/upload", method = RequestMethod.POST)
	public Result upload2(FrontInfoCustDoc doc,HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		System.out.println(doc);
		// 创建一个通用的多部分解析器
		CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver(
				request.getSession().getServletContext());
		String path = "";
		String fileName = "";
		Result result=null;
		List<FrontInfoCustDoc> list=new ArrayList<FrontInfoCustDoc>();
		
		// 判断 request 是否有文件上传,即多部分请求
		if (multipartResolver.isMultipart(request)) {
			// 转换成多部分request
			MultipartHttpServletRequest multiRequest = (MultipartHttpServletRequest) request;
			// 取得request中的所有文件名
			Iterator<String> iter = multiRequest.getFileNames();
			while (iter.hasNext()) {
				// 记录上传过程起始时的时间，用来计算上传时间
				int pre = (int) System.currentTimeMillis();
				// 取得上传文件
				MultipartFile file = multiRequest.getFile(iter.next());
				if (file != null) {
					// 取得当前上传文件的文件名称
					String myFileName = file.getOriginalFilename();
					//后缀
					String prefix = myFileName.substring(myFileName.lastIndexOf(".")+1);
					// 如果名称不为“”,说明该文件存在，否则说明该文件不存在
					if (myFileName.trim() != "") {
						// 重命名上传后的文件名
						fileName = DateTools.getCurrentSysData("yyyyMMddHHmmsss")
								+ (int) ((Math.random() * 9 + 1) * 10000)+"."+prefix;
						// 定义上传路径
						path =ResourcesUtil.getValue("uploanUrl", "uploadUrl")+File.separator+fileName;
						File localFile = new File(path);
						if (!localFile.getParentFile().exists()) {  
			                // 目标文件所在目录不存在  
			                if (!localFile.getParentFile().mkdirs()) {  
			                    // 复制文件失败：创建目标文件所在目录失败  
			        			result = ResultGenerator.genFailResult("创建目标文件所在目录失败");
			                }  
			            }  
						file.transferTo(localFile);
						doc.setFileName(fileName);
						doc.setDocPath(fileName);
						doc=custDocService.saveDoc(doc);
						list.add(doc);
					}
				}else {
					throw new MyException("上传失败文件不存在");
				}
				// 记录上传该文件后的时间
				int finaltime = (int) System.currentTimeMillis();
				System.out.println(finaltime - pre);
				
			}

		}
		result = ResultGenerator.genSuccessResult(list);

		return result;
	}
	@RequestMapping(value="/upload/base64", produces = "application/json", method = { RequestMethod.POST })
	public Result upload3(@RequestBody List<Base64File> files){
		Result result=null;
		List<String> list=new ArrayList<String>();
		try{  
			
			for(Base64File base64File:files) {
				String file=base64File.getData();
	            String dataPrix = "";
	            String data = "";
	            if(file == null || "".equals(file)){
		            return ResultGenerator.genFailResult("上传失败，上传图片数据为空");
	            }else{
	                String [] d = file.split("base64,");
	                if(d != null && d.length == 2){
	                    dataPrix = d[0];
	                    data = d[1];
	                }else{
			            return ResultGenerator.genFailResult("上传失败，数据不合法");
	                }
	            }

	            //对数据进行解析，获取文件名和流数据
	            String suffix = "";
	            if("data:image/jpeg;".equalsIgnoreCase(dataPrix)){//data:image/jpeg;base64,base64编码的jpeg图片数据
	                suffix = ".jpg";
	            } else if("data:image/x-icon;".equalsIgnoreCase(dataPrix)){//data:image/x-icon;base64,base64编码的icon图片数据
	                suffix = ".ico";
	            } else if("data:image/gif;".equalsIgnoreCase(dataPrix)){//data:image/gif;base64,base64编码的gif图片数据
	                suffix = ".gif";
	            } else if("data:image/png;".equalsIgnoreCase(dataPrix)){//data:image/png;base64,base64编码的png图片数据
	                suffix = ".png";
	            }else{
	            	return ResultGenerator.genFailResult("上传失败，数据不合法");
	                
	            }
	            String tempFileName = DateTools
						.getCurrentSysData("yyyyMMddHHmmsss")
						+ (int) ((Math.random() * 9 + 1) * 10000) + suffix;

	            //因为BASE64Decoder的jar问题，此处使用spring框架提供的工具包
	            byte[] bs = Base64Utils.decodeFromString(data);
	            try{
	            	String path = ResourcesUtil.getValue("uploanUrl", "uploadUrl");
	            	System.out.println(path);
	                //使用apache提供的工具类操作流
	                FileUtils.writeByteArrayToFile(new File(path, tempFileName), bs); 
	                list.add(tempFileName);
	            }catch(Exception ee){
	                throw new Exception("上传失败，写入文件失败，"+ee.getMessage());
	            }
			}
            
            result=ResultGenerator.genSuccessResult(list);
        }catch (Exception e) {  
        	result = ResultGenerator.genFailResult("上传失败");
        }  
        return result;

	}
	@RequestMapping(value="/upload/base64_01", produces = "application/json", method = { RequestMethod.POST })
	public Result upload4(@RequestBody Base64File base64File){
		Result result=null;
		FrontInfoCustDoc doc=new FrontInfoCustDoc();
		try{  		 
			String file=base64File.getData();
	            String dataPrix = "";
	            String data = "";
	        if(file == null || "".equals(file)){
		            return ResultGenerator.genFailResult("上传失败，上传图片数据为空");
	            }else{	            
	                String [] d = file.split("base64,");
	                if(d != null && d.length == 2){
	                    dataPrix = d[0];
	                    data = d[1];
		                }else{
				            return ResultGenerator.genFailResult("上传失败，数据不合法");
		                }
	            }

	            //对数据进行解析，获取文件名和流数据
	            String suffix = "";
	            if("data:image/jpeg;".equalsIgnoreCase(dataPrix)){//data:image/jpeg;base64,base64编码的jpeg图片数据
	                suffix = ".jpg";
	            } else if("data:image/x-icon;".equalsIgnoreCase(dataPrix)){//data:image/x-icon;base64,base64编码的icon图片数据
	                suffix = ".ico";
	            } else if("data:image/gif;".equalsIgnoreCase(dataPrix)){//data:image/gif;base64,base64编码的gif图片数据
	                suffix = ".gif";
	            } else if("data:image/png;".equalsIgnoreCase(dataPrix)){//data:image/png;base64,base64编码的png图片数据
	                suffix = ".png";
	            }else{
	            	 throw new MyException("上传失败，数据不合法");                
	            }
	            String tempFileName = DateTools
						.getCurrentSysData("yyyyMMddHHmmsss")
						+ (int) ((Math.random() * 9 + 1) * 10000) + suffix;

	            //因为BASE64Decoder的jar问题，此处使用spring框架提供的工具包
	            byte[] bs = Base64Utils.decodeFromString(data);
	            try{
	            	String path = ResourcesUtil.getValue("uploanUrl", "uploadUrl");
	            	System.out.println(path);
	                //使用apache提供的工具类操作流
	            	FileUtils.writeByteArrayToFile(new File(path, tempFileName), bs); 
	            	
	            	BeanUtils.copyProperties(base64File, doc);
	            	doc.setFileName(tempFileName);
					doc.setDocPath(tempFileName);
					doc=custDocService.saveDoc(doc);	                
		            }catch(Exception ee){
		                throw new MyException("上传失败，写入文件失败，"+ee.getMessage());
		            }        
            result=ResultGenerator.genSuccessResult(doc);
        }catch (Exception e) {  
        	throw new MyException(e.getMessage());
        }  
        return result;

	}
	
	@RequestMapping(value="/upload/base64_02", produces = "application/json", method = { RequestMethod.POST })
	public Result upload5(@RequestBody String file){
		Result result=null;
		Map<String,String> map=null;
		try{  		 
	            String dataPrix = "";
	            String data = "";
	        if(file == null || "".equals(file)){
		            return ResultGenerator.genFailResult("上传失败，上传数据为空");
	            }else{	            
	                String [] d = file.split("base64,");
	                if(d != null && d.length == 2){
	                    dataPrix = d[0];
	                    data = d[1];
		                }else{
				            return ResultGenerator.genFailResult("上传失败，数据不合法");
		                }
	            }

	            //对数据进行解析，获取文件名和流数据
	            String suffix = "";
	            if("data:image/jpeg;".equalsIgnoreCase(dataPrix)){//data:image/jpeg;base64,base64编码的jpeg图片数据
	                suffix = ".jpg";
	            } else if("data:image/x-icon;".equalsIgnoreCase(dataPrix)){//data:image/x-icon;base64,base64编码的icon图片数据
	                suffix = ".ico";
	            } else if("data:image/gif;".equalsIgnoreCase(dataPrix)){//data:image/gif;base64,base64编码的gif图片数据
	                suffix = ".gif";
	            } else if("data:image/png;".equalsIgnoreCase(dataPrix)){//data:image/png;base64,base64编码的png图片数据
	                suffix = ".png";
	            }else{
	            	 throw new MyException("上传失败，数据不合法");                
	            }
	            String tempFileName = DateTools.getCurrentSysData("yyyyMMddHHmmsss")
						+ (int) ((Math.random() * 9 + 1) * 10000) + suffix;

	            //因为BASE64Decoder的jar问题，此处使用spring框架提供的工具包
	            byte[] bs = Base64Utils.decodeFromString(data);
	            try{
	            	String path = ResourcesUtil.getValue("uploanUrl", "uploadCheckDocUrl");
	                //使用apache提供的工具类操作流
	            	FileUtils.writeByteArrayToFile(new File(path, tempFileName), bs); 	   
	            	map=new HashMap<String,String>();
	            	map.put("docPath", path);
	            	map.put("docName", tempFileName);
		            }catch(Exception ee){
		                throw new MyException("上传失败，写入文件失败，"+ee.getMessage());
		            }        
            result=ResultGenerator.genSuccessResult(map);
        }catch (Exception e) {  
        	throw new MyException(e.getMessage());
        }  
      return result;

	}
	@RequestMapping(value="/getPic",method = RequestMethod.GET)
	public void getPic(String fileName,HttpServletRequest request,HttpServletResponse response){
		String path="";
		OutputStream stream = null;
		try {
			path = ResourcesUtil.getValue("uploanUrl", "uploadUrl")+File.separator+fileName;
			File file = new File(path);
			if(!(file.exists() && file.canRead())){
				file = new File("src/main/resources/images/404.jpg");
			}
			FileInputStream inputStream = new FileInputStream(file);
			byte[] data = new byte[(int) file.length()];
			inputStream.read(data);
		    stream = response.getOutputStream();
			stream.write(data);
			stream.flush();
		} catch (Exception e) {
			e.printStackTrace();
			log.error("图片读取错误："+e.toString());
		}finally {
			try {
				stream.close();
			} catch (IOException e) {
				log.error("图片读取错误："+e.toString());
			}
		}
	}
		@RequestMapping(value="/getTablePic",method = RequestMethod.GET)
		public void getPic(String filePath,String fileName,HttpServletResponse response){
			//String path="";
			OutputStream stream = null;
			try {
				//path = ResourcesUtil.getValue("uploanUrl", "uploadUrl")+File.separator+fileName;
				File file = new File(filePath+File.separator+fileName);
				if(!(file.exists() && file.canRead())){
					file = new File("src/main/resources/images/404.jpg");
				}
				FileInputStream inputStream = new FileInputStream(file);
				byte[] data = new byte[(int) file.length()];
				inputStream.read(data);
			    stream = response.getOutputStream();
				stream.write(data);
				stream.flush();
			} catch (Exception e) {
				e.printStackTrace();
				log.error("图片读取错误："+e.toString());
			}finally {
				try {
					stream.close();
				} catch (IOException e) {
					log.error("图片读取错误："+e.toString());
				}
			}
	}
}
