package org.yjht.web.cust;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.yjht.aop.SystemControllerLog;
import org.yjht.bean.cust.FrontInfoRptDtl;
import org.yjht.core.Result;
import org.yjht.service.cust.RptDtlService;
@RestController
@RequestMapping("/cust")  
@EnableAutoConfiguration 
public class RptDtlController {
	@Autowired
	RptDtlService rptDtlService;
	@RequestMapping(value = "/rptdtl/{custId}/{subjCd}",produces = "application/json;charset=UTF-8",method = RequestMethod.GET)
	@SystemControllerLog(description = "查询客户财务明细信息")
	public Result findRptDtl(@PathVariable String custId,@PathVariable String subjCd) {
		return rptDtlService.findRptDtl(custId, subjCd);
	}
	@RequestMapping(value = "/rptdtl",produces = "application/json;charset=UTF-8",method = RequestMethod.POST)
	@SystemControllerLog(description = "维护客户财务明细信息")
	public Result saveRptDtl(@RequestBody List<FrontInfoRptDtl> rptDtls) {
		return rptDtlService.saveRptDtl(rptDtls);
	}
	@RequestMapping(value = "/rptdtl",produces = "application/json;charset=UTF-8",method = RequestMethod.PUT)
	public Result updateRptDtl(@RequestBody FrontInfoRptDtl rptDtl) {
		return rptDtlService.updateRptDtl(rptDtl);
	}
	@RequestMapping(value = "/rptdtl/{rptDtlId}",produces = "application/json;charset=UTF-8",method = RequestMethod.DELETE)
	@SystemControllerLog(description = "删除客户财务明细信息")
	public Result deleteRptDtl(@PathVariable String rptDtlId) {
		return rptDtlService.deleteRptDtl(rptDtlId);
	}

}
