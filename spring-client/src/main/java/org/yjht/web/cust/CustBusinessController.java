package org.yjht.web.cust;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.yjht.aop.SystemControllerLog;
import org.yjht.bean.cust.CustBusiness;
import org.yjht.core.Result;
import org.yjht.service.cust.CustBusinessService;
import org.yjht.util.ResultGenerator;

@RestController
@RequestMapping("/cust")  
@EnableAutoConfiguration 
public class CustBusinessController {
	@Autowired
	CustBusinessService custBusinessService;
	@RequestMapping(value = "/business/{custId}",produces = "application/json;charset=UTF-8",method = RequestMethod.GET)
	@SystemControllerLog(description = "查询客户经商基本信息")
    public Result findBusiness(@PathVariable String custId){  		
        return ResultGenerator.genSuccessResult(custBusinessService.findBusiness(custId));
    }
	@RequestMapping(value = "/business",produces = "application/json;charset=UTF-8",method = RequestMethod.PUT)
	@SystemControllerLog(description = "修改客户经商基本信息")
    public Result updateBusiness(@RequestBody CustBusiness business){ 
        return custBusinessService.updateBusiness(business);
    }
	@RequestMapping(value = "/business",produces = "application/json;charset=UTF-8",method = RequestMethod.POST)
	@SystemControllerLog(description = "维护客户经商基本信息")
    public Result saveBusiness(@RequestBody CustBusiness business){ 
        return custBusinessService.saveBusiness(business);
    }


}
