package org.yjht.web.cust;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.yjht.aop.SystemControllerLog;
import org.yjht.bean.cust.CustFamily;
import org.yjht.core.Result;
import org.yjht.service.cust.CustFamilyService;
import org.yjht.util.ResultGenerator;

@RestController
@RequestMapping("/cust")  
@EnableAutoConfiguration
public class CustFamilyController {
	@Autowired
	CustFamilyService custFamilyService;
	@RequestMapping(value = "/family/{custId}",produces = "application/json;charset=UTF-8",method = RequestMethod.GET)
	@SystemControllerLog(description = "查询客户家庭信息")
    public Result findFamily(@PathVariable String custId){  		
        return ResultGenerator.genSuccessResult(custFamilyService.findFamily(custId));
    }
	@RequestMapping(value = "/family",produces = "application/json;charset=UTF-8",method = RequestMethod.PUT)
	@SystemControllerLog(description = "修改客户家庭信息")
    public Result updateFamily(@RequestBody CustFamily family){ 
        return custFamilyService.updateFamily(family);
    }
	@RequestMapping(value = "/family",produces = "application/json;charset=UTF-8",method = RequestMethod.POST)
	@SystemControllerLog(description = "维护客户家庭信息")
    public Result saveFamily(@RequestBody CustFamily family){ 
        return custFamilyService.saveFamily(family);
    }

}
