package org.yjht.web.cust;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.yjht.aop.SystemControllerLog;
import org.yjht.bean.cust.CustMortgage;
import org.yjht.bean.cust.CustPledge;
import org.yjht.core.Result;
import org.yjht.service.cust.CustMortgageService;
import org.yjht.service.cust.CustPledgeService;

/**
 * @author lengleng
 * @date 2018/3/16
 * 客户抵押信息管理
 */
@RestController
@RequestMapping("/cust")
public class CustMortgagePledgeController {
    @Autowired
    private CustMortgageService custMortgageService;
    @Autowired
    private CustPledgeService custPledgeService;

    /**
     * 查询客户的抵押信息
     *
     * @param custId 客户号
     * @return Result
     */
    @GetMapping("/mortgage/{custId}")
    @SystemControllerLog(description = "查询客户的抵押信息")
    public Result findMortgage(@PathVariable String custId) {
        return custMortgageService.findMortgage(custId);
    }

    /**
     * 修改抵押信息
     * @param custMortgage 抵押信息
     * @return Result
     */
    @PutMapping("/mortgage")
    @SystemControllerLog(description = "修改抵押信息")
    public Result editMortgage(@RequestBody CustMortgage custMortgage){
      return custMortgageService.editMortgage(custMortgage);
    }

    /**
     * 新增抵押信息
     *
     * @param custMortgage 抵押信息
     * @return Result
     */
    @PostMapping("/mortgage")
    @SystemControllerLog(description = "维护抵押信息")
    public Result addMortgage(@RequestBody CustMortgage custMortgage) {
        return custMortgageService.addMortgage(custMortgage);
    }

    /**
     * 删除抵押信息
     * @param id 抵押信息ID
     * @return Result
     */
    @DeleteMapping("/mortgage/{id}")
    @SystemControllerLog(description = "删除抵押信息")
    public Result deleteMortgage(@PathVariable String id){
        return custMortgageService.deleteMortgage(id);
    }

    /**
     * 查询客户的质押信息
     *
     * @param custId 客户号
     * @return Result
     */
    @GetMapping("/pledge/{custId}")
    @SystemControllerLog(description = "查询客户的质押信息")
    public Result findPledge(@PathVariable String custId) {
        return custPledgeService.findPledge(custId);
    }

    /**
     * 修改质押信息
     * @param custPledge 质押信息
     * @return Result
     */
    @PutMapping("/pledge")
    @SystemControllerLog(description = "修改质押押信息")
    public Result editMortgage(@RequestBody CustPledge custPledge){
        return custPledgeService.editCustPledge(custPledge);
    }

    /**
     * 新增质押信息
     *
     * @param custPledge 质押信息
     * @return Result
     */
    @PostMapping("/pledge")
    @SystemControllerLog(description = "新增质押信息")
    public Result addPledge(@RequestBody CustPledge custPledge) {
        return custPledgeService.addCustPledge(custPledge);
    }

    /**
     * 删除质押信息
     * @param id 质押信息ID
     * @return Result
     */
    @DeleteMapping("/pledge/{id}")
    @SystemControllerLog(description = "删除质押信息")
    public Result deletePledge(@PathVariable String id){
        return custPledgeService.deletePledge(id);
    }
}
