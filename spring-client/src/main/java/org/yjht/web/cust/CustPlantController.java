package org.yjht.web.cust;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.yjht.aop.SystemControllerLog;
import org.yjht.bean.cust.CustPlant;
import org.yjht.core.Result;
import org.yjht.service.cust.CustPlantService;
@RestController
@RequestMapping("/cust")  
@EnableAutoConfiguration 
public class CustPlantController {
	@Autowired
	CustPlantService CustPlantService;
	@RequestMapping(value = "/plant/{custId}",produces = "application/json;charset=UTF-8",method = RequestMethod.GET)
	@SystemControllerLog(description = "客户管理-查询种植基本信息")
    public Result findCustPlant(@PathVariable String custId){  		
        return CustPlantService.findCustPlant(custId);
    }

	@RequestMapping(value = "/plant",method = RequestMethod.POST)
	@SystemControllerLog(description = "客户管理-维护种植详细信息")
    public Result saveCustPlant(@RequestBody CustPlant plant){ 
        return CustPlantService.saveCustPlant(plant);
    }

}
