package org.yjht.web.cust;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.yjht.aop.SystemControllerLog;
import org.yjht.bean.cust.FrontInfoCustBusiness;
import org.yjht.core.Result;
import org.yjht.service.cust.FrontInfoCustBusinessService;
@RestController
@RequestMapping("/cust")  
@EnableAutoConfiguration 
public class FrontInfoCustBusinessController {
	@Autowired
	FrontInfoCustBusinessService frontInfoCustBusinessService;
	@RequestMapping(value = "/custBusiness",produces = "application/json;charset=UTF-8",method = RequestMethod.GET)
	@SystemControllerLog(description = "查询客户经商详细信息")
    public Result findFrontInfoCustBusiness(String custId){  		
        return frontInfoCustBusinessService.findFrontInfoCustBusiness(custId);
    }
	@RequestMapping(value = "/custBusiness/{id}",produces = "application/json;charset=UTF-8",method = RequestMethod.GET)
    public Result findFrontInfoCustBusinessById(@PathVariable String id){  		
        return frontInfoCustBusinessService.findFrontInfoCustBusinessById(id);
    }
	
	@RequestMapping(value = "/custBusiness",produces = "application/json;charset=UTF-8",method = RequestMethod.PUT)
	@SystemControllerLog(description = "修改客户经商详细信息")
    public Result updateFrontInfoCustBusiness(@RequestBody FrontInfoCustBusiness frontInfoCustBusiness){ 
        return frontInfoCustBusinessService.updateFrontInfoCustBusiness(frontInfoCustBusiness);
    }
	@RequestMapping(value = "/custBusiness",produces = "application/json;charset=UTF-8",method = RequestMethod.POST)
	@SystemControllerLog(description = "保存客户经商详细信息")
    public Result saveFrontInfoCustBusiness(@RequestBody FrontInfoCustBusiness frontInfoCustBusiness){ 
        return frontInfoCustBusinessService.saveFrontInfoCustBusiness(frontInfoCustBusiness);
    }
	@RequestMapping(value = "/custBusiness/{custId}/{id}",produces = "application/json;charset=UTF-8",method = RequestMethod.DELETE)
	@SystemControllerLog(description = "删除客户经商详细信息")
    public Result deleteFrontInfoCustBusiness(@PathVariable String custId,@PathVariable String id){ 
        return frontInfoCustBusinessService.deleteFrontInfoCustBusiness(custId,id);
    }

}
