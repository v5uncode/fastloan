package org.yjht.web.platform;

import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.yjht.aop.SystemControllerLog;
import org.yjht.bean.cust.CustBase;
import org.yjht.core.Page;
import org.yjht.core.Result;
import org.yjht.service.cust.CustBaseService;
import org.yjht.service.platform.CustRemindService;

@RestController
@RequestMapping("/")
public class CustRemind {
	
	@Autowired
	CustBaseService custBaseService;
	
	@Autowired
	CustRemindService custRemindService;
	
	@RequestMapping(value = "custRemind", produces = "application/json;charset=UTF-8", method = { RequestMethod.GET })
	@SystemControllerLog(description = "查询客户基本信息")
	public Result queryCustRemind(CustBase cust,Page page){		
		return custBaseService.selectList(cust,page);
	}
	@RequestMapping(value = "custRemind", produces = "application/json;charset=UTF-8", method = { RequestMethod.POST })
	@SystemControllerLog(description = "保存批量提醒信息")
	public Result saveCustRemind(@RequestBody Map<String,Object> map){
		
		return custRemindService.saveCustRemind(map);
	}
}
