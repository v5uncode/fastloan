package org.yjht.web.platform;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.yjht.aop.SystemControllerLog;
import org.yjht.aop.SystemServiceLog;
import org.yjht.bean.DailyLog;
import org.yjht.bean.Schedule;
import org.yjht.core.Result;
import org.yjht.service.platform.DailyLogService;
import org.yjht.service.platform.LrdPlatformService;


@RestController
@RequestMapping("/lrdPlatform")
public class LrdPlatformController {
	
	@Autowired
	LrdPlatformService lrdPlatformService;
	@Autowired
	DailyLogService dailyLogService;
	
	@RequestMapping(produces = "application/json;charset=UTF-8", method = { RequestMethod.GET })
	@SystemControllerLog(description = "查询客户纪念日提醒")
	public Result queryTx(HttpServletRequest request) {
		
		return lrdPlatformService.queryTx(request);
	}
	
	@RequestMapping(value = "/schedule", produces = "application/json;charset=UTF-8", method = { RequestMethod.GET })
	@SystemControllerLog(description = "查询用户日程计划")
	public Result query(HttpServletRequest request,@RequestParam Map<String,Object> map) {
		
		return lrdPlatformService.query(request,map);
	}
	
	@RequestMapping(value = "/schedule/schedule", produces = "application/json;charset=UTF-8", method = { RequestMethod.GET })
	@SystemServiceLog(description = "查询用户日程计划数量")
	public Result queryNum(HttpServletRequest request,@RequestParam Map<String,Object> map) {
		
		return lrdPlatformService.queryNum(request,map);
	}
	
	@RequestMapping(value = "/schedule", produces = "application/json;charset=UTF-8", method = { RequestMethod.POST })
	@SystemServiceLog(description = "用户新增-日程计划")
	public Result saveSchedule(HttpServletRequest request,@RequestBody Schedule schedule) {		
		return lrdPlatformService.saveSchedule(request,schedule);
	}
	@RequestMapping(value = "/schedule", produces = "application/json;charset=UTF-8", method = { RequestMethod.PUT })
	@SystemServiceLog(description = "用户修改-日程计划")
	public Result updateSchedule(@RequestBody Schedule schedule) {		
		return lrdPlatformService.updateSchedule(schedule);
	}
	
	@RequestMapping(value = "/schedule/{scheduleId}", produces = "application/json;charset=UTF-8", method = { RequestMethod.DELETE })
	@SystemServiceLog(description = "删除用户日程计划")
	public Result deleteSchedule(@PathVariable String scheduleId) {
		
		return lrdPlatformService.deleteSchedule(scheduleId);
	}
	
	@RequestMapping(value = "/dailylog", produces = "application/json;charset=UTF-8", method = { RequestMethod.GET })
	@SystemServiceLog(description = "查询工作日志")
	public Result queryDailylog(HttpServletRequest request,DailyLog dailylog) {
		
		return dailyLogService.queryDailylog(request,dailylog);
	}
	
	@RequestMapping(value = "/dailylog", produces = "application/json;charset=UTF-8", method = { RequestMethod.POST })
	@SystemServiceLog(description = "保存/修改工作日志")
	public Result saveDailylog(HttpServletRequest request,@RequestBody DailyLog dailylog) {		
		return dailyLogService.saveDailylog(request,dailylog);
	}
	
	@RequestMapping(value = "/dailylog", produces = "application/json;charset=UTF-8", method = { RequestMethod.DELETE })
	@SystemServiceLog(description = "删除工作日志")
	public Result deleteCustZg(HttpServletRequest request,@RequestBody DailyLog dailylog) {
		
		return dailyLogService.deleteDailylog(request,dailylog);
	}
}
