package org.yjht.web.ruleDef;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.yjht.aop.SystemControllerLog;
import org.yjht.bean.RuleDef;
import org.yjht.core.Query;
import org.yjht.core.Result;
import org.yjht.service.ruleDef.RuleDefService;

@RestController
@RequestMapping("")
public class RuleDefController {
	
	@Autowired
	RuleDefService ruleDefService;
	
	@RequestMapping(value = "/ruleDef", produces = "application/json;charset=UTF-8", method = { RequestMethod.GET })
	@SystemControllerLog(description = "查询制度列表")
	public Result queryList(@RequestParam Map<String, Object> params) {
		//查询列表数据
        Query query = new Query(params);
		return ruleDefService.queryList(query);
	}
	@PostMapping(value="ruleDef",produces = "application/json;charset=UTF-8")
	@SystemControllerLog(description = "添加制度")
	public Result saveRuleDef(@RequestBody RuleDef ruleDef) {		
		return ruleDefService.saveRuleDef(ruleDef);
	}
	
	@PutMapping(value = "/ruleDef/{id}",produces = "application/json;charset=UTF-8")
	@SystemControllerLog(description = "修改制度")
	public Result updatRuleDef(@RequestBody RuleDef ruleDef,@PathVariable String id) {
		return ruleDefService.updatRuleDef(ruleDef,id);
	}
	
	@RequestMapping(value = "/ruleDef/{ruleNo}", produces = "application/json;charset=UTF-8", method = { RequestMethod.DELETE })
	@SystemControllerLog(description = "删除制度")
	public Result deleteRuleDef(@PathVariable String ruleNo) {
		
		return ruleDefService.deleteRuleDef(ruleNo);
	}
	
}
