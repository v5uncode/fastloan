package org.yjht.wechat.mapper;

import java.util.Map;

import org.apache.ibatis.annotations.Param;

public interface WcMapper {
	
	//根据身份证编码获取存量客户的信息
	Map<String, String> getClkhInfo(@Param(value = "idNo") String idNo);
	
	//根据农商行组织架构中的法人编码查询对应的信贷系统组织架构中的法人编码
	String getLegalPersonNo(String no);
	
	//根据身份证编码查询客户是否达到评级条件
	public Map<String, String> getCustInfoIsComplate(String idNo);
	
	//根据用户编号查找员工号
	String getManagerIdByUserId(String userId);
	//根据员工号查找用户编号
	String getUserIdByManagerId(String managerId);

}
