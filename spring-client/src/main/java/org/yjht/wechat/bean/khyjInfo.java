package org.yjht.wechat.bean;

import org.yjht.bean.rest.WxBaseBean;

/**
 * 客户移交实体
 * @author lenovo
 *
 */
public class khyjInfo extends WxBaseBean{
	/*
	 * 移交ID
	 */
	private String yjId;	
	/*
	 * 原客户经理
	 */
	private String lastManagerId;	
	/*
	 * 移交结果,1表示已移交,2表示退回
	 */
	private String transferResult;	
	/*
	 * 移交退回原因
	 */
	private String backReason;

	public String getYjId() {
		return yjId;
	}

	public void setYjId(String yjId) {
		this.yjId = yjId;
	}

	public String getLastManagerId() {
		return lastManagerId;
	}

	public void setLastManagerId(String lastManagerId) {
		this.lastManagerId = lastManagerId;
	}

	public String getTransferResult() {
		return transferResult;
	}

	public void setTransferResult(String transferResult) {
		this.transferResult = transferResult;
	}

	public String getBackReason() {
		return backReason;
	}

	public void setBackReason(String backReason) {
		this.backReason = backReason;
	}
	
	

}
