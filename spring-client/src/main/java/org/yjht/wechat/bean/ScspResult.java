package org.yjht.wechat.bean;

import org.yjht.bean.rest.WxBaseBean;

/**
 * 审批结果对象
 * @author zhaolulu
 */
public class ScspResult extends WxBaseBean{
	
	private String approvalResult;//审批结果
	
	private String sxed;//授信额度
	
	private String sxqx;//授信期限
	
	private String notPassReason;//拒绝原因
	
	private String sqje;//申请金额
	
	private String phone;//客户电话

	public String getApprovalResult() {
		return approvalResult;
	}

	public void setApprovalResult(String approvalResult) {
		this.approvalResult = approvalResult;
	}

	public String getSxed() {
		return sxed;
	}

	public void setSxed(String sxed) {
		this.sxed = sxed;
	}

	public String getSxqx() {
		return sxqx;
	}

	public void setSxqx(String sxqx) {
		this.sxqx = sxqx;
	}

	public String getNotPassReason() {
		return notPassReason;
	}

	public void setNotPassReason(String notPassReason) {
		this.notPassReason = notPassReason;
	}

	public String getSqje() {
		return sqje;
	}

	public void setSqje(String sqje) {
		this.sqje = sqje;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	

}
