package org.yjht.wechat.bean;

import lombok.Getter;
import lombok.Setter;
import org.yjht.bean.rest.WxBaseBean;

/**
 * 放款结果对象
 */
@Getter
@Setter
public class LoanResult extends WxBaseBean {
    private String loanMoney;  //放款金额

    private String loanStatus;  //放款状态

    private String phone; //客户电话

}
