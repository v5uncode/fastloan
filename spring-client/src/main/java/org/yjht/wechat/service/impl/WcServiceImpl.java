package org.yjht.wechat.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.yjht.bean.FlInfoApproval;
import org.yjht.bean.FlInfoApprovalVo;
import org.yjht.bean.FrontInfoLoan;
import org.yjht.bean.FrontInfoScsp;
import org.yjht.bean.cust.FrontInfoCustYj;
import org.yjht.bean.rest.ZrParam;
import org.yjht.calculate.model.ObjectResponseVo;
import org.yjht.calculate.service.WechatAdmitService;
import org.yjht.core.Constants;
import org.yjht.mapper.FlInfoApprovalMapper;
import org.yjht.mapper.FrontInfoLoanMapper;
import org.yjht.mapper.FrontInfoScspMapper;
import org.yjht.service.cust.CustYjService;
import org.yjht.wechat.mapper.WcMapper;
import org.yjht.wechat.service.WcService;
import tk.mybatis.mapper.entity.Example;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service("wcService")
@Slf4j
public class WcServiceImpl implements WcService{
	@Autowired
	private CustYjService custYjService;
	@Autowired
	private WcMapper wcMapper;
	@Autowired
	private FlInfoApprovalMapper flInfoApprovalMapper;
	@Autowired
	private FrontInfoScspMapper frontInfoScspMapper;
	@Autowired
	private FrontInfoLoanMapper frontInfoLoanMapper;	
	@Autowired
	private WechatAdmitService wechatAdmitService;
	

	public Map<String,String> getClkhInfo(String idNo){
		return wcMapper.getClkhInfo(idNo);
	}
	
	public String getLegalPersonNo(String no) {
		return wcMapper.getLegalPersonNo(no);
	}
	@Override
	public Map<String, Object> khyj(Map<String, Object> map) {//移交处理
		Map<String,Object> resultMap=new HashMap<String,Object>();
		FrontInfoCustYj custYj=new FrontInfoCustYj();
		custYj.setYjId(map.get("yjId").toString());//移交Id
		String isAgree=map.get("isAgree")+"";
		if(isAgree.equals(Constants.STATYCL)) {//同意
			custYj.setStat(Constants.STATYCL);		
			custYj.setUserIdSq(map.get("managerId").toString());
		}else if(isAgree.equals(Constants.STATJJ)) {//拒绝
			custYj.setStat(Constants.STATJJ);
			if(map.get("refuseReason")!=null) {
				custYj.setContent(map.get("refuseReason").toString());
			}
		}else {
			resultMap.put("returnStatus", "0");//表示传过来的信息有误
			resultMap.put("returnMes", "没有指明是否同意");
			return resultMap;
		}
		Boolean isSuccess = custYjService.updateStat(custYj);
		if(isSuccess){			
			resultMap.put("returnStatus", isAgree);//处理成功			
		}else{
			resultMap.put("returnStatus", "3");//已经被移交过
		}	
		return resultMap;
	}

	@Override
	public Map<String, Object> getClkhInfoByIdNo(String idNo, String frNs,String custName) {//存量客户
		Map<String,Object> resultMap=new HashMap<String,Object>();
		Map<String,String> clkhInfo=getClkhInfo(idNo);
		String fr_xd = null;
		if(clkhInfo!=null) {
			fr_xd = clkhInfo.get("LEGAL_PERSON1");
			if(StringUtils.isBlank(fr_xd)){
				resultMap.put("clyh", "0");//新客户
				resultMap.put("sxye", "2");//无授信
				fr_xd = getLegalPersonNo(frNs);
			}else{
				resultMap.put("clyh", "1");//存量客户
				resultMap.put("khjl", clkhInfo.get("CUST_GRP_JL"));//客户经理
				resultMap.put("fr_ns", clkhInfo.get("LEGAL_PERSON2"));//农商行组织架构下的法人编码
				resultMap.put("jg_ns", clkhInfo.get("GROUP2"));//农商行组织架构下的机构编码
				String sxye = clkhInfo.get("ISCREDIT");//是否有授信
				if(Constants.IS_BALANCE_YES.equals(sxye)){					
					resultMap.put("sxye", "1");//有授信
				}else{
					resultMap.put("sxye", "2");//无授信
				}
			}
		}else {
			resultMap.put("clyh", "0");//不是存量客户
			resultMap.put("sxye", "2");//无授信
			fr_xd = getLegalPersonNo(frNs);
		}
		//调用准入规则
		log.info("=======身份证号：" + idNo +  ",法人：" + fr_xd + ",微信传法人：" + frNs +",姓名："+custName+"=======");
		ZrParam param=new ZrParam();
		param.setCustname(custName);
		param.setIdno(idNo);	
		// TODO: 2018/6/7  策略引擎接口
		ObjectResponseVo result =wechatAdmitService.wechatAdmin(param);
		if(result.getIsException()){
			resultMap.put("zr", "9");
			resultMap.put("fzr_yy", result.getExceptionInfo());
		}else{
			if(result.getIsPass()){
				resultMap.put("zr", "1");//准入通过的标识 1:通过   0:不通过  9:异常
			}else{
				resultMap.put("zr", "0");
				resultMap.put("fzr_yy", result.getNotPassReason());
			}
		}
		return resultMap;
	}
	@Override
	public Map<String, Object> getJdcxByIdNo(String idNo,String applytime) {//进度查询
		Map<String, Object> result = new HashMap<String, Object>();
		Map<String, String> map = getKhztByIdNo(idNo,applytime);
		/**
		 * 审批状态：0表示信息采集,1表示信息采集完成等待评级，2表示拒绝受理，3表示评级完成但未提交审批 或者 审批流程未提交，
		 * 			  4表示授信方案提交后审批中，5表示审批拒绝，6表示审批通过等待放款,7表示放款成功，8表示放款失败
		 */
		result.put("khzt", map.get("khzt"));
		//result.put("kd_sxed", map.get("kd_sxed"));//授信保证额度
		//result.put("kd_sxqx", map.get("kd_sxqx"));//授信期限
		result.put("jjyy", map.get("jjyy"));//拒绝原因
		result.put("dealTime", map.get("dealTime"));//审批时间
		result.put("fktime",map.get("fktime")); //放款时间
		return result;
	}
	@Override
	public Map<String, Object> getEdcxByIdNo(String idNo,String applytime) {//额度查询
		Map<String,Object> map=new HashMap<String,Object>();
		//判断是否已授信
		Boolean isSx = true;
		if(isSx){
			//设置授信结果，已授信
			map.put("sx", "1");
			map.put("xd_sxed", "");
			map.put("xd_kyed", "");		
		}else{
			//设置授信结果,未授信
			map.put("sx", "0");
		}
		Map<String, String> result = getKhztByIdNo(idNo,applytime);
		map.put("khzt", result.get("khzt"));//状态
		map.put("managerId", result.get("managerId"));//客户经理
		map.put("sqed", result.get("sqed"));//申请额度
		//map.put("bzed", result.get("bzed"));//预授信保证额度
		map.put("xyed", result.get("xyed"));//预授信信用额度
		map.put("zged", result.get("zged"));//预授信最高额度
		map.put("dzyed", result.get("dzyed"));//预授信抵质押额度
		//map.put("kd_sxed", result.get("kd_sxed"));//快贷授信额度
		//map.put("kd_kyed", result.get("kd_kyed"));//快贷可用额度
		//map.put("kd_sxqx", result.get("kd_sxqx"));//授信期限
		map.put("jjyy", result.get("jjyy"));//拒绝原因
		return map;
	}
	public Map<String,String> getKhztByIdNo(String idNo,String applytime){
		Map<String, String> result = new HashMap<String, String>();
		Map<String, String> result1 = wcMapper.getCustInfoIsComplate(idNo);
		String isComplate = null;
		if(result1 != null){
			isComplate = result1.get("CREATE_TYPE");
			result.put("managerId",  result1.get("CUST_GRP_JL"));
		}
		//根据身份证号判断客户是否通过通过审批
		if("2".equals(isComplate)){//信息已采集完整
			//根据身份证号判断客户是否通过评级
			FlInfoApprovalVo vo = queryByIdNo(idNo,applytime);
			/*FlInfoApprovalVo vo = null;*/
			if(vo.getIsPass() != null && !"".equals(vo.getIsPass())){//等待评级中
				Boolean isPass = vo.getIsPass();
				if(isPass){//通过评级
					if(vo.getApplyAmount() != null){
						result.put("sqed", (vo.getApplyAmount().doubleValue())+"");//申请额度
					}
					if(vo.getEdLmt()!=null){
						result.put("zged", vo.getEdLmt().doubleValue()+"");//最高额度
					}else if(vo.getCreditAmount()!=null && vo.getPledgeAmount() != null){//信用额度与抵质押额度都不为空时，最高额度取较大值
					    String zged = vo.getCreditAmount().doubleValue() > vo.getPledgeAmount().doubleValue() ? vo.getCreditAmount().doubleValue()+"" : vo.getPledgeAmount().doubleValue()+"";
                        result.put("zged", zged);//最高额度
                    }else if(vo.getCreditAmount()!=null && vo.getPledgeAmount() == null){
                        result.put("zged", vo.getCreditAmount().doubleValue()+"");//最高额度
                    }else if(vo.getCreditAmount()==null && vo.getPledgeAmount() != null){
                        result.put("zged", vo.getPledgeAmount().doubleValue()+"");//最高额度
                    }
					if(vo.getCreditAmount()!=null){
						result.put("xyed", vo.getCreditAmount().doubleValue()+"");//信用额度
					}
					if(vo.getPledgeAmount() != null){
						result.put("dzyed", vo.getPledgeAmount().doubleValue()+"");//抵质押额度
					}
					/*if(vo.getGuaranteeAmount() != null){
						result.put("bzed", vo.getGuaranteeAmount().toPlainString());//保证额度
					}
					result.put("qqded", vo.getQqLimit());//亲情贷额度*/	
					/*Condition condition = new Condition(FrontInfoScsp.class);
					condition.createCriteria().andEqualTo("idNo",idNo );*/
					Example exampleScsp = new Example(FrontInfoScsp.class);
					exampleScsp.createCriteria().andEqualTo("custId",vo.getCustId());
					exampleScsp.createCriteria().andEqualTo("idNo",idNo);
					exampleScsp.setOrderByClause("CRT_DATE DESC");
					List<FrontInfoScsp> result2 = frontInfoScspMapper.selectByExample(exampleScsp);
					if(result2 == null || result2.size() == 0){//未提交审批
						result.put("khzt", "3");
					}else{
						FrontInfoScsp spjg = result2.get(0);
						String status = "";
						if(spjg.getProcessState() != null){
							status =spjg.getProcessState();
						}
						//审批时间
						if(spjg.getMtnDate() != null && !"".equals(spjg.getMtnDate())){
							result.put("dealTime", spjg.getMtnDate() );//审批时间
						}else{
							result.put("dealTime", spjg.getCrtDate() );//审批时间
						}
						if(Constants.STATWTJ.equals(status)){//审批流程未提交
							result.put("khzt", "3");
						}else if(Constants.STATLCZ.equals(status)){
							result.put("khzt", "4"); //未完成审批
						}else if(Constants.STATYJS.equals(status)){
							String spResult = spjg.getSpResult();
							if(Constants.SPJGPASS.equals(spResult)){ //审批通过
								Example exampleLoan = new Example(FrontInfoLoan.class);
								exampleLoan.createCriteria().andEqualTo("scspId", spjg.getScspId());
								exampleLoan.createCriteria().andEqualTo("custId",spjg.getCustId());
								exampleLoan.setOrderByClause("CRT_DATE DESC");
								List<FrontInfoLoan> loanList = frontInfoLoanMapper.selectByExample(exampleLoan);
								if(loanList != null && loanList.size() > 0 ){ //存在放款数据
									FrontInfoLoan loan = loanList.get(0);
									if(loan.getMtnDate() != null && !"".equals(loan.getMtnDate())){
										result.put("fktime",loan.getMtnDate());
									}else{
										result.put("fktime",loan.getCrtDate());
									}
									if(Constants.FKCG.equals(loan.getLoanStatus())){
										result.put("khzt", "7");//放款成功
									}else {
										result.put("khzt","8"); //放款失败
									}
								}else {
									result.put("khzt", "6");//审批通过等待放款
								}
							}else{
								result.put("khzt", "5");//审批拒绝
							}

							/*if(spjg.getSynthesizeLimit() != null){
								if(spjg.getSynthesizeLimit()!=null&&spjg.getSynthesizeLimit()!=""){
									result.put("kd_sxed",Double.parseDouble(spjg.getSynthesizeLimit())*10000+"");//快贷授信额度
									result.put("kd_kyed",Double.parseDouble(spjg.getSynthesizeLimit())*10000+"");//快贷可用额度
								}
							}
							if(spjg.getCreditAllotted() != null){
								result.put("kd_sxqx", spjg.getCreditAllotted());//快贷授信期限
							}*/
						}
					}
				}else{
					//评级不受理
					result.put("khzt", "2");
					result.put("jjyy", vo.getNotPassReason());
				}
			}else{//信息完整未评级
				result.put("khzt", "1");
			}
		}else{ //表示信息不完整，采集过程中
			result.put("khzt", "0");
		}
		return result;
	}
	
		//根据身份证获取评级结果
		public FlInfoApprovalVo queryByIdNo(String idNo,String applytime) {
			FlInfoApproval approval = flInfoApprovalMapper.queryByIdNo(idNo,applytime);
			FlInfoApprovalVo vo = new FlInfoApprovalVo();
			if(approval != null){
				BeanUtils.copyProperties(approval,vo);
				if(!"T".equals(vo.getZrResult())){
					vo.setIsPass(false);
					vo.setNotPassReason(vo.getZrDesc());
				}else{
					vo.setIsPass(true);
				}
			}
			return vo;
		}

		
	
}
