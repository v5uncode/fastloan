package org.yjht.wechat.service;

import java.util.Map;

public interface WcService {
	
	/**
	 * 微信端进行客户移交
	 * @param map 
	 */
	public Map<String,Object> khyj(Map<String,Object> map);
	/**
	 * 查询存量客户信息
	 * @param idNo
	 * @param frNs
	 * @return
	 */
	Map<String,Object> getClkhInfoByIdNo(String idNo,String frNs,String custName);
	/**
	 * 根据身份证号查询审批进度
	 * @param idNo,applytime
	 * @return
	 */
	Map<String,Object> getJdcxByIdNo(String idNo,String applytime);
	/**
	 * 根据身份证号查询额度信息
	 * @param idNo ,applytime
	 * @return
	 */
	Map<String,Object> getEdcxByIdNo(String idNo,String applytime);
}
