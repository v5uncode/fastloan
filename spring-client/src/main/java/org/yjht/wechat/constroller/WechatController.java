package org.yjht.wechat.constroller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.yjht.aop.SystemControllerLog;
import org.yjht.core.Result;
import org.yjht.exception.MyException;
import org.yjht.util.HttpUtil;
import org.yjht.util.JsonUtils;
import org.yjht.util.ResultGenerator;
import org.yjht.wechat.service.WcService;

import java.util.HashMap;
import java.util.Map;

import javax.print.attribute.HashAttributeSet;

@RestController
@RequestMapping("/")
public class WechatController {
	@Autowired
	WcService wcService;
	
	@PostMapping(value="wxInfo",produces = "application/json;charset=UTF-8")
	@SystemControllerLog(description = "接收微信请求接口")
	public Result wcHandler(@RequestBody Map<String,Object> map) {
		Map<String,Object> resultMap=null;
		String qqlx=map.get("qqlx")==null?null:map.get("qqlx").toString();//请求类型	
		String idNo=map.get("sfz")==null?null:map.get("sfz").toString(); //身份证号
		if(qqlx==null) {
			throw new MyException("参数异常");
		}
		if("KHYJ".equals(qqlx)) {//客户移交处理
			resultMap=wcService.khyj(map);   
		}else if("CLKH".equals(qqlx)) {//是否是存量客户
			String frNs=map.get("fr_ns")==null?"":map.get("fr_ns").toString();
			String custName=map.get("kh_name")==null?"":map.get("kh_name").toString();
			resultMap=wcService.getClkhInfoByIdNo(idNo,frNs,custName);
		}else if("JDCX".equals(qqlx)) {//进度查询
			String applytime=map.get("applytime")==null?null:map.get("applytime").toString();//贷款申请时间
			resultMap=wcService.getJdcxByIdNo(idNo,applytime);
		}else if("EDCX".equals(qqlx)) {//额度测算
			String applytime=map.get("applytime")==null?null:map.get("applytime").toString();//贷款申请时间
			resultMap=wcService.getEdcxByIdNo(idNo,applytime);
		}	
		
		return ResultGenerator.genSuccessResult(resultMap);
	}

}
