package org.yjht.enums;

public enum MarriageType {
	WH("未婚","10"),
	YH("已婚","20"),
	CH("初婚","21"),
	H("再婚","22"),
	FH("复婚","23"),
	SO("丧偶","30"),
	LH("离婚","40"),
	OTHER("未说明的婚姻状况","90");
	/** 
	   * 活动状态的描述。 
	   */
	  private String value; 
	  /** 
	   * 活动状态的值。 
	   */
	  private String code;
	  /** 
	   * @param status 活动状态的中文描述 
	   * @param desc 活动状态的值 
	   */
	  private MarriageType(String value, String code) { 
		this.value = value; 
	    this.code = code; 
	  } 
	  /** 
	   * @return 当前枚举对象的描述。 
	   */
	  public String getValue() { 
	    return value; 
	  } 
	  /** 
	   * @return 当前状态的值。 
	   */
	  public String getCode() { 
	    return code; 
	  } 
	  /** 
	   * 根据活动状态的值获取枚举对象。 
	   * 
	   * @param status 活动状态的值 
	   * @return 枚举对象 
	   */
	  public static String getCode(String status) { 
		  MarriageType[] allStatus = MarriageType.values(); 
	    for (MarriageType ws : allStatus) { 
	      if (ws.getValue().equalsIgnoreCase(status)) { 
	        return ws.getCode(); 
	      } 
	    } 
	    throw new IllegalArgumentException("status值非法，没有符合关系类型的枚举对象"); 
	  }
}
