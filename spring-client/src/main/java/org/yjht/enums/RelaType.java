package org.yjht.enums;

public enum RelaType {
	MARRIAGE("配偶","1"),
	PARENT("父母","2"),
	CHILD("子女","3"),
	GRANDPARENT("祖父母","4"),
	GRANDCHILD("孙子女","5"),
	BROTHER("兄弟姐妹","6"),
	OTHMAR("其他","7");
	/** 
	   * 活动状态的值。 
	   */
	  private String value; 
	  /** 
	   * 活动状态的中文描述。 
	   */
	  private String code; 
	  /** 
	   * @param status 活动状态的值 
	   * @param desc 活动状态的中文描述 
	   */
	  private RelaType(String value, String code) { 
		this.value = value; 
	    this.code = code; 
	  } 
	  /** 
	   * @return 当前枚举对象的值。 
	   */
	  public String getValue() { 
	    return value; 
	  } 
	  /** 
	   * @return 当前状态的中文描述。 
	   */
	  public String getCode() { 
	    return code; 
	  } 
	  /** 
	   * 根据活动状态的值获取枚举对象。 
	   * 
	   * @param status 活动状态的值 
	   * @return 枚举对象 
	   */
	  public static String getCode(String status) { 
		  RelaType[] allStatus = RelaType.values(); 
	    for (RelaType ws : allStatus) { 
	      if (ws.getValue().equalsIgnoreCase(status)) { 
	        return ws.getCode(); 
	      } 
	    } 
	    throw new IllegalArgumentException("status值非法，没有符合关系类型的枚举对象"); 
	  } 
}
