package org.yjht.xindai.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.yjht.bean.cust.CustBase;
import org.yjht.bean.cust.CustFamily;
import org.yjht.bean.cust.FrontInfoCustStaff;
import org.yjht.bean.cust.FrontInfoFinRpt;
import org.yjht.bean.scsp.FrontInfoCredit;
import org.yjht.bean.scsp.FrontInfoGuarantor;
import org.yjht.service.cust.CustBaseService;
import org.yjht.service.cust.CustFamilyService;
import org.yjht.service.cust.CustStaffService;
import org.yjht.service.cust.FinRptService;
import org.yjht.service.cust.RptDtlService;
import org.yjht.service.scsp.CreditService;
import org.yjht.service.scsp.GuarantorService;
import org.yjht.xindai.common.req.XingYe2000;
import org.yjht.xindai.common.req.XingYe2001;
import org.yjht.xindai.common.req.XingYe2002;
import org.yjht.xindai.common.req.XingYe2003;
import org.yjht.xindai.service.XingYeGetDateService;
@Service
public class XingYeGetDateServiceImpl implements XingYeGetDateService {
	@Autowired
    private CustBaseService custBaseService;
    @Autowired
    private CustStaffService custStaffService;
    @Autowired
    private CustFamilyService custFamilyService;
    @Autowired
    private FinRptService finRptService;
    @Autowired
    private RptDtlService rptDtlService;  
    @Autowired
    private CreditService  creditService;
    @Autowired
    private GuarantorService guarantorService;
    @Autowired
    //额度默认值
    private static final BigDecimal DEFAULE_VALUE=new BigDecimal(0);
    
	@Override
	public XingYe2000 get2000Date(String custId) {
		XingYe2000 custInfo = new XingYe2000();
        //基础信息表
        CustBase custBase = custBaseService.findByCustid(custId);
        custInfo.setCustBase(custBase);
        //职工信息表
        FrontInfoCustStaff staff = custStaffService.findStaff(custId);
        if(staff!=null){
        	custInfo.setStaff(staff);
        }
        //家庭信息表
        CustFamily family = (CustFamily) custFamilyService.findFamily(custId);
        custInfo.setFamily(family);
        //财务信息
        FrontInfoFinRpt rpt = finRptService.findFinRptById(custId);
        if(rpt != null){
            //个人月收入
            custInfo.setYsr(rpt.getYsr() != null?rpt.getYsr().toString():DEFAULE_VALUE.toString());
            //家庭月收入  
            custInfo.setJtsr(rpt.getSr1y()==null?DEFAULE_VALUE.toString():String.valueOf(rpt.getSr1y().divide(new BigDecimal(12),2,BigDecimal.ROUND_HALF_UP))); 
            List<Map<String,String>> jtsrList = rptDtlService.findSrRptDtlById(custId);
            Double jtwdsr=0.0,jtzj=0.0,jtqtsr=0.0,jtfh=0.0;
            for(Map<String,String> jtsrMap : jtsrList){
            	////家庭稳定收入
            	if(StringUtils.isNotBlank(jtsrMap.get("jtwdsr"))){
            		jtwdsr+=Double.parseDouble(jtsrMap.get("jtwdsr"));
            	}else if(StringUtils.isNotBlank(jtsrMap.get("jtzj"))){ //家庭租金
            		jtzj+=Double.parseDouble(jtsrMap.get("jtzj"));
            	}else if(StringUtils.isNotBlank(jtsrMap.get("jtqtsr"))){ //家庭其他收入
            		jtqtsr+=Double.parseDouble(jtsrMap.get("jtqtsr"));
            	}else if(StringUtils.isNotBlank(jtsrMap.get("jtfh"))){//家庭投资分红
            		jtfh+=Double.parseDouble(jtsrMap.get("jtfh"));
            	}
            }
            custInfo.setJtwdsr(jtwdsr.toString());
            custInfo.setJtzj(jtzj.toString());
            custInfo.setJtqtsr(jtqtsr.toString());
            custInfo.setJtfh(jtfh.toString());
            //金融资产（对应现金及银行存款）
            custInfo.setJrzc(rpt.getXjjyhck() != null?rpt.getXjjyhck().toString():DEFAULE_VALUE.toString());
            //其他资产(对应（应收账款、预付款项、存货、其他资产）)
            double yszk = rpt.getYszk()==null?0:rpt.getYszk().doubleValue();
            double yfcx=rpt.getYfcx()==null?0:rpt.getYfcx().doubleValue();
            double ck=rpt.getCk()==null?0:rpt.getCk().doubleValue();
            double jyzc=rpt.getQtjyzc()==null?0:rpt.getQtjyzc().doubleValue();
            double qtzc=yszk+yfcx+ck+jyzc;
            custInfo.setQtzc(String.valueOf(qtzc));
            //固定资产
            BigDecimal gdzc = rpt.getGdzc() != null?rpt.getGdzc(): DEFAULE_VALUE;
            custInfo.setGdzc(String.valueOf(gdzc));                     
            //负债及或有负债
            BigDecimal hyfz = rpt.getJthyfz() != null?rpt.getJthyfz():DEFAULE_VALUE;
            BigDecimal zfz = rpt.getZfz() != null?rpt.getZfz():DEFAULE_VALUE;
            custInfo.setFzzc(hyfz.add(zfz).toString());
            //每月其他债务支出
            BigDecimal yzc = rpt.getYzc() != null?rpt.getYzc():DEFAULE_VALUE;
            custInfo.setYzc(String.valueOf(yzc));
        }
        return custInfo;
	}

	@Override
	public XingYe2001 get2001Date(String scspId) {
		FrontInfoCredit credit=creditService.findCreditInfoById(scspId);
		XingYe2001 xingYe2001=new XingYe2001(credit);
		return xingYe2001;
	}
	//信贷接口：2002保证人信息
	@Override
	public List<XingYe2002> get2002Date(String scspId) {
		List<XingYe2002> list2002=new ArrayList<XingYe2002>();
		List<FrontInfoGuarantor> list = guarantorService.findList(scspId);
		for(int i=0;i<list.size();i++){
			XingYe2002 guarant2002=new XingYe2002(list.get(i));
			guarant2002.setBzxh(String.valueOf(i+1));
			list2002.add(guarant2002);
		}		
		return list2002;
	}

	@Override
	public XingYe2003 get2003Date(String scspId) {
		return null;
	}

}
