package org.yjht.xindai.service;

import java.util.List;

import org.yjht.xindai.common.req.XingYe2000;
import org.yjht.xindai.common.req.XingYe2001;
import org.yjht.xindai.common.req.XingYe2002;
import org.yjht.xindai.common.req.XingYe2003;

/**
 * 个贷对接获取数据
 * @author zhaolulu
 *
 */

public interface XingYeGetDateService {
	
	/**
	 * 根据客户编号获取2000数据信息
	 * @param id
	 * @return
	 */
    XingYe2000 get2000Date(String custId);
    /**
	 * 根据合同编号获取2001数据信息
	 * @param id
	 * @return
	 */
    XingYe2001 get2001Date(String id);
    /**
	 * 根据合同编号获取2002数据信息
	 * @param id
	 * @return
	 */
    List<XingYe2002> get2002Date(String id);
    /**
	 * 根据合同编号获取2003数据信息
	 * @param id
	 * @return
	 */
    XingYe2003 get2003Date(String id);
}
