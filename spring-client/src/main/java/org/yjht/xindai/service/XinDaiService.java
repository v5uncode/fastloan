package org.yjht.xindai.service;

import org.yjht.bean.cust.FrontInfoCustRela;
import org.yjht.core.ReType;
import org.yjht.core.Result;
import org.yjht.xindai.common.resp.XingYeResp2019;

import java.util.Map;

public interface XinDaiService {
    /**
     * 同步客户信息至个贷
     *
     * @param custId
     * @return
     */
    Result syncCustInfo(String custId);

    /**
     * 信贷客户关系人关联
     *
     * @param custId
     * @param rela
     * @return
     */
    Result syncRelaInfo(String custId, FrontInfoCustRela rela);

    /**
     * 信贷放款接口
     *
     * @param scspId 合同号
     * @return 放款日期
     */
    Result oLoanSync(String scspId);

    /**
     * 查询待放款列表
     *
     * @param map
     * @return
     */
    ReType findList(Map<String,String> map);

    /**
     * 查询放款历史
     * @param map
     * @return
     */
    ReType findLoanInfoList(Map<String,String> map);

    /**
     * 根据loanId删除放款记录
     * @param loanId
     * @return
     */
    Result deleteLoanInfo(String loanId);
    
    XingYeResp2019 oCustDere(String zjlx, String zjhm);
}
