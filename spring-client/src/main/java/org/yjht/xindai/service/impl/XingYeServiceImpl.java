package org.yjht.xindai.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpResponse;
import cn.hutool.http.HttpStatus;
import com.github.pagehelper.PageHelper;
import com.yjht.seq.sequence.Sequence;
import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.yjht.bean.FrontInfoLoan;
import org.yjht.bean.FrontInfoScsp;
import org.yjht.bean.FrontInfoScspVo;
import org.yjht.bean.cust.CustBase;
import org.yjht.bean.cust.FrontInfoCustRela;
import org.yjht.bean.scsp.FrontInfoCredit;
import org.yjht.bean.vo.User;
import org.yjht.core.Constants;
import org.yjht.core.ReType;
import org.yjht.core.Result;
import org.yjht.core.SequenceConstants;
import org.yjht.exception.MyException;
import org.yjht.mapper.FrontInfoLoanMapper;
import org.yjht.mapper.FrontInfoScspMapper;
import org.yjht.service.base.AbstractService;
import org.yjht.service.cust.CustBaseService;
import org.yjht.service.scsp.CreditService;
import org.yjht.service.scsp.LoanInfoService;
import org.yjht.util.CommonUtil;
import org.yjht.util.ResultGenerator;
import org.yjht.util.xml.Head;
import org.yjht.util.xml.RespHead;
import org.yjht.util.xml.Response;
import org.yjht.util.xml.XmlUtils;
import org.yjht.xindai.common.enums.EnumOnLoanStatus;
import org.yjht.xindai.common.enums.EnumXyResponseCode;
import org.yjht.xindai.common.req.*;
import org.yjht.xindai.common.resp.XingYeResp2019;
import org.yjht.xindai.service.XinDaiService;
import org.yjht.xindai.service.XingYeGetDateService;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.yjht.util.ResultGenerator.genFailResult;
import static org.yjht.util.ResultGenerator.genSuccessResult;

@Slf4j
@Service
@Scope("singleton")
public class XingYeServiceImpl extends AbstractService<FrontInfoLoan> implements XinDaiService {
    private final String SOURCE = "wd";


    /**
     * 2000信贷客户信息实时同步
     */
    private final String path2000 = "/loan/oCustSync";

    /**
     * 2017信贷客户关系人关联
     */
    private final String path2017 = "/loan/oCustRelate";

    /**
     * 2001信贷合同信息同步(放款接口)
     */
    private final String path2001 = "/loan/oLoanSync";

    /**
     * 2010审批通过以后通过手机银行放款地址
     */
    private final String path2010 = "/loan/oAppLoan";
    
    /**
     * 2018信贷客户信息实时更新
     */
    private final String path2018 = "/loan/oCustUpdate";
    
    /**
 	 * 2019客户信息贷前测评
 	 */
 	private final String path2019 = "/loan/oCustDere";

    @Value("${xingye.loan-url}")
    private String baseUrl;


    @Autowired
    private XingYeGetDateService xingYeGetDateService;
    @Autowired
    private CustBaseService custBaseService;
    @Autowired
    private CreditService creditService;
    @Autowired
    private LoanInfoService loanInfoService;
    @Autowired
    private FrontInfoScspMapper frontInfoScspMapper;
    @Autowired
    private FrontInfoLoanMapper frontInfoLoanMapper;
    @Autowired
    private Sequence sequence;

    @Override
    /**
     * 同步客户信息调用2000上传客户数据   000000 成功 100001 客户信息已存在
     * 若已存在则调用2018接口   客户信息实时更新
     */    
    public Result syncCustInfo(String custId) {  	
        XingYe2000 xy2000 = xingYeGetDateService.get2000Date(custId);
        log.info("2000数据{}", xy2000);
        CustBase cust = custBaseService.findByCustid(custId);
        //组装2000头部信息
        Head xy = new Head();
        xy.setTrxtype("2000");
        xy.setDestinst(cust.getCorpCd());
        xy.setSource(SOURCE);
        xy.setBrn_no(cust.getOrgCd());
        xy.setOpe_no(cust.getCustGrpJl());
        xy.setSeqno(sequence.nextNo(CommonUtil.getUser().getOrgCD(),SequenceConstants.SEQUENCE_PATTERN));
        String reqXml = XmlUtils.req2XML(xy, xy2000);
        String url2000 = baseUrl + path2000;
        String respXml = httpPost(url2000, reqXml);
        if (respXml != null) {
            Response response = XmlUtils.xml2Resp(respXml, null);
            RespHead head = response.getHead();
            if (head.getRet_code().equals(EnumXyResponseCode.SUCCESS.getCode())) {
                //更新客户为正式客户（以存在与个贷）
                updateCustTmp(custId);
                return genSuccessResult();
            } else if (head.getRet_code().equals(EnumXyResponseCode.IS_EXIST.getCode())) {
            	//更新客户为正式客户（以存在与个贷）
                updateCustTmp(custId);
                //客户已存在更新信息
                xy.setTrxtype("2018");
            	XingYe2018 xy2018=new XingYe2018();
            	BeanUtils.copyProperties(xy2000, xy2018);
            	reqXml = XmlUtils.req2XML(xy, xy2018);
            	String url2018 = baseUrl + path2000;
                respXml = httpPost(url2018, reqXml);
                if (respXml != null) {
                    response = XmlUtils.xml2Resp(respXml, null);
                    head = response.getHead();
                    if (head.getRet_code().equals(EnumXyResponseCode.SUCCESS.getCode())) {                  
                        return genSuccessResult();
                    }
                }else{
                	return genFailResult("同步失败");
                }    
                
            } 
            return genFailResult(head.getRet_desc());

        }
        return genFailResult("同步失败");
    }
    /**
     * 同步关系人关联关系
     * 需要先同步客户本人和关系人信息
     */
    @Override
    public Result syncRelaInfo(String custId, FrontInfoCustRela rela) {
        CustBase cust = custBaseService.findByCustid(custId);
        if (cust.getIsTmp().equals(Constants.ISTMP_YES)) {
            CustBase RelaInfo = custBaseService.findByIdNo(rela.getIdType(), rela.getIdNo());
            if (RelaInfo != null) {
                if (RelaInfo.getIsTmp().equals(Constants.ISTMP_YES)) {
                    XingYe2017 xy2017 = new XingYe2017(cust.getIdType(), cust.getIdNo(), rela.getIdType(), rela.getIdNo(), rela.getRelaType());
                    Head xy = new Head();
                    xy.setTrxtype("2017");
                    xy.setDestinst(cust.getCorpCd());
                    xy.setSource(SOURCE);
                    xy.setBrn_no(cust.getOrgCd());
                    xy.setOpe_no(cust.getCustGrpJl());
                    xy.setSeqno(sequence.nextNo(CommonUtil.getUser().getOrgCD(),SequenceConstants.SEQUENCE_PATTERN));
                    String reqXml = XmlUtils.req2XML(xy, xy2017);
                    String url2017 = baseUrl + path2017;
                    String respXml = httpPost(url2017, reqXml);
                    if (respXml != null) {
                        Response<Object> response = XmlUtils.xml2Resp(respXml, null);
                        RespHead head = response.getHead();
                        if (head.getRet_code().equals(EnumXyResponseCode.SUCCESS.getCode())) {
                            return genSuccessResult();
                        } else {
                            return genFailResult(head.getRet_desc());
                        }
                    }
                    return genFailResult("同步失败");
                } else {
                    return genFailResult("请先同步该关系人信息至个贷");
                }
            } else {
                return genFailResult("关系人信息不存在,请新增后同步");
            }
        } else {
            return genFailResult("请先同步客户信息至个贷");
        }
    }

    /**
     * 信贷放款接口
     * 1. 查询放款表，当前合同今日是否存在放款且成功
     * 2. 根据合同号查询授信方案
     * 3. 拼装数据，请求兴业
     * 4.1 放款成功更新放款记录表
     * 4.2 放款失败，返回100006 继续调用 2010接口
     *
     * @param scspId 合同号
     * @return 放款日期
     */
    @Override
    @Transactional
    public synchronized Result oLoanSync(String scspId) {
        List<FrontInfoLoan> loanList = loanInfoService.findLoanInfoByScspId(scspId);
        if (CollUtil.isNotEmpty((loanList))) {
            for (FrontInfoLoan infoLoan : loanList) {
                if (EnumOnLoanStatus.SUCCESS.getCode().equals(infoLoan.getLoanStatus())
                        && DateUtil.now().equals(infoLoan.getLoanDate())) {
                    return genFailResult(String.format("当前合同放款状态为成功：%s", scspId));
                }
            }
        }

        FrontInfoCredit credit = creditService.findCreditInfoById(scspId);
        if (credit == null) {
            return genFailResult(String.format("授信方案信息不存在，合同编号：%s", scspId));
        }

        User user = CommonUtil.getUser();
        Head head = new Head();
        head.setTrxtype("2001");
        head.setDestinst(user.getCorpCD());
        head.setSource(SOURCE);
        head.setSeqno(sequence.nextNo(CommonUtil.getUser().getOrgCD(),SequenceConstants.SEQUENCE_PATTERN));
        head.setBrn_no(user.getOrgCD());
        head.setOpe_no(user.getUser_id());
        String reqXml = XmlUtils.req2XML(head, new XingYe2001(credit));
        System.err.println(reqXml);
        String respXml = httpPost(baseUrl + path2001, reqXml);
        Response<Object> response = XmlUtils.xml2Resp(respXml, null);
        if (response == null || response.getHead() == null || response.getBody() == null) {
            loanInfoService.insertLoanInfo(credit, EnumOnLoanStatus.ERROR.getCode());
            return genFailResult("请求兴业报文响应异常");
        }

        RespHead respHead = response.getHead();
        if (EnumXyResponseCode.SUCCESS.getCode().equals(respHead.getRet_code())) {
            loanInfoService.insertLoanInfo(credit, EnumOnLoanStatus.SUCCESS.getCode());
            return genSuccessResult();
        }

        if (EnumXyResponseCode.YES_CONTRACT_NO.getCode().equals(respHead.getRet_code())) {
            head.setTrxtype("2010");
            head.setSeqno(sequence.nextNo(CommonUtil.getUser().getOrgCD(),SequenceConstants.SEQUENCE_PATTERN));
            reqXml = XmlUtils.req2XML(head, new XingYe2010(credit.getScspId(), credit.getIdType(), credit.getIdNo()));
            respXml = httpPost(baseUrl + path2010, reqXml);
            response = XmlUtils.xml2Resp(respXml, null);
            respHead = response.getHead();
            if (EnumXyResponseCode.SUCCESS.getCode().equals(respHead.getRet_code())) {
                loanInfoService.insertLoanInfo(credit, EnumOnLoanStatus.SUCCESS.getCode());
                return genSuccessResult();
            }
        }
        loanInfoService.insertLoanInfo(credit, EnumOnLoanStatus.ERROR.getCode());
        return genFailResult(respHead.getRet_desc());
    }
    @Override
 	public XingYeResp2019 oCustDere(String zjlx, String zjhm) {
 		XingYeResp2019 xyrsp2019=null;
 		User user=CommonUtil.getUser();
 		Head xy = new Head();
 	    xy.setTrxtype("2019");
 	    xy.setDestinst(user.getCorpCD());
 	    xy.setSource(SOURCE);
 	    xy.setBrn_no(user.getOrgCD());
 	    xy.setOpe_no(user.getUser_id());
 	    xy.setSeqno(sequence.nextNo(CommonUtil.getUser().getOrgCD(),SequenceConstants.SEQUENCE_PATTERN));
 	    XingYe2019 xy2019=new XingYe2019(zjlx,zjhm);
 	    String reqXml = XmlUtils.req2XML(xy, xy2019);
 	    String url2017 = baseUrl + path2019;
 	    String respXml = httpPost(url2017, reqXml);
 	    if (respXml != null) {
 	    	Response<XingYeResp2019> response = XmlUtils.xml2Resp(respXml, XingYeResp2019.class);
 	        RespHead head = response.getHead();
 	        if (head.getRet_code().equals(EnumXyResponseCode.SUCCESS.getCode())) {
 	        	xyrsp2019 = response.getBody();
 	        }else{
 	            log.error("个贷贷前测评2019接口失败，原因：{}",head.getRet_desc());
 	        } 
 	    }else{
 	       log.error("个贷贷前测评2019接口连接失败");
 	    }
 	    return xyrsp2019;
 	}

    /**
     * 查询待放款列表
     * 1. 审查审批流程结束并且通过的
     * 2. 放款失败的
     *
     * @param params
     * @return
     */
    @Override
    public ReType findList(Map<String, String> params) {
    	params.put("processState", Constants.STATYJS);// 审查审批流程结束
    	params.put("spResult", "T");//审查审批流程通过的
    	params.remove("_");	
        PageHelper.startPage(Integer.parseInt(params.get("page")), Integer.parseInt(params.get("limit")));
        params.remove("page");
    	params.remove("limit");
        Example example = new Example(FrontInfoScsp.class);
        Example.Criteria criteria = example.createCriteria();
        for (Map.Entry<String, String> entry : params.entrySet()) {
        	if(StringUtils.isNotBlank(entry.getValue())){
        		criteria.andEqualTo(entry.getKey(), entry.getValue());
        	}
        }
        example.setOrderByClause("CRT_DATE DESC");
        List<FrontInfoScsp> scspList = frontInfoScspMapper.selectByExample(example);
        List<FrontInfoScspVo> scspVoList = new ArrayList<>();
        for (FrontInfoScsp scsp : scspList) {
            List<FrontInfoLoan> loanList = loanInfoService.findLoanInfoByScspId(scsp.getScspId());
            FrontInfoScspVo scspVo = new FrontInfoScspVo();
            BeanUtils.copyProperties(scsp, scspVo);
            if (CollUtil.isNotEmpty(loanList)) {
                scspVo.setLoanStatus(loanList.get(0).getLoanStatus());
            }
            scspVoList.add(scspVo);
        }
        return new ReType(scspVoList.size(), scspVoList);
    }

    /**
     * 查询放款历史
     *
     * @param map
     * @return
     */
    @Override
    public ReType findLoanInfoList(Map<String, String> map) {
        PageHelper.startPage(Integer.parseInt(map.get("page")), Integer.parseInt(map.get("limit")));
        Example example = new Example(FrontInfoLoan.class);
        example.setOrderByClause("CRT_DATE DESC");
        example.createCriteria().andEqualTo("scspId", map.get("scspId"));
        List<FrontInfoLoan> frontInfoLoanList = frontInfoLoanMapper.selectByExample(example);
        return new ReType(frontInfoLoanList.size(), frontInfoLoanList);
    }

    /**
     * 根据loanId删除放款记录
     *
     * @param loanId
     * @return
     */
    @Override
    public Result deleteLoanInfo(String loanId) {
        frontInfoLoanMapper.deleteByPrimaryKey(loanId);
        return ResultGenerator.genSuccessResult();
    }

    /**
     * 更新客户是否是正式客户（同步到个贷）
     *
     * @param custId
     */
    private void updateCustTmp(String custId) {
        CustBase custInfo = new CustBase();
        custInfo.setCustId(custId);
        //是否是正式客户 （同步到个贷）
        custInfo.setIsTmp(Constants.ISTMP_YES);
        custBaseService.update(custInfo);
    }

    /**
     * 发送http报文请求
     *
     * @param url
     * @param reqXml
     * @return
     */
    private String httpPost(String url, String reqXml) {
        log.info("请求兴业报文：{}", reqXml);
        String result = null;
        try {

            HttpResponse response = HttpRequest.post(url).timeout(10000)
                    .contentType("text/plain; charset=utf-8")
                    .body(reqXml)
                    .execute();
            log.info("请求兴业响应报文：{}", response.body());
            if (response.getStatus() == HttpStatus.HTTP_OK) {
                result = response.body();
            }
        } catch (Exception e) {
            log.error("请求兴业异常", e);
        }
        return result;
    }

}
