package org.yjht.xindai.common.req;

import lombok.Getter;
import lombok.Setter;

/**
 * 信贷接口：2004质押信息
 */
@Getter
@Setter
public class XingYe2004 {
    //贷款合同号
    private String hth;
    //质押序号
    private String zyxh;
    //抵押合同号
    private String zyhth;
    //是否第三方质押
    private String sfdsf;
    //抵押人
    private String zyrxm;
    //证件类型
    private String zjlx;
    //证件号码
    private String zjh;
    //质物种类
    private String zywzl;
    //质物面值
    private String zymz;
    //质物比率
    private String zybl;
    //担保主债权(质押金额)
    private String zyje;
    //单证号码
    private String dzh;
}
