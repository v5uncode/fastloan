package org.yjht.xindai.common.enums;

/**
 * @author lengleng
 * 兴业返回码枚举
 */
public enum EnumXyResponseCode {
    SUCCESS("000000", "成功"),
    ERROR("100000", "失败"),
    IS_EXIST("100001", "客户信息已存在"),
    NO_EXIST("100002", "客户信息不存在"),
    NO_ORGCD("100003", "合作机构未注册"),
    TELE_ERROR("100005", "合作机构未注册"),
    YES_CONTRACT_NO("100006", "外部合同号重复"),
    NO_CONTRACT_NO("100007", "外部合同号不存在"),
    EXCEPTION("900000", "处理异常");

    private String code;
    private String desc;


    EnumXyResponseCode(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

}
