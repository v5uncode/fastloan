package org.yjht.xindai.common.req;

import lombok.Getter;
import lombok.Setter;

/**
 * 信贷接口：2010手机银行放款
 * 审批通过以后通过手机银行放款
 * @author zhanghua
 * @date 2018/03/22
 */
@Getter
@Setter
public class XingYe2010 {
    //贷款合同号
    private String hth;
    //证件类型
    private String zjlx;
    //证件号码
    private String zjhm;
	public XingYe2010(String hth, String zjlx, String zjhm) {
		super();
		this.hth = hth;
		this.zjlx = zjlx;
		this.zjhm = zjhm;
	}
	public XingYe2010() {
		super();
	}
    
    
    
}
