package org.yjht.xindai.common.req;

import java.text.ParseException;

import org.apache.commons.lang3.StringUtils;
import org.yjht.bean.cust.CustBase;
import org.yjht.bean.cust.CustFamily;
import org.yjht.bean.cust.FrontInfoCustStaff;
import org.yjht.core.Constants;
import org.yjht.util.DateTools;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 *  信贷接口：2000信贷客户信息实时同步实体
 * @author zhanghua
 */
@Getter
@Setter
@ToString
public class XingYe2000 {
    private String fhh;   //所属分行
    private String zjlx;   //证件类型
    private String zjhm;   //证件号码
    private String khmc;   //客户名称
    private String zhh;   //所属支行
    private String xb;   //性 别
    private String mz;   //民 族
    private String hyzk;   //婚姻状况  
    private String hjxz;   //户籍性质
    private String hjdz;   //户籍所在地
    private String zgxl;   //最高学历
    private String zgxw;   //最高学位
    private String jkzk;   //健康状况 
    private String zy;   //职业   
    private String zc = "9";   //职称 9:未知
    private String zw = "9";   //职务 9:未知
    private String gwxz = "1";   //岗位性质  1:普通工作人员
    private String gznx = "0000";   //目前工作持续年限
    private String jznx;   //本地持续居住年限
    private String jtrk;   //家庭人口
    private String jtzz;   //家庭住址
    private String zzxz;   //住宅性质
    private String jtyb;   //家庭邮编
    private String jtdh;   //家庭电话
    private String sj;   //手机
    private String dwxz="9";   //单位性质
    private String gzdw = "1";   //工作单位
    private String gsgm = "1";   //公司规模  1:500人以上
    private String hy = "Z";   //行业 Z:未知
    private String dwdz="1";   //单位地址
    private String khfl="0";   //客户分类
    private String sfbhyg="0";   //是否本行员工
    
    private String ysr = "0";   //个人月收入
    private String jtsr = "0";   //家庭月收入
    private String jtwdsr = "0";   //家庭稳定收入(月)
    private String jtzj = "0";   //家庭租金收入(月)
    private String jtqtsr = "0";   //家庭其它收入(月)
    private String jtfh = "0";   //家庭投资分红(月)
    private String jrzc = "0";   //金融资产
    private String qtzc = "0";   //其它资产及投资
    private String gdzc = "0";   //固定资产
    private String fzzc = "0";   //负债及或有负债
    private String yzc = "0";   //每月其他债务支出
    private String kjrzjh;   //借款人证件号
    private String gxrzjh;   //关系人证件号
    private String gx;   //关系
    
    public void setCustBase(CustBase base){
    	this.fhh = base.getOrgCd();
    	this.zjlx = base.getIdType();
    	this.zjhm = base.getIdNo();
    	this.khmc = base.getCustName();
    	this.zhh = base.getOrgCd();   	
    	this.xb = base.getSex();
    	this.mz = base.getNation();
    	this.hjxz = base.getHjxz();
    	this.hjdz = base.getHjdz();
    	this.zgxl = base.getEduLevel();
    	this.zgxw = base.getHigestDegree();
    	this.jkzk = base.getJkzk();
    	this.zy = base.getVocation();
    	this.jznx = base.getJznx();
    	this.jtzz = base.getAddress();
    	this.sj = base.getTelNo();
    	this.gsgm = base.getGsgm();
    	this.hy = base.getHy();
    	this.khfl = base.getKhfl();
    	this.sfbhyg = base.getParty().equals(Constants.YES)?"1":"0";    	
    	this.kjrzjh = base.getIdNo();
    }
    public void setFamily(CustFamily family){
    	this.hyzk = family.getMarriageFlag();
    	this.jtrk = family.getFamilyCount() == null ? "1":family.getFamilyCount().toString();
    	this.zzxz = family.getLiveStatus();
    	this.jtyb = family.getFamilyPostcode();
    	this.jtdh = family.getFamilyPhone();
    }
    
    public void setStaff(FrontInfoCustStaff staff){
    	this.zc = staff.getProfessTitle()==null? "9":staff.getProfessTitle();
    	this.zw = staff.getDuty() == null ? "9":staff.getDuty();
    	this.gwxz = staff.getGwxz() == null ? "1":staff.getGwxz();
    	this.dwxz = staff.getDwxz() == null? "9":staff.getDwxz();
    	String gznx="0000";
    	if(StringUtils.isNoneBlank(staff.getWorkDate())){
    		gznx=DateBetween(staff.getWorkDate());
    	}	
    	this.gznx = gznx;
    	this.gzdw = staff.getWorkName() ==null ? "1":staff.getWorkName();
    	this.dwdz = staff.getWorkAddress()== null ? "1":staff.getWorkAddress();   	    	
    }
    //组装兴业所需年限数据  格式 0101(yymm)  前两位代表年数，后两位代表月数   
    public String DateBetween(String start){
    	String result=null;
    	String end=DateTools.getCurrentSysData(DateTools.DEFAULT_FORMAT);
    	try {
			int months=DateTools.monthsBetween(start, end);
			String year = String.valueOf(months/12);
			String month = String.valueOf(months%12);
			year = year.length()==1?("0"+year):year;
			month = month.length()==1?("0"+month):month;
			result =year+month;
		} catch (ParseException e) {
			result = "0000";
		}
    	return result;
    }
}
