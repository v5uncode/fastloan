package org.yjht.xindai.common.req;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 信贷接口：2019客户在贷款之前查询相关贷款的信息
 * @author lengleng
 * @date 2018/0/22
 */
@Getter
@Setter
@ToString
public class XingYe2019 {
	//证件类型
 	private String zjlx;
 	//证件号码
 	private String zjhm;
 	public XingYe2019() {
 		super();
 	}
 	public XingYe2019(String zjlx, String zjhm) {
 		super();
 		this.zjlx = zjlx;
 		this.zjhm = zjhm;
 	}
}
