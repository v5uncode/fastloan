package org.yjht.xindai.common.resp;

import lombok.Getter;
import lombok.Setter;

/**
 * 信贷接口：2019客户在贷款之前查询相关贷款的信息
 * 返回报文
 * @author lengleng
 * @date 2018/5/31
 *
 */
@Setter
@Getter
public class XingYeResp2019 {
	/**
 	 * 是否新客户
 	 */
 	private String sfxkh;
 	/**
 	 * 最差五级分类
 	 */
 	private String zcwjfl;
 	/**
 	 * 是否正式客户
 	 */
 	private String sfzskh;
 	/**
 	 * 是否有家庭成员
 	 */
 	private String sfyqs;
 	/**
 	 * 家庭成员是否有未结清贷款
 	 */
 	private String qswjq;
 	/**
 	 * 是否有授信
 	 */
 	private String sfsx;
 	/**
 	 * 授信期限
 	 */
 	private String sxqx;
 	/**
 	 * 授信额度
 	 */
 	private String sxed;
 	/**
 	 * 身份是否唯一
 	 */
 	private String sfwy;
 	/**
 	 * 未结清贷款账户数
 	 */
 	private String wjqs;
 	/**
 	 * 是否存在本息逾期记录
 	 */
 	private String sfbxyq;
 	/**
 	 * 近12个月是否展期
 	 */
 	private String seyzq;
 	/**
 	 * 本息最后一次逾期距离现在的月份数
 	 */
 	private String zhyqys;
 	/**
 	 * 近6个月本息逾期次数
 	 */
 	private String lybxcs;
 	/**
 	 * 近6个月本息最大逾期期数
 	 */
 	private String lybxqs;
 	/**
 	 * 近12个月本息逾期次数
 	 */
 	private String sebxcs;
 	/**
 	 * 近12个月本息最大逾期期数
 	 */
 	private String sebxzqs;
 	/**
 	 * 近24个月本息逾期次数
 	 */
 	private String esbxcs;
 	/**
 	+	 * 近24个月本息最大逾期期数
 	+	 */
 	private String esbxqs;
 	/**
 	 * 近36个月本息逾期次数
 	 */
 	private String slbxcs;
 	/**
 	 * 近36个月本息最大逾期期数
 	 */
 	private String sllxcq;
}
