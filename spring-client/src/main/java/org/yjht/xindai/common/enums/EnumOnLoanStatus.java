package org.yjht.xindai.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author lengleng
 * @date 2018/6/4
 * 放款状态
 */
@AllArgsConstructor
public enum EnumOnLoanStatus {
    /**
     * 放款成功
     */
    SUCCESS("1", "成功"),
    /**
     * 放款失败
     */
    ERROR("9", "失败");

    @Getter
    private String code;
    @Getter
    private String desc;
}
