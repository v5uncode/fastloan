package org.yjht.xindai.common.req;

import org.yjht.bean.scsp.FrontInfoGuarantor;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 信贷接口：2002保证人信息
 * @author zhanghua
 * @date 2018/03/22
 */
@Getter
@Setter
@ToString
public class XingYe2002 {
    //贷款合同号
    private String hth;
    //保证序号
    private String bzxh;
    //保证合同号
    private String bzhth;
    //保证人类型
    private String bzrlx="1";
    //保证人证件号
    private String zjh;
    //保证合作项目
    private String hzxmh="999999999999";
    //保证金额
    private String bzj="20";
	public XingYe2002() {
		super();
	}
	public XingYe2002(FrontInfoGuarantor guarantor) {
		super();
		this.hth = guarantor.getScspId();
		this.bzhth = guarantor.getId();
		this.zjh = guarantor.getIdCard();
		//this.bzj = bzj;
	}
	
    
    
}
