package org.yjht.xindai.common.req;

import org.apache.commons.lang3.StringUtils;
import org.yjht.bean.scsp.FrontInfoCredit;
import org.yjht.enmus.WarrantType;
import org.yjht.util.DateTools;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 信贷接口：2001信贷合同信息
 *
 * @author zhanghua
 * @date 2018/03/22
 */
@Getter
@Setter
@ToString
public class XingYe2001 {
    //合同号
    private String hth;
    //证件号
    private String zjh;
    //机构号
    private String jgh="0001";
    //放款日期
    private String fkr;
    //到期日
    private String dqr;
    //贷款品种
    private String pz;
    //放款金额
    private String fkje;
    //放款期限
    private String fkqx;
    //还款方式
    private String hkfs;
    //还款间隔
    private String hkjg;
    //还款日
    private String hkr;
    //基准利率
    private String jzll;
    //利率浮动率
    private String fdbl;
    //执行利率
    private String zxll;
    //利率调整方式
    private String lltzfs;
    //罚息比率
    private String fxbl;
    //罚息利率
    private String fxll;
    //单复率标志
    private String dflbz;
    //行业投向
    private String hytx;
    //借款人属性
    private String jkrsx;
    //宽限期方式
    private String kxqbz;
    //宽限期天数
    private String kxts;
    //客户经理号
    private String khjlh;
    //贷款用途
    private String dkyt;
    //贷款用途描述
    private String dkms;
    //担保方式
    private String dbfs;
    //房屋详细地址
    private String fwdz;
    //建筑面积
    private String jzmj;
    //购买总价位
    private String gmzj;
    //首付金额
    // TODO: 2018/3/22 首付金额 重复字段
    //private String sfje;
    //房屋类型
    private String fwlx;
    //交易类别
    private String jylb;
    //购房原因
    private String gfyy;
    //房屋单价
    private String fwdj;
    //首付比例
    // TODO: 2018/3/22  首付比例 重复字段 
    //private String sfbl;
    //房屋性质
    private String fwxz;
    //购车总价
    private String gczj;
    //首付金额
    private String sfje;
    //首付比例
    private String sfbl;
    //汽车品牌
    private String qcpp;
    //一手车/二手车
    private String qcxjbz;
    //汽车型号
    private String qcxh;
    //发动机号
    private String fdjxh;
    //汽车地盘号
    private String qcdpxh;
    //汽车类型
    private String qclx;
    //汽车用途
    private String qcyt;
    //支付方式
    private String zffs;
    //财政贴息标志
    private String cztxbz;
    //财政贴息帐号
    private String cztxzh;
    //贴息比例
    private String cztxbl;
    //公积金贷款标志
    private String gjjbz;
    //公积金还本账号
    private String gjjhbzh;
    //公积金还息账号
    private String gjjhxzh;
    //公积金中心企业代码
    private String gjjqydm;
    //放款账户
    private String fkzh;
    //还款账户
    private String hkzh;
    //贴息标志
    private String shtxbz;
    //贴息帐号
    private String shtxzh;
    //贴息比例
    private String shtxbl;

    public XingYe2001() {
        super();
    }

    public XingYe2001(FrontInfoCredit credit) {
        super();
        this.hth = credit.getScspId();
        this.zjh = credit.getIdNo();
        this.fkr = DateTools.formatDate(DateTools.parseDate(credit.getStartDate()),"yyyyMMdd");
        this.dqr = DateTools.formatDate(DateTools.parseDate(credit.getEndDate()),"yyyyMMdd");
        this.pz = credit.getLoanType().split(",")[1];
        this.fkje = credit.getGrantMoney()==null?"0":credit.getGrantMoney();
        String fkqx="0000";
        if(StringUtils.isNoneBlank(credit.getGrantTerm())){
        	fkqx=getFkqx(credit.getGrantTerm());
        }
        this.fkqx = fkqx;//放款期限
        this.hkfs = credit.getRepaymentWay();
        this.hkjg = credit.getRepaymentInterval();
        this.hkr = credit.getRepaymentDate();
        this.jzll = credit.getBaseRate();
        this.fdbl = credit.getRateFloat();
        this.zxll = credit.getExecuteRate();
        this.lltzfs = credit.getRateCycle();
        this.fxbl = credit.getDefaultInterestFloat()==null?"0":credit.getDefaultInterestFloat();
        this.fxll = credit.getDefaultInterestRate();
        this.dflbz = credit.getSingleRateSign();
        this.hytx = "U";
        this.jkrsx = credit.getLoanerAttribute();
        this.kxqbz = credit.getGracePeriodWay();
        this.kxts = credit.getGracePeriod()==null?"0":credit.getGracePeriod();
        this.khjlh = credit.getCustGrpJl();
        this.dkyt = credit.getLoanPurpose();
        this.dkms = credit.getLoanPurposeDetail();
        String dbfs="";
        String warrant=credit.getGuaranteeWay();
        if(warrant.contains(WarrantType.XY.getCode())){//信用
        	dbfs+="1";
        }else{
        	dbfs+="0";
        }
        if(warrant.contains(WarrantType.BZ.getCode())){//保证
        	dbfs+="1";
        }else{
        	dbfs+="0";
        }
        if(warrant.contains(WarrantType.DY.getCode())){//抵押
        	dbfs+="1";
        }else{
        	dbfs+="0";
        }
        if(warrant.contains(WarrantType.ZY.getCode())){//质押
        	dbfs+="1";
        }else{
        	dbfs+="0";
        }
        dbfs+="0";//履约默认无
        this.dbfs = dbfs;//担保方式需要修改
        this.fkzh = credit.getGrantAccount();
        this.hkzh = credit.getRepaymentAccount();
    }
    private static String getFkqx(String fkqx){
    	String [] fkqxs=fkqx.split(",");
    	if(fkqxs[0].length()==1){
    		fkqxs[0]="0"+fkqxs[0];
    	}
    	if(fkqxs[1].length()==1){
    		fkqxs[1]="0"+fkqxs[1];
    	}
    	if(fkqxs[2].length()==1){
    		fkqxs[2]="0"+fkqxs[2];
    	}
    	return fkqxs[0]+fkqxs[1]+fkqxs[2];
    }
}
