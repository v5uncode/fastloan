package org.yjht.xindai.common.req;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 2007逾期主动还款
 * 逾期客户主动还款
 * @author zhaolulu
 *
 */
@Setter
@Getter
@ToString
public class XingYe2007 {
	/**
	 * 贷款合同
	 */
	private String hth;
	/**
	 * 证件类型
	 */
	private String zjlx;
	/**
	 * 证件号码
	 */
	private String zjhm;
	/**
	 * 还款金额
	 */
	private String hkje;
	/**
	 * 还款账号
	 */
	private String hkzh;
	/**
	 * 实际还款日
	 */
	private String hkrq;
	

}
