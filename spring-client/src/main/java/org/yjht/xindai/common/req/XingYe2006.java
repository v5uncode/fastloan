package org.yjht.xindai.common.req;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class XingYe2006 {
	/**
	 * 贷款合同 放款请求的外部合同号
	 */
	private String hth;
	/**
	 * 证件类型
	 */
	private String zjlx;
	/**
	 * 证件号码
	 */
	private String zjhm;

}
