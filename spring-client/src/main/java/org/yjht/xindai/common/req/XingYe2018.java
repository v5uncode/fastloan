package org.yjht.xindai.common.req;

import lombok.Getter;
import lombok.Setter;

/**
 * 2018信贷客户信息实时更新
 * @author zhaolulu
 *
 */
@Getter
@Setter
public class XingYe2018 {
	private String fhh;   //所属分行
    private String zjlx;   //证件类型
    private String zjhm;   //证件号码
    private String khmc;   //客户名称
    private String zhh;   //所属支行
    private String xb;   //性 别
    private String mz;   //民 族
    private String hyzk;   //婚姻状况  
    private String hjxz;   //户籍性质
    private String hjdz;   //户籍所在地
    private String zgxl;   //最高学历
    private String zgxw;   //最高学位
    private String jkzk;   //健康状况 
    private String zy;   //职业   
    private String zc = "9";   //职称 9:未知
    private String zw = "9";   //职务 9:未知
    private String gwxz = "1";   //岗位性质  1:普通工作人员
    private String gznx = "0000";   //目前工作持续年限
    private String jznx;   //本地持续居住年限
    private String jtrk;   //家庭人口
    private String jtzz;   //家庭住址
    private String zzxz;   //住宅性质
    private String jtyb;   //家庭邮编
    private String jtdh;   //家庭电话
    private String sj;   //手机
    private String dwxz="9";   //单位性质
    private String gzdw = "1";   //工作单位
    private String gsgm = "1";   //公司规模  1:500人以上
    private String hy = "Z";   //行业 Z:未知
    private String dwdz="1";   //单位地址
    private String khfl="0";   //客户分类
    private String sfbhyg="0";   //是否本行员工
    
    private String ysr = "0";   //个人月收入
    private String jtsr = "0";   //家庭月收入
    private String jtwdsr = "0";   //家庭稳定收入(月)
    private String jtzj = "0";   //家庭租金收入(月)
    private String jtqtsr = "0";   //家庭其它收入(月)
    private String jtfh = "0";   //家庭投资分红(月)
    private String jrzc = "0";   //金融资产
    private String qtzc = "0";   //其它资产及投资
    private String gdzc = "0";   //固定资产
    private String fzzc = "0";   //负债及或有负债
    private String yzc = "0";   //每月其他债务支出
}
