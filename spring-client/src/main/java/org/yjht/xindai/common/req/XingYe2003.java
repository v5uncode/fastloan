package org.yjht.xindai.common.req;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 *  信贷接口：2003抵押信息
 */
@Getter
@Setter
@ToString
public class XingYe2003 {
    //贷款合同号
    private String hth;
    //抵押序号
    private String dyxh;
    //抵押合同号
    private String dyhth;
    //抵押物类型
    private String dylx;
    //抵押物权属人信息
    private String dyqs;
    //证件类型
    private String zjlx;
    //证件号码
    private String zjh;
    //评估价值
    private String pfjz;
    //抵押比率%
    private String dybl;
    //抵押价值
    private String dyjz;
    //权属人与借款人关系
    private String dywqsgx;
    //抵押物所有权属
    private String dywsyq;
    //抵押物(不动产)位置
    private String dywwz;
    //房屋面积(平方米)
    private String dymj;
    //抵押机关
    private String dyjg;
    //抵押物(动产)型号
    // TODO: 2018/3/21    dyxh 字段重复
    //private String dyxh;
    //发动机号
    private String fdjxh;
    //汽车底盘号
    private String qcdph;
    //车牌号
    private String cph;
    //评估机构
    private String pgjg;
    //他项权利证
    private String txqlz;
}
