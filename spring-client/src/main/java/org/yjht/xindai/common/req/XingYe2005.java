package org.yjht.xindai.common.req;

import lombok.Getter;
import lombok.Setter;

/**
 * 信贷接口：2005放款结果查询
 * @author zhanghua
 * @date 2018/03/22
 */
@Getter
@Setter
public class XingYe2005 {
    //贷款合同号
    private String hth;
    //证件类型
    private String zjlx;
    //证件号码
    private String zjhm;

}
