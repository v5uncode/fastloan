package org.yjht.xindai.common.req;

import lombok.Getter;
import lombok.Setter;

/**
 * 2009合同状态查询
 * 查询个贷合同审批结果
 * @author zhaolulu
 *
 */
@Setter
@Getter
public class XingYe2009 {
	/**
	 * 信贷合同号
	 */
	private String xdhth;
	/**
	 * 证件类型
	 */
	private String zjlx;
	/**
	 * 证件号码
	 */
	private String zjhm;

}
