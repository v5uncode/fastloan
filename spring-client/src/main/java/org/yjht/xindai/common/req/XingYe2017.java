package org.yjht.xindai.common.req;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 信贷接口：2017信贷客户关系人关联
 * @author lengleng
 * @date 2018/0/22
 */
@Getter
@Setter
@ToString
public class XingYe2017 {
    //借款人证件类型
    private String kjrzjlx;
    //借款人证件号
    private String kjrzjh;
    //关系人证件类型
    private String gxrzjlx;
    //关系人证件号
    private String gxrzjh;
    //关系
    private String gx;
	public XingYe2017() {
		super();
	}
	public XingYe2017(String kjrzjlx, String kjrzjh, String gxrzjlx, String gxrzjh, String gx) {
		super();
		this.kjrzjlx = kjrzjlx;
		this.kjrzjh = kjrzjh;
		this.gxrzjlx = gxrzjlx;
		this.gxrzjh = gxrzjh;
		this.gx = gx;
	}
    
}
