package org.yjht.xindai.constroller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.yjht.aop.SystemControllerLog;
import org.yjht.bean.cust.FrontInfoCustRela;
import org.yjht.core.Query;
import org.yjht.core.ReType;
import org.yjht.core.Result;
import org.yjht.xindai.service.XinDaiService;

import java.util.Map;

@RestController
@RequestMapping("/")
public class XinDaiConstroller {

    @Autowired
    private XinDaiService xinDaiService;

    @PostMapping(value = "syncCust/{custId}")
    @SystemControllerLog(description = "同步客户信息")
    public Result syncCustInfo(@PathVariable String custId) {
        return xinDaiService.syncCustInfo(custId);
    }

    @PostMapping(value = "syncRela/{custId}")
    @SystemControllerLog(description = "信贷客户关系人关联")
    public Result syncRelaInfo(@PathVariable String custId, @RequestBody FrontInfoCustRela rela) {
        return xinDaiService.syncRelaInfo(custId, rela);
    }

    /**
     * 兴业信贷放款接口
     *
     * @param scspId 合同编号
     * @return 放款日期
     */
    @GetMapping(value = "oLoanSync/{scspId}")
    @SystemControllerLog(description = "信贷合同信息同步放款")
    public Result oLoanSync(@PathVariable String scspId) {
        return xinDaiService.oLoanSync(scspId);
    }

    /**
     * 分页查询待放款列表
     *
     * @param map 查询参数
     * @return
     */
    @GetMapping("loanPage")
    @ResponseBody
    @SystemControllerLog(description = "分页查询待放款列表")
    public ReType loanPage(@RequestParam Map<String, String> map) {
        return xinDaiService.findList(map);
    }

    /**
     * 分页查询放款历史列表
     *
     * @param map 查询参数
     * @return
     */
    @GetMapping("/loanInfoPage")
    @ResponseBody
    @SystemControllerLog(description = "分页查询放款历史列表")
    public ReType loanInfoPage(@RequestParam Map<String, String> map) {
        return xinDaiService.findLoanInfoList(map);
    }
    /**
     * 删除放款记录
     * @param loanId
     * @return
     */
    @DeleteMapping("/loanInfo/{loanId}")
    public Result loanInfoDel(@PathVariable String loanId) {
        return xinDaiService.deleteLoanInfo(loanId);
    }
}
