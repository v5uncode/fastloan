package org.yjht.service.ruleDef.impl;


import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.yjht.bean.RuleDef;
import org.yjht.bean.vo.User;
import org.yjht.core.Page;
import org.yjht.core.Query;
import org.yjht.core.Result;
import org.yjht.core.TableResultResponse;
import org.yjht.mapper.RuleDefMapper;
import org.yjht.service.base.AbstractService;
import org.yjht.service.ruleDef.RuleDefService;
import org.yjht.util.CommonUtil;
import org.yjht.util.DateTools;
import org.yjht.util.ResultGenerator;
import org.yjht.util.StringUtil;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

@Service
public class RuleDefServiceImpl extends AbstractService<RuleDef> implements RuleDefService {
	
	@Autowired
	RuleDefMapper ruleDefMapper;
	@Override
	public Result queryList(Query query) {
		User user=CommonUtil.getUser();
		query.put("corpCd", user.getCorpCD());
		TableResultResponse<RuleDef> response = findByQuery(query);
		return ResultGenerator.genSuccessResult(response);
	}
	
	@Override
	public Result saveRuleDef(RuleDef ruleDef) {
		ruleDef.setRuleNo(StringUtil.getUUID());
		User user=CommonUtil.getUser();
		ruleDef.setCorpCd(user.getCorpCD());
		ruleDef.setPubDate(DateTools.getCurrentSysData(DateTools.FULLTIME_FORMAT));	
		save(ruleDef);
		return ResultGenerator.genSuccessResult("保存成功");
	}
	
	@Override
	public Result updatRuleDef(RuleDef ruleDef,String id) {
		ruleDef.setMtnDate(DateTools.getCurrentSysData(DateTools.SIMPLE_FORMAT));
		ruleDef.setRuleNo(id);
		update(ruleDef);
		return ResultGenerator.genSuccessResult("修改成功");
	}

	@Override
	public Result deleteRuleDef(String ruleNo) {
		deleteById(ruleNo);
		return ResultGenerator.genSuccessResult("删除成功");
	}


}
