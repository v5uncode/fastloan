package org.yjht.service.ruleDef;

import org.yjht.bean.RuleDef;
import org.yjht.core.Query;
import org.yjht.core.Result;

public interface RuleDefService {

	Result queryList(Query query);

	Result saveRuleDef(RuleDef ruleDef);

	Result updatRuleDef(RuleDef ruleDef,String id);

	Result deleteRuleDef(String ruleNo);

	
}
