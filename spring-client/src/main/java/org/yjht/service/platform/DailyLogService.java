package org.yjht.service.platform;

import javax.servlet.http.HttpServletRequest;

import org.yjht.bean.DailyLog;
import org.yjht.core.Result;

public interface DailyLogService {

	Result saveDailylog(HttpServletRequest request, DailyLog dailylog);

	Result queryDailylog(HttpServletRequest request, DailyLog dailylog);

	Result deleteDailylog(HttpServletRequest request, DailyLog dailylog);
	
}
