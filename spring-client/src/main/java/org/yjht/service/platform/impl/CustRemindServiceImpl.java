package org.yjht.service.platform.impl;

import java.text.ParseException;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.yjht.bean.scsp.FrontInfoCustImpdate;
import org.yjht.core.Result;
import org.yjht.mapper.FrontInfoCustImpdateMapper;
import org.yjht.service.base.AbstractService;
import org.yjht.service.platform.CustRemindService;
import org.yjht.util.DateTools;
import org.yjht.util.ResultGenerator;


@Service
public class CustRemindServiceImpl extends AbstractService<FrontInfoCustImpdate> implements CustRemindService  {
	
	@Autowired
	FrontInfoCustImpdateMapper frontInfoCustImpdateMapper;
	
	@Transactional
	@Override
	public Result saveCustRemind(Map<String, Object> map) {
		// TODO Auto-generated method stub
		String[] custIds = map.get("custIds").toString().trim().split(",");
		for (int i = 0; i < custIds.length; i++) {
			FrontInfoCustImpdate frontInfoCustImpdate = new FrontInfoCustImpdate();
			frontInfoCustImpdate.setCustId(custIds[i]);
			frontInfoCustImpdate.setSeqNo(DateTools.getCurrentSysData("yyyyMMddHHmmsss")+(int)(Math.random()*9+1)*10000);
			frontInfoCustImpdate.setWarnFlag("1");
			frontInfoCustImpdate.setWarnDesc(map.get("warnDesc").toString());
			if(map.get("jnrType")!=null&&map.get("jnrType").toString().trim()!=""){
				frontInfoCustImpdate.setJnrType(map.get("jnrType").toString());
			}
			
			frontInfoCustImpdate.setDateYm(DateTools.formatDate(DateTools.parseDate(map.get("dateYm").toString()), DateTools.SIMPLE_FORMAT));
			
			frontInfoCustImpdate.setWarnType(map.get("warnType").toString());
			frontInfoCustImpdateMapper.insertSelective(frontInfoCustImpdate);
		}
		return ResultGenerator.genSuccessResult("保存成功");
	}

}
