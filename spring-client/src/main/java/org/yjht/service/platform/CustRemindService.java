package org.yjht.service.platform;

import java.util.Map;

import org.yjht.core.Result;

public interface CustRemindService {

	Result saveCustRemind(Map<String, Object> map);

}
