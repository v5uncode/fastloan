package org.yjht.service.platform.impl;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.yjht.bean.Schedule;
import org.yjht.bean.vo.User;
import org.yjht.core.Result;
import org.yjht.events.ScheduleEvent;
import org.yjht.mapper.LrdPlatformMapper;
import org.yjht.mapper.ScheduleMapper;
import org.yjht.service.platform.LrdPlatformService;
import org.yjht.util.CommonUtil;
import org.yjht.util.DateTools;
import org.yjht.util.ResultGenerator;

@Slf4j
@Service
public class LrdPlatformServiceImpl implements LrdPlatformService {

    @Autowired
    private ScheduleMapper scheduleMapper;

    @Autowired
    LrdPlatformMapper lrdPlatformMapper;

    @Override
    public Result queryTx(HttpServletRequest request) {
        // TODO Auto-generated method stub
        User user = (User) request.getSession().getAttribute("user");
        String custGrpJl = user.getUser_id();
        List<Map<String, Object>> custList = lrdPlatformMapper.queryTx(custGrpJl);
        return ResultGenerator.genSuccessResult(custList);
    }

    @Override
    public Result query(HttpServletRequest request, Map<String, Object> map) {
        // TODO Auto-generated method stub
        Schedule schedule = new Schedule();
        User user = (User) request.getSession().getAttribute("user");
        schedule.setUserId(user.getUser_id());
        if (map.get("workDate") != null && map.get("workDate").toString().trim() != "") {
            schedule.setWorkDate(map.get("workDate").toString());
        }
        List<Schedule> scheduleList = scheduleMapper.select(schedule);
        return ResultGenerator.genSuccessResult(scheduleList);
    }

    @Override
    public Result queryNum(HttpServletRequest request, Map<String, Object> map) {
        // TODO Auto-generated method stub
        Schedule schedule = new Schedule();
        User user = (User) request.getSession().getAttribute("user");
        schedule.setUserId(user.getUser_id());
        if (map.get("workDate") != null && map.get("workDate").toString().trim() != "") {
            schedule.setWorkDate(map.get("workDate").toString());
        }
        List<Map<String, Object>> scheduleList = scheduleMapper.queryNum(schedule);
        return ResultGenerator.genSuccessResult(scheduleList);
    }

    @Override
    public Result saveSchedule(HttpServletRequest request, Schedule schedule) {
        // TODO Auto-generated method stub
        User user = (User) request.getSession().getAttribute("user");
        schedule.setUserId(user.getUser_id());
        schedule.setCrtDate(DateTools.getCurrentSysData("yyyyMMdd"));
        schedule.setScheduleId(DateTools.getCurrentSysData("yyyyMMddHHmmsss") + (int) ((Math.random() * 9 + 1) * 10000));
        scheduleMapper.insertSelective(schedule);
        return ResultGenerator.genSuccessResult("插入成功");

    }

    @Override
    public Result updateSchedule(Schedule schedule) {
        schedule.setMtnDate(DateTools.getCurrentSysData("yyyyMMdd"));
        scheduleMapper.updateByPrimaryKeySelective(schedule);
        return ResultGenerator.genSuccessResult("更新成功");
    }

    @Override
    public Result deleteSchedule(String scheduleId) {
        // TODO Auto-generated method stub
        scheduleMapper.deleteByPrimaryKey(scheduleId);
        return ResultGenerator.genSuccessResult();
    }

    @Async
    @EventListener
    public void saveEventSchedule(ScheduleEvent scheduleEvent) {
        log.info("收到保存工作计划事件{}", scheduleEvent);
        Schedule schedule = new Schedule();
        BeanUtils.copyProperties(scheduleEvent, schedule);
        User user = CommonUtil.getUser();
        schedule.setUserId(user.getUser_id());
        schedule.setCrtDate(DateTools.getCurrentSysData("yyyyMMdd"));
        schedule.setScheduleId(DateTools.getCurrentSysData("yyyyMMddHHmmsss") + (int) ((Math.random() * 9 + 1) * 10000));
        scheduleMapper.insertSelective(schedule);
        log.info("保存工作计划事件成功");
    }
}
