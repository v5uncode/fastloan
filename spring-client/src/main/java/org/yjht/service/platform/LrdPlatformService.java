package org.yjht.service.platform;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.yjht.bean.Schedule;
import org.yjht.core.Result;

public interface LrdPlatformService {

	Result queryTx(HttpServletRequest request);

	Result query(HttpServletRequest request,Map<String,Object> map);

	Result queryNum(HttpServletRequest request,Map<String,Object> map);

	Result saveSchedule(HttpServletRequest request, Schedule schedule);
	
	Result updateSchedule(Schedule schedule);

	Result deleteSchedule(String scheduleId);

}
