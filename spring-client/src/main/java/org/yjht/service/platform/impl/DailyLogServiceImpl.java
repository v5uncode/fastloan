package org.yjht.service.platform.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.yjht.bean.DailyLog;
import org.yjht.bean.vo.User;
import org.yjht.core.Result;
import org.yjht.mapper.DailyLogMapper;
import org.yjht.service.base.AbstractService;
import org.yjht.service.platform.DailyLogService;
import org.yjht.util.ResultGenerator;

import tk.mybatis.mapper.entity.Condition;
import tk.mybatis.mapper.entity.Example.Criteria;

@Service
public class DailyLogServiceImpl extends AbstractService<DailyLog> implements DailyLogService {
	
	@Autowired
	DailyLogMapper dailyLogMapper;
	
	@Override
	public Result saveDailylog(HttpServletRequest request, DailyLog dailylog) {
		// TODO Auto-generated method stub
		
		User user = (User)request.getSession().getAttribute("user");
		Condition condition = new Condition(DailyLog.class);
		Criteria criteria = condition.createCriteria();
		if(StringUtils.isNotBlank(dailylog.getWorkDate())){
			criteria.andEqualTo("workDate",dailylog.getWorkDate());
		}
		if(StringUtils.isNotBlank(user.getUser_id())){
			dailylog.setUserId(user.getUser_id());
			criteria.andEqualTo("userId",user.getUser_id());
		}
		List<DailyLog>  dailylogList= findByCondition(condition);
		if(dailylogList!=null&&dailylogList.size()>0){
			dailyLogMapper.updateByPrimaryKeySelective(dailylog);
			return ResultGenerator.genSuccessResult("成功更新用户日志");
		}else{
			save(dailylog);
			return ResultGenerator.genSuccessResult("成功保存用户日志");
		}
	}

	@Override
	public Result queryDailylog(HttpServletRequest request, DailyLog dailylog) {
		// TODO Auto-generated method stub
		User user = (User)request.getSession().getAttribute("user");
		Condition condition = new Condition(DailyLog.class);
		Criteria criteria = condition.createCriteria();
		if(StringUtils.isNotBlank(dailylog.getWorkDate())){
			criteria.andEqualTo("workDate",dailylog.getWorkDate());
		}
		if(StringUtils.isNotBlank(user.getUser_id())){
			criteria.andEqualTo("userId",user.getUser_id());
		}
		List<DailyLog>  dailylogList = findByCondition(condition);
		return ResultGenerator.genSuccessResult(dailylogList);
	}

	@Override
	public Result deleteDailylog(HttpServletRequest request, DailyLog dailylog) {
		// TODO Auto-generated method stub
		User user =(User)request.getSession().getAttribute("user");
		dailylog.setUserId(user.getUser_id());
		dailyLogMapper.delete(dailylog);
		return ResultGenerator.genSuccessResult("删除工作日志成功");
	}
	
}
