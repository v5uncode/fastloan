package org.yjht.service.cust.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.yjht.bean.LrdOrg;
import org.yjht.bean.cust.*;
import org.yjht.bean.vo.User;
import org.yjht.core.Constants;
import org.yjht.core.Page;
import org.yjht.core.Result;
import org.yjht.dao.LrdOrgMapper;
import org.yjht.mapper.cust.CustBaseMapper;
import org.yjht.mapper.cust.FrontInfoCustRelaMapper;
import org.yjht.service.LrdOrgService;
import org.yjht.service.base.AbstractService;
import org.yjht.service.base.CompleteService;
import org.yjht.service.cust.*;
import org.yjht.util.CommonUtil;
import org.yjht.util.DateTools;
import org.yjht.util.DicReplace;
import org.yjht.util.ResultGenerator;
import tk.mybatis.mapper.entity.Condition;
import tk.mybatis.mapper.entity.Example.Criteria;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class CustBaseServiceImpl extends AbstractService<CustBase> implements CustBaseService {
	@Autowired
	private CustBaseMapper custBaseMapper;
	@Autowired
	private LrdOrgService lrdOrgService;
	@Autowired
	private LrdOrgMapper lrdOrgMapper;
	@Autowired
	private FrontInfoCustRelaMapper frontInfoCustRelaMapper;
	@Autowired
	private CompleteService<CustBase> completeService;
	
	@Override
	public Result selectList(CustBase cust,Page page) {
		if(page.getLimit()!=null&&page.getPage()!=null) {
			User user=CommonUtil.getUser();
			List<Map<String,Object>> roleList = user.getROLE_CD();//权限列表
			String fw="";		
			for (Map<String, Object> map2 : roleList) {
				fw=map2.get("ROLE_FW")+fw;
			}		
			//寻找最大范围
			if (fw.contains("01")) {//可查看所有---部队数据进行处理
				
			}else if (fw.contains("02")) {//可查看本级及下级
				String childs= lrdOrgService.getChilds(user.getOrgCD());
				cust.setChildsOrgCd(childs);
			}else if(fw.contains("00")){//查询本级
				cust.setOrgCd(user.getOrgCD());
			}else {//查询个人名下
				cust.setOrgCd(user.getOrgCD());
				cust.setCustGrpJl(user.getUser_id());
				
			}
			cust.setIsdel("0");
			PageHelper.startPage(page.getPage(),page.getLimit(), true);
			List<Map<String,Object>> list=custBaseMapper.selectList(cust);
			for(Map<String,Object> map:list) {
				if(map.get("custGrpJl").equals(user.getUser_id())) {
					map.put("private", true);
				}else {
					map.put("private", false);
				}
			}
			Map<String,String> dicMap=new HashMap<String,String>();
			dicMap.put("custType", "Cust_Type");
			DicReplace.replaceDic(list, dicMap);
			page.setRows(list);
			page.setTotal(new PageInfo<Map<String, Object>>(list).getTotal());	
			return ResultGenerator.genSuccessResult(page);
		}else {
			cust.setIsdel("0");
			CustBase custbase = custBaseMapper.selectOne(cust);
			if(custbase==null) {		
				return ResultGenerator.genSuccessResult();
			}else {
				if(StringUtils.isNotBlank(custbase.getJznx())){
					String jzYear=custbase.getJznx().substring(0, 2);
					String jzMonth=custbase.getJznx().substring(2);
					custbase.setJzYear(jzYear.startsWith("0")?String.valueOf(Integer.parseInt(jzYear)):jzYear);//居住年
					custbase.setJzMonth(jzYear.startsWith("0")?String.valueOf(Integer.parseInt(jzMonth)):jzMonth);//居住月
				}
				return ResultGenerator.genSuccessResult(custbase);
			}
		}
	}
	
	/**
	 * 更改客户信息
	 * */
	@Override
	@Transactional
	public Result updateCust(CustBase cust,HttpSession session) {
		
		String telNo=cust.getTelNo();
		//验证手机号是否已存在
		if(StringUtils.isNotBlank(telNo)) {
			Condition condition = new Condition(CustBase.class);
			Criteria criteria=condition.createCriteria();
			criteria.andEqualTo("telNo", telNo);
			criteria.andNotEqualTo("custId", cust.getCustId());
			criteria.andNotEqualTo("isdel", Constants.ISDEL_NO);
			List<CustBase>  list=custBaseMapper.selectByExample(condition);
			if(!list.isEmpty()) {
				return ResultGenerator.genFailResult("该手机号已存在");
			}
		}
		if(StringUtils.isNotBlank(cust.getBirthDate())) {
        	cust.setCustAge(DateTools.yearDateDiff(cust.getBirthDate(),DateTools.getCurrentSysData(DateTools.DEFAULT_FORMAT)));
        }
		if(StringUtils.isNotBlank(cust.getJzYear())&&StringUtils.isNotBlank(cust.getJzMonth())){
			if(cust.getJzYear().length()==1){
				cust.setJzYear("0"+cust.getJzYear());
			}
			if(cust.getJzMonth().length()==1){
				cust.setJzMonth("0"+cust.getJzMonth());
			}
			cust.setJznx(cust.getJzYear()+cust.getJzMonth());//居住年限
		}
        User user = (User) session.getAttribute("user");      
        cust.setCustGrpJl(user.getUser_id());
        update(cust);
        //修改客户信息完整性信息      判断身份证号是否为空   不为空则修改基本信息完整进度 
        if(StringUtils.isNotBlank(cust.getIdNo())) {	        	
        	completeService.updateCompleteCustInfo(cust);
        }	
		return ResultGenerator.genSuccessResult();		
	}
	/**
	 * 删除客户信息
	 * */
	@Override
	@Transactional
	public Result deleteCust(List<String> custIds) {
		for(String custId:custIds) {
			CustBase cust=new CustBase();
			cust.setCustId(custId);
			cust.setIsdel(Constants.ISDEL_YES);
			//删除客户
			custBaseMapper.updateByPrimaryKeySelective(cust);
			//删除家庭成员
			frontInfoCustRelaMapper.deleteRela(custId, null);
			//删除客户完整度信息
			completeService.deleteCompleteCustInfo(custId);
		}	
		return ResultGenerator.genSuccessResult();
	}
	/**
	 * 添加客户信息
	 * */
	@Override
	@Transactional
	public Result addCust(CustBase cust,HttpSession session) {
        User user = (User) session.getAttribute("user");
        cust.setCorpCd(user.getCorpCD());
        cust.setCustGrpJl(user.getUser_id());
        cust.setCustGrpName(user.getUserName());
        cust.setOrgCd(user.getOrgCD());
        LrdOrg org=new LrdOrg();
        //查询管户经理机构名称
        org.setCorpCd(user.getCorpCD());
        org.setOrgCd(user.getOrgCD());
        cust.setOrgName(lrdOrgMapper.selectOne(org).getOrgName());
        cust.setDatafrom(Constants.DATA_FROM_XZ);//客户来源
        cust.setIsdel(Constants.ISDEL_NO);//是否删除
        cust.setIscredit(Constants.IS_BALANCE_NO);//是否授信
        cust.setCreateType(Constants.CREATE_TYPE_NO);//是否完整
        cust.setIsTmp(Constants.ISTMP_NO);//是否是正式客户 （同步到个贷）
        cust.setCustId(DateTools.getCurrentSysData("yyyyMMddHHmmsss") + (int) (Math.random() * 9 + 1) * 10000);	        
        save(cust);
        completeService.saveCompleteCustInfo(cust.getCustId());
        completeService.updateCompleteCustInfo(cust);			
		return ResultGenerator.genSuccessResult(cust);
	}
	/**
	 * 模糊查询法人内客户信息
	 * @param
	 * page:分页信息
	 * key:姓名/电话/身份证号
	 * @return
	 */
	@Override
	public Result selectLikeKey(Page page,String key,HttpSession session) {
		Map<String,Object> paramMap=new HashMap<String,Object>();
		key="%"+key+"%";//拼装模糊查询关键字
		User user=(User) session.getAttribute("user");
		String corpCd=user.getCorpCD();
		paramMap.put("key", key);
		if(!corpCd.equals(Constants.SLS_CORP)){
			paramMap.put("corpCd", corpCd);
		}	
		PageHelper.startPage(page.getPage(),page.getLimit(), true);
		List<Map<String,Object>> list=custBaseMapper.selectBykey(paramMap);
		for(Map<String,Object> map:list) {
			if(map.get("custGrpJl").equals(user.getUser_id())) {
				map.put("private", true);//属于当前客户经理
			}else {
				map.put("private", false);//不属于当前客户经理
			}			
		}
		page.setRows(list);
		page.setTotal(new PageInfo<Map<String,Object>>(list).getTotal());
		return ResultGenerator.genSuccessResult(page);
	}
	/**
	 * 根据身份证号查询客户信息（可跨法人）
	 * @param
	 * idNo:身份证号
	 * @return
	 */
	@Override
	public List<Map<String,Object>> selectByIdNo(String idNo, HttpSession session) {
		Map<String,Object> paramMap=new HashMap<String,Object>();		
		paramMap.put("idNo",idNo);
		List<Map<String,Object>> list=custBaseMapper.selectBykey(paramMap);
		if(session!=null) {			
			User user=(User) session.getAttribute("user");
			for(Map<String,Object> map:list) {
				String corpCd=user.getCorpCD();
				if(map.get("custGrpJl").equals(user.getUser_id())) {//判断是否属于个人名下
					map.put("private", true);//属于个人名下
					map.put("group", true);//属于法人名下
				}else if(corpCd.equals(map.get("corpCd").toString())) {//判断是否属于法人名下
					map.put("private", false);
					map.put("group", true);//属于法人名下
				}else {
					map.put("private", false);//不属于个人名下
					map.put("group", false);//不属于法人名下
				}			
			}
		}
		return list;
	}

	/**
	 * 根据custId查询客户基本信息
	 * @param id
	 * @return
	 */
	@Override
	public CustBase findByCustid(String id) {
		CustBase custBase = findById(id);
		return custBase;
	}

	@Override
	public CustBase findByIdNo(String idType,String idNo) {		
		CustBase cust = new CustBase();
		cust.setIdType(idType);
		cust.setIdNo(idNo);
		cust.setIsdel(Constants.ISDEL_NO);
		return findOne(cust);
	}
	


}
