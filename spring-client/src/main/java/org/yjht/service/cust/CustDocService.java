package org.yjht.service.cust;

import java.util.List;

import org.yjht.bean.cust.FrontInfoCustDoc;
import org.yjht.core.Result;

public interface CustDocService {
	/**
	 * 查询客户档案信息
	 * @param custId
	 * @param docName
	 * @return
	 */
	Result findDoc(String custId,String docName);
	/**
	 * 保存客户档案信息
	 * @param docs
	 * @return
	 */
	Result saveDoc(List<FrontInfoCustDoc> docs);
	/**
	 * 保存客户档案信息
	 * @param doc
	 * @return
	 */
	FrontInfoCustDoc saveDoc(FrontInfoCustDoc doc);
	/**
	 * 修改客户档案备注信息
	 * @param doc
	 * @return
	 */
	Result updateDoc(FrontInfoCustDoc doc);
	/**
	 * 删除客户档案信息
	 * @param seqNo
	 * @return
	 */
	Result deleteDoc(String seqNo);
}
