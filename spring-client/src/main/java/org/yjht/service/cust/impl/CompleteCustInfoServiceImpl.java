package org.yjht.service.cust.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.yjht.bean.cust.CompleteCustInfo;
import org.yjht.bean.cust.CompleteType;
import org.yjht.bean.cust.CustBase;
import org.yjht.bean.cust.FrontInfoCustBreed;
import org.yjht.bean.cust.FrontInfoCustBusiness;
import org.yjht.bean.cust.FrontInfoCustPlant;
import org.yjht.bean.cust.FrontInfoCustRela;
import org.yjht.core.Constants;
import org.yjht.core.Result;
import org.yjht.enums.MarriageType;
import org.yjht.enums.RelaType;
import org.yjht.mapper.cust.CompleteCustInfoMapper;
import org.yjht.mapper.cust.CompleteTypeMapper;
import org.yjht.mapper.cust.CustBaseMapper;
import org.yjht.mapper.cust.FrontInfoCustBreedMapper;
import org.yjht.mapper.cust.FrontInfoCustBusinessMapper;
import org.yjht.mapper.cust.FrontInfoCustPlantMapper;
import org.yjht.mapper.cust.FrontInfoCustRelaMapper;
import org.yjht.service.cust.CompleteCustInfoService;
import org.yjht.util.ResultGenerator;

import tk.mybatis.mapper.entity.Condition;
import tk.mybatis.mapper.entity.Example.Criteria;
@Service
public class CompleteCustInfoServiceImpl  implements CompleteCustInfoService {
	@Autowired
	CompleteTypeMapper completeTypeMapper;
	@Autowired
	CompleteCustInfoMapper completeCustInfoMapper;
	@Autowired
	FrontInfoCustPlantMapper frontInfoCustPlantMapper;
	@Autowired
	FrontInfoCustBreedMapper frontInfoCustBreedMapper;
	@Autowired
	FrontInfoCustBusinessMapper frontInfoCustBusinessMapper;
	@Autowired
	FrontInfoCustRelaMapper frontInfoCustRelaMapper;
	@Autowired
	CustBaseMapper custBaseMapper;
	
	@Override
	public Result getCustInfoComplete(String custId) {
		Map<String,Object> map=new HashMap<String,Object>();//当前客户对应项完整数量
		Condition condition = new Condition(CompleteCustInfo.class);
		condition.createCriteria().andEqualTo("custId", custId);
		List<CompleteCustInfo> list=completeCustInfoMapper.selectByExample(condition);
		for(CompleteCustInfo completeCustInfo:list) {
			map.put(completeCustInfo.getType(), completeCustInfo.getCurrentNum());
		}
		List<CompleteType> list2=completeTypeMapper.selectAll();//当前客户对应项完整总数量
		for(CompleteType completeType:list2) {
				map.put(completeType.getType()+"Total", completeType.getTotal());		
		}
		if(!list.isEmpty()) {
			map.put("Business", (int)map.get("CustBusiness")+(int)map.get("FrontInfoCustBusiness"));
			map.put("BusinessTotal", (int)map.get("CustBusinessTotal")+(int)map.get("FrontInfoCustBusinessTotal"));
			map.put("Plant", (int)map.get("FrontInfoCustPlant"));
			map.put("PlantTotal",(int)map.get("FrontInfoCustPlantTotal"));
			map.put("WorkTotal", 1);
			Map<String,Object> mainMap=completeCustInfoMapper.findMainProject(custId);
			int work=0;		
			if(mainMap!=null&&mainMap.get("mainProject")!=null&&StringUtils.isNotBlank(mainMap.get("mainProject").toString())) {
				String mainProject=mainMap.get("mainProject").toString();
				if(mainProject.equals(Constants.MAIN01)||mainProject.equals(Constants.MAIN02)) {//种植
					boolean flag=false;
					Condition conditionPlant=new Condition(FrontInfoCustPlant.class);
					Criteria criteria=conditionPlant.createCriteria();
					criteria.andEqualTo("custId",custId);
					switch (mainProject) {
		                case Constants.MAIN01:// 种植蔬菜
		                	criteria.andEqualTo("cropType","ZW_Type_Vegetable");
		                    break;
		                case Constants.MAIN02:// 种植其他
		                	criteria.andNotEqualTo("cropType","ZW_Type_Vegetable");
		                    break;
					}
					List<FrontInfoCustPlant> listPlan=frontInfoCustPlantMapper.selectByExample(conditionPlant);
					if(!listPlan.isEmpty()) {
						flag=true;
					}
					if(flag/*&&map.get("CustPlant").equals(map.get("CustPlantTotal"))*/) {
						work+=1;
					}
				}else if(mainProject.equals(Constants.MAIN03)||mainProject.equals(Constants.MAIN04)||mainProject.equals(Constants.MAIN11)) {//养殖
					Condition conditionBreed=new Condition(FrontInfoCustBreed.class);
					Criteria criteria=conditionBreed.createCriteria();
					criteria.andEqualTo("custId",custId);
					switch (mainProject) {
	                	case Constants.MAIN03:// 养殖猪
	                		criteria.andEqualTo("yzKind",Constants.YZZ);
	                		break;
	                	case Constants.MAIN04:// 养殖其他
	                		criteria.andNotEqualTo("yzType",Constants.MARINE_BREED);
	                		criteria.andNotEqualTo("yzKind",Constants.YZZ);
	                		break;
	                	case Constants.MAIN11:// 海水养殖
	                		criteria.andEqualTo("yzType",Constants.MARINE_BREED);
	                		break;
					}
					List<FrontInfoCustBreed> listBreed=frontInfoCustBreedMapper.selectByExample(conditionBreed);
					if(!listBreed.isEmpty()) {
						work+=1;
					}			
				}else if(mainProject.equals(Constants.MAIN05)){//职工
					//主营项目为职工时,财务信息主营项目收入和支出不检验
					map.put("FrontInfoFinRptTotal", Integer.parseInt(map.get("FrontInfoFinRptTotal").toString())-6);
					if((int)map.get("FrontInfoFinRpt")>(int)map.get("FrontInfoFinRptTotal")) {
						map.put("FrontInfoFinRpt", Integer.parseInt(map.get("FrontInfoFinRptTotal").toString()));
					}
					if(map.get("FrontInfoCustStaff").equals(map.get("FrontInfoCustStaffTotal"))) {
						work+=1;
					}
				}else {//经商
					boolean flag=false;
					Condition conditionBusiness=new Condition(FrontInfoCustBusiness.class);
					Criteria criteria=conditionBusiness.createCriteria();
					criteria.andEqualTo("custId",custId);
					switch (mainProject) {
		                case Constants.MAIN06:// 制造业
		                	criteria.andEqualTo("bizHy",Constants.BIZHY01);
		                    break;  
		                case Constants.MAIN07:// 运输业
		                	criteria.andEqualTo("bizHy",Constants.BIZHY02);
		                    break;
		                case Constants.MAIN08:// 批发零售业
		                	criteria.andEqualTo("bizHy",Constants.BIZHY04);
		                    break;
		                case Constants.MAIN09:// 餐饮及住宿业
		                	criteria.andEqualTo("bizHy",Constants.BIZHY03);
		                    break;
		                default :// 其他行业
		                	criteria.andEqualTo("bizHy",Constants.BIZHY05);
		                    break;	                  
					}
					List<FrontInfoCustBusiness> listBusiness=frontInfoCustBusinessMapper.selectByExample(conditionBusiness);
					if(!listBusiness.isEmpty()) {
						flag=true;
					}
					if(flag&&map.get("CustBusiness").equals(map.get("CustBusinessTotal"))) {
						work+=1;
					}				
				}
				
			}
			map.put("Work", work);
			
			//家庭信息
			String marriageFlag=completeCustInfoMapper.findJhFlag(custId);
			Condition conditionRela=new Condition(FrontInfoCustRela.class);
			Criteria criteria=conditionRela.createCriteria();
			criteria.andEqualTo("custId",custId);
			criteria.andEqualTo("isdel",Constants.ISDEL_NO);
			if(StringUtils.isNotBlank(marriageFlag)) {
				if(marriageFlag.equals(MarriageType.getCode("已婚"))) {			
					criteria.andEqualTo("relaType",RelaType.getCode("配偶"));
				}else {
					map.put("CustFamilyTotal",(int) map.get("CustFamilyTotal")-1);
				}
			}
			List<FrontInfoCustRela> listRela=frontInfoCustRelaMapper.selectByExample(conditionRela);
			int valueFamily=0;
			if(!listRela.isEmpty()) {
				valueFamily=6;
			}
			map.put("Family", (int)map.get("CustFamily")+valueFamily);
			map.put("FamilyTatal", (int)map.get("CustFamilyTotal")+6);
			CustBase cust=new CustBase();
			cust.setCustId(custId);
			if(map.get("CustBase").equals(map.get("CustBaseTotal"))&&map.get("Work").equals(map.get("WorkTotal"))&&
					map.get("Family").equals(map.get("FamilyTatal"))&&map.get("FrontInfoFinRpt").equals(map.get("FrontInfoFinRptTotal"))
					&&map.get("CustCreditReport").equals(map.get("CustCreditReportTotal"))) {				
				cust.setCreateType(Constants.CREATE_TYPE_YES);
				custBaseMapper.updateByPrimaryKeySelective(cust);
			}else {
				cust.setCreateType(Constants.CREATE_TYPE_NO);
				custBaseMapper.updateByPrimaryKeySelective(cust);
			}
			
			Map<String,String> resultMap=new HashMap<String,String>();
			resultMap.put("custBase", map.get("CustBase")+"/"+map.get("CustBaseTotal"));
			resultMap.put("custWork", map.get("Work")+"/"+map.get("WorkTotal"));
			resultMap.put("custFamily", map.get("Family")+"/"+map.get("FamilyTatal"));
			resultMap.put("custFin", map.get("FrontInfoFinRpt")+"/"+map.get("FrontInfoFinRptTotal"));
			resultMap.put("report", map.get("CustCreditReport")+"/"+map.get("CustCreditReportTotal"));
			resultMap.put("custStaff", map.get("FrontInfoCustStaff")+"/"+map.get("FrontInfoCustStaffTotal"));
			resultMap.put("custPlant", map.get("Plant")+"/"+map.get("PlantTotal"));
			resultMap.put("custBreed", map.get("FrontInfoCustBreed")+"/"+map.get("FrontInfoCustBreedTotal"));
			resultMap.put("custBusiness", map.get("Business")+"/"+map.get("BusinessTotal"));
			return ResultGenerator.genSuccessResult(resultMap);
		}
		return ResultGenerator.genSuccessResult();
	}
}
