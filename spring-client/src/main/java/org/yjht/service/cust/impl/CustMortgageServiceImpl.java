package org.yjht.service.cust.impl;

import com.yjht.seq.sequence.Sequence;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.yjht.bean.cust.CustMortgage;
import org.yjht.core.CommonConstant;
import org.yjht.core.Result;
import org.yjht.service.base.AbstractService;
import org.yjht.service.cust.CustMortgageService;
import org.yjht.util.CommonUtil;
import org.yjht.util.ResultGenerator;
import org.yjht.util.StringUtil;
import tk.mybatis.mapper.entity.Condition;
import tk.mybatis.mapper.entity.Example;

/**
 * @author lengleng
 * @date 2018/3/16
 */
@Service
public class CustMortgageServiceImpl extends AbstractService<CustMortgage> implements CustMortgageService {
    @Autowired
    private Sequence sequence;
    /**
     * 查询客户的抵押信息
     *
     * @param custId 客户号
     * @return Result
     */
    @Override
    public Result findMortgage(String custId) {
        Condition condition = new Condition(CustMortgage.class);
        Example.Criteria criteria = condition.createCriteria();
        criteria.andEqualTo("custId", custId);
        return ResultGenerator.genSuccessResult(findByCondition(condition));
    }

    /**
     * 新增抵押信息
     *
     * @param custMortgage 抵押信息
     * @return Result
     */
    @Override
    public Result addMortgage(CustMortgage custMortgage) {
    	if(StringUtils.isNotBlank(custMortgage.getMortgageId())){//存在修改
    		update(custMortgage);
    	}else{//没有新增
    		custMortgage.setMortgageId(StringUtil.getUUID());
    		save(custMortgage);
    	}        
        return ResultGenerator.genSuccessResult(custMortgage);
    }

    /**
     * 删除抵押信息
     *
     * @param id 抵押信息ID
     * @return Result
     */
    @Override
    public Result deleteMortgage(String id) {
        return ResultGenerator.genSuccessResult(deleteById(id));
    }

    /**
     * 修改抵押信息
     *
     * @param custMortgage 抵押信息
     * @return Result
     */
    @Override
    public Result editMortgage(CustMortgage custMortgage) {
        return ResultGenerator.genSuccessResult(update(custMortgage));
    }

}
