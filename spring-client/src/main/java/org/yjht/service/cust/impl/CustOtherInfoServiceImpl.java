package org.yjht.service.cust.impl;

import org.springframework.stereotype.Service;
import org.yjht.bean.cust.CustOtherInfo;
import org.yjht.core.Result;
import org.yjht.service.base.AbstractService;
import org.yjht.service.cust.CustOtherInfoService;
import org.yjht.util.ResultGenerator;
@Service
public class CustOtherInfoServiceImpl extends AbstractService<CustOtherInfo> implements CustOtherInfoService {

	@Override
	public Result findOtherInf(String custId) {
		CustOtherInfo custOtherInfo = findById(custId);
		return ResultGenerator.genSuccessResult(custOtherInfo);
	}

	@Override
	public Result saveOtherInfo(CustOtherInfo custOtherInfo) {
		int n=0;
		if(findById(custOtherInfo.getCustId())==null) {		
			n=save(custOtherInfo);
		} else {
			n=update(custOtherInfo);
		}
		if(n>0) {
			return ResultGenerator.genSuccessResult(custOtherInfo);
		}else {
			return ResultGenerator.genFailResult("保存失败");
		}
		
	}

}
