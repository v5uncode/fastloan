package org.yjht.service.cust;

import javax.servlet.http.HttpSession;

import org.yjht.core.Result;

public interface TreeService {
	/**
	 * 查询移交树-当前法人下所有机构和客户经理
	 * @param corpCd
	 * @return
	 */
	Result findYjTree(String corpCd);
	/**
	 * 根据姓名模糊查找客户经理以及所属机构
	 * @param userName
	 * @return
	 */
	Result findUserAndOrg(String userName,HttpSession session);
}
