package org.yjht.service.cust;

import org.yjht.bean.cust.CustOtherInfo;
import org.yjht.core.Result;

public interface CustOtherInfoService {
	/**
	 * 查找客户负面信息
	 * @param custId
	 * @return
	 */
	Result findOtherInf(String custId);
	/**
	 * 维护客户负面信息
	 * @param custOtherInfo
	 * @return
	 */
	Result saveOtherInfo(CustOtherInfo custOtherInfo);
	
}
