package org.yjht.service.cust;

import org.yjht.bean.cust.CustMortgage;
import org.yjht.core.Result;

/**
 * @author lengleng
 * @date 2018/3/16
 * 抵押信息Service
 */
public interface CustMortgageService {

    /**
     * 查询客户的抵押信息
     *
     * @param custId 客户号
     * @return Result
     */
    Result findMortgage(String custId);

    /**
     * 新增抵押信息
     *
     * @param custMortgage 抵押信息
     * @return Result
     */
    Result addMortgage(CustMortgage custMortgage);

    /**
     * 删除抵押信息
     * @param id 抵押信息ID
     * @return Result
     */
    Result deleteMortgage(String id);

    /**
     * 修改抵押信息
     * @param custMortgage 抵押信息
     * @return Result
     */
    Result editMortgage(CustMortgage custMortgage);

}
