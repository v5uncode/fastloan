package org.yjht.service.cust;

import org.yjht.bean.cust.CustPledge;
import org.yjht.core.Result;

/**
 * @author lengleng
 * @date 2018/3/19
 */
public interface CustPledgeService {
    /**
     * 查询客户的质押信息
     *
     * @param custId 客户号
     * @return Result
     */
    Result findPledge(String custId);

    /**
     * 修改质押信息
     * @param custPledge 质押信息
     * @return Result
     */
    Result editCustPledge(CustPledge custPledge);

    /**
     * 新增质押信息
     *
     * @param custPledge 质押信息
     * @return Result
     */
    Result addCustPledge(CustPledge custPledge);

    /**
     * 删除质押信息
     * @param id 抵押信息ID
     * @return Result
     */
    Result deletePledge(String id);
}
