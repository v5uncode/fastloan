package org.yjht.service.cust.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.yjht.bean.cust.FrontInfoCustStaff;
import org.yjht.core.Result;
import org.yjht.service.base.AbstractService;
import org.yjht.service.base.CompleteService;
import org.yjht.service.cust.CustStaffService;
import org.yjht.util.ResultGenerator;


@Service
public class CustStaffServiceImpl extends AbstractService<FrontInfoCustStaff> implements CustStaffService {
	@Autowired
	CompleteService<FrontInfoCustStaff> completeService;
	@Override
	@Transactional
	public Result saveStaff(FrontInfoCustStaff staff) {
		FrontInfoCustStaff frontInfoCustStaff = findById(staff.getCustId());
		int n=0;
		if(frontInfoCustStaff!=null) {
			n=update(staff);			
		}else {
			n = save(staff);
		}
		//修改职工完整信息
		completeService.updateCompleteCustInfo(staff);
		if(n>0) {
			return ResultGenerator.genSuccessResult(staff);
		}else {
			return ResultGenerator.genFailResult("保存失败");
		}
	}

	@Override
	public Result updateStaff(FrontInfoCustStaff staff) {
		int n=update(staff);
		if(n>0) {
			return ResultGenerator.genSuccessResult(staff);
		}else {
			return ResultGenerator.genFailResult("保存失败");
		}
	}

	@Override
	public FrontInfoCustStaff findStaff(String custId) {		
		return findById(custId);
	}


}
