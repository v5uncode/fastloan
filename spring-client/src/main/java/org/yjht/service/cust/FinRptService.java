package org.yjht.service.cust;

import org.yjht.bean.cust.FrontInfoFinRpt;
import org.yjht.core.Result;

public interface FinRptService {
	/**
	 * 获取客户财务信息
	 * @param custId
	 * @return
	 */
	FrontInfoFinRpt findFinRpt(String custId);
	/**
	 * 
	 * @param finRpt
	 * @return
	 */
	Result saveFinRpt(FrontInfoFinRpt finRpt);
	/**
	 * 删除客户财务信息
	 * @param finRpt
	 * @return
	 */
	Result deleteFinRpt(FrontInfoFinRpt finRpt);

	/**
	 * 根据custId获取客户财务信息
	 * @param custId
	 * @return
	 */
	FrontInfoFinRpt findFinRptById(String custId);
}
