package org.yjht.service.cust;

import org.yjht.bean.cust.FrontInfoRptDtl;
import org.yjht.core.Result;

import java.util.List;
import java.util.Map;

public interface RptDtlService {
	/**
	 * 查询财务明细列表
	 * @param custId
	 * @param subjCd
	 * @return
	 */
	Result findRptDtl(String custId,String subjCd);
	/**
	 * 保存财务明细信息
	 * @param rptDtls
	 * @return
	 */
	Result saveRptDtl(List<FrontInfoRptDtl> rptDtls);
	/**
	 * 更新财务明细信息
	 * @param rptDtl
	 * @return
	 */
	Result updateRptDtl(FrontInfoRptDtl rptDtl);
	/**
	 * 删除财务明细信息
	 * @param rptDtlId
	 * @return
	 */
	Result deleteRptDtl(String rptDtlId);

	/**
	 * 根据custId查询家庭收入财务明细  用于信贷接口
	 * @param custId
	 * @return
	 */
	List<Map<String,String>> findSrRptDtlById(String custId);


	Integer getDtlInfoByCustId(String custId);



}
