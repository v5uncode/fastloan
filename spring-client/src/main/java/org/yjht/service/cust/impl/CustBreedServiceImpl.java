package org.yjht.service.cust.impl;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.yjht.bean.cust.FrontInfoCustBreed;
import org.yjht.core.Constants;
import org.yjht.core.Result;
import org.yjht.mapper.cust.FrontInfoCustBreedMapper;
import org.yjht.service.base.AbstractService;
import org.yjht.service.base.CompleteService;
import org.yjht.service.cust.CustBreedService;
import org.yjht.util.DateTools;
import org.yjht.util.ResultGenerator;
import org.yjht.util.StringUtil;
import tk.mybatis.mapper.entity.Condition;
import tk.mybatis.mapper.entity.Example.Criteria;

import java.util.List;
@Service
public class CustBreedServiceImpl extends AbstractService<FrontInfoCustBreed> implements CustBreedService {
	@Autowired
	CompleteService<FrontInfoCustBreed> completeService;
	@Autowired
	FrontInfoCustBreedMapper frontInfoCustBreedMapper;
	@Override
	@Transactional
	public Result saveBreed(FrontInfoCustBreed breed) {
		Condition condition = new Condition(FrontInfoCustBreed.class);
		Criteria criteria=condition.createCriteria();
		criteria.andEqualTo("custId", breed.getCustId());
		if(breed.getYzType().equals(Constants.MARINE_BREED)){
			criteria.andEqualTo("yzType", breed.getYzType());//海产品养殖控制类型不重复
		}else{
			criteria.andEqualTo("yzKind", breed.getYzKind());//其他控制种类不重复
		}
		
		List<FrontInfoCustBreed> list=findByCondition(condition);
		int n=0;
		//存在更新，否则添加
		if(!list.isEmpty()) {
			breed.setId(list.get(0).getId());
			n=update(breed);
		}else {
			breed.setId(StringUtil.getUUID());
			n=save(breed);
			//修改养殖完整信息
			completeService.updateCompleteCustInfo(breed.getCustId(),"FrontInfoCustBreed",breed);
		}
		if(n>0) {
			return ResultGenerator.genSuccessResult(breed);
		}else {
			return ResultGenerator.genFailResult("保存失败");
		}
	}

	@Override
	public Result updateBreed(FrontInfoCustBreed breed) {
		int n=update(breed);
		if(n>0) {
			return ResultGenerator.genSuccessResult(breed);
		}else {
			return ResultGenerator.genFailResult("修改失败");
		}
	}

	@Override
	public Result findBreed(String custId) {
		Condition condition = new Condition(FrontInfoCustBreed.class);
		condition.createCriteria().andEqualTo("custId", custId);
		List<FrontInfoCustBreed> list=findByCondition(condition);
		for (FrontInfoCustBreed breed:list) {
			int time = DateTools.getCurrentYear()-Integer.parseInt(breed.getBizDate());
			breed.setBizYears(new Integer(time));
		}
		return ResultGenerator.genSuccessResult(list);
	}

	@Override
	@Transactional
	public Result deleteBreed(String custId,String id) {
		int n=deleteById(id);
		//修改养殖完整信息
		if(((List<?>)findBreed(custId).getData()).isEmpty()) {
			completeService.updateCompleteCustInfo(custId,"FrontInfoCustBreed",null);
		}
		if(n>0) {
			return ResultGenerator.genSuccessResult();
		}else {
			return ResultGenerator.genFailResult("删除失败");
		}
	}

	@Override
	public Result findBreedById(String id) {
		FrontInfoCustBreed breed =findById(id);
		return ResultGenerator.genSuccessResult(breed);
	}

	@Override
	public FrontInfoCustBreed getBreedInfoBy(String custId, String yzType,String yzKind) {
		Condition condition = new Condition(FrontInfoCustBreed.class);
		Criteria criteria = condition.createCriteria();
		criteria.andEqualTo("custId", custId);
		if(StringUtils.isNotBlank(yzKind)){
			criteria.andEqualTo("yzKind",yzKind);
		}
		if(StringUtils.isNotBlank(yzKind)){
			criteria.andEqualTo("yzKind",yzKind);
		}
		List<FrontInfoCustBreed> breedList = findByCondition(condition);
		if(!breedList.isEmpty()){
			return breedList.get(0);
		}else{
			return null;
		}
	}

	@Override
	public String getBreedOtherYears(String custId,String yzType,String yzKing) {
		return frontInfoCustBreedMapper.getBreedOtherYears(custId,yzType,yzKing);
	}
}
