package org.yjht.service.cust.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.yjht.bean.cust.CustFamily;
import org.yjht.core.Result;
import org.yjht.service.base.AbstractService;
import org.yjht.service.base.CompleteService;
import org.yjht.service.cust.CustFamilyService;
import org.yjht.util.ResultGenerator;
@Service
public class CustFamilyServiceImpl extends AbstractService<CustFamily> implements CustFamilyService {
	@Autowired
	CompleteService<CustFamily> completeService;
	@Override
	@Transactional
	public Result saveFamily(CustFamily custFamily) {
		//查询家庭基本信息
		CustFamily custFamily1 = findById(custFamily.getCustId());
		int n=0;
		//存在更新  不存在添加
		if(custFamily1!=null) {
			n=update(custFamily);			
		}else {
			n = save(custFamily);
		}
		//修改家庭基本信息进度
		completeService.updateCompleteCustInfo(custFamily);
		if(n>0) {
			return ResultGenerator.genSuccessResult(custFamily);
		}else {
			return ResultGenerator.genFailResult("保存失败");
		}
	}

	@Override
	public Result updateFamily(CustFamily custFamily) {
		int n=update(custFamily);
		if(n>0) {
			return ResultGenerator.genSuccessResult(custFamily);
		}else {
			return ResultGenerator.genFailResult("保存失败");
		}
	}

	@Override
	public CustFamily findFamily(String custId) {
		return findById(custId);
	}

}
