package org.yjht.service.cust.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.yjht.bean.cust.FrontInfoCustPlant;
import org.yjht.core.Result;
import org.yjht.mapper.cust.FrontInfoCustPlantMapper;
import org.yjht.service.base.AbstractService;
import org.yjht.service.base.CompleteService;
import org.yjht.service.cust.FrontInfoCustPlantService;
import org.yjht.util.DateTools;
import org.yjht.util.ResultGenerator;
import org.yjht.util.StringUtil;
import tk.mybatis.mapper.entity.Condition;
import tk.mybatis.mapper.entity.Example.Criteria;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
@Service
public class FrontInfoCustPlantServiceImpl extends AbstractService<FrontInfoCustPlant> implements FrontInfoCustPlantService {
	@Autowired
	CompleteService<FrontInfoCustPlant> completeService;
	@Autowired
	FrontInfoCustPlantMapper frontInfoCustPlantMapper;

	@Override
	@Transactional
	public Result savePlant(FrontInfoCustPlant plant) {
		//根据客户编号，作物种类，种植模式查询是否存在
		Condition condition = new Condition(FrontInfoCustPlant.class);
		Criteria criteria=condition.createCriteria();
		criteria.andEqualTo("custId", plant.getCustId());
		criteria.andEqualTo("zwType", plant.getZwType());
		criteria.andEqualTo("zzModel", plant.getZzModel());
		List<FrontInfoCustPlant> list=findByCondition(condition);
		int n=0;
		//存在更新，否则添加
		if(!list.isEmpty()) {
			plant.setId(list.get(0).getId());
			n=update(plant);
		}else {
			plant.setId(StringUtil.getUUID());
			n=save(plant);
			//修改种植完整信息
			completeService.updateCompleteCustInfo(plant.getCustId(),"FrontInfoCustPlant",plant);
		}
		if(n>0) {
			return ResultGenerator.genSuccessResult(plant);
		}else {
			return ResultGenerator.genFailResult("保存失败");
		}
	}

	@Override
	public Result updatePlant(FrontInfoCustPlant plant) {
		int n=update(plant);
		if(n>0) {
			return ResultGenerator.genSuccessResult(plant);
		}else {
			return ResultGenerator.genFailResult("修改失败");
		}
	}

	@Override
	public Result findPlant(String custId) {
		Condition condition = new Condition(FrontInfoCustPlant.class);
		condition.createCriteria().andEqualTo("custId", custId);
		List<FrontInfoCustPlant> list=findByCondition(condition);
		for (FrontInfoCustPlant plant:list) {
			int time = DateTools.getCurrentYear()-Integer.parseInt(plant.getBizDate());
			plant.setBizYears(new BigDecimal(time));
		}
		return ResultGenerator.genSuccessResult(list);
	}

	@Override
	public Result deletePlant(String custId,String id) {
		int n=deleteById(id);
		//修改种植完整信息
		if(((List<?>)findPlant(custId).getData()).isEmpty()) {
			completeService.updateCompleteCustInfo(custId,"FrontInfoCustPlant",null);
		}
		if(n>0) {
			return ResultGenerator.genSuccessResult();
		}else {
			return ResultGenerator.genFailResult("删除失败");
		}
	}

	@Override
	public Result findPlantById(String id) {
		FrontInfoCustPlant plant=findById(id);
		return ResultGenerator.genSuccessResult(plant);
	}
	/**
	 * 评级模型：根据custId和种植作物查询种植亩数
	 * @param custId
	 * @param cropType
	 * @return
	 */
	@Override
	public List<Map<String, Object>> getZzms(String custId, String cropType,String flag) {
		return frontInfoCustPlantMapper.getZzms(custId,cropType,flag);
	}

	@Override
	public String getPlantYears(String custId, String cropType) {
		return frontInfoCustPlantMapper.getPlantYears(custId,cropType);
	}

	@Override
	public String getOtherPlantYears(String custId, String cropType) {
		return frontInfoCustPlantMapper.getOtherPlantYears(custId,cropType);
	}
}
