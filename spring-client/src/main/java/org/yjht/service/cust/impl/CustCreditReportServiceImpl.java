package org.yjht.service.cust.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.yjht.bean.cust.CustCreditReport;
import org.yjht.core.Result;
import org.yjht.mapper.cust.CustCreditReportMapper;
import org.yjht.service.base.AbstractService;
import org.yjht.service.base.CompleteService;
import org.yjht.service.cust.CustCreditReportService;
import org.yjht.util.ResultGenerator;
@Service
public class CustCreditReportServiceImpl extends AbstractService<CustCreditReport> implements CustCreditReportService {
	@Autowired
	CompleteService<CustCreditReport> completeService;

	@Autowired
	CustCreditReportMapper custCreditReportMapper;


	@Override
	public Result findCustCreditReport(String custId) {
		CustCreditReport custCreditReport=findById(custId);
		return ResultGenerator.genSuccessResult(custCreditReport);
	}

	@Override
	@Transactional
	public Result saveCustCreditReport(CustCreditReport custCreditReport) {
		CustCreditReport report=findById(custCreditReport.getCustId());
		int n=0;
		if(report!=null) {
			n=update(custCreditReport);
		}else {
			n=save(custCreditReport);
		}
		//征信信息完整性
		completeService.updateCompleteCustInfo(custCreditReport);
		if(n>0) {
			return ResultGenerator.genSuccessResult(custCreditReport);
		}else {
			return ResultGenerator.genFailResult("保存失败");
		}
	}

	/**
	 * 根据客户证件类型 证件号查询征信信息
	 * @param idNo
	 * @param idType
	 * @return
	 */
	@Override
	public CustCreditReport getCreditInfoByIdNo(String idNo,String idType) {
		CustCreditReport report = custCreditReportMapper.getCreditInfoByIdNo(idNo,idType);
		if(report != null){
			int times = Integer.parseInt(report.getCreditOverdueCardTimes12())+Integer.parseInt(report.getCreditOverdueLoanTimes12());
			report.setCrOverdueCountIn12M(times+"");
		}
		return report;
	}

	/**
	 * 根据custId查询征信信息
	 * @param custId
	 * @return
	 */
	@Override
	public CustCreditReport getCreditInfoByCustId(String custId) {

		return findById(custId);
	}
}
