package org.yjht.service.cust.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.yjht.bean.cust.FrontInfoCustBusiness;
import org.yjht.core.Result;
import org.yjht.mapper.cust.FrontInfoCustBusinessMapper;
import org.yjht.service.base.AbstractService;
import org.yjht.service.base.CompleteService;
import org.yjht.service.cust.FrontInfoCustBusinessService;
import org.yjht.util.DateTools;
import org.yjht.util.ResultGenerator;
import org.yjht.util.StringUtil;
import tk.mybatis.mapper.entity.Condition;
import tk.mybatis.mapper.entity.Example.Criteria;

import java.math.BigDecimal;
import java.util.List;
@Service
public class FrontInfoCustBusinessServiceImpl extends AbstractService<FrontInfoCustBusiness> implements FrontInfoCustBusinessService {
	@Autowired
	CompleteService<FrontInfoCustBusiness> completeService;
	@Autowired
	FrontInfoCustBusinessMapper frontInfoCustBusinessMapper;

	@Override
	public Result saveFrontInfoCustBusiness(FrontInfoCustBusiness business) {
		//根据客户编号，经营行业
		Condition condition = new Condition(FrontInfoCustBusiness.class);
		Criteria criteria=condition.createCriteria();
		criteria.andEqualTo("custId", business.getCustId());
		criteria.andEqualTo("bizHy", business.getBizHy());
		BigDecimal ownArea=business.getOwnArea();
		BigDecimal rentArea=business.getRentArea();
		if(ownArea!=null&&ownArea.intValue()>0) {
			business.setOwnFlag("1");
		}else {
			business.setOwnFlag("0");
		}
		if(rentArea!=null&&rentArea.intValue()>0) {
			business.setRentFlag("1");
		}else {
			business.setRentFlag("0");
		}
		List<FrontInfoCustBusiness> list=findByCondition(condition);
		int n=0;
		//存在更新，否则添加
		if(!list.isEmpty()) {
			business.setId(list.get(0).getId());
			n=update(business);
		}else {
			business.setId(StringUtil.getUUID());
			n=save(business);
			//修改经商完整信息
			completeService.updateCompleteCustInfo(business.getCustId(),"FrontInfoCustBusiness",business);
		}
		if(n>0) {
			return ResultGenerator.genSuccessResult(business);
		}else {
			return ResultGenerator.genFailResult("保存失败");
		}
	}

	@Override
	public Result updateFrontInfoCustBusiness(FrontInfoCustBusiness business) {
		int n=update(business);
		if(n>0) {
			return ResultGenerator.genSuccessResult(business);
		}else {
			return ResultGenerator.genFailResult("修改失败");
		}
	}

	@Override
	public Result findFrontInfoCustBusiness(String custId) {
		Condition condition = new Condition(FrontInfoCustBusiness.class);
		condition.createCriteria().andEqualTo("custId", custId);
		List<FrontInfoCustBusiness> list=findByCondition(condition);
		for (FrontInfoCustBusiness business:list) {
			int time = DateTools.getCurrentYear()-Integer.parseInt(business.getBizDate());
			business.setBizYears(new BigDecimal(time));
		}
		return ResultGenerator.genSuccessResult(list);
	}

	@Override
	public Result deleteFrontInfoCustBusiness(String custId,String id) {
		int n=deleteById(id);
		//修改经商完整信息
		if(((List<?>)findFrontInfoCustBusiness(custId).getData()).isEmpty()) {
			completeService.updateCompleteCustInfo(custId,"FrontInfoCustBusiness",null);
		}
		if(n>0) {
			return ResultGenerator.genSuccessResult();
		}else {
			return ResultGenerator.genFailResult("删除失败");
		}
	}

	@Override
	public Result findFrontInfoCustBusinessById(String id) {
		FrontInfoCustBusiness frontInfoCustBusiness = findById(id);
		return ResultGenerator.genSuccessResult(frontInfoCustBusiness);
	}


	@Override
	public FrontInfoCustBusiness getBusinessByHy(String custId, String bizHy) {

		return frontInfoCustBusinessMapper.getBusinessByHy(custId,bizHy);
	}
}
