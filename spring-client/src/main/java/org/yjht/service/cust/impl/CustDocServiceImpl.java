package org.yjht.service.cust.impl;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.yjht.bean.cust.FrontInfoCustDoc;
import org.yjht.core.Result;
import org.yjht.service.base.AbstractService;
import org.yjht.service.cust.CustDocService;
import org.yjht.util.DateTools;
import org.yjht.util.ResultGenerator;
import org.yjht.util.StringUtil;

import tk.mybatis.mapper.entity.Condition;
import tk.mybatis.mapper.entity.Example.Criteria;
@Service
public class CustDocServiceImpl extends AbstractService<FrontInfoCustDoc> implements CustDocService {

	@Override
	public Result findDoc(String custId,String docName) {
		Condition condition = new Condition(FrontInfoCustDoc.class);
		Criteria criteria=condition.createCriteria();
		criteria.andEqualTo("custId",custId);
		criteria.andEqualTo("docName",docName);
		List<FrontInfoCustDoc> list=findByCondition(condition);
		return ResultGenerator.genSuccessResult(list);
	}

	@Override
	@Transactional
	public Result saveDoc(List<FrontInfoCustDoc> docs) {
		int n=0;
		//保存多条记录
		for(FrontInfoCustDoc doc:docs) {
			doc.setSeqNo(StringUtil.getUUID());
			n+=save(doc);
		}
		if(n>0) {
			return ResultGenerator.genSuccessResult(docs);
		}else {
			return ResultGenerator.genFailResult("保存失败");
		}
	}
	@Override
	public FrontInfoCustDoc saveDoc(FrontInfoCustDoc doc) {
		doc.setSeqNo(DateTools.getCurrentSysData("yyyyMMddHHmmsss")+(int)(Math.random()*9+1)*10000);
		save(doc);
		return doc;
		
		
	}
	@Override
	public Result updateDoc(FrontInfoCustDoc doc) {
		int n=update(doc);
		if(n>0) {
			return ResultGenerator.genSuccessResult(doc);
		}else {
			return ResultGenerator.genFailResult("修改失败");
		}
	}

	@Override
	public Result deleteDoc(String seqNo) {
		int n=deleteById(seqNo);
		if(n>0) {
			return ResultGenerator.genSuccessResult();
		}else {
			return ResultGenerator.genFailResult("修改失败");
		}
	}

}
