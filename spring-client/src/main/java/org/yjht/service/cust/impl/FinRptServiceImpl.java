package org.yjht.service.cust.impl;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.yjht.bean.cust.FrontInfoFinRpt;
import org.yjht.core.Result;
import org.yjht.service.base.AbstractService;
import org.yjht.service.base.CompleteService;
import org.yjht.service.cust.FinRptService;
import org.yjht.util.ResultGenerator;


@Service
public class FinRptServiceImpl extends AbstractService<FrontInfoFinRpt> implements FinRptService {
	@Autowired
	CompleteService<FrontInfoFinRpt> completeService;
	@Override
	public FrontInfoFinRpt findFinRpt(String custId) {
		return findBy("custId", custId);			
	}

	@Override
	@Transactional
	public Result saveFinRpt(FrontInfoFinRpt finRpt) {	
		int n=0;
		if(finRpt.getRptType()==null) {
			finRpt.setRptType("1");
		}
		if (findById(finRpt)!=null) {
			n=update(finRpt);
		} else {
			n=save(finRpt);
		}
		//修改财务完整信息
		completeService.updateCompleteCustInfo(finRpt);
		if(n>0) {
			return ResultGenerator.genSuccessResult();
		}else {
			return ResultGenerator.genFailResult("保存失败");
		}
	}

	@Override
	public Result deleteFinRpt(FrontInfoFinRpt finRpt) {
		if(StringUtils.isNotBlank(finRpt.getCustId())) {
			int n=deleteById(finRpt);
			if(n>0) {
				return ResultGenerator.genSuccessResult();
			}else {
				return ResultGenerator.genFailResult("删除失败");
			}
		}else {
			return ResultGenerator.genFailResult("删除失败");
		}
	}

	/**
	 * 根据custId获取客户财务信息
	 * @param custId
	 * @return
	 */
	@Override
	public FrontInfoFinRpt findFinRptById(String custId) {
		return findById(custId);
	}
}
