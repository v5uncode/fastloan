package org.yjht.service.cust;

import java.util.List;

import org.yjht.bean.cust.FrontInfoCustRela;
import org.yjht.core.Result;
import org.yjht.service.base.Service;

public interface CustRelaService extends Service<FrontInfoCustRela>{
	/**
	 * 保存客户关系人
	 * @param rela
	 * @return
	 */
	Result saveRela(FrontInfoCustRela rela);
	/**
	 * 修改客户关系人信息
	 * @param rela
	 * @return
	 */
	Result updateRela(FrontInfoCustRela rela);
	/**
	 * 获取客户关系人列表
	 * @param custId
	 * @return
	 */
	List<FrontInfoCustRela> findRela(String custId);
	/**
	 * 通过主键查找客户关系人 
	 * @param seqNo
	 * @return
	 */
	Result findRelaById(String seqNo);
	/**
	 * 删除客户关系人
	 *  @param custId
	 * @param seqNo
	 * @return
	 */
	Result deleteRela(String custId,String seqNo);

	/**
	 * 
	 * 审查审批查询家庭成员关系
	 */
	Result findScspRela(String custId);

	/**
	 * 根据借款人身份证查询家庭成员
	 * @param idCard
	 * @return
	 */
    Result getFamilies(String idCard);
    
    
}
