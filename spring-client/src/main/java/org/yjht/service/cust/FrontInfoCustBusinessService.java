package org.yjht.service.cust;

import org.yjht.bean.cust.FrontInfoCustBusiness;
import org.yjht.core.Result;

public interface FrontInfoCustBusinessService {
	/**
	 * 保存客户经商详细信息
	 * @param business
	 * @return
	 */
	Result saveFrontInfoCustBusiness(FrontInfoCustBusiness business);
	/**
	 * 修改客户经商详细信息
	 * @param business
	 * @return
	 */
	Result updateFrontInfoCustBusiness(FrontInfoCustBusiness business);
	/**
	 * 查找客户经商详细信息列表
	 * @param custId
	 * @return
	 */
	Result findFrontInfoCustBusiness(String custId);
	
	Result findFrontInfoCustBusinessById(String id);
	/**
	 * 删除客户经商详细信息
	 * @param custId
	 * @param id
	 * @return
	 */
	Result deleteFrontInfoCustBusiness(String custId,String id);

	/**
	 * 评级模型：制造业---制造业经营年限
	 * @param custId
	 * 参数依次代表  custId  制造业 运输业 餐饮业 批发零售业 其他行业
	 * @return
	 */
	FrontInfoCustBusiness getBusinessByHy(String custId,String bizHy);

}
