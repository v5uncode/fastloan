package org.yjht.service.cust;

import org.yjht.bean.cust.FrontInfoCustBreed;
import org.yjht.core.Result;

public interface CustBreedService {
	/**
	 * 保存养殖信息
	 * @param breed
	 * @return
	 */
	Result saveBreed(FrontInfoCustBreed breed);
	/**
	 * 修改养殖信息
	 * @param breed
	 * @return
	 */
	Result updateBreed(FrontInfoCustBreed breed);
	/**
	 * 查找养殖信息列表
	 * @param custId
	 * @return
	 */
	Result findBreed(String custId);
	/**
	 * 根据id查找养殖信息
	 * @param id
	 * @return
	 */
	Result findBreedById(String id);
	/**
	 * 删除养殖信息
	 * @param custId
	 * @param id
	 * @return
	 */
	Result deleteBreed(String custId,String id);

	/**
	 * 评级模型：养猪----固定资产
	 * @param custId
	 * @param yzType
	 * @return
	 */
	FrontInfoCustBreed getBreedInfoBy(String custId,String yzType,String yzKid);

	/**
	 * 评级模型：养殖其他----经营期限
	 * @param custId
	 * @param yzType  排除类型
	 * @param yzKing  排除种类
	 * @return
	 */
	String getBreedOtherYears(String custId,String yzType,String yzKing);

}
