package org.yjht.service.cust;

import org.yjht.bean.cust.CustFamily;
import org.yjht.core.Result;

public interface CustFamilyService {
	/**
	 * 维护家庭基本信息
	 * @param custFamily
	 * @return
	 */
	Result saveFamily(CustFamily custFamily);

	Result updateFamily(CustFamily custFamily);
	/**
	 * 查找家庭基本信息
	 * @param custId
	 * @return
	 */
	CustFamily findFamily(String custId); 

}
