package org.yjht.service.cust.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.yjht.bean.cust.CustBusiness;
import org.yjht.core.Result;
import org.yjht.service.base.AbstractService;
import org.yjht.service.base.CompleteService;
import org.yjht.service.cust.CustBusinessService;
import org.yjht.util.DateTools;
import org.yjht.util.ResultGenerator;
@Service
public class CustBusinessServiceImpl extends AbstractService<CustBusiness> implements CustBusinessService {
	@Autowired
	CompleteService<CustBusiness> completeService;
	@Override
	@Transactional
	public Result saveBusiness(CustBusiness business) {
		//查找经商基本信息
		CustBusiness custBusiness = findById(business.getCustId());
		int n=0;
		//有则更新,没有添加
		if(custBusiness!=null) {
			n=update(business);	
		}else {
			n = save(business);
		}
		//修改经商基本信息进度
		completeService.updateCompleteCustInfo(business);
		if(n>0) {
			return ResultGenerator.genSuccessResult(business);
		}else {
			return ResultGenerator.genFailResult("保存失败");
		}
	}

	@Override
	public Result updateBusiness(CustBusiness business) {
		business.setMtnDate(DateTools.getCurrentSysData(DateTools.SIMPLE_FORMAT));
		int n=update(business);
		if(n>0) {
			return ResultGenerator.genSuccessResult(business);
		}else {
			return ResultGenerator.genFailResult("保存失败");
		}
	}

	@Override
	public CustBusiness findBusiness(String custId) {
		CustBusiness custBusiness = findById(custId);
		return custBusiness;
	}

	
}
