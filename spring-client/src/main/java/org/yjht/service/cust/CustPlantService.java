package org.yjht.service.cust;

import org.yjht.bean.cust.CustPlant;
import org.yjht.core.Result;

public interface CustPlantService {
	
	Result findCustPlant(String custId);
	
	Result saveCustPlant(CustPlant plant);
}
