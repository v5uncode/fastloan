package org.yjht.service.cust.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.yjht.bean.cust.CustPlant;
import org.yjht.core.Result;
import org.yjht.service.base.AbstractService;
import org.yjht.service.base.CompleteService;
import org.yjht.service.cust.CustPlantService;
import org.yjht.util.ResultGenerator;
@Service
public class CustPlantServiceImpl extends AbstractService<CustPlant> implements CustPlantService {
	@Autowired
	CompleteService<CustPlant> completeService;
	@Override
	public Result findCustPlant(String custId) {
		CustPlant plant=findById(custId);
		return ResultGenerator.genSuccessResult(plant);
	}

	@Override
	@Transactional
	public Result saveCustPlant(CustPlant plant) {
		int n=0;
		if(findById(plant.getCustId())==null) {
			n=save(plant);
		}else {
			n=update(plant);
		}
		//修改种植基本信息完整度
		completeService.updateCompleteCustInfo(plant);
		if(n>0) {
			return ResultGenerator.genSuccessResult(plant);
		}else {
			return ResultGenerator.genFailResult("保存失败");
		}
	}

}
