package org.yjht.service.cust;


import org.yjht.bean.cust.CustBase;
import org.yjht.core.Page;
import org.yjht.core.Result;
import org.yjht.service.base.Service;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;

public interface CustBaseService extends Service<CustBase>{
	/**
	 * 查询客户信息
	 * @param cust
	 * @param page
	 * @param session
	 * @return
	 */
	Result selectList(CustBase cust,Page page);	
	/**
	 * 修改客户信息
	 * @param cust
	 * @param session
	 * @return
	 */
	Result updateCust(CustBase cust,HttpSession session);
	/**
	 * 删除客户信息
	 * @param custIds
	 * @param session
	 * @return
	 */
	Result deleteCust(List<String> custIds);
	/**
	 * 添加客户信息
	 * @param cust
	 * @param session
	 * @return
	 */
	Result addCust(CustBase cust,HttpSession session);
	/**
	 * 模糊查询客户信息
	 * @param page
	 * @param key 姓名/身份证号/电话号码
	 * @param session
	 * @return
	 */
	Result selectLikeKey(Page page,String key,HttpSession session);
	/**
	 * 通过身份证号码查询
	 * @param idNo
	 * @param session
	 * @return
	 */
	List<Map<String,Object>> selectByIdNo(String idNo,HttpSession session);

	/**
	 * 根据cunstId查询客户基本信息
	 * @param id
	 * @return
	 */
	CustBase findByCustid(String id);
	
	/**
	 * 指定证件查找
	 * @param idType
	 * @param idNo
	 * @return
	 */
	CustBase findByIdNo(String idType,String idNo);

}
