package org.yjht.service.cust.impl;

import java.text.ParseException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.yjht.bean.vo.User;
import org.yjht.core.Result;
import org.yjht.dao.LrdOrgMapper;
import org.yjht.mapper.CustStatisticsMapper;
import org.yjht.service.cust.CustStatisticsService;
import org.yjht.util.CommonUtil;
import org.yjht.util.DateTools;
import org.yjht.util.ResultGenerator;

@Service
public class CustStatisticsServiceImpl implements CustStatisticsService {
	
	@Autowired
	LrdOrgMapper lrdOrgMapper;
	@Autowired
	CustStatisticsMapper custStatisticsMapper;
	@Override
	public Result queryCustSum(HttpServletRequest request) {
		User user = CommonUtil.getUser();
		String userId = user.getUser_id();
		List<Map<String,Object>> list = custStatisticsMapper.queryCustSum(userId);
		return ResultGenerator.genSuccessResult(list);
	}
	@Override
	public Result queryOrgCustSum(HttpServletRequest request,Map<String, Object> map) {
		// TODO Auto-generated method stub
		String orgCd = CommonUtil.getUser().getOrgCD();
		String orgCds = getChilds(orgCd);
		if(map.get("beginDate")!=null&&StringUtils.isNotBlank(map.get("beginDate").toString())){
			map.put("beginDate", DateTools.formatDate(DateTools.parseDate(map.get("beginDate").toString()), DateTools.SIMPLE_FORMAT));			
		}
		if(map.get("endDate")!=null&&StringUtils.isNotBlank(map.get("endDate").toString())){
			map.put("endDate", DateTools.formatDate(DateTools.parseDate(map.get("endDate").toString()), DateTools.SIMPLE_FORMAT));		
		}else{
			map.put("endDate", DateTools.getCurrentSysData(DateTools.SIMPLE_FORMAT));
		}
		
		map.put("orgCds", orgCds);
		List<Map<String,Object>> list =custStatisticsMapper.queryOrgCustSum(map);
		return ResultGenerator.genSuccessResult(list);
	}
	
	@Override
	public Result queryJlCustSum(HttpServletRequest request, Map<String, Object> map) {
		// TODO Auto-generated method stub
		String orgCd = ((User) request.getSession().getAttribute("user")).getOrgCD();
		if(map.get("beginDate")!=null&&StringUtils.isNotBlank(map.get("beginDate").toString())){	
			map.put("beginDate", DateTools.formatDate(DateTools.parseDate(map.get("beginDate").toString()), DateTools.SIMPLE_FORMAT));			
		}
		if(map.get("endDate")!=null&&StringUtils.isNotBlank(map.get("endDate").toString())){
			map.put("endDate", DateTools.formatDate(DateTools.parseDate(map.get("endDate").toString()), DateTools.SIMPLE_FORMAT));
		}else{
			map.put("endDate", DateTools.getCurrentSysData(DateTools.SIMPLE_FORMAT));
		}
		String orgCds = getChilds(orgCd);
		map.put("orgCds", orgCds);
		List<Map<String,Object>> list =custStatisticsMapper.queryJlCustSum(map);
		return ResultGenerator.genSuccessResult(list);
	}
	
	public String getChilds(String map) {
		List<Map<String, Object>> list = lrdOrgMapper.getChilds(map);
		StringBuffer sb = new StringBuffer();
		int count=1;
		for (Map<String, Object> map2 : list) {
			sb.append(map2.get("childIds"));
			sb.append(",");
			count++;
			if(count%5==0){
				sb.deleteCharAt(sb.length() - 1);
				sb.append(") or ORG_CD in (");
			}
		}
		if(",".equals(sb.charAt(sb.length()-1))){
			sb.deleteCharAt(sb.length() - 1);
		}else{
			sb.append(1);
		}
		return sb.toString();
	}

}
