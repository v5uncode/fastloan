package org.yjht.service.cust;


import java.util.Map;

import javax.servlet.http.HttpSession;

import org.yjht.bean.cust.FrontInfoCustYj;
import org.yjht.core.Page;
import org.yjht.core.Result;

public interface CustYjService {
	/**
	 * 查询已处理移交信息
	 * @param page
	 * @param session
	 * @return
	 */
	Result findYjYcl(Page page,HttpSession session);
	/**
	 * 查询待处理移交信息
	 * @param page
	 * @param session
	 * @return
	 */
	Result findYjDcl(Page page,HttpSession session);
	/**
	 * 添加移交申请信息-申请移交
	 * @param custYj
	 * @param session
	 * @return
	 */
	Result saveYjInfo(FrontInfoCustYj custYj,HttpSession session);
	/**
	 * 处理移交申请信息-修改状态
	 * @param custYj
	 * @return
	 */
	Result updateYjInfo(FrontInfoCustYj custYj);
	/**
	 * 接收微信消息处理移交申请信息-修改状态
	 * @param custYj
	 * @return
	 */
	boolean updateStat(FrontInfoCustYj custYj);
	/**
	 * 批量移交
	 * @param userIdSq
	 * @param session
	 * @return
	 */
	Result custYj(Map<String,Object> map,HttpSession session);
	
}
