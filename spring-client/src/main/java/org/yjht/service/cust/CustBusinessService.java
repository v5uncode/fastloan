package org.yjht.service.cust;

import org.yjht.bean.cust.CustBusiness;
import org.yjht.core.Result;

public interface CustBusinessService {
	/**
	 * 维护经商基本信息
	 * @param business
	 * @return
	 */
	Result saveBusiness(CustBusiness business);
	
	Result updateBusiness(CustBusiness business);
	/**
	 * 查找经商基本信息
	 * @param custId
	 * @return
	 */
	CustBusiness findBusiness(String custId);

}
