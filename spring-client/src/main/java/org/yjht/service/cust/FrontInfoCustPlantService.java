package org.yjht.service.cust;

import org.yjht.bean.cust.FrontInfoCustPlant;
import org.yjht.core.Result;

import java.util.List;
import java.util.Map;

public interface FrontInfoCustPlantService {
	/**
	 * 保存客户种植信息
	 * @param plant
	 * @return
	 */
	Result savePlant(FrontInfoCustPlant plant);
	/**
	 * 更新客户种植信息
	 * @param plant
	 * @return
	 */
	Result updatePlant(FrontInfoCustPlant plant);
	/**
	 * 查找客户种植信息列表
	 * @param custId
	 * @return
	 */
	Result findPlant(String custId);
	/**
	 * 根据ID查找客户种植信息
	 * @param id
	 * @return
	 */
	Result findPlantById(String id);
	/**
	 * 删除客户种植信息
	 * @param custId
	 * @param id
	 * @return
	 */
	Result deletePlant(String custId,String id);

	/**
	 * 评级模型：根据custId和种植作物查询种植亩数
	 * @param custId  客户id
	 * @param cropType 作物种类
	 * @param flag 是否属于当前作物种类  是:T 否：F
 	 * @return
	 */
	List<Map<String,Object>> getZzms(String custId,String cropType,String flag);

	/**
	 * 评级模型：种植蔬菜---经营年限
	 * @param custId
	 * @param cropType
	 * @return
	 */
	String getPlantYears(String custId,String cropType);

	/**
	 * 评级模型：种植其他 ----经营年限
	 * @param custId
	 * @param cropType
	 * @return
	 */
	String getOtherPlantYears(String custId,String cropType);
}
