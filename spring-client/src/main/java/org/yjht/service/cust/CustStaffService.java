package org.yjht.service.cust;

import org.yjht.bean.cust.FrontInfoCustStaff;
import org.yjht.core.Result;

public interface CustStaffService {
	/**
	 * 维护职工信息
	 * @param staff
	 * @return
	 */
	Result saveStaff(FrontInfoCustStaff staff);
	
	Result updateStaff(FrontInfoCustStaff staff);
	/**
	 * 获取职工信息
	 * @param custId
	 * @return
	 */
	FrontInfoCustStaff findStaff(String custId);

}
