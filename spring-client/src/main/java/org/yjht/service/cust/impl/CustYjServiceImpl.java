package org.yjht.service.cust.impl;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.yjht.bean.cust.CustBase;
import org.yjht.bean.cust.FrontInfoCustYj;
import org.yjht.bean.vo.User;
import org.yjht.core.Constants;
import org.yjht.core.Page;
import org.yjht.core.Result;
import org.yjht.exception.MyException;
import org.yjht.mapper.cust.CustBaseMapper;
import org.yjht.mapper.cust.FrontInfoCustYjMapper;
import org.yjht.service.base.AbstractService;
import org.yjht.service.cust.CustYjService;
import org.yjht.service.scsp.ScspService;
import org.yjht.util.DateTools;
import org.yjht.util.ResultGenerator;
import org.yjht.util.SendHttpService;
import org.yjht.wechat.bean.khyjInfo;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import tk.mybatis.mapper.entity.Condition;
import tk.mybatis.mapper.entity.Example.Criteria;
@Service
public class CustYjServiceImpl extends AbstractService<FrontInfoCustYj> implements CustYjService {
	@Autowired
	private FrontInfoCustYjMapper frontInfoCustYjMapper;
	@Autowired
	private CustBaseMapper custBaseMapper;
	@Autowired
	private ScspService scspService;
	@Override
	public Result findYjYcl(Page page,HttpSession session) {
		User user=(User) session.getAttribute("user");
		String userId=user.getUser_id();
		String stat="("+Constants.STATYCL+","+Constants.STATJJ+","+Constants.STATPL+")";
		List<Map<String, Object>> list=findYjInfo(userId,stat,page);
		page.setRows(list);
		page.setTotal(new PageInfo<>(list).getTotal());
		return ResultGenerator.genSuccessResult(page);
	}

	@Override
	public Result findYjDcl(Page page,HttpSession session) {
		User user=(User) session.getAttribute("user");
		String userId=user.getUser_id();
		String stat="("+Constants.STATDCL+")";
		List<Map<String, Object>> list=findYjInfo(userId,stat,page);
		for(Map<String, Object> map:list) {
			String user_id = (String) map.get("custGrpJl");// 所属客户经理是当前登录人，则显示处理按钮
			if (user.getUser_id().equals(user_id)) {
				map.put("PROCESS", true);
			} else {
				map.put("PROCESS", false);
			}
		}
		page.setRows(list);
		page.setTotal(new PageInfo<>(list).getTotal());
		return ResultGenerator.genSuccessResult(page);
	}
	
	@Override
	/**
	 * 申请移交
	 */
	public Result saveYjInfo(FrontInfoCustYj custYj,HttpSession session) {
		/*if(scspService.findScsp(custYj.getCustId())) {
			throw new MyException("该客户存在未完成的审查审批记录，不能申请移交");
		}*/
		//先查询该客户是否存在未完成移交信息
		Condition condition = new Condition(FrontInfoCustYj.class);
		Criteria criteria=condition.createCriteria();
		criteria.andEqualTo("custId",custYj.getCustId()).andEqualTo("stat", Constants.STATDCL);
		List<FrontInfoCustYj> list=findByCondition(condition);
		if(!list.isEmpty()) {
			return ResultGenerator.genFailResult("申请失败,该客户正在移交中。");
		}
		User user=(User) session.getAttribute("user");
		custYj.setUserIdSq(user.getUser_id());// 申请人
		custYj.setStat(Constants.STATDCL);
		custYj.setCrtDate(DateTools.getCurrentSysData(DateTools.FULLTIME_FORMAT));
		String yjId=DateTools.getCurrentSysData("yyyyMMddHHmmsss") + (int) ((Math.random() * 9 + 1) * 10000);
		custYj.setYjId(yjId);
		/*发送微信消息*/
		khyjInfo yj=new khyjInfo();
		CustBase cust=custBaseMapper.selectByPrimaryKey(custYj.getCustId());
		yj.setModelType("yjsq");
		yj.setYjId(yjId);//移交id
		yj.setIdNo(cust.getIdNo());//身份证号
		yj.setName(cust.getCustName());//客户姓名
		yj.setManagerId(user.getUser_id());//申请移交的客户经理		
		yj.setDealTime(DateTools.getCurrentSysData(DateTools.DEFAULT_FORMAT));//申请移交时间
		yj.setLastManagerId(cust.getCustGrpJl());//原客户经理
		SendHttpService.sendHttp(yj);		
		int n=save(custYj);
		if(n>0) {
			return ResultGenerator.genSuccessResult();
		}else {
			return ResultGenerator.genFailResult("申请失败");
		}
	}

	/**
	 * 查询移交信息
	 * @param useId
	 * @param stat
	 * @param page
	 * @return
	 */

	public List<Map<String, Object>> findYjInfo(String useId, String stat,Page page) {	
		PageHelper.startPage(page.getPage(),page.getLimit(), true);
		return frontInfoCustYjMapper.findYjInfo(useId, stat);
	}

	@Override
	@Transactional
	public Result updateYjInfo(FrontInfoCustYj custYj) {
		FrontInfoCustYj oldyj=findById(custYj.getYjId());
		if(oldyj.getStat().equals(Constants.STATYCL)||oldyj.getStat().equals(Constants.STATJJ)||oldyj.getStat().equals(Constants.STATPL)) {
			throw new MyException("该客户已经被处理过了,无需再次处理");
		}
		/*删除客户评级结果*/
		//删除客户的评级及审查审批数据。
		/*FlInfoApproval sendFlInfoApproval = new FlInfoApproval();
		sendFlInfoApproval.setCustId(custYj.getCustId());
		scspService.deleteByCustIdToYJ(sendFlInfoApproval);*/
		//根据custid查询客户基本信息
		CustBase custbase=custBaseMapper.selectByPrimaryKey(custYj.getCustId());
		//处理结果为同意时更改客户信息中的客户经理，机构信息
		if(custYj.getStat().equals(Constants.STATYCL)) {
			Map<String,Object> map=frontInfoCustYjMapper.findGjInfo(custYj.getUserIdSq());
			CustBase cust=new CustBase();
			cust.setCustId(custYj.getCustId());
			cust.setCustGrpJl(custYj.getUserIdSq());
			cust.setCustGrpName(map.get("userName").toString());
			cust.setOrgCd(map.get("orgCd").toString());
			cust.setOrgName(map.get("orgName").toString());
			cust.setMtnDate(DateTools.getCurrentSysData(DateTools.SIMPLE_FORMAT));
			custBaseMapper.updateByPrimaryKeySelective(cust);
		}
		String mtnDate=DateTools.getCurrentSysData(DateTools.FULLTIME_FORMAT);
		custYj.setUpdDate(mtnDate);
		int n=update(custYj);
		//发送移交结果信息到微信
		khyjInfo yj=new khyjInfo();
		yj.setModelType("yjjg");
		yj.setIdNo(custbase.getIdNo());//客户身份证号
		yj.setName(custbase.getCustName());//客户姓名
		yj.setManagerId(custbase.getCustGrpJl());//处理人  原客户经理
		yj.setDealTime(mtnDate);//处理时间
		yj.setLastManagerId(custYj.getUserIdSq());//接收客户的客户经理
		yj.setTransferResult(custYj.getStat());
		String content=custYj.getContent();
		if(StringUtils.isNotBlank(content)) {
			yj.setBackReason(content);
		}
		SendHttpService.sendHttp(yj);		
		if(n>0) {
			return ResultGenerator.genSuccessResult();
		}else {
			return ResultGenerator.genFailResult("处理失败");
		}
	}
	@Override
	@Transactional
	public boolean updateStat(FrontInfoCustYj custYj) {
		FrontInfoCustYj oldyj=findById(custYj.getYjId());
		if(oldyj.getStat().equals(Constants.STATYCL)||oldyj.getStat().equals(Constants.STATJJ)||oldyj.getStat().equals(Constants.STATPL)) {
			return false;
		}
		/*删除客户评级结果*/
		//删除客户的评级及审查审批数据。
		/*FlInfoApproval sendFlInfoApproval = new FlInfoApproval();
		sendFlInfoApproval.setCustId(oldyj.getCustId());
		scspService.deleteByCustIdToYJ(sendFlInfoApproval);*/
		//处理结果为同意时更改客户信息中的客户经理，机构信息
		if(custYj.getStat().equals(Constants.STATYCL)) {
			Map<String,Object> map=frontInfoCustYjMapper.findGjInfo(custYj.getUserIdSq());
			CustBase cust=new CustBase();
			cust.setCustId(oldyj.getCustId());
			cust.setCustGrpJl(custYj.getUserIdSq());
			cust.setCustGrpName(map.get("userName").toString());
			cust.setOrgCd(map.get("orgCd").toString());
			cust.setOrgName(map.get("orgName").toString());
			cust.setMtnDate(DateTools.getCurrentSysData(DateTools.SIMPLE_FORMAT));
			custBaseMapper.updateByPrimaryKeySelective(cust);
		}
		String mtnDate=DateTools.getCurrentSysData(DateTools.FULLTIME_FORMAT);
		custYj.setUpdDate(mtnDate);
		update(custYj);
		return true;
	}
	@Override
	@Transactional
	public Result custYj(Map<String,Object> map, HttpSession session) {
		User user=(User) session.getAttribute("user");
		String userIdSq=user.getUser_id();//移交经理
		String userId=map.get("userId").toString();//接收经理		
		if(!scspService.findScspByGrpJl(userIdSq).isEmpty()) {
			throw new MyException("批量移交失败,存在未完成的审查审批记录");
		}
		List<Map<String,Object>> list=custBaseMapper.findCustByJL(userIdSq);
		for (Map<String, Object> cust : list) {		
			/*批量移交时删除客户的评级结果数据。*/
			/*FlInfoApproval sendFlInfoApproval = new FlInfoApproval();
			sendFlInfoApproval.setCustId(cust.get("custId").toString());
			scspService.deleteByCustIdToYJ(sendFlInfoApproval);*/
			//添加批量移交记录
			FrontInfoCustYj custYj=new FrontInfoCustYj();
			custYj.setCustId(cust.get("custId").toString());
			custYj.setUserId(userId);
			custYj.setUserIdSq(userIdSq);
			custYj.setStat(Constants.STATPL);
			custYj.setYjId(DateTools.getCurrentSysData("yyyyMMddHHmmssSSS") + (int) ((Math.random() * 9 + 1) * 10000));
			custYj.setCrtDate(DateTools.getCurrentSysData(DateTools.FULLTIME_FORMAT));
			custYj.setUpdDate(DateTools.getCurrentSysData(DateTools.FULLTIME_FORMAT));
			save(custYj);
		}
		map.put("userIdSq", userIdSq);
		map.put("userName",map.get("custGrpName"));
		custBaseMapper.updateCustJL(map);
		return ResultGenerator.genSuccessResult();
	}

	

}
