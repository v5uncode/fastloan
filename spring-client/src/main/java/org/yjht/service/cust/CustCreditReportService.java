package org.yjht.service.cust;

import org.yjht.bean.cust.CustCreditReport;
import org.yjht.core.Result;

public interface CustCreditReportService {
	/**
	 * 查找客户征信信息
	 * @param custId
	 * @return
	 */
	Result findCustCreditReport(String custId);
	/**
	 * 维护客户征信信息
	 * @param custCreditReport
	 * @return
	 */
	Result saveCustCreditReport(CustCreditReport custCreditReport);

	/**
	 * 根据客户证件类型 证件号查询征信信息
	 * @param idNo
	 * @param idType
	 * @return
	 */
    CustCreditReport getCreditInfoByIdNo(String idNo,String idType);

	/**
	 * 根据custId查询征信数据
	 * @param custId
	 * @return
	 */
	CustCreditReport getCreditInfoByCustId(String custId);

}
