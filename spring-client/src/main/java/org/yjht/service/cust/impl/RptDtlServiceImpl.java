package org.yjht.service.cust.impl;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.yjht.bean.cust.FrontInfoRptDtl;
import org.yjht.core.Result;
import org.yjht.mapper.cust.FrontInfoFinRptMapper;
import org.yjht.mapper.cust.FrontInfoRptDtlMapper;
import org.yjht.service.base.AbstractService;
import org.yjht.service.cust.RptDtlService;
import org.yjht.util.ResultGenerator;
import org.yjht.util.StringUtil;
import tk.mybatis.mapper.entity.Condition;
import tk.mybatis.mapper.entity.Example.Criteria;

import java.util.List;
import java.util.Map;

@Service
public class RptDtlServiceImpl extends AbstractService<FrontInfoRptDtl> implements RptDtlService {
	@Autowired
	FrontInfoFinRptMapper frontInfoFinRptMapper;

	@Autowired
	FrontInfoRptDtlMapper frontInfoRptDtlMapper;
	@Override
	public Result findRptDtl(String custId, String subjCd) {
		Condition condition = new Condition(FrontInfoRptDtl.class);
		Criteria criteria=condition.createCriteria();
		if(StringUtils.isNotBlank(custId)&&StringUtils.isNotBlank(subjCd)) {
			criteria.andEqualTo("custId",custId);
			criteria.andEqualTo("subjCd",subjCd);
			List<FrontInfoRptDtl> list=findByCondition(condition);
			return ResultGenerator.genSuccessResult(list);
		}else {
			return ResultGenerator.genFailResult("参数错误");
		}
	}

	@Override
	@Transactional
	public Result saveRptDtl(List<FrontInfoRptDtl> rptDtls) {
		int n=0;
		for(FrontInfoRptDtl rptDtl:rptDtls) {
			if(StringUtils.isBlank(rptDtl.getRptDtlId())) {
				rptDtl.setRptDtlId(StringUtil.getUUID());
				n=save(rptDtl);			
			}else {
				n=update(rptDtl);
			}
		}
		if(n>0) {
			return ResultGenerator.genSuccessResult();
		}else {
			return ResultGenerator.genFailResult("保存失败");
		}
		
	}

	@Override
	public Result updateRptDtl(FrontInfoRptDtl rptDtl) {
		int n=update(rptDtl);
		if(n>0) {
			return ResultGenerator.genSuccessResult();
		}else {
			return ResultGenerator.genFailResult("修改失败");
		}
	}

	@Override
	public Result deleteRptDtl(String rptDtlId) {
		int n=deleteById(rptDtlId);
		if(n>0) {
			return ResultGenerator.genSuccessResult();
		}else {
			return ResultGenerator.genFailResult("删除失败");
		}
	}

	@Override
	public List<Map<String,String>> findSrRptDtlById(String custId) {
		List<Map<String,String>> list= frontInfoRptDtlMapper.findSrRptDtlById(custId);
		return list;
	}

	/**
	 * 策略引擎接口使用：根据custId subj_cd='A0005' subj_type='08' 查询数据
	 * @param custId
	 * @return
	 */
	@Override
	public Integer getDtlInfoByCustId(String custId) {
		return frontInfoRptDtlMapper.getDtlInfoByCustId(custId);
	}
}
