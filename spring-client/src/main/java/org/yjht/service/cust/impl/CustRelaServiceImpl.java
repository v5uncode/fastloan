package org.yjht.service.cust.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.yjht.bean.cust.CustBase;
import org.yjht.bean.cust.FrontInfoCustRela;
import org.yjht.core.Constants;
import org.yjht.core.Result;
import org.yjht.enmus.IdType;
import org.yjht.enums.RelaType;
import org.yjht.exception.MyException;
import org.yjht.mapper.cust.CustBaseMapper;
import org.yjht.mapper.cust.FrontInfoCustRelaMapper;
import org.yjht.service.base.AbstractService;
import org.yjht.service.cust.CustRelaService;
import org.yjht.util.DataCheckUtils;
import org.yjht.util.DateTools;
import org.yjht.util.DicReplace;
import org.yjht.util.ResultGenerator;

import tk.mybatis.mapper.entity.Condition;
import tk.mybatis.mapper.entity.Example.Criteria;

@Service
public class CustRelaServiceImpl extends AbstractService<FrontInfoCustRela> implements CustRelaService {
    @Autowired
    private FrontInfoCustRelaMapper frontInfoCustRelaMapper;
    @Autowired
    private CustBaseMapper baseMapper;

    @Override
    public Result saveRela(FrontInfoCustRela rela) {
        //验证身份证号和手机号
        String idNo = rela.getIdNo();
        String idType = rela.getIdType();
        if (StringUtils.isBlank(idType)) {
            rela.setIdType(IdType.IDCARD.getCode());
        }
        String telNo = rela.getTelNo();
        try {
            telNo = DataCheckUtils.checkTelNo(telNo);
        } catch (MyException e) {
            throw new MyException("添加关系人失败," + e.getMessage());
        }
        rela.setTelNo(telNo);
        //检查是否已经添加该关系人
        if (isExistRela(rela)) {
            throw new MyException("证件号码为" + idNo + "的关系人已经存在");
        }
        //验证配偶唯一性
        if (rela.getRelaType().equals(RelaType.getCode("配偶"))) {
            if (isExistPartner(rela)) {
                List<Map<String, Object>> list = frontInfoCustRelaMapper.getRelaLocation(rela.getIdNo(),RelaType.getCode("配偶"));
                Map<String, Object> map = list.get(0);
                return ResultGenerator.genFailResult("配偶信息已存在,"
                        + "	所属法人:" + map.get("CORP_NAME") + ",	所属机构:" + map.get("ORG_NAME") +
                        ",	所属客户经理:" + map.get("CUST_GRP_NAME") + ",	客户名称:" + map.get("CUST_NAME"));
            }
        }
        //验证父母
        if (rela.getRelaType().equals(RelaType.getCode("父母"))) {
            if (isExistParent(rela)) {
                String sex = rela.getSex();
                if (Constants.MAN.equals(sex)) {
                    return ResultGenerator.genFailResult("父亲信息已存在");
                } else if (Constants.WOMAN.equals(sex)) {
                    return ResultGenerator.genFailResult("母亲信息已存在");
                } else {
                    return ResultGenerator.genFailResult("该父母信息已存在");
                }
            }
        }
        rela.setIsdel(Constants.ISDEL_NO);
        rela.setSeqNo(DateTools.getCurrentSysData("yyyyMMddHHmmsss") + (int) ((Math.random() * 9 + 1) * 10000));
        int n = save(rela);

        if (n > 0) {
            return ResultGenerator.genSuccessResult(rela);
        } else {
            return ResultGenerator.genFailResult("关系人保存失败");
        }
  
    }

    @Override
    public Result updateRela(FrontInfoCustRela rela) {

        //验证身份证号和手机号
        String idNo = rela.getIdNo();
        String idType = rela.getIdType();
        if (StringUtils.isBlank(idType)) {
            rela.setIdType(IdType.IDCARD.getCode());
        }
        String telNo = rela.getTelNo();
        try {
            telNo = DataCheckUtils.checkTelNo(telNo);
        } catch (MyException e) {
            throw new MyException("添加关系人失败," + e.getMessage());
        }
        rela.setTelNo(telNo);
        //检查是否已经添加该关系人
        if (isExistRela(rela)) {
            throw new MyException("证件号码为" + idNo + "的关系人已经存在");
        }
        //验证配偶唯一性
        if (rela.getRelaType().equals(RelaType.getCode("配偶"))) {
            if (isExistPartner(rela)) {
                List<Map<String, Object>> list = frontInfoCustRelaMapper.getRelaLocation(rela.getIdNo(),RelaType.getCode("配偶"));
                Map<String, Object> map = list.get(0);
                return ResultGenerator.genFailResult("配偶信息已存在,所属法人:" + map.get("CORP_NAME") + ",所属机构:" + map.get("ORG_NAME") +
                        ",所属客户经理:" + map.get("CUST_GRP_NAME") + ",客户名称:" + map.get("CUST_NAME"));
            }
        }
        //验证父母
        if (rela.getRelaType().equals(RelaType.getCode("父母"))) {
            if (isExistParent(rela)) {
                String sex = rela.getSex();
                if (Constants.MAN.equals(sex)) {
                    return ResultGenerator.genFailResult("父亲信息已存在");
                } else if (Constants.WOMAN.equals(sex)) {
                    return ResultGenerator.genFailResult("母亲信息已存在");
                } else {
                    return ResultGenerator.genFailResult("该父母信息已存在");
                }
            }
        }

        int n = update(rela);
        if (n > 0) {
            return ResultGenerator.genSuccessResult(rela);
        } else {
            return ResultGenerator.genFailResult("关系人修改失败");
        }
    }

    @Override
    public List<FrontInfoCustRela> findRela(String custId) {
        Condition condition = new Condition(FrontInfoCustRela.class);
        Criteria criteria = condition.createCriteria();
        criteria.andEqualTo("custId", custId);
        criteria.andEqualTo("isdel", Constants.ISDEL_NO);
        List<FrontInfoCustRela> list = findByCondition(condition);
        for (FrontInfoCustRela rela : list) {
            //TODO 把0改为身份证的枚举值
            //TODO 其他证件判断年龄的方式
            if (IdType.IDCARD.getCode().equals(rela.getIdType())) {
                rela.setAge(DateTools.getCurrentYear() - Integer.parseInt(rela.getIdNo().substring(6, 10)));
            }
        }
        return list;
    }

    @Override
    public Result findRelaById(String seqNo) {
        FrontInfoCustRela rela = findById(seqNo);

        return ResultGenerator.genSuccessResult(rela);
    }

    @Override
    public Result deleteRela(String custId, String seqNo) {
        FrontInfoCustRela rela = findById(seqNo);
        if (rela == null) {
            return ResultGenerator.genFailResult("家庭成员不存在");
        } else {
            rela.setIsdel(Constants.ISDEL_YES);
            int n = update(rela);
            if (n > 0) {
                return ResultGenerator.genSuccessResult();
            } else {
                return ResultGenerator.genFailResult("删除失败");
            }
        }
    }

    /**
     * 父母唯一性校验
     * @param rela
     * @return
     */
    public boolean isExistParent(FrontInfoCustRela rela) {
        boolean flag = false;
        Condition condition = new Condition(FrontInfoCustRela.class);
        Criteria criteria = condition.createCriteria();
        criteria.andEqualTo("relaType", RelaType.getCode("父母"));
        /*if (StringUtils.isNotBlank(rela.getIdType())) {
            criteria.andEqualTo("idType", rela.getIdType());
        }*/
        /*if (StringUtils.isNotBlank(rela.getIdNo())) {
            criteria.andEqualTo("idNo", rela.getIdNo());
        }*/
        if (StringUtils.isNotBlank(rela.getCustId())) {
            criteria.andEqualTo("custId", rela.getCustId());
        }
        if (StringUtils.isNotBlank(rela.getSex())) {
            criteria.andEqualTo("sex", rela.getSex());
        }
        if (StringUtils.isNotBlank(rela.getSeqNo())) {
            criteria.andNotEqualTo("seqNo", rela.getSeqNo());
        }

        criteria.andEqualTo("isdel", Constants.ISDEL_NO);
        List<FrontInfoCustRela> list = findByCondition(condition);
        if (!list.isEmpty()) {
            flag = true;
        }
        return flag;
    }


    /**
     * 验证配偶唯一性
     *
     * @return
     */
    public boolean isExistPartner(FrontInfoCustRela rela) {
        boolean flag = false;
        Condition condition = new Condition(FrontInfoCustRela.class);
        Criteria criteria = condition.createCriteria();
        criteria.andEqualTo("relaType", RelaType.getCode("配偶"));
        if (StringUtils.isNotBlank(rela.getIdType())) {
            criteria.andEqualTo("idType", rela.getIdType());
        }
        if (StringUtils.isNotBlank(rela.getIdNo())) {
            criteria.andEqualTo("idNo", rela.getIdNo());
        }
        if (StringUtils.isNotBlank(rela.getSeqNo())) {
            criteria.andNotEqualTo("seqNo", rela.getSeqNo());
        }
        criteria.andEqualTo("isdel", Constants.ISDEL_NO);
        List<FrontInfoCustRela> list = findByCondition(condition);
        if (!list.isEmpty()) {
            flag = true;
        }
        return flag;
    }

    /**
     * 验证该关系人是否已经存在
     *
     * @param rela
     * @return
     */
    public boolean isExistRela(FrontInfoCustRela rela) {
        boolean flag = false;
        Condition condition = new Condition(FrontInfoCustRela.class);
        Criteria criteria = condition.createCriteria();
        if (StringUtils.isNotBlank(rela.getIdType())) {
            criteria.andEqualTo("idType", rela.getIdType());
        }
        if (StringUtils.isNotBlank(rela.getIdNo())) {
            criteria.andEqualTo("idNo", rela.getIdNo());
        }
        if (StringUtils.isNotBlank(rela.getCustId())) {
            criteria.andEqualTo("custId", rela.getCustId());
        }
        if (StringUtils.isNotBlank(rela.getSeqNo())) {
            criteria.andNotEqualTo("seqNo", rela.getSeqNo());
        }
        criteria.andEqualTo("isdel", Constants.ISDEL_NO);
        List<FrontInfoCustRela> list = findByCondition(condition);
        if (!list.isEmpty()) {
            flag = true;
        }
        return flag;
    }

    /**
     * 查询审查审批家庭成员信息
     */
    @Override
    public Result findScspRela(String custId) {
        // TODO Auto-generated method stub
        Condition condition = new Condition(FrontInfoCustRela.class);
        Criteria criteria = condition.createCriteria();
        criteria.andEqualTo("custId", custId);
        criteria.andEqualTo("isdel", Constants.ISDEL_NO);
        List<FrontInfoCustRela> list = findByCondition(condition);
        Map<String, String> dicMap = new HashMap<String, String>();
        dicMap.put("sex", "Sex");
        dicMap.put("relaType", "RELA_TYPE");
        dicMap.put("idType", "id_Type");
        DicReplace.replaceDicList(list, dicMap);
        return ResultGenerator.genSuccessResult(list);
    }

    @Override
    public Result getFamilies(String idCard) {
        CustBase base = new CustBase();
        base.setIdNo(idCard);
        base.setIsdel(Constants.ISDEL_NO);
        base = baseMapper.selectOne(base);
        if (base != null) {
            return this.findScspRela(base.getCustId());
        } else {
            Map<String, Object> flag = new HashMap<>();
            flag.put("flag", false);
            return ResultGenerator.genSuccessResult(flag);
        }
    }

}
