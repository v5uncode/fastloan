package org.yjht.service.cust;

import org.yjht.core.Result;

public interface CompleteCustInfoService {

	Result getCustInfoComplete(String custId);

}
