package org.yjht.service.cust;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.yjht.core.Result;

public interface CustStatisticsService {

	Result queryCustSum(HttpServletRequest request);

	Result queryOrgCustSum(HttpServletRequest request, Map<String, Object> map);

	Result queryJlCustSum(HttpServletRequest request, Map<String, Object> map);


}
