package org.yjht.service.cust.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.yjht.bean.vo.User;
import org.yjht.core.Result;
import org.yjht.mapper.TreeMapper;
import org.yjht.service.cust.TreeService;
import org.yjht.util.ResultGenerator;

@Service
public class TreeServiceImpl implements TreeService {
	@Autowired
	TreeMapper treeMapper;
	@Override
	@Cacheable(value = "yjTree",key="#corpCd")//移交树放入缓存
	public Result findYjTree(String corpCd) {
		List<Map<String, Object>> result=new ArrayList<Map<String, Object>>();
		List<Map<String, Object>> listOrg=treeMapper.findTreeOrg(corpCd);
		List<Map<String, Object>> listUser=treeMapper.findTreeJL(corpCd);
		result.addAll(listOrg);
		result.addAll(listUser);
		return ResultGenerator.genSuccessResult(result);
	}
	
	
	@CachePut(value = "yjTree",key="#corpCd")//机构或经理发生变化时调用此方法
	public Result findYjTree1(String corpCd) {
		List<Map<String, Object>> result=new ArrayList<Map<String, Object>>();
		List<Map<String, Object>> listOrg=treeMapper.findTreeOrg(corpCd);
		List<Map<String, Object>> listUser=treeMapper.findTreeJL(corpCd);
		result.addAll(listOrg);
		result.addAll(listUser);
		return ResultGenerator.genSuccessResult(result);
	}


	@Override
	public Result findUserAndOrg(String userName,HttpSession session) {
		User user=(User) session.getAttribute("user");
		List<Map<String, Object>> list=treeMapper.findUserAndOrg(user.getCorpCD(),userName);
		return ResultGenerator.genSuccessResult(list);
	}
	
	
}
