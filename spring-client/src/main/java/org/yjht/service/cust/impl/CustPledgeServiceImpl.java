package org.yjht.service.cust.impl;

import com.yjht.seq.sequence.Sequence;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.yjht.bean.cust.CustMortgage;
import org.yjht.bean.cust.CustPledge;
import org.yjht.core.CommonConstant;
import org.yjht.core.Result;
import org.yjht.service.base.AbstractService;
import org.yjht.service.cust.CustPledgeService;
import org.yjht.util.CommonUtil;
import org.yjht.util.ResultGenerator;
import org.yjht.util.StringUtil;
import tk.mybatis.mapper.entity.Condition;
import tk.mybatis.mapper.entity.Example;


/**
 * @author lengleng
 * @date 2018/3/19
 */
@Service
public class CustPledgeServiceImpl extends AbstractService<CustPledge> implements CustPledgeService {
    @Autowired
    private Sequence sequence;

    /**
     * 查询客户的质押信息
     *
     * @param custId 客户号
     * @return Result
     */
    @Override
    public Result findPledge(String custId) {
        Condition condition = new Condition(CustMortgage.class);
        Example.Criteria criteria = condition.createCriteria();
        criteria.andEqualTo("custId", custId);
        return ResultGenerator.genSuccessResult(findByCondition(condition));
    }

    /**
     * 修改质押信息
     *
     * @param custPledge 质押信息
     * @return Result
     */
    @Override
    public Result editCustPledge(CustPledge custPledge) {
        return ResultGenerator.genSuccessResult(update(custPledge));
    }

    /**
     * 新增质押信息
     *
     * @param custPledge 质押信息
     * @return Result
     */
    @Override
    public Result addCustPledge(CustPledge custPledge) {
        custPledge.setPledgeId(StringUtil.getUUID());
        return ResultGenerator.genSuccessResult(save(custPledge));
    }

    /**
     * 删除质押信息
     *
     * @param id 抵押信息ID
     * @return Result
     */
    @Override
    public Result deletePledge(String id) {
        return ResultGenerator.genSuccessResult(deleteById(id));
    }
}
