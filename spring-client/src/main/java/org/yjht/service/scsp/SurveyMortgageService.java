package org.yjht.service.scsp;

import java.util.List;

import org.yjht.bean.scsp.FrontInfoSurveyMortgage;
import org.yjht.service.base.Service;

/**
 * 调查报告抵押信息服务接口
 * @author zhaolulu
 *
 */
public interface SurveyMortgageService extends Service<FrontInfoSurveyMortgage>{
	
	List<FrontInfoSurveyMortgage> findByScspId(String scspId);

}
