package org.yjht.service.scsp;

import org.yjht.bean.FrontInfoLoan;
import org.yjht.bean.scsp.FrontInfoCredit;

import java.util.List;

/**
 * @author lengleng
 * @date 2018/6/1
 * 放款信息业务类
 */
public interface LoanInfoService {
    /**
     * 插入放款信息
     *
     * @param credit
     * @param status
     */
    void insertLoanInfo(FrontInfoCredit credit, String status);

    /**
     * 根据合同编号查询放款记录
     *
     * @param scspId 合同编号
     * @return List<FrontInfoLoan>
     */
    List<FrontInfoLoan> findLoanInfoByScspId(String scspId);
}
