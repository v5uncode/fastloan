package org.yjht.service.scsp.impl;

import java.util.List;

import org.springframework.stereotype.Service;
import org.yjht.bean.scsp.FrontInfoSurveyPledge;
import org.yjht.service.base.AbstractService;
import org.yjht.service.scsp.SurveyPledgeService;

import tk.mybatis.mapper.entity.Condition;
@Service
public class SurveyPledgeServiceImpl extends AbstractService<FrontInfoSurveyPledge> implements SurveyPledgeService{

	@Override
	public List<FrontInfoSurveyPledge> findByScspId(String scspId) {
		Condition condition = new Condition(FrontInfoSurveyPledge.class);
		condition.createCriteria().andEqualTo("scspId", scspId);	
		return findByCondition(condition);
	}

}
