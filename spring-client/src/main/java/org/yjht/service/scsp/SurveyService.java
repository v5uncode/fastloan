package org.yjht.service.scsp;

import org.yjht.bean.FlInfoApprovalVo;
import org.yjht.bean.scsp.FrontInfoSurvey;
import org.yjht.bean.scsp.vo.SurveyReportVo;
import org.yjht.service.base.Service;

public interface SurveyService extends Service<FrontInfoSurvey>{
	/**
	 * 获取调查报告
	 * @param scspId
	 * @return
	 */
	SurveyReportVo getReport(String scspId);

    /**
     * 评级授信后插入调查报告等相关表
     * @param custId
     * @return
     */
    FrontInfoSurvey insertSurvey(FlInfoApprovalVo approval);
    
    /**
     * 修改调查报告信息
     * @param approval
     * @return
     */
    void updateSurvey(FrontInfoSurvey survey);

}



