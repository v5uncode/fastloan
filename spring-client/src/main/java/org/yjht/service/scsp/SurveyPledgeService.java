package org.yjht.service.scsp;

import java.util.List;

import org.yjht.bean.scsp.FrontInfoSurveyPledge;
import org.yjht.service.base.Service;

/**
 * 调查报告质押信息服务接口
 * @author zhaolulu
 *
 */
public interface SurveyPledgeService extends Service<FrontInfoSurveyPledge>{
	
	List<FrontInfoSurveyPledge> findByScspId(String scspId);

}
