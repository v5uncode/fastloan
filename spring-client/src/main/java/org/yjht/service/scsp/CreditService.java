package org.yjht.service.scsp;

import org.yjht.bean.scsp.FrontInfoCredit;
import org.yjht.service.base.Service;

/**
 * 授信方案
 * @author zhanghua
 */
public interface CreditService extends Service<FrontInfoCredit> {
    /**
     * 授信方案初始化：根据scspId查询授信信息
     * @param scspId
     * @return
     */
    FrontInfoCredit findCreditInfoById( String scspId);

    /**
     * 授信方案：根据id保存
     * @param credit
     * @return
     */
    int updateCreditById(FrontInfoCredit credit);
    /**
     * 插入授信方案
     * @param credit
     */
    void insertCredit(FrontInfoCredit credit);
}
