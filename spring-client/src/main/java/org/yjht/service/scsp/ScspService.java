package org.yjht.service.scsp;

import java.util.List;

import org.yjht.base.BaseService;
import org.yjht.bean.FrontInfoScsp;

public interface ScspService extends BaseService<FrontInfoScsp, String>{	
	/**
	 * 根据客户id查找流程未结束的审查审批记录
	 * @param custId
	 * @return
	 */
	List<FrontInfoScsp> findScspByCustId(String custId);
	/**
	 * 根据客户经理编号查找流程未结束的审查审批记录
	 * @param grpJlid
	 * @return
	 */
	List<FrontInfoScsp> findScspByGrpJl(String userId);
	/**
	 * 维护审查审批
	 * @param scsp
	 */
	void saveScsp(FrontInfoScsp scsp);
	/**
	 * 根据审查审批id删除审查审批流程信息
	 * @param scspId
	 */
	void deleteScsp(String scspId);
	

}
