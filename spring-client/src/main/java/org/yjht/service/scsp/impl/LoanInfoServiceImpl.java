package org.yjht.service.scsp.impl;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.yjht.bean.FrontInfoLoan;
import org.yjht.bean.cust.CustBase;
import org.yjht.bean.scsp.FrontInfoCredit;
import org.yjht.bean.vo.User;
import org.yjht.core.Constants;
import org.yjht.mapper.FrontInfoLoanMapper;
import org.yjht.mapper.cust.CustBaseMapper;
import org.yjht.service.base.AbstractService;
import org.yjht.service.scsp.LoanInfoService;
import org.yjht.util.CommonUtil;
import org.yjht.util.DateTools;
import org.yjht.util.SendHttpService;
import org.yjht.wechat.bean.LoanResult;
import tk.mybatis.mapper.entity.Example;

import java.util.Date;
import java.util.List;

/**
 * @author lengleng
 * @date 2018/6/1
 * 放款信息业务类
 */
@Slf4j
@Service
public class LoanInfoServiceImpl extends AbstractService<FrontInfoLoan> implements LoanInfoService {
    @Autowired
    private FrontInfoLoanMapper frontInfoLoanMapper;
    @Autowired
    private CustBaseMapper custBaseMapper;

    /**
     * 插入放款信息
     *
     * @param credit
     * @param status
     */
    @Override
    public void insertLoanInfo(FrontInfoCredit credit, String status) {
        FrontInfoLoan infoLoan = new FrontInfoLoan();
        infoLoan.setLoanId(DateTools.getCurrentSysData("yyyyMMddHHmmsss") + (int) ((Math.random() * 9 + 1) * 10000));
        infoLoan.setCustId(credit.getCustId());
        infoLoan.setCustName(credit.getCustName());
        infoLoan.setScspId(credit.getScspId());
        infoLoan.setLoanDate(DateUtil.format(new Date(), DatePattern.PURE_DATE_PATTERN));
        User user = CommonUtil.getUser();
        infoLoan.setOrgCd(user.getOrgCD());
        infoLoan.setCorpCd(user.getCorpCD());
        infoLoan.setOrgName(user.getOrgName());
        infoLoan.setCustGrpJl(credit.getCustGrpJl());
        infoLoan.setCustGrpName(credit.getCustGrpName());
        infoLoan.setLoanMoney(credit.getGrantMoney());
        infoLoan.setApplyLimit(credit.getGrantTerm());
        infoLoan.setCrtDate(DateTools.getCurrentSysData(DateTools.FULLTIME_FORMAT));
        infoLoan.setLoanStatus(status);
        frontInfoLoanMapper.insert(infoLoan);
        //放款成功，向微信端发送放款结果
        if(Constants.FKCG.equals(status)){
        //if(1 == 1){
            sendFkjgToWx(infoLoan);
        }
    }
    /**
     * 向微信端发送放款结果
     */
    public void sendFkjgToWx(FrontInfoLoan loan){
        LoanResult loanResult = new LoanResult();
        loanResult.setModelType("fkjg");
        loanResult.setLoanMoney(loan.getLoanMoney());
        Example example = new Example(CustBase.class);
        example.createCriteria().andEqualTo("custId",loan.getCustId()).andEqualTo("isdel","0");
        example.setOrderByClause("CRT_DATE DESC");
        List<CustBase> bases = custBaseMapper.selectByExample(example);
        if(bases != null &&bases.size() > 0){
            CustBase base = bases.get(0);
            loanResult.setIdNo(base.getIdNo());
            loanResult.setName(base.getCustName());
            loanResult.setManagerId(base.getCustGrpJl());
            loanResult.setPhone(base.getTelNo());
        }
        loanResult.setDealTime(DateTools.getCurrentSysData("yyyy-MM-dd HH:mm:ss"));
        SendHttpService.sendHttp(loanResult);

    }


    /**
     * 根据合同编号查询放款记录
     *
     * @param scspId 合同编号
     * @return List<FrontInfoLoan>
     */
    @Override
    public List<FrontInfoLoan> findLoanInfoByScspId(String scspId) {
        Example example = new Example(FrontInfoLoan.class);
        example.setOrderByClause("CRT_DATE DESC");
        example.createCriteria().andEqualTo("scspId", scspId);
        return frontInfoLoanMapper.selectByExample(example);
    }
}
