package org.yjht.service.scsp.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.yjht.bean.FrontInfoScsp;
import org.yjht.bean.scsp.FrontInfoCredit;
import org.yjht.core.Constants;
import org.yjht.service.base.AbstractService;
import org.yjht.service.scsp.CreditService;
import org.yjht.service.scsp.ScspService;

@Service
public class CreditServiceImpl extends AbstractService<FrontInfoCredit> implements CreditService {

    @Autowired
    private ScspService scspService;

    @Override
    public FrontInfoCredit findCreditInfoById(String scspId) {
        return findById(scspId);
    }

    @Override
    public int updateCreditById(FrontInfoCredit credit) {
        int n = update(credit);
        FrontInfoScsp scsp=new FrontInfoScsp();
        scsp.setScspId(credit.getScspId());
        scsp.setCreditState(Constants.YES);
        scspService.updateByPrimaryKeySelective(scsp);
        return n;
    }

	@Override
	public void insertCredit(FrontInfoCredit credit) {
		save(credit);
		
	}
}
