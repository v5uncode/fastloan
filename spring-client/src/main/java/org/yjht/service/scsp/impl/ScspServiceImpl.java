package org.yjht.service.scsp.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.yjht.base.impl.BaseServiceImpl;
import org.yjht.bean.FrontInfoScsp;
import org.yjht.bean.scsp.FrontInfoCredit;
import org.yjht.bean.scsp.FrontInfoSurvey;
import org.yjht.bean.scsp.FrontInfoSurveyBreed;
import org.yjht.bean.scsp.FrontInfoSurveyBusiness;
import org.yjht.bean.scsp.FrontInfoSurveyPlant;
import org.yjht.bean.scsp.FrontInfoSurveyRela;
import org.yjht.core.Constants;
import org.yjht.mapper.scsp.FrontInfoSurveyBreedMapper;
import org.yjht.mapper.scsp.FrontInfoSurveyBusinessMapper;
import org.yjht.mapper.scsp.FrontInfoSurveyPlantMapper;
import org.yjht.mapper.scsp.FrontInfoSurveyRelaMapper;
import org.yjht.service.scsp.CreditService;
import org.yjht.service.scsp.GuarantorService;
import org.yjht.service.scsp.ScspService;
import org.yjht.service.scsp.SurveyMortgageService;
import org.yjht.service.scsp.SurveyPledgeService;
import org.yjht.service.scsp.SurveyService;

import tk.mybatis.mapper.entity.Condition;
import tk.mybatis.mapper.entity.Example.Criteria;
@Service
public class ScspServiceImpl extends BaseServiceImpl<FrontInfoScsp, String> implements ScspService {
	@Autowired
	private SurveyService surveyService;
	@Autowired
	private CreditService creditService;
	@Autowired
	private SurveyMortgageService surveyMortgageService;
	@Autowired
	private SurveyPledgeService surveyPledgeService;
	@Autowired
	private GuarantorService guarantorService;
	@Autowired
    private FrontInfoSurveyBusinessMapper surveyBusinessMapper;//报告经商列表
    @Autowired
    private FrontInfoSurveyBreedMapper surveyBreedMapper;//报告养殖
    @Autowired
    private FrontInfoSurveyPlantMapper surveyPlantMapper;//报告种植
    @Autowired
    private FrontInfoSurveyRelaMapper relaMapper;//报告关系人
	
	@Override
	public List<FrontInfoScsp> findScspByCustId(String custId) {
		Condition condition = new Condition(FrontInfoScsp.class);
		Criteria criteria=condition.createCriteria();
		criteria.andEqualTo("custId", custId);
		List<String> list=new ArrayList<String>();
		list.add(Constants.STATWTJ);
		list.add(Constants.STATLCZ);
		criteria.andIn("processState", list);
		return selectByCondition(condition);
	}

	@Override
	@Transactional
	public void saveScsp(FrontInfoScsp scsp) {
		updateByPrimaryKeySelective(scsp);  //更新审查审批流程表
		if(StringUtils.isNotBlank(scsp.getWarrant())){//维护担保方式
			FrontInfoSurvey survey = new FrontInfoSurvey();
			survey.setScspId(scsp.getScspId());
			survey.setWarrantType(scsp.getWarrant());
			surveyService.update(survey);//更新调查报告担保方式
			FrontInfoCredit credit = new FrontInfoCredit();
			credit.setScspId(scsp.getScspId());
			credit.setGuaranteeWay(scsp.getWarrant());
			creditService.update(credit);//更新授信方案担保方式
			
		}		
	}

	@Override
	public List<FrontInfoScsp> findScspByGrpJl(String userId) {
		Condition condition = new Condition(FrontInfoScsp.class);
		Criteria criteria=condition.createCriteria();
		criteria.andEqualTo("custGrpJl", userId);
		List<String> list=new ArrayList<String>();
		list.add(Constants.STATWTJ);
		list.add(Constants.STATLCZ);
		criteria.andIn("processState", list);
		return selectByCondition(condition);
	}

	@Override
	@Transactional
	public void deleteScsp(String scspId) {
		FrontInfoScsp scsp = selectByPrimaryKey(scspId);
		String zyxm=scsp.getMainProject();
		//删除审批流程  同时删除关联信息
		deleteByPrimaryKey(scspId);
		surveyService.deleteById(scspId);
		creditService.deleteById(scspId);
		surveyMortgageService.deleteById(scspId);
		surveyPledgeService.deleteById(scspId);
		guarantorService.deleteByScspId(scspId);
		Condition relacondition = new Condition(FrontInfoSurveyRela.class);
		relacondition.createCriteria().andEqualTo("scspId", scspId);
		relaMapper.deleteByExample(relacondition);
		if(StringUtils.isNotBlank(zyxm)&&!zyxm.equals(Constants.MAIN05)){
			Condition condition=null;
			switch (zyxm) {
				case Constants.MAIN01: //种植
		        case Constants.MAIN02:
		        	condition=new Condition(FrontInfoSurveyPlant.class);
		        	condition.createCriteria().andEqualTo("scspId", scspId);
		        	surveyPlantMapper.deleteByExample(condition);        	
		        	break;
		        case Constants.MAIN03://养殖
		        case Constants.MAIN04:
		        case Constants.MAIN11:
		        	condition=new Condition(FrontInfoSurveyBreed.class);
		        	condition.createCriteria().andEqualTo("scspId", scspId);
		        	surveyBreedMapper.deleteByExample(condition);
				default://经商
					condition=new Condition(FrontInfoSurveyBusiness.class);
					condition.createCriteria().andEqualTo("scspId", scspId);
					surveyBusinessMapper.deleteByExample(condition);
					break;
			}					
		}		
	}

}
