package org.yjht.service.scsp.impl;

import java.util.List;

import org.springframework.stereotype.Service;
import org.yjht.bean.scsp.FrontInfoSurveyMortgage;
import org.yjht.service.base.AbstractService;
import org.yjht.service.scsp.SurveyMortgageService;

import tk.mybatis.mapper.entity.Condition;
@Service
public class SurveyMortgageServiceImpl extends AbstractService<FrontInfoSurveyMortgage> implements SurveyMortgageService{

	@Override
	public List<FrontInfoSurveyMortgage> findByScspId(String scspId) {
		Condition condition = new Condition(FrontInfoSurveyMortgage.class);
		condition.createCriteria().andEqualTo("scspId", scspId);	
		return findByCondition(condition);
	}

}
