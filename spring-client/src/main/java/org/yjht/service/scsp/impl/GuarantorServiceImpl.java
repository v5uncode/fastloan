package org.yjht.service.scsp.impl;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.yjht.bean.FlInfoApprovalVo;
import org.yjht.bean.FrontInfoScsp;
import org.yjht.bean.cust.CustBase;
import org.yjht.bean.cust.FrontInfoCustRela;
import org.yjht.bean.scsp.FrontInfoGuarantor;
import org.yjht.calculate.service.FlInfoApprovalService;
import org.yjht.core.Constants;
import org.yjht.core.Result;
import org.yjht.core.SequenceConstants;
import org.yjht.enmus.IdType;
import org.yjht.exception.MyException;
import org.yjht.mapper.FrontInfoScspMapper;
import org.yjht.mapper.scsp.FrontInfoGuarantorMapper;
import org.yjht.service.base.AbstractService;
import org.yjht.service.cust.CustBaseService;
import org.yjht.service.cust.CustRelaService;
import org.yjht.service.scsp.GuarantorService;
import org.yjht.util.DateTools;
import org.yjht.util.DicReplace;
import org.yjht.util.ResultGenerator;
import org.yjht.util.StringUtil;

import com.yjht.seq.sequence.Sequence;

import lombok.extern.slf4j.Slf4j;
import tk.mybatis.mapper.entity.Condition;
import tk.mybatis.mapper.entity.Example.Criteria;
@Service
@Slf4j
public class GuarantorServiceImpl extends AbstractService<FrontInfoGuarantor> implements GuarantorService {
	@Autowired
	private CustBaseService custBaseService;
	@Autowired
	private FrontInfoScspMapper scspMapper;
	@Autowired
	private CustRelaService custRelaService;
	@Autowired
	private FrontInfoGuarantorMapper frontInfoGuarantorMapper;
	@Autowired
	private FlInfoApprovalService flInfoApprovalService;
	@Autowired
    private Sequence sequence;
	@Override
	public List<FrontInfoGuarantor> findList(String scspId) {
		Condition condition = new Condition(FrontInfoGuarantor.class);
		condition.createCriteria().andEqualTo("scspId", scspId);
		List<FrontInfoGuarantor> list=findByCondition(condition);
		return list;
	}

	@Override
	public Result deleteGuarantor(String id) {
		int n=deleteById(id);
		if(n>0) {
			return ResultGenerator.genSuccessResult();
		}else {
			return ResultGenerator.genFailResult("删除失败");
		}
		
	}
	@Override
	public void deleteByScspId(String scspId) {
		Condition condition = new Condition(FrontInfoGuarantor.class);
		condition.createCriteria().andEqualTo("scspId", scspId);
		frontInfoGuarantorMapper.deleteByExample(condition);
	}
	public FrontInfoGuarantor getGuarantor(FrontInfoGuarantor guarantor) {
		log.info("验证保证人信息完整性.");
		//通过身份证号查询保证人的基本信息
		CustBase  custGuarantor=custBaseService.findByIdNo(guarantor.getIdType(), guarantor.getIdCard());
		if(custGuarantor==null) {
			throw new MyException("无保证人信息，请先添加保证人信息。");
		}
		if(!custGuarantor.getCreateType().equals(Constants.CREATE_TYPE_YES)) {
			throw new MyException("保证人信息不完整，请完善保证人信息。");
		}
		//根据保证人审查审批id查询审查审批数据
		FrontInfoScsp scsp=scspMapper.selectByPrimaryKey(guarantor.getScspId());
		if (scsp == null || scsp.getApplyMoney() == null || scsp.getApplyLimit() == null) {
            throw new MyException("授信数据出错，请重新发起授信。");
        }	
    	//查询被保证人的基本信息
		CustBase cust=custBaseService.findByIdNo(IdType.IDCARD.getCode(),guarantor.getInsuredIdCard());
		if (cust==null) {
            throw new MyException("无被保证人信息或被保证人信息不完整，请完善被保证人信息.");
        }
		//判断保证人是否是被保证人家庭成员
		FrontInfoCustRela rela=new FrontInfoCustRela();
		rela.setCustId(cust.getCustId());//客户id
		rela.setIdType(IdType.IDCARD.getCode());//证件类型
		rela.setIdNo(guarantor.getIdCard());//证件号码
		rela.setIsdel(Constants.ISDEL_NO);//没有删除
		rela=custRelaService.findOne(rela);
		if(rela!=null){
			throw new MyException("保证人为客户家庭成员,不能添加");
		}
		//对保证人进行评级测算
		BigDecimal applyAmount = BigDecimal.valueOf(Double.parseDouble(scsp.getApplyMoney()));//申请金额
    	Integer applyTerm=Integer.parseInt(scsp.getApplyLimit());//申请期限
    	FlInfoApprovalVo vo=null;
		try {
			// 保证人评级
			vo = flInfoApprovalService.calculate(custGuarantor.getCustId(),applyAmount,applyTerm);			
		} catch (Exception e) {
			throw new MyException(e.getMessage());
		}
		
        // 对保证人进行准入判断.
        if(!vo.getIsPass()){
        	log.info("保证人准入拒绝，拒绝原因：{}",vo.getNotPassReason());
        	throw new MyException("保证人准入拒绝,拒绝原因："+vo.getNotPassReason());
        }
        //通过传过来的身份证号查询保证人的 基本信息
        Map<String,Object> map=frontInfoGuarantorMapper.getCustInfo(guarantor.getIdCard());
        if(map.get("MAIN_PROJECT")==null||map.get("CUST_NAME")==null) {
        	log.info("保证人的 名称 或 家庭主营项目 为空, 返回错误信息, 结束程序执行.");
            throw new MyException("评级测算时出错，请重新对该用户评级测算");
        }
		guarantor.setPjJb(vo.getPjJb());//评级级别
		long bizNo = sequence.bizValue(guarantor.getScspId());
		guarantor.setId(StringUtil.getTimestampID());//ID
        guarantor.setGuarantorNo(guarantor.getScspId()+SequenceConstants.BZ+String.format("%04d", bizNo));//保证合同号
        guarantor.setJyxm(map.get("MAIN_PROJECT").toString());
        guarantor.setName(map.get("CUST_NAME").toString());
        guarantor.setZzc(map.get("ZZC")==null?"0":map.get("ZZC").toString());
        guarantor.setZfz(map.get("ZFZ")==null?"0":map.get("ZFZ").toString());
		guarantor.setAnnualExpend3y(map.get("ANNUAL_EXPEND_3Y")==null?"0":map.get("ANNUAL_EXPEND_3Y").toString());
		guarantor.setAnnualIncome3y(map.get("ANNUAL_INCOME_3Y")==null?"0":map.get("ANNUAL_INCOME_3Y").toString());
        String custId = map.get("CUST_ID").toString();
        String bizHy=null;

		//判断保证人的家庭主营项目类型.
        switch (map.get("MAIN_PROJECT").toString()) {
			//种植蔬菜
			case Constants.MAIN01:
				guarantor.setWorkName("种植蔬菜");
				//true 种植蔬菜
				Map<String,Object> scMap=frontInfoGuarantorMapper.getPlantWorkYears(custId, true);
				guarantor.setWorkYears(DateTools.getCurrentYear()-Integer.parseInt(scMap.get("BIZ_DATE").toString())+"");
				break;
			//种植其他
			case Constants.MAIN02:
				guarantor.setWorkName("种植蔬菜");
				//false  种植其他
				Map<String,Object> qtMap=frontInfoGuarantorMapper.getPlantWorkYears(custId, false);
				guarantor.setWorkYears(DateTools.getCurrentYear()-Integer.parseInt(qtMap.get("BIZ_DATE").toString())+"");
				break;
			//养殖猪
			case Constants.MAIN03:
				guarantor.setWorkName("养殖猪");
				Map<String,Object> zMap=frontInfoGuarantorMapper.getBreedWorkYears(custId,Constants.OTHER_BREED,Constants.YZZ,true);
				guarantor.setWorkYears(DateTools.getCurrentYear()-Integer.parseInt(zMap.get("BIZ_DATE").toString())+"");
				break;
			case Constants.MAIN04:
				guarantor.setWorkName("养殖其他");
				Map<String,Object> qMap=frontInfoGuarantorMapper.getBreedWorkYears(custId,Constants.MARINE_BREED,Constants.YZZ,false);
				guarantor.setWorkYears(DateTools.getCurrentYear()-Integer.parseInt(qMap.get("BIZ_DATE").toString())+"");
				break;
			case Constants.MAIN11:
				guarantor.setWorkName("海产品养殖");
				Map<String,Object> hMap=frontInfoGuarantorMapper.getBreedWorkYears(custId,Constants.MARINE_BREED,null,true);
				guarantor.setWorkYears(DateTools.getCurrentYear()-Integer.parseInt(hMap.get("BIZ_DATE").toString())+"");
				break;
            case Constants.MAIN05:
                if (map.get("WORK_NAME") == null) {
                    throw new MyException("该用户工作单位有误，请检查。");
                }
                guarantor.setWorkName(map.get("WORK_NAME").toString());
                guarantor.setWorkYears(map.get("WORK_DATE")==null?"0":String.valueOf(DateTools.getCurrentYear()-Integer.parseInt(map.get("WORK_DATE").toString().substring(0, 4))));
                break;
            case Constants.MAIN06:
            	guarantor.setWorkName("制造业");
            	bizHy=Constants.BIZHY01;
                break;
            case Constants.MAIN07:
            	guarantor.setWorkName("运输业");
            	bizHy=Constants.BIZHY02;
                break;
            case Constants.MAIN08:
            	guarantor.setWorkName("批发零售业");
            	bizHy=Constants.BIZHY03;
                break;
            case Constants.MAIN09:
            	guarantor.setWorkName("餐饮及住宿业");
            	bizHy=Constants.BIZHY04;
                break;
			case Constants.MAIN10:
				guarantor.setWorkName("其他行业");
				bizHy=Constants.BIZHY05;
				break;
            default:
            	guarantor.setWorkName("无");
            	guarantor.setWorkYears("0");
                break;
        }
        //查询行业经营年限
        if(bizHy!=null) {
        	Map<String,Object> yearsMap=frontInfoGuarantorMapper.getWorkYears(custId, bizHy);
        	guarantor.setWorkYears(DateTools.getCurrentYear()-Integer.parseInt(yearsMap.get("BIZ_DATE").toString())+"");
        }
		return guarantor;
	}
	@Override
	@Transactional
	public Result addGuarantor(FrontInfoGuarantor guarantor) {
		if(findGuarantor(guarantor)) {
			throw new MyException("该担保人已存在");
		}
		//验证并获取担保人信息
		getGuarantor(guarantor);
		int n=save(guarantor);
		if(n>0) {
			Map<String,String> dicMap=new HashMap<>();
			dicMap.put("jyxm", "JTZYXM");
			DicReplace.replaceDic(guarantor, dicMap);
			return ResultGenerator.genSuccessResult(guarantor);
		}else {
			return ResultGenerator.genFailResult("添加失败");
		}
		
	}
	/**
	 * 验证担保人是否已存在
	 * @param guarantor
	 * @return true  存在  false 不存在
	 */
	public boolean findGuarantor(FrontInfoGuarantor guarantor) {
		Condition condition = new Condition(FrontInfoGuarantor.class);
		Criteria criteria=condition.createCriteria();
		criteria.andEqualTo("scspId", guarantor.getScspId());
		criteria.andEqualTo("idCard", guarantor.getIdCard());
		List<FrontInfoGuarantor> list=findByCondition(condition);
		if(list.isEmpty()) {
			return false;
		}
		return true;
	}

	@Override
	public Result updateGuarantor(FrontInfoGuarantor guarantor) {
		if(guarantor.getId()==null){
			throw new MyException("参数错误");
		}
		update(guarantor);
		return ResultGenerator.genSuccessResult(guarantor);
	}
	

}
