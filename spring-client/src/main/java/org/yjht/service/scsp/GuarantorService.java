package org.yjht.service.scsp;

import java.util.List;

import org.yjht.bean.scsp.FrontInfoGuarantor;
import org.yjht.core.Result;

public interface GuarantorService {	
	/**
	 * 根据审查审批id查询保证人信息	
	 * @param scspId
	 * @return
	 */
	List<FrontInfoGuarantor> findList(String scspId);
	/**
	 * 删除担保人信息
	 * @param id
	 * @return
	 */
	Result deleteGuarantor(String id);
	
	/**
	 * 根据审查审批Id删除担保人信息
	 * @param id
	 * @return
	 */
	void deleteByScspId(String scspId);
	/**
	 * 添加保证人
	 * @param guarantor
	 */
	Result addGuarantor(FrontInfoGuarantor guarantor);
	/**
	 * 维护保证人信息
	 * @param guarantor
	 * @return
	 */	
	Result updateGuarantor(FrontInfoGuarantor guarantor);
	

}
