package org.yjht.service.scsp.impl;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.yjht.bean.FlInfoApprovalVo;
import org.yjht.bean.FrontInfoScsp;
import org.yjht.bean.cust.*;
import org.yjht.bean.scsp.*;
import org.yjht.bean.scsp.vo.SurveyReportVo;
import org.yjht.core.Constants;
import org.yjht.core.SequenceConstants;
import org.yjht.mapper.cust.*;
import org.yjht.mapper.scsp.*;
import org.yjht.service.base.AbstractService;
import org.yjht.service.scsp.CreditService;
import org.yjht.service.scsp.ScspService;
import org.yjht.service.scsp.SurveyService;
import org.yjht.util.DateTools;
import org.yjht.util.DicReplace;
import org.yjht.util.ParamsReflectUtils;
import org.yjht.util.StringUtil;
import com.yjht.seq.sequence.Sequence;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
/**
 * @author wf_sxf@163.com
 */
@Service
public class SurveyServiceImpl extends AbstractService<FrontInfoSurvey> implements SurveyService {
    @Autowired
    private CustBaseMapper baseMapper;
    @Autowired
    private CustFamilyMapper familyMapper;
    @Autowired
    private CustBusinessMapper businessMapper;
    @Autowired
    private FrontInfoCustPlantMapper plantMapper;
    @Autowired
    private FrontInfoCustStaffMapper staffMapper;
    @Autowired
    private FrontInfoCustBreedMapper custBreedMapper;
    @Autowired
    private FrontInfoCustBusinessMapper custBusinessMapper;
    @Autowired
    private FrontInfoCustRelaMapper infoCustRelaMapper;
    @Autowired
    private FrontInfoFinRptMapper finRptMapper;
    @Autowired
    private CustOtherInfoMapper otherMapper;
    @Autowired
    private CustPledgeMapper pledgeMapper;
    @Autowired
    private CustMortgageMapper mortgageMapper;//采集抵押

    @Autowired
    private FrontInfoSurveyBusinessMapper surveyBusinessMapper;//报告经商列表
    @Autowired
    private FrontInfoSurveyBreedMapper surveyBreedMapper;//报告养殖
    @Autowired
    private FrontInfoSurveyPlantMapper surveyPlantMapper;//报告种植
    @Autowired
    private FrontInfoSurveyRelaMapper relaMapper;//报告关系人
    @Autowired
    private FrontInfoSurveyPledgeMapper surveyPledgeMapper;//报告质押
    @Autowired
    private FrontInfoSurveyMortgageMapper surveyMortgageMapper;//报告抵押
    @Autowired
    private ScspService scspService;
    @Autowired
    private CreditService creditService;
    @Autowired
    private Sequence sequence;

    @Override
    public SurveyReportVo getReport(String scspId) {
    	//字典名称转化
        Map<String, String> dicMap = new HashMap<String, String>();
        //查询调查报告
        SurveyReportVo report = findSurveyReport(scspId);
        //家庭成员
        FrontInfoSurveyRela rela = new FrontInfoSurveyRela();
        rela.setScspId(scspId);
        List<FrontInfoSurveyRela> relas = relaMapper.select(rela);
        //家庭成员字典替换
        dicMap.put("relaType", "RELA_TYPE");
        dicMap.put("idType", "ID_TYPE");
        DicReplace.replaceDicList(relas, dicMap);
        report.setFamilies(relas);
        String zyxm = report.getZyxm();
        if (zyxm.equals(Constants.MAIN05)) {
        } else if (zyxm.equals(Constants.MAIN01) || zyxm.equals(Constants.MAIN02)) {
            //种植情况
            FrontInfoSurveyPlant plant = new FrontInfoSurveyPlant();
            plant.setScspId(scspId);
            List<FrontInfoSurveyPlant> plants = surveyPlantMapper.select(plant);
            dicMap.clear();
            dicMap.put("zzModel", "ZZ_MODEL");
            for (FrontInfoSurveyPlant p : plants) {
                dicMap.put("zwType", p.getCropType());
                DicReplace.replaceDic(p, dicMap);
            }
            report.setPlants(plants);
        } else if (zyxm.equals(Constants.MAIN03) || zyxm.equals(Constants.MAIN04)||zyxm.equals(Constants.MAIN11)) {
            //养殖情况
            FrontInfoSurveyBreed breed = new FrontInfoSurveyBreed();
            breed.setScspId(scspId);
            List<FrontInfoSurveyBreed> breeds = surveyBreedMapper.select(breed);
            //养殖种类字典替换
            for(FrontInfoSurveyBreed surveybreed:breeds){
            	dicMap.clear();
            	if(surveybreed.getYzType().equals(Constants.OTHER_BREED)){
            		 dicMap.put("yzKind", "other_breed");
            	}else{
            		dicMap.put("yzKind", "marine_breed");
            	}  
            	DicReplace.replaceDic(surveybreed, dicMap);
            }           
            report.setBreeds(breeds);
        } else {
            //经营信息
            FrontInfoSurveyBusiness busi = new FrontInfoSurveyBusiness();
            busi.setScspId(scspId);
            List<FrontInfoSurveyBusiness> business = surveyBusinessMapper.select(busi);
            //查询经营信息
            dicMap.clear();
            dicMap.put("bizHy", "BIZ_HY");
            DicReplace.replaceDicList(business, dicMap);
            report.setBusinesses(business);
        }
        report.setZyxmShow(report.getZyxm());
        dicMap.clear();
        dicMap.put("nation", "NATION");
        dicMap.put("marriageFlag", "JH_FLAG");
        dicMap.put("vocation", "VOCATION");
        dicMap.put("duty", "ZGZW");
        dicMap.put("childFlag", "YesNo");
        dicMap.put("eduLevel", "EDU_LEVEL");
        dicMap.put("custFamiliesStatus", "JK_JT_STATUS");
        dicMap.put("businessHome", "YesNo");
        dicMap.put("idType", "ID_TYPE");
        dicMap.put("zyxm", "JTZYXM");
        dicMap.put("vocation", "VOCATION");
        DicReplace.replaceDic(report, dicMap);//替换结果对象中的字典值
        dicMap.clear();
        dicMap.put("warrantType", "WARRANT");//替换结果对象中的字典值,(一个字段包含多个同级字典值)
        DicReplace.replaceDics(report,dicMap);
        return report;
    }

    public SurveyReportVo findSurveyReport(String scspId) {
    	SurveyReportVo vo=null;
    	FrontInfoSurvey survey=findById(scspId);
    	if(survey!=null){
    		vo=new SurveyReportVo();
    		BeanUtils.copyProperties(survey,vo);
    	}
        return vo;
    }

    /**
     * 调查报告审查审批表初始化
     *
     * @param vo
     * @return
     */
    @Override
    @Transactional
    public FrontInfoSurvey insertSurvey(FlInfoApprovalVo vo) {
        //审查审批ID
        String scspId = sequence.nextNo("hb", SequenceConstants.HTH_SEQUENCE_PATTERN);
        String custId = vo.getCustId();
        //调查报告
        FrontInfoSurvey survey = new FrontInfoSurvey();
        survey.setState(Constants.NO);
        survey.setReportDate(DateTools.getCurrentSysData(DateTools.DEFAULT_FORMAT));
        //基本信息
        CustBase base = new CustBase();
        base.setCustId(custId);
        base.setIsdel(Constants.ISDEL_NO);
        base = baseMapper.selectOne(base);
        BeanUtils.copyProperties(base, survey);
        survey.setZyxm(base.getMainProject());

        //家庭信息
        CustFamily family = familyMapper.selectByPrimaryKey(custId);
        if (family != null) {
            BeanUtils.copyProperties(family, survey);
        }

        //家庭成员信息
        FrontInfoCustRela rela = new FrontInfoCustRela();
        rela.setCustId(custId);
        rela.setIsdel(Constants.ISDEL_NO);
        List<FrontInfoCustRela> relas = infoCustRelaMapper.select(rela);
        relas.forEach(r -> {
            FrontInfoSurveyRela surveyRela = new FrontInfoSurveyRela();
            BeanUtils.copyProperties(r, surveyRela);
            surveyRela.setScspId(scspId);
            surveyRela.setSeqNo(StringUtil.getTimestampID());
            relaMapper.insert(surveyRela);
        });


        //财务信息
        FrontInfoFinRpt finRpt = finRptMapper.selectByPrimaryKey(custId);
        if (finRpt != null) {
            BeanUtils.copyProperties(finRpt, survey);
            Double yszk = finRpt.getYszk() == null ? 0 : finRpt.getYszk().doubleValue();
            Double yfcx = finRpt.getYfcx() == null ? 0 : finRpt.getYfcx().doubleValue();
            survey.setYsyf((double) Math.round(yszk + yfcx));
            Double yskx = finRpt.getYskx() == null ? 0 : finRpt.getYskx().doubleValue();
            Double zfzk = finRpt.getZfzk() == null ? 0 : finRpt.getZfzk().doubleValue();
            survey.setYfys((double) Math.round(yskx + zfzk));
            survey.setNjsr(Math.round(finRpt.getSr1y().add(finRpt.getSr2y()).add(finRpt.getSr3y()).doubleValue() / 3) + "");
            survey.setNjzc(Math.round(finRpt.getZc1y().add(finRpt.getZc2y()).add(finRpt.getZc3y()).doubleValue() / 3) + "");
        }
        //其他信息
        CustOtherInfo other = otherMapper.selectByPrimaryKey(custId);
        if (other != null) {
            survey.setBadOtherInfo(other.getInfo());
            survey.setBadInfo(other.getBadBehavior());
        }
        //抵押信息
        CustMortgage mortgage = new CustMortgage();
        mortgage.setCustId(custId);
        List<CustMortgage> mortgages = mortgageMapper.select(mortgage);
        mortgages.forEach(m -> {
            FrontInfoSurveyMortgage mo = new FrontInfoSurveyMortgage();
            BeanUtils.copyProperties(m, mo);
            long bizNo = sequence.bizValue(scspId);
            mo.setMortgageNo(scspId +SequenceConstants.DY+ String.format("%04d", bizNo));
            mo.setScspId(scspId);
            mo.setMortgageId(StringUtil.getTimestampID());
            surveyMortgageMapper.insert(mo);
        });

        //质押信息
        CustPledge custPledge = new CustPledge();
        custPledge.setCustId(custId);
        List<CustPledge> pledges = pledgeMapper.select(custPledge);
        pledges.forEach(p -> {
            FrontInfoSurveyPledge pl = new FrontInfoSurveyPledge();
            BeanUtils.copyProperties(p, pl);
            pl.setScspId(scspId);
            pl.setZyrxm(p.getCzrxm());
            long bizNo = sequence.bizValue(scspId);
            pl.setPledgeNo(scspId +SequenceConstants.ZY+ String.format("%04d", bizNo));
            pl.setPledgeId(StringUtil.getTimestampID());
            surveyPledgeMapper.insert(pl);
        });

        //授信信息
        survey.setApplyMoney(vo.getApplyAmount());
        survey.setApplyLimit(vo.getApplyTerm());
        survey.setGrade(vo.getPjJb());
        survey.setHighCredit(vo.getEdLmt());
        Double baseRate = Constants.BASE_RATE;
        survey.setCreditLimit(vo.getCreditAmount());
        //survey.setCreditFloatRate(vo.getCreditRate());
        //信用一年期贷款日利率=(浮动比例/100+1)*基础利率/360*10000
        if (vo.getCreditRate() != null) {
            survey.setCreditRate(vo.getCreditRate().doubleValue());
        }
        survey.setEnsureLimit(vo.getGuaranteeAmount());
        //survey.setEnsureFloatRate(vo.getGuaranteeRate());
        if (vo.getGuaranteeRate() != null) {
        	survey.setEnsureRate(vo.getGuaranteeRate().doubleValue());
            //survey.setEnsureRate((double) SplitAndRound((vo.getGuaranteeRate().doubleValue() / 100 + 1) * baseRate / 360 * 10000, 4));
        }
        //survey.setPledgeFloatRate(vo.getPledgeRate());
        survey.setPledgeLimit(vo.getPledgeAmount());
        if (vo.getPledgeRate() != null) {
            survey.setPledgeRate(vo.getPledgeRate().doubleValue());
        }

        String zyxm = base.getMainProject();
        switch (zyxm) {
            case Constants.MAIN01:
            case Constants.MAIN02:
                //种植详细信息
                FrontInfoCustPlant plant = new FrontInfoCustPlant();
                plant.setCustId(custId);
                List<FrontInfoCustPlant> plants = plantMapper.select(plant);
                plants.forEach(p -> {
                    FrontInfoSurveyPlant pl = new FrontInfoSurveyPlant();
                    BeanUtils.copyProperties(p, pl);
                    pl.setScspId(scspId);
                    pl.setId(StringUtil.getTimestampID());
                    surveyPlantMapper.insert(pl);
                });
                break;
            case Constants.MAIN03:
            case Constants.MAIN04:
            case Constants.MAIN11:	
                //养殖詳細信息
                FrontInfoCustBreed breed = new FrontInfoCustBreed();
                breed.setCustId(custId);
                List<FrontInfoCustBreed> breeds = custBreedMapper.select(breed);
                breeds.forEach(b -> {
                    FrontInfoSurveyBreed sb = new FrontInfoSurveyBreed();
                    BeanUtils.copyProperties(b, sb);
                    sb.setScspId(scspId);
                    sb.setId(StringUtil.getTimestampID());
                    surveyBreedMapper.insert(sb);
                });
                break;
            case Constants.MAIN05:
                //职工信息
                FrontInfoCustStaff staff = new FrontInfoCustStaff();
                staff.setCustId(custId);
                staff = staffMapper.selectOne(staff);
                if (staff != null) {
                    BeanUtils.copyProperties(staff, survey);
                    Integer year = DateTools.yearsBetween(staff.getWorkDate(), DateTools.getCurrentSysData(DateTools.DEFAULT_FORMAT));
                    survey.setWorkYears(year);
                }
                break;
            case Constants.MAIN06:
            case Constants.MAIN07:
            case Constants.MAIN08:
            case Constants.MAIN09:
            case Constants.MAIN10:
                //经商信息
                CustBusiness business = new CustBusiness();
                business.setCustId(custId);
                business = businessMapper.selectOne(business);
                if (business != null) {
                    BeanUtils.copyProperties(business, survey);
                }
                //经营详细信息
                FrontInfoCustBusiness custBusiness1 = new FrontInfoCustBusiness();
                custBusiness1.setCustId(custId);
                List<FrontInfoCustBusiness> businesses = custBusinessMapper.select(custBusiness1);
                businesses.forEach(custBusiness -> {
                    FrontInfoSurveyBusiness surveyBusiness = new FrontInfoSurveyBusiness();
                    BeanUtils.copyProperties(custBusiness, surveyBusiness);
                    surveyBusiness.setScspId(scspId);
                    surveyBusiness.setId(StringUtil.getTimestampID());
                    surveyBusinessMapper.insert(surveyBusiness);
                });
            default:
                break;
        }

        survey.setScspId(scspId);
        survey.setPjLmt(vo.getPjLmt());
        save(survey);
        //TODO 插入审查审批记录
        FrontInfoScsp scsp = new FrontInfoScsp();
        BeanUtils.copyProperties(base, scsp);
        scsp.setScspId(scspId);
        scsp.setMtnDate("");
        scsp.setProcessState(Constants.STATWTJ);
        scsp.setLoseDate(vo.getLoseDate());
        scsp.setPjJb(vo.getPjJb());
        scsp.setApplyLimit(vo.getApplyTerm() == null ? "" : vo.getApplyTerm().toString());
        scsp.setApplyMoney(vo.getApplyAmount() == null ? "0" : vo.getApplyAmount().toString());
        scspService.insertSelective(scsp);
        //插入授信方案
        FrontInfoCredit credit = new FrontInfoCredit();
        BeanUtils.copyProperties(scsp, credit);
        credit.setCreatedTime(DateTools.getCurrentSysData(DateTools.DEFAULT_FORMAT));
        creditService.insertCredit(credit);
        return survey;
    }

    /**
     * 保留n位小数并四舍五入取整
     *
     * @param a
     * @param n
     * @return
     */
    public static double SplitAndRound(double a, int n) {
        a = a * Math.pow(10, n);
        return (Math.round(a)) / (Math.pow(10, n));
    }

    @Override
    @Transactional
    public void updateSurvey(FrontInfoSurvey survey) {
        FrontInfoScsp scsp = new FrontInfoScsp();
        scsp.setScspId(survey.getScspId());
        scsp.setSurveyState(Constants.YES);
        scspService.updateByPrimaryKeySelective(scsp);
        update(survey);
    }
}