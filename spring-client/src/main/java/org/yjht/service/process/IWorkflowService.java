package org.yjht.service.process;

import java.io.File;
import java.io.InputStream;
import java.util.List;

import org.yjht.core.Result;

public interface IWorkflowService {
	/**
	 * 部署流程定义
	 * @param file 流程文件
	 * @param filename 流程名称
	 */
	Result saveNewDeploye(File file, String filename);
	/**
	 * 查询部署对象信息，对应表（act_re_deployment）
	 * @return
	 */
	/*List<Deployment> findDeploymentList();*/
	/**
	 * 查询流程定义的信息，对应表（act_re_procdef）
	 * @return
	 */
	/*List<ProcessDefinition> findProcessDefinitionList();*/
	/**
	 * 使用部署对象ID和资源图片名称，获取图片的输入流
	 * @param deploymentId
	 * @param imageName
	 * @return
	 */
	InputStream findImageInputStream(String deploymentId, String imageName);

	/**
	 * 使用部署对象ID，删除流程定义
	 * @param deploymentId
	 */
	Result deleteProcessDefinitionByDeploymentId(String deploymentId);
}
