package org.yjht.service.base;

import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.yjht.bean.cust.CompleteCustInfo;
import org.yjht.bean.cust.CompleteDetails;
import org.yjht.bean.cust.CompleteType;
import org.yjht.mapper.cust.CompleteCustInfoMapper;
import org.yjht.mapper.cust.CompleteDetailsMapper;
import org.yjht.mapper.cust.CompleteTypeMapper;
import org.yjht.util.ParamsReflectUtils;

import tk.mybatis.mapper.entity.Condition;
/**
 * 客户信息完整性服务类
 * @author lenovo
 *
 * @param <T>
 */
@Service
public class CompleteService<T>{
	@Autowired
	CompleteCustInfoMapper CompleteCustInfoMapper;
	@Autowired
	CompleteTypeMapper completeTypeMapper;
	@Autowired
	CompleteDetailsMapper completeDetailsMapper;
	public void saveCompleteCustInfo(String custId) throws RuntimeException{
		try {
			List<CompleteType> list=completeTypeMapper.selectAll();
			//客户信息完整性初始化
			for(CompleteType completeType:list) {
				CompleteCustInfo completeCustInfo=new CompleteCustInfo();
				completeCustInfo.setCustId(custId);
				completeCustInfo.setType(completeType.getType());
				completeCustInfo.setCurrentNum(0);
				completeCustInfo.setIscomplete("0");//1:完整 0:不完整
				CompleteCustInfoMapper.insert(completeCustInfo);
			}
		}catch(Exception e) {
			throw new RuntimeException("初始化完整信息出错");
		}
	}
	/**
	 * 客户表单信息完整性修改
	 * @param model
	 */
	public void updateCompleteCustInfo(T model){
		int n=0;
		CompleteCustInfo completeCustInfo=new CompleteCustInfo();
		//获取客户id
		String custId=ParamsReflectUtils.getFieldValue(model,"custId");
		//类名作为类型名称
		String className = model.getClass().getName();
		String type=className.substring(className.lastIndexOf(".")+1);
		completeCustInfo.setCustId(custId);	
		completeCustInfo.setType(type);
		//查询完整性条件必填属性
		Condition condition = new Condition(CompleteDetails.class);
		condition.createCriteria().andEqualTo("type", type).andEqualTo("isValid", "1"); 
		List<CompleteDetails> list=completeDetailsMapper.selectByExample(condition);
		//遍历所有必填属性,获取值,判断是否填写,填写进度+1
		for(CompleteDetails details:list) {
			if(StringUtils.isNotBlank(ParamsReflectUtils.getFieldValue(model, details.getProperty()))) {
				n++;
			}
		}
		//设置当前进度
		completeCustInfo.setCurrentNum(n);
		//当前进度等于总条数  改变完整状态为完整
		if(n==completeTypeMapper.getTotal(type)) {
			completeCustInfo.setIscomplete("1");
		}else {
			completeCustInfo.setIscomplete("0");
		}
		CompleteCustInfoMapper.updateByPrimaryKeySelective(completeCustInfo);		
	}
	/**
	 * 客户列表信息完整性修改
	 * @param custId
	 */
	public void updateCompleteCustInfo(String custId,String type,Object obj) {
		//类名作为类型名称
		CompleteCustInfo completeCustInfo=new CompleteCustInfo();
		completeCustInfo.setCustId(custId);	
		completeCustInfo.setType(type);		
		if(obj==null) {
			completeCustInfo.setCurrentNum(0);
			completeCustInfo.setIscomplete("0");
		}else {
			completeCustInfo.setCurrentNum(completeTypeMapper.getTotal(type));
			completeCustInfo.setIscomplete("1");
		}
		CompleteCustInfoMapper.updateByPrimaryKeySelective(completeCustInfo);
	}

	/**
	 * 删除客户完整性信息
	 */
	public void deleteCompleteCustInfo(String custId) {
		Condition condition = new Condition(CompleteCustInfo.class);
		condition.createCriteria().andEqualTo("custId", custId);
		CompleteCustInfoMapper.deleteByExample(condition);
	}
	
	
}
