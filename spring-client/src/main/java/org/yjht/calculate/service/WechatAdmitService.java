package org.yjht.calculate.service;

import org.yjht.bean.rest.ZrParam;
import org.yjht.calculate.model.ObjectResponseVo;

public interface WechatAdmitService {
	
	ObjectResponseVo wechatAdmin(ZrParam param);

}
