package org.yjht.calculate.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.yjht.bean.rest.ZrParam;
import org.yjht.calculate.constants.InterfaceInfo;
import org.yjht.calculate.model.Calculate;
import org.yjht.calculate.model.ObjectResponseVo;
import org.yjht.calculate.service.GetDateService;
import org.yjht.calculate.service.WechatAdmitService;
import org.yjht.calculate.util.SendHttpUtil;
import org.yjht.core.ErrorCode;
@Service
public class WechatAdmitServiceImpl implements WechatAdmitService{
	
	@Autowired
	private GetDateService getDateService;
	//策略要执行的步骤
	private final String[] callstep={InterfaceInfo.ADMIT.getNo()};
	
	@Override
	public ObjectResponseVo wechatAdmin(ZrParam param) {
		ObjectResponseVo response=new ObjectResponseVo();
		response.setIsException(false);
		//传入数据为空
		if(param == null){
			response.setIsException(true);
			response.setExceptionInfo(ErrorCode.PARAM_ISNULL.getCode());
			return response;
		}
		//证件号码为空
		if(param.getIdno()== null){
			response.setIsException(true);
			response.setExceptionInfo(ErrorCode.ID_NO_ISNULL.getCode());
			return response;
		}
		//客户姓名为空
		if(param.getCustname()== null){
			response.setIsException(true);
			response.setExceptionInfo(ErrorCode.NAME_ISNULL.getCode());
			return response;
		}
		//获取微信准入数据
		Calculate calculate =getDateService.getWechatAdminDate(param.getIdno(),param.getCustname());
		calculate.setCallstep(callstep);
		ObjectResponseVo vo = SendHttpUtil.sendPjcsHttp(calculate);		
		return vo;
	}	

}
