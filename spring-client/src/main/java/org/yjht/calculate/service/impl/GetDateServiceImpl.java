package org.yjht.calculate.service.impl;

import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpResponse;
import cn.hutool.http.HttpStatus;
import com.yjht.seq.sequence.Sequence;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.yjht.bean.cust.*;
import org.yjht.bean.vo.User;
import org.yjht.calculate.constants.ObjectCode;
import org.yjht.calculate.model.Admit;
import org.yjht.calculate.model.Calculate;
import org.yjht.calculate.model.LimitModel;
import org.yjht.calculate.model.PriceModel;
import org.yjht.calculate.model.Rate;
import org.yjht.calculate.service.GetDateService;
import org.yjht.core.Constants;
import org.yjht.core.SequenceConstants;
import org.yjht.enmus.IdType;
import org.yjht.mapper.cust.CustMortgageMapper;
import org.yjht.mapper.cust.CustPledgeMapper;
import org.yjht.service.base.AbstractService;
import org.yjht.service.cust.*;
import org.yjht.util.CommonUtil;
import org.yjht.util.DateTools;
import org.yjht.util.xml.Head;
import org.yjht.util.xml.RespHead;
import org.yjht.util.xml.Response;
import org.yjht.util.xml.XmlUtils;
import org.yjht.xindai.common.enums.EnumXyResponseCode;
import org.yjht.xindai.common.req.XingYe2019;
import org.yjht.xindai.common.resp.XingYeResp2019;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Slf4j
@Service
public class GetDateServiceImpl extends AbstractService<CustCreditReport> implements GetDateService {
    private final String SOURCE = "wd";

    /**
     * 2019客户信息贷前测评
     */
    private final String path2019 = "/loan/oCustDere";

    @Value("${xingye.loan-url}")
    private String baseUrl;
    @Autowired
    private CustCreditReportService custCreditReportService;
    @Autowired
    private CustBaseService custBaseService;
    @Autowired
    private CustFamilyService custFamilyService;
    @Autowired
    private FinRptService finRptService;
    @Autowired
    private CustRelaService custRelaService;
    @Autowired
    private CustBusinessService custBusinessService;
    @Autowired
    private FrontInfoCustBusinessService frontInfoCustBusinessService;
    @Autowired
    private CustStaffService custStaffService;
    @Autowired
    private FrontInfoCustPlantService frontInfoCustPlantService;
    @Autowired
    private CustBreedService custBreedService;
    @Autowired
    private RptDtlService rptDtlService;
    @Autowired
    private CustMortgageMapper custMortgageMapper;
    @Autowired
    private CustPledgeMapper custPledgeMapper;
    @Autowired
    private Sequence sequence;
    @Autowired
    private RedisTemplate redisTemplate;
    
    @Override
	public Calculate getWechatAdminDate(String idNo,String name) {
    	Calculate calculate = new Calculate();              
        calculate.setIdnum(idNo);//证件号码     
        calculate.setIdtype(IdType.IDCARD.getCode());//证件类型        
        calculate.setBusDate(DateTools.getCurrentSysData(DateTools.DEFAULT_FORMAT));//业务日期
    	List<Admit> resultList = new ArrayList<>();
    	CustBase cust=custBaseService.findByIdNo(IdType.IDCARD.getCode(), idNo);
    	Admit admit = new Admit();
    	if(cust==null){ 
    		calculate.setCustname(name);//客户姓名
    		admit.setObjType(ObjectCode.Admit.AdmitBorrower.toString());
    		admit.setCardType(IdType.IDCARD.getCode());//证件类型
    		admit.setCardNum(idNo);//证件号码
    		admit.setCustName(name);//姓名
            resultList.add(admit);//客户信息
    	}else{
    		calculate.setCustcode(cust.getCustId());//客户编号
    		calculate.setCustname(cust.getCustName()==null?name:cust.getCustName());//客户姓名
    		admit.setObjType(ObjectCode.Admit.AdmitBorrower.toString());
    		admit.setCardType(IdType.IDCARD.getCode());//证件类型
    		admit.setCardNum(idNo);//证件号码
    		admit.setCustName(cust.getCustName()==null?name:cust.getCustName());//姓名
    		String custId=cust.getCustId();
    		//家庭信息
            CustFamily family = custFamilyService.findFamily(custId);
            if (family != null) {
            	admit.setMarrige(family.getMarriageFlag());
            }
            //财务信息
            FrontInfoFinRpt rpt = finRptService.findFinRpt(custId);//财务信息
            if (rpt != null) {
            	admit.setFamilyAsset(rpt.getZzc() == null ? "0" : rpt.getZzc().toString());
            	admit.setFamilyDebt(rpt.getZfz() == null ? "0" : rpt.getZfz().toString());
            }
            //家庭成员基本信息
            List<FrontInfoCustRela> relaList = custRelaService.findRela(custId);
            if (relaList != null && relaList.size() > 0) {
                for (FrontInfoCustRela rela : relaList) {
                    Admit relaAdmit = new Admit();
                    relaAdmit.setObjType(ObjectCode.Admit.AdmitFamily.toString());
                    relaAdmit.setCardType(rela.getIdType());//家庭成员证件类型
                    relaAdmit.setCardNum(rela.getIdNo());//家庭成员证件号码
                    relaAdmit.setCustName(rela.getVsName());//家庭成员姓名
                    resultList.add(relaAdmit);//家庭成员信息
                }
            }
    	}
    	//行内数据
    	/*XingYeResp2019 resp2019 = oCustDere(IdType.IDCARD.getCode(), idNo);
        if(resp2019!=null){ 
        	admit.setIsOverdue(resp2019.getSfbxyq());//行内是否存在本金逾期记录
        	admit.setWorstFiveClass(resp2019.getZcwjfl());
        	admit.setIsExend(resp2019.getSeyzq());
        	admit.setInBankOverdueTimes12(resp2019.getSebxcs());
        	admit.setInBankOverdueMaxTimes24(resp2019.getEsbxqs());
        }*/
    	resultList.add(admit);//添加借款人信息
    	calculate.setAdmitList(resultList);
		return calculate;
	}
    @Override
    public List<Admit> getAdmitData(String custId, String applyAmount, String applyTerm) {
        List<Admit> resultList = new ArrayList<>();
        CustCreditReport creditReport = custCreditReportService.getCreditInfoByCustId(custId);
        creditReport.setCrOverdueCountIn12M(
                ((creditReport.getCreditOverdueLoanTimes12() == null ? 0 : Integer.parseInt(creditReport.getCreditOverdueLoanTimes12()))
                        + (creditReport.getCreditOverdueCardTimes12() == null ? 0 : Integer.parseInt(creditReport.getCreditOverdueCardTimes12()))) + ""
        );
        //creditReport.setCreditCardUseRatio(creditReport.getCreditCardUseRatio()==null?"0":String.valueOf(Double.parseDouble(creditReport.getCreditCardUseRatio())/100));
        Admit borrowerAdmit = new Admit();
        borrowerAdmit.setApplyAmount(applyAmount);//申请金额
        borrowerAdmit.setApplyTerm(applyTerm);//申请期限
        //添加征信信息
        BeanUtils.copyProperties(creditReport, borrowerAdmit);
        borrowerAdmit.setCrOverdueSerialIn24m(creditReport.getCrOverdueMaxCountIn24M() == null ? "0" : creditReport.getCrOverdueMaxCountIn24M());//征信近24个月本息最大逾期期数(银行贷款、信用卡)
        
        borrowerAdmit.setObjType(ObjectCode.Admit.AdmitBorrower.toString());//对象类型为借款人
        borrowerAdmit.setCustCode(custId);//客户编号
        //基本信息
        CustBase custBase = custBaseService.findByCustid(custId);
        if (custBase != null) {
            borrowerAdmit.setCardType(custBase.getIdType());//证件类型
            borrowerAdmit.setCardNum(custBase.getIdNo());//证件号码
            borrowerAdmit.setGender(custBase.getSex());//性别
            borrowerAdmit.setAge(String.valueOf(DateTools.getCurrentYear() - (Integer.parseInt(custBase.getBirthDate().substring(0, 4)))));//客户年龄
            borrowerAdmit.setCustName(custBase.getCustName());//客户姓名
        }
        //家庭信息
        CustFamily family = custFamilyService.findFamily(custId);
        if (family != null) {
            borrowerAdmit.setMarrige(family.getMarriageFlag());
        }
        //财务信息
        FrontInfoFinRpt rpt = finRptService.findFinRpt(custId);//财务信息
        if (rpt != null) {
            borrowerAdmit.setFamilyAsset(rpt.getZzc() == null ? "0" : rpt.getZzc().toString());
            borrowerAdmit.setFamilyDebt(rpt.getZfz() == null ? "0" : rpt.getZfz().toString());
        }
        //家庭成员基本信息
        List<FrontInfoCustRela> relaList = custRelaService.findRela(custId);
        if (relaList != null && relaList.size() > 0) {
            for (FrontInfoCustRela rela : relaList) {
                Admit relaAdmit = new Admit();
                relaAdmit.setObjType(ObjectCode.Admit.AdmitFamily.toString());
                relaAdmit.setCardType(rela.getIdType());//家庭成员证件类型
                relaAdmit.setCardNum(rela.getIdNo());//家庭成员证件号码
                relaAdmit.setCustName(rela.getVsName());//家庭成员姓名
                resultList.add(relaAdmit);//家庭成员信息
            }
        }
        resultList.add(borrowerAdmit);//添加借款人信息
        //行内数据
        XingYeResp2019 resp2019 = oCustDere(custBase.getIdType(), custBase.getIdNo());
        if(resp2019!=null){ 
	        borrowerAdmit.setIsOverdue(resp2019.getSfbxyq());//行内是否存在本金逾期记录
	        borrowerAdmit.setWorstFiveClass(resp2019.getZcwjfl());
	        borrowerAdmit.setIsExend(resp2019.getSeyzq());
	        borrowerAdmit.setInBankOverdueTimes12(resp2019.getSebxcs());
	        borrowerAdmit.setInBankOverdueMaxTimes24(resp2019.getEsbxqs());
        }
        return resultList;
    }

    /**
     * 评级模型数据
     *
     * @param custId
     * @return
     */
    @Override
    public Rate getRateData(String custId,BigDecimal applyAmount,Integer loanTimeLimit) {
        Rate rate = new Rate();
        rate.setApplyAmount(String.valueOf(applyAmount.doubleValue()));//申请金额
        rate.setApplyDate(DateTools.getCurrentSysData(DateTools.DEFAULT_FORMAT));//申请日期
        rate.setApplyMoney(String.valueOf(applyAmount.doubleValue()));//规则运算用申请额度
        rate.setUpOrDown(String.valueOf(DateTools.getCurrentMonth()));
        
        //基本信息
        CustBase custBase = custBaseService.findByCustid(custId);
        rate.setObjType(ObjectCode.Rate.RateBorrower.toString());//对象类型
        //家庭信息
        CustFamily family = (CustFamily) custFamilyService.findFamily(custId);
        if (family != null) {
            rate.setMarriage(family.getMarriageFlag());//婚姻状况
            rate.setHavaChild(family.getChildFlag());//有无子女
            rate.setLiveStatus(family.getLiveStatus());//居住状况
            rate.setWorkStatus(family.getCustFamiliesStatus());//借款人及家庭成员工作情况
            rate.setIsBuyHouse(family.getBusinessHome());//是否在城区购买商住商用房
            rate.setOwnHouseCount(family.getHouseCount() == null ? "0" : family.getHouseCount().toString());//自有房产数量
            rate.setMarriageYear(StringUtils.isEmpty(family.getMarriageDate()) ? "0" : String.valueOf(DateTools.getCurrentYear() - Integer.parseInt(family.getMarriageDate().substring(0, 4))));//婚龄
        }
        //征信信息
        CustCreditReport creditReport = custCreditReportService.getCreditInfoByCustId(custId);
        creditReport.setCrOverdueCountIn12M(
                ((creditReport.getCreditOverdueLoanTimes12() == null ? 0 : Integer.parseInt(creditReport.getCreditOverdueLoanTimes12()))
                        + (creditReport.getCreditOverdueCardTimes12() == null ? 0 : Integer.parseInt(creditReport.getCreditOverdueCardTimes12()))) + ""
        );
        //creditReport.setCreditCardUseRatio(creditReport.getCreditCardUseRatio()==null?"0":String.valueOf(Double.parseDouble(creditReport.getCreditCardUseRatio())/100));
        BeanUtils.copyProperties(creditReport, rate);
        //财务信息
        FrontInfoFinRpt rpt = (FrontInfoFinRpt) finRptService.findFinRpt(custId);//财务信息
        if (rpt != null) {
            double sr1y = rpt.getSr1y() == null ? 0 : rpt.getSr1y().doubleValue();//一年前家庭收入
            double sr2y = rpt.getSr2y() == null ? 0 : rpt.getSr2y().doubleValue();//二年前家庭收入
            double sr3y = rpt.getSr3y() == null ? 0 : rpt.getSr3y().doubleValue();//三年前家庭收入
            double zc1y = rpt.getZc1y() == null ? 0 : rpt.getZc1y().doubleValue();//一年前家庭支出
            double zc2y = rpt.getZc2y() == null ? 0 : rpt.getZc2y().doubleValue();//二年前家庭支出
            double zc3y = rpt.getZc3y() == null ? 0 : rpt.getZc3y().doubleValue();//三年前家庭支出
            double zyxmSr1y = rpt.getZyxmSr1y() == null ? 0 : rpt.getZyxmSr1y().doubleValue();//一年前主营项目收入
            double zyxmSr2y = rpt.getZyxmSr2y() == null ? 0 : rpt.getZyxmSr2y().doubleValue();//二年前主营项目收入
            double zyxmSr3y = rpt.getZyxmSr3y() == null ? 0 : rpt.getZyxmSr3y().doubleValue();//三年前主营项目收入
            double zyxmZc1y = rpt.getZyxmZc1y() == null ? 0 : rpt.getZyxmZc1y().doubleValue();//一年前主营项目支出
            double zyxmZc2y = rpt.getZyxmZc2y() == null ? 0 : rpt.getZyxmZc2y().doubleValue();//二年前主营项目支出
            double zyxmZc3y = rpt.getZyxmZc3y() == null ? 0 : rpt.getZyxmZc3y().doubleValue();//三年前主营项目支出

            rate.setFamilyAsset(rpt.getZzc() == null ? "0" : String.valueOf(rpt.getZzc().doubleValue()));//总资产
            rate.setFamilyDebt(rpt.getZfz() == null ? "0" : String.valueOf(rpt.getZfz().doubleValue()));//总负责
            //截至目前家庭支出
            rate.setFamilyExpendThisYear(String.valueOf((zc1y + zc2y + zc3y) / 3));
            //截至目前家庭收入
            rate.setFamilyIncomeThisYear(String.valueOf((sr1y + sr2y + sr3y) / 3));
            //近三年经营年均收入
            rate.setLast3yearOperateIncome(String.valueOf((zyxmSr1y + zyxmSr2y + zyxmSr3y) / 3));
            //近三年经营年均支出
            rate.setLast3yearOperateOutcome(String.valueOf((zyxmZc1y + zyxmZc2y + zyxmZc3y) / 3));
            //存货（元）
            rate.setStockCount(rpt.getCk() == null ? "0" : String.valueOf(rpt.getCk().doubleValue()));
            //家庭或有负债
            rate.setFamilyPossibleDebt(rpt.getJthyfz() == null ? "0" : String.valueOf(rpt.getJthyfz().doubleValue()));
            //去年家庭收入
            rate.setFamilyIncomeLastYear(String.valueOf(sr1y));
            //去年家庭年支出
            rate.setFamilyExpendLatsYear(String.valueOf(zc1y));
            //前年家庭收入
            rate.setFamilyIncomeBeforeLastYear(String.valueOf(sr2y));
            //前年家庭年支出
            rate.setFamilyExpendBeforeLastYear(String.valueOf(zc2y));
            //上年度经营支出
            rate.setProjectExpendLastYear(String.valueOf(zyxmZc1y));
            //上年度经营收入
            rate.setProjectIncomeLastYear(String.valueOf(zyxmSr1y));
        }
        if (custBase != null) {
            rate.setCustType(custBase.getCustType());//客户类型
            String mainProject = custBase.getMainProject();
            rate.setMainProject(mainProject);//主营项目
            rate.setAge(String.valueOf(DateTools.getCurrentYear() - (Integer.parseInt(custBase.getBirthDate().substring(0, 4)))));//年龄
            rate.setGender(custBase.getSex());//性别
            rate.setEdu(custBase.getEduLevel());//学历
            rate.setProfession(custBase.getOccSubdivision());//职业细分
            /* String industryClass = custBase.getIndustryClass();//行业分类
            if (industryClass != null && !"".equals(industryClass)) {
                String[] strArry = industryClass.split(",");
                for (int i = 0; i < strArry.length; i++) {
                    if (i == 0) {
                        rate.setIndustryone(strArry[i]);
                    } else if (i == 1) {
                        rate.setIndustrytwo(strArry[i]);
                    } else if (i == 2) {
                        rate.setIndustrythree(strArry[i]);
                    } else if (i == 3) {
                        rate.setIndustryfour(strArry[i]);
                    }
                }
            }*/

            //种植、养殖、经营年限
            String plantYears = "";
            //经营类
            FrontInfoCustBusiness business = null;
            CustBusiness custBusiness = null;
            switch (mainProject) {
                case Constants.MAIN01://种植蔬菜
                    List<Map<String, Object>> zzmsList = frontInfoCustPlantService.getZzms(custId, Constants.ZWTYPE01, Constants.YES);//种植亩数
                    double zzms = 0;
                    for (Map<String, Object> map : zzmsList) {
                        if (Constants.ZZMODEL01.equals(map.get("zzModel"))) { //露天亩数
                            rate.setOpenArea(map.get("zzMs") == null ? "0" : map.get("zzMs").toString());
                        } else { //大棚亩数
                            rate.setGreenhoustArea(map.get("zzMs") == null ? "0" : map.get("zzMs").toString());
                        }
                        zzms += Double.parseDouble(map.get("zzMs") == null ? "0" : map.get("zzMs").toString());
                    }
                    rate.setMuSum(String.valueOf(zzms));//种植亩数
                    //经营年限
                    plantYears = frontInfoCustPlantService.getPlantYears(custId, Constants.ZWTYPE01);
                    rate.setPlantYear(plantYears);
                    break;
                case Constants.MAIN02: //种植其他
                    List<Map<String, Object>> zzOthermsList = frontInfoCustPlantService.getZzms(custId, Constants.ZWTYPE01, Constants.NO);//种植亩数
                    double zzms1 = 0;
                    for (Map<String, Object> map : zzOthermsList) {
                        if (Constants.ZZMODEL01.equals(map.get("zzModel"))) { //露天亩数
                            rate.setOpenArea(map.get("zzMs") == null ? "0" : map.get("zzMs").toString());
                        } else { //大棚亩数
                            rate.setGreenhoustArea(map.get("zzMs") == null ? "0" : map.get("zzMs").toString());
                        }
                        zzms1 += Double.parseDouble(map.get("zzMs") == null ? "0" : map.get("zzMs").toString());
                    }
                    rate.setMuSum(String.valueOf(zzms1));//种植亩数
                    plantYears = frontInfoCustPlantService.getOtherPlantYears(custId, Constants.ZWTYPE03);
                    rate.setPlantYear(plantYears);//经营年限
                    break;
                case Constants.MAIN03://养猪
                    FrontInfoCustBreed breedPig = custBreedService.getBreedInfoBy(custId,null,Constants.YZZ);
                    if(breedPig != null){
                        rate.setProjectAssets(breedPig.getBizFixedasset()==null?"0":String.valueOf(breedPig.getBizFixedasset().doubleValue()));//经营项目固定资产
                    }
                    break;
                case Constants.MAIN04://养殖其他
                    String breedYears = custBreedService.getBreedOtherYears(custId,Constants.MARINE_BREED,Constants.YZZ);
                    rate.setPlantYear(breedYears);//经营年限
                    break;
                case Constants.MAIN11://海产品养殖
                	FrontInfoCustBreed breedMarine = custBreedService.getBreedInfoBy(custId,Constants.MARINE_BREED,null);
                	if(breedMarine != null){
                        rate.setPlantYear(String.valueOf(DateTools.getCurrentYear()-Integer.parseInt(breedMarine.getBizDate())));//经营年限
                        rate.setYzType(breedMarine.getYzType());//养殖类型
                        rate.setYzKind(breedMarine.getYzKind());//养殖种类
                        rate.setYzModel(breedMarine.getYzModel());//养殖模式
                        rate.setYzScale(breedMarine.getYzScale());//养殖规模
                        rate.setYzCycle(breedMarine.getYzCycle());//养殖周期(月)
                        rate.setYzLoanUse(breedMarine.getYzLoanUse());//海水养殖贷款用途                       
                    }
                    break;
                case Constants.MAIN05://职工
                    FrontInfoCustStaff staff = (FrontInfoCustStaff) custStaffService.findStaff(custId);
                    if (staff != null) {
                        rate.setWorkYear(staff.getWorkDate() == null ? "0" : String.valueOf(DateTools.getCurrentYear() - Integer.parseInt(staff.getWorkDate().substring(0, 4))));//工作年限
                        rate.setMiAmount(staff.getYlbxamt() == null ? "0" : staff.getYlbxamt().toString());//医疗保险上年度年缴纳总额
                    }
                    break;
                case Constants.MAIN06://制造业
                    custBusiness = custBusinessService.findBusiness(custId);
                    if (custBusiness != null) {
                        rate.setStaffCount(custBusiness.getEmployeeCount() == null ? "0" : custBusiness.getEmployeeCount().toString());//员工总数量
                        rate.setInsuranceStaffCount(custBusiness.getBxCnt() == null ? "0" : custBusiness.getBxCnt().toString());//有保险的员工数量
                    }
                    business = frontInfoCustBusinessService.getBusinessByHy(custId, Constants.BIZHY01);
                    if (business != null) {
                        rate.setPlantYear(String.valueOf(DateTools.getCurrentYear() - Integer.parseInt(business.getBizDate())));//经营年限
                        rate.setProjectArea(business.getOwnArea() == null ? "0" : business.getOwnArea().toString());//生产经营面积
                    }
                    break;
                case Constants.MAIN07://运输业
                    business = frontInfoCustBusinessService.getBusinessByHy(custId, Constants.BIZHY02);
                    if (business != null) {
                        rate.setPlantYear(String.valueOf(DateTools.getCurrentYear() - Integer.parseInt(business.getBizDate())));
                    }
                    //营运车数量
                    Integer yyc = rptDtlService.getDtlInfoByCustId(custId);
                    rate.setCarCount(yyc == null ? "0" : yyc.toString());
                    break;
                case Constants.MAIN08://批发零售业
                    business = frontInfoCustBusinessService.getBusinessByHy(custId, Constants.BIZHY04);
                    if (business != null) {
                        rate.setPlantYear(String.valueOf(DateTools.getCurrentYear() - Integer.parseInt(business.getBizDate())));//经营年限
                    }
                    break;
                case Constants.MAIN09://餐饮及住宿业
                    custBusiness = custBusinessService.findBusiness(custId);
                    if (custBusiness != null) {
                        rate.setStaffCount(custBusiness.getEmployeeCount() == null ? "0" : custBusiness.getEmployeeCount().toString());//员工总数量
                    }
                    business = frontInfoCustBusinessService.getBusinessByHy(custId, Constants.BIZHY03);
                    if (business != null) {
                        rate.setPlantYear(String.valueOf(DateTools.getCurrentYear() - Integer.parseInt(business.getBizDate())));//经营年限
                        double ownArea = business.getOwnArea() == null ? 0 : business.getOwnArea().doubleValue();
                        double rentArea = business.getRentArea() == null ? 0 : business.getRentArea().doubleValue();
                        rate.setProjectBuildArea(String.valueOf(ownArea + rentArea));//经营场所总面积
                    }
                    break;
                case Constants.MAIN10://其他行业
                    business = frontInfoCustBusinessService.getBusinessByHy(custId, Constants.BIZHY05);
                    if (business != null) {
                        rate.setPlantYear(String.valueOf(DateTools.getCurrentYear() - Integer.parseInt(business.getBizDate())));
                    }
                    break;
                default:
                    break;

            }
        }
        //行内数据
        XingYeResp2019 resp2019 = oCustDere(custBase.getIdType(), custBase.getIdNo());
        if(resp2019!=null){
        	//最近一次本金逾期距离现在的月数(本行)
        	rate.setMoneyOverdueMonthCount(resp2019.getZhyqys()==null?"999":resp2019.getZhyqys());
        	//行内未结清贷款账户数
        	rate.setAccountAcount(resp2019.getWjqs()==null?"0":resp2019.getWjqs());
        	//本行是否有本息逾期
        	rate.setOurIsOverdueInAndC(resp2019.getSfbxyq()==null?Constants.NO:(resp2019.getSfbxyq().equals("1")?Constants.YES:Constants.NO));
        	//行内近6个月本息逾期次数
        	rate.setOverdueCountIn6M(resp2019.getLybxcs()==null?"0":resp2019.getLybxcs());
        	//征信近12个月本息逾期次数
        	rate.setOverdueCountIn12M(resp2019.getSebxcs()==null?"0":resp2019.getSebxcs());
        	//行内最近24个月本息逾期次数
        	rate.setOverdueCountIn24M(resp2019.getEsbxcs()==null?"0":resp2019.getEsbxcs());
        	//行内最近36个月本息逾期次数
        	rate.setOverdueCountIn36M(resp2019.getSlbxcs()==null?"0":resp2019.getSlbxcs());           	
        	//行内最近6个月本息最大逾期期数
        	rate.setOverdueMaxCountIn6M(resp2019.getLybxqs()==null?"0":resp2019.getLybxqs()); 
        	//行内近12个月本息最大逾期期数
        	rate.setOverbxMaxCountIn12M(resp2019.getSebxzqs()==null?"0":resp2019.getSebxzqs());
        	//行内最近24个月本息最大逾期期数
        	rate.setOverdueMaxCountIn24M(resp2019.getEsbxqs()==null?"0":resp2019.getEsbxqs());     	
        	
        }
        
        return rate;
    }

    /**
     * 额度模型数据
     *
     * @param custId
     * @return
     */
    @Override
    public LimitModel getLimitModelData(String custId, BigDecimal applyAmount, Integer loanTimeLimit) {
        LimitModel model = new LimitModel();
        model.setObjType(ObjectCode.LimitAmount.LimitAmountBorrower.toString());
        model.setApplyAmount(String.valueOf(applyAmount.doubleValue()));
        model.setLoanTimeLimit(loanTimeLimit);
        //基本信息
        CustBase custBase = custBaseService.findByCustid(custId);
        if (custBase != null) {
            model.setCustType(custBase.getCustType());//客户类型
            model.setProfession(custBase.getOccSubdivision());//职业细分
        }
        //财务信息
        FrontInfoFinRpt rpt = (FrontInfoFinRpt) finRptService.findFinRpt(custId);//财务信息
        if (rpt != null) {
            model.setFamilyAsset(rpt.getZzc() == null ? "0" : rpt.getZzc().toString());
            model.setFamilyDebt(rpt.getZfz() == null ? "0" : rpt.getZfz().toString());
            model.setFamilyIncome(rpt.getSr1y() == null ? "0" : rpt.getSr1y().toString());
            model.setFamilyExpend(rpt.getZc1y() == null ? "0" : rpt.getZc1y().toString());
        }
        BigDecimal mortGageValue = custMortgageMapper.selectMortGageValue(custId);
        BigDecimal pledgeValue = custPledgeMapper.selectPledgeValue(custId);
        if (mortGageValue == null) {
            mortGageValue = new BigDecimal(0);
        }
        if (pledgeValue == null) {
            pledgeValue = new BigDecimal(0);
        }
        model.setPledgeValue(mortGageValue.add(pledgeValue).toString());
        return model;
    }

    @Override
    public PriceModel getPriceModelData(String custId) {
        //基本信息
        CustBase custBase = custBaseService.findByCustid(custId);
        PriceModel model = new PriceModel();
        model.setObjType(ObjectCode.Price.PriceBorrower.toString());
        if (custBase != null) {
            model.setCustType(custBase.getCustType());//客户类型
            model.setProfession(custBase.getOccSubdivision());//职业细分
        }
        return model;
    }

    @Override
    public XingYeResp2019 oCustDere(String zjlx, String zjhm) {
        String redisKey = "xingye:2019:" + zjlx + zjhm;
        Object obj = redisTemplate.opsForValue().get(redisKey);
        if (obj != null) {
            return (XingYeResp2019) obj;
        }
        XingYeResp2019 xyrsp2019 = null;
        User user = CommonUtil.getUser();
        Head xy = new Head();
        xy.setTrxtype("2019");
        xy.setDestinst(user.getCorpCD());
        xy.setSource(SOURCE);
        xy.setBrn_no(user.getOrgCD());
        xy.setOpe_no(user.getUser_id());
        xy.setSeqno(sequence.nextNo(CommonUtil.getUser().getOrgCD(), SequenceConstants.SEQUENCE_PATTERN));
        XingYe2019 xy2019 = new XingYe2019(zjlx, zjhm);
        String reqXml = XmlUtils.req2XML(xy, xy2019);
        String url2017 = baseUrl + path2019;
        String respXml = httpPost(url2017, reqXml);
        if (respXml != null) {
            Response<XingYeResp2019> response = XmlUtils.xml2Resp(respXml, XingYeResp2019.class);
            RespHead head = response.getHead();
            if (head.getRet_code().equals(EnumXyResponseCode.SUCCESS.getCode())) {
                xyrsp2019 = response.getBody();
                redisTemplate.opsForValue().set(redisKey, xyrsp2019);
                redisTemplate.expire(redisKey, 60L, TimeUnit.SECONDS);
            } else {
                log.error("个贷贷前测评2019接口失败，原因：{}", head.getRet_desc());
            }
        } else {
            log.error("个贷贷前测评2019接口连接失败");
        }
        return xyrsp2019;
    }


    /**
     * 发送http报文请求
     *
     * @param url
     * @param reqXml
     * @return
     */
    private String httpPost(String url, String reqXml) {
        log.info("请求兴业报文：{}", reqXml);
        String result = null;
        try {

            HttpResponse response = HttpRequest.post(url).timeout(10000)
                    .contentType("text/plain; charset=utf-8")
                    .body(reqXml)
                    .execute();
            log.info("请求兴业响应报文：{}", response.body());
            if (response.getStatus() == HttpStatus.HTTP_OK) {
                result = response.body();
            }
        } catch (Exception e) {
            log.error("请求兴业异常", e);
        }
        return result;
    }

	
}
