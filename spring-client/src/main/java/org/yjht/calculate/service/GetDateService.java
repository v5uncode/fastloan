package org.yjht.calculate.service;

import org.yjht.calculate.model.Admit;
import org.yjht.calculate.model.Calculate;
import org.yjht.calculate.model.LimitModel;
import org.yjht.calculate.model.PriceModel;
import org.yjht.calculate.model.Rate;
import org.yjht.xindai.common.resp.XingYeResp2019;

import java.math.BigDecimal;
import java.util.List;

public interface GetDateService {
	/**
	 * 获取微信准入数据
	 * @param idNo
	 * @return
	 */
	Calculate getWechatAdminDate(String idNo,String name);
    /**
     * 根据custId获取准入信息
     * @param custId
     * @return
     */
    List<Admit> getAdmitData(String custId,String applyAmount,String applyTerm);

    /**
     * 评级数据
     * @param custId
     * @return
     */
    Rate getRateData(String custId,BigDecimal applyAmount,Integer loanTimeLimit);

    /**
     * 额度模型数据
     * @param custId
     * @return
     */
    LimitModel getLimitModelData(String custId,BigDecimal applyAmount,Integer loanTimeLimit);

    /**
     * 利率定价模型数据
     * @param custId
     * @return
     */
    PriceModel getPriceModelData(String custId);

    /**
     * 客户信息贷前测评
     * 客户在贷款之前查询相关贷款的信息
     * @param zjlx 证件类型
     * @param zjhm 证件号码
     * @return
     */
    XingYeResp2019 oCustDere(String zjlx, String zjhm);

}
