package org.yjht.calculate.service.impl;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.yjht.base.impl.BaseServiceImpl;
import org.yjht.bean.FlInfoApproval;
import org.yjht.bean.FlInfoApprovalVo;
import org.yjht.bean.cust.CustBase;
import org.yjht.bean.vo.User;
import org.yjht.calculate.constants.AdmitResult;
import org.yjht.calculate.constants.InterfaceInfo;
import org.yjht.calculate.model.Admit;
import org.yjht.calculate.model.Calculate;
import org.yjht.calculate.model.LimitModel;
import org.yjht.calculate.model.ObjectResponseVo;
import org.yjht.calculate.model.PriceModel;
import org.yjht.calculate.model.Rate;
import org.yjht.calculate.service.FlInfoApprovalService;
import org.yjht.calculate.service.GetDateService;
import org.yjht.calculate.util.SendHttpUtil;
import org.yjht.exception.MyException;
import org.yjht.service.cust.CustBaseService;
import org.yjht.util.CommonUtil;
import org.yjht.util.DateTools;
import org.yjht.util.StringUtil;
import tk.mybatis.mapper.entity.Condition;

@Service
public class FlInfoApprovalServiceImpl extends BaseServiceImpl<FlInfoApproval, String> implements FlInfoApprovalService {
	@Autowired
	private CustBaseService custBaseService;
	@Autowired
    private GetDateService getDateService;
	//策略要执行的步骤
	private final String[] callstep={InterfaceInfo.ADMIT.getNo(),InterfaceInfo.RATE.getNo(),InterfaceInfo.LIMIT.getNo(),InterfaceInfo.PRICE.getNo()};
	@Override
	@Transactional
	public FlInfoApprovalVo calculate(String custId,BigDecimal applyAmount,Integer applyTerm) {
		User user=CommonUtil.getUser();
		String businessCode = StringUtil.getUUID();
		FlInfoApproval approval = new FlInfoApproval();
		approval.setCustId(custId);
		approval.setBusinessCode(businessCode);
		approval.setState("T");
		approval.setApplyAmount(applyAmount);//申请金额
		approval.setApplyTerm(applyTerm);//申请期限
		approval.setCrtDate(DateTools.getCurrentSysData(DateTools.DEFAULT_FORMAT));//计算日期
		approval.setLoseDate(DateTools.getCurrentSysMonth(3, DateTools.DEFAULT_FORMAT));//失效日期	
        CustBase base = custBaseService.findByCustid(custId);  
        if(base != null){
        	approval.setIdNo(base.getIdNo());//身份证号
            approval.setOrgName(base.getOrgCd());//机构名
            approval.setCustGrpGl(base.getCustGrpJl());
            approval.setCustGrpGlName(base.getCustGrpName());
            approval.setZyxm(base.getMainProject());
            
            //准入模型
            List<Admit> admitList = getDateService.getAdmitData(custId,applyAmount.toString(),applyTerm.toString());
            //评级模型
            Rate rate = getDateService.getRateData(custId,applyAmount,applyTerm);
            //额度测算模型
            LimitModel model = getDateService.getLimitModelData(custId,applyAmount,applyTerm);
            //利率定价模型
            PriceModel priceModel = getDateService.getPriceModelData(custId);
            priceModel.setLoanTimeLimit(Integer.valueOf(applyTerm));
            //实体封装
            Calculate calculate = new Calculate();
            calculate.setAdmitList(admitList);
            calculate.setPriceModel(priceModel);
            calculate.setLimitModel(model);
            calculate.setRate(rate);

            calculate.setCustcode(base.getCustId());//客户编号
            calculate.setIdnum(base.getIdNo());//证件号码
            calculate.setCustname(base.getCustName());//客户姓名
            calculate.setIdtype(base.getIdType());//证件类型
            calculate.setCallstep(callstep);
            calculate.setBusDate(DateTools.getCurrentSysData(DateTools.DEFAULT_FORMAT));//业务日期
            calculate.setCustId(base.getCustId());
            calculate.setOprCode(user.getUser_id());
            ObjectResponseVo vo = SendHttpUtil.sendPjcsHttp(calculate);
            if(vo.getIsException()){
            	throw new MyException(vo.getExceptionInfo());
            }else{
            	if(vo.getIsPass()){
            		approval.setZrResult(AdmitResult.PASS.getCode());
            		Map<String,Object> limitAmountResults = vo.getLimitAmountResults();
            		approval.setCreditAmount(limitAmountResults.get("C03")==null?null:new BigDecimal(limitAmountResults.get("C03").toString()));//信用额度
            		approval.setGuaranteeAmount(limitAmountResults.get("C02")==null?null:new BigDecimal(limitAmountResults.get("C02").toString()));
            		approval.setPledgeAmount(limitAmountResults.get("C01")==null?null:new BigDecimal(limitAmountResults.get("C01").toString()));
            		Map<String,Object> dayRates = vo.getDayRates();
            		approval.setCreditRate(dayRates.get("C03")==null?null:new BigDecimal(dayRates.get("C03").toString()));
            		approval.setGuaranteeRate(dayRates.get("C02")==null?null:new BigDecimal(dayRates.get("C02").toString()));
            		approval.setPledgeRate(dayRates.get("C01")==null?null:new BigDecimal(dayRates.get("C01").toString()));           		
            	}else{
            		approval.setZrResult(AdmitResult.REFUSE.getCode());
            		approval.setZrDesc(vo.getNotPassReason());
            	}
            	approval.setPjJb(vo.getTmpResult());
        		approval.setPjLmt(vo.getRateResult());
        		
        		FlInfoApproval oldApproval=new FlInfoApproval();
        		oldApproval.setState("F");
        		Condition condition = new Condition(FlInfoApproval.class);
        		condition.createCriteria().andEqualTo("idNo",base.getIdNo());
        		updateByExampleSelective(oldApproval, condition);
        		insertSelective(approval);
            }
            FlInfoApprovalVo approvalVo=new FlInfoApprovalVo();
            BeanUtils.copyProperties(approval, approvalVo);
    		if(approval.getZrResult().equals("F")){
    			approvalVo.setIsPass(false);
    			approvalVo.setNotPassReason(approval.getZrDesc());
    		}else{
    			approvalVo.setIsPass(true);
    		}
            return approvalVo;

        }else{
        	throw new MyException("查无数据");
        }
	}

}
