package org.yjht.calculate.service;

import java.math.BigDecimal;
import org.yjht.base.BaseService;
import org.yjht.bean.FlInfoApproval;
import org.yjht.bean.FlInfoApprovalVo;

public interface FlInfoApprovalService extends BaseService<FlInfoApproval, String>{
	
	public FlInfoApprovalVo calculate(String custId,BigDecimal applyAmount,Integer applyTerm);

}
