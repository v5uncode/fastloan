package org.yjht.calculate.model;

import java.math.BigDecimal;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 额度模型
 * @author lqw
 *
 */
@Setter
@Getter
@ToString
public class LimitModel{
	/**
	 * 对象类型（LimitAmountBorrower）
	 */
	private String objType;
	/**
	 * 借款人信贷已授信金额(18.02.01add lqw)
	 */
	private BigDecimal borrowerBalamt;
	
	/**
	 * 客户类型
	 */
	private String custType;
	/**
	 * 扩张系数
	 */
	private BigDecimal expandratio;  //策略（内部数据）
	/**
	 * 总资产
	 */
	private String familyAsset="0";
	/**
	 * 总负债
	 */
	private String familyDebt="0";
	/**
	 * 年收入
	 */
	private String familyIncome="0";
	/**
	 * 年支出
	 */
	private String familyExpend="0";
	
	//抵质押物有效价值
	private String pledgeValue="0";//抵质押物价值*抵质押率
	/**
	 * 评级相关净收入系数  净收入调整系数
	 */
	private BigDecimal familynetinratio; //策略（内部数据）
	/**
	 *担保方式
	 */
	private String guaranteeType;
	/**
	 * 申请金额
	 */
	private String applyAmount;
	/**
	 * 申请期限
	 */
	private Integer loanTimeLimit;
	/**
	 * 职业细分
	 */
	private String profession;
	/**
	 * 评级结果
	 */
	private String rateResult; //策略（内部数据）
	/**
	 * 净资产调整系数1    评级无关净资产系数:
	 */ 
	private BigDecimal rateralatefamilynetassetratioone;   //策略（内部数据）
	/**
	 * 净资产调整系数2   评级相关净资产系
	 */
	private BigDecimal rateralatefamilynetassetratiotwo;   //策略（内部数据）
	
	
	//-----------------新增字段-------------------------
	//征信他行非住房未结清贷款余额(元)
	private String crOtherFzfwjqdkye = "0";
	//征信他行住房未结清贷款余额(元)
	private String crOtherZfwjqdkye = "0";
	//征信他行住房未结清贷款月还款总额(元)
	private String crOtherZfyhk = "0";
	//征信信用卡授信总额度(元)
	private String crXyksxzed = "0";
	//征信信用卡已用总额度(元)
	private String crZxxykyyzed = "0";
	//有效授信额度
	private String yxsxye = "0";

	
}
