package org.yjht.calculate.model;

import org.yjht.util.StringUtil;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class PublicData implements Cloneable {
	//业务编号
	private String businesscode=StringUtil.getUUID();
	//当前操作员编码
	private String oprCode;
	//当前机构编码
	private String inscode="020109";;
	//当前地市编码
	private String cityCode = "020109";
	//当前法人编码
	private String legalCode="020109";
	//业务日期
	private String busDate;
	//自行阶段
	private String step;
	//核心地市编码   
	private String coreCityCode="020109";
	//所选评分卡类型
	private String rateCardCode;
	/**
	 *省联社准入标志（'1'为需要执行'0'为不需要） 
	 */
	private String privenceAdmit="0";
	/**
	 * 是否是批量预授信
	 */
	private boolean batch=false;
	/**
	 * 客户编号（新增）
	 */
	private String custId;
	/**
	 * 接口结果编号
	 */
	private String resultId;
	/**
	 * 接口调用阶段
	 */
	private String[] callstep;
	
}
