package org.yjht.calculate.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.yjht.core.Constants;

import java.math.BigDecimal;

@Setter
@Getter
@ToString
public class Admit {
	//-----------------以下为基本数据---------------------------
	//对象类型
	private String objType;
	//客户编号
	private String custCode;
	//证件类型
	private String cardType;
	//证件号码
	private String cardNum;
	//客户姓名
	private  String custName;



	//------------------省联社准入需加入的字段
	//身份证号码是否18位  是：T 否：F
	private String ifCardNum18Bit=Constants.YES;
	//身份是否唯一    从接口字段中获取证件号码进行判断，到ODS昨天的客户信息中查找是否存在多条即可，因为当天不能同时增加出两条相同证件号码的数据
	private String ifUniqueId=Constants.YES;

	//年龄
	private String age;
	//性别
	private String gender;
	//婚姻状况
	private String marrige;
	//家庭总资产
	private String familyAsset;
	//家庭总负债
	private String familyDebt;
	/**
	 * 行内数据
	 */
	//是否属于黑名单
	private String isBlackList = Constants.NO;
	//是否外部名单
	private String isOutList = Constants.NO;
	//行内近24个月是否存在存款账户冻结
	private String isFreeze = Constants.NO;
	//行内是否存在本金逾期记录
	private String isOverdue = Constants.NO;
	//行内表内未结清贷款最差五级分类形态
	private String worstFiveClass = "0";
	//行内对外担保表内未结清贷款最差五级分类形态
	private String inTableGuarWorstFiveClass = "0";
	//行内对外担保表内未结清信贷是否存在本金逾期或欠息
	private String isGuaranteInSheetBad  = Constants.NO;
	//行内是否存在对外担保表外未结清贷款
	private String isGuaranteOutSheetBad = Constants.NO;
	//行内是否存在表外未结清贷款
	private String isUnsettledOutSheetBad  = Constants.NO;
	//行内近36个月是否存在已结清表外贷款
	private String isSettledOutSheetBad  = Constants.NO;
	//行内近12个月是否存在展期
	private String isExend  = Constants.NO;
	//行内最近24个月本息最大逾期期数
	private String inBankOverdueMaxTimes24 = "0";
	//行内最近12个月本息逾期次数
	private String inBankOverdueTimes12 = "0";
	/**
	 * 征信数据
	 */
	//征信当前逾期期数
	private String crOverdueCount = "0";
	//征信近12个月本息逾期次数(银行贷款、信用卡)
	private String crOverdueCountIn12M = "0";
	//征信近24个月本息最大逾期期数(银行贷款、信用卡)
	private String crOverdueSerialIn24m = "0";
	//征信报告中贷款法人机构数
	private String crLoanOrgCount = "0";
	//征信近12个月征信报告查询次数
	private String crQueryCountIn12M = "0";
	//征信未销户贷记卡与准贷记卡最近6个月平均使用率
	private String withoutCreditAndSemiLateUseRatioIn6M = "0";
	//征信对外担保贷款本金余额(元)
	private String crGuranteeAmount = "0";
	//征信对外担保贷款最差五级分类形态
	private String crGuaranteWorstFiveClass = "0";
	//征信他行贷款最差五级分类形态
	private String crOtherWorstFiveClass = "0";
	//是否有征信信息
	private String isHaveCreditInfo;
	//征信近12个月本息逾期次数(信用卡)
	private String creditOverdueCardTimes12 = "0";
	//征信报告中最近12个月本息逾期次数(银行贷款)
	private String creditOverdueLoanTimes12 = "0";
	//征信报告中是否存在保证人代偿记录
	private String haveGuarantorHelp = Constants.NO;
	//征信报告中是否有助学贷款信息
	private String haveStudentLoan = Constants.NO;
	//征信报告中是否有职业信息
	private String haveVocation = Constants.NO;
	//征信报告中已激活未销户信用卡发卡法人机构数
	private String normalCreditCardCorpNum = "0";
	//征信报告中近12个月信用卡审批征信报告查询次数
	private String queryTimesWithCredit12 = "0";
	//征信报告中近12个月第一次信用卡审批查询后信用卡办理次数
	private String after12CreditFirstQueryCardTimes = "0";
	//近12个月贷款审批征信报告查询次数
	private String queryTimesWithLoan12 = "0";
	//征信报告中近12个月第一次贷款审批查询后贷款办理次数
	private String after12LoanFirstQueryLoanTimes = "0";
	//征信报告中未销户信用卡发卡机构数
	private String crCreditCardOrgCount = "0";
	//征信呆账信息汇总笔数
	private String crCountDebts = "0";
	//征信资产处置信息汇总笔数
	private String crCountReforms = "0";
	//征信保证人代偿信息汇总笔数
	private String crCountPensas = "0";
	//征信贷款逾期笔数
	private String crDueCounts = "0";
	//征信贷记卡逾期账户数
	private String crCardCounts = "0";
	//征信数据准贷记卡60天以上透支账户数
	private String crCounts60Days = "0";

	//-------------------库中未标记的datafrom的------分割线--------------------
	//是否新增客户
	private String isNewCust = "F";
	//授信申请期限
	private String applyTerm;
	//申请金额
	private String applyAmount;
	//申请产品编号
	private String productCode;
	
	
	
	//相关企业是否存在贷款余额
	private String isEtpHaveCredit = Constants.NO;
	//办理机构是否相同
	private String isOrgSame;
	//征信查询状态
	private String creditStatus;
	//最近一次结案距今年份数
	private String closeCaseYear;

	
	//---------------------------表中没有的----分割线-----------------------
	//36个月连续最高高逾期次数
	private String crOverdueSerialIn36m="0";
	//上层业务品种
	private String smallProduct;
	/**
	 * 是否正式客户（T:正式  F:否）
	 */
	private String ifFormalCust;
	
	/**
	 * 身份是否唯一(同一客户是否存在多个身份证号码)
	 */
	private String ifHaveGtOneId;
	/**
	 * 配偶或家庭成员状态(是否存在配偶或家庭成员待审核状态)
	 */
	private String famStatus = Constants.NO;
	/**
	 * 存量贷款发放类别(借款人及家庭成员存量贷款发放类别是否为借新还旧、资产重组、展期) T,F
	 */
	private String ifStoreLoan = Constants.NO;
	/**
	 * 担保贷款发放类别(借款人及家庭成员担保贷款发放类别是否为借新还旧、资产重组、展期)
	 */
	private String ifGuaranteeLoan = Constants.NO;
	/**
	 * 信用贷款(借款人是否在我行已存在担保方式为信用且非信e贷贷款的未到期贷款合同)
	 */
	private String ifCreditLoanSelf = Constants.NO;
	/**
	 * 家庭成员贷款(借款人家庭成员在我行是否存在未结清贷款)
	 */
	private String ifFamLoanNoFinish = Constants.NO;
	/**
	 * 信用贷款授信期限(授信期限)(月)
	 */
	private Integer giveCreditTerm;
	/**
	 * 评级情况（是否在信贷管理系统中评级授信）
	 */
	private String ifRateInXD = Constants.NO;
		
	//------------------分割线结束----------------------------
	//-------------------新增-----------------------------
	//授信审批授信余额
	private String sxspsxje;
	//采集人与主办客户经理是否一致
	private String ifCjryz;
	//是否有家庭成员
	private String ifHaveFamily =  Constants.YES;
	//有效授信余额
	private String useuamt;
	/**
	 * 企业总资产
	 */
	private BigDecimal etpAsset;
		
}
