package org.yjht.calculate.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class ObjectResponseVo extends ObjectResponse{
	/**
	 * 运算是否异常
	 */
	private Boolean isException;
	/**
	 * 异常信息
	 */
	private String exceptionInfo;
	/**
	 * 是否通过
	 */
	private Boolean isPass;
	/**
	 * 不通过理由
	 */
	private String notPassReason;
}
