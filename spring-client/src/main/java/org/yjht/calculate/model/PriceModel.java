package org.yjht.calculate.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 利率定价模型
 * @author lqw
 *
 */
@Setter
@Getter
@ToString
public class PriceModel {
	//-----------------以下为基本数据---------------------------------
	/**
	 * 对象类型（PriceBorrower）
	 */
	private String objType;
	

	/**
	 * 客户类型
	 */
	private String custType;
	/**
	 * 担保方式
	 */
	private String guaranteeType;
	/**
	 * 贷款期限
	 */
	private Integer loanTimeLimit;
	/**
	 * 职业细分
	 */
	private String profession;
	/**
	 * 评级结果（内部）	
	 */
	private String rateResult;  //策咯内部
	

}
