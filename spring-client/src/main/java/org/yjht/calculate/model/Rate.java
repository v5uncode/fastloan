package org.yjht.calculate.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import org.yjht.core.Constants;

import com.fasterxml.jackson.annotation.JsonProperty;

@Setter
@Getter
@ToString
public class Rate {
	//-----------------以下为基本数据---------------------------
	//对象类型
	private String objType;

	/**
	 * 征信
	 */
	//是否有征信报告
	private String isHaveCreditInfo = Constants.NO;
	//征信最近12个月贷记卡开户个数
	private String crCreditcardOpenCount = "0";
	//征信最早贷款账户距今月份数(银行贷款、信用卡)
	private String crEarly = "999";
	//征信对外担保贷款本金余额(元)
	private String crGuranteeAmount = "0";
	//征信最近24个月本息逾期次数(银行贷款、信用卡)
	private String crOverdueCountI24M = "0";
	//征信最近12个月本息逾期次数(银行贷款、信用卡)
	private String crOverdueCountIn12M = "0";
	//征信最近36个月本息逾期次数(银行贷款、信用卡)
	private String crOverdueCountIn36M = "0";
	//征信近6个月本息逾期次数（银行贷款、信用卡）（评级）
	private String crOverdueCountIn6M = "0";
	//征信最近12个月本息最大逾期期数(银行贷款、信用卡)
	private String crOverdueMaxCountIn12M = "0";
	//征信最近24个月本息最大逾期期数(银行贷款、信用卡)
	private String crOverdueMaxCountIn24M = "0";
	//征信最近6个月本息最大逾期期数(银行贷款、信用卡)
	private String crOverdueMaxCountIn6M = "0";
	//征信本息最近一次逾期距离现在的月份数(银行贷款、信用卡)
	private String crOverdueMonthCount = "999";
	//最近6个月征信报告查询次数
	private String crQueryCountIn6M = "0";
	//最近12个月征信报告查询次数
	private String crQueryCountIn12M = "0";
	//最近24个月征信报告查询次数
	private String crQueryCountIn24M = "0";
	//最近36个月征信报告查询次数
	private String crQueryCountIn36M = "0";
	//征信是否有本息逾期 
	private String creIsOverdueInAndC = Constants.NO;
	//征信贷记卡数量(不含销户) 
	private String creditCardCount = "0";
	//当前征信信用卡额度使用率(评级)   
    private String creditCardUseRatio = "0";
	/**
	 * 采集
	 */
	/*基本信息*/
	//近三年实际控制人变更情况
	private String actualControllerChangeIn3Y = Constants.NO;
	//客户类型
	private String custType;
	//主营项目
	private String mainProject;
	//年龄
	private String age;
	//性别
	private String gender;
	//学历
	private String edu;
	//职业细分
	private String profession;
	//行业分类一级
	private String industryone;
	//行业分类二级
	private String industrytwo;
	//行业分类三级
	private String industrythree;
	//行业分类四级
	private String industryfour;
	/*财务信息*/
	//营运车辆数量
	private String carCount;
	//家庭总资产
	private String familyAsset;
	//家庭总负债
	private String familyDebt;
	//截至目前家庭支出
	private String familyExpendThisYear;
	//截至目前家庭收入
	private String familyIncomeThisYear;
	//近三年经营收入
	private String last3yearOperateIncome;
	//近三年经营支出
	private String last3yearOperateOutcome;
	//存货（元）
	private String stockCount;
	//家庭或有负债
	private String familyPossibleDebt;
	//去年家庭收入
	private String familyIncomeLastYear;
	//前年家庭收入
	private String familyIncomeBeforeLastYear;
	//去年家庭年支出
	private String familyExpendLatsYear;
	//前年家庭年支出
	private String familyExpendBeforeLastYear;
	//上年度经营支出
	private String projectExpendLastYear;
	//上年度经营收入
	private String projectIncomeLastYear;
	
	//有无子女
	private String havaChild;
	//是否购买车库
	private String isBuyGarage;
	//是否在城区购买商住商用房
	private String isBuyHouse;
	//是否在固定日发放工资
	private String isFixedWages;
	//是否有固定工作
	private String isgdWork=Constants.YES;
	//自有房产数量
	private String ownHouseCount;	
	//婚姻状况
	private String marriage;
	//婚龄
	private String marriageYear;//婚龄通过结婚登记日期进行计算
	//借款人及家庭成员工作情况
	private String workStatus;
	//居住状况
	private String liveStatus;
	
	/*工作经营信息*/
	//医疗保险上年度年缴纳总额（家庭总额）
	private String miAmount;
	//种植亩数
	private String muSum;	
	//养殖/种植/经营年限
	private String plantYear;	
	//生产经营面积
	private String projectArea;
	//经营项目固定资产
	private String projectAssets;
	//生产经营场所总建筑面积
	private String projectBuildArea;
	//经营项目负债
	private String projectDebt = "0";
	//经营项目应收账款
	private String projectShouldIncome="0";
	//职工数量
	private String staffCount;
	//有人身意外伤害保险或工伤保险的员工数量
	private String insuranceStaffCount;
	//本单位起始年份 （计算工作年限）
	private String workYear="0";
	//种植模式
	private String zzModel;
	//大棚面积
	private String greenhoustArea="0";
	// 露天面积
	private String openArea="0";
	//借款人与下游客户平均合作年限
	private String averateCooYear = "0";
	//养殖类型  (海产品、其他)
	private String yzType;
	//养殖种类
	private String yzKind;
	//养殖模式
	private String yzModel;
	//养殖规模
	private String yzScale;
	//养殖周期(月)
	private Integer yzCycle;
	//海水养殖贷款用途
	private String yzLoanUse;
	
	//申请时点所在月份
	private String upOrDown;
    
	/**
	 * 行内数据
	 */
	//是否有本行银行卡
	private String isHaveOurBankCard = Constants.NO;
	//是否有本行存储帐户
	private String isHaveOurStorageAcc = Constants.NO;
	//最近一次本金逾期距离现在的月数(本行)
	private String moneyOverdueMonthCount = "999";
	//行内贷款申请时点敞口数
	private String exposureCount = "0";
	//近6个月日均存款金额
	private String dailyDepositAmountIn6M = "0";//行内
	//近9个月日均存款金额
	private String dailyDepositAmountIn9M = "0";//行内
	//近12个月日均存款金额
	private String dailyDepositAmountIn12M = "0";//行内
	//近6个月（存款+理财）日均额
	private String dailyFinAndDepAmountIn6M = "0";//行内
	//近9个月（存款+理财）日均额
	private String dailyFinAndDepAmountIn9M = "0";//行内
	//近12个月（存款+理财）日均额
	private String dailyFinAndDepAmountIn12M = "0";//行内
	//近12个月ARM交易次数夜间
	@JsonProperty("ATMCountIn12M")
	private String ATMCountIn12M = "0";     //行内
	//最近12个月ATM与pos交易次数之和
	@JsonProperty("ATMPOSCountIn12M")
	private String ATMPOSCountIn12M = "0";   //行内
	//行内未结清贷款账户数
	private String accountAcount = "0";
	//申请时点其他敞口个数
	private String otherExposureCount = "0";
	//本行是否有本金逾期
	private String ourIsOverdueCap = Constants.NO;
	//本行是否有本息逾期
	private String ourIsOverdueInAndC = Constants.NO;
	//行内近6个月本息逾期次数
	private String overdueCountIn6M = "0";	
	//征信近12个月本息逾期次数
	private String overdueCountIn12M = "0";
	//行内最近24个月本息逾期次数
	private String overdueCountIn24M = "0";
	//行内最近36个月本息逾期次数
	private String overdueCountIn36M = "0";	
	
	//行内最近6个月本息最大逾期期数
	private String overdueMaxCountIn6M = "0";
	
	//行内最近24个月本息最大逾期期数
	private String overdueMaxCountIn24M = "0";
	
	//行内最近一次本息逾期距离现在的月数
	private String overdueMonthCount = "999";
	//最近12个月POS交易次数（夜间）
	private String posNightCountIn12M = "0";
	
	//最近一次利息逾期距离现在的月数(本行)
	private String rateOverdueMonthCount = "0";
	//与银行交易时长
	private String transactionDuration = "0";
	//pos消费次数
	private String posConsumeCount = "0";
	//行内是否存在利息逾期记录
	private String isHavelxdue = Constants.NO;
	//行内近12个月本金逾期次数
	private String overbjCountIn12M = "0";
	//行内近24个月本金逾期次数
	private String overbjCountIn24M = "0";
	//行内近36个月本金逾期次数
	private String overbjCountIn36M = "0";
	//行内近6个月本金逾期次数
	private String overbjCountIn6M = "0";
	//行内近12个月本金最大逾期期数
	private String overbjMaxCountIn12M = "0";
	//行内近24个月本金最大逾期期数
	private String overbjMaxCountIn24M = "0";
	//行内近6个月本金最大逾期期数
	private String overbjMaxCountIn6M = "0";
	//行内近12个月本息最大逾期期数
	private String overbxMaxCountIn12M = "0";
	//行内近12个月利息逾期次数
	private String overlxCountIn12M = "0";
	//行内近24个月利息逾期次数
	private String overlxCountIn24M = "0";
	//行内近36个月利息逾期次数
	private String overlxCountIn36M = "0";
	//行内近6个月利息逾期次数
	private String overlxCountIn6M = "0";
	//行内近12个月利息最大逾期期数
	private String rateOverdueMaxCountIn12M  = "0";	
	//行内近24个月利息最大逾期期数
	private String overlxMaxCountIn24M = "0";
	//行内近6个月利息最大逾期期数
	private String overlxMaxCountIn6M = "0";

	
	//-------------库中没有标注datafrom的-分割线------------------------	
	//申请额度
	private String applyAmount;
	//申请日期
	private String applyDate;
	//首付比例
	private String downpayment;
	//对外担保本金余额（家庭）
	private String faOutGuarAmount;
	//是否有逾期记录
	private String isHaveOverdue;
	//最近一次贷款发放日
	private String loanPayDate;
	//申请产品编号
	private String productCode;
	//拟购房产类别
	private String wantHouseType;

	
	//-----------------库中没有的---------分割线------------------------

	//征信未结清贷款余额汇总
	private String crLoanbalancesum;
	//征信未销户贷记卡余额汇总
	private String crCreditcardbalancesum;
	//征信未销户准贷记卡余额汇总
	private String crQuasicreditcardbalancesum;
	//征信家庭或有负债
	private String crFamily;
	//申请产品小类
	private String smallProduct;
	//拟购房屋套数
	private String wantHouseCount;
	//本行贷款余额
	private String ourBackLoanBal;
	//他行贷款余额
	private String otherBackLoanBal;
	//贷记卡已用额度
	private String cardUserLimit;
	//准贷记卡透支额度
	private String cardBeyondLimit;
	//是否有银行卡
	private String isHaveBankCard;
	//有效授信额度
	private String effectCreditAmount;
	//打分卡映射逻辑申请额度
	private String applyLimit;
	//规则运算用申请额度
	private String applyMoney;
	
	

}
