package org.yjht.calculate.model;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
@Setter
@Getter
@ToString
public class Calculate extends PublicData{
	/**
	 * 客户编号
	 */
	private String custcode;
	/**
	 * 客户姓名
	 */
	private String custname;
	/**
	 * 证件类型
	 */
	private String idtype;
	/**
	 * 身份证号码
	 */
	private String idnum;  
	
	private PriceModel priceModel;//评级模型数据
	
	private LimitModel limitModel;//额度模型数据
	
	private Rate rate;//评级数据
	
	private List<Admit> admitList;//准入数据

}
