package org.yjht.calculate.model;

import java.util.Map;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class ObjectResponse {

	private String code;  //是否通过  0000代表通过

	private String desc;  //描述
	
	private String token;
	
	private String haveCreditInfo;
	/**
	 * 准入结果
	 */
	private String admitResult;//准入结果 0通过 1拒绝
		
	/**
	 * 评级结果
	 */
	private String rateResult;
	/**
	 * 额度
	 */
	private String limitAmountResult;
	
	private Map<String,Object> limitAmountResults;//C01 抵质押  C02 保证  C03 信用
	/**
	 * 利率
	 */
	private String dayRate;
	
	private Map<String,Object> dayRates;//C01 抵质押  C02 保证  C03 信用
	/**
	 * 评级级别
	 */
	private String tmpResult;
	
	private String limit;
	
	private String productCode;
	
}
