package org.yjht.calculate.constants;

public enum AdmitResult {
    PASS("T"), //通过
    REFUSE("F"); //拒绝
    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    private AdmitResult(String code){
        this.code = code;
    }

}
