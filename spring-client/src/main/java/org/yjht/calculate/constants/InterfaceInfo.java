package org.yjht.calculate.constants;
/**
 * 接口数字及code对照表
 * @author lqw
 *
 */
public enum InterfaceInfo {
	CREDIT("1","CREDIT001"),//征信
	ADMIT("2","ADMIT001"),//准入
	RATE("3","RATE001"),//评级
	LIMIT("4","LIMIT005"),//额度
	PRICE("5","PRICE001"),//定价
	RISK("6","RISK001"),//贷后预警
	RATE_ADMIT("7","RATEADMIT001"),//评级准入
	CREDIT_TERM("8","CREDITTERM001");//授信期限
	
	private String no;
	private String code;
	private InterfaceInfo(String no,String code) {
		this.no = no;
		this.code = code;
	}
	public String getNo() {
		return no;
	}
	public void setNo(String no) {
		this.no = no;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	
	
}
