package org.yjht.calculate.constants;

public class ObjectCode {
	public static final int admit = 1;//准入策略(单一)
	public static final int limitAmount = 2;//额度计算
	public static final int rate = 3;//利率定价
	public static final int rateAdmit = 4; //评级模型
	public static final int lastTrial = 5;//评级准入
	public static final int processTrend = 6;
//	public static final String rateSurvey = "1";
//	public static final String rateBase = "2";
//	public static final String rateCredit = "3";
	public static final int price = 7;//贷后预警
	public static final int admitGroup = 8;//准入策略(组合)
	public static final int limitCredit = 9;//授信期限

	public enum Admit {
		AdmitBorrower, AdmitWarrant, AdmitRelateCompany, AdmitFamily;
	}

	public enum LimitAmount {
		LimitAmountBorrower, LimitAmountWarrant;
	}

	public enum Rate {
		RateBorrower,RateWarrant;
		//RateSurvey,RateBase,RateCredit;
	}

//	public enum FirstTrial {
//		FirstTrial;
//	}

	public enum LastTrial {
		LastTrial;
	}
	
	public enum ProcessTrend {
		ApprovalProcessTrend;
	}
	/**
	 * 授信期限
	 * @author lqw
	 *
	 */
	public enum LimitCredit{
		limitCredit;
	}
	/**
	 * 定价
	 * @author lqw
	 *
	 */
	public enum Price{
		PriceBorrower;
	}
	/**
	 * 评级准入 适用对象
	 * @author lqw
	 */
	public enum RateAdmit{
		ApplyCard;
	}
	
	public static String getObjectCode(int type, String objectType) {
		String result = "";
		if(type < admit | type >limitCredit)
			return result;
		if (type == admit ||type == admitGroup ) {
			if(ObjectType.borrower.getCode().equals(objectType))
				return Admit.AdmitBorrower.toString();
			else if(ObjectType.warrant.getCode().equals(objectType))
				return Admit.AdmitWarrant.toString();
			else if(ObjectType.relateCompany.getCode().equals(objectType))
				return Admit.AdmitRelateCompany.toString();
			else if(ObjectType.family.getCode().equals(objectType))
				return Admit.AdmitFamily.toString();
		} else if (type == limitAmount) {
			if(ObjectType.borrower.getCode().equals(objectType))
				return LimitAmount.LimitAmountBorrower.toString();
			else if(ObjectType.warrant.getCode().equals(objectType))
				return LimitAmount.LimitAmountWarrant.toString();;
		} else if (type == rate) {
			if(ObjectType.borrower.getCode().equals(objectType)){
				return Rate.RateBorrower.toString();
			}else if(ObjectType.warrant.getCode().equals(objectType)){
				return Rate.RateWarrant.toString();
			}
			/*if(rateSurvey.equals(objectType))
				return Rate.RateSurvey.toString();
			else if(rateBase.equals(objectType))
				return Rate.RateBase.toString();
			else if(rateCredit.equals(objectType))
				return Rate.RateCredit.toString();*/
		} else if (type == rateAdmit) {
			return RateAdmit.ApplyCard.toString();
		} else if (type == lastTrial) {
			return LastTrial.LastTrial.toString();
		} else if (type == processTrend) {
			return ProcessTrend.ApprovalProcessTrend.toString();
		}else if(type==price){
			if(ObjectType.borrower.getCode().equals(objectType))
			return Price.PriceBorrower.toString();
		}else if(type==limitCredit){
			return LimitCredit.limitCredit.toString();
		}
		return result;
	}
}
