package org.yjht.calculate.util;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.core.env.Environment;
import org.yjht.calculate.model.Calculate;
import org.yjht.calculate.model.ObjectResponse;
import org.yjht.calculate.model.ObjectResponseVo;
import org.yjht.core.ErrorCode;
import org.yjht.util.HttpUtil;
import org.yjht.util.JsonUtils;
import org.yjht.util.SpringUtil;

import lombok.extern.slf4j.Slf4j;
@Slf4j
public class SendHttpUtil {
	public static ObjectResponseVo sendPjcsHttp(Calculate calculate){
		
		Environment environment = SpringUtil.getBean(Environment.class);
		String url= environment.getProperty("calculate.url");
		log.info("向策略发送请求开始，地址{}",url);
		ObjectResponseVo vo=new ObjectResponseVo();
		vo.setIsPass(true);
		vo.setIsException(false);
		//参数为空
		if(calculate == null){
			vo.setIsException(true);
			vo.setExceptionInfo(ErrorCode.PARAM_ISNULL.getCode());
			return vo;
		}
		//证件号码为空
		if(calculate.getIdnum()== null){
			vo.setIsException(true);
			vo.setExceptionInfo(ErrorCode.ID_NO_ISNULL.getCode());
			return vo;
		}
		//客户姓名为空
		if(calculate.getCustname()== null){
			vo.setIsException(true);
			vo.setExceptionInfo(ErrorCode.NAME_ISNULL.getCode());
			return vo;
		}
		Map<String,Object> map =new HashMap<String,Object>();
		map.put("calculate", calculate);
		log.info("参数：{}",JsonUtils.objectToJson(map));
		String result =HttpUtil.doPostJson(url,JsonUtils.objectToJson(map));
		log.info("结果：{}",result);
		if(StringUtils.isBlank(result)) {
			vo.setIsException(true);
			vo.setExceptionInfo("网络接口异常");
		}else{
			ObjectResponse response=null;
			try{
				response=JsonUtils.jsonToPojo(result, ObjectResponse.class);
			}catch(Exception e){
				vo.setIsException(true);
				vo.setExceptionInfo("与策略引擎交互失败");
				return vo;
			}
			if(!response.getCode().equals("00000")) {
				vo.setIsException(true);
				vo.setExceptionInfo(response.getDesc());
				return vo;
			}
			if(response.getAdmitResult().equals("1")){
				vo.setIsPass(false);
				vo.setNotPassReason(response.getDesc());
				return vo;
			}
			BeanUtils.copyProperties(response, vo);
		}
		return vo;
		
	}

}
