package org.yjht.service;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.yjht.bean.LrdUser;
import org.yjht.core.Result;

public interface LrdUserService {
	/**
	 * 通过登录名查找用户
	 * @param userId
	 * @return
	 */
	LrdUser getUserByUsername(String userId); 
	Result queryTree(HttpServletRequest request, Map<String, Object> map);
	Result resetPwd(LrdUser lrdUser, HttpServletRequest request);
	Result insertUser(LrdUser user);
	Result queryInsert(HttpServletRequest request);
	Result updateUser(LrdUser user);
	Result deleteUserAndRole(String userId);
	Result queryOrgTree(HttpServletRequest request);
	Result updatePwd(HttpServletRequest request, Map<String, Object> map);	
	Result queryRoleList(HttpServletRequest request);
	List<LrdUser> selectAll();

	/**
	 * 查找指定法人，机构，角色的用户信息
	 * @param corpCd
	 * @param orgCd
	 * @param roleId
	 * @return
	 */
	List<LrdUser> findBranchManagerByOrgCd(String corpCd,String orgCd,String roleId);



}
