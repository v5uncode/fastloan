package org.yjht.service.Impl;

import java.util.List;

import org.springframework.stereotype.Service;
import org.yjht.bean.LrdUserrole;
import org.yjht.service.UserRoleService;
import org.yjht.service.base.AbstractService;
@Service
public class UserRoleServiceImpl extends AbstractService<LrdUserrole> implements UserRoleService {

	@Override
	public List<LrdUserrole> selectAll() {
		return findAll();
	}

}
