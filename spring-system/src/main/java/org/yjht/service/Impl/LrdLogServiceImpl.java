package org.yjht.service.Impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.yjht.bean.LrdLog;
import org.yjht.core.Result;
import org.yjht.mapper.LrdLogMapper;
import org.yjht.dao.LrdUserMapper;
import org.yjht.service.LrdLogService;
import org.yjht.util.DateTools;
import org.yjht.util.ResultGenerator;

import tk.mybatis.mapper.entity.Condition;
import tk.mybatis.mapper.entity.Example.Criteria;
@Service
public class LrdLogServiceImpl implements LrdLogService {
	@Autowired
	private LrdUserMapper lrdUserMapper;
	@Autowired
	private LrdLogMapper lrdLogMapper;
	@Override
	public Result deleteSysLogByTime() {
		// TODO Auto-generated method stub
		Condition condition = new Condition(LrdLog.class);
		Criteria criteria=condition.createCriteria();
		criteria.andCondition("CRT_TIME <=",DateTools.getCurrentSysData(-30, DateTools.FULLTIME_FORMAT));
		lrdLogMapper.deleteByExample(condition);
		return ResultGenerator.genSuccessResult();
	}
}
