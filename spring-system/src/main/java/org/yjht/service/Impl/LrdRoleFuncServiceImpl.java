package org.yjht.service.Impl;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.yjht.bean.LrdRolefunc;
import org.yjht.core.Result;
import org.yjht.dao.LrdRolefuncMapper;
import org.yjht.service.LrdRoleFuncService;
import org.yjht.util.ResultGenerator;
import org.yjht.web.LrdRoleFuncController;

import tk.mybatis.mapper.entity.Condition;
import tk.mybatis.mapper.entity.Example.Criteria;

@Service
public class LrdRoleFuncServiceImpl  implements LrdRoleFuncService {
	
	@Autowired
	LrdRolefuncMapper lrdRolefuncMapper;
	@Override
	public Result findLrdRoleFunc(LrdRolefunc lrdRolefunc) {
		// TODO Auto-generated method stub
		Condition condition = new Condition(LrdRolefunc.class);
		Criteria criteria = condition.createCriteria();
		if(StringUtils.isNotBlank(lrdRolefunc.getCorpCd())){
			criteria.andEqualTo("corpCd",lrdRolefunc.getCorpCd());
		}
		if(StringUtils.isNotBlank(lrdRolefunc.getRoleCd())){
			criteria.andEqualTo("roleCd",lrdRolefunc.getRoleCd());
		}
		List<LrdRolefunc> list = lrdRolefuncMapper.selectByExample(condition);
		return ResultGenerator.genSuccessResult(list);
	}

}
