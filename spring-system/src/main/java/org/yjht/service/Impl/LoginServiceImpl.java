package org.yjht.service.Impl;

import java.util.ArrayList;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.yjht.bean.LrdUser;
import org.yjht.dao.LrdUserMapper;
import org.yjht.service.LoginService;
import org.yjht.service.base.AbstractService;
@Service
public class LoginServiceImpl extends AbstractService<LrdUser> implements LoginService {
	@Autowired
	LrdUserMapper lrdUserMapper;
	@Override
	public LrdUser findUserByName(String userName) {
		LrdUser lrdUser = findById(userName);
		return lrdUser;
	}
	@Override
	public ArrayList<Map<String,Object>> queryRole(String userId) {
		return lrdUserMapper.queryRole(userId);
	}

}
