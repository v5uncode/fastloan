package org.yjht.service.Impl;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.yjht.bean.LrdOrg;
import org.yjht.bean.LrdUser;
import org.yjht.bean.SysLog;
import org.yjht.core.Page;
import org.yjht.core.Result;
import org.yjht.dao.LrdOrgMapper;
import org.yjht.dao.LrdUserMapper;
import org.yjht.dao.SysLogMapper;
import org.yjht.service.SysLogServcie;
import org.yjht.service.base.AbstractService;
import org.yjht.util.ResultGenerator;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import tk.mybatis.mapper.entity.Condition;
import tk.mybatis.mapper.entity.Example.Criteria;


@Service
public class SysLogServiceImpl extends AbstractService<SysLog> implements SysLogServcie{
	
	@Autowired
	SysLogMapper sysLogMapper;
	@Autowired
	private LrdOrgMapper lrdOrgMapper;
	@Autowired
	private LrdUserMapper lrdUserMapper;
	
	@Override
	public Result findSysLog(SysLog sysLog,Page page) {
		// TODO Auto-generated method stub
			if(page.getLimit()!=null&& page.getPage()!=0){
				Condition condition = new Condition(SysLog.class);
				Criteria criteria=condition.createCriteria();
				if(StringUtils.isNotBlank(sysLog.getCrtDate())) {
					criteria.andCondition("OPER_DATE >=", (sysLog.getCrtDate()+"").replace("/", ""));
				}
				if(StringUtils.isNotBlank(sysLog.getMtnDate())) {
					criteria.andCondition("OPER_DATE <=", (sysLog.getMtnDate()+"").replace("/", ""));
				}
				if(StringUtils.isNotBlank(sysLog.getOrgCd())){
					criteria.andEqualTo("orgCd",sysLog.getOrgCd());
				}
				if(StringUtils.isNotBlank(sysLog.getUserId())){
					String[] userIDs = sysLog.getUserId().toString().split(",");
					List<String> userIds=Arrays.asList(userIDs);
					criteria.andIn("userId", userIds);
				}
				PageHelper.startPage(page.getPage(),page.getLimit(), true);
				List<SysLog> list = findByCondition(condition);
				page.setRows(list);
				page.setTotal(new PageInfo(list).getTotal());
				return ResultGenerator.genSuccessResult(page);
			}
			return ResultGenerator.genFailResult("查询失败");
	}

	@Override
	public Result findByPK(Map<String,Object> map) {
		// TODO Auto-generated method stub
		List<LrdUser> listLrdUser =lrdUserMapper.selectAll();
		List<Map<String,Object>> listLrdOrg = lrdOrgMapper.findMenu(map);
		Map<String,Object> resultmap=new HashMap<String, Object>();
		resultmap.put("org", listLrdOrg);
		resultmap.put("user", listLrdUser);
		return ResultGenerator.genSuccessResult(resultmap);
	}

	@Override
	public Result deleteSysLog() {
		// TODO Auto-generated method stub
		sysLogMapper.deleteAll();
		return ResultGenerator.genSuccessResult();
	}
}
