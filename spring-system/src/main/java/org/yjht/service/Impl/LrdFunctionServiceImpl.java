package org.yjht.service.Impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.yjht.bean.LrdFunction;
import org.yjht.core.Result;
import org.yjht.dao.LrdFunctionMapper;
import org.yjht.service.LrdFunctionService;
import org.yjht.service.base.AbstractService;
import org.yjht.util.ResultGenerator;

import tk.mybatis.mapper.entity.Condition;
import tk.mybatis.mapper.entity.Example.Criteria;

@Service
public class LrdFunctionServiceImpl extends AbstractService<LrdFunction> implements LrdFunctionService{
	
	@Autowired
	LrdFunctionMapper lrdFunctionMapper;
	
	@Override
	public Result queryTree(String sysCd) {
		// TODO Auto-generated method stub
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("sysCd", sysCd);
		List<Map<String,Object>>  list= lrdFunctionMapper.queryTree(map);
		return ResultGenerator.genSuccessResult(list);
	}

	@Override
	public Result insertLrdFunction(LrdFunction lrdFunction) {
		// TODO Auto-generated method stub
			int result = lrdFunctionMapper.insertSelective(lrdFunction);
			return ResultGenerator.genSuccessResult(result);
	}

	@Override
	public Result queryInsert(String funcCd) {
		// TODO Auto-generated method stub
		LrdFunction lrdFunction = findById(funcCd);
		if(lrdFunction!=null){
			return ResultGenerator.genFailResult("功能代号已经存在");
		}
		return ResultGenerator.genSuccessResult(lrdFunction);
	}

	@Override
	public Result updateFunction(LrdFunction lrdFunction) {
		// TODO Auto-generated method stub
			int result = lrdFunctionMapper.updateByPrimaryKeySelective(lrdFunction);
			return ResultGenerator.genSuccessResult(result);
	}

	@Override
	public Result deleteFunction(LrdFunction lrdFunction) {
		// TODO Auto-generated method stub
		Condition condition = new Condition(LrdFunction.class);
		Criteria criteria = condition.createCriteria();
		criteria.andCondition("FUNC_PCD = ",lrdFunction.getFuncCd());
		List<LrdFunction> list = findByCondition(condition);
		if(list.size()>0){
			return ResultGenerator.genFailResult("该菜单包含子功能，请先删除子功能");
		}
		int result = lrdFunctionMapper.delete(lrdFunction);
		return ResultGenerator.genSuccessResult(result);
	}

}
