package org.yjht.service.Impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.yjht.bean.LrdOrg;
import org.yjht.bean.vo.User;
import org.yjht.core.Constants;
import org.yjht.core.Result;
import org.yjht.dao.LrdOrgMapper;
import org.yjht.dao.LrdRoleMapper;
import org.yjht.service.LrdOrgService;
import org.yjht.service.base.AbstractService;
import org.yjht.thread.AsyncTaskServcie;
import org.yjht.util.DicReplace;
import org.yjht.util.ResultGenerator;

@Service
public class LrdOrgServiceImpl extends AbstractService<LrdOrg> implements LrdOrgService {
	
	@Autowired
	LrdOrgMapper lrdOrgMapper;
	
	@Autowired
	LrdRoleMapper lrdRoleMapper;
	@Autowired
	AsyncTaskServcie asyncTaskServcie;
	@Override
	public Map<String,Object> queryUserOrg(Map<String,Object> map) {
		return lrdOrgMapper.queryUserOrg(map);
	}
	@Override
	public String getChilds(String orgCd) {
		List<Map<String, Object>> list = lrdOrgMapper.getChilds(orgCd);
		StringBuffer sb = new StringBuffer();
		for (Map<String, Object> map2 : list) {
			sb.append(map2.get("childIds"));
			sb.append(",");
		}
		if(StringUtils.isNotBlank(sb)) {
			sb.deleteCharAt(sb.length() - 1);
		}
		return sb.toString();
	}
	@Override
	public List<String> getChildList(String orgCd) {
		return lrdOrgMapper.getChildList(orgCd);
	}
	@Override
	//@Cacheable(value = "org",key="#user.getCorpCD()")
	public Result queryMenu(User user, Map<String, Object> map) {
		if (!Constants.SLS_CORP.equals(user.getCorpCD())) {
			map.put("corpCd", user.getCorpCD());
		}
		List<Map<String,Object>> list = lrdOrgMapper.findMenu(map);
		Map<String,String> dicMap=new HashMap<String,String>();
		dicMap.put("orgFlag", "SYS_ORG_FLAG");
		dicMap.put("orgVflag", "SYS_ORG_VFLAG");
		DicReplace.replaceDic(list, dicMap);
		return ResultGenerator.genSuccessResult(list);
	}
	
	@Override
	//@CacheEvict( value= "org",key="#user.getCorpCD()",beforeInvocation=true)
	public Result saveLrdOrg(User user,LrdOrg lrdOrg) {
		LrdOrg newLrdOrg =new LrdOrg();
		newLrdOrg.setCorpCd(lrdOrg.getCorpCd());
		newLrdOrg.setOrgCd(lrdOrg.getOrgCd());
		List<LrdOrg> listLrdOrg = lrdOrgMapper.select(newLrdOrg);
		if(listLrdOrg.size()>0){
			return ResultGenerator.genFailResult("机构代码重复");
		}
		
		int n = lrdOrgMapper.insertSelective(lrdOrg);
		if(n>0){
			Map<String,Object> map=new HashMap<String,Object>();
			map.put("orgRelaType", lrdOrg.getOrgRelaType());
			//asyncTaskServcie.executeAsyncTas(user, map);
			return ResultGenerator.genSuccessResult("保存成功");
		}else{
			return ResultGenerator.genFailResult("保存失败");
		}
	}

	@Override
	//@CacheEvict( value= "org",key="#user.getCorpCD()",beforeInvocation=true)
	public Result updateLrdOrg(User user,LrdOrg lrdOrg) {
		// TODO Auto-generated method stub
		int n = lrdOrgMapper.updateByPrimaryKeySelective(lrdOrg);
		if(n>0){
			Map<String,Object> map=new HashMap<String,Object>();
			map.put("orgRelaType", lrdOrg.getOrgRelaType());
			//asyncTaskServcie.executeAsyncTas(user, map);
			return ResultGenerator.genSuccessResult("修改成功");
		}else{
			return ResultGenerator.genSuccessResult("修改失败");
		}
	}
	
	@Transactional
	@Override
	//@CacheEvict( value= "org",key="#user.getCorpCD()",beforeInvocation=true)
	public Result deleteLrdOrg(User user,LrdOrg lrdOrg) {
		// TODO Auto-generated method stub
		LrdOrg childLrdOrg = new LrdOrg();
		childLrdOrg.setCorpCd(lrdOrg.getCorpCd());
		childLrdOrg.setOrgPcd(lrdOrg.getOrgCd());
		childLrdOrg.setOrgRelaType(lrdOrg.getOrgRelaType());
		lrdOrgMapper.delete(childLrdOrg);
		lrdOrgMapper.delete(lrdOrg);
		Map<String,Object> map=new HashMap<String,Object>();
		map.put("orgRelaType", lrdOrg.getOrgRelaType());
		//asyncTaskServcie.executeAsyncTas(user, map);
		return ResultGenerator.genSuccessResult("删除成功");
	}

	
}
