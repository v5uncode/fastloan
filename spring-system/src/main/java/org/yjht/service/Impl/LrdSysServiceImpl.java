package org.yjht.service.Impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.yjht.bean.LrdSys;
import org.yjht.bean.vo.User;
import org.yjht.core.Constants;
import org.yjht.core.Result;
import org.yjht.dao.LrdSysMapper;
import org.yjht.service.LrdSysService;
import org.yjht.service.base.AbstractService;
import org.yjht.util.ResultGenerator;

import tk.mybatis.mapper.entity.Condition;
import tk.mybatis.mapper.entity.Example.Criteria;

@Service
public class LrdSysServiceImpl  extends AbstractService<LrdSys> implements LrdSysService {
	
	@Autowired
	LrdSysMapper lrdSysMapper;
	@Override
	public Result query(HttpServletRequest request) {
		// TODO Auto-generated method stub
		User user = (User) request.getSession().getAttribute("user");
		String corp_cd = user.getCorpCD();
		Condition condition = new Condition(LrdSys.class);
		Criteria criteria= condition.createCriteria();
		if(corp_cd.equals(Constants.SLS_CORP)){//市联社用户
			criteria.andEqualTo("corpCd",Constants.SLS_CORP);
		}
		List<LrdSys> listLrdSys= findByCondition(condition);
		return ResultGenerator.genSuccessResult(listLrdSys);
	}

}
