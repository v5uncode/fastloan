package org.yjht.service;

import java.util.ArrayList;
import java.util.Map;

import org.yjht.bean.LrdUser;


public interface LoginService {
	LrdUser findUserByName(String userName);
	
	public ArrayList<Map<String,Object>> queryRole(String userId);
}
