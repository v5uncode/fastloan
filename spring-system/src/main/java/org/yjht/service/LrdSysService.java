package org.yjht.service;

import javax.servlet.http.HttpServletRequest;

import org.yjht.core.Result;

public interface LrdSysService {

	Result query(HttpServletRequest request);

}
