package org.yjht.service;

import org.yjht.bean.LrdRolefunc;
import org.yjht.core.Result;

public interface LrdRoleFuncService{

	Result findLrdRoleFunc(LrdRolefunc lrdRolefunc);
	
}
