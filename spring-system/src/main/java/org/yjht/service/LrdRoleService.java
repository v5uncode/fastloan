package org.yjht.service;

import java.util.List;
import java.util.Map;
import org.yjht.bean.LrdRole;
import org.yjht.core.Result;
import org.yjht.tree.AuthorityMenuTree;

public interface LrdRoleService {
	
	public Result queryList(Map<String, Object> map);
	
	public Result InsertLrdRole(LrdRole lrdRole);
	public Result updateLrdRole(LrdRole lrdRole);
	
	public Result deleteRole(String roleCd);
	/**
     * 变更群组关联的菜单
     * @param roleCd
     * @param menuTrees
     */
	public void modifyAuthorityMenu(String roleCd, List<AuthorityMenuTree> menuTrees,String corpCd);
	/**
     * 分配资源权限
     * @param roleCd
     * @param menuId
     * @param elementId
     */
	public void modifyAuthorityElement(String roleCd,int menuId,int elementId,String corpCd);
	/**
     * 移除资源权限
     * @param roleCd
     * @param menuId
     * @param elementId
     */
	public void removeAuthorityElement(String roleCd, int menuId, int elementId);
	/**
     * 获取角色关联的菜单
     * @param groupId
     * @return
     */
	public List<AuthorityMenuTree> getAuthorityMenu(String groupId);
	
	public List<Integer> getAuthorityElement(String groupId);
	
	List<LrdRole> selectAll();
}
