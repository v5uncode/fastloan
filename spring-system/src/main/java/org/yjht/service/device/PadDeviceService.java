package org.yjht.service.device;


import org.yjht.bean.PadDevice;
import org.yjht.core.Result;
import org.yjht.service.base.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface PadDeviceService extends Service<PadDevice>{
    /**
     * 查询设备列表
     * @param device
     * @return
     */
    List<PadDevice> findList(PadDevice device);

    /**
     * 修改设备信息
     * @param device
     * @param request
     * @return
     */
    Result updateDevice(PadDevice device, HttpServletRequest request);

    /**
     * 新增设备
     * @param device
     * @param request
     * @return
     */
    Result saveDevice(PadDevice device, HttpServletRequest request);

    Result delete(String deviceId);
}
