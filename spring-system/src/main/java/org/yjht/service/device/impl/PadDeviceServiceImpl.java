package org.yjht.service.device.impl;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.yjht.bean.PadDevice;
import org.yjht.bean.vo.User;
import org.yjht.core.Result;
import org.yjht.service.base.AbstractService;
import org.yjht.service.device.PadDeviceService;
import org.yjht.util.DateTools;
import org.yjht.util.ResultGenerator;
import tk.mybatis.mapper.entity.Condition;
import tk.mybatis.mapper.entity.Example.Criteria;

import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.util.List;

@Service
public class PadDeviceServiceImpl extends AbstractService<PadDevice> implements PadDeviceService {



	//查询设备列表
	@Override
	public List<PadDevice> findList(PadDevice device) {
		Condition condition = new Condition(PadDevice.class);
		Criteria criteria=condition.createCriteria();
		if(StringUtils.isNotBlank(device.getDeviceId())){
			criteria.andLike("deviceId","%"+device.getDeviceId()+"%");
		}
		if(StringUtils.isNotBlank(device.getCreateUser())){
			criteria.andLike("createUser","%"+device.getCreateUser()+"%");
		}
		condition.orderBy("createDate").desc();
		List<PadDevice> list=findByCondition(condition);
		return list;
	}
	@Override
	public Result updateDevice(PadDevice device,HttpServletRequest request) {
		device.setUpdateDate(DateTools.parseDate((DateTools.getCurrentSysData(DateTools.FULLTIME_FORMAT)),DateTools.FULLTIME_FORMAT));		
		//获取当前登录用户id
		User user = (User) request.getSession().getAttribute("user");
		device.setUpdateUser(user.getUser_id());
		int n=update(device);
		if(n>0) {
			return ResultGenerator.genSuccessResult(device);
		}else {
			return ResultGenerator.genFailResult("更新失败");
		}
	}
	//新增设备
	@Override
	public Result saveDevice(PadDevice device,HttpServletRequest request) {
		PadDevice olddevice=findById(device.getDeviceId());
		if(olddevice==null) {			
			device.setCreateDate(DateTools.parseDate((DateTools.getCurrentSysData(DateTools.FULLTIME_FORMAT)),DateTools.FULLTIME_FORMAT));
			device.setUpdateDate(DateTools.parseDate((DateTools.getCurrentSysData(DateTools.FULLTIME_FORMAT)),DateTools.FULLTIME_FORMAT));		
			//获取当前登录用户id
			User user = (User) request.getSession().getAttribute("user");
			device.setCreateUser(user.getUser_id());
			device.setUpdateUser(user.getUser_id());
			int n=save(device);
			if(n>0) {
				return ResultGenerator.genSuccessResult(device);
			}else {
				return ResultGenerator.genFailResult("添加失败");
			}
		}else {
			return ResultGenerator.genFailResult("添加失败,设备号已存在");
		}
	}

	@Override
	public Result delete(String deviceId) {
		deleteById(deviceId);
		return ResultGenerator.genSuccessResult();
	}
}
