package org.yjht.service;

import java.util.Map;

import org.yjht.bean.SysLog;
import org.yjht.core.Page;
import org.yjht.core.Result;


public interface SysLogServcie {
	/**
	 * 
	 * @param sysLog
	 * @param page
	 * @return Result
	 */
	Result findSysLog(SysLog sysLog,Page page);
	/**
	 * 
	 * @param map
	 * @return Result
	 */
	Result findByPK(Map<String,Object> map);
	/**
	 * 
	 * @return Result
	 */
	Result deleteSysLog();
}
