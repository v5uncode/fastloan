package org.yjht.service;

import java.util.List;
import java.util.Map;
import org.yjht.bean.LrdOrg;
import org.yjht.bean.vo.User;
import org.yjht.core.Result;

public interface LrdOrgService {
	
	Map<String,Object> queryUserOrg(Map<String, Object> map);
	String getChilds(String orgCd);
	List<String> getChildList(String orgCd);
	Result queryMenu(User user, Map<String, Object> map);
	Result saveLrdOrg(User user,LrdOrg lrdOrg);
	Result updateLrdOrg(User user,LrdOrg lrdOrg);
	Result deleteLrdOrg(User user,LrdOrg lrdOrg);
	
}
