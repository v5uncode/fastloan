package org.yjht.service;

import org.yjht.bean.LrdFunction;
import org.yjht.core.Result;

public interface LrdFunctionService {
	
	public Result queryTree(String sysCd);
	public Result insertLrdFunction(LrdFunction lrdFunction);
	public Result queryInsert(String funcCd);
	public Result updateFunction(LrdFunction lrdFunction);
	public Result deleteFunction(LrdFunction lrdFunction);
}
