package org.yjht.thread;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.yjht.bean.vo.User;
import org.yjht.core.Result;
import org.yjht.service.LrdOrgService;
import org.yjht.service.SysDicService;

@Service
public class AsyncTaskServcie {
	
	@Autowired
	SysDicService SysDicService;
	@Autowired
	LrdOrgService LrdOrgService;
	@Async
	public Result executeAsyncTaskPlus(String parentId){		
		return SysDicService.finSysDicByParentId(parentId);
	}
	
	@Async
	public Result executeAsyncTas(User user, Map<String, Object> map){
		return LrdOrgService.queryMenu(user,map);
	}
}
