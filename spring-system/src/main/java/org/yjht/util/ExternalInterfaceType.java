package org.yjht.util;

/**
 * 对外数据接口枚举.
 */
public enum ExternalInterfaceType {

    /**
     * 信贷类型
     */
    XINDAI("信贷", 1);

    /**
     * 外部数据接口名称
     */
    private String name;

    /**
     * 外部数据索引
     */
    private int index;

    /**
     * 外部数据接口构造方法
     * 
     * @param name
     *            外部数据接口名称
     * @param index
     *            外部数据索引
     */
    private ExternalInterfaceType(String name, int index) {
        this.name = name;
        this.index = index;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
    
    public String print() {
        return "[" + this.name + ":" + this.index + "]";

    }

}
