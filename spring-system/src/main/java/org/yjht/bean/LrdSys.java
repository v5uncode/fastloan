package org.yjht.bean;

import javax.persistence.*;

@Table(name = "LRD_SYS")
public class LrdSys {
    @Id
    @Column(name = "SYS_CD")
    private String sysCd;

    @Column(name = "SYS_NAME")
    private String sysName;

    @Column(name = "SYS_DES")
    private String sysDes;

    @Column(name = "CRT_DATE")
    private String crtDate;

    @Column(name = "MTN_DATE")
    private String mtnDate;

    @Column(name = "CORP_CD")
    private String corpCd;

    /**
     * @return SYS_CD
     */
    public String getSysCd() {
        return sysCd;
    }

    /**
     * @param sysCd
     */
    public void setSysCd(String sysCd) {
        this.sysCd = sysCd == null ? null : sysCd.trim();
    }

    /**
     * @return SYS_NAME
     */
    public String getSysName() {
        return sysName;
    }

    /**
     * @param sysName
     */
    public void setSysName(String sysName) {
        this.sysName = sysName == null ? null : sysName.trim();
    }

    /**
     * @return SYS_DES
     */
    public String getSysDes() {
        return sysDes;
    }

    /**
     * @param sysDes
     */
    public void setSysDes(String sysDes) {
        this.sysDes = sysDes == null ? null : sysDes.trim();
    }

    /**
     * @return CRT_DATE
     */
    public String getCrtDate() {
        return crtDate;
    }

    /**
     * @param crtDate
     */
    public void setCrtDate(String crtDate) {
        this.crtDate = crtDate == null ? null : crtDate.trim();
    }

    /**
     * @return MTN_DATE
     */
    public String getMtnDate() {
        return mtnDate;
    }

    /**
     * @param mtnDate
     */
    public void setMtnDate(String mtnDate) {
        this.mtnDate = mtnDate == null ? null : mtnDate.trim();
    }

    /**
     * @return CORP_CD
     */
    public String getCorpCd() {
        return corpCd;
    }

    /**
     * @param corpCd
     */
    public void setCorpCd(String corpCd) {
        this.corpCd = corpCd == null ? null : corpCd.trim();
    }
}