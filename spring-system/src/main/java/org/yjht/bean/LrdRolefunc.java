package org.yjht.bean;

import javax.persistence.*;

@Table(name = "LRD_ROLEFUNC")
public class LrdRolefunc {
	
    @Column(name = "CORP_CD")
    private String corpCd;

    @Id
    @Column(name = "ROLE_CD")
    private String roleCd;

    @Id
    @Column(name = "FUNC_CD")
    private String funcCd;
    
    @Id
    @Column(name = "RESOURCE_TYPE")
    private String resourceType;
    
    public LrdRolefunc(String resourceType) {
        this.resourceType = resourceType;
    }
    public LrdRolefunc() {
    }
    /**
     * @return CORP_CD
     */
    public String getCorpCd() {
        return corpCd;
    }

    /**
     * @param corpCd
     */
    public void setCorpCd(String corpCd) {
        this.corpCd = corpCd == null ? null : corpCd.trim();
    }

    /**
     * @return ROLE_CD
     */
    public String getRoleCd() {
        return roleCd;
    }

    /**
     * @param roleCd
     */
    public void setRoleCd(String roleCd) {
        this.roleCd = roleCd == null ? null : roleCd.trim();
    }

    /**
     * @return FUNC_CD
     */
    public String getFuncCd() {
        return funcCd;
    }

    /**
     * @param funcCd
     */
    public void setFuncCd(String funcCd) {
        this.funcCd = funcCd == null ? null : funcCd.trim();
    }
    /**
     * 
     * @return resourceType
     */
	public String getResourceType() {
		return resourceType;
	}
	/**
	 * 
	 * @param resourceType
	 */
	public void setResourceType(String resourceType) {
		this.resourceType = resourceType == null ? null : resourceType.trim();;
	}
    
    
}