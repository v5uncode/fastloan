package org.yjht.bean;

import javax.persistence.*;

@Table(name = "LRD_ROLE")
public class LrdRole {
    @Id
    @Column(name = "ROLE_CD")
    private String roleCd;

    @Column(name = "ROLE_NAME")
    private String roleName;

    @Column(name = "ROLE_FW")
    private String roleFw;

    @Column(name = "ROLE_ORG")
    private String roleOrg;

    @Column(name = "ROLE_DESC")
    private String desc;

    @Column(name = "CRT_DATE")
    private String crtDate;

    @Column(name = "MTN_DATE")
    private String mtnDate;

    /**
     * @return ROLE_CD
     */
    public String getRoleCd() {
        return roleCd;
    }

    /**
     * @param roleCd
     */
    public void setRoleCd(String roleCd) {
        this.roleCd = roleCd == null ? null : roleCd.trim();
    }

    /**
     * @return ROLE_NAME
     */
    public String getRoleName() {
        return roleName;
    }

    /**
     * @param roleName
     */
    public void setRoleName(String roleName) {
        this.roleName = roleName == null ? null : roleName.trim();
    }

    /**
     * @return ROLE_FW
     */
    public String getRoleFw() {
        return roleFw;
    }

    /**
     * @param roleFw
     */
    public void setRoleFw(String roleFw) {
        this.roleFw = roleFw == null ? null : roleFw.trim();
    }

    /**
     * @return ROLE_ORG
     */
    public String getRoleOrg() {
        return roleOrg;
    }

    /**
     * @param roleOrg
     */
    public void setRoleOrg(String roleOrg) {
        this.roleOrg = roleOrg == null ? null : roleOrg.trim();
    }

    /**
     * @return DESC
     */
    public String getDesc() {
        return desc;
    }

    /**
     * @param desc
     */
    public void setDesc(String desc) {
        this.desc = desc == null ? null : desc.trim();
    }

    /**
     * @return CRT_DATE
     */
    public String getCrtDate() {
        return crtDate;
    }

    /**
     * @param crtDate
     */
    public void setCrtDate(String crtDate) {
        this.crtDate = crtDate == null ? null : crtDate.trim();
    }

    /**
     * @return MTN_DATE
     */
    public String getMtnDate() {
        return mtnDate;
    }

    /**
     * @param mtnDate
     */
    public void setMtnDate(String mtnDate) {
        this.mtnDate = mtnDate == null ? null : mtnDate.trim();
    }

	@Override
	public String toString() {
		return "LrdRole [roleCd=" + roleCd + ", roleName=" + roleName + ", roleFw=" + roleFw + ", roleOrg=" + roleOrg
				+ ", desc=" + desc + ", crtDate=" + crtDate + ", mtnDate=" + mtnDate + "]";
	}
    
}