package org.yjht.bean;

import java.util.List;

import javax.persistence.*;

import lombok.Getter;
import lombok.Setter;

@Table(name = "LRD_USER")
@Getter
@Setter
public class LrdUser {
    @Id
    @Column(name = "USER_ID")
    private String userId;

    @Column(name = "CORP_CD")
    private String corpCd;

    @Column(name = "ORG_CD")
    private String orgCd;

    @Column(name = "DEPT_CD")
    private String deptCd;

    @Column(name = "USER_NAME")
    private String userName;

    @Column(name = "PASSWORD")
    private String password;

    @Column(name = "SEX")
    private String sex;

    @Column(name = "TEL_NO")
    private String telNo;

    @Column(name = "EMAIL")
    private String email;

    @Column(name = "CRT_DATE")
    private String crtDate;

    @Column(name = "MTN_DATE")
    private String mtnDate;

    @Column(name = "JL_FLAG")
    private String jlFlag;

    @Column(name = "ID_NO")
    private String idNo;

    @Column(name = "XD_PASS")
    private String xdPass;

    @Column(name = "MANAGERID")
    private String managerid;
    
    @Transient 
    private List<String> roleCds;

}