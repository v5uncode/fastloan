package org.yjht.bean;

import javax.persistence.*;

@Table(name = "LRD_USERROLE")
public class LrdUserrole {
    @Id
    @Column(name = "CORP_CD")
    private String corpCd;

    @Id
    @Column(name = "USER_ID")
    private String userId;

    @Id
    @Column(name = "ROLE_ID")
    private String roleId;

    /**
     * @return CORP_CD
     */
    public String getCorpCd() {
        return corpCd;
    }

    /**
     * @param corpCd
     */
    public void setCorpCd(String corpCd) {
        this.corpCd = corpCd == null ? null : corpCd.trim();
    }

    /**
     * @return USER_ID
     */
    public String getUserId() {
        return userId;
    }

    /**
     * @param userId
     */
    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    /**
     * @return ROLE_ID
     */
    public String getRoleId() {
        return roleId;
    }

    /**
     * @param roleId
     */
    public void setRoleId(String roleId) {
        this.roleId = roleId == null ? null : roleId.trim();
    }
}