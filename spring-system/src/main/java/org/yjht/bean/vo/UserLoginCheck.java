package org.yjht.bean.vo;

import java.util.Date;

public class UserLoginCheck extends User {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// 登录失败次数(默认为0)
	private int loginFailCount = 0;

	// 最后失败登陆时间
	private Date lastFailDatetime;

	public int getLoginFailCount() {
		return loginFailCount;
	}

	public void setLoginFailCount(int loginFailCount) {
		this.loginFailCount = loginFailCount;
	}

	public Date getLastFailDatetime() {
		return lastFailDatetime;
	}

	public void setLastFailDatetime(Date lastFailDatetime) {
		this.lastFailDatetime = lastFailDatetime;
	}

	@Override
	public String toString() {
		return "UserLoginCheck [loginFailCount=" + loginFailCount + ", lastFailDatetime=" + lastFailDatetime + "]";
	}

}
