package org.yjht.bean;

import javax.persistence.*;

@Table(name = "SYS_PARA")
public class SysPara {
    @Id
    @Column(name = "CORP_CD")
    private String corpCd;

    @Id
    @Column(name = "CHPARAKEY")
    private String chparakey;

    @Column(name = "CHPARAVALUE")
    private String chparavalue;

    @Column(name = "CHPARADESC")
    private String chparadesc;

    @Column(name = "CHPARAVISIBLE")
    private String chparavisible;

    @Column(name = "CHPARAMAINTAIN")
    private String chparamaintain;

    @Column(name = "CHPARALOCALE")
    private String chparalocale;

    @Column(name = "MTN_DATE")
    private String mtnDate;

    /**
     * @return CORP_CD
     */
    public String getCorpCd() {
        return corpCd;
    }

    /**
     * @param corpCd
     */
    public void setCorpCd(String corpCd) {
        this.corpCd = corpCd == null ? null : corpCd.trim();
    }

    /**
     * @return CHPARAKEY
     */
    public String getChparakey() {
        return chparakey;
    }

    /**
     * @param chparakey
     */
    public void setChparakey(String chparakey) {
        this.chparakey = chparakey == null ? null : chparakey.trim();
    }

    /**
     * @return CHPARAVALUE
     */
    public String getChparavalue() {
        return chparavalue;
    }

    /**
     * @param chparavalue
     */
    public void setChparavalue(String chparavalue) {
        this.chparavalue = chparavalue == null ? null : chparavalue.trim();
    }

    /**
     * @return CHPARADESC
     */
    public String getChparadesc() {
        return chparadesc;
    }

    /**
     * @param chparadesc
     */
    public void setChparadesc(String chparadesc) {
        this.chparadesc = chparadesc == null ? null : chparadesc.trim();
    }

    /**
     * @return CHPARAVISIBLE
     */
    public String getChparavisible() {
        return chparavisible;
    }

    /**
     * @param chparavisible
     */
    public void setChparavisible(String chparavisible) {
        this.chparavisible = chparavisible == null ? null : chparavisible.trim();
    }

    /**
     * @return CHPARAMAINTAIN
     */
    public String getChparamaintain() {
        return chparamaintain;
    }

    /**
     * @param chparamaintain
     */
    public void setChparamaintain(String chparamaintain) {
        this.chparamaintain = chparamaintain == null ? null : chparamaintain.trim();
    }

    /**
     * @return CHPARALOCALE
     */
    public String getChparalocale() {
        return chparalocale;
    }

    /**
     * @param chparalocale
     */
    public void setChparalocale(String chparalocale) {
        this.chparalocale = chparalocale == null ? null : chparalocale.trim();
    }

    /**
     * @return MTN_DATE
     */
    public String getMtnDate() {
        return mtnDate;
    }

    /**
     * @param mtnDate
     */
    public void setMtnDate(String mtnDate) {
        this.mtnDate = mtnDate == null ? null : mtnDate.trim();
    }
}