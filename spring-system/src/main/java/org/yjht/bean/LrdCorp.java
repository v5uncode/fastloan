package org.yjht.bean;

import javax.persistence.*;

@Table(name = "lrd_corp")
public class LrdCorp {
    /**
     * 法人号
     */
    @Id
    @Column(name = "CORP_CD")
    private String corpCd;

    /**
     * 法人名称
     */
    @Column(name = "CORP_NAME")
    private String corpName;

    /**
     * 描述
     */
    @Column(name = "CORP_DESC")
    private String corpDesc;

    /**
     * 状态
     */
    @Column(name = "CORP_FLAG")
    private String corpFlag;

    /**
     * 数据创建日期
     */
    @Column(name = "CRT_DATE")
    private String crtDate;

    /**
     * 数据修改日期
     */
    @Column(name = "MTN_DATE")
    private String mtnDate;

    /**
     * logo名称
     */
    @Column(name = "CORP_BELONG")
    private String corpBelong;

    /**
     * 获取法人号
     *
     * @return CORP_CD - 法人号
     */
    public String getCorpCd() {
        return corpCd;
    }

    /**
     * 设置法人号
     *
     * @param corpCd 法人号
     */
    public void setCorpCd(String corpCd) {
        this.corpCd = corpCd == null ? null : corpCd.trim();
    }

    /**
     * 获取法人名称
     *
     * @return CORP_NAME - 法人名称
     */
    public String getCorpName() {
        return corpName;
    }

    /**
     * 设置法人名称
     *
     * @param corpName 法人名称
     */
    public void setCorpName(String corpName) {
        this.corpName = corpName == null ? null : corpName.trim();
    }

    /**
     * 获取描述
     *
     * @return CORP_DESC - 描述
     */
    public String getCorpDesc() {
        return corpDesc;
    }

    /**
     * 设置描述
     *
     * @param corpDesc 描述
     */
    public void setCorpDesc(String corpDesc) {
        this.corpDesc = corpDesc == null ? null : corpDesc.trim();
    }

    /**
     * 获取状态
     *
     * @return CORP_FLAG - 状态
     */
    public String getCorpFlag() {
        return corpFlag;
    }

    /**
     * 设置状态
     *
     * @param corpFlag 状态
     */
    public void setCorpFlag(String corpFlag) {
        this.corpFlag = corpFlag == null ? null : corpFlag.trim();
    }

    /**
     * 获取数据创建日期
     *
     * @return CRT_DATE - 数据创建日期
     */
    public String getCrtDate() {
        return crtDate;
    }

    /**
     * 设置数据创建日期
     *
     * @param crtDate 数据创建日期
     */
    public void setCrtDate(String crtDate) {
        this.crtDate = crtDate == null ? null : crtDate.trim();
    }

    /**
     * 获取数据修改日期
     *
     * @return MTN_DATE - 数据修改日期
     */
    public String getMtnDate() {
        return mtnDate;
    }

    /**
     * 设置数据修改日期
     *
     * @param mtnDate 数据修改日期
     */
    public void setMtnDate(String mtnDate) {
        this.mtnDate = mtnDate == null ? null : mtnDate.trim();
    }

    /**
     * 获取logo名称
     *
     * @return CORP_BELONG - logo名称
     */
    public String getCorpBelong() {
        return corpBelong;
    }

    /**
     * 设置logo名称
     *
     * @param corpBelong logo名称
     */
    public void setCorpBelong(String corpBelong) {
        this.corpBelong = corpBelong == null ? null : corpBelong.trim();
    }
}