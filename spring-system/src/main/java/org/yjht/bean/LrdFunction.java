package org.yjht.bean;

import java.math.BigDecimal;
import javax.persistence.*;

@Table(name = "LRD_FUNCTION")
public class LrdFunction {
    @Id
    @Column(name = "FUNC_CD")
    private String funcCd;

    @Column(name = "SYS_CD")
    private String sysCd;

    @Column(name = "FUNC_NAME")
    private String funcName;

    @Column(name = "FUNC_TYPE")
    private String funcType;

    @Column(name = "FUNC_PCD")
    private String funcPcd;

    @Column(name = "FUNC_URL")
    private String funcUrl;

    @Column(name = "FUNC_NUM")
    private BigDecimal funcNum;

    @Column(name = "CRT_DATE")
    private String crtDate;

    @Column(name = "MTN_DATE")
    private String mtnDate;

    /**
     * @return FUNC_CD
     */
    public String getFuncCd() {
        return funcCd;
    }

    /**
     * @param funcCd
     */
    public void setFuncCd(String funcCd) {
        this.funcCd = funcCd == null ? null : funcCd.trim();
    }

    /**
     * @return SYS_CD
     */
    public String getSysCd() {
        return sysCd;
    }

    /**
     * @param sysCd
     */
    public void setSysCd(String sysCd) {
        this.sysCd = sysCd == null ? null : sysCd.trim();
    }

    /**
     * @return FUNC_NAME
     */
    public String getFuncName() {
        return funcName;
    }

    /**
     * @param funcName
     */
    public void setFuncName(String funcName) {
        this.funcName = funcName == null ? null : funcName.trim();
    }

    /**
     * @return FUNC_TYPE
     */
    public String getFuncType() {
        return funcType;
    }

    /**
     * @param funcType
     */
    public void setFuncType(String funcType) {
        this.funcType = funcType == null ? null : funcType.trim();
    }

    /**
     * @return FUNC_PCD
     */
    public String getFuncPcd() {
        return funcPcd;
    }

    /**
     * @param funcPcd
     */
    public void setFuncPcd(String funcPcd) {
        this.funcPcd = funcPcd == null ? null : funcPcd.trim();
    }

    /**
     * @return FUNC_URL
     */
    public String getFuncUrl() {
        return funcUrl;
    }

    /**
     * @param funcUrl
     */
    public void setFuncUrl(String funcUrl) {
        this.funcUrl = funcUrl == null ? null : funcUrl.trim();
    }

    /**
     * @return FUNC_NUM
     */
    public BigDecimal getFuncNum() {
        return funcNum;
    }

    /**
     * @param funcNum
     */
    public void setFuncNum(BigDecimal funcNum) {
        this.funcNum = funcNum;
    }

    /**
     * @return CRT_DATE
     */
    public String getCrtDate() {
        return crtDate;
    }

    /**
     * @param crtDate
     */
    public void setCrtDate(String crtDate) {
        this.crtDate = crtDate == null ? null : crtDate.trim();
    }

    /**
     * @return MTN_DATE
     */
    public String getMtnDate() {
        return mtnDate;
    }

    /**
     * @param mtnDate
     */
    public void setMtnDate(String mtnDate) {
        this.mtnDate = mtnDate == null ? null : mtnDate.trim();
    }
}