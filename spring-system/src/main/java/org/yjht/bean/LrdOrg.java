package org.yjht.bean;

import javax.persistence.*;

@Table(name = "LRD_ORG")
public class LrdOrg {
    @Id
    @Column(name = "CORP_CD")
    private String corpCd;

    @Id
    @Column(name = "ORG_CD")
    private String orgCd;

    @Id
    @Column(name = "ORG_RELA_TYPE")
    private String orgRelaType;

    @Column(name = "ORG_NAME")
    private String orgName;

    @Column(name = "ORG_PCD")
    private String orgPcd;

    @Column(name = "ORG_FLAG")
    private String orgFlag;

    @Column(name = "ORG_VFLAG")
    private String orgVflag;

    @Column(name = "ORG_DESC")
    private String orgDesc;

    @Column(name = "LEVELID")
    private String levelid;

    @Column(name = "CRT_DATE")
    private String crtDate;

    @Column(name = "MTN_DATE")
    private String mtnDate;

    @Column(name = "ORG_ABB")
    private String orgAbb;

    @Column(name = "WD_ID")
    private String wdId;

    @Column(name = "ORG_TYPE")
    private String orgType;

    /**
     * @return CORP_CD
     */
    public String getCorpCd() {
        return corpCd;
    }

    /**
     * @param corpCd
     */
    public void setCorpCd(String corpCd) {
        this.corpCd = corpCd == null ? null : corpCd.trim();
    }

    /**
     * @return ORG_CD
     */
    public String getOrgCd() {
        return orgCd;
    }

    /**
     * @param orgCd
     */
    public void setOrgCd(String orgCd) {
        this.orgCd = orgCd == null ? null : orgCd.trim();
    }

    /**
     * @return ORG_RELA_TYPE
     */
    public String getOrgRelaType() {
        return orgRelaType;
    }

    /**
     * @param orgRelaType
     */
    public void setOrgRelaType(String orgRelaType) {
        this.orgRelaType = orgRelaType == null ? null : orgRelaType.trim();
    }

    /**
     * @return ORG_NAME
     */
    public String getOrgName() {
        return orgName;
    }

    /**
     * @param orgName
     */
    public void setOrgName(String orgName) {
        this.orgName = orgName == null ? null : orgName.trim();
    }

    /**
     * @return ORG_PCD
     */
    public String getOrgPcd() {
        return orgPcd;
    }

    /**
     * @param orgPcd
     */
    public void setOrgPcd(String orgPcd) {
        this.orgPcd = orgPcd == null ? null : orgPcd.trim();
    }

    /**
     * @return ORG_FLAG
     */
    public String getOrgFlag() {
        return orgFlag;
    }

    /**
     * @param orgFlag
     */
    public void setOrgFlag(String orgFlag) {
        this.orgFlag = orgFlag == null ? null : orgFlag.trim();
    }

    /**
     * @return ORG_VFLAG
     */
    public String getOrgVflag() {
        return orgVflag;
    }

    /**
     * @param orgVflag
     */
    public void setOrgVflag(String orgVflag) {
        this.orgVflag = orgVflag == null ? null : orgVflag.trim();
    }

    /**
     * @return ORG_DESC
     */
    public String getOrgDesc() {
        return orgDesc;
    }

    /**
     * @param orgDesc
     */
    public void setOrgDesc(String orgDesc) {
        this.orgDesc = orgDesc == null ? null : orgDesc.trim();
    }

    /**
     * @return LEVELID
     */
    public String getLevelid() {
        return levelid;
    }

    /**
     * @param levelid
     */
    public void setLevelid(String levelid) {
        this.levelid = levelid == null ? null : levelid.trim();
    }

    /**
     * @return CRT_DATE
     */
    public String getCrtDate() {
        return crtDate;
    }

    /**
     * @param crtDate
     */
    public void setCrtDate(String crtDate) {
        this.crtDate = crtDate == null ? null : crtDate.trim();
    }

    /**
     * @return MTN_DATE
     */
    public String getMtnDate() {
        return mtnDate;
    }

    /**
     * @param mtnDate
     */
    public void setMtnDate(String mtnDate) {
        this.mtnDate = mtnDate == null ? null : mtnDate.trim();
    }

    /**
     * @return ORG_ABB
     */
    public String getOrgAbb() {
        return orgAbb;
    }

    /**
     * @param orgAbb
     */
    public void setOrgAbb(String orgAbb) {
        this.orgAbb = orgAbb == null ? null : orgAbb.trim();
    }

    /**
     * @return WD_ID
     */
    public String getWdId() {
        return wdId;
    }

    /**
     * @param wdId
     */
    public void setWdId(String wdId) {
        this.wdId = wdId == null ? null : wdId.trim();
    }

    /**
     * @return ORG_TYPE
     */
    public String getOrgType() {
        return orgType;
    }

    /**
     * @param orgType
     */
    public void setOrgType(String orgType) {
        this.orgType = orgType == null ? null : orgType.trim();
    }
}