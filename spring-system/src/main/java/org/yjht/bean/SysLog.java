package org.yjht.bean;

import javax.persistence.*;

@Table(name = "SYS_LOG")
public class SysLog {
    @Column(name = "CORP_CD")
    private String corpCd;

    @Column(name = "ORG_CD")
    private String orgCd;

    @Column(name = "USER_ID")
    private String userId;

    @Column(name = "OPER_DATE")
    private String operDate;

    @Column(name = "OPER_NAME")
    private String operName;

    @Column(name = "OPER_DX")
    private String operDx;

    @Column(name = "OPER_IG")
    private String operIg;

    @Column(name = "CRT_DATE")
    private String crtDate;

    @Column(name = "MTN_DATE")
    private String mtnDate;

    @Column(name = "OPER_PARA")
    private String operPara;

    /**
     * @return CORP_CD
     */
    public String getCorpCd() {
        return corpCd;
    }

    /**
     * @param corpCd
     */
    public void setCorpCd(String corpCd) {
        this.corpCd = corpCd == null ? null : corpCd.trim();
    }

    /**
     * @return ORG_CD
     */
    public String getOrgCd() {
        return orgCd;
    }

    /**
     * @param orgCd
     */
    public void setOrgCd(String orgCd) {
        this.orgCd = orgCd == null ? null : orgCd.trim();
    }

    /**
     * @return USER_ID
     */
    public String getUserId() {
        return userId;
    }

    /**
     * @param userId
     */
    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    /**
     * @return OPER_DATE
     */
    public String getOperDate() {
        return operDate;
    }

    /**
     * @param operDate
     */
    public void setOperDate(String operDate) {
        this.operDate = operDate == null ? null : operDate.trim();
    }

    /**
     * @return OPER_NAME
     */
    public String getOperName() {
        return operName;
    }

    /**
     * @param operName
     */
    public void setOperName(String operName) {
        this.operName = operName == null ? null : operName.trim();
    }

    /**
     * @return OPER_DX
     */
    public String getOperDx() {
        return operDx;
    }

    /**
     * @param operDx
     */
    public void setOperDx(String operDx) {
        this.operDx = operDx == null ? null : operDx.trim();
    }

    /**
     * @return OPER_IG
     */
    public String getOperIg() {
        return operIg;
    }

    /**
     * @param operIg
     */
    public void setOperIg(String operIg) {
        this.operIg = operIg == null ? null : operIg.trim();
    }

    /**
     * @return CRT_DATE
     */
    public String getCrtDate() {
        return crtDate;
    }

    /**
     * @param crtDate
     */
    public void setCrtDate(String crtDate) {
        this.crtDate = crtDate == null ? null : crtDate.trim();
    }

    /**
     * @return MTN_DATE
     */
    public String getMtnDate() {
        return mtnDate;
    }

    /**
     * @param mtnDate
     */
    public void setMtnDate(String mtnDate) {
        this.mtnDate = mtnDate == null ? null : mtnDate.trim();
    }

    /**
     * @return OPER_PARA
     */
    public String getOperPara() {
        return operPara;
    }

    /**
     * @param operPara
     */
    public void setOperPara(String operPara) {
        this.operPara = operPara == null ? null : operPara.trim();
    }
}