package org.yjht.wechat.bean;

import org.yjht.bean.rest.WxBaseBean;

public class UserInfo extends WxBaseBean{
	
	//private String managerid;//柜员号
	
	private String idNo;//身份证号
	
	private String userName;//用户姓名
	
	 private String crtDate;//创建时间
	
	 private String mtnDate;//修改时间


	public String getIdNo() {
		return idNo;
	}

	public void setIdNo(String idNo) {
		this.idNo = idNo;
	}
	
	
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getCrtDate() {
		return crtDate;
	}

	public void setCrtDate(String crtDate) {
		this.crtDate = crtDate;
	}

	public String getMtnDate() {
		return mtnDate;
	}

	public void setMtnDate(String mtnDate) {
		this.mtnDate = mtnDate;
	}
	 
	 
}
