package org.yjht.dao;

import org.apache.ibatis.annotations.Param;
import org.yjht.bean.LrdRolefunc;
import org.yjht.common.MyMapper;

public interface LrdRolefuncMapper extends MyMapper<LrdRolefunc> {
	public void deleteByRoleCdAndResourceType(@Param("roleCd")String roleCd,@Param("resourceType") String resourceType);
}