package org.yjht.dao;

import java.util.List;
import java.util.Map;

import org.yjht.bean.LrdFunction;
import org.yjht.common.MyMapper;

public interface LrdFunctionMapper extends MyMapper<LrdFunction> {
	
	List<Map<String, Object>> queryTree(Map<String,Object> sysCd);
	
}