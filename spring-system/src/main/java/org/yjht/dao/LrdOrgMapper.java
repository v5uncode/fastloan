package org.yjht.dao;

import java.util.List;
import java.util.Map;

import org.yjht.bean.LrdOrg;
import org.yjht.common.MyMapper;

public interface LrdOrgMapper extends MyMapper<LrdOrg> {
	
	List<Map<String,Object>> findMenu(Map<String,Object> map);
	
	Map<String,Object> queryUserOrg(Map<String, Object> map);

	List<Map<String, Object>> getChilds(String orgCd);
	
	List<String> getChildList(String orgCd);

}