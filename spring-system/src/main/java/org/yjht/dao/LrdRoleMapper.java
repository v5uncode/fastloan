package org.yjht.dao;

import java.util.List;
import java.util.Map;

import org.yjht.bean.LrdRole;
import org.yjht.common.MyMapper;

public interface LrdRoleMapper extends MyMapper<LrdRole> {
	
	List<Map<String,Object>> queryLrdRole(LrdRole lrdRole);
	List<Map<String, Object>> queryFunc(Map<String, Object> map);
	
	List<Map<String, Object>> queryUserRole(Map<String, Object> map);
	List<Map<String, Object>> queryList(Map<String, Object> map);
	
}