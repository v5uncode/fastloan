package org.yjht.dao;

import org.apache.ibatis.annotations.Param;
import org.yjht.bean.Menu;
import org.yjht.common.MyMapper;
import java.util.List;

public interface MenuMapper extends MyMapper<Menu> {
    public List<Menu> selectMenuByAuthorityId(@Param("authorityId") String authorityId);

    /**
     * 根据用户和组的权限关系查找用户可访问菜单
     * @param userId
     * @return
     */
    public List<Menu> selectAuthorityMenuByUserId (@Param("userId") String userId);

    /**
     * 根据用户和组的权限关系查找用户可访问的系统
     * @param userId
     * @return
     */
    public List<Menu> selectAuthoritySystemByUserId (@Param("userId") String userId);
}