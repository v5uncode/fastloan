package org.yjht.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.yjht.bean.LrdUser;
import org.yjht.common.MyMapper;

public interface LrdUserMapper extends MyMapper<LrdUser>{

	List<Map<String, Object>> queryUserRole(Map<String, Object> map);

	List<Map<String, Object>> queryList(Map<String, Object> map);
	
	ArrayList<Map<String, Object>> queryRole(String userId);

	/**
	 * 根据法人号、机构好、角色号查找用户信息
	 * @param corpCd 法人号
	 * @param orgCd  机构号
	 * @param roleId 角色编号
	 * @return
	 */
	List<LrdUser> findBranchManagerByOrgCd(@Param("corpCd") String corpCd, @Param("orgCd") String orgCd, @Param("roleId")String roleId);
}