package org.yjht.dao;

import java.util.List;
import java.util.Map;

import org.yjht.bean.SysLog;
import org.yjht.common.MyMapper;

public interface SysLogMapper extends MyMapper<SysLog> {
	
	List<Map<String,Object>> findSysLog(SysLog sysLog);

	void deleteAll();
}