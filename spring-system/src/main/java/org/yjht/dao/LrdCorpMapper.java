package org.yjht.dao;

import org.yjht.bean.LrdCorp;
import org.yjht.common.MyMapper;

public interface LrdCorpMapper extends MyMapper<LrdCorp> {
}