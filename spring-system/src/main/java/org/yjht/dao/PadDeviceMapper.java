package org.yjht.dao;

import org.yjht.bean.PadDevice;
import org.yjht.common.MyMapper;

public interface PadDeviceMapper extends MyMapper<PadDevice> {
}