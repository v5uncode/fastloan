package org.yjht.dao;

import org.apache.ibatis.annotations.Param;
import org.yjht.bean.Element;
import org.yjht.common.MyMapper;
import java.util.List;

public interface ElementMapper extends MyMapper<Element> {
    public List<Element> selectAuthorityElementByUserId(@Param("userId")String userId);
    public List<Element> selectAuthorityMenuElementByUserId(@Param("userId")String userId,@Param("menuId")String menuId);
    public List<Element> selectAuthorityElementByClientId(@Param("clientId")String clientId);

}