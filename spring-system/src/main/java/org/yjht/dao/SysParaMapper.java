package org.yjht.dao;

import org.yjht.bean.SysPara;
import org.yjht.common.MyMapper;

public interface SysParaMapper extends MyMapper<SysPara> {
}