package org.yjht.schedule;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.yjht.service.LrdLogService;
import org.yjht.util.DateTools;

@Component
public class LogSchedule {

	@Autowired
	LrdLogService lrdLogService;
	
	@Scheduled(cron= "0 59 23 * * ?")
	public void scheduler() {
		System.out.println("定时任务启动Time:"+DateTools.getCurrentSysData(DateTools.FULLTIME_FORMAT));
		lrdLogService.deleteSysLogByTime();
	}
}
