package org.yjht.schedule;

import lombok.Cleanup;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.yjht.util.ftp.Ftp;
import org.yjht.util.ftp.FtpFactory;

import java.io.IOException;

/**
 * @author lengleng
 * @date 2018/5/17
 * 同步兴业FTP数据
 */
@Slf4j
@Component
public class SyncDataSchedule {
    /**
     * 每天1点执行
     */
    @Scheduled(cron = "0 0 1 * * ?")
    public void scheduler() {
        log.info("开始同步兴业数据");
        Ftp ftp = FtpFactory.createFtpClient();
        try {
            ftp.connect("192.168.0.16", 21, "frpuser", "frpuser");
            ftp.asc();
            ftp.cd("/home/ftp");
            ftp.download("love", "/Users/lengleng/work/temp/text.txt");
        } catch (IOException e) {
            log.error("FTP操作失败", e);
        } finally {
            if (ftp != null) {
                try {
                    ftp.close();
                } catch (IOException e) {
                    log.error("FTP关闭失败", e);
                }
            }
        }
    }
}
