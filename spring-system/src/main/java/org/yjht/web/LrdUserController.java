package org.yjht.web;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.yjht.aop.SystemControllerLog;
import org.yjht.bean.LrdUser;
import org.yjht.core.Result;
import org.yjht.service.LrdUserService;

@RestController
@RequestMapping("/lrduser")
public class LrdUserController {
	
	Logger log = Logger.getLogger(LrdUserController.class);
	@Autowired
	private LrdUserService lrdUserService;
	
	@RequestMapping(value = "/user", produces = "application/json;charset=UTF-8", method = { RequestMethod.GET })
	@SystemControllerLog(description = "查询用户")
	public Result queryTree(HttpServletRequest request,@RequestParam Map<String, Object> map){
		log.info("接收的JSON:" + map.toString());
		return lrdUserService.queryTree(request,map);
		
	}
	@RequestMapping(value = "/user/orgTree", produces = "application/json;charset=UTF-8", method = { RequestMethod.GET })
	@SystemControllerLog(description = "查询机构树")
	public Result queryOrgTree(HttpServletRequest request){
		return lrdUserService.queryOrgTree(request);
		
	}
	@RequestMapping(value = "/user/roleList", produces = "application/json;charset=UTF-8", method = { RequestMethod.GET })
	@SystemControllerLog(description = "查询角色列表")
	public Result queryRoleList(HttpServletRequest request){
		return lrdUserService.queryRoleList(request);
		
	}
	@RequestMapping(value = "/user/resetPwd", produces = "application/json;charset=UTF-8", method = { RequestMethod.POST })
	@SystemControllerLog(description = "重置密码")
	public Result resetPwd(@RequestBody LrdUser lrdUser, HttpServletRequest request){
		return lrdUserService.resetPwd(lrdUser,request);
	}
	
	@RequestMapping(value = "/user/queryInsert", produces = "application/json;charset=UTF-8", method = { RequestMethod.GET })
	@SystemControllerLog(description = "添加查询")
	public Result queryInsert(HttpServletRequest request){
		return lrdUserService.queryInsert(request);
	}
	
	@RequestMapping(value = "/user", produces = "application/json;charset=UTF-8", method = { RequestMethod.POST })
	@SystemControllerLog(description = "添加操作员")
	public Result insertUser(@RequestBody LrdUser user) {
		
		return lrdUserService.insertUser(user);
	}
	
	@RequestMapping(value = "/user", produces = "application/json;charset=UTF-8", method = { RequestMethod.PUT })
	@SystemControllerLog(description = "修改操作员")
	public Result updateUser(@RequestBody LrdUser user) {
		
		return lrdUserService.updateUser(user);
	}
	
	@RequestMapping(value = "/user/{userId}", produces = "application/json;charset=UTF-8", method = { RequestMethod.DELETE })
	@SystemControllerLog(description = "删除操作员")
	public Result deleteUser(@PathVariable String userId){
		
		return  lrdUserService.deleteUserAndRole(userId);
	}
}
