package org.yjht.web;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.yjht.aop.SystemControllerLog;
import org.yjht.bean.LrdOrg;
import org.yjht.bean.vo.User;
import org.yjht.core.Result;
import org.yjht.service.LrdOrgService;

@RestController
@RequestMapping("/lrdOrg")
public class LrdOrgCotroller {
	
	@Autowired
	LrdOrgService lrdOrgService;
	
	@RequestMapping(value = "/lrdOrg", produces = "application/json;charset=UTF-8", method = { RequestMethod.GET })
	@SystemControllerLog(description = "机构查询")
	public Result queryMenu(HttpServletRequest request,@RequestParam Map<String, Object> map) {
		User user = (User) request.getSession().getAttribute("user");	
		return lrdOrgService.queryMenu(user,map);
	}

	@RequestMapping(value = "/lrdOrg", produces = "application/json;charset=UTF-8", method = { RequestMethod.POST })
	@SystemControllerLog(description = "机构添加")
	public Result saveLrdOrg(HttpServletRequest request,@RequestBody LrdOrg lrdOrg) {
		User user = (User) request.getSession().getAttribute("user");
		return lrdOrgService.saveLrdOrg(user,lrdOrg);
	}
	
	@RequestMapping(value = "/lrdOrg", produces = "application/json;charset=UTF-8", method = { RequestMethod.PUT })
	@SystemControllerLog(description = "机构--修改")
	public Result updateLrdOrg(HttpServletRequest request,@RequestBody LrdOrg lrdOrg) {
		User user = (User) request.getSession().getAttribute("user");
		return lrdOrgService.updateLrdOrg(user,lrdOrg);
	}
	@RequestMapping(value = "/lrdOrg", produces = "application/json;charset=UTF-8", method = { RequestMethod.DELETE })
	@SystemControllerLog(description = "机构删除")
	public Result deleteLrdOrg(HttpServletRequest request,@RequestBody LrdOrg lrdOrg) {
		User user = (User) request.getSession().getAttribute("user");
		return lrdOrgService.deleteLrdOrg(user,lrdOrg);
	}
}
