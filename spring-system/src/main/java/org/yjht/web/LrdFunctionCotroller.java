package org.yjht.web;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.yjht.aop.SystemControllerLog;
import org.yjht.bean.LrdFunction;
import org.yjht.core.Result;
import org.yjht.service.LrdFunctionService;

@RestController
@RequestMapping("/lrdfunc")
public class LrdFunctionCotroller {
	Logger log = Logger.getLogger(LrdFunctionCotroller.class);
	@Autowired
	private LrdFunctionService lrdFunctionService;
	
	@RequestMapping(value = "/func/funcCd/{funcCd}", produces = "application/json;charset=UTF-8", method = { RequestMethod.GET })
	@SystemControllerLog(description = "查询功能列表")
	public Result queryTree(@PathVariable String funcCd) {
		
		return lrdFunctionService.queryTree(funcCd);
	}
	
	@RequestMapping(value = "/func/{funcCd}", produces = "application/json;charset=UTF-8", method = { RequestMethod.GET })
	@SystemControllerLog(description = "添加功能-查询")
	public Result queryInsert(@PathVariable String funcCd) {
		
		return lrdFunctionService.queryInsert(funcCd);
	}
	
	@RequestMapping(value = "/func", produces = "application/json;charset=UTF-8", method = { RequestMethod.POST })
	@SystemControllerLog(description = "添加")
	public Result insertLrdFunction(HttpServletRequest request,@RequestBody LrdFunction lrdFunction) {
		
		return lrdFunctionService.insertLrdFunction(lrdFunction);
	}
	
	@RequestMapping(value = "/func", produces = "application/json;charset=UTF-8",method = { RequestMethod.PUT })
	@SystemControllerLog(description = "功能修改")
	public Result updateFunction(@RequestBody LrdFunction lrdFunction) {
		
		return lrdFunctionService.updateFunction(lrdFunction);
	}
	
	@RequestMapping(value = "/func", produces = "application/json;charset=UTF-8", method = { RequestMethod.DELETE })
	@SystemControllerLog(description = "功能删除")
	public Result deleteFunction(HttpServletRequest request,@RequestBody LrdFunction lrdFunction) {
		
		return lrdFunctionService.deleteFunction(lrdFunction);
	}
	
	
}
