package org.yjht.web;

import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.yjht.aop.SystemControllerLog;
import org.yjht.bean.LrdRole;
import org.yjht.bean.vo.User;
import org.yjht.core.Result;
import org.yjht.service.LrdRoleService;
import org.yjht.tree.AuthorityMenuTree;
import org.yjht.util.ResultGenerator;

import com.alibaba.fastjson.JSONObject;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/lrdrole")
public class LrdRoleController {
	Logger log = Logger.getLogger(LrdRoleController.class);
	
	@Autowired
	LrdRoleService lrdRoleService;
	
	@RequestMapping(produces = "application/json;charset=UTF-8", method = { RequestMethod.GET })
	@SystemControllerLog(description = "查询角色列表")
	public Result queryAllList(@RequestParam Map<String, Object> map) {
		return lrdRoleService.queryList(map);
	}
	
	@RequestMapping(value = "/add",produces = "application/json;charset=UTF-8", method = RequestMethod.POST)
	@SystemControllerLog(description = "添加角色--添加")
	public Result insertLrdRole(LrdRole lrdRole) {
		return lrdRoleService.InsertLrdRole(lrdRole);
	}
	
	@RequestMapping(produces = "application/json;charset=UTF-8", method = { RequestMethod.PUT })
	@SystemControllerLog(description = "修改角色--修改")
	public Result updateLrdRole(LrdRole lrdRole) {
		System.out.println(lrdRole);
		return lrdRoleService.updateLrdRole(lrdRole);
	}
	
	@RequestMapping(value = "/{roleCd}", produces = "application/json;charset=UTF-8", method = { RequestMethod.DELETE })
	@SystemControllerLog(description = "删除角色")
	public Result delete(@PathVariable String roleCd) {			 
		return lrdRoleService.deleteRole(roleCd);
	}
	@RequestMapping(value = "/{roleCd}/authority/menu", method = RequestMethod.POST)
    public Result modifiyMenuAuthority(@PathVariable String roleCd, String menuTrees,HttpSession session){
        List<AuthorityMenuTree> menus =  JSONObject.parseArray(menuTrees,AuthorityMenuTree.class);
        User user =(User) session.getAttribute("user");
        String corpCd=user.getCorpCD();
        lrdRoleService.modifyAuthorityMenu(roleCd, menus,corpCd);
        return ResultGenerator.genSuccessResult();
    }

    @RequestMapping(value = "/{roleCd}/authority/menu", method = RequestMethod.GET)
    public Result getMenuAuthority(@PathVariable  String roleCd){

        return ResultGenerator.genSuccessResult(lrdRoleService.getAuthorityMenu(roleCd));
    }

    @RequestMapping(value = "/{id}/authority/element/add", method = RequestMethod.POST)
    public Result addElementAuthority(@PathVariable String id,int menuId, int elementId,HttpSession session){
    	 User user =(User) session.getAttribute("user");
         String corpCd=user.getCorpCD();
    	lrdRoleService.modifyAuthorityElement(id,menuId,elementId,corpCd);
        return ResultGenerator.genSuccessResult();
    }

    @RequestMapping(value = "/{id}/authority/element/remove", method = RequestMethod.POST)
    public Result removeElementAuthority(@PathVariable String id,int menuId, int elementId){
    	lrdRoleService.removeAuthorityElement(id,menuId,elementId);
    	return ResultGenerator.genSuccessResult();
    }

    @RequestMapping(value = "/{id}/authority/element", method = RequestMethod.GET)
    public Result addElementAuthority(@PathVariable String id){
        return ResultGenerator.genSuccessResult(lrdRoleService.getAuthorityElement(id));
    }
}
