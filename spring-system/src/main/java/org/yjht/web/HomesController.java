package org.yjht.web;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpSession;
import java.util.Map;

@Controller
public class HomesController {
	//主页
   @RequestMapping(value = "admin",method = RequestMethod.GET)
    public String index(Map<String,Object> map,HttpSession session){
	   map.put("user", session.getAttribute("user"));
       return "system/admin/index";
    }
	//菜单
    @RequestMapping(value = "menu",method = RequestMethod.GET)
    public String menu(){
        return "system/menu/list";
    }
    @RequestMapping(value = "menu/edit",method = RequestMethod.GET)
    public String menuEdit(){
        return "system/menu/edit";
    }
    //角色菜单
    @RequestMapping(value = "group",method = RequestMethod.GET)
    public String group(){
        return "system/group/list";
    }
    //角色授权
    @RequestMapping(value = "group/authority",method = RequestMethod.GET)
    public String groupAuthority(){
        return "system/group/authority";
    }
    //角色编辑
    @RequestMapping(value = "group/edit",method = RequestMethod.GET)
    public String groupEdit(){
        return "system/group/edit";
    }
   
    @RequestMapping(value="element/edit",method = RequestMethod.GET)
    public String elementEdit(){
        return "system/element/edit";
    }
    //日志管理
    @RequestMapping(value = "lrdLog",method = RequestMethod.GET)
    public String gateLog(){
        return "system/lrdLog/list";
    }
    
    //用户管理
    @RequestMapping(value = "sysUser",method = RequestMethod.GET)
    public String sysUser(){
        return "system/sysUser/list";
    }
    @RequestMapping(value = "sysUser/add",method = RequestMethod.GET)
    public String sysUserAdd(){
        return "system/sysUser/add";
    }
    @RequestMapping(value = "sysUser/edit",method = RequestMethod.GET)
    public String sysUserEdit(){
        return "system/sysUser/edit";
    }
    @RequestMapping(value = "updatePwd",method = RequestMethod.GET)
    public String sysUserUpdatePass(){
        return "system/updatePwd/updatepwd";
    }
    //日志管理
    @RequestMapping(value = "sysLog",method = RequestMethod.GET)
    public String sysLog(){
        return "system/sysLog/list";
    }
    @RequestMapping(value = "sysLog/edit",method = RequestMethod.GET)
    public String sysLogEdit(){
    	return "system/sysLog/edit";
    }
    //机构管理
    @RequestMapping(value = "sysOrg",method = RequestMethod.GET)
    public String sysOrg(){
        return "system/sysOrg/list";
    }
    @RequestMapping(value = "sysOrg/add",method = RequestMethod.GET)
    public String sysOrgAdd(){
    	return "system/sysOrg/add";
    }
    //字典管理
    @RequestMapping(value = "sysDic",method = RequestMethod.GET)
    public String sysDic(){
        return "system/sysDic/list";
    }
    //字典添加
    @RequestMapping(value = "sysDic/add",method = RequestMethod.GET)
    public String sysDicAdd(){
    	return "system/sysDic/add";
    }
    //字典编辑
    @RequestMapping(value = "sysDic/edit",method = RequestMethod.GET)
    public String sysDicEdit(){
    	return "system/sysDic/edit";
    }
    //参数管理
    @RequestMapping(value = "sysParam",method = RequestMethod.GET)
    public String sysParam(){
    	return "system/sysParam/list";
    }
    //参数编辑
    @RequestMapping(value = "sysParam/edit",method = RequestMethod.GET)
    public String sysParamEdit(){
    	return "system/sysParam/edit";
    }
    //法人管理
    @RequestMapping(value = "lrdCorp",method = RequestMethod.GET)
    public String lrdCorp(){
    	return "system/lrdCorp/list";
    }
    @RequestMapping(value = "lrdCorp/edit",method = RequestMethod.GET)
    public String lrdCorpEdit(){
    	return "system/lrdCorp/edit";
    }
    //设备管理
    @RequestMapping(value = "padDevice",method = RequestMethod.GET)
    public String padDevice(){
     return "system/padDevice/list";
    }
    @RequestMapping(value = "padDevice/edit",method = RequestMethod.GET)
    public String padDeviceEdit(){
     return "system/padDevice/edit";
    }
}
