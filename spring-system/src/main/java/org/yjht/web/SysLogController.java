package org.yjht.web;

import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.yjht.aop.SystemControllerLog;
import org.yjht.bean.SysLog;
import org.yjht.core.Page;
import org.yjht.core.Result;
import org.yjht.service.SysLogServcie;

@RestController
@RequestMapping("/sysLog")  
@EnableAutoConfiguration 
public class SysLogController {
	Logger log = Logger.getLogger(SysLogController.class);
	@Autowired
	private SysLogServcie sysLogService;

	@RequestMapping(value = "/log",produces = "application/json;charset=UTF-8",method = RequestMethod.GET)
	@SystemControllerLog(description="查询日志")
	public Result findSysLog(SysLog sysLog,Page page){ 
		
		return sysLogService.findSysLog(sysLog,page);
	}

	@RequestMapping(value = "/log/log",produces = "application/json;charset=UTF-8",method = RequestMethod.GET)
	@SystemControllerLog(description="单一查询用户或者机构")
	public Result findUserAndOrgByPK(Map<String,Object> map){
		log.info(map.toString());
		return sysLogService.findByPK(map);
	}

	@RequestMapping(value = "/log",produces = "application/json;charset=UTF-8",method = RequestMethod.DELETE)
	@SystemControllerLog(description="删除日志")
	public Result delete(){
		
			return sysLogService.deleteSysLog();
	}

}
