package org.yjht.web;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.yjht.bean.Element;
import org.yjht.bean.LrdUser;
import org.yjht.bean.Menu;
import org.yjht.bean.vo.PermissionInfo;
import org.yjht.biz.ElementBiz;
import org.yjht.biz.MenuBiz;
import org.yjht.core.CommonConstant;
import org.yjht.service.LrdUserService;
import org.yjht.tree.MenuTree;
import org.yjht.util.TreeUtil;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("api")
public class UserService {
    @Autowired
    private MenuBiz menuBiz;
    @Autowired
    private ElementBiz elementBiz;
    @Autowired
    private LrdUserService lrdUserService;
    

    @RequestMapping(value = "/user/un/{username}/permissions", method = RequestMethod.GET)
    public List<PermissionInfo> getPermissionByUsername(@PathVariable("username") String username){
        LrdUser user = lrdUserService.getUserByUsername(username);
        List<Menu> menus = menuBiz.getUserAuthorityMenuByUserId(user.getUserId());
        List<PermissionInfo> result = new ArrayList<PermissionInfo>();
        PermissionInfo info = null;
        for(Menu menu:menus){
            if(StringUtils.isBlank(menu.getHref()))
                continue;
            info = new PermissionInfo();
            info.setCode(menu.getCode());
            info.setType(CommonConstant.RESOURCE_TYPE_MENU);
            info.setName(CommonConstant.RESOURCE_ACTION_VISIT);
            String uri = menu.getHref();
            if(!uri.startsWith("/"))
                uri = "/"+uri;
            info.setUri(uri);
            info.setMethod(CommonConstant.RESOURCE_REQUEST_METHOD_GET);
            result.add(info);
            info.setMenu(menu.getTitle());
        }
        List<Element> elements = elementBiz.getAuthorityElementByUserId(user.getUserId());
        for(Element element:elements){
            info = new PermissionInfo();
            info.setCode(element.getCode());
            info.setType(element.getType());
            info.setUri(element.getUri());
            info.setMethod(element.getMethod());
            info.setName(element.getName());
            info.setMenu(element.getMenuId());
            result.add(info);
        }
        return result;
    }
    @RequestMapping(value = "/user/un/{username}/system", method = RequestMethod.GET)
    public List<Menu> getSystemsByUsername(@PathVariable("username") String username){
        //String userId = lrdUserService.getUserByUsername(username).getUserId();
        return menuBiz.getUserAuthoritySystemByUserId(username);
    }
    @RequestMapping(value = "/user/un/{username}/menu/parent/{parentId}", method = RequestMethod.GET)
    public List<MenuTree> getMenusByUsername(@PathVariable("username") String username,@PathVariable("parentId")Integer parentId){
        //String userId = lrdUserService.getUserByUsername(username).getUserId();
        try {
            if (parentId == null||parentId<0) {
                parentId = menuBiz.getUserAuthoritySystemByUserId(username).get(0).getId();
            }
        } catch (Exception e) {
            return new ArrayList<MenuTree>();
        }
        return getMenuTree(menuBiz.getUserAuthorityMenuByUserId(username), parentId);
    }

    private List<MenuTree> getMenuTree(List<Menu> menus,int root) {
        List<MenuTree> trees = new ArrayList<MenuTree>();
        MenuTree node = null;
        for (Menu menu : menus) {
            node = new MenuTree();
            BeanUtils.copyProperties(menu, node);
            trees.add(node);
        }
        return TreeUtil.bulid(trees, root) ;
    }
}
