package org.yjht.web;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.yjht.aop.SystemControllerLog;
import org.yjht.bean.SysDic;
import org.yjht.core.Result;
import org.yjht.service.SysDicService;
import org.yjht.util.ResultGenerator;

@RestController
@RequestMapping("/dic")  
@EnableAutoConfiguration 
public class SysDicController {
	@Autowired
	private SysDicService sysDicService;
	@RequestMapping(value = "/find/{parentId}",produces = "application/json;charset=UTF-8",method = RequestMethod.GET)
    public Result selectList(@PathVariable String parentId){ 		
        return sysDicService.finSysDicByParentId(parentId);
    }
	@RequestMapping(value = "/{dicParentid}",produces = "application/json;charset=UTF-8",method = RequestMethod.GET)
	@SystemControllerLog(description = "查询字典信息")
	public Result selectDic(@PathVariable String dicParentid) {
		return ResultGenerator.genSuccessResult(sysDicService.selectDic(dicParentid));
	}
	
	@RequestMapping(value = "/sysDic/tree", produces = "application/json;charset=UTF-8", method = { RequestMethod.GET })
	@SystemControllerLog(description = "查询字典树")
	public Result querySysDicTree() {		
		return sysDicService.querySysDicTree();
	}
	
	@RequestMapping(value = "/sysDic/page", produces = "application/json;charset=UTF-8", method = { RequestMethod.GET })
	@SystemControllerLog(description = "查询")
	public Result querySysDicById(@RequestParam(defaultValue = "0") String parentId) {	
		if(parentId.equals("0")) {
			return ResultGenerator.genSuccessResult(new ArrayList<SysDic>());	
		}else {
			return ResultGenerator.genSuccessResult(sysDicService.querySysDicById(parentId));
		}
	}
	
	@RequestMapping(value = "/sysDic", produces = "application/json;charset=UTF-8", method = { RequestMethod.PUT })
	@SystemControllerLog(description = "修改字典")
	public Result updateSysDic(SysDic sysDic) {		
		return sysDicService.updateSysDic(sysDic);
	}
	@RequestMapping(value = "/sysDic", produces = "application/json;charset=UTF-8", method = { RequestMethod.DELETE })
	@SystemControllerLog(description = "删除字典")
	public Result deleteSysDic(@RequestBody SysDic sysdic) {
		return sysDicService.deleteSysDic(sysdic);
	}
	@RequestMapping(value = "/sysDic", produces = "application/json;charset=UTF-8", method = { RequestMethod.POST })
	@SystemControllerLog(description = "添加字典")
	public Result saveSysDic(@RequestBody List<SysDic> list,HttpServletRequest request) {
		return sysDicService.saveSysDic(request,list);
	}
}

