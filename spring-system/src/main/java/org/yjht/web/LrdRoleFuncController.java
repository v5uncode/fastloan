package org.yjht.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.yjht.aop.SystemControllerLog;
import org.yjht.bean.LrdRolefunc;
import org.yjht.core.Result;
import org.yjht.service.LrdRoleFuncService;

@RestController
@RequestMapping("/lrdRoleFunc")
public class LrdRoleFuncController {
	
	@Autowired
	LrdRoleFuncService lrdRoleFuncService; 
	
	@RequestMapping(value = "/lrdRoleFunc", produces = "application/json;charset=UTF-8", method = { RequestMethod.GET })
	@SystemControllerLog(description = "查询权限")
	public Result findLrdRoleFunc(LrdRolefunc lrdRolefunc) {
		
		return lrdRoleFuncService.findLrdRoleFunc(lrdRolefunc);
	}
}
