package org.yjht.web;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.yjht.aop.SystemControllerLog;
import org.yjht.core.Result;
import org.yjht.service.LrdSysService;

@RestController
@RequestMapping("/lrdsys")
public class LrdSysController {

	Logger log = Logger.getLogger(LrdSysController.class);
	@Autowired
	LrdSysService lrdSysService;
	
	@RequestMapping(value = "/lrdsys", produces = "application/json;charset=UTF-8", method = { RequestMethod.GET })
	@SystemControllerLog(description = "查询系统")
	public Result querySys(HttpServletRequest request) {
		
		return lrdSysService.query(request);
	}
}
