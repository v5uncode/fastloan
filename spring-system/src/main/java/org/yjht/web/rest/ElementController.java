package org.yjht.web.rest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.yjht.bean.Element;
import org.yjht.bean.vo.User;
import org.yjht.biz.ElementBiz;
import org.yjht.core.Result;
import org.yjht.core.TableResultResponse;
import org.yjht.util.ResultGenerator;
import org.yjht.web.rest.base.BaseController;

import tk.mybatis.mapper.entity.Example;

import java.util.List;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("element")
public class ElementController extends BaseController<ElementBiz, Element> {
  /**
   * 查询指定菜单下的功能列表
   * @param limit  每页记录数
   * @param offset 当前页码
   * @param name
   * @param menuId 菜单id
   * @return
   */
  @RequestMapping(value = "/page", method = RequestMethod.GET)
  @ResponseBody
  public TableResultResponse<Element> page(@RequestParam(defaultValue = "10") int limit,
      @RequestParam(defaultValue = "1") int offset,String name, @RequestParam(defaultValue = "0") int menuId) {
    Example example = new Example(Element.class);
    Example.Criteria criteria = example.createCriteria();
    criteria.andEqualTo("menuId", menuId);
    if(StringUtils.isNotBlank(name)){
      criteria.andLike("name", "%" + name + "%");
    }
    List<Element> elements = baseBiz.selectByExample(example);
    return new TableResultResponse<Element>(elements.size(), elements);
  }

  @RequestMapping(value = "/user", method = RequestMethod.GET)
  @ResponseBody
  public Result getAuthorityElement(String menuId,HttpSession session) {
   // int userId = userBiz.getUserByUsername(getCurrentUserName()).getId();
    User user = (User) session.getAttribute("user");
    String userId=user.getUser_id();
    List<Element> elements = baseBiz.getAuthorityElementByUserId(userId + "",menuId);
    return ResultGenerator.genSuccessResult(elements);
  }

  @RequestMapping(value = "/user/menu", method = RequestMethod.GET)
  @ResponseBody
  public Result getAuthorityElement(HttpSession session) {
   //int userId = userBiz.getUserByUsername(getCurrentUserName()).getId();
    User user = (User) session.getAttribute("user");
    String userId=user.getUser_id();
    List<Element> elements = baseBiz.getAuthorityElementByUserId(userId + "");
    return ResultGenerator.genSuccessResult(elements);
  }
}
