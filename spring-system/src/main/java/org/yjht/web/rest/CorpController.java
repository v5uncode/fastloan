package org.yjht.web.rest;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.web.bind.annotation.*;
import org.yjht.aop.SystemControllerLog;
import org.yjht.bean.LrdCorp;
import org.yjht.biz.LrdCorpBiz;
import org.yjht.core.Page;
import org.yjht.core.Result;
import org.yjht.util.ResultGenerator;
import org.yjht.web.rest.base.BaseController;

import java.util.List;
@RestController
@RequestMapping("corp")
public class CorpController extends BaseController<LrdCorpBiz, LrdCorp>{
	
	@GetMapping(value = "/page")
	@SystemControllerLog(description = "查询法人信息列表")
	public Result page(LrdCorp param,Page page) {
		PageHelper.startPage(page.getPage(), page.getLimit(),true);
		List<LrdCorp> list = baseBiz.selectList(param);
		page.setTotal(new PageInfo<LrdCorp>(list).getTotal());
		page.setRows(list);
		return ResultGenerator.genSuccessResult(page);	
	}

	@PostMapping(value = "/add",produces = "application/json;charset=UTF-8")
	public Result addCorp(@RequestBody LrdCorp corp){
		LrdCorp oldCorp = baseBiz.selectById(corp.getCorpCd());
		if(oldCorp != null){
			return ResultGenerator.genFailResult("该法人号已存在，请检查");
		}
		baseBiz.insertSelective(corp);
		return ResultGenerator.genSuccessResult("添加成功");
	}

	@PutMapping(value = "/update",produces = "application/json;charset=UTF-8")
	public Result updateCorp(@RequestBody LrdCorp corp){
		baseBiz.updateSelectiveById(corp);
		return ResultGenerator.genSuccessResult("修改成功");
	}

	@DeleteMapping(value = "/delete",produces = "application/json;charset=UTF-8")
	public Result deleteCorp(@RequestBody LrdCorp corp){
		baseBiz.delete(corp);
		return ResultGenerator.genSuccessResult("删除成功成功");
	}




}
