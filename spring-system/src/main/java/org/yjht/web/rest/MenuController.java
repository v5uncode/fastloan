package org.yjht.web.rest;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.yjht.bean.Menu;
import org.yjht.bean.vo.User;
import org.yjht.biz.MenuBiz;
import org.yjht.core.CommonConstant;
import org.yjht.tree.AuthorityMenuTree;
import org.yjht.tree.MenuTree;
import org.yjht.util.TreeUtil;
import org.yjht.web.rest.base.BaseController;

import tk.mybatis.mapper.entity.Example;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("menu")
public class MenuController extends BaseController<MenuBiz, Menu> {
    /*@Autowired
    private UserBiz userBiz;*/
	/**
	 * 查询菜单列表
	 * @param title  菜单名称
	 * @return
	 */
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public List<Menu> list(String title) {
        Example example = new Example(Menu.class);
        if (StringUtils.isNotBlank(title))
            example.createCriteria().andLike("title", "%" + title + "%");
        return baseBiz.selectByExample(example);
    }
    /**
     * 获取所有菜单
     * @return
     */
    @RequestMapping(value = "/system", method = RequestMethod.GET)
    public List<Menu> getSystem() {
        Menu menu = new Menu();
        menu.setParentId(CommonConstant.ROOT);
        return baseBiz.selectList(menu);
    }
    /**
     * 查询当前系统菜单下所有子菜单
     * @param parentId 父菜单ID
     * @return
     */
    @RequestMapping(value = "/menuTree", method = RequestMethod.GET)
    public List<MenuTree> listMenu(Integer parentId) {
        try {
            if (parentId == null) {
                parentId = this.getSystem().get(0).getId();
            }
        } catch (Exception e) {
            return new ArrayList<MenuTree>();
        }
        List<MenuTree> trees = new ArrayList<MenuTree>();
        MenuTree node = null;
        Example example = new Example(Menu.class);
        Menu parent = baseBiz.selectById(parentId);
        example.createCriteria().andLike("path", parent.getPath() + "%").andNotEqualTo("id",parent.getId());
        return getMenuTree(baseBiz.selectByExample(example), parent.getId());
    }

    @RequestMapping(value = "/authorityTree", method = RequestMethod.GET)
    public List<AuthorityMenuTree> listAuthorityMenu() {
        List<AuthorityMenuTree> trees = new ArrayList<AuthorityMenuTree>();
        AuthorityMenuTree node = null;
        for (Menu menu : baseBiz.selectListAll()) {
            node = new AuthorityMenuTree();
            node.setText(menu.getTitle());
            BeanUtils.copyProperties(menu, node);
            trees.add(node);
        }
        return TreeUtil.bulid(trees,CommonConstant.ROOT);
    }

    @RequestMapping(value = "/user/authorityTree", method = RequestMethod.GET)
    public List<MenuTree> listUserAuthorityMenu(Integer parentId,HttpSession session){
    	User user = (User) session.getAttribute("user");
    	String userId = user.getUser_id();
       // int userId = userBiz.getUserByUsername(getCurrentUserName()).getId();
        try {
            if (parentId == null) {
                parentId = this.getSystem().get(0).getId();
            }
        } catch (Exception e) {
            return new ArrayList<MenuTree>();
        }
        return getMenuTree(baseBiz.getUserAuthorityMenuByUserId(userId),parentId);
    }

    @RequestMapping(value = "/user/system", method = RequestMethod.GET)
    public List<Menu> listUserAuthoritySystem(HttpSession session) {
        //int userId = userBiz.getUserByUsername(getCurrentUserName()).getId();
    	User user = (User) session.getAttribute("user");
    	String userId = user.getUser_id();
        return baseBiz.getUserAuthoritySystemByUserId(userId);
    }

    private List<MenuTree> getMenuTree(List<Menu> menus,int root) {
        List<MenuTree> trees = new ArrayList<MenuTree>();
        MenuTree node = null;
        for (Menu menu : menus) {
            node = new MenuTree();
            BeanUtils.copyProperties(menu, node);
            trees.add(node);
        }
        return TreeUtil.bulid(trees,root) ;
    }


}
