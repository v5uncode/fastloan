package org.yjht.web.rest;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.*;
import org.yjht.aop.SystemControllerLog;
import org.yjht.bean.PadDevice;
import org.yjht.core.Page;
import org.yjht.core.Result;
import org.yjht.service.device.PadDeviceService;
import org.yjht.util.ResultGenerator;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/device")
@EnableAutoConfiguration
public class PadDeviceController {
    @Autowired
    private PadDeviceService deviceService;

    @RequestMapping(value = "/list", produces = "application/json;charset=UTF-8", method = RequestMethod.GET)
    @SystemControllerLog(description = "查询设备信息")
    public Result findDevice(PadDevice device, Page page) {
        PageHelper.startPage(page.getPage(), page.getLimit(), true);
        List<PadDevice> list = deviceService.findList(device);
        page.setTotal(new PageInfo<PadDevice>(list).getTotal());
        page.setRows(list);
        return ResultGenerator.genSuccessResult(page);


    }

    @RequestMapping(value = "/add", produces = "application/json;charset=UTF-8", method = {RequestMethod.POST})
    @SystemControllerLog(description = "添加设备")
    public Result insertDevice(@RequestBody PadDevice device, HttpServletRequest request) {

        return deviceService.saveDevice(device, request);
    }

    @RequestMapping(value = "/edit", produces = "application/json;charset=UTF-8", method = {RequestMethod.PUT})
    @SystemControllerLog(description = "修改设备")
    public Result updatePadDevice(@RequestBody PadDevice device, HttpServletRequest request) {
        System.out.println(device);
        return deviceService.updateDevice(device, request);
    }

    @RequestMapping(produces = "application/json;charset=UTF-8", method = RequestMethod.DELETE)
    @SystemControllerLog(description = "删除设备信息")
    public Result deleteDevice(@RequestBody PadDevice device) {
        return deviceService.delete(device.getDeviceId());
    }

}
