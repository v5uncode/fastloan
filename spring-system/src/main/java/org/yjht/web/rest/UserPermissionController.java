package org.yjht.web.rest;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.yjht.aop.SystemControllerLog;
import org.yjht.bean.vo.User;
import org.yjht.core.Result;
import org.yjht.service.LrdUserService;
import org.yjht.util.CommonUtil;
import org.yjht.util.ResultGenerator;
import org.yjht.web.UserService;
import com.alibaba.fastjson.JSONObject;

@RestController
@RequestMapping("/admin")
public class UserPermissionController {
    @Autowired
    private UserService userService;
    @Autowired
    private LrdUserService lrdUserService;
    
    @RequestMapping(value = "/user/system",method = RequestMethod.GET)
    public Result getUserSystem(){
       User user=CommonUtil.getUser();
       return ResultGenerator.genSuccessResult(userService.getSystemsByUsername(user.getUser_id()));
    }
    @RequestMapping(value = "/user/menu",method = RequestMethod.GET)
    public String getUserMenu(@RequestParam(defaultValue = "-1") Integer parentId){
    	User user=CommonUtil.getUser();
        return JSONObject.toJSONString(userService.getMenusByUsername(user.getUser_id(),parentId));
    }
    @RequestMapping(value = "/user/pad/menu",method = RequestMethod.GET)
    public Result getUserPadMenu(@RequestParam(defaultValue = "-1") Integer parentId){
    	User user=CommonUtil.getUser();
    	if(user!=null){
    		return ResultGenerator.genSuccessResult(userService.getMenusByUsername(user.getUser_id(),parentId));
    	}else{
    		return ResultGenerator.genUNAUTHORResult();
    	}
    }
    @RequestMapping(value = "/user/updatePwd", produces = "application/json;charset=UTF-8", method = { RequestMethod.POST })
	@SystemControllerLog(description = "修改密码")
	public Result updatePwd(HttpServletRequest request,@RequestBody Map<String,Object> map){		
		return lrdUserService.updatePwd(request,map);
	}
}
