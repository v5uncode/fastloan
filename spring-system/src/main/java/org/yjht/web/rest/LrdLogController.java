package org.yjht.web.rest;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.yjht.aop.SystemControllerLog;
import org.yjht.bean.LrdLog;
import org.yjht.bean.vo.User;
import org.yjht.biz.LrdLogBiz;
import org.yjht.core.Constants;
import org.yjht.core.Page;
import org.yjht.core.Result;
import org.yjht.service.LrdLogService;
import org.yjht.service.LrdOrgService;
import org.yjht.util.ResultGenerator;
import org.yjht.web.rest.base.BaseController;

import tk.mybatis.mapper.entity.Condition;
import tk.mybatis.mapper.entity.Example.Criteria;

@RestController
@RequestMapping("/log")
public class LrdLogController extends BaseController<LrdLogBiz,LrdLog> {
	@Autowired
	private LrdLogService lrdLogService;
	@Autowired
	private LrdOrgService lrdOrgService;
	
    @RequestMapping(value = "/page",method = RequestMethod.GET)
    @SystemControllerLog(description = "查询日志")
    public Result page(HttpServletRequest request,Page page,LrdLog log){
    	User user = (User) request.getSession().getAttribute("user");
    	Condition condition = new Condition(LrdLog.class);
    	Criteria criteria=condition.createCriteria();
    	if (!Constants.SLS_CORP.equals(user.getCorpCD())) {
    		criteria.andEqualTo("corpCd", user.getCorpCD());
		}
        if(log.getCrtTime()!=null) {
			criteria.andCondition("CRT_TIME >=", log.getCrtTime());
		}
		if(log.getEndTime()!=null) {
			criteria.andCondition("CRT_TIME <=", log.getEndTime());
		}
		if(StringUtils.isNotBlank(log.getOrgCd())){
			criteria.andEqualTo("orgCd",log.getOrgCd());
		}
		if(StringUtils.isNotBlank(log.getCrtUser())){
//			String[] userIDs = log.getCrtUser().toString().split(",");
//			List<String> userIds=Arrays.asList(userIDs);
//			criteria.andIn("crtUser", userIds);
			criteria.andEqualTo("crtUser",log.getCrtUser());
		}
		condition.orderBy("crtTime").desc();
        PageHelper.startPage(page.getPage(), page.getLimit(),true);
        List<LrdLog> list=baseBiz.selectByExample(condition);
        page.setTotal(new PageInfo<LrdLog>(list).getTotal());
        page.setRows(list);
        return ResultGenerator.genSuccessResult(page);
    }
    @RequestMapping(produces = "application/json;charset=UTF-8",method = RequestMethod.DELETE)
    @SystemControllerLog(description = "清除日志")
	public Result deleteAll(HttpServletRequest request){	
    	User user = (User) request.getSession().getAttribute("user");
    	LrdLog log=new LrdLog();
    	if (!Constants.SLS_CORP.equals(user.getCorpCD())) {
    		log.setCorpCd(user.getCorpCD());
		}
    	baseBiz.delete(log);
		// baseBiz.deleteAll();
		 return ResultGenerator.genSuccessResult();
	}
    @RequestMapping(value = "/orgTree",produces = "application/json;charset=UTF-8",method = RequestMethod.GET)
	@SystemControllerLog(description="单一查询机构")
	public Result findOrg(HttpServletRequest request,@RequestParam Map<String,Object> map){
    	User user = (User) request.getSession().getAttribute("user");
		return lrdOrgService.queryMenu(user,map);
	}
    @RequestMapping(value = "/user",produces = "application/json;charset=UTF-8",method = RequestMethod.GET)
	@SystemControllerLog(description="单一查询用户")
	public Result findUserList(HttpServletRequest request){
		//return lrdLogService.findByPK(map);
    	return ResultGenerator.genSuccessResult();
	}
}
