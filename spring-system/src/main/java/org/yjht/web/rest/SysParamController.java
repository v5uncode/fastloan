package org.yjht.web.rest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.yjht.aop.SystemControllerLog;
import org.yjht.bean.Element;
import org.yjht.bean.LrdLog;
import org.yjht.bean.SysDic;
import org.yjht.bean.SysPara;
import org.yjht.bean.vo.User;
import org.yjht.biz.SysParaBiz;
import org.yjht.core.Constants;
import org.yjht.core.Page;
import org.yjht.core.Result;
import org.yjht.util.ResultGenerator;
import org.yjht.web.rest.base.BaseController;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import tk.mybatis.mapper.entity.Condition;
import tk.mybatis.mapper.entity.Example.Criteria;
@RestController
@RequestMapping("param")
public class SysParamController extends BaseController<SysParaBiz, SysPara> {
	
	  @GetMapping(value = "/page")
	  @SystemControllerLog(description = "查询系统参数列表")
	  public Result page(SysPara param,Page page,HttpSession session) {
		 User user=(User) session.getAttribute("user");
		 if (!Constants.SLS_CORP.equals(user.getCorpCD())) {
			 param.setCorpCd(user.getCorpCD()); 
		 }		 
		 param.setChparavisible(Constants.YES);
		 if(StringUtils.isBlank(param.getChparalocale())) {
			 param.setChparalocale(null);
		 }
		 PageHelper.startPage(page.getPage(), page.getLimit(),true);
		 List<SysPara> list = baseBiz.selectList(param);
		 Map<String,String> dicMap=new HashMap<String,String>();
		 page.setTotal(new PageInfo<SysPara>(list).getTotal());
		 page.setRows(list);
		 Map<String,Object> map=new HashMap<String,Object>();
		 map.put("corpCd", user.getCorpCD());
		 map.put("page", page);
		 return ResultGenerator.genSuccessResult(map);
	  }
	  @DeleteMapping(produces = "application/json;charset=UTF-8")
	  public Result delete(@RequestBody SysPara param) {
		  baseBiz.deleteById(param);
		  return ResultGenerator.genSuccessResult();
	  }
	  @GetMapping(value = "/getParam/{baseRate}")
	  @SystemControllerLog(description = "根据法人号和参数标识查询参数值")
	  public Result getParam(HttpSession session,@PathVariable String baseRate){
		  User user=(User) session.getAttribute("user");
		  SysPara param = new SysPara();
		  param.setCorpCd(user.getCorpCD());
		  param.setChparakey(baseRate);
		  SysPara resuleParam = baseBiz.selectOne(param);
		  return ResultGenerator.genSuccessResult(resuleParam);
	  }
}
