package org.yjht.web.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.yjht.core.Result;
import org.yjht.service.device.PadDeviceService;
import org.yjht.util.ResultGenerator;

@RestController
@RequestMapping("/admin")
public class AdminSysConstroller {
	@Autowired
	private PadDeviceService padDeviceService;
	@GetMapping(value = "/device/{deviceId}")
	public Result findPad(@PathVariable String deviceId){		
		return ResultGenerator.genSuccessResult(padDeviceService.findById(deviceId));
	}

}
