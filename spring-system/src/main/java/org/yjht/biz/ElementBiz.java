package org.yjht.biz;


import org.springframework.stereotype.Service;
import org.yjht.bean.Element;
import org.yjht.dao.ElementMapper;

import java.util.List;

@Service
public class ElementBiz extends BaseBiz<ElementMapper,Element> {
    public List<Element> getAuthorityElementByUserId(String userId){
       return mapper.selectAuthorityElementByUserId(userId);
    }
    public List<Element> getAuthorityElementByUserId(String userId,String menuId){
        return mapper.selectAuthorityMenuElementByUserId(userId,menuId);
    }
}
