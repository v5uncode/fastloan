package org.yjht.biz;

import org.springframework.stereotype.Service;
import org.yjht.bean.LrdCorp;
import org.yjht.dao.LrdCorpMapper;
import org.yjht.util.DateTools;
@Service
public class LrdCorpBiz extends BaseBiz<LrdCorpMapper, LrdCorp> {
	
	@Override
    public void insert(LrdCorp entity) {
		entity.setCrtDate(DateTools.getCurrentSysData(DateTools.DEFAULT_FORMAT));
 		entity.setMtnDate(DateTools.getCurrentSysData(DateTools.DEFAULT_FORMAT));
        mapper.insert(entity);
    }

    @Override
    public void insertSelective(LrdCorp entity) {
    	entity.setCrtDate(DateTools.getCurrentSysData(DateTools.DEFAULT_FORMAT));
    	entity.setMtnDate(DateTools.getCurrentSysData(DateTools.DEFAULT_FORMAT));
        mapper.insertSelective(entity);
    }
    @Override
    public void updateSelectiveById(LrdCorp entity) {
    	entity.setMtnDate(DateTools.getCurrentSysData(DateTools.DEFAULT_FORMAT));
        mapper.updateByPrimaryKeySelective(entity);
    }

}
