package org.yjht.biz;

import org.springframework.stereotype.Service;
import org.yjht.bean.SysPara;
import org.yjht.dao.SysParaMapper;
import org.yjht.util.DateTools;
@Service
public class SysParaBiz extends BaseBiz<SysParaMapper, SysPara> {
	
	 	@Override
	    public void insert(SysPara entity) {
	 		entity.setMtnDate(DateTools.getCurrentSysData(DateTools.DEFAULT_FORMAT));
	        mapper.insert(entity);
	    }

	    @Override
	    public void insertSelective(SysPara entity) {
	    	entity.setMtnDate(DateTools.getCurrentSysData(DateTools.DEFAULT_FORMAT));
	        mapper.insertSelective(entity);
	    }
	    @Override
	    public void updateSelectiveById(SysPara entity) {
	    	entity.setMtnDate(DateTools.getCurrentSysData(DateTools.DEFAULT_FORMAT));
	        mapper.updateByPrimaryKeySelective(entity);
	    }
}
