package org.yjht.biz;

import org.springframework.stereotype.Service;
import org.yjht.bean.Menu;
import org.yjht.core.CommonConstant;
import org.yjht.dao.MenuMapper;
import org.yjht.util.DateTools;

import java.util.Date;
import java.util.List;
@Service
public class MenuBiz extends BaseBiz<MenuMapper,Menu> {
    @Override
    public void insertSelective(Menu entity) {
        if(CommonConstant.ROOT == entity.getParentId()){
            entity.setPath("/"+entity.getCode());
        }else{
            Menu parent = this.selectById(entity.getParentId());
            entity.setPath(parent.getPath()+"/"+entity.getCode());
        }
        entity.setCrtTime(DateTools.getCurrentSysData(DateTools.FULLTIME_FORMAT));
        entity.setUpdTime(DateTools.getCurrentSysData(DateTools.FULLTIME_FORMAT));
        super.insertSelective(entity);
    }

    @Override
    public void updateById(Menu entity) {
        if(CommonConstant.ROOT == entity.getParentId()){
            entity.setPath("/"+entity.getCode());
        }else{
            Menu parent = this.selectById(entity.getParentId());
            entity.setPath(parent.getPath()+"/"+entity.getCode());
        }
        entity.setUpdTime(DateTools.getCurrentSysData(DateTools.FULLTIME_FORMAT));
        super.updateById(entity);
    }
    @Override
    public void updateSelectiveById(Menu entity) {
        if(CommonConstant.ROOT == entity.getParentId()){
            entity.setPath("/"+entity.getCode());
        }else{
            Menu parent = this.selectById(entity.getParentId());
            entity.setPath(parent.getPath()+"/"+entity.getCode());
        }
        entity.setUpdTime(DateTools.getCurrentSysData(DateTools.FULLTIME_FORMAT));
        super.updateById(entity);
    }
    /**
     * 获取用户可以访问的菜单
     * @param id
     * @return
     */
    public List<Menu> getUserAuthorityMenuByUserId(String userId){
        return mapper.selectAuthorityMenuByUserId(userId);
    }

    /**
     * 根据用户获取可以访问的系统
     * @param id
     * @return
     */
    public List<Menu> getUserAuthoritySystemByUserId(String UserId){
        return mapper.selectAuthoritySystemByUserId(UserId);
    }
}
